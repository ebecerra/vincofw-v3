define(['app'], function (app) {
    "use strict";

    return app.controller("LanguagesCtrl", LanguagesCtrl);

    function LanguagesCtrl($scope, $rootScope, Authentication) {
        // Setting the selected language for the current user
        $rootScope.selectedLanguage = _.find($rootScope.languages, function(language){
            return (language.idLanguage === Authentication.getCurrentLanguage())
        });

        /**
         * Sets the currently selected language
         *
         * @param language Language to set
         */
        $scope.selectLanguage = function (language) {
            $rootScope.selectedLanguage = language;
            localStorage["selected_language"] = language.idLanguage;
        };
    }
});