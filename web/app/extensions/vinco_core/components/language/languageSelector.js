define(['app'], function(module){
    "use strict";

    /**
     * This directive is used to display the Language Selector Control
     */
    module.registerDirective('languageSelector',['cacheService', function(cacheService){
        return {
            restrict: "EA",
            replace: true,
            templateUrl: APP_EXTENSION_VINCO_CORE + 'components/language/language-selector.tpl.html?v=' + FW_VERSION,
            scope: true
        }
    }]);
});
