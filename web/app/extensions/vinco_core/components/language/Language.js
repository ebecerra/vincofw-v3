define(['app'], function (app) {
    "use strict";

    return app.factory('Language', ['$rootScope', 'Authentication', function ($rootScope, Authentication) {

        $rootScope.lang = {};
        $rootScope.getWord = function (key, args) {
            if (angular.isDefined($rootScope.lang[key])) {
                var value = $rootScope.lang[key];
                // Codes sent as parameters are replaced
                if (value && args && args.length > 0) {
                    value = vsprintf(value, args);
                }
                return value ? value : key;
            } else {
                return key;
            }
        };

        /**
         * Gets the caption of a column
         *
         * @param table     Table containing the column
         * @param column    Column
         * @param id        Translation key
         * @returns {*} Translated key or the key itself if no translation was found
         */
        $rootScope.getColumnCaption = function (table, column, id) {
            if (table === undefined) {
                if ($rootScope.translationChartFilterParams && angular.isDefined($rootScope.translationChartFilterParams[column])) {
                    return $rootScope.translationChartFilterParams[column];
                } else {
                    return column;
                }
            }
            var key = table + "$" + column;
            if (id)
                key += ("$" + id);
            if ($rootScope.tranlationsField && angular.isDefined($rootScope.tranlationsField[key])) {
                return $rootScope.tranlationsField[key];
            } else {
                return key;
            }
        };

        /**
         * Gets the caption of a column
         *
         * @param table     Table containing the column
         * @param column    Column
         * @param id        Translation key
         * @returns {*} Translated key or the key itself if no translation was found
         */
        $rootScope.getColumnChartCaption = function (chart, column) {
            var key = chart + "$" + column;
            if ($rootScope.translationChartColumns && angular.isDefined($rootScope.translationChartColumns[key])) {
                return $rootScope.translationChartColumns[key];
            } else {
                return key;
            }
        };

        /**
         * Gets a translated message
         *
         * @param key   Translation key
         * @param args  Translation extra parameters
         * @returns {*} Translated key or the key itself if no translation was found
         */
        $rootScope.getMessage = function (key, args) {
            if ($rootScope.tranlationsMessages && angular.isDefined($rootScope.tranlationsMessages[key])) {
                var value = $rootScope.tranlationsMessages[key];
                // Codes sent as parameters are replaced
                if (value && args && args.length > 0) {
                    value = vsprintf(value, args);
                }
                return value ? value : key;
            } else {
                if (args && args.length > 0) {
                    var result = vsprintf(key, args);
                    return result;
                }
                return key;
            }
        };

        /**
         * Gets the translated name of a process parameter
         *
         * @param name  Process parameter name
         * @returns {*} Translated name or the name itself if no translation was found
         */
        $rootScope.getProcessParam = function (name) {
            if ($rootScope.tranlationsProcessParams && angular.isDefined($rootScope.tranlationsProcessParams[name])) {
                return $rootScope.tranlationsProcessParams[name];
            } else {
                return name;
            }
        };

        // Notifying the translation functions are available
        $rootScope.$broadcast(app.eventDefinitions.TRANSLATIONS_READY);

        return {};
    }]);
});
