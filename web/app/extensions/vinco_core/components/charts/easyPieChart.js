define(['app', 'lodash'], function (app, _) {

    'use strict';

    return app.registerDirective('easyPieChart', ['$log', 'cacheService', 'chartRendererService',
        function ($log, cacheService, chartRendererService) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    chartItem: '=',
                    filterInfo: '=',
                    filterRangeSet: '='
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'components/charts/easy-pie-chart.tpl.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes) {

                            /**
                             * Unregister watch function
                             * @type {Function}
                             */
                            scope.unwatch = undefined;

                            /**
                             * Previous filter in string format. Used to verify if the filter has changed from the previous value in order to refresh the chart
                             * @type {string}
                             */
                            scope.oldFilter = undefined;

                            var color = '#2C3E50';
                            scope.pies = [];
                            scope.percent = 25;
                            scope.baseOption = {
                                animate: {
                                    duration: 3000,
                                    enabled: true
                                },
                                barColor: color,
                                rotate: -90,
                                scaleColor: false,
                                size: 150,
                                trackColor: '#dfdedb',
                                lineWidth: 15,
                                lineCap: 'circle'
                            };

                            scope.tableElements = [];

                            scope.$on(app.eventDefinitions.GLOBAL_TAB_SELECTED, function (event, data) {
                                chartRendererService.doGlobalTabSelected(scope, data);
                            });

                            scope.$on(app.eventDefinitions.CHART_RESET_SEARCH, function (event, data) {
                                chartRendererService.doChartResetSearch(scope, data);
                            });

                            scope.$on(app.eventDefinitions.CHART_ITEM_CLICKED, function (event, data) {
                                chartRendererService.doChartItemClicked(scope, data);
                            });

                            scope.$on('$destroy', function() {
                                chartRendererService.doDestroy(scope);
                            });

                            var parentQuery = "";
                            if (scope.parentItem) {
                                _.each(scope.parentItem.fields, function (field) {
                                    if (field.usedInChild) {
                                        parentQuery += ("," + field.name + "=" + scope.parentItem.item[field.standardName]);
                                    }
                                });
                            }

                            scope.updateChart = function (extraArgs) {
                                chartRendererService.evaluateChart(scope, extraArgs, function (data) {
                                    if (scope.unwatch)
                                        scope.unwatch();
                                    scope.unwatch = scope.$watchCollection("[filterInfo.filtersRange, filterInfo.filters]", function (newData, old, scope) {
                                        chartRendererService.doUpdate(newData, old, scope);
                                    });

                                    scope.items = data.items;
                                    scope.bootstrapCol = (12 % scope.items) == 0 ? 12 / scope.items : 12 % scope.items;
                                    scope.total = 0;
                                    var percentages = [], chartStyle = [], chartOptions = [], chartValues = [], chartLegends = [];
                                    var i;
                                    for (i = 0; i < scope.items; i++) {
                                        scope.total += data.values[0]['value' + (i + 1)];
                                        var option = _.clone(scope.baseOption);
                                        if (data['color' + (i + 1)])
                                            option.barColor = data['color' + (i + 1)];
                                        chartOptions.push(option);
                                        chartStyle.push("color: " + option.barColor);
                                        chartLegends.push(data['legend' + (i + 1)]);
                                    }
                                    for (i = 0; i < scope.items; i++) {
                                        var value = data.values[0]['value' + (i + 1)];
                                        if (value === null)
                                            value = 0;
                                        var percentage = scope.total == 0 ? 0 : Number((value * 100 / scope.total)).toFixed(0);
                                        if (data.values[0]['value' + (i + 1)] == 0)
                                            percentage = 0;
                                        chartValues.push(value);
                                        percentages.push(percentage);
                                    }
                                    scope.pies = [];
                                    _.each(percentages, function (percentage, index) {
                                        scope.pies.push({
                                            percentage: percentage,
                                            chartStyle: chartStyle[index],
                                            chartOptions: chartOptions[index],
                                            chartValues: chartValues[index],
                                            chartLegends: chartLegends[index]
                                        });
                                    });
                                });
                            };

                            cacheService.updateLinkedCharts(scope.chartItem.idChart).then(function (linkedCharts) {
                                scope.chartItem.linkedCharts = linkedCharts;
                                scope.updateChart();
                            }).catch(function (err) {
                                $log.error(err);
                            });
                        }
                    }
                }
            }
        }
    ]);
});
