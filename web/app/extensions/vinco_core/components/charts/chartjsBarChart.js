define(['app', 'chartjs'], function (app) {

    'use strict';

    return app.registerDirective('chartjsBarChart', ['adChartService', 'cacheService', '$log', 'chartRendererService',
        function (adChartService, cacheService, $log, chartRendererService) {
        return {
            restrict: 'A',
            scope: {
                chartItem: '=',
                filterInfo: '=',
                filterRangeSet: '='
            },
            link: function (scope, element, attributes) {
                var qParam = chartRendererService.getQueryParam(scope.filterInfo, scope.filterRangeSet);
                adChartService.evaluate({
                    idClient: cacheService.loggedIdClient(),
                    idUserLogged: Authentication.getLoginData().idUserLogged,
                    id: scope.chartItem.idChart,
                    q: qParam
                }, function (data) {

                    var barOptions = {
                        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                        scaleBeginAtZero : true,
                        //Boolean - Whether grid lines are shown across the chart
                        scaleShowGridLines : true,
                        //String - Colour of the grid lines
                        scaleGridLineColor : "rgba(0,0,0,.05)",
                        //Number - Width of the grid lines
                        scaleGridLineWidth : 1,
                        //Boolean - If there is a stroke on each bar
                        barShowStroke : true,
                        //Number - Pixel width of the bar stroke
                        barStrokeWidth : 1,
                        //Number - Spacing between each of the X value sets
                        barValueSpacing : 5,
                        //Number - Spacing between data sets within X values
                        barDatasetSpacing : 1,
                        //Boolean - Re-draw chart on page resize
                        responsive: true,
                        //String - A legend template
                        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                    };

                    var barData = {
                        labels: [],
                        datasets: []
                    };

                    var dataSets = [];
                    for (var i = 1; i <= data.items; i++) {
                        var labels = [];
                        var dataSet = {
                            label: data['legend'+i],
                            fillColor: chartRendererService.getColor(i).color,
                            strokeColor: "rgba(151,187,205,0.8)",
                            highlightFill: chartRendererService.getColor(i).hover,
                            highlightStroke: "rgba(151,187,205,1)",
                            data: []
                        };
                        _.each(data.values, function (current, index) {
                            labels.push(current.x);
                            dataSet.data.push(current['value' + i]);
                        });
                        dataSets.push(dataSet);
                        barData.labels = labels;
                    }
                    barData.datasets = dataSets;

                    // render chart
                    var ctx = element[0].getContext("2d");
                    new Chart(ctx).Bar(barData, barOptions);
                }, function (err) {
                    $log.error(err);
                });
            }
        }
    }]);
});
