/**
 * This directive generates a grid corresponding to the data defined in the given tab
 * It receives the following parameters in the form of attributes
 *
 *      tab-generator-id        ->  The idTab key
 *      table-generator-title   ->  The text to show in the title of the table
 *      table-generator-icon    ->  The icon class to use in the table header
 *      table-reference-item    ->  Parent item associated to the grid (the items of the grid by this item's reference id)
 *      hide-header             ->  Hides the header caption and icon
 *      table-filter            ->  Linked column filter value
 *
 * The following is an example usage of the directive
 *
 *  <autogen-table-generator tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018"
 *      table-generator-title="window.title"
 *      table-generator-icon = "fa-list-alt"></autogen-table-generator>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('listChart', ['$rootScope', '$log', 'cacheService', 'chartRendererService',
        function ($rootScope, $log, cacheService, chartRendererService) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    chartItem: '=',
                    filterInfo: '=',
                    filterRangeSet: '=',
                    filterSet: '=',
                    parentItem: '='
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.list.chart.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            /**
                             * Unregister watch function
                             * @type {Function}
                             */
                            scope.unwatch = undefined;

                            /**
                             * Previous filter in string format. Used to verify if the filter has changed from the previous value in order to refresh the chart
                             * @type {string}
                             */
                            scope.oldFilter = undefined;
                            scope.tableElements = [];
                            scope.tableTotalElements = 0;
                            // Max amount of selectable pages in paginator. More than this will be displayed as ...
                            scope.maxSize = 3;
                            scope.tableData = {
                                currentPage: 1,
                                totalElements: 0,
                                pageSize: scope.chartItem.rowLimit
                            };

                            cacheService.processChartColumns(scope.chartItem.idTab, scope.chartItem.idChart).then(function(columns){
                                scope.chartItem.columns = columns;
                                scope.gridColumns = columns;
                                if (scope.gridColumns) {
                                    _.each(scope.gridColumns, function (current) {
                                        if (current.reference.rtype == 'TABLE' || current.reference.rtype == 'TABLEDIR' || current.reference.rtype == 'SEARCH' || current.reference.rtype == 'LIST')
                                            cacheService.loadReferenceInfo(current.idReferenceValue);
                                    });
                                }
                                scope.visibleColumns = _.filter(columns, function (current) {
                                    return current.displayed;
                                });
                                scope.visibleColumns = _.sortBy(scope.visibleColumns, function (current) {
                                    return current.seqno;
                                });
                            });

                            scope.changePage = function(page){
                                scope.tableData.currentPage = page;
                                scope.updateChart();
                            };

                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            scope.getMessage = $rootScope.getMessage;
                            scope.getColumnChartCaption = $rootScope.getColumnChartCaption;

                            scope.$on(app.eventDefinitions.GLOBAL_TAB_SELECTED, function (event, data) {
                                chartRendererService.doGlobalTabSelected(scope, data);
                            });

                            scope.$on(app.eventDefinitions.CHART_ITEM_CLICKED, function (event, data) {
                                chartRendererService.doChartItemClicked(scope, data);
                            });

                            scope.$on(app.eventDefinitions.CHART_RESET_SEARCH, function (event, data) {
                                chartRendererService.doChartResetSearch(scope, data);
                            });

                            scope.clickRow = function(index, item){
                                $rootScope.$broadcast(app.eventDefinitions.CHART_ITEM_CLICKED, {
                                    idChart: scope.chartItem.idChart,
                                    value: item.keyValue,
                                    keyField: scope.chartItem.keyField
                                });
                            };

                            scope.$on('$destroy', function() {
                                chartRendererService.doDestroy(scope);
                            });

                            scope.updateChart = function (extraArgs) {
                                chartRendererService.evaluateChart(scope, extraArgs, function (data) {
                                    if (scope.unwatch)
                                        scope.unwatch();
                                    scope.unwatch = scope.$watchCollection("[filterInfo.filtersRange, filterInfo.filters]", function (newData, old, scope) {
                                        chartRendererService.doUpdate(newData, old, scope);
                                    });

                                    scope.tableElements = [];
                                    scope.tableTotalElements = data.items;
                                    for (var i = 0; i < data.values.length; i++) {
                                        var row = {
                                            keyValue: data.values[i].id
                                        };
                                        _.each(data.values[i].valueList, function(current){
                                           var column = _.find(scope.gridColumns, function(column){
                                               return column.idChartColumn === current.id;
                                           });
                                           if (column){
                                               row[column.standardName] = current.value;
                                           }
                                        });
                                        scope.tableElements.push(row);
                                    }
                                    _.each(scope.gridColumns, function (column, columnIndex, array) {
                                        $log.info(column);
                                        if (column.reference.rtype == 'YESNO') {
                                            _.each(scope.tableElements, function (elem) {
                                                elem[column.standardName] = row[columnIndex] ? $rootScope.getMessage('AD_labelYes') : $rootScope.getMessage('AD_labelNo');
                                            });
                                        } else if (column.reference.rtype == 'LIST') {
                                            _.each(scope.tableElements, function (elem) {
                                                elem[column.standardName] = cacheService.getListValue(column.idReferenceValue, elem[column.standardName]);
                                            });
                                        } else if (column.reference.rtype == 'TABLE' || column.reference.rtype == 'TABLEDIR' || column.reference.rtype == 'SEARCH') {
                                            _.each(scope.tableElements, function (elem) {
                                                elem[column.standardName] = cacheService.getTableValue(column, elem);
                                            });
                                        } else if (column.reference.rtype == 'IMAGE') {
                                            _.each(scope.tableElements, function (elem) {
                                                elem[column.standardName + '_imgSrc'] = scope.stdPref.serverCacheUrl + elem['updated'] + '/files/' + elem['table'].name + '/' + elem['idRow'] + '/' + elem[columnIndex];
                                            });
                                        }
                                    });
                                });
                            };

                            cacheService.updateLinkedCharts(scope.chartItem.idChart).then(function(linkedCharts){
                                scope.chartItem.linkedCharts = linkedCharts;
                                scope.updateChart();
                            }).catch(function(err){
                                $log.error(err);
                            });
                        }
                    }
                }
            }
        }
    ]);
});
