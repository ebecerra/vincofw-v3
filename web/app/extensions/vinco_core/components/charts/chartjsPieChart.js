define(['app', 'lodash', 'chartjs'], function (app,_) {

    'use strict';

    return app.registerDirective('chartjsPieChart', ['adChartService', 'cacheService', '$log', 'chartRendererService',
        function (adChartService, cacheService, $log, chartRendererService) {
            return {
                restrict: 'A',
                scope: {
                    chartItem: '=',
                    filterInfo: '=',
                    filterRangeSet: '='
                },
                link: function (scope, element, attributes) {

                    adChartService.evaluate({
                        idClient: cacheService.loggedIdClient(),
                        id: scope.chartItem.idChart,
                        idUserLogged: Authentication.getLoginData().idUserLogged,
                        q: chartRendererService.getQueryParam(scope.filterInfo, scope.filterRangeSet)
                    }, function (data) {

                        var pieOptions = {
                            //Boolean - Whether we should show a stroke on each segment
                            segmentShowStroke: true,
                            //String - The colour of each segment stroke
                            segmentStrokeColor: "#fff",
                            //Number - The width of each segment stroke
                            segmentStrokeWidth: 2,
                            //Number - Amount of animation steps
                            animationSteps: 100,
                            //String - types of animation
                            animationEasing: "easeOutBounce",
                            //Boolean - Whether we animate the rotation of the Doughnut
                            animateRotate: true,
                            //Boolean - Whether we animate scaling the Doughnut from the centre
                            animateScale: false,
                            //Boolean - Re-draw chart on page resize
                            responsive: true,
                            //String - A legend template
                            legendTemplate:
                                '<ul class="<%= name.toLowerCase() %>-legend">' +
                                '   <% for (var i = 0; i < segments.length; i++) { ' +
                                '   <li>' +
                                '       <span style="background-color:<%= segments[i].fillColor %>"></span>' +
                                '       <% if (segments[i].label) { %><%= segments[i].label %><% } ' +
                                '   </li>' +
                                '   <% } %>' +
                                '</ul>'
                        };

                        var pieData = [];
                        for (var i = 1; i <= data.items; i++) {
                            _.each(data.values, function (current, index) {
                                pieData.push({
                                    value: current['value' + i],
                                    color: chartRendererService.getColor(index).color,
                                    highlight: chartRendererService.getColor(index).hover,
                                    label: current.fullX
                                });
                            });

                            // render chart
                            var ctx = element[0].getContext("2d");
                            var myNewChart = new Chart(ctx).Pie(pieData, pieOptions);
                        }
                    }, function (err) {
                        $log.error(err);
                    });
                }
            }
        }]
    );
});
