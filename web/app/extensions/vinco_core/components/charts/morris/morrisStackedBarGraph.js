define(['app', 'morris'], function (app) {

    "use strict";


    return app.registerDirective('morrisStackedBarGraph', ['$rootScope', '$log', 'cacheService', 'chartRendererService',
        function ($rootScope, $log, cacheService, chartRendererService) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    chartItem: '=',
                    filterInfo: '=',
                    filterSet: '=',
                    filterRangeSet: '='
                },
                template: '<div class="chart no-padding"></div>',
                link: function (scope, element) {

                    /**
                     * Unregister watch function
                     * @type {Function}
                     */
                    scope.unwatch = undefined;

                    /**
                     * Previous filter in string format. Used to verify if the filter has changed from the previous value in order to refresh the chart
                     * @type {string}
                     */
                    scope.oldFilter = undefined;

                    scope.$on(app.eventDefinitions.GLOBAL_TAB_SELECTED, function (event, data) {
                        chartRendererService.doGlobalTabSelected(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_RESET_SEARCH, function (event, data) {
                        chartRendererService.doChartResetSearch(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_ITEM_CLICKED, function (event, data) {
                        chartRendererService.doChartItemClicked(scope, data);
                    });

                    scope.$on('$destroy', function() {
                        chartRendererService.doDestroy(scope);
                    });

                    scope.updateChart = function (extraArgs) {
                        $(element).find('.mbox').remove();
                        $(element).find('.morris-hover').remove();

                        chartRendererService.evaluateChart(scope, extraArgs, function (data) {
                            if (scope.unwatch)
                                scope.unwatch();
                            scope.unwatch = scope.$watchCollection("[filterInfo.filtersRange, filterInfo.filters]", function (newData, old, scope) {
                                chartRendererService.doUpdate(newData, old, scope);
                            });

                            scope.chartItem.titleX = data.titleX;
                            scope.dataSet = [];
                            scope.dataItems = data.items;
                            var i;
                            for (i = 1; i <= data.items && i <= 1; i++) {
                                _.each(data.values, function (current, index) {
                                    scope.dataSet.push({
                                        key: current.x,
                                        id: current.id,
                                        fullX: current.fullX
                                    });
                                });
                            }
                            scope.labels = [];
                            for (i = 1; i <= data.items; i++) {
                                _.each(data.values, function (current, index) {
                                    scope.dataSet[index][data["legend" + (i)]] = parseFloat(current['value' + i]).toFixed(2);
                                });
                                scope.labels.push(data["legend" + (i)]);
                            }

                            var morrisArea;
                            morrisArea = Morris.Bar({
                                element: element,
                                data: scope.dataSet,
                                hoverCallback: function (index, options, content) {
                                    var data = options.data[index];
                                    var html = '<div>';
                                    html += '<p><b>' + data.key + (data.fullX ? ' - ' + data.fullX : '') + '</b></p>';
                                    _.each(scope.labels, function (current) {
                                        html += (current + ': ' + parseFloat(data[current]).toFixed(2) + '<br/>');
                                    });
                                    html += '</div>';
                                    return html;
                                },
                                barColors: chartRendererService.getColors(data),
                                xkey: 'key',
                                ykeys: scope.labels,
                                labels: scope.labels,
                                hideHover: 'auto',
                                parseTime: false,
                                stacked : true
                            });

                            $(element).find("rect").unbind("click").bind('click', function () {
                                var index = 0, that = this;
                                // Checking X index
                                while (that.previousSibling.nodeName == 'rect'){
                                    that = that.previousSibling;
                                    index++;
                                }

                                index = Math.floor(index / scope.dataItems);

                                $rootScope.$broadcast(app.eventDefinitions.CHART_ITEM_CLICKED, {
                                    idChart: scope.chartItem.idChart,
                                    value: scope.dataSet[index].id,
                                    keyField: scope.chartItem.keyField
                                });
                            });

                            var currentHeight = 20;
                            var previousHeight = 0;
                            if (data.items > 1) {
                                _.each(scope.labels, function (current, index, array) {
                                    var parsed = parseFloat(previousHeight);
                                    currentHeight += parsed;
                                    var top = 20 * (index + 1);
                                    var legendlabel = $('<span style="display: block;">' + current + '</span>');
                                    legendlabel.css('margin-left', '10px')
                                        .css('margin-top', '-5px')
                                        .css('width', '80px')
                                        .css('background-color', 'white');
                                    legendlabel.css('margin-left', '10px')
                                        .css('margin-top', '-5px')
                                        .css('width', '80px')
                                        .css('background-color', 'white');
                                    var legendItem = $('<div class="mbox"></div>')
                                        .css('right', "25px")
                                        .css('position', "absolute")
                                        .css('top', currentHeight + "px")
                                        .css('border', "2px solid " + morrisArea.options.barColors[index])
                                        .css('border-radius', "50%")
                                        .css('background-color', "white")
                                        .css('font-size', "smaller")
                                        .css('color', "#545454")
                                        .css('-webkit-border-radius', "50%")
                                        .css('-moz-border-radius', "50%")
                                        .append(legendlabel);
                                    legendItem.append(legendlabel);
                                    $(element).append(legendItem);
                                    previousHeight = legendlabel.css('height');
                                });
                            }
                        });
                    };

                    cacheService.updateLinkedCharts(scope.chartItem.idChart).then(function(linkedCharts){
                        scope.chartItem.linkedCharts = linkedCharts;
                        scope.updateChart();
                    }).catch(function(err){
                        $log.error(err);
                    });
                }
            }
        }
    ]);
});
