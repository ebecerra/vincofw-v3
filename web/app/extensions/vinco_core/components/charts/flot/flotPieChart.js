define(['app', './FlotConfig',
    'flot',
    'flot-resize',
    'flot-fillbetween',
    'flot-orderBar',
    'flot-pie',
    'flot-time',
    'flot-tooltip'
], function (app, config) {
    "use strict";

    return app.registerDirective('flotPieChart', ['$rootScope', '$log', 'cacheService', 'chartRendererService',
        function ($rootScope, $log, cacheService, chartRendererService) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    chartItem: '=',
                    filterInfo: '=',
                    parentItem: '=',
                    filterSet: '=',
                    filterRangeSet: '='
                },
                template: '<div class="chart"></div>',
                link: function (scope, element) {

                    /**
                     * Unregister watch function
                     * @type {Function}
                     */
                    scope.unwatch = undefined;

                    /**
                     * Previous filter in string format. Used to verify if the filter has changed from the previous value in order to refresh the chart
                     * @type {string}
                     */
                    scope.oldFilter = undefined;

                    scope.$on(app.eventDefinitions.GLOBAL_TAB_SELECTED, function (event, data) {
                        chartRendererService.doGlobalTabSelected(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_RESET_SEARCH, function (event, data) {
                        chartRendererService.doChartResetSearch(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_ITEM_CLICKED, function (event, data) {
                        chartRendererService.doChartItemClicked(scope, data);
                    });

                    scope.$on('$destroy', function() {
                        chartRendererService.doDestroy(scope);
                    });

                    scope.updateChart = function (extraArgs) {
                        chartRendererService.evaluateChart(scope, extraArgs, function (data) {
                            if (scope.unwatch)
                                scope.unwatch();
                            scope.unwatch = scope.$watchCollection("[filterInfo.filtersRange, filterInfo.filters]", function (newData, old, scope) {
                                chartRendererService.doUpdate(newData, old, scope);
                            });

                            var dataSets = [];
                            scope.ids = [];
                            scope.chartItem.runtime.values = [];
                            for (var i = 1; i <= data.items; i++) {
                                _.each(data.values, function (current, index) {
                                    scope.chartItem.runtime.values.push({ id: current.id, name: current.x });
                                    dataSets.push({ label: current.x, data: current['value' + i], color: data['color' + (index + 1)], fullLabel: current.fullX });
                                    scope.ids.push(current.id);
                                });
                            }
                            $.plot(element, dataSets, {
                                series: {
                                    pie: {
                                        show: true,
                                        radius: 1,
                                        innerRadius: 0.4,
                                        label: {
                                            show: true,
                                            radius: 2 / 3,
                                            formatter: function (label, series) {
                                                return '<div style="font-size:11px;text-align:center;padding:4px;color:white;">' + Math.round(series.percent) + '%</div>';
                                            },
                                            threshold: 0.1
                                        }
                                    }
                                },
                                legend: config.legend,
                                grid: {
                                    hoverable: true,
                                    tickColor: config.chartBorderColor,
                                    clickable: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: function (label, xval, yval, flotItem) {
                                        return '<div class="flot-bar-chart-tooltips"><b>' + dataSets[flotItem.seriesIndex].fullLabel + '<br>' +
                                            '<span style="color: red">' + yval.toFixed(2) + '</span> - ' +
                                            '<span style="color: blue">' + flotItem.datapoint[0].toFixed(0) + '%</span></b></div>';
                                    },
                                    defaultTheme: false
                                }
                            });

                            $(element).unbind("plotclick").bind("plotclick", function (event, pos, item) {
                                if (item && ((item.seriesIndex !== undefined && item.seriesIndex < scope.chartItem.rowLimit)
                                    || (item.seriesIndex === undefined || item.seriesIndex === null))) {
                                    $rootScope.$broadcast(app.eventDefinitions.CHART_ITEM_CLICKED, {
                                        idChart: scope.chartItem.idChart,
                                        value: scope.ids[item.seriesIndex],
                                        keyField: scope.chartItem.keyField
                                    });
                                }
                            });
                        });
                    };

                    cacheService.updateLinkedCharts(scope.chartItem.idChart).then(function (linkedCharts) {
                        scope.chartItem.linkedCharts = linkedCharts;
                        scope.updateChart();
                    }).catch(function (err) {
                        $log.error(err);
                    });
                }
            }
        }
    ]);
});
