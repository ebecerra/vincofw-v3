define(['app', './FlotConfig',
    'flot',
    'flot-resize',
    'flot-fillbetween',
    'flot-orderBar',
    'flot-pie',
    'flot-time',
    'flot-tooltip'
], function (app, config) {
    "use strict";

    return app.registerDirective('flotHorizontalBarChart', ['$rootScope', '$log', 'cacheService', 'chartRendererService',
        function ($rootScope, $log, cacheService, chartRendererService) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    chartItem: '=',
                    filterInfo: '=',
                    parentItem: '=',
                    filterSet: '=',
                    filterRangeSet: '='
                },
                template: '<div class="chart"></div>',
                link: function (scope, element) {
                    /**
                     * Unregister watch function
                     * @type {Function}
                     */
                    scope.unwatch = undefined;

                    /**
                     * Previous filter in string format. Used to verify if the filter has changed from the previous value in order to refresh the chart
                     * @type {string}
                     */
                    scope.oldFilter = undefined;

                    scope.$on(app.eventDefinitions.GLOBAL_TAB_SELECTED, function (event, data) {
                        chartRendererService.doGlobalTabSelected(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_RESET_SEARCH, function (event, data) {
                        chartRendererService.doChartResetSearch(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_ITEM_CLICKED, function (event, data) {
                        chartRendererService.doChartItemClicked(scope, data);
                    });

                    scope.$on('$destroy', function() {
                        chartRendererService.doDestroy(scope);
                    });

                    scope.updateChart = function (extraArgs) {
                        chartRendererService.evaluateChart(scope, extraArgs, function (data) {
                            if (scope.unwatch)
                                scope.unwatch();

                            scope.unwatch = scope.$watchCollection("[filterInfo.filtersRange, filterInfo.filters]", function (newData, old, scope) {
                                chartRendererService.doUpdate(newData, old, scope);
                            });

                            scope.chartItem.titleX = data.titleX;
                            var dataSets = [];
                            scope.ids = [];
                            var max = 0;
                            for (var i = 1; i <= data.items; i++) {
                                var dataSet = [];
                                scope.chartItem.runtime.values = [];
                                _.each(data.values, function (current, index) {
                                    scope.chartItem.runtime.values.push({ id: current.id, name: current.x });
                                    if (max < current['value' + i]) {
                                        max = current['value' + i];
                                    }
                                    scope.ids.push(current.id);
                                    dataSet.push([current['value' + i], index, current.fullX]);
                                });
                                var barWidth = (data.values.length >= 3) ? 0.5 : 0.02;
                                dataSets.push({
                                    data: dataSet,
                                    label: data["legend" + i],
                                    bars: {
                                        horizontal: true,
                                        show: true,
                                        barWidth: barWidth,
                                        order: i + 1
                                    }
                                });
                            }

                            var captions = chartRendererService.getChartCaptions(data, config.legend);
                            $.plot(element, dataSets, {
                                colors: chartRendererService.getColors(data),
                                grid: {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: config.chartBorderColor,
                                    borderWidth: 0,
                                    borderColor: config.chartBorderColor
                                },
                                xaxes: [{
                                    axisLabel: captions.xTitle,
                                    min: 0,
                                    max: max
                                }],
                                yaxis: {
                                    ticks: data.values.length,
                                    tickFormatter: function (v) {
                                        return data.values[v] ? data.values[v].x : '';
                                    },
                                    axisLabel: captions.yTitle
                                },
                                legend: captions.legend,
                                tooltip: true,
                                tooltipOpts: {
                                    content: function (label, xval, yval, flotItem) {
                                        return '<div class="flot-bar-chart-tooltips"><b>' +
                                            '<span style="color: black">' + dataSet[flotItem.dataIndex][2] + '</span><br>' +
                                            label + ': <span style="color: blue">' + xval.toFixed(2) + '</span></b></div>';
                                    },
                                    defaultTheme: false
                                }
                            });

                            $(element).unbind("plotclick").bind("plotclick", function (event, pos, item) {
                                if (item) {
                                    $rootScope.$broadcast(app.eventDefinitions.CHART_ITEM_CLICKED, {
                                        idChart: scope.chartItem.idChart,
                                        value: scope.ids[item.dataIndex],
                                        keyField: scope.chartItem.keyField
                                    });
                                }
                            });
                        });
                    };

                    cacheService.updateLinkedCharts(scope.chartItem.idChart).then(function(linkedCharts){
                        scope.chartItem.linkedCharts = linkedCharts;
                        scope.updateChart();
                    }).catch(function(err){
                        $log.error(err);
                    });
                }
            }
        }
    ]);
});
