define(['app', './FlotConfig',
    'flot',
    'flot-resize',
    'flot-fillbetween',
    'flot-orderBar',
    'flot-pie',
    'flot-time',
    'flot-tooltip'
], function (app, config) {
    "use strict";

    return app.registerDirective('flotSinChart', ['$rootScope', '$log', 'cacheService', 'chartRendererService',
        function ($rootScope, $log, cacheService, chartRendererService) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    chartItem: '=',
                    filterInfo: '=',
                    parentItem: '=',
                    filterSet: '=',
                    filterRangeSet: '='
                },
                template: '<div class="chart"></div>',
                link: function (scope, element) {

                    /**
                     * Unregister watch function
                     * @type {Function}
                     */
                    scope.unwatch = undefined;

                    /**
                     * Previous filter in string format. Used to verify if the filter has changed from the previous value in order to refresh the chart
                     * @type {string}
                     */
                    scope.oldFilter = undefined;

                    scope.$on(app.eventDefinitions.GLOBAL_TAB_SELECTED, function (event, data) {
                        chartRendererService.doGlobalTabSelected(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_RESET_SEARCH, function (event, data) {
                        chartRendererService.doChartResetSearch(scope, data);
                    });

                    scope.$on(app.eventDefinitions.CHART_ITEM_CLICKED, function (event, data) {
                        chartRendererService.doChartItemClicked(scope, data);
                    });

                    scope.$on('$destroy', function() {
                        chartRendererService.doDestroy(scope);
                    });

                    scope.updateChart = function (extraArgs) {
                        chartRendererService.evaluateChart(scope, extraArgs, function (data) {
                            if (scope.unwatch)
                                scope.unwatch();
                            scope.unwatch = scope.$watchCollection("[filterInfo.filtersRange, filterInfo.filters]", function (newData, old, scope) {
                                chartRendererService.doUpdate(newData, old, scope);
                            });

                            scope.chartItem.titleX = data.titleX;
                            scope.dateMode = data.dateMode;
                            scope.year = data.year;
                            var dataSets = [], ticks = [], step = data.values.length > 10 ? 2 : 1;
                            scope.ids = [];
                            for (var i = 1; i <= data.items; i++) {
                                var dataSet = [];
                                _.each(data.values, function (current, index) {
                                    dataSet.push([index, current['value' + i]]);
                                    ticks[index] = [index, index % step == 0 ? current.x : '', current.fullX];
                                    scope.ids.push(current.id);
                                });
                                dataSets.push({
                                    data: dataSet,
                                    label: data["legend" + i]
                                });
                            }

                            // render chart
                            var captions = chartRendererService.getChartCaptions(data, config.legend);
                            $.plot(element, dataSets, {
                                series: {
                                    lines: { show: true },
                                    points: { show: true }
                                },
                                colors: chartRendererService.getColors(data),
                                grid: {
                                    show: true,
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: config.chartBorderColor,
                                    borderWidth: 0,
                                    borderColor: config.chartBorderColor
                                },
                                xaxes: [{
                                    axisLabel: captions.xTitle,
                                    ticks: ticks
                                }],
                                yaxis: { axisLabel: captions.yTitle },
                                threshold: {
                                    below: 0.0,
                                    color: "rgb(200, 20, 30)"
                                },
                                legend: captions.legend,
                                tooltip: true,
                                tooltipOpts: {
                                    content: function (label, xval, yval, flotItem) {
                                        return '<div class="flot-bar-chart-tooltips" style="z-index: 9999999"><b>' +
                                            flotItem.series.xaxis.options.axisLabel + ': <span style="color: blue">' + ticks[flotItem.dataIndex][2] + '</span><br>' +
                                            label + ': <span style="color: red">' + yval.toFixed(2) + '</span></b></div>';
                                    },
                                    defaultTheme: false
                                }
                            });

                            $(element).unbind("plotclick").bind("plotclick", function (event, pos, item) {
                                if (item) {
                                    $rootScope.$broadcast(app.eventDefinitions.CHART_ITEM_CLICKED, {
                                        idChart: scope.chartItem.idChart,
                                        value: scope.ids[item.dataIndex],
                                        keyField: scope.chartItem.keyField
                                    });
                                }
                            });
                        });
                    };

                    cacheService.updateLinkedCharts(scope.chartItem.idChart).then(function(linkedCharts){
                        scope.chartItem.linkedCharts = linkedCharts;
                        scope.updateChart();
                    }).catch(function(err){
                        $log.error(err);
                    });
                }
            }
        }
    ]);
});
