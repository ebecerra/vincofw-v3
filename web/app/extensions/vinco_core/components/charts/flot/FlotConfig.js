define(['app'], function () {
    "use strict";

    return {
        "chartBorderColor": "#efefef",
        "chartGridColor": "#DDD",
        "charMain": "#E24913",
        "chartSecond": "#6595b4",
        "chartThird": "#FF9F01",
        "chartFourth": "#7e9d3a",
        "chartFifth": "#BD362F",
        "chartMono": "#000",
        "legend": {
            show: true,
            noColumns: 1, // number of colums in legend table
            labelFormatter: null, // fn: string -> string
            labelBoxBorderColor: "#000", // border color for the little label boxes
            container: null, // container (as jQuery object) to put legend in, null means default on top of graph
            position: "nw", // position of default legend container within plot
            margin: [0, 0], // distance from grid edge to default legend container within plot
            backgroundColor: null, // null means auto-detect
            backgroundOpacity: 1 // set to 0 to avoid background
        }
    };

});