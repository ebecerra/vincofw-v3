define(['app', 'fullcalendar'], function (app) {

    'use strict';

    return app.registerDirective('fullCalendar', ['$timeout', 'cacheService', '$log',
        function ($timeout, cacheService, $log) {
            return {
                restrict: 'E',
                templateUrl: APP_EXTENSION_VINCO_CORE + 'components/calendar/full-calendar.tpl.html?v=' + FW_VERSION,
                scope: {
                    events: "=",
                    title: '=?',
                    selectedDate: '=?',
                    calendarView: '=?',
                    onMonthChanged: "&?",
                    onEventClicked: "&?",
                    onEventMoved: "&?",
                    onEventResized: "&?",
                    onDateClicked: "&?"
                },
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.hideHeader = false;
                            if (attributes.hideHeader)
                                scope.hideHeader = true;
                            var $calendar = $("#calendar");

                            scope.$watch('calendarView', function (newValue, oldValue) {
                                if (scope.calendarView)
                                    scope.changeView(scope.calendarView);
                            });

                            scope.$watch('selectedDate', function (newValue, oldValue) {
                                if (scope.selectedDate)
                                    $calendar.fullCalendar('gotoDate', scope.selectedDate);
                            });

                            var calendar = null;


                            function initCalendar() {

                                // $log.log(events);


                                calendar = $calendar.fullCalendar({
                                    editable: true,
                                    draggable: true,
                                    selectable: false,
                                    selectHelper: true,
                                    unselectAuto: false,
                                    // disableResizing: false,
                                    droppable: true,

                                    header: {
                                        left: 'title', //,today
                                        center: 'prev, next, today',
                                        right: 'month, agendaWeek, agendaDay' //month, agendaDay,
                                    },

                                    drop: function (date, allDay) { // this function is called when something is dropped

                                        // retrieve the dropped element's stored Event Object
                                        var originalEventObject = $(this).data('eventObject');

                                        // we need to copy it, so that multiple events don't have a reference to the same object
                                        var copiedEventObject = $.extend({}, originalEventObject);

                                        // assign it the date that was reported
                                        copiedEventObject.start = date;
                                        copiedEventObject.allDay = allDay;

                                        // $log.log(scope);

                                        // render the event on the calendar
                                        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                                        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                                        // is the "remove after drop" checkbox checked?
                                        if ($('#drop-remove').is(':checked')) {

                                            // if so, remove the element from the "Draggable Events" list
                                            // $(this).remove();
                                            // $log.log($(this).scope());
                                            var index = $(this).scope().$index;

                                            $("#external-events").scope().eventsExternal.splice(index, 1);
                                            $(this).remove();

                                        }

                                    },
                                    select: function (start, end, allDay) {
                                        var title = prompt('Event Title:');
                                        if (title) {
                                            calendar.fullCalendar('renderEvent', {
                                                    title: title,
                                                    start: start,
                                                    end: end,
                                                    allDay: allDay
                                                }, true // make the event "stick"
                                            );
                                        }
                                        calendar.fullCalendar('unselect');
                                    },

                                    // events: scope.events,

                                    events: function (start, end, timezone, callback) {
                                        callback(scope.events);
                                    },
                                    eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                                        if (scope.onEventMoved)
                                            scope.onEventMoved({
                                                event: event,
                                                days: delta.days()
                                            });
                                    },
                                    eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                                        if (scope.onEventResized)
                                            scope.onEventResized({
                                                event: event,
                                                days: delta.days()
                                            });
                                    },
                                    dayClick: function (date, jsEvent, view) {
                                        // handle the event
                                        // see https://fullcalendar.io/docs/mouse/eventClick/
                                        if (scope.onDateClicked)
                                            scope.onDateClicked({
                                                // We will make this conversion to ensure the date will be correctly selected, otherwise, we might obtain a day before
                                                date: moment(moment(date).format("YYYY-MM-DD"), "YYYY-MM-DD")
                                            });
                                    },
                                    eventClick: function (calEvent, jsEvent, view) {
                                        // handle the event
                                        // see https://fullcalendar.io/docs/mouse/eventClick/
                                        if (scope.onEventClicked)
                                            scope.onEventClicked({
                                                event: calEvent
                                            });
                                    },
                                    eventRender: function (event, element, icon) {
                                        if (event.color) {
                                            element.find('.fc-event').css("background-color", event.color);
                                        }
                                        if (!event.description == "") {
                                            element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description + "</span>");
                                        }
                                        if (!event.icon == "") {
                                            element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon + " '></i>");
                                        }
                                    },
                                    eventMouseover: function (data, event, view) {

                                        if (data.tooltip) {
                                            var tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#FFFAAE;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + data.tooltip + '</div>';

                                            $("body").append(tooltip);
                                            $(this).mouseover(function (e) {
                                                $(this).css('z-index', 10000);
                                                $('.tooltiptopicevent').fadeIn('500');
                                                $('.tooltiptopicevent').fadeTo('10', 1.9);
                                            }).mousemove(function (e) {
                                                $('.tooltiptopicevent').css('top', e.pageY + 10);
                                                $('.tooltiptopicevent').css('left', e.pageX + 20);
                                            });
                                        }
                                    },
                                    eventMouseout: function (data, event, view) {
                                        $(this).css('z-index', 8);
                                        $('.tooltiptopicevent').remove();

                                    }
                                });

                                $('.fc-header-right, .fc-header-center', $calendar).hide();
                            }


                            initCalendar();

                            // Now events will be refetched every time events scope is updated in controller!!!
                            scope.$watch("events", function (newValue, oldValue) {
                                $calendar.fullCalendar('refetchEvents');
                            }, true);


                            scope.$watchCollection("[calendarProperties.view]", function (newValue, oldValue) {
                                if (scope.calendarProperties && scope.calendarProperties.view)
                                    scope.changeView(scope.calendarProperties.view);
                            });

                            scope.next = function () {
                                $('.fc-button-next', $calendar).click();
                                var moment = $('#calendar').fullCalendar('getDate');
                                if (scope.onMonthChanged) {
                                    scope.onMonthChanged({
                                        date: moment
                                    });
                                }
                            };
                            scope.prev = function () {
                                $('.fc-button-prev', $calendar).click();
                                var moment = $('#calendar').fullCalendar('getDate');
                                if (scope.onMonthChanged) {
                                    scope.onMonthChanged({
                                        date: moment
                                    });
                                }
                            };
                            scope.today = function () {
                                $('.fc-button-today', $calendar).click();
                            };
                            scope.changeView = function (period) {
                                $calendar.fullCalendar('changeView', period);
                            };
                        }
                    }
                }
            }
        }]);
});
