define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adUserService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_user/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idUser: '@idUser'
            }, {
                query: { method: "GET", isArray: false },
                update: {
                    method: 'PUT',
                    params: {
                        idClient: '@idClient',
                        idUser: '@idUser'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/:idUser'
                },
                updateProfile: {
                    method: 'PUT',
                    params: {
                        idClient: '@idClient',
                        idUser: '@idUser'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/:idUser/profile',
                    headers: {'Content-Type': undefined},
                    transformRequest: function (data) {
                        var formData = new FormData();
                        var keys = _.keys(data);
                        var containsFiles = false;
                        var newData = {};
                        _.each(keys, function (key) {
                            if (!(data[key] instanceof (FileList))) {
                                newData[key] = data[key];
                            } else {
                                newData[key] = data[key][0].name;
                            }
                        });
                        var entityData = new Blob([JSON.stringify(newData)], {
                            type: "application/json"
                        });
                        formData.append("entity", entityData);
                        _.each(keys, function (key) {
                            if ((data[key] instanceof (FileList))) {
                                containsFiles = true;
                                formData.append("file_" + key, data[key][0]);
                            }
                        });
                        return formData;
                    }
                },
                deleteImage: {
                    method: 'DELETE',
                    params: {
                        idClient: '@idClient',
                        id: '@id',
                        field: '@field',
                        fileName: '@fileName'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/:idUser/delete_image/:id/:field/:fileName'
                },
                changeRole: {
                    method: 'POST',
                    params: {
                        idClient: '@idClient',
                        idUser: '@idUser',
                        idRole: '@idRole',
                        idLanguage: '@idLanguage'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/:idUser/change_role'
                },
                changePassword: {
                    method: 'POST',
                    params: {
                        idClient: '@idClient',
                        idUser: '@idUser',
                        oldPassword: '@oldPassword',
                        newPassword: '@newPassword',
                        idLanguage: '@idLanguage',
                        size: '@size',
                        security: '@security'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/:idUser/change_password'
                },
                checkSecurity: {
                    method: 'GET',
                    params: {
                        idClient: '@idClient',
                        idUser: '@idUser',
                        idLanguage: '@idLanguage',
                        size: '@size',
                        security: '@security'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/check_security'
                }
            }
        );
    });
});

