define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adTableService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_table/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT'
                    , params: {
                        idClient: '@idClient',
                        idTable: '@idTable'
                    }
                    , url: REST_HOST_CORE + 'ad_table/:idClient/:idTable'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_table/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                },
                listTables: {
                    method: 'GET'
                    , url: REST_HOST_CORE + 'ad_table/:idClient/listTables'
                }
            }
        );
    });
});

