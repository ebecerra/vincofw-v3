define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adColumnService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_column/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT'
                    , params: {idColumn: '@idColumn'}
                    , url: REST_HOST_CORE + 'ad_column/:idClient/:idColumn'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_column/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

