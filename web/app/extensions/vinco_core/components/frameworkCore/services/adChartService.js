define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adChartService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_chart/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: { method: "GET", isArray: false },
                evaluate: {
                    method: 'GET',
                    url: REST_HOST_CORE + 'ad_chart/:idClient/evaluate/:id'
                },
                evaluateList: {
                    method: 'GET',
                    url: REST_HOST_CORE + 'ad_chart/:idClient/evaluate_list/:id'
                }
            }
        );
    });
});

