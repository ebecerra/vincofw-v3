define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adPreferenceService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_preference/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: { method: "GET", isArray: false },
                permissions: {
                    method: 'GET',
                    params: { idClient: '@idClient' },
                    url: REST_HOST_CORE + 'ad_preference/:idClient/permissions'
                },
                preference: {
                    method: 'GET',
                    params: { idClient: '@idClient' },
                    url: REST_HOST_CORE + 'ad_preference/:idClient/preference'
                },
                preferences: {
                    method: 'GET',
                    params: { idClient: '@idClient' },
                    url: REST_HOST_CORE + 'ad_preference/:idClient/preferences'
                }
            }
        );
    });
});

