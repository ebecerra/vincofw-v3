define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adChartLinkedService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_chart_linked/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: { method: "GET", isArray: false }
            }
        );
    });
});

