define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adFieldService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_field/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: { method: "GET", isArray: false },
                captions: {
                    method: "GET",
                    isArray: false,
                    params: { idLanguage: '@idLanguage', idClient: '@idClient' },
                    url: REST_HOST_CORE + 'ad_field/:idClient/captions?idLanguage=:idLanguage'
                },
                multiselect: {
                    method: "GET",
                    params: { idClient: '@idClient' },
                    url: REST_HOST_CORE + 'ad_field/:idClient/:id/multiselect'
                },
                update: {
                    method: 'PUT',
                    params: {idField: '@idField'},
                    url: REST_HOST_CORE + 'ad_field/:idClient/:idField'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_field/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

