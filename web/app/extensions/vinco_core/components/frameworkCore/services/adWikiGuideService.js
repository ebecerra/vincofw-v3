define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adWikiGuideService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_wiki_guide/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                guides: {
                    method: 'GET', isArray: true,
                    url: REST_HOST_CORE + 'ad_wiki_guide/:idClient/guides'
                }
            }
        );
    });
});

