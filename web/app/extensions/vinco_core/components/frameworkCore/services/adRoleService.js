define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adRoleService', function ($resource) {
        return $resource(
            REST_HOST_CORE +'ad_role/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            },
            {
                query: {
                    method: "GET",
                    isArray: false
                }
            }
        );
    });
});

