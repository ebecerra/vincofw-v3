define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adTranslationService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_translation/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                fields: {
                    method: "GET",
                    isArray: false,
                    params: { idLanguage: '@idLanguage', idClient: '@idClient' },
                    url: REST_HOST_CORE + 'ad_translation/:idClient/fields?idLanguage=:idLanguage'
                },
                messages: {
                    method: "GET",
                    isArray: false,
                    params: { idLanguage: '@idLanguage', idClient: '@idClient' },
                    url: REST_HOST_CORE + 'ad_translation/:idClient/messages?idLanguage=:idLanguage'
                },
                query: {
                    method: "GET",
                    isArray: false
                },
                update: {
                    method: 'PUT',
                    params: { idTranslation: '@idTranslation' },
                    url: REST_HOST_CORE + 'ad_translation/:idClient/:idTranslation'
                },
                updateFile: {
                    method: 'PUT',
                    params: {idTranslation: '@idTranslation'},
                    url: REST_HOST_CORE + 'ad_translation/:idClient/:idTranslation/file_update',
                    headers: { 'Content-Type': undefined },
                    transformRequest: function (data) {
                        var formData = new FormData();
                        var keys = _.keys(data);
                        var containsFiles = false;
                        var newData = {};
                        _.each(keys, function (key) {
                            if (!(data[key] instanceof (FileList))) {
                                //if (!(data[key] == null || data[key] === undefined))
                                //    formData.append(key, data[key]);
                                newData[key] = data[key];
                            }
                            else
                                newData[key] = '';
                        });

                        var entityData = new Blob([JSON.stringify(newData)], {
                            type: "application/json"
                        });
                        formData.append("entity", entityData);
                        _.each(keys, function (key) {
                            if ((data[key] instanceof (FileList))) {
                                containsFiles = true;
                                formData.append("file_"+key, data[key][0]);
                            }
                        });
                        return formData;
                    }
                },
                deleteFile: {
                    method: 'DELETE',
                    params: { idTranslation: '@idTranslation' },
                    url: REST_HOST_CORE + 'ad_translation/:idClient/file/:idTranslation'
                },
                delete: {
                    method: 'DELETE',
                    params: { idTranslation: '@idTranslation' },
                    url: REST_HOST_CORE + 'ad_translation/:idClient/:idTranslation'
                },
                create: {
                    method: 'POST',
                    url: REST_HOST_CORE + 'ad_translation/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                },
                createFile: {
                    method: 'POST',
                    url: REST_HOST_CORE + 'ad_translation/:idClient/file_create',
                    headers: { 'Content-Type': undefined },
                    transformRequest: function (data) {
                        var formData = new FormData();
                        var keys = _.keys(data);
                        var containsFiles = false;
                        var newData = {};
                        _.each(keys, function (key) {
                            if (!(data[key] instanceof (FileList))) {
                                newData[key] = data[key];
                            }
                            else
                                newData[key] = '';
                        });

                        var entityData = new Blob([JSON.stringify(newData)], {
                            type: "application/json"
                        });
                        formData.append("entity", entityData);
                        _.each(keys, function (key) {
                            if ((data[key] instanceof (FileList))) {
                                containsFiles = true;
                                formData.append("file_"+key, data[key][0]);
                            }
                        });
                        return formData;
                    },
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

