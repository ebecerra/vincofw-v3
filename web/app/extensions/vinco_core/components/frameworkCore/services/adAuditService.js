define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adAuditService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_audit/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});

