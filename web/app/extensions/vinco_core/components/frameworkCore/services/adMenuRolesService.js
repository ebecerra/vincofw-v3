define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adMenuRolesService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_menu_roles/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                saveMenu: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_menu_roles/:idClient/save_menu'
                }
            }
        );
    });
});

