/**
 * Created by yokiro on 27/11/2015.
 */
define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adRefListService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_ref_list/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});