define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adWikiEntryService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_wiki_entry/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});

