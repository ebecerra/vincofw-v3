define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adCharacteristicValueService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_characteristic_value/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT',
                    params: {
                        idClient: '@idClient',
                        idCharacteristicValue: '@idCharacteristicValue'
                    },
                    url: REST_HOST_CORE + 'ad_characteristic_value/:idClient/:idCharacteristicValue'
                },
                updateFile: {
                    method: 'PUT',
                    params: {
                        idClient: '@idClient',
                        idCharacteristicValue: '@idCharacteristicValue'
                    },
                    url: REST_HOST_CORE + 'ad_characteristic_value/:idClient/:idCharacteristicValue/file_update',
                    headers: {'Content-Type': undefined},
                    transformRequest: function (data) {
                        var formData = new FormData();
                        var keys = _.keys(data);
                        var containsFiles = false;
                        var newData = {};
                        _.each(keys, function (key) {
                            if (!((data[key] instanceof (FileList)) || (data[key] instanceof (Object)))) {
                                newData[key] = data[key];
                            }
                        });
                        var entityData = new Blob([JSON.stringify(newData)], {
                            type: "application/json"
                        });
                        formData.append("entity", entityData);
                        _.each(keys, function (key) {
                            if ((data[key] instanceof (FileList) || (data[key] instanceof (Object)))) {
                                containsFiles = true;
                                for (var i = 0; i < data[key].length; i++) {
                                    formData.append("file_" + key, data[key][i]);
                                }
                            }
                        });
                        return formData;
                    }
                },
                create: {
                    method: 'POST',
                    idClient: '@idClient',
                    url: REST_HOST_CORE + 'ad_characteristic_value/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                },
                createFile: {
                    method: 'POST',
                    idClient: '@idClient',
                    url: REST_HOST_CORE + 'ad_characteristic_value/:idClient/create_file',
                    headers: {'Content-Type': undefined},
                    transformRequest: function (data) {
                        var formData = new FormData();
                        var keys = _.keys(data);
                        var containsFiles = false;
                        var newData = {};
                        _.each(keys, function (key) {
                            if (!(data[key] instanceof (FileList))) {
                                newData[key] = data[key];
                            }
                        });
                        var entityData = new Blob([JSON.stringify(newData)], {
                            type: "application/json"
                        });
                        formData.append("entity", entityData);
                        _.each(keys, function (key) {
                            if ((data[key] instanceof (FileList))) {
                                containsFiles = true;
                                for (var i = 0; i < data[key].length; i++) {
                                    formData.append("file_" + key, data[key][i]);
                                }
                            }
                        });
                        return formData;
                    },
                    transformResponse: function (data) {
                        return {id: data};
                    }
                },
                // Deletes an item
                delete: {
                    method: 'DELETE',
                    url: REST_HOST_CORE + 'ad_characteristic_value/:idClient/:id',
                    params: {
                        id: '@id',
                        idClient: '@idClient'
                    }
                }
            }
        );
    });
});

