define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adClientService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_client/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT'
                    , params: {idClient: '@idClient'}
                    , url: REST_HOST_CORE + 'ad_client/:idClient'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_client/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

