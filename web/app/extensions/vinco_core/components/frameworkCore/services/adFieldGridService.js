define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adFieldGridService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_field_grid/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                columns: {
                    method: 'GET',
                    url: REST_HOST_CORE + 'ad_field_grid/:idClient/columns'
                },
                save: {
                    method: 'POST',
                    params: { idTab: '@idTab', idUser: '@idUser', fields: '@fields' },
                    url: REST_HOST_CORE + 'ad_field_grid/:idClient/save'
                },
                update: {
                    method: 'PUT',
                    params: { idFieldGrid: '@idFieldGrid' },
                    url: REST_HOST_CORE + 'ad_field_grid/:idClient/:idFieldGrid'
                },
                create: {
                    method: 'POST',
                    url: REST_HOST_CORE + 'ad_field_grid/:idClient',
                    transformResponse: function (data) {
                        return { id: data };
                    }
                }
            }
        );
    });
});

