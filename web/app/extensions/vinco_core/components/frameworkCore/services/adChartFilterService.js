define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adChartFilterService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_chart_filter/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: { method: "GET", isArray: false }
            }
        );
    });
});

