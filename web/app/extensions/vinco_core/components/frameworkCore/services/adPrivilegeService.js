define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adPrivilegeService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_privilege/:idClient',
            {
                _: (new Date()).getTime()
            }, {
                query: {method: "GET", isArray: false},
                users: {
                    method: 'GET', isArray: true,
                    params: { idClient: '@idClient', privilege: '@privilege'  },
                    url: REST_HOST_CORE + 'ad_privilege/:idClient/:privilege/users'
                }
            }
        );
    });
});

