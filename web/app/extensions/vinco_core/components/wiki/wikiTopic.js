define(['app', 'lodash'], function (app,_) {

    'use strict';

    return app.registerDirective('wikiTopic', [ 'Authentication', 'cacheService', '$log',
        function (adWikiGuideService, Authentication, cacheService, $log) {
            var templateUrl = APP_EXTENSION_VINCO_CORE + 'components/wiki/wiki.topic.tpl.html?v=' + FW_VERSION;
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    topic: "=",
                    onTopicSelected: "&"
                },
                priority: 1,
                templateUrl: templateUrl,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.topicTemplate = templateUrl;
                            scope.selectTopic = function (topic) {
                                scope.onTopicSelected({
                                    topic: topic
                                });
                            };
                        }
                    }
                }
            }
        }]
    );
});
