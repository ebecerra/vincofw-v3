define(['app', 'lodash'], function (app,_) {

    'use strict';

    return app.registerDirective('wikiMainContainer', [ 'adWikiGuideService', 'Authentication', 'cacheService', '$log',
        function (adWikiGuideService, Authentication, cacheService, $log) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'components/wiki/wiki.main.container.tpl.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.id = 0;
                            scope.getId = function(){
                                scope.id ++ ;
                                return scope.id;
                            };

                            scope.setIds = function (topic, index) {
                                topic.index = index;
                                var totalElements = 1;
                                var startIndex = index + 1;
                                if (topic.topics) {
                                    for (var i = 0; i < topic.topics.length; i++) {
                                        var total = scope.setIds(topic.topics[i], startIndex);
                                        startIndex += total;
                                        totalElements +=total;
                                    }
                                }
                                return totalElements;
                            };

                            adWikiGuideService.guides({
                                idClient: cacheService.loggedIdClient(),
                                idLanguage: Authentication.getCurrentLanguage(),
                                idRole: Authentication.getCurrentRole()
                            }, function(data) {
                                scope.guides = data;
                                _.each(scope.guides, function(current){
                                    scope.setIds(current, 0);
                                });
                            }, function (error) {
                                $log.error(error);
                            });

                            scope.onGuideSelected = function (guide) {
                                scope.selectedGuide = guide;
                                scope.selectedTopic = null;
                            };

                            scope.onTopicSelected = function (topic) {
                                scope.selectedGuide = null;
                                scope.selectedTopic = topic;
                            };
                        }
                    }
                }
            }
        }]
    );
});
