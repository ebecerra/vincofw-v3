define(['app', 'lodash'], function (app,_) {

    'use strict';

    return app.registerDirective('wikiTopicDetail', [ 'Authentication', 'cacheService', '$log', 'adWikiEntryService',
        function (Authentication, cacheService, $log, adWikiEntryService) {
            var templateUrl = APP_EXTENSION_VINCO_CORE + 'components/wiki/wiki.topic.detail.tpl.html?v=' + FW_VERSION;
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    topic: "="
                },
                priority: 1,
                templateUrl: templateUrl,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.topicTemplate = templateUrl;
                            scope.$watch('topic', function(){
                                if (scope.topic) {
                                    scope.loadEntry();
                                } else {
                                    scope.topicEntries = null;
                                }
                            });

                            scope.loadEntry = function(){
                                adWikiEntryService.query({
                                    idClient: cacheService.loggedIdClient(),
                                    idLanguage: Authentication.getCurrentLanguage(),
                                    q: 'idWikiTopic=' + scope.topic.idWikiTopic
                                }, function (data) {
                                    scope.topicEntries = _.sortBy(data.content, function(current){
                                        return current.seqno;
                                    });
                                }, function(error) {
                                    $log.error(error);
                                });
                            };
                        }
                    }
                }
            }
        }]
    );
});
