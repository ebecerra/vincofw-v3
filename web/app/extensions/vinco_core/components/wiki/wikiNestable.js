define(['app', 'jquery-nestable'], function (app,_) {

    'use strict';

    return app.registerDirective('wikiNestable', [ 'adWikiGuideService', 'Authentication', 'cacheService', '$log', '$timeout',
        function (adWikiGuideService, Authentication, cacheService, $log, $timeout) {
            return {
                restrict: 'A',
                scope: {
                    group: '@',
                    output: '='
                },
                priority: 3,
                link: function (scope, element, attributes) {
                    $timeout(function(){
                        var options = {};
                        if(scope.group){
                            options.group = scope.group;
                        }
                        element.nestable(options);
                        if (attributes.output) {
                            element.on('change', function () {
                                scope.$apply(function () {
                                    scope.output = element.nestable('serialize');
                                });
                            });
                            scope.output = element.nestable('serialize');
                        }
                    });
                }
            }
        }]
    );
});
