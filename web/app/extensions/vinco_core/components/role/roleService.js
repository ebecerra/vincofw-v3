define(['app'], function(app){
    "use strict";

	console.log('Role Service - BEFORE');

	return app.factory('Role', function($http, $log){

		function getLanguage(key, callback) {

			$http.get('api/langs/' + key + '.json?_='+(new Date()).getTime()).success(function(data){

				callback(data);
				
			}).error(function(){

				$log.log('Error');
				callback([]);

			});

		}

		function getLanguages(callback) {

			$http.get('api/languages.json').success(function(data){

				callback(data);
				
			}).error(function(){

				$log.log('Error');
				callback([]);

			});

		}

		console.log('Language Service');

		return {
			getLang: function(type, callback) {
				getLanguage(type, callback);
			},
			getLanguages:function(callback){
				getLanguages(callback);
			}
		}

    });
});
