define(['app'], function (app) {
    "use strict";

    app.registerDirective('roleSelector', function ($rootScope, Authentication, cacheService, $log) {
        return {
            restrict: "EA",
            replace: true,
            templateUrl: APP_EXTENSION_VINCO_CORE + 'components/role/role-selector.tpl.html?v=' + FW_VERSION,
            // scope: true,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.currentRole = {};

                        scope.loadRoleData = function () {
                            scope.roles = Authentication.getLoginData().roles;
                            if (localStorage["selected_role"] !== undefined && localStorage["selected_role"] !== null)
                                $rootScope.defaultRole = localStorage["selected_role"];
                            else
                                $rootScope.defaultRole = Authentication.getLoginData().defaultIdRole;
                            scope.checkedRole = _.find(scope.roles, function (role) {
                                return role.idRole == $rootScope.defaultRole;
                            });
                            $rootScope.checkedRole = scope.checkedRole;
                            $log.log(scope.roles);
                        };

                        scope.selectRole = function (role) {
                            $rootScope.checkedRole = role;
                            scope.checkedRole = role;
                            localStorage["selected_role"] = role.idRole;
                        };

                        if (Authentication.isUserLogged() && cacheService.loggedIdClient()) {
                            if (scope.roles === undefined || scope.roles == [])
                                scope.loadRoleData();
                        }

                        scope.$on(app.eventDefinitions.GLOBAL_USER_LOGGED, function () {
                            scope.loadRoleData();
                        });
                    }
                }

            }
        };
    })
});
