define(['app'], function (module) {
    "use strict";

    module.registerDirective('menuActivator', function () {
        return {
            restrict: "EA",
            replace: true,
            templateUrl: APP_EXTENSION_VINCO_CORE + 'components/menuIcon/menuIcon.tpl.html?v=' + FW_VERSION,
            scope: true
        }
    })
});
