define(['app'], function (app) {
    "use strict";

    return app.controller("MenuIconController", MenuIconController);

    function MenuIconController($scope, $rootScope, $timeout, $log) {

        if ($rootScope.showMenu === undefined || $rootScope.showMenu === null)
            $rootScope.showMenu = false;
        $rootScope.checkMenuVisibility = function () {
            $rootScope.showMenu = !$rootScope.showMenu;

            var $body = $('body');
            var $window = $(window);
            if (!$rootScope.showMenu) {
                $body.toggleClass('no-menu', $window.width() < 979);
                if ($window.width() < 979)
                    $body.removeClass('menu-on-top');
            }
            else {
                $body.removeClass('menu-on-top');
                $body.removeClass('no-menu');
                $body.toggleClass('minified', $window.width() < 979);
            }
        };

        $(window).resize(function () {
            $scope.$apply(function () {
                if (window.innerWidth > 979) {
                    var $root = $('body');
                    $root.toggleClass('menu-on-top', true);
                    $root.removeClass('no-menu');
                    $root.removeClass('minified');
                    $rootScope.showMenu = false;
                }
                else {
                    if (!$rootScope.showMenu)
                    {
                        var $root = $('body');
                        $root.toggleClass('no-menu', true);
                        $root.removeClass('menu-on-top');
                    }
                }
            });
        });
    }

});