define(['app', 'moment'], function (app) {
    "use strict";

    return app.registerFactory('utilsService', function ($rootScope, $log, $q, cacheService, coreConfigService, autogenService, commonService) {
        var service = {};

        /**
         * Execute javascript code
         *
         * @param scope Scope
         * @param field Field information
         * @param execFrom Execute from (LIST, FORM)
         */
        service.executeJavaScriptCode = function (scope, field, execFrom) {
            var jsFunction = field.column.refButton.info.jsCode;
            if (jsFunction) {
                var table = cacheService.getTableByName(field.tableName);
                if (table && buttonJSCode) {
                    if (buttonJSCode[table.module.restPath] && typeof(buttonJSCode[table.module.restPath][jsFunction]) === 'function') {
                        buttonJSCode[table.module.restPath][jsFunction].call(this, cacheService, autogenService, $log, scope);
                    } else {
                        $rootScope.$broadcast(coreConfigService.events.BUTTON_EXECUTE_ACTION, {
                            jsFunction: jsFunction,
                            scope: scope,
                            execFrom: execFrom
                        });
                    }
                } else {
                    $log.error("Can not load table information: " + field.tableName);
                }
            } else {
                $log.warn("No defined JS Code");
            }
        };

        /**
         * Execute server process
         *
         * @param scope Scope
         * @param field Field information
         * @param item Item edited
         */
        service.executeServerProccess = function (scope, field, item) {
            var process = cacheService.getProcess(field.column.refButton.info.idProcess);
            scope.selectedProcess = process;
            if (process.uipattern == "DIALOG") {
                $rootScope.modalProcessItem = item;
                $rootScope.modalProcessTab = scope.adTab;
                scope.dialogProcess = process;
                $('#' + scope.processDialogId).modal();
            } else {
                $rootScope.tabbedProcessItem = item;
                $rootScope.tabbedProcess = process;
                $state.go('app.process.form');
            }
        };

        /**
         * Update Sql Where when param is changed
         *
         * @param changedField Changed field
         * @param field Field
         */
        service.updateParamSqlWhere = function (changedField, field) {
            if (changedField && field.reference.rtype == 'TABLEDIR') {
                if (!field.referenceValue) {
                    field.referenceValue = cacheService.getReference(field.idReferenceValue);
                }
                if (field.referenceValue && field.referenceValue.info.sqlwhere && cacheService.isFieldInConditional(changedField, field.referenceValue.info.sqlwhere)) {
                    field.runtime.sqlwhere = field.referenceValue.info.sqlwhere;
                    if (!field.runtime.sqlwhere || field.runtime.sqlwhere != field.referenceValue.info.sqlwhere) {
                        field.runtime.sqlwhere = field.referenceValue.info.sqlwhere;
                    }
                    if (changedField.value) {
                        field.runtime.sqlwhere = cacheService.replaceSQLConditional(changedField, changedField.value, field.runtime.sqlwhere);
                    }
                }
            }
        };

        /**
         * Apply display logic when a field change
         *
         * @param changedField Changed field
         * @param field Field
         */
        service.applyParamDisplayLogic = function (changedField, field) {
            if (field.displaylogic) {
                if (!field.runtime.displaylogic) {
                    field.runtime.displaylogic = field.displaylogic;
                }
                var value = null;
                if (changedField && changedField.ranged) {
                    var valueStart = scope.rangedInput[changedField.name].start;
                    var valueEnd = scope.rangedInput[changedField.name].end;
                    if (changedField.reference && changedField.reference.rtype === 'DATE') {
                        if (moment.isMoment(valueStart)) {
                            valueStart = valueStart.format('DD/MM/YYYY')
                        }
                        if (moment.isMoment(valueEnd)) {
                            valueEnd = valueEnd.format('DD/MM/YYYY')
                        }
                    }
                    field.runtime.displaylogic = cacheService.replaceJSConditional({column: {standardName: changedField.name + 'Start'}}, valueStart, field.runtime.displaylogic);
                    field.runtime.displaylogic = cacheService.replaceJSConditional({column: {standardName: changedField.name + 'End'}}, valueEnd, field.runtime.displaylogic);
                    if (cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                        field.displayed = eval(field.runtime.displaylogic);
                        field.runtime.displaylogic = field.displaylogic;
                    }
                } else {
                    if (changedField) {
                        value = changedField.value;
                    }
                    field.runtime.displaylogic = cacheService.replaceJSConditional({column: {standardName: changedField.name}}, value, field.runtime.displaylogic);
                    if (cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                        field.displayed = eval(field.runtime.displaylogic);
                        field.runtime.displaylogic = field.displaylogic;
                    }
                }
            }
        };

        /**
         * Gets the data needed for the input of the LIST search component
         *
         * @param param Field to obtain data from
         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
         */
        service.getListFieldAttr = function (param) {
            return {
                idReferenceValue: param.idReferenceValue,
                name: param.name,
                idColumn: param.idProcessParam,
                rtype: param.reference.rtype,
                readOnly: false,
                required: param.mandatory
            };
        };

        /**
         * Get max password security level supported
         *
         * @param password Password to check
         * @returns {string}
         */
        service.getPasswordSecurityLevel = function (password) {
            var hasUpperCase = password.match(new RegExp("[A-Z]"));
            var hasLowerCase = password.match(new RegExp("[a-z]"));
            var hasNumber = password.match(new RegExp("[0-9]"));
            var hasSpecialChar = password.match(new RegExp(/^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{4,}$/g));
            var pwSatisfy = 'BASIC';
            if (hasLowerCase && hasUpperCase)
                pwSatisfy = 'STANDARD';
            if (hasNumber && hasLowerCase && hasUpperCase)
                pwSatisfy = 'HIGH';
            if (hasSpecialChar && hasNumber && hasLowerCase && hasUpperCase)
                pwSatisfy = 'SECURE';
            return pwSatisfy;
        };

        /** Load table content
         *
         * @param tableName Table name
         * @param params Query parameters
         * @returns {*}
         */
        service.loadTableContent = function (tableName, params) {
            var deferred = $q.defer();

            cacheService.loadTable(tableName).then(function (table) {
                var service = autogenService.getAutogenService(table);
                service.query(
                    params,
                    function (data) {
                        var content = data && data.content ? data.content : [];
                        $log.info('Load table: ' + tableName + ' (' + content.length + ' rows)');
                        deferred.resolve(content);
                    },
                    function (err) {
                        $log.error(err);
                        deferred.reject(err);
                    }
                );
            });

            return deferred.promise;
        };

        /** Load table row
         *
         * @param tableName Table name
         * @param params Query parameters
         * @returns {*}
         */
        service.loadTableRow = function (tableName, params) {
            var deferred = $q.defer();

            cacheService.loadTable(tableName).then(function (table) {
                var service = autogenService.getAutogenService(table);
                service.get(
                    params,
                    function (data) {
                        deferred.resolve(data);
                    },
                    function (err) {
                        $log.error(err);
                        deferred.reject(err);
                    }
                );
            });

            return deferred.promise;
        };

        /**
         * Show error server
         *
         * @param response Server response
         */
        service.showServerError = function (response) {
            if (response && response.data && response.data.message) {
                $log.error(response.data.message);
                commonService.showError(response.data.message, 30000);
            } else {
                $log.error(response);
                commonService.showError($rootScope.getMessage('AD_ErrValidationServerError'), 30000);
            }
        };

        return service;
    });
});

