define(['app', 'moment'], function (app, moment) {
    "use strict";

    return app.registerFactory('commonService', ['$resource', '$rootScope', '$templateRequest', '$timeout', '$compile', 'coreConfigService',
        function ($resource, $rootScope, $templateRequest, $timeout, $compile, coreConfigService) {
            var service = {};

            service.showFieldHint = function (message, timeout) {
                if (!timeout)
                    timeout = 6000;
                $.smallBox({
                    content: message,
                    color: "#675100",
                    timeout: timeout,
                    icon: "fa fa-bullhorn swing animated"
                });
            };

            service.showHint = function (message, timeout) {
                if (!timeout)
                    timeout = 6000;
                $.bigBox({
                    title: $rootScope.getMessage('AD_titleInformation'),
                    content: message,
                    color: "#3276B1",
                    timeout: timeout,
                    icon: "fa fa-bell swing animated"
                });
            };

            service.showError = function (message, timeout) {
                if (!timeout)
                    timeout = 6000;
                $.bigBox({
                    title: $rootScope.getMessage('AD_titleError'),
                    content: message,
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    timeout: timeout
                });
            };

            /*
             * SmartAlerts
             */
            // With Callback
            service.showConfirm = function (message, scope, callbackOk, callbackCancel) {
                $.SmartMessageBox({
                    title: $rootScope.getMessage('AD_GlobalConfirm'),
                    content: message,
                    buttons: '[' + $rootScope.getMessage('AD_labelNo') + '][' + $rootScope.getMessage('AD_labelYes') + ']'
                }, function (ButtonPressed) {
                    if (ButtonPressed === $rootScope.getMessage('AD_labelYes')) {
                        if (callbackOk)
                            callbackOk(scope);
                    }
                    if (ButtonPressed === $rootScope.getMessage('AD_labelNo')) {
                        if (callbackCancel)
                            callbackCancel(scope);
                    }

                });
            };

            service.getMoment = function (value) {
                if (value._isAMomentObject)
                    return moment(value);
                if (value instanceof Date)
                    return moment(value);
                var d = new moment(value, "DD/MM/YYYY");
                return d.isValid() ? d : new moment(new Date(parseInt(value)));
            };

            service.getMomentDate = function (value) {
                var d = value instanceof Date ? new moment(value) : new moment(value, "DD/MM/YYYY");
                return d.isValid() ? d : new moment(new Date(parseInt(value)));
            };

            service.getFilterDate = function (value) {
                var d = value;
                if (typeof value == 'string') {
                    var indx = value.indexOf('T');
                    if (indx > 0) {
                        d = moment(value.substring(0, indx));
                    } else {
                        d = moment(value, 'DD/MM/YYYY');
                    }
                } else if (value instanceof Date) {
                    d = moment(value);
                }
                return d.isValid() ? d.format('DD/MM/YYYY') : value;
            };

            /**
             * Gets the first day of the current month
             *
             * @param {int} month Month to check. Starts at 1 (January)
             * @param {int} year Year to check
             * @returns {Date} First day
             */
            service.getFirstDayOfMonth = function (month, year) {
                var today = new Date();
                if (month === null || month === undefined) {
                    month = (today.getMonth() + 1);
                }
                if (year === null || year === undefined) {
                    year = (today.getYear() + 1900);
                }
                return new Date(month + /01/ + year);
            };

            /**
             * Gets the last day of the current month
             *
             * @param {int} month Month to check. Starts at 1 (January)
             * @param {int} year Year to check
             * @returns {Date} Last day
             */
            service.getLastDayOfMonth = function (month, year) {
                var today = new Date();
                if (month === null || month === undefined) {
                    return new Date((today.getYear() + 1900), (today.getMonth() + 1), 0);
                }
                if (year === null || year === undefined) {
                    year = (today.getYear() + 1900);
                }
                return new Date(year, month, 0);
            };

            service.getParamsFromHookResult = function (data, params) {
                var paramString = "";
                _.each(data.results, function (current) {
                    paramString += ("," + current);
                });
                if (paramString) {
                    if (params === "") {
                        return paramString.substring(1);
                    } else {
                        return params + paramString;
                    }
                }
                return params;
            };

            //show message
            service.showLoading = function () {
                $('.jarviswidget-loader').show();
            };

            //show message
            service.showSuccesNotification = function (msg, timeout) {
                if (!timeout)
                    timeout = 5000;
                $('.jarviswidget-loader').hide();
                $.smallBox({
                    title: $rootScope.getMessage('AD_titleSuccess'),
                    content: $rootScope.getMessage(msg),
                    color: "#739E73",
                    timeout: timeout,
                    icon: "fa fa-check"
                });
            };

            service.showRejectNotification = function (msg, timeout) {
                if (!timeout)
                    timeout = 5000;
                $('.jarviswidget-loader').hide();
                $.smallBox({
                    title: $rootScope.getMessage('AD_titleFail'),
                    content: $rootScope.getMessage(msg),
                    color: "#C46A69",
                    timeout: timeout,
                    icon: "fa fa-warning"
                });
            };

            service.clearResponsiveTable = function () {
                // Required to regenerate the responsive table
                if ($rootScope._dataTable) {
                    $rootScope._dataTable.destroy();
                    $rootScope._dataTable = null;
                }
            };

            /**
             * Remove underscores y convert to Java conventions
             *
             * @param input Name to convert (id_item_product)
             * @returns {string} Java-convention name (idItemProduct)
             */
            service.removeUnderscores = function (input) {
                var result = "";
                var tmp = input.split("_");
                for (var i = 0; i < tmp.length; i++) {
                    var fragment;
                    if (i == 0) {
                        fragment = tmp[i];
                    }
                    else {
                        fragment = tmp[i].substring(0, 1).toUpperCase() + tmp[i].substring(1);
                    }
                    result += fragment;
                }
                return result;
            };

            service.downloadMemoryFile = function (memoryStream, fileName) {
                var arrayBuffer2String = function (buf, callback) {
                    var f = new FileReader();
                    f.onload = function (e) {
                        callback(e.target.result)
                    };
                    var blob = new Blob([memoryStream], {type: "octet/stream"});
                    f.readAsText(blob);
                };
                arrayBuffer2String(memoryStream,
                    function (string) {
                        var parsed = null;
                        try {
                            parsed = JSON.parse(string);
                        }
                        catch (e) {
                        }
                        if (!parsed || parsed.success !== false) {
                            var blob = new Blob([memoryStream], {type: "octet/stream"});
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = fileName;
                            link.click();
                        }
                    }
                );
            };

            service.S4 = function () {
                return (Math.floor((1 + Math.random()) * 0x10000)).toString(16).substring(1).toUpperCase();
            };

            /**
             * Generates a UUID
             * @returns {string}
             */
            service.generateUID = function () {
                var array;
                var uuid = "",
                    i, digit = "";
                if (window.crypto && window.crypto.getRandomValues) {
                    array = new Uint8Array(16);
                    window.crypto.getRandomValues(array);

                    for (i = 0; i < array.length; i++) {
                        digit = array[i].toString(16).toLowerCase();
                        if (digit.length === 1) {
                            digit = "0" + digit;
                        }
                        uuid += digit;
                    }

                    return uuid;
                }

                return (this.S4() + this.S4() + this.S4() + this.S4() + this.S4() + this.S4() + this.S4() + this.S4());
            };

            /***********************************************************************************************************
             * Context menu section
             **********************************************************************************************************/

            service.contextMenuScope = null;
            service.hideContextMenu = function () {
                var $contextMenu = $("#floatingContextMenu");
                $contextMenu.remove();
                if (service.contextMenuScope)
                    service.contextMenuScope.$destroy();
                service.contextMenuScope = null;
            };

            service.showContextMenu = function (menuEntries, contextData, position) {
                $(window).click(function () {
                    service.hideContextMenu();
                });

                $timeout(function () {
                    $templateRequest(APP_EXTENSION_VINCO_CORE + '/common/views/context.menu.html?v=' + FW_VERSION).then(function (template) {
                        service.hideContextMenu();
                        service.contextMenuScope = $rootScope.$new(true);
                        service.contextMenuScope.menuEntries = menuEntries;
                        var linkFn = $compile(template);
                        var htlm = linkFn(service.contextMenuScope);
                        service.contextMenuScope.notifyClickMenu = function(menuOption) {
                            $rootScope.$broadcast(coreConfigService.events.CONTEXT_MENU_ITEM_SELECTED, { entry: menuOption, contextData: contextData });
                        };
                        $("body").append(htlm);
                        var $contextMenu = $("#floatingContextMenu");
                        var maxZ = Math.max.apply(null, $.map($('body > *'), function (e, n) {
                                if ($(e).css('position') === 'absolute')
                                    return parseInt($(e).css('z-index')) || 1;
                            })
                        );
                        $contextMenu.css('zIndex', maxZ + 1);
                        $contextMenu.css({
                            display: "block",
                            left: position.x,
                            top: position.y
                        });
                    }).catch(function (error) {
                        $log.error(error);
                    });
                }, 0)
            };

            /***********************************************************************************************************
             * End Context menu section
             **********************************************************************************************************/
            return service;
        }]);
});

