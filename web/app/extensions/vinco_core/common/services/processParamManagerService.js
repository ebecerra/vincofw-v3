/**
 * This service is used to store parameter data from local storage according to the
 * parameter rules defined
 * The following rules are applied according to the parameter saveType field:
 *
 *  1. NO       ----    The parameter value is never stored/retrieved from local storage
 *  2. GLOBAL   ----    The parameter value is stored/retrieved from the local storage key
 *                      "globalReportParam_" + parameter.name
 *                      meaning that the parameter will be shared across all the process with
 *                      a same parameter name
 *  3. PROCESS  ----    The parameter value is stored/retrieved from the local storage key
 *                      "processParam_" + parameter.idProcess + "_" + parameter.id + "_" + parameter.name
 *                      meaning that the parameter will be unique to the process that defined it
 *
 */
define(['app'], function (app) {
    "use strict";

    return app.registerFactory('processParamManagerService', ['cacheService', function (cacheService) {
        var service = {};

        /**
         * Loads the parameter value from the storage
         *
         * @param parameter Parameter
         */
        service.loadParameter = function (parameter) {
            var value = service.loadSessionParameter(parameter);
            if (!parameter.ranged && ((value === null || value === undefined))){
                    value = service.loadLocalParameter(parameter);
            } else if (parameter.ranged &&
                (value === null || value.start === null || value.start === undefined || value.start === null
                || value.end === null || value.end === undefined || value.end === null)) {
                value = service.loadLocalParameter(parameter);
            }
            return value;
        };

        /**
         * Loads the parameter value from the local storage
         *
         * @param storage Type of storage (e.g. localStorage, sessionStorage)
         * @param parameter Parameter
         */
        service.generalLoadParameter = function (storage, parameter) {
            var key;
            var keyRanged = {};
            if (parameter.saveType == "GLOBAL") {
                key = "globalReportParam_" + parameter.name;
            } else {
                key = "processParam_" + parameter.idProcess + "_" + parameter.id + "_" + parameter.name;
            }
            if (parameter.ranged) {
                keyRanged.start = key + "_start";
                keyRanged.end = key + "_end";
            }
            if (!parameter.ranged) {
                if (storage[key] == 'null')
                    return null;
                return storage[key];
            } else {
                return {
                    start: (storage[keyRanged.start] == 'null') ? null : storage[keyRanged.start],
                    end: (storage[keyRanged.end] == 'null') ? null : storage[keyRanged.end]
                };
            }
        };

        /**
         * Loads the parameter value from the session storage
         *
         * @param parameter Parameter
         */
        service.loadSessionParameter = function (parameter) {
            return service.generalLoadParameter(sessionStorage, parameter);
        };

        /**
         * Loads the parameter value from the local storage
         *
         * @param parameter Parameter
         */
        service.loadLocalParameter = function (parameter) {
            if (parameter.saveType == "NO")
                return null;
            return service.generalLoadParameter(localStorage, parameter);
        };

        /**
         * Stores the parameter value in the session storage
         *
         * @param parameter Parameter
         * @param start For ranged parameters, defines if the value corresponds to the start or the end of the range
         */
        service.storeSessionParameter = function (parameter, start) {
            service.generalStoreParameter(sessionStorage, parameter, start);
        };

        /**
         * Stores the parameter value in the local storage
         *
         * @param parameter Parameter
         * @param start For ranged parameters, defines if the value corresponds to the start or the end of the range
         */
        service.storeLocalParameter = function (parameter, start) {
            if (parameter.saveType == "NO")
                return;
            service.generalStoreParameter(localStorage, parameter, start);
        };

        /**
         * Stores the parameter value in the storage
         *
         * @param storage Type of storage (e.g. localStorage, sessionStorage)
         * @param parameter Parameter
         * @param start For ranged parameters, defines if the value corresponds to the start or the end of the range
         */
        service.generalStoreParameter = function (storage, parameter, start) {
            var key;
            var keyRanged = {};
            if (parameter.saveType == "GLOBAL") {
                key = "globalReportParam_" + parameter.name;
            } else {
                key = "processParam_" + parameter.idProcess + "_" + parameter.id + "_" + parameter.name;
            }
            var value = parameter.value;
            if (value && parameter.reference.rtype === 'DATE') {
                value = parameter.value.format("DD/MM/YYYY");
            }
            if (parameter.ranged) {
                keyRanged.start = key + "_start";
                keyRanged.end = key + "_end";
                if (start) {
                    storage[keyRanged.start] = value;
                } else {
                    storage[keyRanged.end] = value;
                }
            } else {
                storage[key] = value;
            }
        };

        /**
         * Stores the parameter value in the storage
         *
         * @param parameter Parameter
         * @param start For ranged parameters, defines if the value corresponds to the start or the end of the range
         */
        service.storeParameter = function (parameter, start) {
            service.storeLocalParameter(parameter, start);
            service.storeSessionParameter(parameter, start);
        };

        /**
         * Replace default value for a parameter
         *
         * @param item Input values
         * @param parentItem Parent item
         * @param globalParameters Global parameters
         * @param valueDefault Default expression
         * @returns {{success: boolean, defaultValue: *}}
         */
        service.getParameterDefaultValue = function (item, parentItem, globalParameters, valueDefault) {
            var keys = _.keys(item);
            var defaultVal = valueDefault;
            var result = {
                success: true,
                defaultValue: valueDefault
            };
            if (!cacheService.isGlobalParameterCompleteConditional(defaultVal)) {
                defaultVal = cacheService.replaceGlobalParameterConditional(globalParameters, defaultVal);
            }
            if (keys.length === 0) {
                defaultVal = cacheService.replaceJSConditional({
                    column: {
                        standardName: ''
                    }
                }, null, defaultVal);
            } else {
                _.each(keys, function (key) {
                    if (!cacheService.isCompleteConditional(defaultVal)) {
                        defaultVal = cacheService.replaceJSConditional({
                            column: {
                                standardName: key
                            }
                        }, item[key], defaultVal);
                    }
                });
            }
            if (parentItem) {
                keys = _.keys(parentItem.item);
                if (keys.length === 0) {
                    defaultVal = cacheService.replaceJSConditional({
                        column: {
                            standardName: ''
                        }
                    }, null, defaultVal);
                } else {
                    _.each(keys, function (key) {
                        if (!cacheService.isCompleteConditional(defaultVal)) {
                            defaultVal = cacheService.replaceJSConditional({
                                column: {
                                    standardName: key
                                }
                            }, parentItem.item[key], defaultVal);
                        }
                    });
                }
            }
            if (cacheService.isCompleteConditional(defaultVal) || (defaultVal !== valueDefault)) {
                result.defaultValue = defaultVal;
            } else {
                result.success = false;
            }
            return result;
        };

        return service;

    }]);
});

