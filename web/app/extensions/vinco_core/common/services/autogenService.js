/**
 * This service handles the injection of dynamic services
 * We will asume that the injected service will have the table name (without underscores) followed by the string AutogenService
 * The following is an example of the generated service name
 *
 *      Table       Service Name
 *      ad_column   adColumnAutogenService
 *      ad_table    adTableAutogenService
 */
define(['app', 'lodash'], function (app, _) {
    "use strict";

    return app.registerFactory('autogenService', ['$resource', '$rootScope', '$injector', 'commonService', '$log', 'hookService', function ($resource, $rootScope, $injector, commonService, $log, hookService) {
        var service = {};

        /**
         * Returns the associated table service
         *
         * @returns {*} Service
         */
        service.getTableService = function (tableInfo) {
            var service = null;
            try {
                service = this.getAutogenService(tableInfo);
            } catch (e) {
                $log.log(e);
            }
            return service;
        };

        service.getAutogenService = function (adTable) {
            $log.log("Generating autogen service for table: " + adTable.name);
            var serviceName = commonService.removeUnderscores(adTable.name) + "AutogenService";
            var service = null;
            try {
                service = $injector.get(serviceName);
            } catch (e) {
                $log.log("Not found: " + serviceName + " it be created");
            }
            if (service) {
                $log.log("Returning already registered service for table: " + adTable.name);
                return service;
            } else {
                var primaryKey = _.find(adTable.columns, function (currentCol) {
                    return currentCol.primaryKey;
                });
                if (!primaryKey) {
                    $log.error("Could not register service. Table does not have a primary key. Table: " + adTable.name);
                    return null;
                }
                var params = {};
                params[primaryKey.standardName] = '@' + primaryKey.standardName;
                params['_'] = (new Date()).getTime();
                var putParams = {};
                putParams[primaryKey.standardName] = '@' + primaryKey.standardName;

                var baseUrl = adTable.module.restPath + '/' + adTable.name;
                var url = baseUrl + '/:idClient/:id';
                var exportUrl = baseUrl + "/:idClient/export/:idTab";
                var putUrl = baseUrl + '/:idClient/:' + primaryKey.standardName;
                var postUrl = baseUrl + '/:idClient';
                var sortUrl = baseUrl + '/:idClient/sort';
                var deleteUrl = baseUrl + '/:idClient/delete_batch';
                var saveMultipleUrl = baseUrl + '/:idClient/save_multiple?ids=:ids';
                var deleteImageUrl = baseUrl + '/:idClient/delete_image/:id/:fieldName/:file';
                var rowAuditUrl = baseUrl + '/:idClient/row_audit';
                if (adTable.name == "ad_client") {
                    url = baseUrl + "/:id";
                    // exportUrl = baseUrl + "/export/:idTab";
                    putUrl = baseUrl + '/:' + primaryKey.standardName;
                    postUrl = baseUrl;
                    deleteUrl = baseUrl + '/delete_batch';
                    deleteImageUrl = baseUrl + "/delete_image/:id/:fieldName/:file";
                    saveMultipleUrl = baseUrl + '/save_multiple';
                    rowAuditUrl = baseUrl + '/row_audit';
                }
                var operations = {
                    list: {
                        method: "GET",
                        isArray: false
                    },
                    export: {
                        method: "GET",
                        url: REST_HOST_PATH + exportUrl,
                        responseType: 'arraybuffer',
                        transformResponse: function (data, headers) {
                            var fileName = 'unknown';
                            if (headers('content-disposition')) {
                                fileName = headers('content-disposition').replace(/attachment; filename=/g, '');
                                fileName = fileName.replace(/"/g, '');
                                fileName = fileName.replace(/'/g, '');
                            }
                            return {
                                result: data,
                                fileName: fileName,
                                headers: headers
                            };
                        }
                    },
                    rowAudit: {
                        method: "GET",
                        url: REST_HOST_PATH + rowAuditUrl
                    },
                    deleteImage: {
                        method: "DELETE",
                        url: REST_HOST_PATH + deleteImageUrl
                    },
                    // Updates an item
                    update: {
                        method: 'PUT',
                        params: putParams,
                        url: REST_HOST_PATH + putUrl
                    },
                    // Updates an item
                    updateFile: {
                        method: 'PUT',
                        params: putParams,
                        url: REST_HOST_PATH + putUrl,
                        headers: {'Content-Type': undefined},
                        transformRequest: function (data) {
                            var formData = new FormData();
                            var keys = _.keys(data);
                            var containsFiles = false;
                            var newData = {};
                            _.each(keys, function (key) {
                                if (!(data[key] instanceof (FileList))) {
                                    newData[key] = data[key];
                                }
                            });
                            var entityData = new Blob([JSON.stringify(newData)], {
                                type: "application/json"
                            });
                            formData.append("entity", entityData);
                            _.each(keys, function (key) {
                                if ((data[key] instanceof (FileList))) {
                                    containsFiles = true;
                                    formData.append("file_" + key, data[key][0]);
                                }
                            });
                            return formData;
                        }
                    },
                    // Creates multiple items at once
                    saveMultiple: {
                        method: 'POST',
                        params: {ids: '@ids'},
                        url: REST_HOST_PATH + saveMultipleUrl,
                        transformResponse: function (data) {
                            return data;
                        }
                    },
                    // Creates an item
                    create: {
                        method: 'POST',
                        url: REST_HOST_PATH + postUrl,
                        transformResponse: function (data) {
                            return {id: data};
                        }
                    },
                    // Creates an item
                    createFile: {
                        method: 'POST',
                        url: REST_HOST_PATH + postUrl,
                        headers: {'Content-Type': undefined},
                        transformRequest: function (data) {
                            var formData = new FormData();
                            var keys = _.keys(data);
                            var containsFiles = false;
                            var newData = {};
                            _.each(keys, function (key) {
                                if (!(data[key] instanceof (FileList))) {
                                    //if (!(data[key] == null || data[key] === undefined))
                                    //    formData.append(key, data[key]);
                                    newData[key] = data[key];
                                }
                            });

                            var entityData = new Blob([JSON.stringify(newData)], {
                                type: "application/json"
                            });
                            formData.append("entity", entityData);
                            _.each(keys, function (key) {
                                if ((data[key] instanceof (FileList))) {
                                    containsFiles = true;
                                    formData.append("file_" + key, data[key][0]);
                                    // if (data[key].length > 1) {
                                    //     for (var i = 0; i < data[key].length; i++) {
                                    //         formData.append("file" + key, data[key][i]);
                                    //     }
                                    // } else if (data[key].length == 1) {
                                    //     formData.append("file", data[key][0]);
                                    // }
                                }
                            });
                            return formData;
                        }, transformResponse: function (data) {
                            return {id: data};
                        }
                    },
                    // Deletes an item
                    delete: {
                        method: 'DELETE',
                        params: {id: '@id'}
                    },
                    // Deletes an item
                    deleteBatch: {
                        method: 'DELETE',
                        url: REST_HOST_PATH + deleteUrl
                    },
                    // Deletes an item
                    sort: {
                        method: 'POST',
                        url: REST_HOST_PATH + sortUrl
                    }
                };
                app.registerFactory(serviceName, function ($resource) {
                    var resource = $resource(
                        REST_HOST_PATH + url,
                        {
                            _: (new Date()).getTime(),
                            idClient: '@idClient'
                        },
                        operations
                    );

                    /**
                     * Calls the list function in the backend by adding the AD_QueryTable hook parameters to the request
                     *
                     * @param args Original parameters
                     * @param okCallback Success callback
                     * @param errorCallback Error callback
                     */
                    resource.query = function(args, okCallback, errorCallback){
                        var that = this;
                        hookService.execHooks('AD_QueryTable', { tableName: adTable.name, constraints: args.q }).then(function (data) {
                            if (data && data.success) {
                                var argKeys = _.keys(args);
                                if (_.find(argKeys, function (current) { return current === "q"; })) {
                                    args.q = commonService.getParamsFromHookResult(data, args.q);
                                }
                                that.list(args, okCallback, errorCallback);
                            } else {
                                errorCallback("Could not retrieve information for hook AD_QueryTable");
                            }
                        }, function (error) {
                            if (errorCallback)
                                errorCallback(error);
                        });
                    };

                    return resource;
                });
                return $injector.get(serviceName);
            }

        };

        return service;
    }]);
});

