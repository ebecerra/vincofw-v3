/**
 * This service handles the dynamic cache for application
 */
define(['app', 'lodash'], function (app, _) {
    "use strict";

    return app.registerFactory('cacheService', ['$resource', '$rootScope', '$q', '$log', 'commonService', 'autogenService', 'hookService', 'Authentication',
        'adWindowService', 'adTabService', 'adRefListService', 'adRefTableService', 'adRefButtonService', 'adRefSearchColumnService',
        'adTableService', 'adRoleService', 'tabManagerService', 'adReferenceService', 'adPreferenceService', 'Language', 'adClientLanguageService',
        'adTranslationService', 'adChartFilterService', 'adChartLinkedService', 'adChartColumnService', 'adFieldGridService', 'adHtmlTemplateService',
        'adClientService', 'adPrivilegeService', 'adProcessService',
        function ($resource, $rootScope, $q, $log, commonService, autogenService, hookService, Authentication,
                  adWindowService, adTabService, adRefListService, adRefTableService, adRefButtonService, adRefSearchColumnService,
                  adTableService, adRoleService, tabManagerService, adReferenceService, adPreferenceService, Language, adClientLanguageService,
                  adTranslationService, adChartFilterService, adChartLinkedService, adChartColumnService, adFieldGridService, adHtmlTemplateService,
                  adClientService, adPrivilegeService, adProcessService) {
            var service = {};

            // Constants

            // Properties
            service.rolClientId = null;
            service.windows = [];
            service.tabs = [];
            service.pendTabs = [];
            service.tables = [];
            service.pendTables = [];
            service.fieldGroups = [];
            service.processes = [];
            service.references = [];
            service.tempTabledir = [];
            service.roles = [];
            service.modules = [];
            service.primaryKeys = [];
            service.editingItem = [];
            service.editingItemWithId = [];
            service.editingSiblings = [];
            service.tabFormItem = {};
            service.preferences = {};
            service.customCache = [];
            service.htmlTemplates = null;

            service.stdPref = {
                fractionSize: 3,
                canChangeViewColumns: false,
                canExportListData: false,
                dateFormat: 'dd/MM/yyyy',
                dateTimeFormat: 'dd/MM/yyyy HH:mm:ss',
                timeFormat: 'HH:mm:ss',
                serverCacheUrl: null
            };

            // Cache to store the clients
            service.clients = [];
            service.privileges = [];
            // Cache to store information about the columns of a grid
            service.gridColumns = {};
            // Cache store for reference table values
            // Cache will store the following information:
            // referenceTableDataValues  = {
            //                              referenceTableId: {
            //                                                  scopes: [],  //Array of Scopes that will be cached
            //                                                  data: {}     //Data values to be cached
            //                              ...
            //                             }
            // Each time a scope request for the information related to the referenced table,
            // if no entry is found in referenceTableDataValues, a request call to the backend will be performed and
            // a new entry will be created, with the calling scope id as the first element in the array
            // if an entry is found, the scope id will be added to the scopes array, in order to keep track of the scopes using the data
            // When the scope is destroyed, it has the responsability to call the clearing function clearReferenceTableDataValue
            // in order to be deregistered from the cache
            service.referenceTableDataValues = {};

            service.clearCache = function () {
                service.rolClientId = null;
                service.windows = [];
                service.tabs = [];
                service.pendTabs = [];
                service.tables = [];
                service.pendTables = [];
                service.fieldGroups = [];
                service.processes = [];
                service.references = [];
                service.tempTabledir = [];
                service.roles = [];
                service.modules = [];
                service.primaryKeys = [];
                service.editingItem = [];
                service.tabFormItem = {};
                service.customCache = [];
                service.privileges = [];
                service.referenceTableDataValues = {};
                service.gridColumns = {};
                service.clearHtmlTemplates();
                service.clearClientCache();
            };

            /**
             * Clears HtmlTemplates cache
             */
            service.clearHtmlTemplates = function(){
                service.htmlTemplates = null;
            };

            service.clearClientCache = function(){
                service.clients = [];
            };

            service.loadClients = function(){
                var defered = $q.defer();
                var promise = defered.promise;
                if (service.clients.length > 0){
                    defered.resolve(service.clients);
                } else {
                    $log.info("Loading clients into cache");
                    adClientService.query(
                        { q: '' },
                        function (data) {
                            service.clients = data.content;
                            defered.resolve(service.clients);
                        }, function (err) {
                            defered.reject(err);
                        }
                    );
                }
                return promise;
            };

            /**
             * Loads the HtmlTemplates and stores them in cache
             */
            service.loadHtmlTemplates = function(){
                var defered = $q.defer();
                var promise = defered.promise;
                if (service.htmlTemplates){
                    defered.resolve(service.htmlTemplates);
                } else {
                    adHtmlTemplateService.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: ''
                        },
                        function (data) {
                            service.htmlTemplates = data.content;
                            defered.resolve(service.htmlTemplates);
                        }, function (err) {
                            defered.reject(err);
                        }
                    );
                }
                return promise;
            };

            /**
             * Loads the Privileges and stores them in cache
             */
            service.loadPrivileges = function(){
                var defered = $q.defer();
                var promise = defered.promise;
                if (service.privileges.length !== 0){
                    defered.resolve(service.privileges);
                } else {
                    adPrivilegeService.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: ''
                        },
                        function (data) {
                            service.privileges = _.map(data.content, function(current){
                               return {
                                   idPrivilege: current.idPrivilege,
                                   name: current.name
                               }
                            });
                            defered.resolve(service.privileges);
                        }, function (err) {
                            defered.reject(err);
                        }
                    );
                }
                return promise;
            };

            /**
             * Set item into custom cache
             *
             * @param id Identifier
             * @param value Value
             */
            service.setCustomCache = function (id, value) {
                var cache = _.find(service.customCache, function (item) {
                    return item.id == id;
                });
                if (cache) {
                    cache.value = value;
                } else {
                    service.customCache.push({id: id, value: value});
                }
            };

            /**
             * Verify is item
             * @param id
             * @returns {boolean}
             */
            service.isInCustomCache = function (id) {
                var cache = _.find(service.customCache, function (item) {
                    return item.id == id;
                });
                return cache !== undefined;
            };

            service.getCustomCache = function (id) {
                var cache = _.find(service.customCache, function (item) {
                    return item.id == id;
                });
                return cache ? cache.value : null;
            };

            /**
             * Clears all the cache info of all the forms
             */
            service.clearAllFormInfo = function () {
                service.editingItem = [];
                service.tabFormItem = {};
            };

            $rootScope.$on(app.eventDefinitions.GLOBAL_TAB_CLOSED, function (event, data) {
                // Clearing form cached information
                service.clearFormItem(data.idWindow);

                // Remove cache information of the tab
                var storedFormInfoTabs = _.keys(service.editingItem);
                _.each(storedFormInfoTabs, function (idTab) {
                    if (service.editingItem[idTab] && service.editingItem[idTab].tab.idWindow == data.idWindow) {
                        service.editingItem = _.omit(service.editingItem, idTab);
                    }
                });
            });

            /**
             * Store information about the item and fields to be used in child forms
             *
             * @param formInfo Form information
             * @param formInfo.idWindow Window ID
             * @param formInfo.tablevel Tab level
             * @param item Form item
             * @param fields Form associated fields
             * @param tab Form associated tab
             */
            service.storeFormItem = function (formInfo, item, fields, tab) {
                if (!service.tabFormItem[formInfo.idWindow]) {
                    service.tabFormItem[formInfo.idWindow] = {};
                }
                service.tabFormItem[formInfo.idWindow][formInfo.tablevel] = {item: item, fields: fields, tab: tab};
            };

            /**
             * Gets the form information about item and fields to be used in child forms
             *
             * @param formInfo Form information with idWindow and level of the desired form to obtain data from
             * @returns {*} Returns the related item and fields
             */
            service.getFormItem = function (formInfo) {
                if (!service.tabFormItem[formInfo.idWindow]) {
                    return null;
                }
                return service.tabFormItem[formInfo.idWindow][formInfo.tablevel];
            };

            service.clearFormItem = function (idWindow, tablevel) {
                if (!tablevel) {
                    service.tabFormItem[idWindow] = null;
                } else {
                    service.tabFormItem[idWindow][tablevel] = null;
                }
            };

            service.getPrimaryKey = function () {
                // Check cache
                return this.primaryKeys;
            };

            service.setPrimaryKey = function (name1, value1) {
                // Check cache
                var primary = _.find(this.primaryKeys, function (pry) {
                    return pry.name == name1;
                });
                if (primary) {
                    primary.value = value1;
                } else {
                    var data = {
                        name: name1,
                        value: value1
                    };
                    this.primaryKeys.push(data);
                }
            };

            /**
             * Clears the columns' cache related to the tab
             * @param idTab
             */
            service.clearGridColumnCache = function(idTab){
                if (service.getTab(idTab)) {
                    // The tab was cached, it should be reloaded
                    service.loadTab(idTab, true);
                }
                if (service.gridColumns[idTab])
                    delete service.gridColumns[idTab];
            };

            /**
             * Load the grid columns from cache or from server, if no cache information was found
             */
            service.loadGridColumn = function (idTab) {
                var defered = $q.defer();
                var promise = defered.promise;
                if (service.gridColumns[idTab]) {
                    defered.resolve(service.gridColumns[idTab]);
                } else {
                    adFieldGridService.columns({
                        idClient: service.loggedIdClient(),
                        idLanguage: Authentication.getCurrentLanguage(),
                        idTab: idTab,
                        idUser: service.loggedIdUser()
                    }, function (data) {
                        var columns = _.sortBy(data.content, function (current) {
                            return current.seqno;
                        });
                        service.gridColumns[idTab] = columns;
                        defered.resolve(columns);
                    }, function (err) {
                        defered.reject(err);
                    });
                }
                return promise;
            };

            /**
             * Get preference value
             *
             * @param name Preference name
             * @param def Default value
             * @returns {*}
             */
            service.getPreference = function (name, def) {
                var pref = service.preferences[name];
                return pref ? pref.value : def;
            };

            /**
             * Load standard preferences preferences
             */
            service.loadStandardPreferences = function () {
                service.stdPref.fractionSize = service.getPreference('NumberDecimalPlaces', 2);
                service.stdPref.dateFormat = service.getPreference('DateFormat', 'dd/MM/yyyy');
                service.stdPref.dateTimeFormat = service.getPreference('DateTimeFormat', 'dd/MM/yyyy HH:mm:ss');
                service.stdPref.timeFormat = service.getPreference('TimeFormat', 'HH:mm:ss');
                service.stdPref.serverCacheUrl = service.getPreference('ApacheServerCacheUrl', null);

                service.loadPreference('CanChangeViewColumns', false).then(function (pref) {
                    service.stdPref.canChangeViewColumns = (pref.value === "Y");
                });
                service.loadPreference('CanExportListData', false).then(function (pref) {
                    service.stdPref.canExportListData = (pref.value === "Y");
                });
            };

            /**
             * Load all preferences' information and store them in cache
             */
            service.loadPreferences = function () {
                var defered = $q.defer();
                var promise = defered.promise;
                adPreferenceService.preferences({
                    idClient: service.loggedIdClient(),
                    idRole: Authentication.getCurrentRole()
                }, function (data) {
                    _.each(data.properties.preferences, function (pref) {
                        service.preferences[pref.property] = {
                            property: pref.property,
                            value: pref.value
                        };
                    });
                    service.loadStandardPreferences();
                    defered.resolve();
                }, function (err) {
                    defered.reject(err);
                });
                return promise;
            };

            /**
             * Load preference information (Not use Cache)
             *
             * @param property Preference property name.
             * @param defValue Default value
             * @returns {*}
             */
            service.loadPreference = function (property, defValue) {
                var defered = $q.defer();
                var promise = defered.promise;
                hookService.execHooks('AD_BeforeLoadPreference', { name: property }).then(function (result) {
                    adPreferenceService.preference({
                        idClient: service.loggedIdClient(),
                        idRole: Authentication.getCurrentRole(),
                        name: property,
                        params: result.results
                    }, function (data) {
                        if (data.properties && data.properties.preference) {
                            var pref = data.properties.preference;
                            service.preferences[property] = {
                                idPreference: pref.idPreference,
                                value: pref.value,
                                property: pref.property
                            };
                            defered.resolve(service.preferences[property]);
                        } else {
                            if (defValue) {
                                service.preferences[property] = {
                                    idPreference: '0',
                                    value: defValue,
                                    property: property
                                };
                                defered.resolve(service.preferences[property]);
                            } else {
                                defered.reject("No reference with property: " + property);
                            }
                        }
                    }, function (err) {
                        defered.reject(err);
                    });
                });
                return promise;
            };

            /**
             * Load window information (from Cache if exist)
             *
             * @param idWindow Tab id.
             * @returns {*}
             */
            service.loadWindow = function (idWindow) {
                var defered = $q.defer();
                var promise = defered.promise;
                // Check cache
                var window = _.find(this.windows, function (win) {
                    return win.idWindow === idWindow;
                });
                if (window) {
                    defered.resolve(window.info);
                } else {
                    var that = this;
                    adWindowService.query(
                        {
                            idClient: service.loggedIdClient(),
                            idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : null,
                            q: 'idWindow=' + idWindow
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                // Loading window tabs
                                adTabService.query(
                                    {
                                        idClient: service.loggedIdClient(),
                                        idUser: service.loggedIdUser(),
                                        idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : null,
                                        q: 'active=1,idWindow=' + idWindow,
                                        sort: '[{ "property": "tablevel", "direction": "ASC" }]'
                                    },
                                    function (dataTabs) {
                                        if (dataTabs.content.length > 0) {
                                            var tabs = [], pendTabs = [];
                                            _.each(dataTabs.content, function (tab) {
                                                tabs.push({idTab: tab.idTab});
                                                pendTabs.push(tab);
                                            });
                                            that.processPendTabs(pendTabs).then(function () {
                                                _.each(tabs, function (tab) {
                                                    tab.tab = that.getTab(tab.idTab);
                                                });

                                                var emptyTab = _.find(tabs, function (current) {
                                                    return current.tab === null || current.tab === undefined;
                                                });
                                                if (emptyTab) {
                                                    defered.reject("Incompleted tabs");
                                                } else {
                                                    var winInfo = {
                                                        idWindow: idWindow,
                                                        info: {
                                                            idWindow: data.content[0].idWindow,
                                                            name: data.content[0].name,
                                                            tabs: tabs
                                                        }
                                                    };
                                                    that.windows.push(winInfo);
                                                    _.each(winInfo.info.tabs, function (current) {
                                                        current.tab.runtime = {};
                                                        if (current.tab && !service.isSpecialTab(current.tab)) {
                                                            var siblingSortable = _.find(winInfo.info.tabs, function (sibling) {
                                                                return (sibling.idTab !== current.idTab &&
                                                                    sibling.tab.tablevel === current.tab.tablevel && sibling.tab.uipattern === "SORTABLE" &&
                                                                    current.tab.idTable === sibling.tab.idTable)
                                                            });
                                                            var siblingMultiselect = _.find(winInfo.info.tabs, function (sibling) {
                                                                return (sibling.idTab !== current.idTab &&
                                                                    sibling.tab.tablevel === (current.tab.tablevel) && sibling.tab.uipattern === "MULTISELECT" &&
                                                                    current.tab.idTable === sibling.tab.idTable)
                                                            });
                                                            if (siblingSortable) {
                                                                current.tab.runtime.sortable = siblingSortable.tab;
                                                            }
                                                            if (siblingMultiselect) {
                                                                current.tab.runtime.multiselect = siblingMultiselect.tab;
                                                            }
                                                        }
                                                    });
                                                    // Set main tab and secondary related tab
                                                    _.each(winInfo.info.tabs, function (current) {
                                                        if (current.tab && !service.isSpecialTab(current.tab) && current.tab.ttype !== "CHARACTERISTIC") {
                                                            current.tab.runtime.defaultTab = true;
                                                            current.tab.runtime.defaultIdTab = current.tab.idTab;
                                                            /*
                                                             var sibling = _.find(winInfo.info.tabs, function (sibling) {
                                                             return (
                                                             sibling.idTab !== current.idTab && sibling.tab.tablevel === current.tab.tablevel &&
                                                             !service.isSpecialTab(sibling.tab) && current.tab.idTable === sibling.tab.idTable &&
                                                             sibling.tab.ttype !== "CHARACTERISTIC" && sibling.tab.ttype !== "CHART"
                                                             );
                                                             });
                                                             if (sibling) {
                                                             current.tab.runtime.relatedTab = sibling.tab.idTab;
                                                             if (current.tab.viewDefault) {
                                                             sibling.tab.runtime.defaultTab = false;
                                                             current.tab.runtime.defaultIdTab = current.tab.idTab;
                                                             sibling.tab.runtime.defaultIdTab = current.tab.idTab;
                                                             } else if (sibling.tab.viewDefault) {
                                                             current.tab.runtime.defaultTab = false;
                                                             sibling.tab.runtime.defaultTab = true;
                                                             current.tab.runtime.defaultIdTab = sibling.tab.idTab;
                                                             sibling.tab.runtime.defaultIdTab = sibling.tab.idTab;
                                                             }
                                                             } else {
                                                             current.tab.runtime.defaultIdTab = current.tab.idTab;
                                                             }
                                                             */
                                                        } else if (current.tab && current.tab.ttype === "CHARACTERISTIC") {
                                                            current.tab.runtime.defaultTab = true;
                                                            current.tab.runtime.defaultIdTab = current.tab.idTab;
                                                        }
                                                    });
                                                    defered.resolve(winInfo.info);
                                                }
                                            }).catch(function (err) {
                                                defered.reject(err);
                                            });
                                        } else {
                                            defered.reject();
                                        }
                                    },
                                    function (err) {
                                        defered.reject(err);
                                    }
                                );
                            } else {
                                defered.reject();
                            }
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                }
                return promise;
            };

            /**
             * Defines if the tab should not be displayed unless explicitly demanded (e.g. SORTABLE and MULTISELECT tabs)
             *
             * @param tab Tab information
             * @returns {boolean} True if the tab should not be displayed. False i.o.c.
             */
            service.isSpecialTab = function (tab) {
                return tab.uipattern === "SORTABLE" || tab.uipattern === "MULTISELECT";
            };

            /**
             * Load the related MULTISELECT tab (e.g. the tab of uipattern MULTISELECT at the same tab level as the desired tab
             *
             * @param idTab Tab id.
             * @returns {*}
             */
            service.loadRelatedMultiselectTab = function (idTab) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var tab = _.find(service.tabs, function (tab) {
                    return tab.idTab === idTab;
                });
                if (tab && tab.info.runtime && tab.info.runtime.multiselect) {

                    defered.resolve(tab.info.runtime.multiselect);
                }
                else {
                    defered.reject("Could not find a related MULTISELECT for tab: " + idTab);
                }
                return promise;
            };

            /**
             * Load the related SORTABLE tab (e.g. the tab of uipattern SORTABLE at the same tab level as the desired tab
             *
             * @param idTab Tab id.
             * @returns {*}
             */
            service.loadRelatedSortableTab = function (idTab) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var tab = _.find(service.tabs, function (tab) {
                    return tab.idTab === idTab;
                });
                if (tab) {
                    // Related sortable tab should be of the same window, same tab level, same table and uipattern SORTABLE
                    var tabElement = _.find(service.tabs, function (current) {
                        var info = tab.info;
                        var currentInfo = current.info;
                        return info.idWindow === currentInfo.idWindow && info.tablevel === currentInfo.tablevel
                            && currentInfo.uipattern === "SORTABLE"
                            && info.idTable === currentInfo.idTable;
                    });
                    if (tabElement) {
                        defered.resolve(tabElement.info);
                    }
                    else
                        defered.reject("Could not find a related MULTISELECT for tab: " + idTab);
                }
                else {
                    defered.reject("Could not find a related MULTISELECT for tab: " + idTab);
                }
                return promise;
            };

            service.addAuditTab = function(auditTabId, tabInfo){
                var tab = _.find(service.tabs, function (tab) {
                    return tab.idTab === auditTabId;
                });
                if (tab){
                    tab.info = tabInfo;
                } else {
                    service.tabs.push({
                        idTab: auditTabId,
                        info: tabInfo
                    })
                }
            };

            /**
             * Load tab information (from Cache if exist)
             *
             * @param idTab Tab id.
             * @returns {*}
             */
            service.loadTab = function (idTab, forceReload) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var tab = _.filter(service.tabs, function (tab) {
                    return tab.idTab === idTab;
                });
                if (tab.length > 0 && !forceReload) {
                    defered.resolve(tab[0].info);
                } else {
                    this.tabs = _.filter(this.tabs, function (tab) {
                        return tab.idTab !== idTab;
                    });
                    var that = this;
                    adTabService.query(
                        {
                            idClient: service.loggedIdClient(),
                            idUser: service.loggedIdUser(),
                            q: 'idTab=' + idTab
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                that.processTab(data.content[0], forceReload).then(function (data) {
                                    if (tab.length > 0 && forceReload)
                                        data.runtime = tab[0].info.runtime;
                                    defered.resolve(data);
                                }).catch(function () {
                                    defered.reject();
                                });
                            } else {
                                defered.reject();
                            }
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                }

                return promise;
            };

            /**
             * Load table by name
             *
             * @param name Table name
             * @returns {*}
             */
            service.loadTable = function (name) {
                var defered = $q.defer();
                var promise = defered.promise;

                var table = _.find(this.tables, function (table) {
                    return table.info.name == name;
                });
                // Check cache
                if (table) {
                    defered.resolve(table.info);
                } else {
                    var that = this;
                    adTableService.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: 'name=' + name
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                that.processTable(data.content[0]).then(function (table) {
                                    defered.resolve(table);
                                }).catch(function () {
                                    defered.reject();
                                });
                            } else {
                                defered.reject();
                            }
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                }

                return promise;
            };

            /**
             * Load table by id
             *
             * @param id Table id
             * @returns {*}
             */
            service.loadTableById = function (id) {
                var defered = $q.defer();
                var promise = defered.promise;

                var table = _.find(this.tables, function (table) {
                    return table.info.name == name;
                });
                // Check cache
                if (table) {
                    defered.resolve(table.info);
                } else {
                    var that = this;
                    adTableService.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: 'id=' + id
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                that.processTable(data.content[0]).then(function (table) {
                                    defered.resolve(table);
                                }).catch(function () {
                                    defered.reject();
                                });
                            } else {
                                defered.reject();
                            }
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                }

                return promise;
            };

            /**
             * Load Reference information (from Cache if exist)
             *
             * @param idReference Reference Id.
             * @returns {*}
             */
            service.loadReferenceData = function (idReference) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var reference = _.find(this.references, function (ref) {
                    return ref.idReference === idReference;
                });

                if (reference) {
                    defered.resolve(reference.rtype);
                } else {
                    var that = this;
                    adReferenceService.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: 'idReference=' + idReference
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                that.loadReference(idReference, data.content[0].rtype).then(function () {
                                    that.processPendTables().then(function () {
                                        defered.resolve(data.content[0].rtype);
                                    }).catch(function (err) {
                                        $log.log(err);
                                        defered.reject(err);
                                    });
                                }).catch(function (err) {
                                    $log.log(err);
                                    defered.reject(err);
                                });
                            } else {
                                defered.reject("No reference loaded" + idReference);
                            }

                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        });
                }
                return promise;
            };

            /**
             * Load Reference information (from Cache if exist)
             *
             * @param idReference Reference Id.
             * @param rtype Reference type
             * @returns {*}
             */
            service.loadReference = function (idReference, rtype) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var reference = _.find(this.references, function (ref) {
                    return ref.idReference == idReference;
                });
                if (reference) {
                    defered.resolve();
                } else {
                    var that = this;
                    if (rtype == 'LIST' || rtype == "LISTMULTIPLE") {
                        adRefListService.query(
                            {
                                idClient: service.loggedIdClient(),
                                idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : null,
                                q: 'idReference=' + idReference
                            },
                            function (data) {
                                that.references.push({idReference: idReference, rtype: rtype, values: data.content});
                                defered.resolve();
                            },
                            function (err) {
                                $log.log(err);
                                defered.reject(err);
                            }
                        );
                    } else if (rtype == "TABLEDIR" || rtype == "TABLE" || rtype == "SEARCH") {
                        adRefTableService.query(
                            {
                                idClient: service.loggedIdClient(),
                                q: 'idReference=' + idReference
                            },
                            function (data) {
                                if (data.content.length > 0) {
                                    var ref = data.content[0];
                                    var referenceInfo = {
                                        idReference: idReference,
                                        rtype: rtype,
                                        info: {
                                            idTable: ref.idTable,
                                            idRefTable: ref.idRefTable,
                                            idKey: ref.idKey,
                                            idDisplay: ref.idDisplay,
                                            sqlwhere: ref.sqlwhere,
                                            sqlorderby: ref.sqlorderby
                                        }
                                    };
                                    that.references.push(referenceInfo);
                                    that.pendTables.push(ref.table);
                                    if (rtype == 'SEARCH') {
                                        // Obtaining column list
                                        adRefSearchColumnService.query(
                                            {
                                                idClient: service.loggedIdClient(),
                                                q: 'active=1,idRefTable=' + ref.idRefTable
                                            },
                                            function (datacol) {
                                                referenceInfo.info.searchColumns = [];
                                                // Loading columns
                                                _.each(datacol.content, function (current) {
                                                    referenceInfo.info.searchColumns.push({
                                                        idColumn: current.idColumn,
                                                        name: current.column.name,
                                                        standardName: commonService.removeUnderscores(current.column.name),
                                                        idReference: current.column.idReference,
                                                        idReferenceValue: current.idReferenceValue,
                                                        seqno: current.seqno
                                                    });
                                                });
                                                defered.resolve();
                                            },
                                            function (err) {
                                                $log.log(err);
                                                defered.reject();
                                            }
                                        );
                                    } else {
                                        defered.resolve();
                                    }
                                } else {
                                    defered.resolve();   // defered.reject("Empty array received from server for AdRefTable for reference ID " + idReference + "");
                                }
                            },
                            function (err) {
                                $log.log(err);
                                defered.reject(err);
                            }
                        );
                    } else if (rtype == "BUTTON") {
                        adRefButtonService.query(
                            {
                                idClient: service.loggedIdClient(),
                                q: 'idReference=' + idReference
                            },
                            function (data) {
                                if (data.content.length > 0) {
                                    var ref = data.content[0];
                                    that.references.push({
                                        idReference: idReference,
                                        rtype: rtype,
                                        info: {
                                            idProcess: ref.idProcess,
                                            btype: ref.btype,
                                            jsCode: ref.jsCode,
                                            icon: ref.icon,
                                            showMode: ref.showMode
                                        }
                                    });
                                    if (ref.process)
                                        that.processProcess(ref.process);
                                    defered.resolve();
                                } else {
                                    defered.reject();
                                }
                            },
                            function (err) {
                                $log.log(err);
                                defered.reject(err);
                            }
                        );
                    } else {
                        that.references.push({
                            idReference: idReference,
                            rtype: rtype,
                            base: true
                        });
                        defered.resolve();
                    }
                }
                return promise;
            };

            /**
             * Loads the reference information
             *
             * @param idReference Reference ID
             * @returns {*}
             */
            service.loadReferenceInfo = function (idReference) {
                var defered = $q.defer();
                var promise = defered.promise;
                service.loadReferenceData(idReference)
                    .then(function (rtype) {
                        service.loadReference(idReference, rtype)
                            .then(function () {
                                var loadedRef = service.getReference(idReference);
                                defered.resolve(loadedRef);
                            })
                            .catch(function (err) {
                                defered.reject(err);
                            });
                    })
                    .catch(function (err) {
                        defered.reject(err);
                    });
                return promise;
            };

            /**
             * Process information of process and store in cache
             *
             * @param idProcess Process ID to load
             * @returns {*} Promise
             */
            service.loadProcess = function (idProcess) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var process = _.find(this.processes, function (proc) {
                    return proc.idProcess === idProcess;
                });
                if (process) {
                    defered.resolve(process.info);
                } else {
                    adProcessService.query({
                        idClient: Authentication.loggedIdClient(),
                        q: 'idProcess='+idProcess
                    }, function (data) {
                        if (data.content.length > 0) {
                            var processInfo = data.content[0];
                            var processData = {
                                idProcess: processInfo.idProcess,
                                info: {
                                    idProcess: processInfo.idProcess,
                                    idClient: processInfo.idClient,
                                    name: processInfo.name,
                                    ptype: processInfo.ptype,
                                    uipattern: processInfo.uipattern,
                                    pdf: processInfo.pdf,
                                    excel: processInfo.excel,
                                    html: processInfo.html,
                                    word: processInfo.word,
                                    showinparams: processInfo.showinparams
                                }
                            };
                            service.processes.push(processData);
                            defered.resolve(processData.info);
                        } else {
                            defered.reject("No process with ID: " + idProcess);
                        }
                    }, function (error) {
                        defered.reject(error);
                    });
                }
                return promise;
            };

            /**
             * Process information of process and store in cache
             *
             * @param processInfo Process information
             * @returns {*}
             */
            service.processProcess = function (processInfo) {
                // Check cache
                var process = _.find(this.processes, function (proc) {
                    return proc.idProcess == processInfo.idProcess;
                });
                if (!process) {
                    this.processes.push({
                        idProcess: processInfo.idProcess,
                        info: {
                            idProcess: processInfo.idProcess,
                            idClient: processInfo.idClient,
                            name: processInfo.name,
                            description: processInfo.description,
                            ptype: processInfo.ptype,
                            uipattern: processInfo.uipattern,
                            pdf: processInfo.pdf,
                            excel: processInfo.excel,
                            html: processInfo.html,
                            word: processInfo.word,
                            showinparams: processInfo.showinparams
                        }
                    });
                }
            };

            /**
             * Process table information and store in cache
             *
             * @param tableInfo Table information
             */
            service.processTable = function (tableInfo, forceReload) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var table = _.find(this.tables, function (table) {
                    return table.idTable === tableInfo.idTable;
                });
                if (table && !forceReload) {
                    defered.resolve(table.info);
                } else {
                    this.tables = _.filter(this.tables, function (table) {
                        return table.idTable !== tableInfo.idTable;
                    });
                    var that = this, columns = [];
                    if (tableInfo) {
                        var actions = [];
                        _.each(tableInfo.actions, function (action) {
                            if (action.active) {
                                actions.push({
                                    idTableAction: action.idTableAction,
                                    idTable: action.idTable,
                                    idProcess: action.idProcess,
                                    idWindow: action.idWindow,
                                    atype: action.atype,
                                    name: action.name,
                                    icon: action.icon,
                                    uipattern: action.uipattern,
                                    restCommand: action.restCommand,
                                    url: action.url,
                                    target: action.target,
                                    displaylogic: action.displaylogic,
                                    privilege: action.privilege,
                                    privilegeDesc: action.privilegeDesc
                                });
                            }
                        });

                        _.each(tableInfo.columns, function (column) {
                            if (column.active) {
                                columns.push({
                                    idColumn: column.idColumn,
                                    idTable: column.idTable,
                                    idReference: column.idReference,
                                    idReferenceValue: column.idReferenceValue,
                                    name: column.name,
                                    standardName: commonService.removeUnderscores(column.name),
                                    ctype: column.ctype,
                                    cformat: column.cformat,
                                    csource: column.csource,
                                    primaryKey: column.primaryKey,
                                    mandatory: column.mandatory,
                                    valuemin: column.valuemin,
                                    valuemax: column.valuemax,
                                    lengthMin: column.lengthMin,
                                    lengthMax: column.lengthMax,
                                    valuedefault: column.valuedefault,
                                    readonlylogic: column.readonlylogic,
                                    showinsearch: column.showinsearch,
                                    groupedsearch: column.groupedsearch,
                                    linkParent: column.linkParent,
                                    linkColumn: column.linkColumn,
                                    translatable: column.translatable,
                                    completed: false,
                                    reference: {
                                        name: column.reference.name,
                                        rtype: column.reference.rtype,
                                        base: column.reference.base
                                    }
                                });
                            }
                        });
                        _.each(columns, function (current) {
                            current.isSortColumn = current.idColumn === tableInfo.idSortColumn
                        });
                        var cacheTable = {
                            idTable: tableInfo.idTable,
                            info: {
                                idTable: tableInfo.idTable,
                                name: tableInfo.name,
                                audit: tableInfo.audit,
                                dataOrigin: tableInfo.dataOrigin,
                                isview: tableInfo.isview,
                                exportData: tableInfo.exportData,
                                characteristic: tableInfo.exportData,
                                idSortColumn: tableInfo.idSortColumn,
                                columns: columns,
                                actions: actions,
                                module: {
                                    dbprefix: tableInfo.module.dbprefix ? tableInfo.module.dbprefix : "",
                                    restPath: tableInfo.module.restPath,
                                    idModule: tableInfo.module.idModule
                                }
                            }
                        };
                        this.tables.push(cacheTable);
                        var module = this.getModule(tableInfo.module.idModule);
                        if (!module) {
                            this.modules.push({
                                dbprefix: tableInfo.module.dbprefix ? tableInfo.module.dbprefix : "",
                                restPath: tableInfo.module.restPath,
                                idModule: tableInfo.module.idModule
                            });
                        }
                        _.each(columns, function (column) {
                            if (column.idReferenceValue) {
                                that.loadReference(column.idReferenceValue, column.reference.rtype).then(function () {
                                    column.completed = true;
                                    if (!_.find(columns, function (col) {
                                            return !col.completed
                                        })) {
                                        defered.resolve(cacheTable.info);
                                    }
                                }).catch(function () {
                                    defered.reject();
                                });
                            } else {
                                column.completed = true;
                                if (!_.find(columns, function (col) {
                                        return !col.completed
                                    })) {
                                    defered.resolve(cacheTable.info);
                                }
                            }
                        });
                    }
                }
                return promise;
            };

            /**
             * Process tab information and store in cache
             *
             * @param tabInfo Tab information
             */
            service.processTab = function (tabInfo, forceReload) {
                var defered = $q.defer();
                var promise = defered.promise;

                var fields = [], charts = [], that = this;
                _.each(tabInfo.fields, function (field) {
                    if (field.active) {
                        fields.push({
                            column: {
                                linkParent: field.column.linkParent,
                                linkColumn: field.column.linkColumn
                            },
                            valuemax: field.column.valuemax,
                            valuemin: field.column.valuemin,
                            idColumn: field.idColumn,
                            idFieldGroup: field.idFieldGroup,
                            idField: field.id,
                            name: field.name,
                            caption: field.caption,
                            standardName: commonService.removeUnderscores(field.name),
                            readonly: field.readonly,
                            displayed: field.displayed,
                            showingrid: field.showingrid,
                            startnewrow: field.startnewrow,
                            showinstatusbar: field.showinstatusbar,
                            onchangefunction: field.onchangefunction,
                            displaylogic: field.displaylogic,
                            span: field.span,
                            usedInChild: field.usedInChild,
                            filterRange: field.filterRange,
                            filterType: field.filterType,
                            sortable: field.sortable,
                            gridSeqno: field.gridSeqno,
                            multiSelect: field.multiSelect,
                            placeholder: field.placeholder,
                            seqno: field.seqno,
                            ctype: field.column.ctype,
                            cssClass: _.join(_.split(field.cssClass, ";"), " "),
                            runtime: {}
                        });
                        if (field.idFieldGroup) {
                            that.fieldGroups.push({
                                idFieldGroup: field.idFieldGroup,
                                info: {
                                    idFieldGroup: field.idFieldGroup,
                                    name: field.fieldGroup.name,
                                    columns: field.fieldGroup.columns,
                                    collapsed: field.fieldGroup.collapsed,
                                    displaylogic: field.fieldGroup.displaylogic
                                }
                            });
                        }
                    }
                });
                if (tabInfo.charts) {
                    _.each(tabInfo.charts, function (chart) {
                        if (chart.active) {
                            charts.push({
                                ctype: chart.ctype,
                                displaylogic: chart.displaylogic,
                                extra: chart.extra,
                                idChart: chart.id,
                                idTab: chart.idTab,
                                keyField: chart.keyField,
                                legend1: chart.legend1,
                                legend2: chart.legend2,
                                legend3: chart.legend3,
                                legend4: chart.legend4,
                                legend5: chart.legend5,
                                mode: chart.mode,
                                name: chart.name,
                                prefix: chart.prefix,
                                rowLimit: chart.rowLimit,
                                seqno: chart.seqno,
                                titleX: chart.titleX,
                                titleY: chart.titleY,
                                value1: chart.value1,
                                value2: chart.value2,
                                value3: chart.value3,
                                value4: chart.value4,
                                value5: chart.value5,
                                span: chart.span,
                                startnewrow: chart.startnewrow,
                                runtime: {}
                            });
                        }
                    });
                }
                var tabData = {
                    idTab: tabInfo.idTab,
                    info: {
                        idTab: tabInfo.idTab,
                        idTable: tabInfo.idTable,
                        idParentTable: tabInfo.idParentTable,
                        idWindow: tabInfo.idWindow,
                        name: tabInfo.name,
                        tablevel: tabInfo.tablevel,
                        columns: tabInfo.columns,
                        uipattern: tabInfo.uipattern,
                        ttype: tabInfo.ttype,
                        displaylogic: tabInfo.displaylogic,
                        seqno: tabInfo.seqno,
                        command: tabInfo.command,
                        sqlwhere: tabInfo.sqlwhere,
                        sortColumns: tabInfo.sortColumns,
                        fields: fields,
                        charts: charts,
                        viewMode: tabInfo.viewMode,
                        supportsMultipleViewMode: _.includes(tabInfo.viewMode, 'CARD') && _.includes(tabInfo.viewMode, 'LIST'),
                        viewDefault: tabInfo.viewDefault,
                        groupMode: tabInfo.groupMode
                    }
                };
                if (!_.find(service.tabs, function (current) {
                        return current.idTab === tabData.idTab;
                    })) {
                    service.tabs.push(tabData);
                }
                if (tabInfo.ttype == "CHART") {
                    that.processChartFilters(tabData).then(function () {
                        that.processPendTables().then(function () {
                            defered.resolve(that.getTab(tabInfo.idTab));
                        }).catch(function (err) {
                            defered.reject(err);
                        });
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                } else {
                    if (tabInfo.table) {
                        this.processTable(tabInfo.table, forceReload).then(function () {
                            if (!forceReload) {
                                that.processPendTables().then(function () {
                                    defered.resolve(that.getTab(tabInfo.idTab));
                                }).catch(function (err) {
                                    defered.reject(err);
                                });
                            } else {
                                defered.resolve(that.getTab(tabInfo.idTab));
                            }
                        }).catch(function (err) {
                            defered.reject(err);
                        });
                    } else {
                        that.processPendTables().then(function () {
                            defered.resolve(that.getTab(tabInfo.idTab));
                        }).catch(function (err) {
                            defered.reject(err);
                        });
                    }
                }
                return promise;
            };

            service.processChartFilters = function (tabData) {
                var defered = $q.defer();
                var promise = defered.promise;
                adChartFilterService.query({
                    idClient: Authentication.loggedIdClient(),
                    q: 'idTab=' + tabData.idTab
                }, function (data) {
                    tabData.info.chartFilters = [];
                    _.each(data.content, function (current) {
                        tabData.info.chartFilters.push({
                            caption: current.caption,
                            description: current.description,
                            displayed: current.displayed,
                            extra: current.extra,
                            idChartFilter: current.id,
                            idReference: current.idReference,
                            idReferenceValue: current.idReferenceValue,
                            mandatory: current.mandatory,
                            name: current.name,
                            standardName: commonService.removeUnderscores(current.name),
                            ranged: current.ranged,
                            saveType: current.saveType,
                            seqno: current.seqno,
                            valuedefault: current.valuedefault,
                            valuedefault2: current.valuedefault2,
                            valuemax: current.valuemax,
                            valuemin: current.valuemin,
                            displaylogic: current.displaylogic,
                            reference: {rtype: current.reference.rtype, base: current.reference.base}
                        });
                    });
                    defered.resolve();
                }, function (err) {
                    defered.reject(err);
                });
                return promise;
            };

            /**
             * Process pending tabs
             *
             * @returns {*}
             */
            service.processPendTabs = function (pendTabs) {
                var defered = $q.defer();
                var promise = defered.promise;

                var tabInfo = pendTabs.pop();
                if (tabInfo) {
                    var that = this;
                    this.processTab(tabInfo).then(function () {
                        that.processPendTabs(pendTabs).then(function () {
                            defered.resolve();
                        }).catch(function (err) {
                            defered.reject(err);
                        });
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                } else {
                    defered.resolve();
                }

                return promise;
            };

            /**
             * Process pending tables
             *
             * @returns {*}
             */
            service.processPendTables = function () {
                var defered = $q.defer();
                var promise = defered.promise;

                var tableInfo = this.pendTables.pop();
                if (tableInfo) {
                    var that = this;
                    this.processTable(tableInfo).then(function () {
                        that.processPendTables().then(function () {
                            defered.resolve();
                        }).catch(function (err) {
                            defered.reject(err);
                        });
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                } else {
                    defered.resolve();
                }

                return promise;
            };

            service.processChartColumns = function (idTab, idChart) {
                var defered = $q.defer();
                var promise = defered.promise;

                var tab = _.find(service.tabs, function (tab) {
                    return tab.idTab == idTab && _.find(tab.info.charts, function (chart) {
                        return chart.idChart == idChart;
                    });
                });
                if (tab) {
                    var chart = _.find(tab.info.charts, function (chart) {
                        return chart.idChart == idChart;
                    });
                    if (chart.columns !== undefined)
                        defered.resolve(chart.columns);
                    adChartColumnService.query({
                        idClient: Authentication.loggedIdClient(),
                        q: 'idChart=' + idChart
                    }, function (data) {
                        chart.columns = [];
                        _.each(data.content, function (current) {
                            current.standardName = commonService.removeUnderscores(current.name);
                            current.chartName = chart.name;
                            chart.columns.push(current);
                        });
                        defered.resolve(chart.columns);
                    }, function (err) {
                        defered.reject(err);
                    });
                } else {
                    defered.reject("Tab not processed");
                }

                return promise;
            };

            /**
             * Refresh the cache for a new added item
             *
             * @param tableName Table to check
             * @param id New element ID
             */
            service.refreshCache = function (tableName, id) {
                if (tableName == 'ad_role') {
                    service.loadRole(id);
                }
            };

            /**
             * Returns idClient associated to the selected role
             *
             * @returns {*} idClient
             */
            service.loggedIdClient = function () {
                var selected = _.find(Authentication.roles, function (current) {
                    return current.idRole == localStorage["selected_role"];
                });
                if (selected) {
                    return selected.idClient;
                }
                return null;
            };

            /**
             * Returns idUser for log user
             *
             * @returns {*}
             */
            service.loggedIdUser = function () {
                var loginData = Authentication.getLoginData();
                return loginData ? (loginData.idUserLogged ? loginData.idUserLogged : loginData.idUser) : " ";
            };

            /**
             * Get role data
             *
             * @param idRole Role id
             */
            service.loadRole = function (idRole) {
                var defered = $q.defer();
                var promise = defered.promise;

                // Check cache
                var role = _.find(this.roles, function (role) {
                    return role.idRole == idRole;
                });
                if (role) {
                    defered.resolve(role);
                } else {
                    var that = this;
                    if (!Authentication.isUserLogged()) {
                        defered.reject();
                    } else {
                        adRoleService.query(
                            {
                                idClient: Authentication.loggedIdClient(),
                                q: 'idRole=' + idRole
                            },
                            function (data) {
                                if (data.content.length > 0) {
                                    var resolvedRole = data.content[0];
                                    that.roles.push(resolvedRole);
                                    defered.resolve(resolvedRole);
                                } else {
                                    defered.reject();
                                }
                            },
                            function (err) {
                                $log.log(err);
                                defered.reject(err);
                            }
                        );
                    }
                }

                return promise;
            };

            /**
             * Get window information from cache
             *
             * @param idWindow Window id.
             * @returns {*}
             */
            service.getWindow = function (idWindow) {
                var window = _.find(this.windows, function (win) {
                    return win.idWindow == idWindow;
                });
                return window ? window.info : null;
            };

            /**
             * Get tab information from cache
             *
             * @param idTab Tab id.
             * @returns {*}
             */
            service.getTab = function (idTab) {
                var tab = _.find(service.tabs, function (tab) {
                    return tab.idTab == idTab;
                });
                return tab ? tab.info : null;
            };

            /**
             * Get siblings for the given tab
             *
             * @param idTab Tab id.
             * @returns {*} Siblings
             */
            service.getSiblingTabs = function (idTab) {
                var tab = _.find(service.tabs, function (tab) {
                    return tab.idTab == idTab;
                });
                return tab ? _.filter(service.tabs, function (current) {
                    return (tab.info.idWindow == current.info.idWindow) && ((tab.info.tablevel + 1) == current.info.tablevel);
                }) : new [];
            };

            /**
             * Get table information from cache
             *
             * @param idTable Table id.
             * @returns {*}
             */
            service.getTable = function (idTable) {
                var table = _.find(this.tables, function (table) {
                    return table.idTable == idTable;
                });
                return table ? table.info : null;
            };

            /**
             * Get table information from cache by name
             *
             * @param name Table name
             * @returns {*}
             */
            service.getTableByName = function (name) {
                var table = _.find(this.tables, function (table) {
                    return table.info.name == name;
                });
                return table ? table.info : null;
            };

            /**
             * Get process information from cache
             *
             * @param idProcess Process id.
             * @returns {*}
             */
            service.getProcess = function (idProcess) {
                var process = _.find(this.processes, function (proc) {
                    return proc.idProcess == idProcess;
                });
                if (process && process.info && !process.info.idClient) {
                    process.info.idClient = service.loggedIdClient();
                }
                return process ? process.info : null;
            };

            /**
             * Get reference information from cache
             *
             * @param idReference Reference id.
             * @returns {*}
             */
            service.getReference = function (idReference) {
                return _.find(this.references, function (ref) {
                    return ref.idReference == idReference;
                });
            };

            /**
             * Get module information from cache
             *
             * @param idModule Module id.
             * @returns {*}
             */
            service.getModule = function (idModule) {
                return _.find(this.modules, function (mod) {
                    return mod.idModule == idModule;
                });
            };

            /**
             * Get field group information from cache
             *
             * @param idFieldGroup Field group id.
             * @returns {*}
             */
            service.getFieldGroup = function (idFieldGroup) {
                var fieldGroup = _.find(this.fieldGroups, function (group) {
                    return group.idFieldGroup == idFieldGroup;
                });
                return fieldGroup ? fieldGroup.info : null;
            };

            /**
             * Get detail information of AdRefTable
             *
             * @param idReferenceValue Reference id.
             * @returns {*}
             */
            service.getRefTableInfo = function (idReferenceValue) {
                var refTable = this.getReference(idReferenceValue);
                if (refTable) {
                    var table = this.getTable(refTable.info.idTable);
                    if (table) {
                        var colKey = _.find(table.columns, function (col) {
                            return col.idColumn == refTable.info.idKey;
                        });
                        var colDisplay = _.find(table.columns, function (col) {
                            return col.idColumn == refTable.info.idDisplay;
                        });
                        return {
                            refTable: refTable,
                            table: table,
                            colKey: colKey,
                            colDisplay: colDisplay
                        }
                    }
                }
                return null;
            };

            /**
             * Get a name of reference list value
             *
             * @param idRefList Reference id.
             * @param value Value to find
             * @returns {*}
             */
            service.getListValue = function (idRefList, value) {
                var list = _.find(this.references, function (ref) {
                    return ref.idReference === idRefList && ref.rtype == 'LIST';
                });
                if (list) {
                    var refList = _.find(list.values, function (item) {
                        return item.value === value;
                    });
                    return refList ? refList.name : value;
                }
                return value;
            };

            /**
             * Get value of TABLE or TABLEDIR reference (ForeignKey)
             *
             * @param column Column information
             * @param item Row element
             * @returns {*}
             */
            service.getTableValue = function (column, item) {
                if (!item[column.standardName])
                    return item[column.standardName];
                if (column.refTableField) {
                    if (column.refTableField.fieldValid || column.reference.rtype != 'TABLEDIR') {
                        return column.refTableField.fieldValid && item[column.refTableField.fieldName] ? item[column.refTableField.fieldName][column.refTableField.fieldDisplay] : item[column.standardName];
                    }
                }
                column.refTableField = {
                    fieldValid: false
                };
                var tableInfo = this.getRefTableInfo(column.idReferenceValue);
                if (tableInfo) {
                    if (column.reference.rtype == 'TABLEDIR') {
                        var colInfo = this.getTempTableDir(column.idTable, column.idColumn);
                        if (colInfo) {
                            var value = _.find(colInfo.data.data.content, function (value) {
                                return item[column.standardName] == value[column.standardName];
                            });
                            if (value) {
                                return value[colInfo.data.colDisplay.standardName];
                            }
                        }
                    }
                    var fieldName = column.name;
                    if (fieldName.indexOf('id_') == 0) {
                        fieldName = fieldName.substring(3);
                    } else if (fieldName.indexOf('id') == 0) {
                        fieldName = fieldName.substring(2);
                    }
                    fieldName = commonService.removeUnderscores(fieldName);
                    if (fieldName in item) {
                        column.refTableField.fieldValid = true;
                        column.refTableField.fieldName = fieldName;
                        column.refTableField.fieldDisplay = commonService.removeUnderscores(tableInfo.colDisplay.name);
                        return this.getTableValue(column, item);
                    }
                }

                return item[column.standardName];
            };

            /**
             * Get table information (TABLEDIR) from temporal cache
             *
             * @param idTable Table id.
             * @param idColumn Column id. (optional)
             * @returns {*}
             */
            service.getTempTableDir = function (idTable, idColumn) {
                var table = _.find(this.tempTabledir, function (t) {
                    return t.idTable == idTable;
                });
                if (!table || !idColumn) {
                    return table;
                }
                return _.find(table.references, function (col) {
                    return col.idColumn == idColumn;
                });
            };

            /**
             * Load to cache values of TABLEDIR fields if not have FK field
             *
             * @param table Table
             * @param reset Indicate if clear cache
             * @returns {*}
             */
            service.loadTempTabledir = function (table, reset, restrictColumns, scope) {
                if (restrictColumns && !Array.isArray(restrictColumns)) {
                    $log.warn("Column cache restrict information is not a valid array. Ignoring restriction data.");
                }

                var defered = $q.defer();
                var promise = defered.promise;

                var tabledir = this.getTempTableDir(table.idTable, null);
                if (reset) {
                    if (tabledir) {
                        tabledir.references = [];
                    }
                    _.each(table.columns, function (column) {
                        if (column.reference.rtype == 'TABLEDIR') {
                            column.loadTabledir = false;
                        }
                    });
                }

                var columnsToCheck = table.columns;
                if (restrictColumns) {
                    columnsToCheck = _.filter(columnsToCheck, function (currentColumn) {
                            var found = _.find(restrictColumns, function (current) {
                                return current.standardName === currentColumn.standardName;
                            });
                            return found;
                        }
                    );
                }
                var pendColumn = _.find(columnsToCheck, function (column) {
                    return column.reference.rtype == 'TABLEDIR' && !column.loadTabledir;
                }, this);

                if (pendColumn) {
                    var that = this;
                    if (!pendColumn.referenceValue) {
                        pendColumn.referenceValue = this.getReference(pendColumn.idReferenceValue);
                    }
                    if (!tabledir) {
                        tabledir = {idTable: table.idTable, references: []};
                        that.tempTabledir.push(tabledir);
                    }
                    pendColumn.loadTabledir = true;

                    // Complete table references (Global Parameters) [*param*]
                    if (_.keys(scope.globalParameters).length > 0) {
                        if (pendColumn.reference.rtype === 'TABLE' || pendColumn.reference.rtype === 'TABLEDIR' || pendColumn.reference.rtype === 'SEARCH') {
                            if (!this.isGlobalParameterCompleteConditional(pendColumn.referenceValue.info.sqlwhere)) {
                                pendColumn.referenceValue.info.sqlwhere = this.replaceGlobalParameterConditional(scope.globalParameters, pendColumn.referenceValue.info.sqlwhere);
                            }
                        }
                    }

                    if (this.isCompleteConditional(pendColumn.referenceValue.info.sqlwhere)) {
                        this.loadReferenceTableData(pendColumn.idReferenceValue, pendColumn.referenceValue.info.sqlwhere, scope).then(function (data) {
                            tabledir.references.push({
                                idColumn: pendColumn.idColumn,
                                data: data
                            });
                            that.loadTempTabledir(table, false, restrictColumns, scope).then(function () {
                                defered.resolve();
                            }).catch(function (err) {
                                defered.reject(err);
                            });
                        }).catch(function (err) {
                            defered.reject(err);
                        });
                    } else {
                        var data = null, tableInfo = this.getRefTableInfo(pendColumn.idReferenceValue);
                        if (tableInfo) {
                            data = {
                                colKey: tableInfo.colKey,
                                colDisplay: tableInfo.colDisplay,
                                data: {content: [], totalElements: 0},
                                table: tableInfo.table,
                                idRefTable: tableInfo.refTable.info.idRefTable,
                                sqlwhere: tableInfo.refTable.info.sqlwhere,
                                sqlorderby: tableInfo.refTable.info.sqlorderby
                            };
                        }
                        tabledir.references.push({
                            idColumn: pendColumn.idColumn,
                            data: data
                        });
                        defered.resolve();
                    }
                } else {
                    defered.resolve();
                }

                return promise;
            };

            service.updateLinkedCharts = function (idChart) {
                var defered = $q.defer();
                var promise = defered.promise;
                var tab = _.find(service.tabs, function (tab) {
                    return _.find(tab.charts, function (chart) {
                        return chart.idChart == idChart;
                    });
                });
                var chart = null;
                if (tab) {
                    chart = _.find(tab.charts, function (chart) {
                        return chart.idChart == idChart;
                    });
                }
                if (chart && chart.linkedCharts && chart.linkedCharts.length != 0)
                    defered.resolve(chart.linkedCharts);
                else {
                    adChartLinkedService.query({
                        idClient: service.loggedIdClient(),
                        q: 'idChart=' + idChart
                    }, function (data) {
                        var tab = _.find(service.tabs, function (tab) {
                            return _.find(tab.charts, function (chart) {
                                return chart.idChart == idChart;
                            });
                        });
                        if (tab) {
                            chart = _.find(tab.charts, function (chart) {
                                return chart.idChart == idChart;
                            });
                            chart.linkedCharts = _.map(data.content, function (current) {
                                return current.idLinked;
                            });
                        }
                        defered.resolve(_.map(data.content, function (current) {
                            return current.idLinked;
                        }));
                    }, function (err) {
                        defered.reject(err);
                    });
                }
                return promise;

            };

            /**
             * Remove reference table data from cache
             *
             * @param idReferenceValue
             */
            service.removeReferenceTableData = function (idReferenceValue) {
                if (service.referenceTableDataValues[idReferenceValue]) {
                    delete service.referenceTableDataValues[idReferenceValue];
                }
            };

            /**
             * Clears the cache of the reference table information for the requested scope id
             *
             * @param scopeId {string} Scope ID to clear. If the related reference table id contains no more associated scopes, the information will be cleared
             */
            service.clearReferenceTableDataValue = function (scopeId) {
                var tableIds = _.keys(service.referenceTableDataValues);
                _.each(tableIds, function (tableId) {
                    if (service.referenceTableDataValues[tableId].scopes) {
                        service.referenceTableDataValues[tableId].scopes = _.filter(service.referenceTableDataValues[tableId.scopes],
                            function (current) {
                                return current !== scopeId;
                            });
                        if (service.referenceTableDataValues[tableId].scopes.length === 0) {
                            $log.info("Clearing cache for reference table: " + tableId);
                            delete (service.referenceTableDataValues[tableId]);
                        }
                    }
                });
            };

            /**
             * Load data for referenced table (AdRefTable).
             * IMPORTANT. When calling to loadReferenceTableData, a call to clearReferenceTableDataValue should be done
             * to perform cleanup and release resources so related cache can be cleared
             *
             * @param idReferenceValue {string} Referenced table id.
             * @param sqlwhere {string} Extra condition
             * @param scope {Object} Scope that triggered the call (if known)
             * @param scope.$id {string} ID of the angular scope that triggered the call
             * @returns {*}
             */
            service.loadReferenceTableData = function (idReferenceValue, sqlwhere, scope) {
                var defered = $q.defer();
                var promise = defered.promise;
                var scopeId = scope ? scope.$id : undefined;
                if (service.referenceTableDataValues[idReferenceValue]
                    && service.referenceTableDataValues[idReferenceValue].scopes
                    && service.referenceTableDataValues[idReferenceValue].scopes.length > 0
                    && service.referenceTableDataValues[idReferenceValue].sqlwhere === sqlwhere) {
                    if (scope) {
                        service.referenceTableDataValues[idReferenceValue].scopes = _.union(service.referenceTableDataValues[idReferenceValue].scopes, [scope.$id])
                    }
                    defered.resolve(service.referenceTableDataValues[idReferenceValue].data);
                } else {
                    var tableInfo = this.getRefTableInfo(idReferenceValue);
                    if (tableInfo) {
                        if (tableInfo.table.name === "ad_client" && service.referenceTableDataValues[tableInfo.table.name]) {
                            // For the table AD_CLIENT we are always loading from cache if it exists
                            defered.resolve(service.referenceTableDataValues[tableInfo.table.name].data)
                        } else {
                            var _service = null;
                            try {
                                _service = autogenService.getAutogenService(tableInfo.table);
                            } catch (e) {
                                $log.log(e);
                            }
                            if (_service) {
                                var q = "";
                                if (sqlwhere) {
                                    if (!this.isCompleteConditional(sqlwhere)) {
                                        defered.resolve({
                                            colKey: tableInfo.colKey,
                                            colDisplay: tableInfo.colDisplay,
                                            data: {content: [], totalElements: 0},
                                            table: tableInfo.table,
                                            idRefTable: tableInfo.refTable.info.idRefTable,
                                            sqlwhere: tableInfo.refTable.info.sqlwhere,
                                            sqlorderby: tableInfo.refTable.info.sqlorderby
                                        });
                                        return;
                                    }
                                    q += "$extended$={" + sqlwhere + "}";
                                }
                                var sort = tableInfo.colDisplay.csource === "DATABASE" ? tableInfo.colDisplay.standardName : null;
                                if (tableInfo.refTable.info.sqlorderby) {
                                    sort = tableInfo.refTable.info.sqlorderby;
                                }
                                // Load data from table
                                $log.info("Requesting data from server for table: " + tableInfo.table.name + " with query: " + q);
                                _service.query(
                                    {
                                        idClient: service.loggedIdClient(),
                                        q: q,
                                        sort: '[{ "property":"' + sort + '","direction":"ASC" }]'
                                    },
                                    function (data) {
                                        var resolvedData = {
                                            colKey: tableInfo.colKey,
                                            colDisplay: tableInfo.colDisplay,
                                            data: data,
                                            table: tableInfo.table,
                                            idRefTable: tableInfo.refTable.info.idRefTable,
                                            sqlwhere: tableInfo.refTable.info.sqlwhere,
                                            sqlorderby: tableInfo.refTable.info.sqlorderby
                                        };
                                        if (tableInfo.table.name === "ad_client") {
                                            // Table AD_CLIENT is a special case, it will always be stored in cache once loaded
                                            service.referenceTableDataValues[tableInfo.table.name] = {
                                                data: resolvedData
                                            };
                                        } else if (scopeId) {
                                            if (!service.referenceTableDataValues[idReferenceValue])
                                                service.referenceTableDataValues[idReferenceValue] = {
                                                    scopes: [],
                                                    data: undefined,
                                                    sqlwhere: undefined
                                                };
                                            // Storing the data in the cache of the scope
                                            service.referenceTableDataValues[idReferenceValue].scopes.push(scopeId);
                                            service.referenceTableDataValues[idReferenceValue].data = resolvedData;
                                            service.referenceTableDataValues[idReferenceValue].sqlwhere = sqlwhere;
                                        }
                                        defered.resolve(resolvedData);
                                    },
                                    function (err) {
                                        // TODO: Error handling
                                        defered.reject(err);
                                    }
                                );
                            } else {
                                // TODO: Error handling
                                defered.reject();
                            }
                        }
                    } else {
                        // TODO: Error handling
                        defered.reject();
                    }
                }
                return promise;
            };

            /**
             * Replace field value in conditional string, for example:
             *
             *    SQL condition: base = 1 and rtype = '{idReference}'
             *
             * @param field Field information
             * @param value Current value of field
             * @param conditional Conditional string
             * @returns {*}
             */
            service.replaceSQLConditional = function (field, value, conditional) {
                if (field) {
                    var pattern;
                    if (field.column) {
                        pattern = new RegExp("\\{" + field.column.standardName + "\\}", "g");
                        if (pattern.test(conditional)) {
                            return conditional.replace(pattern, value);
                        }
                        pattern = new RegExp("\\{" + field.column.standardName + "\\.(.*?)\\}", "");
                        if (pattern.test(conditional)) {
                            var values = conditional.match(pattern);
                            conditional = conditional.replace(pattern, "#(" + field.column.idReferenceValue + ";" + value + ")." + values[1] + "#");
                            if (this.isFieldInConditional(field, conditional)) {
                                return this.replaceSQLConditional(field, value, conditional);
                            } else {
                                return conditional;
                            }
                        }
                    } else if (field.ptype) {
                        pattern = new RegExp("\\{" + field.name + "\\}", "g");
                        return conditional.replace(pattern, value);
                    }
                }
                return conditional;
            };

            /**
             * Replace field value in conditional string, for example:
             *
             *    Javascript condition:
             *          '{idReference}' === 'LIST' || '{idReference}' === 'TABLE' || '{idReference}' === 'TABLEDIR'
             *          @idClient@
             *
             * @param field Field information
             * @param value Current value of field
             * @param conditional Conditional string
             * @param itemValue Item object value
             * @returns {*}
             */
            service.replaceJSConditional = function (field, value, conditional, itemValue) {
                if (!conditional) {
                    return conditional;
                }
                if (field) {
                    var pattern;
                    if (field.column) {
                        pattern = new RegExp("\\{" + field.column.standardName + "\\}", "g");
                        if (pattern.test(conditional)) {
                            return conditional.replace(pattern, value);
                        }
                    } else if (field.ptype) {
                        pattern = new RegExp("\\{" + field.name + "\\}", "g");
                        return conditional.replace(pattern, value);
                    }
                }
                // Predefined replace patterns
                pattern = new RegExp("\\@(.*?)\\@", "g");
                while (pattern.test(conditional)) {
                    var values = conditional.match(pattern);
                    _.each(values, function (val) {
                        if (val.length > 2) {
                            var item = val.substr(1, val.length - 2);
                            var replacePattern = new RegExp("\\@" + item + "\\@");
                            var value;
                            switch (item) {
                                case "idClient":
                                    value = service.loggedIdClient();
                                    break;
                                case "idUserLogged":
                                    value = service.loggedIdUser();
                                    break;
                                case "today":
                                    value = new Date().getTime();
                                    break;
                                case "yesterday":
                                    value = moment().subtract(1, 'days').toDate().getTime();
                                    break;
                                case "tomorrow":
                                    value = moment().add(1, 'days').toDate().getTime();
                                    break;
                                case "year":
                                    value = moment().year();
                                    break;
                                case "month":
                                    value = moment().month() + 1;
                                    break;
                                case "week":
                                    value = moment().week();
                                    break;
                                case "firstMonthDay":
                                    var today = new Date();
                                    value = moment([today.getFullYear(), today.getMonth()]).toDate().getTime();
                                    break;
                                case "lastMonthDay":
                                    value = moment().endOf('month').toDate().getTime();
                                    break;
                                case "firstYearDay":
                                    value = moment([new Date().getFullYear()]).toDate().getTime();
                                    break;
                                case "lastYearDay":
                                    value = moment().endOf('year').toDate().getTime();
                                    break;
                                case "true":
                                    value = true;
                                    break;
                                case "false":
                                    value = false;
                                    break;
                                default:
                                    if (field && field[item]) {
                                        value = field[item];
                                    } else {
                                        value = item;
                                        if (item.indexOf('_daysago') > 0) {
                                            var itemParts = item.split('_');
                                            if (itemParts.length == 2) {
                                                value = moment().subtract(parseInt(itemParts[0]), 'days').toDate().getTime();
                                            }
                                        }
                                        if (item.indexOf('nextdays_') == 0) {
                                            var itemParts = item.split('_');
                                            if (itemParts.length == 2) {
                                                value = moment().add(parseInt(itemParts[1]), 'days').toDate().getTime();
                                            }
                                        }
                                    }
                            }
                            conditional = conditional.replace(replacePattern, value);
                        }
                    }, this);
                }
                // Permissions
                pattern = new RegExp("\\#(.*?)\\#", "g");
                while (pattern.test(conditional)) {
                    var permissions = conditional.match(pattern);
                    _.each(permissions, function (val) {
                        if (val.length > 2) {
                            var item = val.substr(1, val.length - 2);
                            var replacePattern = new RegExp("\\#" + item + "\\#");
                            conditional = conditional.replace(replacePattern, $rootScope.hasPermission(item));
                        }
                    }, this);
                }
                // Item fields
                if (itemValue) {
                    pattern = new RegExp("\\{(.*?)\\}", "g");
                    while (pattern.test(conditional)) {
                        var values = conditional.match(pattern);
                        _.each(values, function (val) {
                            if (val.length > 2) {
                                var item = val.substr(1, val.length - 2);
                                var replacePattern = new RegExp("\\{" + item + "\\}");
                                conditional = conditional.replace(replacePattern, itemValue[item]);
                            }
                        }, this);
                    }
                }

                return conditional;
            };

            service.replaceJSItemConditional = function (item, conditional) {
                var result = service.replaceJSConditional(null, null, conditional);
                var pattern = new RegExp("\\{(.*?)\\}", "g");
                if (pattern.test(conditional)) {
                    var values = conditional.match(pattern);
                    _.each(values, function (value) {
                        var fieldName = value.substr(1, value.length - 2);
                        var replacePattern = new RegExp("\\{" + fieldName + "\\}");
                        result = result.replace(replacePattern, item[fieldName]);
                    });
                }
                return result;
            };

            service.replaceGlobalParameterConditional = function (item, conditional) {
                var result = conditional;
                var pattern = new RegExp("\\*(.*?)\\*", "g");
                if (pattern.test(conditional)) {
                    var values = conditional.match(pattern);
                    _.each(values, function (value) {
                        var fieldName = value.substr(1, value.length - 2);
                        var replacePattern = new RegExp("\\*" + fieldName + "\\*");
                        result = result.replace(replacePattern, item[fieldName]);
                    });
                }
                return result;
            };

            /**
             * Verify if conditional string have replace all parameters
             *
             * @param conditional Conditional string
             * @returns {boolean}
             */
            service.isCompleteConditional = function (conditional) {
                return !/\{(.*?)\}/.test(conditional) && !/\@(.*?)\@/.test(conditional);
            };

            /**
             * Verify if conditional string have replace all parameters
             *
             * @param conditional Conditional string
             * @returns {boolean}
             */
            service.isGlobalParameterCompleteConditional = function (conditional) {
                return !/\*(.*?)\*/.test(conditional);
            };

            /**
             * Verify if conditional have a reference to field
             *
             * @param field Field information
             * @param conditional Conditional string
             * @returns {boolean}
             */
            service.isFieldInConditional = function (field, conditional) {
                if (field) {
                    var pattern;
                    if (field.column) {
                        pattern = new RegExp("\\{" + field.column.standardName + "\\}|\\{" + field.column.standardName + "\.(.*?)\\}", "");
                        return pattern.test(conditional);
                    }
                    if (field.ptype) {
                        pattern = new RegExp("\\{" + field.name + "\\}", "");
                        return pattern.test(conditional);
                    }
                }
                return false;
            };

            /**
             * Stores the item being edited
             *
             * @param idTab Form's tab id
             * @param item  Item to store
             * @param fieldValues Form fields
             * @param tab Form related tab information
             */
            service.storeFormInfo = function (idTab, item, fieldValues, tab) {
                var elementToAdd = {item: item, fieldValue: fieldValues, tab: tab};
                if (service.editingItemWithId[idTab] === undefined || service.editingItemWithId[idTab] === null) {
                    service.editingItemWithId[idTab] = [];
                }
                if (item.id) {
                    service.editingItemWithId[idTab][item.id] = elementToAdd;
                }
                service.editingItem[idTab] = elementToAdd;
            };

            service.storeTabItems = function (idTab, items) {
                service.editingSiblings[idTab] = items;
            };

            service.getTabItems = function (idTab, items) {
                return service.editingSiblings[idTab];
            };

            /**
             * Clears the item being edited
             *
             * @param idTab Form's tab id
             */
            service.clearFormInfo = function (idTab) {
                var formData = service.editingItem[idTab];
                if (!formData) return;
                _.each(formData.tab.fields, function (current) {
                    current.runtime = {};
                });
                service.editingSiblings[idTab] = null;
                service.editingItem[idTab] = null;
                service.editingItemWithId[idTab] = null;
                var tabIds = _.keys(service.editingItem);
                _.each(tabIds, function (current) {
                    if (service.editingItem[current] != null && service.editingItem[current].tab && service.editingItem[current].tab.idWindow == formData.tab.idWindow && service.editingItem[current].tab.tablevel > formData.tab.tablevel) {
                        // We need to clear also the form data of the child forms
                        _.each(service.editingItem[current].tab.fields, function (field) {
                            field.runtime = {};
                        });
                        tabManagerService.removeSubItemEdited({
                            windowGeneratorId: service.editingItem[current].tab.idWindow,
                            tabGeneratorId: service.editingItem[current].tab.idTab
                        });
                        service.editingItem[current] = null;
                    }
                });
            };

            /**
             * Returns the item being edited that is associated to the form
             *
             * @param idTab Form's tab id
             * @returns {*} Item being edited or null if the item was not present
             */
            service.getFormInfo = function (idTab, itemId) {
                if (itemId && service.editingItemWithId[idTab]) {
                    return service.editingItemWithId[idTab][itemId];
                }
                return service.editingItem[idTab];
            };


            service.getMessages = function () {
                var defered = $q.defer();
                var promise = defered.promise;
                // Load messages
                adTranslationService.messages({
                    idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : "",
                    idClient: service.loggedIdClient() ? service.loggedIdClient() : "%%"
                }, function (data) {
                    if (data.success) {
                        $rootScope.tranlationsField = data.properties.fields;
                        $rootScope.tranlationsFieldDescriptions = data.properties.fieldDescriptions;
                        $rootScope.tranlationsMessages = data.properties.messages;
                        $rootScope.translationChartFilterParams = data.properties.chartFilterParams;
                        $rootScope.translationChartColumns = data.properties.chartColumns;
                        $rootScope.tranlationsProcessParams = data.properties.processParams;
                        defered.resolve();
                    } else {
                        defered.reject(data);
                    }
                }, function (error) {
                    defered.reject(error);
                });
                return promise;
            };

            service.loadLanguages = function () {
                var defered = $q.defer();
                var promise = defered.promise;
                if (service.loggedIdClient() && (!$rootScope.languages || $rootScope.languages.length == 0)) {
                    // Load languages
                    adClientLanguageService.list({
                        idClient: service.loggedIdClient()
                    }, function (data) {
                        var exists = false;
                        _.each(data.content, function (current) {
                            if (Authentication.isUserLogged() && localStorage["selected_language"] == current.id) {
                                $rootScope.selectedLanguage = current;
                                exists = true;
                            }
                        });

                        if (data.content.length == 0) {
                            Language.getLanguages(function (result) {
                                $rootScope.selectedLanguage = result[0];
                                $rootScope.languages = result;
                                Language.getLang($rootScope.selectedLanguage.countrycode, function (data) {
                                    $rootScope.lang = data;
                                    $rootScope.$broadcast('GLOBAL_GET_WORD_CREATED', '');
                                });
                            })
                        } else {
                            $rootScope.languages = data.content;
                        }
                        service.getMessages().then(function () {
                            defered.resolve();
                        }).catch(function (error) {
                            defered.reject(error);
                        });
                    }, function (error) {
                        defered.reject(error);
                    });
                } else {
                    defered.resolve();
                }
                return promise;
            };

            return service;
        }]);
});
