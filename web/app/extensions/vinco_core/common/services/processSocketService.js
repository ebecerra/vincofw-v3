define(['app'], function (app) {
    "use strict";

    return app.registerFactory('processSocketService', function ($q, $timeout, $rootScope, $log) {
        var service = {}, socket = {
            client: null,
            stomp: null
        };
        service.eventReceivedCallback = [];
        service.DISCONNECT_TIMEOUT = 2000;
        service.RECONNECT_TIMEOUT = 5000;
        service.initialized = false;
        // URL del Socket
        var url = document.location.protocol + '//' + document.location.host + REST_HOST_PATH + 'websocket';

        service.SOCKET_URL = url;
        service.EVENT_TOPIC = "/topic/process.event";

        var getEvent = function (data) {
            var message = JSON.parse(data);
            var log = 'Event = { success: ' + message.success + ', finish: ' + message.finish;
            if (message.adProcessLog) {
                log += ', type: ' + message.adProcessLog.ltype + ', log: ' + message.adProcessLog.log;
            }
            log += " }";
            $log.log(log);
            return message;
        };

        var registerEvent = function (event) {
            var eventReceived = _.find(service.eventReceivedCallback, function (c) {
                return c.idProcess == event.idProcess;
            });
            if (eventReceived) {
                eventReceived.callback.apply(service, [event]);
                if (event.finish) {
                    $timeout(function () {
                        // Remove from listeners
                        service.eventReceivedCallback = _.without(service.eventReceivedCallback, _.find(service.eventReceivedCallback, {
                            idProcess: event.idProcess
                        }));
                    }, service.DISCONNECT_TIMEOUT);
                }
            }
        };

        var startListener = function () {
            socket.stomp.subscribe(service.EVENT_TOPIC, function (data) {
                var event = getEvent(data.body);
                registerEvent(event);
            });
        };

        var initialize = function (callback) {
            if (!service.initialized) {
                $log.info("Attempting connection to web socket");
                socket.client = new SockJS(service.SOCKET_URL, undefined, {debug: false});
                socket.stomp = Stomp.over(socket.client);
                socket.stomp.connect(
                    {},
                    function () {
                        startListener();
                        if (callback)
                            callback();
                    },
                    service.reconnect
                );
                socket.stomp.onclose = service.reconnect;
                service.initialized = true;
            } else if (!socket.stomp.connected) {
                $log.info("Attempting re-connection to web socket");
                socket.stomp.connect(
                    {},
                    function () {
                        startListener();
                        if (callback)
                            callback();
                    },
                    service.reconnect
                );
            } else {
                if (callback)
                    callback();
            }
        };

        var stop = function () {
            if (socket.stomp && socket.stomp.connected) {
                socket.stomp.disconnect();
                socket.stomp.unsubscribe(service.EVENT_TOPIC);
            }
            service.initialized = false;
        };

        service.reconnect = function (msg) {
            $log.error("Error accessing web socket: " + msg);
            service.initialized = false;
            $timeout(function () {
                $log.warn("Attempting reconnection to web socket");
                initialize();
            }, service.RECONNECT_TIMEOUT);
        };

        service.initialize = function (idProcess, callback) {
            var defered = $q.defer();
            var promise = defered.promise;

            if (idProcess) {
                var eventReceived = _.find(service.eventReceivedCallback, function (c) {
                    return c.idProcess == idProcess;
                });
                if (!eventReceived) {
                    service.eventReceivedCallback.push({ idProcess: idProcess, callback: callback });
                }
            } else {
                service.eventReceivedCallback = [];
            }
            initialize(function() {
                defered.resolve();
            });

            return promise;
        };

        service.stop = stop;

        return service;

    });
});

