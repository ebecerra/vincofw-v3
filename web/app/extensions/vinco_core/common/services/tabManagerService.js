/**
 * Service to manage the application tabs
 */
define(['app'], function (app) {
    "use strict";

    return app.registerFactory('tabManagerService', [
        '$resource', '$rootScope', '$state', '$location', '$log', 'adProcessService', '$templateRequest',
        'Authentication', '$compile', '$timeout', '$injector',
        function (
            $resource, $rootScope, $state, $location, $log, adProcessService, $templateRequest, Authentication,
            $compile, $timeout, $injector) {
            var service = {};
            var prefixFilterStorage = "TAB_FILTERS_";

            service.activeTabId = null;

            /**
             * Clears all tab information
             */
            service.clearTabs = function () {
                service.tabs = [];
                // The subitem being edited. An object with the following properties:
                // {
                //    windowGeneratorId: The window to where the item belongs,
                //    tabGeneratorId: The tab to where the item belongs,
                //    editingItemId: Item id
                // }
                service.subitems = [];
                // The cached tab information for regular Tabs
                service.tabItems = [];
                // The cached tab information for autogen Tabs
                service.autogenTabItems = [];
                // The tab headers
                service.tabHeaders = {};
                // The state params for autogen Tabs
                service.autogenStateParams = {};
                // The state params for the currently selected tab
                service.currentStateParams = {};
                // The initial form item as loaded from server
                service.initialFormItems = {};

                service.activeIndex = -1;
                service.isClosingTabs = false;
            };

            service.clearTabs();

            /**
             * Goes the the state defined by the tab
             *
             * @param tab Tab to analize
             */
            service.go = function (tab) {
                var ignore = false;
                if (tab.route.startsWith('app.autogen')) {
                    service.currentStateParams[$state.params.idWindow] = $state.params;
                    if (service.autogenStateParams[tab.id]) {
                        tab.params = service.currentStateParams[tab.id];
                    }
                    else {
                        if (tab.route == "app.autogen.process") {
                            tab.params = {idProcess: tab.id};
                        } else {
                            tab.params = {idWindow: tab.id};
                        }
                    }
                }
                if (!ignore) {
                    if (service.activeTabId != tab.id) {
                        $rootScope.$broadcast(app.eventDefinitions.GLOBAL_TAB_SELECTED, {idTab: tab.id});
                    }
                    service.activeTabId = tab.id;
                    $state.go(tab.route, tab.params);
                }
            };

            /**
             * Stores the state params for the current tab
             */
            service.setAutogenStateParams = function () {
                service.currentStateParams[$state.params.idWindow] = $state.params;
            };

            /**
             * Stores the state params for the current tab
             */
            service.setAutogenProcessStateParams = function () {
                service.currentStateParams[$state.params.idProcess] = $state.params;
            };

            /**
             * Defines if the tab should be active
             *
             * @param route Route to analize
             * @returns {*}
             */
            service.active = function (route) {
                return $state.is(route);
            };

            /**
             * Deletes a tab from the list
             *
             * @param index Tab index
             */
            service.removeTab = function (index) {
                var tab = service.tabs[index];
                service.tabs = _.without(service.tabs, service.tabs[index]);
                if (tab) { // Remove cache information of the tab
                    // Removing information about elements in editing status
                    service.subitems = _.filter(service.subitems, function (current) {
                        return (current.windowGeneratorId != tab.id );
                    });
                    // Clearing all information about the tab
                    var storedTabData = _.keys(service.autogenTabItems);
                    _.each(storedTabData, function (idTab) {
                        if (service.autogenTabItems[idTab] && service.autogenTabItems[idTab].idWindow == tab.id) {
                            service.autogenTabItems = _.omit(service.autogenTabItems, idTab);
                        }
                    });
                    // Globally notify that the tab was closed
                    $rootScope.$broadcast(app.eventDefinitions.GLOBAL_TAB_CLOSED, { idWindow: tab.id });
                }
                if (service.tabs.length == 0) {
                    service.isClosingTabs = true;
                    service.activeTabId = null;
                    $location.path("/home");
                }
            };

            /**
             * Remove all tabs
             */
            service.removeAllTabs = function () {
                while (service.tabs.length > 0) {
                    this.removeTab(0);
                }
            };

            /**
             * Deletes the selected tab from the list
             */
            service.removeActive = function () {
                if (service.activeIndex >= 0) {
                    service.removeTab(service.activeIndex);
                }
            };

            /**
             * Adds a tab to the tab list
             *
             * @param tab Tab to add
             */
            service.addTab = function (tab) {
                _.each(service.tabs, function (current) {
                    current.active = false;
                });
                service.tabs.push(tab);
                service.activeIndex = service.tabs.length - 1;
            };

            /**
             * Change tab heading
             *
             * @param tabId Tab to add
             * @param heading New heading
             */
            service.setTabHeading = function (tabId, heading) {
                _.each(service.tabs, function (current) {
                    if (current.id == tabId) {
                        current.heading = heading;
                    }
                });
                service.tabHeaders[tabId] = heading;
            };

            /**
             * Go to a previous state
             *
             * @param state         Previous state
             * @param stateParams   State params
             */
            service.goBackContent = function (state, stateParams) {
                service.tabItems[$state.$current.name] = null;
                var keys = _.keys($state.$current.includes);
                var baseRoute = "";
                if (keys.length > 3) {
                    baseRoute = keys[2];
                    _.each(service.tabs, function (current) {
                        if (current.baseRoute == baseRoute) {
                            current.item = null;
                        }
                    });
                }
                $state.go(state, stateParams);
            };

            /**
             * Opens an New Item tab
             *
             * @param idWindow Window ID where the tab belongs
             * @param item Item information to pass to the tab
             * @param initialData Initial information to show in the edit window
             */
            service.openNewItemTab = function (idWindow, item, initialData) {
                service.setTabContent({
                    initialData: initialData
                }, idWindow);
                service.goAutogenForm(idWindow, item, null, null, null, false, 'app.autogen.table');
            };

            /**
             * Opens an Edit Item tab
             *
             * @param idWindow Window ID where the tab belongs
             * @param idTab ID of the editing tab
             * @param item Item to edit
             */
            service.openExistingItemTab = function (idWindow, idTab, item) {
                var cacheService = $injector.get('cacheService');
                cacheService.clearFormInfo(idTab);
                cacheService.storeTabItems(idTab, [item]);
                service.goAutogenForm(idWindow, item, item.id, idTab, null, false, 'app.autogen.table');
            };

            /**
             * Go to the following state and store current state params
             *
             * @param idWindow
             * @param item
             * @param currentItemId
             * @param currentIdTab
             * @param parentItem
             * @param multiple
             * @param state
             */
            service.goAutogenForm = function (idWindow, item, currentItemId, currentIdTab, parentItem, multiple, state) {
                if (multiple === undefined || multiple === null) {
                    multiple = false;
                }
                if (!service.autogenStateParams[idWindow]) {
                    service.autogenStateParams[idWindow] = [];
                }
                service.autogenStateParams[idWindow].push({
                    prevState: state ? state : $state.$current.name,
                    params: {
                        idWindow: idWindow,
                        itemId: currentItemId,
                        idTab: currentIdTab,
                        isMultiple: multiple
                    },
                    parentItem: parentItem,
                    item: item
                });
                $state.go('app.autogen.form', {
                    idWindow: idWindow,
                    itemId: item.id,
                    idTab: item.idTab,
                    isMultiple: multiple
                });
            };

            /**
             * Go to a previous state for autogen tabs
             *
             * @param idWindow Tab associated window id
             */
            service.goAutogenBackContent = function (idWindow) {
                if (service.autogenStateParams[idWindow]) {
                    var prevData = _.last(service.autogenStateParams[idWindow]);
                    service.autogenStateParams[idWindow].pop();

                    $state.go(prevData.prevState, prevData.params);
                }
            };

            /**
             * Gets the stored tab content
             *
             * @param defaultItem Default item to return if no tab content is stored
             * @param autogenTabId The Autogen tab Window ID
             * @returns {*} Tab content stored
             */
            service.getTabContent = function (defaultItem, autogenTabId) {
                if (autogenTabId) {
                    if (service.autogenTabItems[autogenTabId]) {
                        return service.autogenTabItems[autogenTabId];
                    }
                    if (localStorage[prefixFilterStorage + autogenTabId])
                        return JSON.parse(localStorage[prefixFilterStorage + autogenTabId]);
                    return defaultItem;
                } else {
                    if (service.tabItems[$state.$current.name]) {
                        return service.tabItems[$state.$current.name];
                    }
                    return defaultItem;
                }
            };

            /**
             * Sets tab content to be stored for future use
             *
             * @param item Tab content to store
             * @param autogenTabId The Autogen tab ID
             */
            service.setTabContent = function (item, autogenTabId) {
                if (autogenTabId) {
                    localStorage[prefixFilterStorage + autogenTabId] = JSON.stringify(item.initialData ? {} : item);
                    service.autogenTabItems[autogenTabId] = item;
                } else {
                    service.tabItems[$state.$current.name] = item;
                    var keys = _.keys($state.$current.includes);
                    var baseRoute = "";
                    if (keys.length > 3) {
                        baseRoute = keys[2];
                        if (service.tabs.length == 0) {
                            var tab = {
                                heading: '',
                                route: $state.$current.name,
                                baseRoute: baseRoute,
                                active: true,
                                params: {}
                            };
                            $rootScope.$on('GLOBAL_GET_WORD_CREATED', function () {
                                tab.heading = $rootScope.getWord(baseRoute);
                            });
                            service.addTab(tab);
                        }
                        _.each(service.tabs, function (current) {
                            if (current.baseRoute == baseRoute) {
                                current.item = item;
                            }
                        });
                    }
                }
            };

            /**
             * Clear tab initial content
             */
            service.clearTabInitialData = function () {
                var keys = _.keys(service.autogenTabItems);
                _.each(keys, function (key) {
                    delete service.autogenTabItems[key].initialData;
                });
            };

            /**
             * Changes the active tab according to the angular state
             */
            service.changeTab = function (params) {
                // $templateCache.removeAll();
                var exists = false;
                var currentActive = _.find(service.tabs, function (current) {
                        return current.active;
                    }
                );
                var differentRoute = false;
                _.each(service.tabs, function (current, index) {
                    if ($state.$current.name.startsWith('app.autogen')) {
                        if ($state.$current.name == 'app.autogen.process') {
                            current.active = ($state.params.idProcess === current.id);
                        } else {
                            current.active = ($state.params.idWindow === current.id);
                        }
                    } else {
                        if (!$state.$current.name.startsWith('app.autogen')) {
                            // We are dealing with a custom tab
                            current.active = current.id === $state.params.value;
                        } else {
                            current.active = ($state.$current.includes[current.baseRoute]);
                        }
                    }
                    if (current.active) {
                        differentRoute = !$state.$current.name.startsWith(current.route);
                        if ($state.$current.name.startsWith('app.autogen')) {
                            if ($state.$current.name == 'app.autogen.process') {
                                if ($state.params.idProcess === current.id) {
                                    exists = true;
                                    service.activeIndex = index;
                                }
                            } else if ($state.params.idWindow === current.id) {
                                exists = true;
                                service.activeIndex = index;
                            }
                        } else {
                            exists = true;
                            service.activeIndex = index;
                            current.params = $state.params;
                        }
                        current.route = $state.$current.name;
                    }
                });
                var keys = _.keys($state.$current.includes);
                var baseRoute = "";
                if (keys.length > 3) {
                    baseRoute = keys[2];
                    if (!exists) {
                        var tab = {
                            heading: $rootScope.getWord(baseRoute),
                            route: $state.$current.name,
                            baseRoute: baseRoute,
                            active: true,
                            params: {}
                        };
                        // This is to set header of menus of action type WINDOW
                        // Those menus correspong to autogenerated forms
                        if ($state.$current.name.startsWith('app.autogen')) {
                            if ($state.$current.name === 'app.autogen.process') {
                                tab.id = params.currentScope.$stateParams.idProcess;
                            } else {
                                tab.id = params.currentScope.$stateParams.idWindow;
                            }
                            tab.isAutogen = true;
                            if (service.tabHeaders[tab.id]) {
                                tab.heading = service.tabHeaders[tab.id];
                            }
                        }
                        else {
                            // We are dealing with a custom tab

                            //  For the tab heading to work, the defined angular state should include a parameter named "value"
                            //  For example
                            //  .state('app.custom.state', {
                            //      url: '/custom_state',
                            //          ...
                            //      params: {
                            //          value: null,
                            //          heading: null
                            //      }
                            // },
                            if ($state.params.heading)
                                tab.heading = $state.params.heading;
                            else
                                tab.heading = $rootScope.getWord($state.$current.name);
                            if ($state.params.value)
                                tab.params.value = $state.params.value;
                            if (!tab.id)
                                tab.id = $state.params.value;
                        }
                        service.addTab(tab);
                    }
                    if (currentActive && currentActive.baseRoute == baseRoute && differentRoute) {
                        currentActive.item = null;
                    }
                }
            };

            /**
             * Adds the element to the cached subitems
             *
             * @param element Element to be stored
             */
            service.setSubItemEdited = function (element) {
                var result = _.find(service.subitems, element);
                if (typeof(result) === 'undefined') {
                    service.subitems.push(element);
                } else {
                    result.editingItemId = element.editingItemId;
                }
            };

            /**
             * Adds the element to the cached subitems
             *
             * @param element Element to be stored
             */
            service.getSubItemEdited = function (element) {
                var result = _.find(service.subitems, element);
                if (typeof(result) === 'undefined') {
                    return null;
                }
                return result;
            };

            /**
             * Removes the element from cached subitems
             *
             * @param element Element to be stored
             */
            service.removeSubItemEdited = function (element) {
                service.subitems = _.filter(service.subitems, function (current) {
                    return (!(current.windowGeneratorId == element.windowGeneratorId && current.tabGeneratorId == element.tabGeneratorId ));
                });
            };

            service.setInitialFormItem = function (idTab, item) {
                service.initialFormItems[idTab] = item;
            };

            service.getInitialFormItem = function (idTab) {
                return service.initialFormItems[idTab];
            };

            service.analyzeProcess = function (menuData) {
                if (menuData.action != "PROCESS") return;
                $log.info(menuData);

                adProcessService.query({
                    idClient: Authentication.loggedIdClient(),
                    q: "idProcess=" + menuData.idProcess
                }, function (data) {
                    if (data.content.length > 0) {
                        var process = data.content[0];
                        if (process.uipattern != "DIALOG") {
                            $state.go('app.autogen.process', {idProcess: menuData.idProcess});
                        } else {
                            var $scope = $rootScope.$new(true);
                            var processDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.dialog.html?v=' + FW_VERSION;
                            $scope.processDialogId = menuData.idProcess;
                            $scope.dialogProcess = process;
                            $scope.selectedProcess = process;
                            $scope.item = {};
                            $scope.processLogs = [];

                            /**
                             * Hides the process dialog
                             */
                            $scope.hideProcessDialog = function () {
                                $('#' + $scope.processDialogId).modal('hide');
                            };

                            $templateRequest(processDialogTemplate).then(function (html) {
                                var template = angular.element(html);
                                // element.append(template);
                                var html = $compile(template)($scope);
                                $("#customModalDialog").append(html);
                                $timeout(function () {
                                    $('#' + $scope.processDialogId).modal();
                                });
                            });
                        }
                    }
                }, function (error) {
                    $log.error(error);
                });
            };

            return service;
        }]);
});

