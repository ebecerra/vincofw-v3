/**
 * This service handles the resolution of reference values
 */
define(['app'], function (app) {
    "use strict";

    return app.registerFactory('referenceResolverService', function ($resource, $rootScope, $q, $log, cacheService) {
        var service = {};

        /**
         * Loads the reference's key and display information
         *
         * @param itemId If passed, requests the item that matches the required itemId according to
         *              the reference information
         * @param idReferenceValue The reference value
         */
        service.resolveTableData = function (itemId, idReferenceValue, scope) {
            var defered = $q.defer();
            var promise = defered.promise;
            cacheService.loadReferenceData(idReferenceValue).then(function () {
                var refTable = cacheService.getReference(idReferenceValue);
                if (refTable) {
                    var sqlwhere = cacheService.replaceJSConditional(null, null, refTable.info.sqlwhere)
                    if (cacheService.isCompleteConditional(sqlwhere)) {
                        // var sqlwhere = refTable.info.sqlwhere;
                        var table = cacheService.getTable(refTable.info.idTable);
                        cacheService.loadReferenceData(idReferenceValue).then(function () {
                            cacheService.processPendTables().then(function () {
                                var tableInfo = cacheService.getRefTableInfo(idReferenceValue);
                                var visualField = {
                                    key: tableInfo.colKey.standardName,
                                    display: tableInfo.colDisplay.standardName,
                                    displaySource: tableInfo.colDisplay.csource,
                                    table: tableInfo.table,
                                    sqlwhere: tableInfo.refTable.info.sqlwhere,
                                    sqlorderby: tableInfo.refTable.info.sqlorderby,
                                    displayCaption: tableInfo.colDisplay.name
                                };
                                if (itemId) {
                                    // Obtaining value of initial item

                                    //
                                    // TODO: Eduardo
                                    //
                                    // No entiendo exactamente que hace este código, al seleccionar un elemento de la componente select2,
                                    // se llama al servidor pero con una select que nunca va a funcionar.
                                    // Ejemplo: idTable = 'ad_chart'

                                    sqlwhere = (sqlwhere ? sqlwhere + ' and ' : '') + visualField.key + " = '" + itemId + "'";
                                    cacheService.loadReferenceTableData(idReferenceValue, sqlwhere, scope).then(function (result) {
                                        visualField.objectValues = result.data.content;
                                        defered.resolve({
                                            visualField: visualField,
                                            referenceTotalElements: result.data.totalElements
                                        });
                                    }).catch(function (err) {
                                        defered.reject(err);
                                    });
                                } else {
                                    defered.resolve({visualField: visualField});
                                }
                            }).catch(function (err) {
                                defered.reject(err);
                            });
                        });
                    }
                } else {
                    defered.reject("No reference table obtained for: " + idReferenceValue);
                }
            }).catch(function(err){
                defered.reject(err);
            });

            return promise;
        };

        return service;
    });
});
