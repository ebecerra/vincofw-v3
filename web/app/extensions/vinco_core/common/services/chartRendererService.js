define(['app', 'moment'], function (app, moment) {
    "use strict";

    return app.registerFactory('chartRendererService', ['$rootScope', '$log', 'Authentication', 'cacheService', 'hookService', 'commonService', 'adChartService',
        function ($rootScope, $log, Authentication, cacheService, hookService, commonService, adChartService) {
            var service = {};

            service.colors = [
                { color:'rgb(47, 126, 216)', hover:'rgb(72, 151, 241)' },
                { color:'rgb(13, 35, 58)', hover:'rgb(38, 60, 83)' },
                { color:'rgb(139, 188, 33)', hover:'rgb(164, 213, 58)' },
                { color:'rgb(145, 0, 0)', hover:'rgb(170, 25, 25)' },
                { color:'rgb(26, 173, 206)', hover:'rgb(51, 198, 231)' },
                { color:'rgb(73, 41, 112)', hover:'rgb(98, 66, 137)' }
            ];

            service.getColor = function (index) {
                return service.colors[index%service.colors.length];
            };

            service.getDefaultColor = function (color, defValue) {
                return color ? color : defValue;
            };

            service.getColors = function (data) {
                return [
                    service.getDefaultColor(data.color1, '#6595b4'), service.getDefaultColor(data.color2, '#7e9d3a'),
                    service.getDefaultColor(data.color3, '#666'), service.getDefaultColor(data.color4, '#BBB'),
                    service.getDefaultColor(data.color5, '#BD362F'), service.getDefaultColor(data.color6, '#e6b8af'),
                    service.getDefaultColor(data.color7, '#9900ff'), service.getDefaultColor(data.color8, '#1155cc'),
                    service.getDefaultColor(data.color9, '#ff9900'), service.getDefaultColor(data.color10, '#ff00ff')
                ];
            };

            service.doChartItemClicked = function (scope, data) {
                var found = _.find(scope.chartItem.linkedCharts, function (current) {
                    return current === data.idChart
                });
                if (found) {
                    $rootScope.$broadcast(app.eventDefinitions.CHART_UPDATE_FILTER_DESC, {
                        idChart: scope.chartItem.idChart,
                        idClickedChart: found,
                        keyField: data.keyField,
                        value: data.value
                    });
                    scope.updateChart("," + data.keyField + "=" + data.value);
                }
            };

            service.doGlobalTabSelected = function (scope, data) {
                if (scope.chartItem.idTab == data.idTab) {
                    scope.updateChart();
                }
            };

            service.doChartResetSearch = function (scope, data) {
                if (scope.chartItem.idTab == data.idTab) {
                    scope.updateChart();
                }
            };

            service.doUpdate = function (newData, old, scope) {
                if (scope.oldFilter === undefined)
                    scope.oldFilter = JSON.stringify(old);
                if (JSON.stringify(newData)!==scope.oldFilter) {
                    scope.oldFilter = JSON.stringify(old);
                    scope.updateChart();
                }
            };

            service.doDestroy = function (scope) {
                if (scope.unwatch)
                    scope.unwatch();
            };

            service.getChartCaptions = function (data, legend) {
                return {
                    xTitle: data.showTitleX ? data.titleX : '',
                    yTitle: data.showTitleY ? data.titleY : '',
                    legend: data.items > 1 ? legend : { show: false }
                };
            };

            service.evaluateChart = function (scope, extraArgs, callback) {
                var parentQuery = "";
                if (scope.parentItem) {
                    _.each(scope.parentItem.fields, function (field) {
                        if (field.usedInChild) {
                            parentQuery += ("," + field.name + "=" + scope.parentItem.item[field.standardName]);
                        }
                    });
                }
                var qParam = service.getQueryParam(scope.filterInfo, scope.filterRangeSet, scope.filterSet);
                if (extraArgs)
                    qParam += extraArgs;
                qParam += parentQuery;
                hookService.execHooks('AD_GetChartParams', { chart: scope.chartItem, constraints: qParam, scope: scope }).then(function (data) {
                    if (data && data.success) {
                        adChartService.evaluate({
                            idClient: cacheService.loggedIdClient(),
                            idUserLogged: Authentication.getLoginData().idUserLogged,
                            id: scope.chartItem.idChart,
                            idLanguage: Authentication.getCurrentLanguage(),
                            q: commonService.getParamsFromHookResult(data, qParam)
                        }, function (data) {
                            callback(data);
                        }, function (err) {
                            $log.error(err);
                        });
                    } else {
                        $log.error("Could not retrieve information for hook AD_GetChartParams");
                    }
                });
            };

            /**
             * Gets the query to pass as a parameter to the chart evaluate function
             *
             * @returns {string} Query assembled
             */
            service.getQueryParam = function (filterInfo, filterRangeSet, filterSet) {
                var query = "";
                // Add filters
                var filterKeys = _.keys(filterInfo.filters);
                _.each(filterKeys, function (current) {
                    if (filterInfo.filters[current] === "" || filterInfo.filters[current] === null) {
                        return;
                    }
                    if (current == "search") {
                        if (filterInfo.filters[current] && filterInfo.filters[current] != "") {
                            query += ('search=%' + filterInfo.filters[current] + "%,");
                        }
                        return;
                    }
                    var filter = _.find(filterSet, function (currentElement) {
                        return currentElement.filterName == current;
                    });
                    if (filter) {
                        var type = filter.reference ? filter.reference.rtype : null;
                        if (filter.reference && !filter.reference.base) {
                            if (type == 'LIST' || type == 'TABLEDIR' || type == 'TABLE') {
                                query += (filter.name + '=' + filterInfo.filters[current] + ",");
                            }
                        } else {
                            if (filterInfo.filters[current] && filterInfo.filters[current] != "") {
                                var value = service.getQueryParamValue(type, filterInfo.filters[current]);
                                if (type == 'STRING' || type == 'EMAIL' || type == 'HTML' || type == 'SEARCH' || type == 'TEXT') {
                                    query += (filter.name + '=%' + value + "%,");
                                    // TODO: MUltiselect
/*
                                    if (filter.field.filterType == "MULTISELECT" && filterInfo.filters[current] && filterInfo.filters[current].length > 0) {
                                        var filterResult = "";
                                        for (var i = 0; i < filterInfo.filters[current].length; i++) {
                                            filterResult += filterInfo.filters[current][i];
                                            if (i + 1 < filterInfo.filters[current].length)
                                                filterResult += ";"
                                        }
                                        query += (filter.name + '=[' + filterResult + "],");
                                    }
*/
                                } else {
                                    query += (filter.name + '=' + value + ",");
                                }
                            }
                        }
                    }
                });

                // Add range filters
                var filterRangeKeys = _.keys(filterInfo.filtersRange);
                _.each(filterRangeKeys, function (current) {
                    var startStr = '';
                    var endStr = '';
                    var filter = _.find(filterRangeSet, function (currentElement) {
                        return currentElement.standardName === current;
                    });
                    if (current !== 'undefined' && filterInfo.filtersRange[current]) {
                        // Get default values
                        if (filterInfo.filtersRange[current].start === '' && filter.valuedefault) {
                            filterInfo.filtersRange[current].start = cacheService.replaceJSConditional(null, null, filter.valuedefault);
                        }
                        if (filterInfo.filtersRange[current].end === '' && filter.valuedefault2) {
                            filterInfo.filtersRange[current].end = cacheService.replaceJSConditional(null, null, filter.valuedefault2);
                        }
                        // Start value
                        if (filterInfo.filtersRange[current].start) {
                            startStr = service.getQueryParamValue(filter.reference.rtype, filterInfo.filtersRange[current].start);
                        }
                        if (filterInfo.filtersRange[current].end) {
                            endStr = service.getQueryParamValue(filter.reference.rtype, filterInfo.filtersRange[current].end);
                        }
                    }

                    if ((startStr != '') || (endStr != '')) {
                        query += (current + '=' + startStr + "~" + endStr + ',');
                    }
                });
                return query;
            };

            service.getQueryParamValue = function (rtype, value) {
                var result = value;
                switch (rtype) {
                    case 'DATE':
                        if (moment.isMoment(value)) {
                            result = value.format('DD/MM/YYYY');
                        } else if (_.isDate(value)) {
                            result = moment(value).format('DD/MM/YYYY');
                        } else {
                            var indx = value.indexOf('T');
                            if (indx == 10) {
                                var val = value.substr(0, indx);
                                if (moment(val, 'YYYY-MM-DD').isValid())
                                    result = moment(val, 'YYYY-MM-DD').format('DD/MM/YYYY');
                            }
                        }
                        break;
                }
                return result;
            };

            return service;
        }
    ]);
});

