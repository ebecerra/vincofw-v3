define(['app'], function (app) {

    'use strict';

    return app.registerDirective('fileUploader', function ($timeout) {
        return {
            restrict: 'A',
            require: '?ngModel',
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {
                        element.removeAttr('file-uploader');
                        // Showing file selector dialog when clicking on textbox
                        element.parent().parent().find(":text").on('click',
                            function(event){
                                element.parent().parent().find(":file").click();
                            }
                        );
                        element.on('change', function (event) {
                            var files = event.target.files;
                            if (files.length > 0) {
                                if (ngModel) {
                                    scope.$apply(function () {
                                        ngModel.$setViewValue(event.target.files);
                                    });
                                }
                                this.parentNode.nextSibling.nextSibling.value = files[0].name;
                            }
                        });
                        if (ngModel) {
                            ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                var text = "...";
                                if (ngModel.$viewValue)
                                    text = ngModel.$viewValue;
                                element.val(null);
                                element.parent().next().val(text);
                            };
                        }
                    }
                }
            }
        }
    })
});