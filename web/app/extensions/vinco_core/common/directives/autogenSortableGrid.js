define(['app', 'lodash', 'jquery', 'angular'], function (app, _, $, angular) {

    /**
     * This directive generates the HTML for visually sorting a grid by using drag & drop
     * The following are the parameters to be passed:
     *
     *      parent-item           ->  The parent process associated to the element
     *
     * The following is an example usage of the directive
     *
     *      <autogen-sortable-grid>
     *      </autogen-sortable-grid>
     */
    'use strict';

    return app.registerDirective('autogenSortableGrid', ['$rootScope', 'cacheService', '$log', 'coreConfigService', '$timeout', 'autogenService', 'commonService',
        function ($rootScope, cacheService, $log, coreConfigService, $timeout, autogenService, commonService) {
            return {
                restrict: 'E',
                scope: {
                    items: '=',
                    start: '=',
                    step: '=',
                    tabGeneratorId: '=',
                    checkAll: '=?',
                    relatedTabGeneratorId: '='
                },
                replace: true,
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.sortable.grid.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.cardMode = _.includes(_.keys(attributes), "cardMode");
                            scope.visible = true;
                            scope.itemIds = [];
                            scope.gridClass = "grid" + scope.$id;
                            scope.gridSelector = "." + scope.gridClass;
                            cacheService.loadTab(scope.tabGeneratorId).then(function (sortable) {
                                scope.windowTabInfo = sortable;
                                if (!sortable) {
                                    $log.error("Could not find related sortable tab for tab ID: " + scope.tabGeneratorId);
                                    return;
                                }
                                scope.service = null;
                                try {
                                    cacheService.loadTableById(sortable.idTable).then(function (table) {
                                        scope.service = autogenService.getAutogenService(table);

                                        scope.sortColumn = _.find(table.columns, function (current) {
                                            return current.isSortColumn;
                                        });
                                        if (!scope.sortColumn) {
                                            $log.error("No sort column is defined for table " + table.name);
                                        }
                                        else {
                                            scope.refreshItems();
                                        }
                                    }).catch(function (error) {
                                        $log.error(error);
                                    });
                                } catch (error) {
                                    $log.error(error);
                                }

                                /**
                                 * Refreshes the items and step and start values
                                 */
                                scope.refreshItems = function () {
                                    if (scope.sortColumn) {
                                        scope.itemsCopy = angular.copy(scope.items);
                                        scope.itemsCopy = _.sortBy(scope.itemsCopy, function (current) {
                                            return current[scope.sortColumn.standardName];
                                        });
                                        if (scope.itemsCopy.length > 0) {
                                            scope.step = 0;
                                            if (scope.itemsCopy.length === 1) {
                                                scope.step = 10;
                                            } else {
                                                var dif = scope.itemsCopy[1][scope.sortColumn.standardName] - scope.itemsCopy[0][scope.sortColumn.standardName];
                                                var difSegments = [1, 5, 10, 50, 100, 500, 1000];
                                                for (var i = 0; i < difSegments.length; i++) {
                                                    if (dif >= difSegments[i]) {
                                                        scope.step = difSegments[i];
                                                    }
                                                }
                                            }
                                            scope.start = scope.itemsCopy[0][scope.sortColumn.standardName];

                                            $timeout(function () {
                                                try {
                                                    scope.itemIds = $(scope.gridSelector).sortable("toArray");
                                                }
                                                catch (ex) {
                                                }
                                            }, 500);
                                        }
                                    }
                                };

                                scope.$on(coreConfigService.events.NAVIGATION_REFRESH_SORT_VALUES, function (event, data) {
                                    if (data.idTab === scope.relatedTabGeneratorId) {
                                        scope.start = data.start;
                                        scope.step = data.step;
                                    }
                                });

                                scope.throwCancelSort = function () {
                                    scope.$emit(coreConfigService.events.NAVIGATION_CANCEL_SORT, {idTab: scope.relatedTabGeneratorId});
                                };

                                scope.throwDoSort = function () {
                                    scope.service.sort({
                                        field: scope.sortColumn.standardName,
                                        idClient: cacheService.loggedIdClient(),
                                        start: scope.start,
                                        step: scope.step,
                                        ids: scope.itemIds
                                    }, function (data) {
                                        commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                        scope.$emit(coreConfigService.events.NAVIGATION_APPLY_SORT, {idTab: scope.relatedTabGeneratorId});
                                    }, function (error) {
                                        commonService.showRejectNotification($rootScope.getMessage('AD_SortMsgError'));
                                        $log.error(error);
                                    });
                                };

                                scope.colSpan = Math.floor(12 / sortable.sortColumns);

                                if (!scope.cardMode) {
                                    $(scope.gridSelector).sortable({
                                        tolerance: 'pointer',
                                        revert: 'invalid',
                                        placeholder: 'col-md-' + scope.colSpan + ' placeholder title',
                                        forceHelperSize: true,
                                        cursorAt: {top: 10, left: 10},
                                        stop: function (event, ui) {
                                            scope.$apply(function () {

                                                try {
                                                    scope.itemIds = $(scope.gridSelector).sortable("toArray");
                                                } catch (ex) {
                                                }
                                            })
                                        }
                                    });
                                } else {
                                    try {
                                        $(scope.gridSelector).sortable('destroy');
                                    } catch (ex) {
                                    }
                                }
                                var timeoutFunc = $timeout(function () {

                                    try {
                                        scope.itemIds = $(scope.gridSelector).sortable("toArray");
                                    } catch (ex) {
                                    }
                                }, 0);

                                scope.$watchCollection('[items]', function () {
                                    scope.refreshItems();
                                });

                                /**
                                 * Checks the item and deselects all the other items
                                 *
                                 * @param item Item to check
                                 */
                                scope.checkItem = function (item) {
                                    _.each(scope.items, function (current) {
                                        current.isCheckedGrid = item.id === current.id;
                                    });
                                    _.each(scope.itemsCopy, function (current) {
                                        current.isCheckedGrid = item.id === current.id;
                                    });
                                    scope.checkAll = false;
                                    scope.$emit(coreConfigService.events.NAVIGATION_UPDATE_CHECK_ALL, {
                                        idTab: scope.relatedTabGeneratorId,
                                        checkAll: scope.checkAll
                                    });
                                };

                                /**
                                 * Triggers the selection of an item through the DoubleClick event
                                 *
                                 * @param item Item to be edited
                                 */
                                scope.triggerDoubleClick = function (item) {
                                    if (scope.cardMode) {
                                        _.each(scope.items, function (current) {
                                            current.isCheckedGrid = item.id === current.id;
                                        });
                                        _.each(scope.itemsCopy, function (current) {
                                            current.isCheckedGrid = item.id === current.id;
                                        });
                                        scope.$emit(coreConfigService.events.NAVIGATION_EDIT_ITEM, {
                                            idTab: scope.relatedTabGeneratorId,
                                            item: item
                                        });
                                    }
                                };

                                scope.verifyCheck = function (item) {
                                    _.each(scope.items, function (current) {
                                        if (item.id === current.id)
                                            current.isCheckedGrid = item.isCheckedGrid;
                                    });
                                    _.each(scope.itemsCopy, function (current) {
                                        if (item.id === current.id)
                                            current.isCheckedGrid = item.isCheckedGrid;
                                    });
                                    scope.checkAll = false;
                                };

                                scope.$on(coreConfigService.events.NAVIGATION_CHECK_ALL, function (event, data) {
                                    if (data && data.idTab === scope.relatedTabGeneratorId) {
                                        _.each(scope.itemsCopy, function (current) {
                                            current.isCheckedGrid = data.checkAll;
                                        });
                                        _.each(scope.items, function (current) {
                                            current.isCheckedGrid = data.checkAll;
                                        });
                                    }
                                });

                                scope.$watchCollection("[checkAll]", function () {
                                    if (scope.checkAll) {
                                        _.each(scope.itemsCopy, function (current) {
                                            current.isCheckedGrid = scope.checkAll;
                                        });
                                    }
                                });

                                // Clean up
                                element.on('$destroy', function () {
                                    $timeout.cancel(timeoutFunc);
                                });
                            });
                        }
                    }
                }
            }
        }]);
});