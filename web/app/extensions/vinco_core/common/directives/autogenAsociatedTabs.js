/**
 * This directive generates the tabs corresponding to a given window
 * It receives the following parameters in the form of attributes
 *
 *      tab-generator-id        ->  The idTab key
 *      window-generator-id     ->  The associated window id
 *      row-id                  ->  The ID of the item to be edited
 *      sub-item                ->  Accepts true or false. Used to define if the edited item is a main item or a subitem
 *                                  of a main item. Default value is false
 *      parent-id-tab           ->  The ID of the parent tab
 *
 * The following is an example usage of the directive
 *
 *  <autogen-asociated-tabs tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018"></autogen-asociated-tabs>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('autogenAsociatedTabs', [
        '$rootScope', '$compile', '$injector', '$log', '$timeout', 'cacheService', 'Authentication', 'commonService',
        'autogenService', 'tabManagerService', 'coreConfigService',
        function ($rootScope, $compile, $injector, $log, $timeout, cacheService, Authentication, commonService,
                  autogenService, tabManagerService, coreConfigService) {
            return {
                restrict: 'E',
                require: '?ngModel',
                templateUrl: APP_EXTENSION_VINCO_CORE + '/common/views/autogen.asociated.tabs.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.tabGeneratorId = attributes.tabGeneratorId;
                            scope.windowTitle = attributes.windowTitle;
                            scope.rowId = attributes.rowId;
                            scope.isEditing = (scope.rowId !== undefined && scope.rowId !== null && scope.rowId !== "");
                            scope.itemToEdit = null;
                            scope.validItem = false;
                            scope.subItem = false;
                            scope.showRefresh = false;
                            scope.parentIdTab = attributes.parentIdTab;
                            if (attributes.subItem) {
                                scope.subItem = JSON.parse(attributes.subItem);
                            }
                            if (attributes.isMultiple) {
                                scope.isMultiple = JSON.parse(attributes.isMultiple);
                            }
                            if (attributes.linkData) {
                                scope.linkData = JSON.parse(attributes.linkData);
                            }
                            if (attributes.initialData) {
                                scope.initialData = JSON.parse(attributes.initialData);
                            }
                            scope.windowGeneratorId = attributes.windowGeneratorId;
                            scope.otherTabInfoLoaded = false;
                            scope.isMainSelected = true;
                            scope.selectTab = function (tab) {
                                $('a[data-toggle="tab"]').unbind('shown.bs.tab').bind('shown.bs.tab', function (e) {
                                    $rootScope.$broadcast(app.eventDefinitions.GLOBAL_TAB_SELECTED, {idTab: $(e.target)[0].id});
                                    scope.isMainSelected  = scope.windowTabInfo.idTab === $(e.target)[0].id;
                                });
                            };

                            scope.$on(coreConfigService.events.NAVIGATION_NEXT, function (event, data) {
                                if (scope.tabGeneratorId === data.idTab){
                                    scope.itemToEdit = null;
                                }
                            });

                            scope.$on(coreConfigService.events.NAVIGATION_PREV, function (event, data) {
                                if (scope.tabGeneratorId === data.idTab){
                                    scope.itemToEdit = null;
                                }
                            });

                            scope.$on(coreConfigService.events.NAVIGATION_FIRST, function (event, data) {
                                if (scope.tabGeneratorId === data.idTab){
                                    scope.itemToEdit = null;
                                }
                            });

                            scope.$on(coreConfigService.events.NAVIGATION_LAST, function (event, data) {
                                if (scope.tabGeneratorId === data.idTab){
                                    scope.itemToEdit = null;
                                }
                            });

                            scope.$on(coreConfigService.events.NAVIGATION_NEW, function (event, data) {
                                if (scope.tabGeneratorId === data.idTab){
                                    scope.itemToEdit = null;
                                }
                            });

                            scope.$on(app.eventDefinitions.GLOBAL_ITEM_CREATED, function (event, data) {
                                if (scope.windowGeneratorId === data.windowGeneratorId) {
                                    if (!data.idTab || data.idTab === scope.tabGeneratorId) {
                                        scope.validItem = true;
                                        if (data.item) {
                                            scope.showRefresh = true;
                                            scope.itemToEdit = data.item;
                                            scope.visualizeTabs();
                                        }
                                    }
                                }
                            });
                            scope.$on(coreConfigService.events.NAVIGATION_BEFORE_NEW, function (event, data) {
                                if (scope.windowTabInfo.idTab === data.idTab) {
                                    scope.showRefresh = false;
                                }
                            });

                            scope.showAudit = function(){
                                var table = cacheService.getTable(scope.windowTabInfo.idTable);
                                if (scope.table.audit) {
                                    var auditTab = _.find(scope.allOtherTabs, function (current) {
                                        return current.specialCase === coreConfigService.SPECIAL_CASE_TAB.AUDIT;
                                    });
                                    if (auditTab) {
                                        if (auditTab.tab.displaylogic === "@true@") {
                                            table.isShowingAudit = false;
                                            auditTab.tab.displaylogic = "@false@";
                                        }
                                        else {
                                            auditTab.tab.displaylogic = "@true@";
                                            table.isShowingAudit = true;
                                        }
                                        scope.visualizeTabs();
                                        $timeout(function(){
                                            if (auditTab.tab.displaylogic === "@true@")
                                                $('#'+coreConfigService.auditIdTab + scope.windowTabInfo.idTable).tab('show');
                                            else
                                                $('#'+scope.windowTabInfo.idTab).tab('show');
                                        });
                                    } else {
                                        $log.warn("Attempting to show Audit tab, but no audit tab was found for tab ID: " + scope.windowTabInfo.idTab);
                                    }
                                }
                            };

                            /**
                             * Checks the fields marked as "UsedInChild" in the parent
                             */
                            scope.checkLinkColumns = function (columns, itemToEdit) {
                                for (var i = 0; i < scope.windowTabInfo.tablevel; i++) {
                                    var parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel - (i + 1)
                                    });
                                    if (parentItem) {
                                        _.each(parentItem.fields, function (field) {
                                            if (field.usedInChild && !scope.isEditing) {
                                                columns.push(field.column);
                                                itemToEdit[field.standardName] = parentItem.item[field.standardName];
                                            }
                                        });
                                    }
                                }
                            };

                            scope.$on(app.eventDefinitions.TAB_VISUALIZATION_NOTIFICATION, function (event, data) {
                                if (data.idWindow === scope.windowTabInfo.idWindow && scope.itemToEdit) {
                                    scope.visualizeTabs(data.field, data.value);
                                }
                            });

                            /**
                             * Checks if the tab is a special case for display logic
                             * @param tab Tab information
                             * @returns {boolean} True if the tab is of type AUDIT tab
                             */
                            scope.isAuditTab = function(tab){
                                return tab.specialCase === coreConfigService.SPECIAL_CASE_TAB.AUDIT;
                            };

                            scope.visualizeTabs = function (extraField, extraFieldValue) {
                                var table = cacheService.getTable(scope.windowTabInfo.idTable);
                                scope.table = table;
                                scope.otherTabs = _.filter(scope.allOtherTabs, function (tab) {
                                    var _show = true;
                                    if (scope.isAuditTab(tab)){
                                        return eval(cacheService.replaceJSConditional({}, {}, tab.tab.displaylogic));
                                    }
                                    if (tab.tab.displaylogic) {
                                        var columns = [];
                                        _.each(table.columns, function (col) {
                                            columns.push(col);
                                        });
                                        scope.checkLinkColumns(columns, scope.itemToEdit);
                                        var i, _logic = tab.tab.displaylogic;
                                        for (i = 0; i < columns.length; i++) {
                                            var field = {column: columns[i]};
                                            if (cacheService.isFieldInConditional(field, _logic)) {
                                                _logic = cacheService.replaceJSConditional(field, scope.itemToEdit[field.column.standardName], _logic);
                                                if (cacheService.isCompleteConditional(_logic)) {
                                                    _show = eval(_logic);
                                                    break;
                                                }
                                            }
                                        }
                                        var _logic = tab.tab.displaylogic;
                                        if (extraField) {
                                            if (cacheService.isFieldInConditional(extraField, _logic)) {
                                                _logic = cacheService.replaceJSConditional(field, extraFieldValue, _logic);
                                                if (cacheService.isCompleteConditional(_logic)) {
                                                    _show = eval(_logic);
                                                }
                                            }

                                        }
                                    }
                                    return _show;
                                });
                                // Filtering tabs with columns marked as linkParent
                                scope.otherTabs = _.filter(scope.otherTabs, function (current) {
                                    var included = true;
                                    if (current.tab.ttype != 'IMAGE' && current.tab.ttype != 'FILE') {
                                        if (current.tab.idParentTable &&
                                            current.tab.idParentTable === scope.windowTabInfo.idTable) {
                                            return included;
                                        }
                                        var currentTable = cacheService.getTable(current.tab.idTable);
                                        var fields = _.filter(current.tab.fields, function (field) {
                                            return field.column.linkParent;
                                        });
                                        if (fields.length > 0) {
                                            var found = _.find(fields, function (field) {
                                                var column = _.find(currentTable.columns, function (col) {
                                                    return col.idColumn === field.idColumn;
                                                });
                                                if (column) {
                                                    if (column.linkColumn) {
                                                        var col = _.find(scope.table.columns, function (c) {
                                                            return c.name === field.column.linkColumn;
                                                        });
                                                        return col !== undefined;
                                                    } else {
                                                        var ref = field.column.referenceValue;
                                                        if (!ref) {
                                                            ref = cacheService.getReference(column.idReferenceValue);
                                                            field.column.referenceValue = ref;
                                                        }
                                                        if (ref && ref.info) {
                                                            return ref.info.idTable === scope.table.idTable;
                                                        } else {
                                                            return false;
                                                        }
                                                    }
                                                }
                                                return false;
                                            });
                                            included = found !== undefined;
                                        }
                                    }
                                    return included;
                                });
                                scope.otherTabs = _.sortBy(scope.otherTabs, function (current) {
                                    return current.tab.seqno;
                                });

                                scope.otherTabInfoLoaded = true;
                            };

                            cacheService.loadTab(scope.tabGeneratorId).then(function (data) {
                                scope.windowTabInfo = data;
                                if (scope.subItem) {
                                    scope.windowTitle = scope.windowTabInfo.name;
                                }
                                var thatScope = scope;
                                var table = cacheService.getTable(scope.windowTabInfo.idTable);
                                scope.table = table;
                                if (table) {
                                    thatScope.service = null;
                                    try {
                                        thatScope.service = autogenService.getAutogenService(table);
                                    } catch (e) {
                                        $log.error(e);
                                    }
                                    var proceedWindowLoad = function (itemToEdit) {
                                        thatScope.itemToEdit = itemToEdit;
                                        tabManagerService.setInitialFormItem(thatScope.windowTabInfo.idTab, thatScope.itemToEdit);
                                        cacheService.loadWindow(thatScope.windowTabInfo.idWindow).then(function (data) {
                                            thatScope.otherTabs = _.filter(data.tabs, function (tab) {
                                                return tab.idTab !== thatScope.windowTabInfo.idTab
                                                    && tab.tab.tablevel === (thatScope.windowTabInfo.tablevel + 1)
                                                    && !cacheService.isSpecialTab(tab.tab) && tab.tab.runtime.defaultTab;
                                            });
                                            thatScope.allOtherTabs = thatScope.otherTabs;
                                            var maxSeqno = _.maxBy(thatScope.allOtherTabs, function(current){
                                                return current.seqno;
                                            });
                                            var auditTabId = coreConfigService.auditIdTab + scope.windowTabInfo.idTable;
                                            var auditTabInfo = {
                                                idTab: auditTabId,
                                                idTable: scope.windowTabInfo.idTable,
                                                idParentTable: scope.windowTabInfo.idParentTable,
                                                idWindow: scope.windowTabInfo.idWindow,
                                                name: $rootScope.getMessage("AD_labelAudit"),
                                                tablevel: scope.windowTabInfo.tablevel + 1,
                                                columns: [],
                                                uipattern: "STANDARD",
                                                ttype: "AUDIT",
                                                displaylogic: table.isShowingAudit ? "@true@" : "@false@",
                                                seqno: maxSeqno + 10,
                                                command: "",
                                                sqlwhere: "",
                                                sortColumns: [],
                                                fields: [],
                                                charts: [],
                                                viewMode: "LIST",
                                                supportsMultipleViewMode: false,
                                                viewDefault: true,
                                                groupMode: "TAB",
                                                item: scope.itemToEdit
                                            };
                                            cacheService.addAuditTab(auditTabId, auditTabInfo );
                                            scope.allOtherTabs.push({
                                                idTab: auditTabId,
                                                specialCase: coreConfigService.SPECIAL_CASE_TAB.AUDIT,
                                                tab: auditTabInfo
                                            });

                                            thatScope.visualizeTabs();
                                        }).catch(function (err) {
                                            $log.error(err);
                                        });
                                    };
                                    if (thatScope.rowId) {
                                        thatScope.service.get(
                                            {
                                                idClient: cacheService.loggedIdClient(),
                                                id: thatScope.rowId
                                            },
                                            function (data) {
                                                scope.showRefresh = true;
                                                proceedWindowLoad(data);
                                            },
                                            function (data) {
                                                $log.error(data);
                                            }
                                        );
                                    } else {
                                        proceedWindowLoad({});
                                    }
                                } else {
                                    // TODO: Error handling
                                }
                            }).catch(function (err) {
                                $log.error(err);
                            });

                            scope.throwReturnToList = function (idTab) {
                                scope.$broadcast(coreConfigService.events.NAVIGATION_RETURN_TO_LIST, {idTab: idTab});
                            };

                            scope.throwSaveElement = function (idTab) {
                                scope.$broadcast(coreConfigService.events.NAVIGATION_APPLY, {idTab: idTab});
                            };

                            scope.throwCancelElement = function (idTab) {
                                scope.$broadcast(coreConfigService.events.NAVIGATION_CANCEL, {idTab: idTab});
                            };

                            scope.throwNextElement = function (idTab) {
                                scope.$broadcast(coreConfigService.events.NAVIGATION_NEXT, {idTab: idTab});
                            };

                            scope.throwPrevElement = function (idTab) {
                                scope.$broadcast(coreConfigService.events.NAVIGATION_PREV, {idTab: idTab});
                            };
                        }
                    }
                }
            }
        }]);
});