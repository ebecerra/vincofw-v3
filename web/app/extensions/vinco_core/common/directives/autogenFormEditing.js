define(['app', 'moment', 'angular', 'modules/forms/directives/editors/smartCkEditor'], function (app, moment, angular) {

    /**
     * This directive generates a form corresponding to the data defined in the given tab
     * It receives the following parameters in the form of attributes
     *
     *      tab-generator-id        ->  The idTab key
     *      row-id                  ->  The primary key of row to edit
     *      sub-item                ->  Accepts true or false. Used to define if the edited item is a main item or a subitem
     *                                  of a main item. Default value is false
     *      link-data               ->  The reference keys if this form belongs to an entity that contains foreign keys
     *      other-tabs              ->  The number of siblings this form has in the tabular structure
     *                                  (e.g. the "Fields" tab when editing a Tab, the number of siblings will be 1)
     *                                  Default value is 0
     *      parent-id-tab           ->  The ID of the parent tab
     *      force-read-only         ->  Forces the content to be displayed in read-only mode
     *      hide-footer             ->  Hides the footer
     *      disable-inline          ->  Disables the inline editing of TABLEDIR, TABLE and SEARCH elements
     *
     * The following is an example usage of the directive
     *
     *  <autogen-form-editing tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018" row-id=""></autogen-form-editing>
     */
    'use strict';

    return app.registerDirective('autogenFormEditing', [
        '$rootScope', '$timeout', '$log', '$state', '$q', 'Authentication', 'commonService', 'cacheService', 'autogenService',
        'hookService', 'adTranslationService', 'tabManagerService', 'coreConfigService', 'utilsService',
        function ($rootScope, $timeout, $log, $state, $q, Authentication, commonService, cacheService, autogenService,
                  hookService, adTranslationService, tabManagerService, coreConfigService, utilsService) {
        return {
            restrict: 'E',
            require: '?ngModel',
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.editing.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.forceReadOnly = 'forceReadOnly' in attributes;
                        scope.hideFooter = 'hideFooter' in attributes;
                        scope.hideHeader = 'hideHeader' in attributes;
                        element.removeAttr('force-read-only');
                        element.removeAttr('hide-footer');
                        element.removeAttr('hide-header');
                        scope.inputsTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.inputs.html?v=' + FW_VERSION;
                        scope.processDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.dialog.html?v=' + FW_VERSION;
                        scope.translationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.translation.dialog.html?v=' + FW_VERSION;
                        scope.editingWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.editing.window.html?v=' + FW_VERSION;
                        scope.searchWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.editing.search.window.html?v=' + FW_VERSION;
                        scope.authorizationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.authorization.dialog.html?v=' + FW_VERSION;
                        scope.tabGeneratorId = attributes.tabGeneratorId;
                        scope.translationDialogId = 'translationModal' + scope.tabGeneratorId + scope.$id;
                        scope.processDialogId = 'processModal' + scope.tabGeneratorId+ scope.$id;
                        scope.tableSearchDialogId = 'tableModal' + scope.tabGeneratorId+ scope.$id;
                        scope.searchInputDialogId = 'searchModal' + scope.tabGeneratorId+ scope.$id;
                        scope.authorizationDialogId = 'authorizationModal' + scope.tabGeneratorId+ scope.$id;
                        scope.rowId = attributes.rowId;
                        scope.isEditing = (scope.rowId !== undefined && scope.rowId !== null && scope.rowId !== "");
                        scope.parentIdTab = null;
                        scope.parentTab = null;
                        scope.fieldReadOnlyRuntime = [];
                        scope.hasRole = $rootScope.hasRole;
                        scope.hasPermission = $rootScope.hasPermission;

                        /**
                         * Defines if the reference field can be quickly edited (TABLEDIR, TABLE, SEARCH)
                         * @type {boolean}
                         */
                        scope.editReferences = false;

                        /**
                         * Defines if the buttons should be placed in a separate row at the end of the fields, or if they should be included
                         * as part of the common bootstrap rendering logic.
                         * @type {boolean} True to use the default rendering logic of all the fields. False to place buttons at the end of the form
                         */
                        scope.includeButtonsInBootstrap = false;
                        scope.allItems = cacheService.getTabItems(scope.tabGeneratorId);

                        scope.loadNewElementFromList = function (index) {
                            scope.service.get(
                                {
                                    idClient: cacheService.loggedIdClient(),
                                    id: scope.allItems[index].id
                                },
                                function (data) {
                                    tabManagerService.setInitialFormItem(scope.tabGeneratorId, data);
                                    scope.tableForm.$dirty = false;
                                    cacheService.clearFormInfo(scope.tabGeneratorId);
                                    scope.fieldValue = {};
                                    scope.isEditing = true;
                                    scope.rowId = scope.allItems[index].id;
                                    scope.fieldInitializations();
                                    scope.loadForm();
                                    scope.tableForm.$setPristine();
                                },
                                function (data) {
                                    $log.error(data);
                                }
                            );
                        };

                        scope.$on(coreConfigService.events.NAVIGATION_UPDATE_REFERENCE, function (event, data) {
                            if (data.idTab === scope.tabGeneratorId) {
                                $log.info(data);
                            }
                        });

                        scope.$on(app.eventDefinitions.GLOBAL_ITEM_FIELD_CHANGE, function (event, data) {
                            if (data.idTab === scope.tabGeneratorId) {
                                var fld = _.find(scope.allFields, function (f) {
                                    return f.standardName == data.fieldName;
                                });
                                if (fld) {
                                    scope.item[data.fieldName] = data.fieldValue;
                                    scope.fieldValue[fld.column.name] = data.fieldValue;
                                    scope.changeValue(fld);
                                }
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_RETURN_TO_LIST, function (event, data) {
                            if (data.idTab === scope.tabGeneratorId) {
                                scope.throwCancelClick();
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_CANCEL, function (event, data) {
                            if (data.idTab === scope.tabGeneratorId) {
                                scope.throwCancelClick();
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_APPLY, function (event, data) {
                            if (data.idTab === scope.tabGeneratorId) {
                                if (scope.adTab.runtime.readonly || scope.forceReadOnly) {
                                    scope.throwCancelClick();
                                } else {
                                    scope.save();
                                }
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_NEXT, function (event, data) {
                            if (scope.tabGeneratorId === data.idTab) {
                                var callback = function () {
                                    var nextIndex = -1;
                                    _.each(scope.allItems, function (current, index) {
                                        if (current.id === scope.item.id) {
                                            nextIndex = index + 1;
                                        }
                                    });
                                    if (nextIndex < scope.allItems.length && nextIndex >= 0) {
                                        scope.loadNewElementFromList(nextIndex);
                                    }
                                    if (nextIndex === scope.allItems.length) {
                                        scope.tableForm.$setPristine();
                                        commonService.showHint($rootScope.getMessage('AD_msgCaptionBarAlreadyLast'));
                                    }
                                };
                                if (scope.adTab.runtime.readonly || scope.forceReadOnly) {
                                    callback();
                                } else {
                                    if (scope.tableForm.$pristine) {
                                        callback();
                                    } else {
                                        scope.save(callback);
                                    }
                                }
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_PREV, function (event, data) {
                            if (scope.tabGeneratorId === data.idTab) {
                                var callback = function () {
                                    var nextIndex = -1;
                                    _.each(scope.allItems, function (current, index) {
                                        if (current.id === scope.item.id) {
                                            nextIndex = index - 1;
                                        }
                                    });
                                    if (nextIndex >= 0) {
                                        scope.loadNewElementFromList(nextIndex);
                                    } else {
                                        scope.tableForm.$setPristine();
                                        commonService.showHint($rootScope.getMessage('AD_msgCaptionBarAlreadyFirst'));
                                    }
                                };
                                if (scope.adTab.runtime.readonly || scope.forceReadOnly) {
                                    callback();
                                } else {
                                    if (scope.tableForm.$pristine) {
                                        callback();
                                    } else {
                                        scope.save(callback);
                                    }
                                }
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_LAST, function (event, data) {
                            if (scope.tabGeneratorId === data.idTab) {
                                var callback = function () {
                                    var index = scope.allItems.length - 1;
                                    scope.loadNewElementFromList(index);
                                };
                                if (scope.adTab.runtime.readonly || scope.forceReadOnly) {
                                    callback();
                                } else {
                                    if (scope.tableForm.$pristine) {
                                        callback();
                                    } else {
                                        scope.save(callback);
                                    }
                                }
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_FIRST, function (event, data) {
                            if (scope.tabGeneratorId === data.idTab) {
                                var callback = function () {
                                    scope.loadNewElementFromList(0);
                                };
                                if (scope.adTab.runtime.readonly || scope.forceReadOnly) {
                                    callback();
                                } else {
                                    if (scope.tableForm.$pristine) {
                                        callback();
                                    } else {
                                        scope.save(callback);
                                    }
                                }
                            }
                        });

                        scope.$on(coreConfigService.events.NAVIGATION_NEW, function (event, data) {
                            if (scope.tabGeneratorId === data.idTab) {
                                var callback = function () {
                                    scope.rowId = null;
                                    cacheService.clearFormInfo(scope.tabGeneratorId);
                                    scope.fieldValue = {};
                                    scope.tableForm.$dirty = false;
                                    scope.isEditing = false;
                                    scope.fieldInitializations();
                                    scope.loadForm();
                                    $rootScope.$broadcast(coreConfigService.events.NAVIGATION_BEFORE_NEW, {idTab: scope.tabGeneratorId});
                                };
                                scope.save(callback);
                            }
                        });

                        scope.$on(app.eventDefinitions.GLOBAL_PROCESS_FINISHED, function (event, data) {
                                if (data.tabGeneratorId && data.tabGeneratorId === scope.adTab.idTab) {
                                    var restPath = scope.adTab.table.module.restPath;
                                    if (onEvents && onEvents[restPath] && onEvents[restPath]['onGlobalProcessFinished']) {
                                        onEvents[restPath]['onGlobalProcessFinished'].call(this, data.idProcess, cacheService, $log, scope);
                                    }
                                }
                            }
                        );

                        var parentItem = cacheService.getFormItem({
                            idWindow: scope.windowTabInfo.idWindow,
                            tablevel: scope.windowTabInfo.tablevel - 1
                        });
                        scope.parentItem = parentItem;

                        scope.htmlTemplates = null;
                        cacheService.loadHtmlTemplates().then(function(data){
                            scope.htmlTemplates = data;
                        }).catch(function(err){
                            scope.htmlTemplates = [];
                            $log.error(err);
                        });

                        /**
                         * Preload preferences
                         */
                        scope.stdPref = cacheService.stdPref;

                        /**
                         * Find the primary key column name in the fields array
                         *
                         * @param fields
                         */
                        scope.getPrimaryKeyColumnName = function (fields) {
                            var columnName = null;
                            _.each(fields, function (current, index) {
                                if (current.column.primaryKey) {
                                    columnName = current.column.name;
                                }
                            });
                            return columnName;
                        };

                        if (attributes.parentIdTab) {
                            scope.parentIdTab = attributes.parentIdTab;
                            cacheService.loadTab(scope.parentIdTab).then(function (data) {
                                scope.parentTab = data;
                            });
                        }
                        scope.disableInline = attributes.$attr.disableInline;
                        scope.linkData = attributes.linkData ? JSON.parse(attributes.linkData) : null;
                        scope.initialData = attributes.initialData ? JSON.parse(attributes.initialData) : null;
                        scope.otherTabs = attributes.otherTabs ? JSON.parse(attributes.otherTabs) : 0;
                        var initialData = JSON.stringify(scope.initialData);
                        if (initialData !== 'null' && initialData !== '{}') {
                            tabManagerService.clearTabInitialData();
                        }
                        scope.fieldInitializations = function () {
                            scope.cols = 1;
                            scope.fields = [];
                            scope.primaryFields = [];
                            scope.groups = [];
                            scope.posField = [];
                            scope.fieldValue = {};
                            scope.service = null;
                            scope.allVisibleFields = [];
                            scope.allFields = [];
                            scope.item = {};
                            scope.windowGeneratorId = attributes.windowGeneratorId;
                            scope.complicateVisualFields = {};
                            scope.currentIdReferenceValue = null;
                            scope.currentColumnName = null;
                            scope.sortDirection = 'ASC';
                            scope.searchFilter = '';
                            scope.referenceData = null;
                            if (scope.linkData) {
                                var keys = _.keys(scope.linkData);
                                _.each(keys, function (current) {
                                    scope.item[current] = scope.linkData[current];
                                });
                            }
                            if (scope.initialData) {
                                var keys = _.keys(scope.initialData);
                                _.each(keys, function (current) {
                                    scope.item[current] = scope.initialData[current];
                                });
                            }
                        };

                        scope.fieldInitializations();

                        if (attributes.subItem) {
                            scope.subItem = JSON.parse(attributes.subItem);
                        } else {
                            scope.subItem = false;
                        }
                        scope.translation = {};
                        scope.translationProperty = {
                            translationClientId: null,
                            translationInitClientId: null
                        };

                        // Sort order. False equals ASC, True equals DESC
                        scope.tableReverseSort = false;
                        // Order by field
                        scope.tableOrderByField = '';
                        // Page options
                        scope.tablePageSizeList = [10, 20, 40, 60, 100];
                        scope.referenceData = {
                            referencePageSize: 10,
                            referenceCurrentPage: 1,
                            currentFilterValue: ""
                        };
                        scope.maxSize = 5;

                        scope.filterName = function (item) {
                            if (item[scope.complicateVisualFields[scope.currentIdReferenceValue].display] && scope.currentFilterValue) {
                                return item[scope.complicateVisualFields[scope.currentIdReferenceValue].display].toLowerCase().indexOf(scope.currentFilterValue.toLowerCase()) >= 0;
                            }
                        };

                        scope.doEnter = function (field) {
                            var key = field.tableName + '$' +  field.column.name;
                            scope.fieldDescription = $rootScope.tranlationsFieldDescriptions[key];
/*
                            if (scope.fieldDescription) {
                                commonService.showFieldHint(scope.fieldDescription);
                            }
*/
                        };

                        scope.doComponentEnter = function (fieldName) {
                            var field = _.find(scope.allVisibleFields, function(field){
                                return field.column.name === fieldName;
                            });
                            if (field) {
                                scope.doEnter(field);
                            }
                        };

                        scope.doFormSubmit = function () {
                            return false;
                        };

                        scope.getDescription = function (fieldName) {
                            var field = _.find(scope.allVisibleFields, function(field){
                                return field.column.name === fieldName;
                            });
                            if (field) {
                                var key = field.tableName + '$' +  field.column.name;
                                return $rootScope.tranlationsFieldDescriptions[key];
                            }
                            return null;
                        };

                        /**
                         * Back to previous window
                         */
                        scope.goBack = function () {
                            var event = {
                                tabGeneratorId: scope.tabGeneratorId,
                                tablevel: scope.windowTabInfo.tablevel
                            };
                            if (!scope.subItem) {
                                $rootScope.$broadcast(app.eventDefinitions.CANCEL_GLOBAL_ITEM_EDIT, event);
                                $rootScope.$broadcast('cancel_edit_form', 1);
                            } else {
                                $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, event);
                            }
                        };

                        scope.throwCancelClick = function () {
                            $log.info("Executing autogenFormEditing.throwCancelClick");
                            cacheService.clearFormInfo(scope.tabGeneratorId);
                            var event = {
                                tabGeneratorId: scope.tabGeneratorId,
                                tablevel: scope.windowTabInfo.tablevel
                            };
                            if (!scope.subItem) {
                                $rootScope.$broadcast(app.eventDefinitions.CANCEL_GLOBAL_ITEM_EDIT, event);
                                $rootScope.$broadcast('cancel_to_parent', 1);
                            } else {
                                $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, event);
                            }
                        };

                        /**
                         * Changes current grid page size
                         *
                         * @param pageSize Grid page size
                         */
                        scope.changeTablePage = function (pageSize) {
                            scope.referenceData.referenceCurrentPage = 1;
                            scope.scope.referenceData.referencePageSize = pageSize;
                        };

                        /**
                         * Executes a table sort
                         *
                         * @param item Sort details
                         */
                        scope.orderByFieldFn = function (item) {
                            if (scope.tableOrderByField != commonService.removeUnderscores(item.name)) {
                                scope.tableReverseSort = false;
                            } else {
                                scope.tableReverseSort = !scope.tableReverseSort;
                            }
                            scope.tableOrderByField = commonService.removeUnderscores(item.name);
                            scope.loadSearchData(scope.currentIdReferenceValue);
                        };

                        /**
                         * Show/Hide a group of fields
                         *
                         * @param id Group field id.
                         */
                        scope.toggleGroup = function (id) {
                            var pos = scope.posGroup(scope.groups, id);
                            scope.groups[pos].collapsed = !scope.groups[pos].collapsed;
                        };

                        scope.selectTableValue = function (item) {
                            var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                            var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                            scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                            scope.fieldValue[scope.currentColumnName] = item[col];
                            scope.changeValue(scope.currentField);
                            $('#' + scope.tableSearchDialogId).modal('hide');
                        };

                        /**
                         * Show search selector dialog
                         *
                         * @param field Origin field
                         */
                        scope.showSearchWindow = function (field) {
                            scope.currentIdReferenceValue = field.column.idReferenceValue;
                            scope.currentColumnName = field.column.name;
                            scope.currentField = field;
                            scope.referenceData.referencePageSize = 10;
                            scope.referenceData.referenceCurrentPage = 1;
                            $('#searchModal').modal();
                        };

                        scope.selectSearchValue = function (item) {
                            var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                            var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                            scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                            scope.fieldValue[scope.currentColumnName] = item[commonService.removeUnderscores(col)];
                            scope.changeValue(scope.currentField);
                            $('#searchModal').modal('hide');
                        };

                        /**
                         * Hides the process dialog
                         */
                        scope.hideProcessDialog = function () {
                            $('#' + scope.processDialogId).modal('hide');
                        };

                        /**
                         * Executes the selected process
                         *
                         * @param item Item being edited
                         * @param field Field clicked
                         */
                        scope.executeProcess = function (item, field) {
                            utilsService.executeServerProccess(scope, field, item);
                        };

                        /**
                         * Executes the selected JS process
                         *
                         * @param item Item being edited
                         * @param field Field clicked
                         */
                        scope.executeJS = function (field) {
                            utilsService.executeJavaScriptCode(scope, field, 'FORM');
                        };

                        /**
                         * Load the row to edit or create and prepared the service
                         */
                        scope.reloadBusinessData = function () {
                            var defered = $q.defer();
                            var promise = defered.promise;
                            scope.initialLoading = true;

                            function finishLoading() {
                                scope.checkLinkColumns();
                                scope.initialLoading = false;
                                _.each(scope.allFields, function (current) {
                                    scope.fieldValue['old_' + current.standardName] = null;
                                    if (current.column.reference.rtype == 'INTEGER' && scope.fieldValue[current.standardName]) {
                                        scope.fieldValue[current.standardName] = Number(scope.fieldValue[current.standardName]);
                                    }
                                    if (current.column.readonlylogic) {
                                        var evalReadOnly = scope.$eval(cacheService.replaceJSConditional(current, scope.fieldValue[current.column.name], current.column.readonlylogic, scope.fieldValue));
                                        current.readonly = evalReadOnly !== undefined && evalReadOnly !== false && evalReadOnly !== 0;
                                        scope.fieldReadOnlyRuntime[current.name] = current.readonly;
                                    }
                                    scope.changeValue(current);
                                });
                                scope.checkPasswordField();
                            }

                            // Initializing field values
                            var initialDataFields = scope.initialData ? _.keys(scope.initialData) : [];
                            _.each(scope.allFields, function (current) {

                                if (current.column && current.column.valuedefault && !current.column.primaryKey) {
                                    // If the column is id_client
                                    if (parentItem && current.column.idColumn === coreConfigService.idColumn.adClient.idClient) {
                                        scope.fieldValue[current.column.name] = null;
                                        return;
                                    }
                                    if (current.column.valuedefault === '@null@') {
                                        scope.fieldValue[current.column.name] = null;
                                    } else {
                                        var defValue = cacheService.replaceJSConditional(current, '', current.column.valuedefault);
                                        switch (current.column.ctype) {
                                            case 'INTEGER':
                                            case 'NUMBER':
                                                scope.fieldValue[current.column.name] = parseFloat(defValue);
                                                break;
                                            case 'BOOLEAN':
                                                scope.fieldValue[current.column.name] = eval(defValue);
                                                break;
                                            default:
                                                scope.fieldValue[current.column.name] = defValue;
                                        }
                                    }
                                } else {
                                    var initializationFieldName = _.find(initialDataFields, function(fieldName){
                                        return current.column.field && fieldName === current.column.standardName;
                                    });
                                    if (initializationFieldName)
                                        scope.fieldValue[current.column.name] = scope.item[initializationFieldName];
                                    else
                                        scope.fieldValue[current.column.name] = null;
                                }
                            });

                            // Checking if there is any field in the parent that should be replaced in the sql where of the child fields
                            if (parentItem) {
                                _.each(scope.allFields, function (field) {
                                    _.each(parentItem.fields, function (current) {
                                        if (field.column.reference.rtype == 'TABLEDIR') {
                                            if (!field.column.referenceValue) {
                                                field.column.referenceValue = cacheService.getReference(field.column.idReferenceValue);
                                            }
                                            if (field.column.referenceValue.info.sqlwhere && cacheService.isFieldInConditional(current, field.column.referenceValue.info.sqlwhere)) {
                                                if (field.runtime.sqlwhere === undefined || !cacheService.isCompleteConditional(field.runtime.sqlwhere)) {
                                                    field.runtime.sqlwhere = field.column.referenceValue.info.sqlwhere;
                                                    field.runtime.sqlwhere = cacheService.replaceSQLConditional(current, parentItem.item[current.column.standardName], field.runtime.sqlwhere);
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                            scope.service = null;
                            try {
                                scope.service = autogenService.getAutogenService(scope.adTab.table);
                            } catch (e) {
                                // TODO: Error handling
                                $log.error(e);
                                scope.initialLoading = false;
                                defered.reject();
                            }
                            if (scope.service) {
                                if (scope.isEditing) {
                                    var storedFormInfo = cacheService.getFormInfo(scope.tabGeneratorId, scope.rowId);
                                    if (storedFormInfo != null) {
                                        scope.item = storedFormInfo.item;
                                        scope.fieldValue = storedFormInfo.fieldValue;
                                        scope.checkPasswordField();
                                        _.each(scope.allFields, function (current) {
                                            if (current.column.primaryKey) {
                                                cacheService.setPrimaryKey(current.column.standardName, scope.item[current.column.standardName])
                                            }
                                        });
                                        cacheService.storeFormItem({
                                                idWindow: scope.windowTabInfo.idWindow,
                                                tablevel: scope.windowTabInfo.tablevel
                                            },
                                            scope.item,
                                            scope.allFields,
                                            scope.adTab
                                        );
                                        finishLoading();
                                        defered.resolve();
                                    } else {
                                        var initFields = function () {
                                            cacheService.storeFormItem({
                                                    idWindow: scope.windowTabInfo.idWindow,
                                                    tablevel: scope.windowTabInfo.tablevel
                                                },
                                                scope.item,
                                                scope.allFields,
                                                scope.adTab
                                            );
                                            scope.fieldReadOnlyRuntime = [];
                                            _.each(scope.allFields, function (current) {
                                                current.readonly = (current.readonly || scope.adTab.runtime.readonly || scope.forceReadOnly);
                                                if (current.column) {
                                                    if (current.column.readonlylogic) {
                                                        var evalReadOnly = scope.$eval(cacheService.replaceJSConditional(current, scope.fieldValue[current.column.name], current.column.readonlylogic, scope.fieldValue));
                                                        current.readonly = evalReadOnly !== undefined && evalReadOnly !== false && evalReadOnly !== 0;
                                                        scope.fieldReadOnlyRuntime[current.name] = current.readonly;
                                                    }
                                                    if (current.column.primaryKey) {
                                                        cacheService.setPrimaryKey(current.column.standardName, scope.item[current.column.standardName])
                                                    }
                                                    if (current.column.ctype == "DATE" &&
                                                        scope.item[current.column.standardName] !== undefined &&
                                                        scope.item[current.column.standardName] !== null) {
                                                        scope.fieldValue[current.column.name] = moment(scope.item[current.column.standardName]).toDate();
                                                    } else {
                                                        scope.fieldValue[current.column.name] = scope.item[current.column.standardName];
                                                        scope.fieldValue['KEY_' + current.column.name] = scope.item[current.column.standardName];
                                                        if (current.column.reference && current.column.reference.rtype === "LISTMULTIPLE") {
                                                            if (scope.item[current.column.standardName] !== undefined &&
                                                                scope.item[current.column.standardName] !== null) {
                                                                scope.fieldValue['ARRAY_' + current.column.name] = _.split(scope.item[current.column.standardName], ";");
                                                            }
                                                            else {
                                                                scope.fieldValue['ARRAY_' + current.column.name] = [];
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                            hookService.execHooks('AD_AfterLoadItem', { tab: scope.adTab, item: scope.item, scope: scope }).then(function () {
                                                finishLoading();
                                                defered.resolve();
                                            });
                                        };
                                        // Checking data in cache
                                        var initialItem = tabManagerService.getInitialFormItem(scope.tabGeneratorId);
                                        if (initialItem && initialItem.id === scope.rowId) {
                                            scope.item = initialItem;
                                            initFields();
                                        } else {
                                            // Falling back to actually loading from server if not present in cache
                                            // Perform data request
                                            scope.service.get(
                                                {
                                                    idClient: cacheService.loggedIdClient(),
                                                    id: scope.rowId
                                                },
                                                function (data) {
                                                    scope.item = data;
                                                    initFields();
                                                },
                                                function (err) {
                                                    $log.error(err);
                                                    scope.initialLoading = false;
                                                    // TODO: Error handling
                                                    defered.reject();
                                                }
                                            );
                                        }
                                    }
                                } else {
                                    var pk = _.find(scope.allFields, function (current) {
                                        return current.column.primaryKey;
                                    });
                                    if (pk) {
                                        cacheService.setPrimaryKey(pk.standardName, null);
                                    }
                                    finishLoading();
                                    defered.resolve();
                                }
                            }
                            return promise;
                        };

                        /**
                         * Initializes Password field value to an empty string.
                         * This method will make sense only when editing an user, hence the need to check
                         * if we are editing the tab that matches the user editing tab form
                         */
                        scope.checkPasswordField = function () {
                            _.each(scope.allFields, function (field) {
                                if (field.column.reference.rtype != 'PASSWORD')
                                    return;
                                scope.fieldValue[field.column.standardName] = null;
                                field.column.mandatory = field.column.mandatory && !scope.isEditing;
                            });
                        };

                        /**
                         * Sets the sending password as an md5 hash.
                         * This method will make sense only when editing an user, hence the need to check
                         * if we are editing the tab that matches the user editing tab form
                         */
                        scope.checkPasswordToSend = function () {
                            _.each(scope.allFields, function (field) {
                                if (field.column.reference.rtype != 'PASSWORD')
                                    return;
                                if (scope.item[field.column.standardName] && scope.item[field.column.standardName] != "")
                                    scope.item[field.column.standardName] = hex_md5(scope.item[field.column.standardName]);
                            });
                        };

                        /**
                         * Checks the fields marked as "UsedInChild" in the parent
                         * and the fields marked as "LinkParent" in the child
                         */
                        scope.checkLinkColumns = function () {
                            for (var i = 0; i < scope.windowTabInfo.tablevel; i++) {
                                var parentItem = cacheService.getFormItem({
                                    idWindow: scope.windowTabInfo.idWindow,
                                    tablevel: scope.windowTabInfo.tablevel - (i + 1)
                                });
                                if (parentItem) {
                                    _.each(scope.allFields, function (field) {
                                        if (field.column && field.column.linkParent && parentItem.item[field.column.standardName]) {
                                            var parent = parentItem;
                                            cacheService.loadReferenceData(field.column.idReferenceValue).then(function () {
                                                var ref = cacheService.getReference(field.column.idReferenceValue);
                                                var foundField = _.find(parent.fields, function (fieldData) {
                                                    return fieldData.idColumn == ref.info.idDisplay;
                                                });
                                                if (foundField) {
                                                    scope.formName = parent.item[foundField.column.standardName];
                                                }
                                            })
                                        }
                                    });
                                    // Checking "linkParent" restriction only when this is a new Item
                                    if (!scope.isEditing) {
                                        _.each(scope.allFields, function (field) {
                                            if (field.column && field.column.linkParent && parentItem.item[field.column.standardName]) {
                                                scope.fieldValue[field.column.name] = parentItem.item[field.column.standardName];
                                                scope.changeValue(field);
                                            }
                                        });
                                    }
                                }
                            }
                            scope.checkLinkColumnValues(false);
                        };

                        /**
                         * Checks and assigns the values of the parent item's field that are marekd
                         * as "useInChild"
                         *
                         * @param ignoreChange If true, it won't trigger a change check cycle
                         */
                        scope.checkLinkColumnValues = function (ignoreChange) {
                            var parentItem;
                            for (var i = 0; i < scope.windowTabInfo.tablevel; i++) {
                                parentItem = cacheService.getFormItem({
                                    idWindow: scope.windowTabInfo.idWindow,
                                    tablevel: scope.windowTabInfo.tablevel - (i + 1)
                                });
                                if (parentItem) {
                                    _.each(parentItem.fields, function (field) {
                                        if (field.usedInChild) {
                                            var any = _.find(scope.allFields, function (current) {
                                                return current.column.name == field.column.name;
                                            });
                                            if (any) {
                                                scope.fieldValue[any.column.name] = parentItem.item[any.column.standardName];
                                                scope.item[any.column.standardName] = parentItem.item[any.column.standardName];
                                                if (!ignoreChange)
                                                    scope.changeValue(any);
                                            } else {
                                                scope.allFields.push({
                                                    column: field.column,
                                                    display: false,
                                                    displayed: false,
                                                    standardName: field.standardName
                                                });
                                                scope.fieldValue[field.column.name] = parentItem.item[field.column.standardName];
                                                scope.item[field.column.standardName] = parentItem.item[field.column.standardName];
                                                if (!ignoreChange)
                                                    scope.changeValue(field);
                                            }
                                        }
                                    });
                                }
                            }

                            parentItem = cacheService.getFormItem({
                                idWindow: scope.windowTabInfo.idWindow,
                                tablevel: scope.windowTabInfo.tablevel - 1
                            });
                            var tableField = _.find(scope.allFields, function (current) {
                                return current.column.standardName === "idTable";
                            });
                            var idRowField = _.find(scope.allFields, function (current) {
                                return current.column.standardName === "idRow";
                            });
                            if (parentItem) {
                                if (scope.adTab.idParentTable && tableField) {
                                    scope.fieldValue["id_table"] = parentItem.tab.idTable;
                                    scope.item["idTable"] = parentItem.tab.idTable;
                                }
                                if (idRowField) {
                                    scope.fieldValue["id_row"] = parentItem.item.id;
                                    scope.item["idRow"] = parentItem.item.id;
                                }
                            }
                        };

                        /**
                         * Prepare form data to send
                         *
                         * @returns {boolean}
                         */
                        scope.prepareToSend = function () {
                            _.each(scope.allFields, function (current) {
                                var columName = current.column.standardName;
                                scope.item[columName] = scope.fieldValue[current.column.name];
                                if (scope.item[columName] === undefined || scope.item[columName] === "") {
                                    scope.item[columName] = null;
                                }
                                if (current.column.ctype == 'DATE' && !moment(scope.item[columName]).isValid() && scope.item[columName] != null) {
                                    scope.item[columName] = commonService.getMomentDate(scope.item[columName]);
                                }
                                if ((scope.item[columName]) && (current.column.reference.rtype == 'TABLE' || current.column.reference.rtype == 'SEARCH')) {
                                    scope.item['DISPLAY_' + columName] = scope.item[columName];
                                    if (_.has(scope.fieldValue, 'KEY_' + current.column.name)) {
                                        scope.item[columName] = scope.fieldValue['KEY_' + current.column.name];
                                    }
                                }
                                if (current.column.reference.rtype == 'YESNO' && !(scope.item[columName])) {
                                    scope.item[columName] = false;
                                }

                                if (current.column.primaryKey && scope.item.id && !scope.item[columName]) {
                                    scope.item[columName] = scope.item.id;
                                }
                            });
                            if (scope.adTab.idParentTable) {
                                var parentTab = _.find(scope.associatedWindow.tabs, function (current) {
                                    return current.tab.idTable === scope.adTab.idParentTable;
                                });
                                if (parentTab) {
                                    scope.item['idTable'] = parentTab.tab.idTable;
                                } else {
                                    scope.item['idTable'] = scope.adTab.idTable;
                                }
                            }

                            // Load FK
                            var result = cacheService.getPrimaryKey();
                            _.each(result, function (current) {
                                if (!scope.item[current.name]) {
                                    scope.item[current.name] = current.value;
                                }
                            });
                            if (scope.linkData) {
                                var keys = _.keys(scope.linkData);
                                _.each(keys, function (current) {
                                    scope.item[current] = scope.linkData[current];
                                });
                            }

                            if ((scope.parentTab && (scope.parentTab.ttype == 'IMAGE' || scope.parentTab.ttype == 'FILE') && parentItem) ||
                                scope.adTab.idParentTable) {
                                scope.item.idTable = parentItem.tab.idTable;
                            }
                            scope.checkPasswordToSend();
                        };

                        /**
                         * Restore TABLE and SEARCH values
                         */
                        scope.restoreValues = function () {
                            _.each(scope.allVisibleFields, function (current) {
                                var columName = current.column.standardName;
                                if ((scope.item[columName]) && (current.column.reference.rtype == 'TABLE' || current.column.reference.rtype == 'SEARCH')) {
                                    scope.item[columName] = scope.item['DISPLAY_' + columName];
                                }
                            });
                        };

                        /**
                         * Persist data into the table
                         * @param callback Callback function to call. If callback is defined, this is the function that will be called if the save is successful, not the usual event to notify the closing of the form
                         */
                        scope.save = function (callback) {
                            scope.tableForm.$dirty = true;
                            if (scope.tableForm.$valid && scope.service) {
                                scope.prepareToSend();
                                var containsFiles = false;
                                // Checking if any field was generated as <input type="file"/>
                                _.each(scope.allFields, function (field) {
                                    if (field.column.reference && (field.column.reference.rtype == "IMAGE" || field.column.reference.rtype == "FILE")) {
                                        containsFiles = true;
                                    }
                                });
                                hookService.execHooks('AD_BeforeSaveItem', { tableName: scope.adTab.table.name, item: scope.item, scope: scope }).then(function () {
                                    if (scope.isEditing) {
                                        var methodUpdate = scope.service.update;
                                        if (containsFiles) {
                                            methodUpdate = scope.service.updateFile;
                                        }
                                        if (!scope.item.idClient) {
                                            if (parentItem) {
                                                scope.item.idClient = parentItem.item.idClient;
                                            } else {
                                                scope.item.idClient = cacheService.loggedIdClient();
                                            }
                                        }
                                        // Update the row
                                        methodUpdate(scope.item, function (data) {
                                            scope.notifyItemSaved();
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            cacheService.clearFormInfo(scope.tabGeneratorId);
                                            if (callback) {
                                                callback();
                                            } else {
                                                var event = {
                                                    tabGeneratorId: scope.tabGeneratorId,
                                                    tablevel: scope.windowTabInfo.tablevel,
                                                    operation: 'UPDATE',
                                                    itemId: scope.rowId
                                                };
                                                if (!scope.subItem) {
                                                    $rootScope.$broadcast(app.eventDefinitions.CANCEL_GLOBAL_ITEM_EDIT, event);
                                                    $rootScope.$broadcast('cancel_to_parent', 1);
                                                } else {
                                                    $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, event);
                                                }
                                            }
                                        }, function (err) {
                                            $log.error('Status:' + err.status + ', Error: ' + err.statusText);
                                            if (err.data.objectErrors && err.data.objectErrors.length > 0 && err.data.objectErrors[0].code === "APPROVAL_NEED"){
                                                scope.privilegeToValidate = err.data.objectErrors[0].message.split('$')[0];
                                                scope.displayMessage = err.data.objectErrors[0].message.split('$')[1];
                                                $('#' + scope.authorizationDialogId).modal();
                                            } else {
                                                scope.restoreValues();
                                                $rootScope.showValidationsErrors(err, scope.allFields);
                                            }
                                        });
                                    } else {
                                        _.each(scope.allFields, function (current) {
                                            if (current.column.primaryKey && scope.item[current.column.standardName]) {
                                                scope.item.id = scope.item[current.column.standardName];
                                            }
                                        });

                                        var parentItem = cacheService.getFormItem({
                                            idWindow: scope.windowTabInfo.idWindow,
                                            tablevel: scope.windowTabInfo.tablevel - 1
                                        });
                                        if (!scope.item.idClient) {
                                            if (parentItem) {
                                                scope.item.idClient = parentItem.item.idClient;
                                            } else {
                                                scope.item.idClient = cacheService.loggedIdClient();
                                            }
                                        }
                                        // Create the row
                                        if (scope.adTab.table.name == 'ad_table') {
                                            scope.item.idTable = null;
                                        }
                                        if (scope.adTab.table.name == 'ad_client') {
                                            scope.item.idClient = null;
                                        }
                                        if (scope.adTab.table.name == 'ad_module') {
                                            scope.item.idModule = null;
                                        }

                                        var methodCreate = containsFiles ? scope.service.createFile : scope.service.create;

                                        methodCreate(scope.item,
                                            function (data) {
                                                scope.rowId = data.id[0] == '"' ? JSON.parse(data.id) : data.id;
                                                scope.isEditing = true;
                                                scope.item.id = scope.rowId;
                                                _.each(scope.allFields, function (current) {
                                                    if (current.column.primaryKey) {
                                                        scope.item[current.column.standardName] = scope.rowId;
                                                        scope.fieldValue[current.column.name] = scope.rowId;
                                                    }
                                                });
                                                cacheService.storeFormItem({
                                                        idWindow: scope.windowTabInfo.idWindow,
                                                        tablevel: scope.windowTabInfo.tablevel
                                                    },
                                                    scope.item,
                                                    scope.allFields,
                                                    scope.adTab
                                                );
                                                scope.notifyItemSaved();
                                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                hookService.execHooks('AD_AfterCreateItem', { tab: scope.adTab, item: scope.item, scope: scope }).then(function () {
                                                    if (callback) {
                                                        callback();
                                                    } else {
                                                        if (scope.otherTabs == 0) {
                                                            cacheService.clearFormInfo(scope.tabGeneratorId);
                                                            var event = {
                                                                tabGeneratorId: scope.tabGeneratorId,
                                                                tablevel: scope.windowTabInfo.tablevel,
                                                                operation: 'CREATE',
                                                                itemId: scope.rowId
                                                            };
                                                            if (!scope.subItem) {
                                                                $rootScope.$broadcast(app.eventDefinitions.CANCEL_GLOBAL_ITEM_EDIT, event);
                                                                $rootScope.$broadcast('cancel_to_parent', 1);
                                                            } else {
                                                                $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, event);
                                                            }
                                                        } else {
                                                            scope.restoreValues();
                                                            cacheService.refreshCache(scope.adTab.table.name, data.id);
                                                            $rootScope.$broadcast(app.eventDefinitions.GLOBAL_ITEM_CREATED, {
                                                                windowGeneratorId: scope.adTab.idWindow,
                                                                idTab: scope.adTab.idTab,
                                                                item: scope.item
                                                            });
                                                        }
                                                    }
                                                });
                                            }, function (err) {
                                                $log.error('Status:' + err.status + ', Error: ' + err.statusText);
                                                if (err.data.objectErrors && err.data.objectErrors.length > 0 && err.data.objectErrors[0].code === "APPROVAL_NEED"){
                                                    scope.privilegeToValidate = err.data.objectErrors[0].message.split('$')[0];
                                                    scope.displayMessage = err.data.objectErrors[0].message.split('$')[1];
                                                    $('#' + scope.authorizationDialogId).modal();
                                                } else {
                                                    scope.restoreValues();
                                                    $rootScope.showValidationsErrors(err, scope.allFields);
                                                }
                                            }
                                        );
                                    }
                                }).catch(function (err) {
                                    $log.error(err);
                                    if (err && err.error)
                                        commonService.showRejectNotification(err.error);
                                    else
                                        commonService.showRejectNotification($rootScope.getMessage('AD_msgErrorCheckFields'));
                                });
                            } else {
                                commonService.showRejectNotification($rootScope.getMessage('AD_msgErrorCheckFields'));
                            }
                        };

                        /**
                         * Recalls the backend with the approval information added
                         * @param {object}  result Validation result
                         * @param {boolean} result.validated True if the user was validated. False i.o.c.
                         * @param {string}  result.idUser Id of the user that was validated.
                         */
                        scope.doOnApproval = function(result){
                            $('#' + scope.authorizationDialogId).modal('hide');
                            if (result.validated) {
                                if (!scope.item.approvals){
                                    scope.item.approvals = [];
                                }
                                scope.item.approvals.push({
                                    idUser: result.idUser,
                                    idClient: cacheService.loggedIdClient(),
                                    privilege: scope.privilegeToValidate,
                                    idApproval: commonService.generateUID(),
                                    idTable: scope.adTab.table.idTable,
                                    action: "TABLE",
                                    rowkey: scope.item.id
                                });
                                scope.save();
                            }
                        };

                        /**
                         * Performs a refresh of the readonly property
                         */
                        scope.notifyItemSaved = function () {
                            if (scope.adTab.table.name === 'ad_client'){
                                cacheService.clearClientCache();
                            }
                            scope.fieldReadOnlyRuntime = [];
                            _.each(scope.allFields, function (current) {
                                if (current.column) {
                                    if (current.column.readonlylogic) {
                                        var evalReadOnly = scope.$eval(cacheService.replaceJSConditional(current, null, current.column.readonlylogic, scope.fieldValue));
                                        current.readonly = evalReadOnly !== undefined && evalReadOnly !== false && evalReadOnly !== 0;
                                        scope.fieldReadOnlyRuntime[current.name] = current.readonly;
                                    }
                                    if (current.displaylogic) {
                                        if (!current.runtime.displaylogic) {
                                            current.runtime.displaylogic = current.displaylogic;
                                        }
                                        current.runtime.displaylogic = cacheService.replaceJSConditional(null, null, current.runtime.displaylogic);
                                        if (cacheService.isCompleteConditional(current.runtime.displaylogic)) {
                                            current.display = eval(current.runtime.displaylogic);
                                            current.runtime.displaylogic = current.displaylogic;
                                        }
                                    }
                                }
                            });
                        };

                        /**
                         * Find the position of group in the group array
                         * @param groups
                         * @param idGroup
                         */
                        scope.posGroup = function (groups, idGroup) {
                            var outIndex = -1;
                            _.each(groups, function (current, index) {
                                if (current.id == idGroup) {
                                    outIndex = index;
                                }
                            });
                            return outIndex;
                        };

                        scope.filterSearchData = function () {
                            scope.loadSearchData(scope.currentIdReferenceValue);
                        };

                        scope.loadSearchData = function (idReferenceValue) {
                            var table = scope.complicateVisualFields[idReferenceValue].table;
                            var _service = null;
                            try {
                                _service = autogenService.getAutogenService(table);
                            } catch (e) {
                                $log.error(e);
                            }
                            if (_service) {
                                var sort = '';
                                if (scope.tableOrderByField != '') {
                                    sort = '[{ "property":"' + scope.tableOrderByField + '","direction":"' + (scope.tableReverseSort ? 'DESC' : 'ASC') + '" }]';
                                }
                                // Checking filters
                                var query = "";
                                if (scope.complicateVisualFields[idReferenceValue].columns) {
                                    _.each(scope.complicateVisualFields[idReferenceValue].columns, function (current, index) {
                                        if (scope.complicateVisualFields[idReferenceValue].filterValues[current.name]) {
                                            query += (current.name + '=%' + scope.complicateVisualFields[idReferenceValue].filterValues[current.name] + "%,");
                                        }
                                    });
                                } else {
                                    query += scope.searchFilter;
                                }

                                // Loading data
                                $log.info("Requesting data from server for table: " + table.name + " with query: " + query);
                                _service.query(
                                    {
                                        idClient: cacheService.loggedIdClient(),
                                        q: query,
                                        sort: sort,
                                        page: page,
                                        limit: scope.referenceData.referencePageSize
                                    },
                                    function (data4) {
                                        //Armando los datos en el scope
                                        scope.complicateVisualFields[idReferenceValue].objectValues = data4.content;
                                        scope.referenceTotalElements = data4.totalElements;
                                    },
                                    function (err) {
                                        $log.error(err);
                                    }
                                );
                            }
                        };

                        /**
                         * Notify a field of change on other
                         *
                         * @param field Field notified
                         * @param changedField Change field
                         */
                        scope.notifyFieldChange = function (field, changedField) {
                            if (field.displaylogic) {
                                if (!field.runtime.displaylogic) {
                                    field.runtime.displaylogic = field.displaylogic;
                                }
                                field.runtime.displaylogic = cacheService.replaceJSConditional(changedField, scope.fieldValue[changedField.column.name], field.runtime.displaylogic);
                                if (!cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                                    _.each(scope.allFields, function (current) {
                                        field.runtime.displaylogic = cacheService.replaceJSConditional(current, scope.fieldValue[current.column.name], field.runtime.displaylogic);
                                    });
                                }
                                if (cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                                    field.display = eval(field.runtime.displaylogic);
                                    field.runtime.displaylogic = field.displaylogic;
                                }
                            }
                            if (field.column.readonlylogic) {
                                if (!field.runtime.readonlylogic) {
                                    field.runtime.readonlylogic = field.column.readonlylogic;
                                }
                                field.runtime.readonlylogic = cacheService.replaceJSConditional(changedField, scope.fieldValue[changedField.column.name], field.runtime.readonlylogic);
                                if (!cacheService.isCompleteConditional(field.runtime.readonlylogic)) {
                                    _.each(scope.allFields, function (current) {
                                        field.runtime.readonlylogic = cacheService.replaceJSConditional(current, scope.fieldValue[current.column.name], field.runtime.readonlylogic);
                                    });
                                }
                                if (cacheService.isCompleteConditional(field.runtime.readonlylogic)) {
                                    field.readonly = eval(field.runtime.readonlylogic);
                                    field.runtime.readonlylogic = field.column.readonlylogic;
                                }
                            }
                            if (field.column.reference.rtype == 'TABLEDIR') {
                                if (!field.column.referenceValue) {
                                    field.column.referenceValue = cacheService.getReference(field.column.idReferenceValue);
                                }
                                if (field.column.referenceValue.info.sqlwhere && cacheService.isFieldInConditional(changedField, field.column.referenceValue.info.sqlwhere)) {
                                    field.runtime.sqlwhere = field.column.referenceValue.info.sqlwhere;
                                    if (!field.runtime.sqlwhere || field.runtime.sqlwhere != field.column.referenceValue.info.sqlwhere) {
                                        field.runtime.sqlwhere = field.column.referenceValue.info.sqlwhere;
                                    }
                                    var value = scope.fieldValue[changedField.column.name];
                                    if (changedField.column.idReferenceValue && (changedField.column.reference.rtype == 'TABLE' || changedField.column.reference.rtype == 'SEARCH')) {
                                        if (scope.fieldValue['KEY_' + changedField.column.name]) {
                                            value = scope.fieldValue['KEY_' + changedField.column.name];
                                        }
                                    }
                                    if (value) {
                                        field.runtime.sqlwhere = cacheService.replaceSQLConditional(changedField, value, field.runtime.sqlwhere);
                                    }
                                }
                            }
                        };

                        /**
                         * Notify a change value on a field to row elements
                         *
                         * @param fieldRows Row of fields
                         * @param field Change field
                         */
                        scope.changeValueRow = function (fieldRows, field) {
                            $rootScope.$broadcast(app.eventDefinitions.TAB_VISUALIZATION_NOTIFICATION, {
                                idWindow: scope.windowTabInfo.idWindow,
                                field: field,
                                value: scope.fieldValue[field.column.name]
                            });
                            _.each(fieldRows, function (fRow) {
                                _.each(fRow.fields, function (f) {
                                    if (f.standardName != field.standardName) {
                                        scope.notifyFieldChange(f, field);
                                    }
                                });
                            });
                            _.each(scope.groups, function (group) {
                                scope.notifyFieldGroupChange(group, field);
                            });
                            _.each(scope.buttons, function (btn) {
                                scope.notifyFieldChange(btn, field);
                            });
                        };

                        /**
                         * Notify a group of change on a field
                         *
                         * @param group Group notified
                         * @param changedField Changed field
                         */
                        scope.notifyFieldGroupChange = function (group, changedField) {
                            if (group.displaylogic) {
                                if (!group.runtime)
                                    group.runtime = {};
                                if (!group.runtime.displaylogic) {
                                    group.runtime.displaylogic = group.displaylogic;
                                }
                                group.runtime.displaylogic = cacheService.replaceJSConditional(changedField, scope.fieldValue[changedField.column.name], group.runtime.displaylogic);
                                if (cacheService.isCompleteConditional(group.runtime.displaylogic)) {
                                    group.display = eval(group.runtime.displaylogic);
                                    group.runtime.displaylogic = group.displaylogic;
                                }
                            }
                        };

                        /**
                         * Change value of field
                         *
                         * @param field Field information
                         */
                        scope.changeFile = function (value, field) {
                            this.parentNode.nextSibling.value = this.value
                            scope.changeValue(field);
                        };

                        /**
                         * Change value of field
                         *
                         * @param field Field information
                         */
                        scope.changeValue = function (field) {
                            scope.changeValueRow(scope.fieldRows, field);
                            _.each(scope.groups, function (group) {
                                scope.changeValueRow(group.fieldRows, field);
                            });
                            var value = scope.fieldValue[field.name];
                            if ((field.rtype == 'TABLE' || field.rtype == 'SEARCH') && scope.fieldValue['KEY_' + field.name]) {
                                value = scope.fieldValue['KEY_' + field.name];
                            }
                            if (field.onchangefunction && !scope.initialLoading && scope.fieldValue['old_' + field.standardName] != value) {
                                var table = cacheService.getTableByName(field.tableName);
                                if (table && onChangeFunction) {
                                    onChangeFunction[table.module.restPath][field.onchangefunction].call(this, scope, $log, cacheService, autogenService, commonService, utilsService);
                                }
                            }
                            scope.fieldValue['old_' + field.standardName] = value;
                            scope.persistFormData();
                        };

                        scope.setFieldValue = function (name, value) {
                            var field = _.find(scope.allFields, function (fld) {
                                return fld.standardName == name;
                            });
                            if (field) {
                                scope.fieldValue[field.name] = value;
                                scope.item[name] = value;
                                scope.changeValue(field);
                            }
                        };

                        /**
                         * Persists the form information in cache for tab changing purposes
                         */
                        scope.persistFormData = function () {
                            cacheService.storeFormInfo(scope.tabGeneratorId, scope.item, scope.fieldValue, scope.adTab);
                        };

                        scope.loadFieldValues = function (fieldReferenceValueId, type, result) {
                            if (scope.complicateVisualFields[fieldReferenceValueId]) {
                                scope.complicateVisualFields[fieldReferenceValueId].key = result.colKey.standardName;
                                scope.complicateVisualFields[fieldReferenceValueId].display = result.colDisplay.standardName;
                                scope.complicateVisualFields[fieldReferenceValueId].table = result.table;
                                scope.complicateVisualFields[fieldReferenceValueId].sqlwhere = result.sqlwhere;
                                scope.complicateVisualFields[fieldReferenceValueId].sqlorderby = result.sqlorderby;
                                scope.complicateVisualFields[fieldReferenceValueId].objectValues = result.data.content;
                                scope.complicateVisualFields[fieldReferenceValueId].displayCaption = result.colDisplay.name;

                                scope.referenceTotalElements = result.data.totalElements;
                                if (scope.complicateVisualFields[fieldReferenceValueId].type == 'SEARCH') {
                                    var tValue = _.find(result.data.content, function (element) {
                                        var a = element[scope.complicateVisualFields[fieldReferenceValueId].key];
                                        var b = scope.fieldValue[scope.complicateVisualFields[fieldReferenceValueId].columnName];
                                        return a == b;
                                    });
                                    if (tValue) {
                                        scope.fieldValue['KEY_' + scope.complicateVisualFields[fieldReferenceValueId].columnName] = tValue[scope.complicateVisualFields[fieldReferenceValueId].key];
                                        scope.fieldValue[scope.complicateVisualFields[fieldReferenceValueId].columnName] = tValue[scope.complicateVisualFields[fieldReferenceValueId].display];
                                    }
                                }
                            }
                        };

                        /**
                         * Loads and builds form structure
                         */
                        scope.loadForm = function () {
                            if (scope.tabGeneratorId !== undefined && scope.tabGeneratorId != "") {
                                var loadTime = Date.now();
                                cacheService.loadTab(scope.tabGeneratorId).then(function (data) {
                                    scope.adTab = data;
                                    if (scope.adTab.runtime == undefined)
                                        scope.adTab.runtime = { readonly: false };
                                    scope.adTab.runtime.readonly = (scope.adTab.runtime.readonly || scope.adTab.uipattern == 'READONLY' || scope.forceReadOnly);
                                    scope.editReferences = !scope.disableInline && !(scope.adTab.runtime.readonly || scope.forceReadOnly);
                                    cacheService.loadWindow(scope.adTab.idWindow).then(function (data) {
                                        scope.associatedWindow = data;
                                        scope.formName = data.name;
                                        scope.adTab.table = cacheService.getTable(scope.adTab.idTable);
                                        hookService.execHooks('AD_AfterLoadTab', { tab: scope.adTab }).then(function () {
                                            scope.allFields = _.sortBy(scope.adTab.fields, function (current) {
                                                return current.seqno;
                                            });
                                            _.each(scope.allFields, function (current) {
                                                current.tableName = scope.adTab.table.name;
                                                current.display = true;
                                                current.column = _.find(scope.adTab.table.columns, function (col) {
                                                    return col.idColumn == current.idColumn;
                                                });
                                            });
                                            scope.checkLinkColumnValues(true);

                                            var visible = _.filter(scope.allFields, function (current) {
                                                return current.displayed;
                                            });

                                            // Get defined buttons
                                            _.each(visible, function (field, index) {
                                                if (field.column) {
                                                    if (field.column.reference.rtype == 'BUTTON') {
                                                        field.column.refButton = cacheService.getReference(field.column.idReferenceValue);
                                                        if (field.column.refButton.info.showMode !== 'IN_FORM') {
                                                            field.ignoreButton = true;
                                                        }
                                                    }
                                                }
                                            });
                                            // Excluding buttons where SHOW_MODE != IN_FORM
                                            visible = _.filter(visible, function (field) {
                                                return !field.ignoreButton;
                                            });

                                            // Excluding buttons where SHOW_MODE != IN_FORM
                                            scope.buttons = _.filter(visible, function (field) {
                                                return field.column && field.column.reference && field.column.reference.rtype === "BUTTON";
                                            });

                                            _.each(visible, function (current, index) {
                                                scope.allVisibleFields.push(current);
                                                if (current.column) {
                                                    scope.posField.push(index);

                                                    if (current.column.primaryKey) {
                                                        scope.primaryFields.push(current);
                                                    }
                                                    if (!current.idFieldGroup) {
                                                        current.bootstrapCol = (12 / scope.adTab.columns) * current.span;
                                                        scope.fields.push(current);
                                                    } else {
                                                        if (scope.posGroup(scope.groups, current.idFieldGroup) == -1) {
                                                            var group = {
                                                                name: current.name,
                                                                id: current.idFieldGroup,
                                                                fields: [],
                                                                fieldRows: []
                                                            };
                                                            group.fields.push(current);
                                                            scope.groups.push(group);
                                                        } else {
                                                            var pos = scope.posGroup(scope.groups, current.idFieldGroup);
                                                            scope.groups[pos].fields.push(current);
                                                        }
                                                    }
                                                    if (current.column.reference.rtype == 'SEARCH') {
                                                        // Initializing structure to store select list values
                                                        scope.complicateVisualFields[current.column.idReferenceValue] =  {
                                                            fieldReferenceValueId: current.column.idReferenceValue,
                                                            type: current.column.reference.rtype,
                                                            key: null,
                                                            display: null,
                                                            columnName: current.column.name,
                                                            objectValues: []
                                                        };
                                                    }
                                                }
                                            });

                                            // Loading the group name for each group
                                            _.each(scope.groups, function (current, index) {
                                                var fieldGroup = cacheService.getFieldGroup(current.id);
                                                var pos = scope.posGroup(scope.groups, current.id);
                                                scope.groups[pos].name = fieldGroup.name;
                                                scope.groups[pos].display = true;
                                                scope.groups[pos].columns = fieldGroup.columns;
                                                scope.groups[pos].collapsed = fieldGroup.collapsed;
                                                scope.groups[pos].displaylogic = fieldGroup.displaylogic;
                                                _.each(scope.groups[pos].fields, function (current) {
                                                    current.bootstrapGroupColumns = (12 / (fieldGroup.columns ? fieldGroup.columns : scope.adTab.columns)) * current.span;
                                                    // current.groupColumns = fieldGroup.columns;
                                                });
                                            });

                                            // Here we define the rows according to the rules.
                                            // A new row is started if any of the following applies:
                                            //      1. If it is the first element
                                            //      2. If the field explicitly states it starts a new row
                                            //      3. If there is no more space left in the row
                                            scope.fieldRows = [];
                                            var currentRow;
                                            var availableSpace = 12;
                                            scope.fields = _.sortBy(scope.fields, function (current) {
                                                return current.seqno;
                                            });
                                            _.each(scope.fields, function (current, index) {
                                                if (current.column.reference.rtype === "BUTTON" && !scope.includeButtonsInBootstrap)
                                                    return;
                                                var newRow = {
                                                    fields: []
                                                };
                                                var neededSpace = ((12 / scope.adTab.columns) * current.span);
                                                if (index == 0 || current.startnewrow || availableSpace < neededSpace) {
                                                    currentRow = newRow;
                                                    currentRow.fields.push(current);
                                                    scope.fieldRows.push(currentRow);
                                                    availableSpace = 12 - neededSpace;
                                                } else {
                                                    currentRow.fields.push(current);
                                                    availableSpace -= neededSpace;
                                                }
                                            });

                                            // Organizing group field to show by row
                                            _.each(scope.groups, function (currentG, indexG) {
                                                var currentRowG;
                                                var columnSpaceG = 12;
                                                _.each(currentG.fields, function (currentF, indexF) {
                                                    var newRow = {
                                                        fields: []
                                                    };
                                                    var needcolumnSpaceG = ((12 / (currentG.columns ? currentG.columns : scope.adTab.columns)) * currentF.span);
                                                    if (indexF == 0 || currentF.startnewrow || columnSpaceG < needcolumnSpaceG) {
                                                        currentRowG = newRow;
                                                        currentRowG.fields.push(currentF);
                                                        currentG.fieldRows.push(currentRowG);
                                                        columnSpaceG = 12 - needcolumnSpaceG;
                                                    } else {
                                                        currentRowG.fields.push(currentF);
                                                        columnSpaceG -= needcolumnSpaceG;
                                                    }
                                                });
                                                currentG.fields = null;
                                            });

                                            // Loading data or business row
                                            scope.reloadBusinessData().then(function () {
                                                if (scope.rowId) {
                                                    $rootScope.$broadcast(
                                                        app.eventDefinitions.GLOBAL_ITEM_CREATED,
                                                        {
                                                            windowGeneratorId: scope.adTab.idWindow, item: scope.item,
                                                            idTab: scope.tabGeneratorId
                                                        }
                                                    );
                                                } else {
                                                    hookService.execHooks('AD_AfterNewItem', { tableName: scope.adTab.table.name, fieldValue: scope.fieldValue, scope: scope }).then(function () {

                                                    });
                                                }
                                                $log.debug("loadForm in: " + (Date.now() - loadTime));
                                            }).catch(function (err) {
                                                $log.error(err);
                                            });
                                        }).catch(function (err) {
                                            $log.error(err);
                                        });
                                    });
                                }).catch(function (err) {
                                    $log.error(err);
                                });
                            } else {
                                // TODO: Error handling
                            }
                        };

                        scope.loadForm();

                        scope.getBootrapWidth = function (field, item) {
                            if (field.column.translatable && item[field.standardName] && item.id) {
                                return 7;
                            }
                            if (field.column.translatable || (item[field.standardName] && item.id)) {
                                return 9;
                            }
                            return 12;
                        };

                        scope.deleteImage = function (field, item) {
                            scope.service.deleteImage({
                                idClient: cacheService.loggedIdClient(),
                                id: item.id,
                                fieldName: field.name,
                                file: item[field.standardName]
                            }, function () {
                                scope.fieldValue[field.column.name] = null;
                                scope.item[field.standardName] = null;
                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                            }, function (error) {
                                $log.error('Status: ' + error.status + ', Error: ' + error.statusText);
                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataDeleteError'));
                            });
                        };

                        /**
                         * Shows translation dialog
                         *
                         * @param field Field to be translated
                         */
                        scope.showTranslation = function (field) {
                            scope.translationHasSystem = _.some(Authentication.getLoginData().roles, function (current) {
                                return (current.name == "ROL_SYSTEM") || (current.name == "ROLE_SUPERADMIN");
                            });
                            scope.translationClients = [];
                            if (!scope.translationHasSystem) {
                                scope.translationProperty.translationClientId = cacheService.loggedIdClient();
                                scope.translationProperty.translationInitClientId = cacheService.loggedIdClient();
                            }
                            scope.translationInfos = {};
                            scope.translatableField = field;

                            scope.loadTranslations(cacheService.loggedIdClient()).then(function () {
                                $('#' + scope.translationDialogId).modal();
                            }).catch(function (err) {
                                $log.error(err);
                            });
                        };

                        scope.loadTranslations = function (idClient) {
                            var defered = $q.defer();
                            var promise = defered.promise;
                            var thatScope = scope;

                            scope.translationInfos = {};
                            adTranslationService.query({
                                idClient: idClient,
                                q: "rowkey=" + scope.item.id +
                                ",idTable=" + scope.adTab.table.idTable +
                                ",idColumn=" + scope.translatableField.column.idColumn
                            }, function (data) {
                                scope.translation[scope.translatableField.idField] = {};
                                _.each($rootScope.languages, function (language) {
                                    _.each(data.content, function (translation) {
                                        if (translation.idLanguage === language.idLanguage
                                            && translation.idClient === idClient) {
                                            scope.translationInfos[language.countrycode] = translation;
                                            scope.translation[scope.translatableField.idField][language.countrycode] = translation.translation;
                                        }
                                    });
                                });
                                if (scope.translationProperty.translationClientId !== idClient
                                    || scope.translationProperty.translationClientId === null) {
                                    $timeout(function () {
                                        scope.translationProperty.translationClientId = idClient;
                                        scope.translationProperty.translationInitClientId = idClient;
                                        $log.info('Client ID: ' + thatScope.translationProperty.translationClientId)
                                    }, 0, true);
                                }
                                defered.resolve();
                            }, function (err) {
                                defered.reject(err);
                            });
                            return promise;
                        };

                        /**
                         * Hides translation dialog
                         */
                        scope.cancelTranslation = function () {
                            $('#' + scope.translationDialogId).modal('hide');
                        };

                        /**
                         * Stores translations
                         *
                         * @param field Field to be translated
                         */
                        scope.deleteTranslation = function (field, language) {
                            $.SmartMessageBox({
                                title: $rootScope.getMessage('AD_msgConfirmDelete'),
                                buttons: '[' + $rootScope.getMessage('AD_labelNo') + '][' + $rootScope.getMessage('AD_labelYes') + ']'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === $rootScope.getMessage('AD_labelYes')) {
                                    var translation = scope.translationInfos[language];
                                    adTranslationService.deleteFile(
                                        {
                                            idTranslation: translation.idTranslation,
                                            idClient: scope.translationProperty.translationClientId
                                        },
                                        function (data, data2, data3, data4) {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            scope.translation[field.idField][language] = null;
                                            scope.translationInfos[language] = null;
                                        }, function (data) {
                                            commonService.showRejectNotification($rootScope.getMessage('AD_msgDataDeleteError'));
                                        }
                                    );
                                }
                            });
                            // The following lines are needed to prevent the dialog from
                            // being shown again by pressing Enter
                            var $focused = $(':focus');
                            $focused.blur();
                        };

                        /**
                         * Stores translations
                         *
                         * @param field Field to be translated
                         */
                        scope.doTranslation = function (field) {
                            var counterSent = 0;
                            var counter = 0;
                            _.each($rootScope.languages, function (language) {
                                if (scope.translation[field.idField][language.countrycode] !== null &&
                                    scope.translation[field.idField][language.countrycode] !== undefined) {
                                    counterSent++;
                                }
                            });
                            var methodCreate = adTranslationService.create;
                            var methodUpdate = adTranslationService.update;
                            var methodDelete = adTranslationService.delete;
                            if (field.column.reference && (field.column.reference.rtype == "IMAGE" || field.column.reference.rtype == "FILE")) {
                                methodCreate = adTranslationService.createFile;
                                methodUpdate = adTranslationService.updateFile;
                                methodDelete = adTranslationService.deleteFile;
                            }

                            scope.translationProperty.translationInitClientId = scope.translationProperty.translationClientId;
                            _.each($rootScope.languages, function (language) {
                                var idModule = scope.fieldValue.id_module ? scope.fieldValue.id_module : scope.adTab.table.module.idModule;
                                var request = {
                                    idClient: scope.translationProperty.translationClientId,
                                    idModule: idModule,
                                    idTable: scope.adTab.table.idTable,
                                    idColumn: field.column.idColumn,
                                    active: true,
                                    rowkey: scope.item.id
                                };
                                if (scope.translation[field.idField][language.countrycode]) {
                                    request.translation = scope.translation[field.idField][language.countrycode];
                                    request.idLanguage = language.idLanguage;
                                    if (scope.translationInfos[language.countrycode]) {
                                        request.idTranslation = scope.translationInfos[language.countrycode].idTranslation;
                                        request.created = scope.translationInfos[language.countrycode].created;
                                        request.active = scope.translationInfos[language.countrycode].active;
                                        methodUpdate(
                                            request,
                                            function (data) {
                                                counter++;
                                                if (counter == counterSent) {
                                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                    scope.translationClients = null;
                                                    $('#' + scope.translationDialogId).modal('hide');
                                                }
                                            }, function (data) {
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                            }
                                        );
                                    } else {
                                        request.idTranslation = null;
                                        request.created = null;
                                        request.active = true;
                                        methodCreate(
                                            request,
                                            function (data) {
                                                counter++;
                                                if (counter == counterSent) {
                                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                    $('#' + scope.translationDialogId).modal('hide');
                                                }
                                            }, function (data) {
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                            }
                                        );
                                    }
                                } else if (scope.translationInfos[language.countrycode] && scope.translationInfos[language.countrycode].idTranslation) {
                                    methodDelete(
                                        {
                                            idTranslation: scope.translationInfos[language.countrycode].idTranslation,
                                            idClient: cacheService.loggedIdClient()
                                        },
                                        function () {
                                            scope.translation[field.idField][language] = null;
                                            scope.translationInfos[language] = null;
                                            counter++;
                                            if (counter === counterSent) {
                                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                scope.translationClients = null;
                                                $('#' + scope.translationDialogId).modal('hide');
                                            }
                                        }, function (data) {
                                            commonService.showRejectNotification($rootScope.getMessage('AD_msgDataDeleteError '));
                                        }
                                    );
                                }
                            });
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getFieldAttr = function (field) {
                            return {
                                idReferenceValue: field.column.idReferenceValue,
                                name: field.column.name,
                                idColumn: field.column.idColumn,
                                rtype: field.column.reference.rtype,
                                display: field.display,
                                required: field.column.mandatory,
                                readonly: field.readonly,
                                relatedItem: this.item
                            };
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getListFieldAttr = function (field) {
                            return {
                                idReferenceValue: field.column.idReferenceValue,
                                name: field.column.name,
                                idColumn: field.column.idColumn,
                                rtype: field.column.reference.rtype,
                                readonly: field.readonly,
                                required: field.column.mandatory,
                                tableName: field.tableName
                            };
                        };

                        scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                            var field = _.find(scope.allFields, function (current) {
                                return current.column.idColumn == idColumnData
                            });
                            scope.fieldValue['KEY_' + field.column.name] = fieldValueData;
                            scope.fieldValue[field.column.name] = fieldDisplayData;
                            scope.changeValue(field);
                        };

                        scope.selectMultipleElement = function (idColumnData, fieldValueData, fieldDisplayData, addedData) {
                            var field = _.find(scope.fields, function (current) {
                                return current.column.idColumn === idColumnData
                            });
                            if (field) {
                                if (addedData) {
                                    if (scope.fieldValue['KEY_' + field.column.name] === "" ||
                                        scope.fieldValue['KEY_' + field.column.name] === null ||
                                        scope.fieldValue['KEY_' + field.column.name] === undefined) {
                                        scope.fieldValue['KEY_' + field.column.name] = fieldValueData;
                                        scope.fieldValue[field.column.name] = fieldValueData;
                                        scope.fieldValue['ARRAY_' + field.column.name].push(fieldValueData);
                                    } else {
                                        // Verifying that the element is not already added. Done to prevent recursive calling
                                        if (!_.find(scope.fieldValue['ARRAY_' + field.column.name], function (current) {
                                                return current === fieldValueData;
                                            })) {
                                            scope.fieldValue['KEY_' + field.column.name] += (";" + fieldValueData);
                                            scope.fieldValue['ARRAY_' + field.column.name].push(fieldValueData);
                                            scope.fieldValue[field.column.name] += (";" + fieldValueData);
                                        }
                                    }
                                } else {
                                    var keys = _.split(scope.fieldValue['KEY_' + field.column.name], ";");
                                    var displays = _.split(scope.fieldValue[field.column.name], ";");
                                    keys = _.filter(keys, function (current) {
                                        return current !== fieldValueData;
                                    });
                                    displays = _.filter(displays, function (current) {
                                        return current !== fieldValueData;
                                    });
                                    scope.fieldValue['ARRAY_' + field.column.name] = _.filter(scope.fieldValue['ARRAY_' + field.column.name], function (current) {
                                        return current !== fieldValueData;
                                    });
                                    scope.fieldValue['KEY_' + field.column.name] = _.join(keys, ';');
                                    scope.fieldValue[field.column.name] = _.join(displays, ';');
                                }
                                scope.changeValue(field);
                            }
                        };

                        scope.referenceInlineUpdate = function(field, value){
                            if (value) {
                                scope.fieldValue[field.column.name] = value;
                            }
                            scope.$broadcast(coreConfigService.events.NAVIGATION_REFRESH_REFERENCE, {
                                idColumn: field.idColumn,
                                value: value
                            });
                            if (value){
                                scope.changeValue(field);
                            }
                        };

                        scope.editReference = function (fieldInfo) {
                            var field = fieldInfo.field;
                            var value = field.value;
                            $log.info(value);
                        };

                        scope.doApproval = function(authorizationForm, user, password){
                            authorizationForm.$dirty = true;
                            if (authorizationForm.$valid) {
                                Authentication.validateUser(user, password, scope.privilegeToValidate)
                                    .then(function (result) {
                                        if (result.validated) {
                                            $('#' + scope.authorizationDialogId).modal('hide');
                                            if (!scope.item.approvals){
                                                scope.item.approvals = [];
                                            }
                                            scope.item.approvals.push({
                                                idClient: cacheService.loggedIdClient(),
                                                privilege: scope.privilegeToValidate
                                            });
                                            scope.save();
                                        }
                                    }).catch(function (error) {
                                        $log.error(error);
                                        if (error.code === 403) {
                                            commonService.showError($rootScope.getMessage('AD_GlobalUserNotAuth'));
                                        } else {
                                            commonService.showError($rootScope.getMessage('AD_ErrValidationUnknow'));
                                        }
                                    });
                            }
                        }
                    }
                }
            }
        }
    }]);
});
