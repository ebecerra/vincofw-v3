/**
 * This directive should be used to obtain reference values from TABLE components. The following attributes
 * should be used with the directive. The directive applies ONLY as an attribute:
 *  *   field-attr          The field being processed. Should be a structure with the following information
 *                          {{
 *                              idReferenceValue: Reference value ID,
 *                              name: Field column name
 *                              idColumn: Unique identifier for the field
 *                              rtype: Reference type
 *                              sqlorderby: SQL order clause to use, in case it is needed
 *                           }}
 *  *   fn-on-focus         A callback function to be notified when the component has focus
 *  *   selected-fn-attr    A callback function that the caller scope should implement to receive the selected value
 *                          In order to guarantee the callback function is called, the rest of the following attributes
 *                          (including selected-fn-attr), should be present exactly as presented below:
 *
 *                              selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                              id-column-data-attr="idColumnData"
 *                              field-value-data-attr="fieldValueData"
 *                              field-display-data-attr="fieldDisplayData"
 *  *   sqlwhere-attr       SQL where to apply as an extra filter to the elements
 *  *   related-item        Related item to obtain the filters from (e.g. The item from where obtain the values to be included in the sqlwhere filter)
 *
 * The caller scope should implement the callback function like this:
 *
 *          scope.selectElement = function(idColumnData, fieldValueData, fieldDisplayData){
 *                                   // custom logic goes here
 *                                  };
 *
 *  Example usage
 *
 *      <div class="input input-file">
 *          <span class="button">
 *              <input type="text"
 *                      table-selector
 *                      field-attr="{{getFieldAttr(field)}}"
 *                      item-attr="{{fieldValue}}"
 *                      tab-id-attr="{{scope.tab}}"
 *                      sqlwhere-attr="{{field.runtime.sqlwhere}}"
 *                      selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                      id-column-data-attr="idColumnData"
 *                      field-value-data-attr="fieldValueData"
 *                      fn-on-focus="doComponentEnter(name)"
 *                      field-display-data-attr="fieldDisplayData">
 *              <i class="fa fa-search"></i>
 *          </span>
 *          <input type="text" placeholder="" readonly="" value="">
 *      </div>
 */
define(['app', 'jquery'], function (app, $) {

    'use strict';

    return app.registerDirective('inputTableDirSelector', [
        '$templateRequest', '$compile', '$rootScope', 'cacheService', 'coreConfigService',
        function ($templateRequest, $compile, $rootScope, cacheService,  coreConfigService) {
        return {
            restrict: 'A',
            scope: {
                fieldStr: '@fieldAttr',
                tabIdAttrData: '@tabIdAttr',
                relatedItem: '=',
                parentItem: '=',
                sqlwhereAttr: '@',
                selectedElementFunction: '&selectedFnAttr',
                onFocusFunction: '&fnOnFocus'
            },
            require: 'ngModel',
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {

                        scope.initialized = false;
                        scope.unWatchVisible = scope.$watch(function () {
                                return $(element).is(":visible");
                            },  function(newValue, oldValue){
                                if (newValue && newValue!==oldValue){
                                    scope.initializeElement(true);
                                }
                            },
                            true);

                        scope.initializeElement = function(loadData){
                            if ($(element).is(":visible") && !scope.initialized) {
                                if (scope.unWatchVisible)
                                    scope.unWatchVisible();
                                scope.initialized = true;
                                scope.disabled = 'disabled' in attrs;
                                element.removeAttr('input-table-dir-selector');
                                scope.field = JSON.parse(scope.fieldStr);
                                scope.visualField = {
                                    fieldReferenceValueId: scope.field.idReferenceValue,
                                    type: scope.field.rtype,
                                    key: null,
                                    display: null,
                                    columnName: scope.field.name,
                                    objectValues: []
                                };

                                scope.templateUrl = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.input.tabledir.selector.html?v=' + FW_VERSION;
                                $templateRequest(scope.templateUrl).then(function (html) {
                                    var template = angular.element(html);
                                    element.append(template);
                                    $compile(template)(scope);
                                });
                                scope.getColumnCaption = $rootScope.getColumnCaption;
                                scope.getMessage = $rootScope.getMessage;

                                if (ngModel) {
                                    ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                        if (!scope.visualField.objectValues
                                            || !scope.visualField.objectValues
                                            || scope.visualField.objectValues.length == 0) {
                                            if (scope.sqlwhereAttr == "" || scope.sqlwhereAttr == null) {
                                                // Loading data only when sql filter is empty, otherwise data will
                                                // be loaded when sql filter changes
                                                scope.resolveData();
                                            }
                                        }
                                    };
                                }

                                scope.previousSqlwhere = "";

                                // Loading data when sql filter changes
                                attrs.$observe('sqlwhereAttr', function (value) {
                                    if (!(value == "" || value == null)) {
                                        scope.resolveData(value);
                                    }
                                });
                                // Loading data when relatedItem changes
                                scope.$watchCollection('[relatedItem, parentItem]', function (value, oldValue) {
                                    var relatedItemId = (scope.relatedItem && scope.relatedItem.id) ? scope.relatedItem.id : undefined;
                                    var newRelatedId = (value[0] && value[0].id) ? value[0].id : undefined;
                                    var parentItemId = (scope.parentItem && scope.relatedItem.id) ? scope.parentItem.id : undefined;
                                    var newparentItemId = (value[1] && value[0].id) ? value[1].id : undefined;
                                    if (((newRelatedId || newparentItemId)
                                        && (relatedItemId !== newRelatedId
                                            || parentItemId !== newparentItemId))
                                        || JSON.stringify(value)!==JSON.stringify(oldValue)) {
                                        scope.resolveData();
                                    }
                                });

                                scope.forceReload = function () {
                                    scope.resolveData(scope.previousSqlwhere, true)
                                };

                                scope.$on(coreConfigService.events.NAVIGATION_REFRESH_REFERENCE, function (event, data) {
                                    if (data.idColumn === scope.field.idColumn) {
                                        scope.forceReload();
                                    }
                                });

                                /**
                                 * Notifies the client that the component is on focus
                                 *
                                 * @param field Field information associated to the component
                                 */
                                scope.doEnter = function (field) {
                                    scope.onFocusFunction({
                                        name: field.name
                                    });
                                };

                                /**
                                 * Loading data
                                 *
                                 * @param sqlwhere SQL filter
                                 */
                                scope.resolveData = function (sqlwhere, force) {
                                    if (!force && (sqlwhere === scope.previousSqlwhere) && scope.previousSqlwhere !== undefined) {
                                        return;
                                    }
                                    scope.previousSqlwhere = sqlwhere;
                                    var idReferenceValue = scope.field.idReferenceValue;
                                    cacheService.loadReferenceData(idReferenceValue).then(function () {
                                        var refTable = cacheService.getReference(idReferenceValue);
                                        if (refTable) {
                                            if (sqlwhere === "" || sqlwhere === undefined) {
                                                sqlwhere = refTable.info.sqlwhere;
                                            }
                                            if (scope.relatedItem) {
                                                _.each(_.keys(scope.relatedItem), function (currentKey) {
                                                    var field = {
                                                        column: {
                                                            standardName: currentKey
                                                        }
                                                    };
                                                    field[currentKey] = scope.relatedItem[currentKey];
                                                    if (cacheService.isFieldInConditional(field, sqlwhere)) {
                                                        sqlwhere = cacheService.replaceJSConditional(field, scope.relatedItem[currentKey], sqlwhere);
                                                    }
                                                });
                                            }
                                            if (scope.parentItem) {
                                                _.each(_.keys(scope.parentItem), function (currentKey) {
                                                    var field = {
                                                        column: {
                                                            standardName: currentKey
                                                        }
                                                    };
                                                    field[currentKey] = scope.parentItem[currentKey];
                                                    if (cacheService.isFieldInConditional(field, sqlwhere)) {
                                                        sqlwhere = cacheService.replaceJSConditional(field, scope.parentItem[currentKey], sqlwhere);
                                                    }
                                                });
                                            }
                                            if (cacheService.isCompleteConditional(sqlwhere)) {
                                                cacheService.loadReferenceTableData(idReferenceValue, sqlwhere, scope.$parent).then(function (result) {
                                                    scope.currentIdReferenceValue = scope.field.idReferenceValue;
                                                    scope.visualField.key = result.colKey.standardName;
                                                    scope.visualField.display = result.colDisplay.standardName;
                                                    scope.visualField.table = result.table;
                                                    scope.visualField.sqlwhere = result.sqlwhere;
                                                    scope.visualField.sqlorderby = result.sqlorderby;
                                                    if (!scope.field.required) {
                                                        var empty = {};
                                                        empty[scope.visualField.key] = "";
                                                        empty[scope.visualField.display] = "";
                                                        var contentData = [empty];
                                                        scope.visualField.objectValues = contentData.concat(result.data.content);
                                                    }
                                                    else {
                                                        scope.visualField.objectValues = result.data.content;
                                                    }
                                                    scope.visualField.displayCaption = result.colDisplay.name;
                                                    scope.referenceTotalElements = result.data.totalElements;
                                                    var value = _.find(result.data.content, function (current) {
                                                        return (ngModel.$viewValue === current[scope.visualField.key]);
                                                    });
                                                    if (value) {
                                                        scope.selectedField = ngModel.$viewValue;
                                                    }
                                                    else {
                                                        scope.selectedField = "";
                                                        scope.selectedElementFunction(
                                                            {
                                                                idColumnData: scope.field.idColumn,
                                                                fieldValueData: null,
                                                                fieldDisplayData: null
                                                            });
                                                    }
                                                }).catch(function (err) {
                                                    // TODO: Error handling
                                                });

                                            }
                                        } else {
                                            // TODO: Error handling
                                        }
                                    });
                                };

                                /**
                                 * Calls the callback function to return the selected value
                                 * Returns a structure with the following information
                                 *  - idColumnData: The id of the field as passed from the caller
                                 *  - fieldValueData: The selected value
                                 *  - fieldDisplayData: The selected value String representation                         *
                                 *
                                 * @param item Selected item
                                 */
                                scope.selectValue = function (item) {
                                    scope.selectedElementFunction({
                                        idColumnData: scope.field.idColumn,
                                        fieldValueData: item,
                                        fieldDisplayData: item
                                    });
                                };
                            }

                            if (loadData){
                                scope.resolveData();
                            }
                        };

                        scope.initializeElement();

                        scope.$on('$destroy', function() {
                            scope.unWatchVisible();
                            cacheService.clearReferenceTableDataValue(scope.$id);
                        });
                    }
                }
            }
        }
    }]);
});
