define(['app', 'moment', 'lodash', 'modules/forms/directives/input/smartClockpicker'], function (app, moment, _) {

    /**
     * This directive generates the HTML for editing an Advanced Characteristic whose type is SELECT_LIST
     * The following are the parameters to be passed:
     *
     *      parent-item           ->  The parent process associated to the element
     *
     * The following is an example usage of the directive
     *
     *      <autogen-process-planning>
     *      </autogen-process-planning>
     */
    'use strict';

    return app.registerDirective('autogenProcessPlanning', ['$rootScope', '$compile', '$timeout', '$injector', '$log', '$state', '$q',
        'Authentication', 'commonService', 'cacheService', 'adProcessService', 'adPrivilegeService',
        function ($rootScope, $compile, $timeout, $injector, $log, $state, $q,
                  Authentication, commonService, cacheService, adProcessService, adPrivilegeService) {
            return {
                restrict: 'E',
                scope: {
                    parentItem: '='
                },
                replace: true,
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.process.planning.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            var processDayReference = 'ff80808153b448ff0153b539f2ba0000';
                            var processExecTypeReference = 'ff808081539a160601539a1e472b0001';
                            scope.inputsTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.inputs.html?v=' + FW_VERSION;
                            scope.formName = "Proceso";
                            scope.getMessage = $rootScope.getMessage;
                            scope.getColumnCaption = $rootScope.getColumnCaption;
                            scope.item = {
                                executingClient: null,
                                executingUser: null,
                                hour: moment(new Date()).format("HH:mm"),
                                monthDay: 1,
                                execWeekday: 'MON',
                                frequencyHour: 0,
                                frequencyMinute: 1
                            };
                            scope.cronDef = scope.parentItem.item.cronExpression;
                            scope.item.executingClient = scope.parentItem.item.executingIdClient;
                            scope.item.executingUser = scope.parentItem.item.executingIdUser;
                            scope.isWeeklyStored = false;

                            cacheService.loadClients().then(
                                function (data) {
                                    scope.clients = data;
                                }
                            ).catch(function (err) {
                                $log.error(err);
                            });
                            adPrivilegeService.users({
                                idClient: cacheService.loggedIdClient(),
                                privilege: "ROLE_BACKGROUND_PROCESS"
                            }, function (data){
                                scope.users = data;
                            },function (error) {
                                $log.error(error);
                            });

                            /**
                             * Parsing cron definition for ONE_EXEC type to be able to show the corresponding visual values
                             *
                             * @param splitted Splitted cron definition
                             */
                            scope.parseOneExec = function (splitted) {
                                scope.item.execType = "ONE_EXEC";
                                for (var i = 1; i <= 4; i++) {
                                    splitted[i] = splitted[i].length === 2 ? splitted[i] : "0" + splitted[i];
                                }
                                scope.item.execOneExecDay = moment(splitted[3] + splitted[4] + splitted[6], "DDMMYYYY");
                                var date = new Date();
                                date.setHours(splitted[2]);
                                date.setMinutes(splitted[1]);
                                scope.item.hour =  date;
                            };

                            /**
                             * Parsing cron definition for MONTHLY type to be able to show the corresponding visual values
                             *
                             * @param splitted Splitted cron definition
                             */
                            scope.parseMonthly = function (splitted) {
                                scope.item.execType = "MONTHLY";
                                for (var i = 1; i <= 4; i++) {
                                    splitted[i] = splitted[i].length === 2 ? splitted[i] : "0" + splitted[i];
                                }
                                scope.item.monthDay = Number(splitted[3]);
                                var date = new Date();
                                date.setHours(splitted[2]);
                                date.setMinutes(splitted[1]);
                                scope.item.hour =  date;
                            };

                            /**
                             * Parsing cron definition for WEEKLY type to be able to show the corresponding visual values
                             *
                             * @param splitted Splitted cron definition
                             */
                            scope.parseWeekly = function (splitted) {
                                scope.item.execType = "WEEKLY";
                                for (var i = 1; i <= 2; i++) {
                                    splitted[i] = splitted[i].length === 2 ? splitted[i] : "0" + splitted[i];
                                }

                                var date = new Date();
                                date.setHours(splitted[2]);
                                date.setMinutes(splitted[1]);
                                scope.item.hour =  date;
                                scope.item.execWeekday = splitted[5];
                            };

                            /**
                             * Parsing cron definition for DAILY type to be able to show the corresponding visual values
                             *
                             * @param splitted Splitted cron definition
                             */
                            scope.parseDaily = function (splitted) {
                                scope.item.execType = "DAILY";
                                for (var i = 1; i <= 2; i++) {
                                    splitted[i] = splitted[i].length === 2 ? splitted[i] : "0" + splitted[i];
                                }
                                var date = new Date();
                                date.setHours(splitted[2]);
                                date.setMinutes(splitted[1]);
                                scope.item.hour =  date;
                                var days = splitted[5].split(',');
                                scope.isDailyStored = true;

                                cacheService.loadReferenceData(processDayReference).then(function () {
                                    var refList = cacheService.getReference(processDayReference);
                                    scope.days = _.sortBy(refList.values, function (current) {
                                        return current.seqno;
                                    });
                                    scope.item.day = [];
                                    _.each(scope.days, function (current) {
                                        scope.item.day[current.value] = _.some(days, function (stored) {
                                            return current.value === stored;
                                        });
                                    });
                                });
                            };

                            /**
                             * Parsing cron definition for FREQUENTLY type to be able to show the corresponding visual values
                             *
                             * @param splitted Splitted cron definition
                             */
                            scope.parseFrequently = function (splitted) {
                                scope.item.execType = "FREQUENTLY";
                                if (splitted[2] === "*") {
                                    var minutes = splitted[1].split('/');
                                    scope.item.frequencyMinute = Number(minutes[1]);
                                    scope.item.frequencyHour = 0;
                                }
                                else if (splitted[1] === "*") {
                                    var hours = splitted[2].split('/');
                                    scope.item.frequencyHour = Number(hours[1]);
                                    scope.item.frequencyMinute = 0;
                                }
                            };

                            /**
                             * Reverting cron expression to show corresponding visual values
                             */
                            scope.reverseCronDef = function () {
                                if (scope.cronDef === null || scope.cronDef === undefined)
                                    return;
                                var splitted = scope.cronDef.split(' ');
                                if (new RegExp('^(.+[ ]){6}.+').test(scope.cronDef)) {
                                    scope.parseOneExec(splitted);
                                }
                                else if (new RegExp('^(\\d+[ ]){4}\\*[ ]\\?').test(scope.cronDef)) {
                                    scope.parseMonthly(splitted);
                                }
                                else if (new RegExp('^(\\d+[ ]){3}\\?[ ]\\*[ ]\\w{3}$').test(scope.cronDef)) {
                                    scope.parseWeekly(splitted);
                                }
                                else if (new RegExp('^(\\d+[ ]){3}\\?[ ]\\*[ ]\\w{3}.+').test(scope.cronDef)) {
                                    scope.parseDaily(splitted);
                                }
                                else {
                                    scope.parseFrequently(splitted);
                                }
                            };
                            scope.reverseCronDef();
                            scope.days = {};

                            /**
                             * Gets the data needed for the input of the LIST component
                             *
                             * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                             */
                            scope.getExecutionTypes = function () {
                                return {
                                    //Database ID of the Reference "List - Process background execution type"
                                    idReferenceValue: 'ff808081539a160601539a1e472b0001',
                                    name: 'execType',
                                    idColumn: 'execType',
                                    rtype: 'LIST',
                                    readOnly: false,
                                    required: true
                                };
                            };

                            /**
                             * Gets the data needed for the input of the LIST component
                             *
                             * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                             */
                            scope.getDaysTypes = function () {
                                return {
                                    //Database ID of the Reference "List - Process background execution days"
                                    idReferenceValue: processDayReference,
                                    name: 'execWeekday',
                                    idColumn: 'execWeekday',
                                    rtype: 'LIST',
                                    readOnly: false,
                                    required: true
                                };
                            };


                            if (!scope.isDailyStored)
                                cacheService.loadReferenceData(processDayReference).then(function () {
                                    var refList = cacheService.getReference(processDayReference);
                                    scope.days = _.sortBy(refList.values, function (current) {
                                        return current.seqno;
                                    });
                                    scope.item.day = [];
                                    _.each(scope.days, function (current) {
                                        scope.item.day[current.value] = true;
                                    });
                                });

                            scope.executionTypeData = scope.getExecutionTypes();
                            scope.executionWeekdayData = scope.getDaysTypes();

                            /**
                             * Updates the item with the value selected
                             *
                             * @param idColumnData LIST item name (execType)
                             * @param fieldValueData Item's column's value (item.execType value)
                             * @param fieldDisplayData Unused (required by the visual component)
                             */
                            scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                                scope.item[idColumnData] = fieldValueData;
                                scope[idColumnData] = fieldValueData;
                            };

                            scope.save = function () {
                                angular.forEach(scope.processPlanningForm.$error.required, function (field) {
                                    field.$setDirty();
                                });
                                if (scope.processPlanningForm.hour)
                                    scope.processPlanningForm.hour.$setDirty();
                                if (scope.item.execType === 'FREQUENTLY' && !((scope.item.frequencyMinute * scope.item.frequencyHour) === 0 && (scope.item.frequencyMinute + scope.item.frequencyHour) !== 0))
                                    return;
                                if (scope.processPlanningForm.$valid) {
                                    scope.cronDef = scope.parseCronDef();
                                    console.log(scope.item);
                                    console.log("Cron definition: " + scope.cronDef);
                                    adProcessService.schedule({
                                        idClient: scope.item.executingClient,
                                        idUser: scope.item.executingUser,
                                        idProcess: scope.parentItem.item.idProcess,
                                        cron: scope.cronDef
                                    }, function (data) {
                                        if (data.success === false) {
                                            commonService.showRejectNotification($rootScope.getMessage(data.message));
                                        } else {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            $log.log("Process scheduled correctly: " + scope.parentItem.item.idProcess);
                                        }
                                    }, function (data) {
                                        commonService.showRejectNotification($rootScope.getMessage('AD_ErrValidationUnknow'));
                                        $log.error(data);
                                    })
                                }
                            };

                            scope.clear = function () {
                                $.SmartMessageBox({
                                    title: $rootScope.getMessage('AD_msgConfirmClearSchedule'),
                                    buttons: '[' + $rootScope.getMessage('AD_btnClearSchedule') + '][' + $rootScope.getMessage('AD_btnCancel') + ']'
                                }, function (ButtonPressed) {
                                    setTimeout(function () {
                                        if (ButtonPressed === $rootScope.getMessage('AD_btnClearSchedule')) {
                                            adProcessService.clearSchedule({
                                                idClient: scope.parentItem.item.idClient,
                                                idProcess: scope.parentItem.item.idProcess
                                            }, function (data) {
                                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                $log.log("Process unscheduled correctly: " + scope.parentItem.item.idProcess);
                                                scope.cronDef = null;
                                            }, function (data) {
                                                $log.error(data);
                                            });
                                        }
                                    }, 10);
                                });
                                // The following lines are needed to prevent the dialog from
                                // being shown again by pressing Enter
                                var $focused = $(':focus');
                                $focused.blur();
                            };

                            scope.watchFunction = scope.$watchCollection('item', function () {
                                scope.updateCronDef();
                            });

                            scope.updateCronDef = function () {
                                if (scope.item.execType)
                                    scope.cronDef = scope.parseCronDef();
                            };

                            /**
                             * Assembles a cron expression from the input
                             *
                             * @returns {string} Cron expression
                             */
                            scope.parseCronDef = function () {
                                var hourDefinition = scope.getHourDef();
                                var cronDef = '0';
                                cronDef += (" " + hourDefinition.minute);
                                cronDef += (" " + hourDefinition.hour);
                                switch (scope.item.execType) {
                                    case 'DAILY':
                                        cronDef += " ? *";
                                        var days = _.keys(scope.item.day);
                                        days = _.filter(days, function (current) {
                                            return scope.item.day[current];
                                        });
                                        var cronDay = '';
                                        _.each(days, function (current, index) {
                                            var comma = (index + 1) != days.length ? ',' : '';
                                            cronDay += (current + comma);
                                        });
                                        cronDef += (cronDay != '' ? (" " + cronDay) : " *");
                                        break;
                                    case 'WEEKLY':
                                        cronDef += " ? *";
                                        if (scope.item.execWeekday)
                                            cronDef += (" " + scope.item.execWeekday);
                                        else
                                            cronDef += (" *");
                                        break;

                                        break;
                                    case 'MONTHLY':
                                        cronDef += (" " + scope.item.monthDay);
                                        cronDef += " * ?";
                                        break;
                                    case 'ONE_EXEC':
                                        var day = moment(scope.item.execOneExecDay, "DD/MM/YYYY").date();
                                        var month = moment(scope.item.execOneExecDay, "DD/MM/YYYY").month() + 1;
                                        var year = moment(scope.item.execOneExecDay, "DD/MM/YYYY").year();
                                        cronDef += " " + day + " " + month + " ?" + " " + year;
                                        break;
                                    case 'FREQUENTLY':
                                        cronDef = '0';
                                        if (scope.item.frequencyMinute > 0)
                                            cronDef += (" " + "0/" + scope.item.frequencyMinute + " * * * ?");
                                        else
                                            cronDef += (" * " + "0/" + scope.item.frequencyHour + " * * ?");
                                        break;
                                }
                                return cronDef;
                            };

                            /**
                             * Returns the splitted hour and minute
                             *
                             * @returns {*}
                             */
                            scope.getHourDef = function () {
                                if (moment(scope.item.hour).isValid()){
                                    // We are dealing with a valid date object, so we are just going to retrieve the hour from it
                                  return {
                                      hour: moment(scope.item.hour).hour(),
                                      minute: moment(scope.item.hour).minute()
                                  }
                                } else {
                                    var regex = new RegExp('^(\\d{2})\\:\\d{2}$');
                                    if (regex.test(scope.item.hour)) {
                                        var splitted = scope.item.hour.split(':');
                                        return {
                                            hour: Number(splitted[0]).toString(),
                                            minute: Number(splitted[1]).toString()
                                        }
                                    }
                                    return {
                                        hour: 0,
                                        minute: 0
                                    }
                                }
                            };

                            // Cleanup
                            // here is where the cleanup happens
                            scope.$on('$destroy', function () {
                                scope.watchFunction();
                            });
                        }


                    }
                }
            }
        }]);
});