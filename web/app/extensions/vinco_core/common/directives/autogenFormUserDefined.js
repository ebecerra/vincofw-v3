/**
 *
 * The following is an example usage of the directive
 *
 *  <autogen-form-user-defined></autogen-form-user-defined>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('autogenFormUserDefined', function ($rootScope, $compile, $injector) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.user.defined.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes) {
                        scope.unWatchVisible = scope.$watch(
                            function () {
                                return $(element).is(":visible");
                            },
                            function(newValue, oldValue) {
                                if (newValue) {
                                    scope.initializeElement(true);
                                }
                            },
                            true);
                        scope.initialized = false;
                        scope.initializeElement = function (loadData) {
                            if ($(element).is(":visible") && !scope.initialized) {
                                if (scope.unWatchVisible)
                                    scope.unWatchVisible();
                                scope.initialized = true;
                                var splitted = attributes.command.split("+");
                                var module = splitted[0].trim();
                                var view = splitted[1].trim();
                                view = view.replace(/^"(.*)"$/, '$1');
                                view = view.replace(/^'(.*)'$/, '$1');
                                scope.include = sprintf("%s%s", MODULES[module], view);
                            }
                        };


                    }
                }
            }
        }
    });
});