define(['app', 'moment', 'modules/forms/directives/editors/smartCkEditor'], function (app) {

    /**
     * This directive generates a form corresponding to the data defined in the given tab
     * It receives the following parameters in the form of attributes
     *
     *      tab-generator-id        ->  The idTab key
     *      row-id                  ->  The primary key of row to edit
     *      sub-item                ->  Accepts true or false. Used to define if the edited item is a main item or a subitem
     *                                  of a main item. Default value is false
     *      link-data               ->  The reference keys if this form belongs to an entity that contains foreign keys
     *                                  (e.g. the "Fields" tab when editing a Tab, the number of siblings will be 1)
     *                                  Default value is 0
     *      parent-id-tab           ->  The ID of the parent tab
     *
     * The following is an example usage of the directive
     *
     *  <autogen-form-characteristics-advanced tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018" row-id=""></autogen-form-characteristics-advanced>
     */
    'use strict';

    return app.registerDirective('autogenFormCharacteristicsAdvanced', function ($rootScope, $compile, $timeout, $injector, $log, $state, $q,
                                                                                 Authentication, commonService, cacheService, adCharacteristicService,
                                                                                 adCharacteristicValueService, adClientService, adTranslationService) {
        return {
            restrict: 'E',
            require: '?ngModel',
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.characteristics.advanced.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.inputsTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.inputs.html?v=' + FW_VERSION;
                        scope.translationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.translation.dialog.html?v=' + FW_VERSION;
                        scope.tabGeneratorId = attributes.tabGeneratorId;
                        scope.translationDialogId = 'translationModal' + scope.tabGeneratorId;
                        scope.tableSearchDialogId = 'tableModal' + scope.tabGeneratorId;
                        scope.rowId = attributes.rowId;
                        scope.parentIdTab = null;
                        scope.parentTab = null;
                        scope.fieldRows = [];
                        if (attributes.parentIdTab) {
                            scope.parentIdTab = attributes.parentIdTab;
                            cacheService.loadTab(scope.parentIdTab).then(function (data) {
                                scope.parentTab = data;
                            });
                        }
                        scope.linkData = null;
                        if (attributes.linkData) {
                            scope.linkData = JSON.parse(attributes.linkData);
                        }
                        scope.otherTabs = 0;
                        if (attributes.otherTabs) {
                            scope.otherTabs = JSON.parse(attributes.otherTabs);
                        }
                        scope.fields = [];
                        scope.primaryFields = [];
                        scope.groups = [];
                        scope.posField = [];
                        scope.fieldValue = {};
                        scope.service = null;
                        scope.item = {};
                        scope.windowGeneratorId = attributes.windowGeneratorId;
                        scope.complicateVisualFields = {};
                        scope.subItem = false;
                        if (attributes.subItem) {
                            scope.subItem = JSON.parse(attributes.subItem);
                        }
                        scope.translation = {};
                        scope.translationProperty = {
                            translationClientId: null,
                            translationInitClientId: null
                        };

                        /**
                         * Starts loading the form
                         */
                        scope.loadCharacteristicForm = function () {
                            if (scope.tabGeneratorId !== undefined && scope.tabGeneratorId != "") {
                                cacheService.loadTab(scope.tabGeneratorId).then(function (data) {
                                    scope.tabInformation = data;
                                    scope.adTab = scope.tabInformation;
                                    scope.parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowGeneratorId,
                                        tablevel: scope.tabInformation.tablevel - 1
                                    });
                                    if (scope.parentItem) {
                                        adCharacteristicService.fields({
                                            idClient: scope.parentItem.item.idClient,
                                            idTable: scope.parentItem.tab.table.idTable,
                                            dtype: scope.parentItem.item.characteristicType,
                                            mode: 'ADVANCED'
                                        }, function (data) {
                                            scope.fields = _.sortBy(data, function (item) {
                                                return item.seqno;
                                            });
                                            scope.loadFieldReferences(0);
                                        }, function (err) {
                                            // TODO
                                        });
                                    }

                                }).catch(function (err) {
                                    // TODO: Error handling
                                    $log.error(err);
                                });
                            } else {
                                // TODO: Error handling
                            }
                        };

                        /**
                         * Loads reference information for a given characteristic
                         *
                         * @param index Characteristic index
                         */
                        scope.loadFieldReferences = function (index) {
                            if (index < scope.fields.length) {
                                cacheService.loadReferenceData(scope.fields[index].idReference).then(
                                    function () {
                                        _.each(scope.fields, function (currentField) {
                                            if (currentField.characteristic.idCharacteristic == scope.fields[index].characteristic.idCharacteristic) {
                                                currentField.display = true;
                                                currentField.span = 1;
                                                currentField.column = {
                                                    name: "field_" + index,
                                                    mandatory: currentField.mandatory,
                                                    reference: cacheService.getReference(currentField.idReference)
                                                };
                                                currentField.reference = cacheService.getReference(currentField.idReference);
                                            }
                                        });
                                        scope.loadFieldReferences(index + 1);
                                    }
                                ).catch(function (err) {
                                    _.each(scope.fields, function (currentField) {
                                        if (currentField.characteristic.idCharacteristic == scope.fields[index].characteristic.idCharacteristic) {
                                            currentField.display = true;
                                            currentField.span = 1;
                                            currentField.column = {
                                                name: "field_" + index,
                                                mandatory: currentField.mandatory,
                                                reference: cacheService.getReference(currentField.idReference)
                                            };
                                            currentField.reference = cacheService.getReference(currentField.idReference);
                                        }
                                    });
                                    scope.loadFieldReferences(index + 1);
                                    $log.error(err);
                                });
                            } else if (index == scope.fields.length) {
                                for (var i = 0; i < scope.fields.length; i++) {
                                    if (!scope.fieldRows[Number.parseInt(i / (scope.adTab.columns))]) {
                                        scope.fieldRows[Number.parseInt(i / (scope.adTab.columns))] = {fields: []};
                                    }
                                    scope.fieldRows[Number.parseInt(i / (scope.adTab.columns))].fields.push(scope.fields[i]);
                                }
                                // Load characteristic values from backend
                                _.each(scope.fields, function (field) {
                                    adCharacteristicValueService.query({
                                        idClient: cacheService.loggedIdClient(),
                                        q: 'idCharacteristic=' + field.characteristic.idCharacteristic + ",keyValue=" + scope.parentItem.item.id
                                    }, function (data) {
                                        if (data.content.length > 0) {
                                            var selected = _.find(scope.fields, function (currentField) {
                                                return currentField.characteristic.idCharacteristic == data.content[0].idCharacteristic;
                                            });
                                            if (selected) {
                                                selected.idCharacteristicValue = data.content[0].idCharacteristicValue;
                                                selected.characteristicValue = data.content[0];
                                                scope.fieldValue[selected.column.name] = data.content[0].value;
                                                scope.processFieldValue(field);
                                            }
                                        }
                                    })
                                });
                            }
                        };

                        /**
                         * Process reference information for a given characteristic
                         *
                         * @param field Characteristic field to analyze
                         */
                        scope.processFieldValue = function (field) {
                            if (field.column.reference) {
                                if (field.column.reference.rtype == 'INTEGER') {
                                    scope.fieldValue[field.column.name] = Number(scope.fieldValue[field.column.name]);
                                } else if (field.column.reference.rtype == 'YESNO') {
                                    scope.fieldValue[field.column.name] = ((scope.fieldValue[field.column.name] == true) ||
                                    (scope.fieldValue[field.column.name] == 'S') ||
                                    (scope.fieldValue[field.column.name] == 'YES') ||
                                    (scope.fieldValue[field.column.name] == 'SI') ||
                                    (scope.fieldValue[field.column.name] == 'true'));
                                }
                            }
                        };

                        /**
                         * Saves the characteristics
                         */
                        scope.saveCharacteristic = function () {
                            var valueNames = _.keys(scope.fieldValue);
                            var saved = 0;
                            _.each(valueNames, function (keyName) {
                                var fValue = scope.fieldValue[keyName];
                                var field = _.find(scope.fields, function (field) {
                                    return field.column.name == keyName;
                                });
                                var valueData = fValue;
                                if (scope.complicateVisualFields[field.idReferenceValue] &&
                                    (scope.complicateVisualFields[field.idReferenceValue].type == 'SEARCH')) {
                                    valueData = scope.fieldValue["KEY_" + scope.complicateVisualFields[field.idReferenceValue].columnName];
                                }
                                if (field.idCharacteristicValue) {
                                    var updateFunction = adCharacteristicValueService.update;
                                    if (field.column.reference.rtype == 'FILE') {
                                        updateFunction = adCharacteristicValueService.updateFile;
                                    }
                                    field.characteristicValue.value = valueData;
                                    var cloned = JSON.parse(JSON.stringify(field.characteristicValue));
                                    if (cloned.characteristic)
                                        delete cloned.characteristic;
                                    if (cloned.client)
                                        delete cloned.client;
                                    if (cloned.module)
                                        delete cloned.module;
                                    updateFunction(cloned,
                                        function (data) {
                                            scope.checkFinishedSaving(++saved, true);
                                        }, function (err) {
                                            scope.checkFinishedSaving(++saved, false);
                                        }
                                    );
                                } else {
                                    field.idCharacteristicValue = adCharacteristicValueService.create({
                                        idClient: scope.parentItem.item.idClient,
                                        idModule: scope.parentItem.item.idModule,
                                        idCharacteristic: field.characteristic.idCharacteristic,
                                        active: true,
                                        keyValue: scope.parentItem.item.id,
                                        value: valueData
                                    }, function (data) {
                                        scope.checkFinishedSaving(++saved, true);
                                    }, function (err) {
                                        scope.checkFinishedSaving(++saved, false);
                                    });
                                }
                            });

                        };

                        scope.checkFinishedSaving = function (index, ok) {
                            if ((index + 1) >= scope.fields.length && ok) {
                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                            }
                        };

                        scope.selectTableValue = function (item) {
                            var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                            var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                            scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                            scope.fieldValue[scope.currentColumnName] = item[col];
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getFieldAttr = function (field) {
                            return {
                                idReferenceValue: field.idReferenceValue,
                                name: field.column.name,
                                idColumn: field.characteristic.id,
                                rtype: field.column.reference.rtype,
                                display: true,
                                required: field.column.mandatory
                            };
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getListFieldAttr = function (field) {
                            return {
                                idReferenceValue: field.column.idReferenceValue,
                                name: field.column.name,
                                idColumn: field.column.idColumn,
                                rtype: field.column.reference.rtype,
                                readOnly: field.readOnly,
                                required: field.column.mandatory
                            };
                        };

                        /**
                         * Function to be called when an element has been selected in an component
                         * of type TABLE and TABLEDIR
                         *
                         * @param idCharacteristic ID Characteristic
                         * @param fieldValueData Selected value
                         * @param fieldDisplayData Selected value's display text
                         */
                        scope.selectElement = function (idCharacteristic, fieldValueData, fieldDisplayData) {
                            var field = _.find(scope.fields, function (current) {
                                return current.characteristic.id == idCharacteristic
                            });
                            scope.fieldValue['KEY_' + field.column.name] = fieldValueData;
                            scope.fieldValue[field.column.name] = fieldDisplayData;
                        };
                        scope.loadCharacteristicForm();


                        /**
                         * Shows translation dialog
                         *
                         * @param field Field to be translated
                         */
                        scope.showTranslation = function (field) {
                            scope.translationHasSystem = _.some(Authentication.getLoginData().roles, function (current) {
                                return (current == "ROL_SYSTEM");
                            });
                            scope.translationClients = [];
                            if (scope.translationHasSystem) {
                                cacheService.loadClients().then(
                                    function (data) {
                                        scope.translationClients = data;
                                    }
                                ).catch(function (err) {
                                    $log.error(err);
                                });
                            } else {
                                scope.translationProperty.translationClientId = cacheService.loggedIdClient();
                                scope.translationProperty.translationInitClientId = cacheService.loggedIdClient();
                            }
                            scope.translationInfos = {};
                            scope.translatableField = field;
                            adTranslationService.query({
                                idClient: cacheService.loggedIdClient(),
                                q: "rowkey=" + scope.item.id +
                                ",idTable=" + scope.adTab.table.idTable +
                                ",idColumn=" + field.column.idColumn
                            }, function (data) {
                                scope.translation[field.idField] = {};
                                _.each($rootScope.languages, function (language) {
                                    _.each(data.content, function (translation) {
                                        if (translation.idLanguage == language.idLanguage) {
                                            scope.translationInfos[language.countrycode] = translation;
                                            scope.translation[field.idField][language.countrycode] = translation.translation;
                                            $timeout(function () {
                                                scope.translationProperty.translationClientId = translation.idClient;
                                                scope.translationProperty.translationInitClientId = translation.idClient;
                                            }, 0, true);
                                        }
                                    });
                                });
                                $('#' + scope.translationDialogId).modal();
                            }, function (err) {
                                $log.error(err);
                            });
                        };
                    }
                }
            }
        }
    });
});