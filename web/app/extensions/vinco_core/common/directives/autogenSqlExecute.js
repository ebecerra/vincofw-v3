define(['app', 'moment'], function (app, moment) {

    /**
     * This directive generates the HTML for executing native SQL or HQL queries
     * The following are the parameters to be passed:
     *
     *      parent-item           ->  The parent process associated to the element
     *
     * The following is an example usage of the directive
     *
     *      <autogen-sql-execute parent-item="parentItemModel">
     *      </autogen-sql-execute>
     */
    'use strict';

    return app.registerDirective('autogenSqlExecute', function ($rootScope, $compile, $timeout, $injector, $log, $state, $q,
                                                                Authentication, adSqlExecService, cacheService, commonService) {
        return {
            restrict: 'E',
            scope: {
                parentItem: '='
            },
            replace: true,
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.sql.execute.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.getMessage = $rootScope.getMessage;
                        scope.sqlQuery = null;
                        scope.data = {
                            resultList: null,
                            columns: []
                        };
                        scope.selectedSqlLanguage = 'SQL';
                        scope.sqlLanguages = [{
                            value: 'SQL', name: 'SQL - Standard Query Language'
                        }, {
                            value: 'HSQL', name: 'HSQL - Hibernate Query Language'
                        }];

                        scope.changeLanguage = function (language) {
                            scope.selectedSqlLanguage = language;
                        };
                        scope.changeQuery = function (sqlQuery) {
                            scope.sqlQuery = sqlQuery;
                        };

                        /**
                         * Executes the query
                         */
                        scope.exec = function () {
                            scope.data.resultList = null;
                            scope.data.columns = [];
                            scope.sqlForm.$dirty = true;
                            if (scope.sqlForm.$valid) {
                                adSqlExecService.exec(
                                    {
                                        idClient: cacheService.loggedIdClient(),
                                        sqlType: scope.selectedSqlLanguage,
                                        sqlQuery: scope.sqlQuery
                                    }, function (data) {
                                        scope.data.resultList = [];
                                        scope.searchLanguage = scope.selectedSqlLanguage;
                                        if (scope.selectedSqlLanguage === 'SQL'){
                                            if (data.properties) {
                                                scope.data.resultList = data.properties.resultData;
                                                if (data.properties.resultData.length > 0)
                                                    scope.data.columns = _.keys(data.properties.resultData[0]);
                                            }
                                        } else {
                                            if (data.properties) {
                                                scope.data.resultList = data.properties.resultData;
                                            }
                                        }
                                        commonService.showSuccesNotification($rootScope.getMessage('AD_msgSqlQueryExecuted'));
                                    }, function (error) {
                                        if (error.data && error.data.objectErrors) {
                                            _.each(error.data.objectErrors, function (current) {
                                                commonService.showRejectNotification($rootScope.getMessage(current.message));
                                            });
                                        } else {
                                            commonService.showRejectNotification($rootScope.getMessage('AD_ErrValidationUnknow'));
                                        }
                                        $log.error(error);
                                    }
                                );
                            }
                        }
                    }
                }
            }
        }
    });
});