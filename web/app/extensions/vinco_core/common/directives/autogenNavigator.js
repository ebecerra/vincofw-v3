/**
 * Raises a Next Event to navigate to the next element in the collection
 */
define(['app'], function (app) {

    'use strict';


    return app.registerDirective('autogenNavigator', ["coreConfigService", "$rootScope", function (coreConfigService, $rootScope) {
        return {
            restrict: 'A',
            scope: {
                tabId: "=",
                eventName: "=",
                isCheckbox: "=?",
                checkboxStatus: "=?",
                extraInfo: "=?"
            },
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attr) {

                        var handler = function(){
                            var eventData = {
                                idTab: scope.tabId,
                                extraInfo: scope.extraInfo
                            };
                            if (scope.isCheckbox){
                                scope.checkboxStatus = !scope.checkboxStatus;
                                eventData.checkStatus = scope.checkboxStatus;
                            }
                            $rootScope.$broadcast(coreConfigService.events[scope.eventName], eventData);
                        };
                        $(element).off( "click");
                        $(element).on( "click", handler);
                    }
                }
            }

        }
    }]);
});
