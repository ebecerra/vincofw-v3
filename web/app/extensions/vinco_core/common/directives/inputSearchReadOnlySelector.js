/**
 * This directive should be used to obtain reference values from SEARCH components in READONLY mode. The following attributes
 * should be used with the directive. The directive applies ONLY as an attribute:
 *  *   field-attr          The field being processed. Should be a structure with the following information
 *                          {{
 *                              idReferenceValue: Reference value ID,
 *                              name: Field column name
 *                              idColumn: Unique identifier for the field
 *                              rtype: Reference type
 *                              sqlwhere: SQL where clause to add to the filter
 *                              sqlorderby: SQL order clause to use, in case it is needed
 *                           }}
 *
 *  Example usage
 *      <label ng-show="field.readonly" ng-model="fieldValue[field.column.name]"
 *                  field-attr="{{getFieldAttr(field)}}"
 *                  input-search-read-only-selector></label>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('inputSearchReadOnlySelector',
        function ($rootScope, $q, $log, cacheService, referenceResolverService) {
            return {
                restrict: 'A',
                scope: {
                    fieldAttrData: '@fieldAttr',
                    tabIdAttrData: '@tabIdAttr'
                },
                require: 'ngModel',
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs, ngModel) {

                            scope.referenceData = {};
                            element.removeAttr('input-search-read-only-selector');
                            scope.field = JSON.parse(scope.fieldAttrData);
                            scope.complicateVisualFields = {};
                            scope.visualField = {
                                fieldReferenceValueId: scope.field.idReferenceValue,
                                type: scope.field.rtype,
                                key: null,
                                display: null,
                                columnName: scope.field.name,
                                objectValues: []
                            };
                            scope.yesNoValues = [
                                {value: null, name: ''},
                                {value: true, name: $rootScope.getMessage('AD_labelYes')},
                                {value: false, name: $rootScope.getMessage('AD_labelNo')}
                            ];
                            scope.referenceData = {
                                referencePageSize: 10,
                                referenceCurrentPage: 1,
                                currentFilterValue: {}
                            };

                            /**
                             * Sets display data and notifies selected item has been changed
                             */
                            scope.setVisualValue = function () {
                                var value = _.find(scope.visualField.objectValues, function (current) {
                                    return (ngModel.$viewValue == current[scope.visualField.key]);
                                });
                                if (value) {
                                    // Setting display information
                                    $(element).html(value[scope.visualField.display]);
                                }
                            };

                            /**
                             * Loads the reference's key and display information
                             *
                             * @param itemId If passed, requests the item that matches the required itemId according to
                             *              the reference information
                             */
                            scope.resolveData = function (itemId) {
                                var defered = $q.defer();
                                var promise = defered.promise;
                                referenceResolverService.resolveTableData(itemId, scope.field.idReferenceValue)
                                    .then(function (data) {
                                        scope.visualField = data.visualField;
                                        scope.referenceTotalElements = data.referenceTotalElements;
                                        if (itemId) {
                                            scope.setVisualValue();
                                        }
                                        defered.resolve();
                                    })
                                    .catch(function (err) {
                                        defered.reject(err);
                                        $log.error(err);
                                    });
                                return promise;
                            };

                            scope.ngModel = ngModel;
                            if (ngModel) {
                                ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                    if ((!scope.visualField.objectValues
                                        || !scope.visualField.objectValues
                                        || scope.visualField.objectValues.length == 0) && ngModel.$viewValue) {
                                        scope.resolveData(ngModel.$viewValue);
                                    } else {
                                        scope.setVisualValue();
                                    }
                                };

                            }

                            var idReferenceValue = scope.field.idReferenceValue;
                            var refTable = cacheService.getReference(idReferenceValue);
                            if (refTable) {
                                scope.searchColumns = _.sortBy(refTable.info.searchColumns, function (current) {
                                    return current.seqno;
                                });
                                var table = cacheService.getTable(refTable.info.idTable);
                                cacheService.loadTempTabledir(table, true, scope.searchColumns).then(function () {

                                    /**
                                     * Gets the data needed for the input of the TABLE, SEARCH, LIST AND TABLEDIR search component
                                     *
                                     * @param currentColumn Field to obtain data from
                                     * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                                     */
                                    scope.getListFieldAttr = function (currentColumn) {
                                        return {
                                            idReferenceValue: currentColumn.columnInfo.idReferenceValue,
                                            name: currentColumn.columnInfo.name,
                                            idColumn: currentColumn.columnInfo.idColumn,
                                            rtype: currentColumn.columnInfo.reference.rtype,
                                            readOnly: false,
                                            required: false
                                        };
                                    };
                                });
                            }
                        }
                    }
                }
            }
        }
    );
});
