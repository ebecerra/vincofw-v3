/**
 * This directive should be used to obtain reference values from TABLE components. The following attributes
 * should be used with the directive. The directive applies ONLY as an attribute:
 *  *   field-attr          The field being processed. Should be a structure with the following information
 *                          {{
 *                              idReferenceValue: Reference value ID,
 *                              name: Field column name
 *                              idColumn: Unique identifier for the field
 *                              rtype: Reference type
 *                              sqlwhere: SQL where clause to add to the filter
 *                              sqlorderby: SQL order clause to use, in case it is needed
 *                           }}
 *  *   selected-fn-attr    A callback function that the caller scope should implement to receive the selected value
 *                           The callback will receive three parameters.
 *                              idColumnData: Unique identifier for the field (as received in **field-attr**
 *                              fieldValueData: Selected value
 *                              fieldDisplayData: Selected value's display representation
 *                        In order to guarantee the callback function is called, the rest of the following attributes
 *                          (including selected-fn-attr), should be present exactly as presented below:
 *
 *                              selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                              id-column-data-attr="idColumnData"
 *                              field-value-data-attr="fieldValueData"
 *                              field-display-data-attr="fieldDisplayData"
 *
 * The caller scope should implement the callback function like this:
 *
 *          scope.selectElement = function(idColumnData, fieldValueData, fieldDisplayData){
 *                                   // custom logic goes here
 *                                  };
 *
 *  Example usage
 *
 *      <div class="input input-file">
 *          <span class="button">
 *              <input type="text"
 *                      table-selector
 *                      field-attr="{{getFieldAttr(field)}}"
 *                      item-attr="{{fieldValue}}"
 *                      tab-id-attr="{{scope.tab}}"
 *                      selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                      id-column-data-attr="idColumnData"
 *                      field-value-data-attr="fieldValueData"
 *                      field-display-data-attr="fieldDisplayData">
 *              <i class="fa fa-search"></i>
 *          </span>
 *          <input type="text" placeholder="" readonly="" value="">
 *      </div>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('inputTableSelector', function ($templateRequest, $compile, $rootScope, $q, $log, commonService, cacheService, autogenService, referenceResolverService) {
        return {
            restrict: 'A',
            scope: {
                fieldAttrData: '@fieldAttr',
                tabIdAttrData: '@tabIdAttr',
                selectedElementFunction: '&selectedFnAttr'
            },
            require: 'ngModel',
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {

                        scope.referenceData = {};
                        scope.maxSize = 3;
                        scope.tablePageSizeList = [10, 20, 40, 60, 100];
                        element.removeAttr('table-selector');
                        scope.field = JSON.parse(scope.fieldAttrData);
                        scope.complicateVisualFields = {};
                        scope.visualField = {
                            fieldReferenceValueId: scope.field.idReferenceValue,
                            type: scope.field.rtype,
                            key: null,
                            display: null,
                            columnName: scope.field.name,
                            objectValues: []
                        };
                        scope.referenceData = {
                            referencePageSize: 10,
                            referenceCurrentPage: 1,
                            currentFilterValue: ""
                        };

                        scope.getColumnCaption = $rootScope.getColumnCaption;
                        scope.getMessage = $rootScope.getMessage;

                        // Showing selector dialog when clicking on textbox
                        var clickEvent = function (event) {
                            scope.$apply(function () {
                                scope.currentColumnName = scope.field.name;
                                scope.referenceData.referencePageSize = 10;
                                scope.referenceData.referenceCurrentPage = 1;
                                scope.referenceData.currentFilterValue = "";
                                scope.tableFilterReverseSort = false;
                                if (ngModel.$viewValue === null || ngModel.$viewValue === undefined) {
                                    scope.resolveData()
                                        .then(function () {
                                            scope.doFilterVisualFields('', scope.referenceData.referencePageSize, scope.referenceData.referenceCurrentPage);
                                        });
                                } else {
                                    scope.doFilterVisualFields('', scope.referenceData.referencePageSize, scope.referenceData.referenceCurrentPage);
                                }
                                $('#' + scope.tableSearchDialogId).on("shown.bs.modal", function () {
                                    $('#searchTable' + scope.tableSearchDialogId)[0].focus();
                                });
                                $('#' + scope.tableSearchDialogId).modal();
                            });
                        };

                        element.on('click',
                            clickEvent
                        );

                        element.parent().siblings().on('click',
                            clickEvent
                        );
                        scope.ngModel = ngModel;
                        if (ngModel) {
                            ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                if ((!scope.visualField.objectValues
                                    || !scope.visualField.objectValues
                                    || scope.visualField.objectValues.length == 0) && ngModel.$viewValue) {
                                    scope.resolveData(ngModel.$viewValue);
                                } else {
                                    scope.setVisualValue();
                                }
                            };
                        }

                        /**
                         * Sets display data and notifies selected item has been changed
                         */
                        scope.setVisualValue = function () {
                            var value = _.find(scope.visualField.objectValues, function (current) {
                                return (ngModel.$viewValue == current[scope.visualField.key]);
                            });
                            if (value) {
                                // Setting display information
                                element.parent().next().val(value[scope.visualField.display]);
                                // Notifying selected item was changed
                                scope.selectedElementFunction({
                                    idColumnData: scope.field.idColumn,
                                    fieldValueData: value[scope.visualField.key],
                                    fieldDisplayData: value[scope.visualField.display]
                                });
                            }
                        };

                        scope.tableSearchDialogId = "search_dialog" + scope.field.idColumn;
                        scope.searchWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.input.table.selector.window.html?v=' + FW_VERSION;

                        $templateRequest(scope.searchWindowDialogTemplate).then(function (html) {
                            var template = angular.element(html);
                            element.closest("#content").append(template);
                            scope.currentColumnName = scope.field.name;
                            scope.referenceData.referencePageSize = 10;
                            scope.referenceData.referenceCurrentPage = 1;
                            scope.referenceData.currentFilterValue = "";
                            scope.tableFilterReverseSort = false;
                            $compile(template)(scope);
                        });

                        /**
                         * Loads the reference's key and display information
                         *
                         * @param itemId If passed, requests the item that matches the required itemId according to
                         *              the reference information
                         */
                        scope.resolveData = function (itemId) {
                            var defered = $q.defer();
                            var promise = defered.promise;
                            referenceResolverService.resolveTableData(itemId, scope.field.idReferenceValue, scope)
                                .then(function (data) {
                                    scope.visualField = data.visualField;
                                    scope.referenceTotalElements = data.referenceTotalElements;
                                    if (itemId) {
                                        scope.setVisualValue();
                                    }
                                    defered.resolve();
                                })
                                .catch(function (err) {
                                    defered.reject(err);
                                    $log.error(err);
                                });
                            return promise;
                        };

                        scope.$watch("[referenceData.referenceCurrentPage]", function (old, newData) {
                            if (scope.visualField) {
                                scope.doFilterVisualFields(scope.referenceData.currentFilterValue, scope.referenceData.referencePageSize, scope.referenceData.referenceCurrentPage);
                            }
                        });

                        scope.$watch("[referenceData.referencePageSize,referenceData.currentFilterValue]", function (old, newData) {
                            if (scope.visualField) {
                                var page = 1;
                                scope.doFilterVisualFields(scope.referenceData.currentFilterValue, scope.referenceData.referencePageSize, page);
                            }
                        });

                        /**
                         * Calls the callback function to return the selected value
                         * Returns a structure with the following information
                         *  - idColumnData: The id of the field as passed from the caller
                         *  - fieldValueData: The selected value
                         *  - fieldDisplayData: The selected value String representation                         *
                         *
                         * @param item Selected item
                         */
                        scope.selectTableValue = function (item) {
                            var key = scope.visualField.key;
                            var col = scope.visualField.display;
                            element.parent().next().val(item[col]);
                            scope.selectedElementFunction(
                                {
                                    idColumnData: scope.field.idColumn,
                                    fieldValueData: item[key],
                                    fieldDisplayData: item[col]
                                });
                            $('#' + scope.tableSearchDialogId).modal('hide');
                        };

                        /**
                         * Requests a page to display in the dialog
                         *
                         * @param filter Filter information
                         * @param pageSize Page size
                         * @param currentPage Page number. If not present, 1 will be assumed
                         */
                        scope.doFilterVisualFields = function (filter, pageSize, currentPage) {
                            if (scope.visualField.display) {
                                if (currentPage) {
                                    scope.referenceData.referenceCurrentPage = currentPage;
                                } else {
                                    scope.referenceData.referenceCurrentPage = 1;
                                }
                                scope.referenceData.referencePageSize = pageSize;
                                scope.tableOrderByField = commonService.removeUnderscores(scope.visualField.display);
                                scope.searchFilter = scope.visualField.displaySource == 'DATABASE' ? scope.visualField.display + "=%" + filter + "%" : "";
                                scope.loadFilterData(scope.searchFilter);
                            }
                        };

                        /**
                         * Loads dialog page with filter information
                         *
                         * @param customFilter Filter information
                         */
                        scope.loadFilterData = function (customFilter) {
                            var q = customFilter;
                            var idReferenceValue = scope.field.idReferenceValue;
                            if (!customFilter) {
                                q = scope.field.sqlwhere;
                            }
                            var sort = scope.visualField.displaySource == 'DATABASE' ? scope.visualField.display : null;
                            if (scope.field.sqlorderby) {
                                sort = scope.field.sqlorderby;
                            }
                            var _service = null;
                            var refTable = cacheService.getReference(idReferenceValue);
                            if (refTable) {
                                if (cacheService.isCompleteConditional(refTable.info.sqlwhere)) {
                                    var table = cacheService.getTable(refTable.info.idTable);
                                    try {
                                        _service = autogenService.getAutogenService(table);
                                    } catch (e) {
                                        $log.error(e);
                                    }
                                    $log.info("Requesting data from server for table: " + table.name + " with query: " + q);
                                    _service.query(
                                        {
                                            idClient: cacheService.loggedIdClient(),
                                            q: q,
                                            sort: sort ? '[{ "property":"' + sort + '","direction":"' + (scope.tableFilterReverseSort ? 'DESC' : 'ASC') + '" }]' : null,
                                            page: scope.referenceData.referenceCurrentPage,
                                            limit: scope.referenceData.referencePageSize
                                        },
                                        function (data2) {
                                            var empty = {};
                                            empty[scope.visualField.key] = null;
                                            empty[scope.visualField.display] = null;
                                            var contentData = [empty];
                                            contentData = contentData.concat(data2.content);
                                            if (!scope.field.required) {
                                                scope.visualField.objectValues = contentData;
                                                scope.referenceTotalElements = data2.totalElements + 1;
                                            } else {
                                                scope.visualField.objectValues = data2.content;
                                                scope.referenceTotalElements = data2.totalElements;
                                            }
                                        },
                                        function (err) {
                                            $log.error(err);
                                        }
                                    );
                                }
                            }

                        };

                        /**
                         * Executes a table sort
                         *
                         * @param item Sort details
                         */
                        scope.orderByFieldFnFilter = function (item, current, filter, pageSize) {
                            scope.tableFilterReverseSort = !scope.tableFilterReverseSort;
                            if (scope.currentFilterValue === undefined) {
                                scope.currentFilterValue = "";
                            }
                            scope.doFilterVisualFields(scope.currentFilterValue, pageSize);
                        };
                    }
                }
            }
        }
    });
});
