define(['app', 'lodash'], function (app, _) {

    'use strict';

    return app.registerDirective('autogenSortableGridElement', ['$log', 'cacheService', '$rootScope', 'hookService', function ($log, cacheService, $rootScope, hookService) {
        return {
            restrict: 'E',
            scope: {
                tabGeneratorId: '=',
                itemData: '=',
                windowTabInfo: '='
            },
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.sortable.grid.element.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.allFields =_.sortBy(scope.windowTabInfo.fields, function (current) {
                            return current.seqno;
                        });
                        scope.singleFields = [];
                        scope.stdPref = cacheService.stdPref;
                        cacheService.loadTab(scope.windowTabInfo.idTab).then(function(tab){
                           return cacheService.loadTableById(tab.idTable);
                        }).then(function(table){
                            _.each(scope.allFields, function(field){
                                var refData = _.find(table.columns, function(column){
                                    return field.idColumn === column.idColumn;
                                });
                                if (refData && refData.reference){
                                    field.rtype = refData.reference.rtype;
                                }
                            });
                            scope.loadData();
                        });

                        scope.loadData = function(){
                            _.each(scope.allFields, function (current) {
                                if (!current.idFieldGroup) {
                                    current.bootstrapCol = (12 / scope.windowTabInfo.columns) * current.span;
                                    scope.singleFields.push(current);
                                } else {
                                    if (scope.posGroup(scope.groups, current.idFieldGroup) == -1) {
                                        var group = {
                                            name: current.name,
                                            id: current.idFieldGroup,
                                            fields: [],
                                            fieldRows: []
                                        };
                                        group.fields.push(current);
                                        scope.groups.push(group);
                                    } else {
                                        var pos = scope.posGroup(scope.groups, current.idFieldGroup);
                                        scope.groups[pos].fields.push(current);
                                    }
                                }
                            });

                            _.each(scope.groups, function (group) {
                                var fieldGroup = cacheService.getFieldGroup(group.id);
                                _.each(group.fields, function (current) {
                                    current.bootstrapGroupColumns = (12 / (fieldGroup.columns ? fieldGroup.columns : scope.adTab.columns)) * current.span;
                                });
                            });

                            // Here we define the rows according to the rules.
                            // A new row is started if any of the following applies:
                            //      1. If it is the first element
                            //      2. If the field explicitly states it starts a new row
                            //      3. If there is no more space left in the row
                            scope.fieldRows = [];
                            var currentRow;
                            var availableSpace = 12;
                            _.each(scope.singleFields, function (current, index) {
                                var newRow = {
                                    fields: []
                                };
                                var neededSpace = current.bootstrapCol;
                                if (index == 0 || current.startnewrow || availableSpace < neededSpace) {
                                    currentRow = newRow;
                                    currentRow.fields.push(current);
                                    scope.fieldRows.push(currentRow);
                                    availableSpace = 12 - neededSpace;
                                } else {
                                    currentRow.fields.push(current);
                                    availableSpace -= neededSpace;
                                }
                            });

                            // Organizing group field to show by row
                            _.each(scope.groups, function (currentG, indexG) {
                                var currentRowG;
                                var columnSpaceG = 12;
                                _.each(currentG.fields, function (currentF, indexF) {
                                    var newRow = {
                                        fields: []
                                    };
                                    var needcolumnSpaceG = ((12 / (currentG.columns ? currentG.columns : scope.windowTabInfo.columns)) * currentF.span);
                                    if (indexF == 0 || currentF.startnewrow || columnSpaceG < needcolumnSpaceG) {
                                        currentRowG = newRow;
                                        currentRowG.fields.push(currentF);
                                        currentG.fieldRows.push(currentRowG);
                                        columnSpaceG = 12 - needcolumnSpaceG;
                                    } else {
                                        currentRowG.fields.push(currentF);
                                        columnSpaceG -= needcolumnSpaceG;
                                    }
                                });
                                currentG.fields = null;
                            });

                            scope.posGroup = function (groups, idGroup) {
                                var outIndex = -1;
                                _.each(groups, function (current, index) {
                                    if (current.id == idGroup) {
                                        outIndex = index;
                                    }
                                });
                                return outIndex;
                            };

                            var table = cacheService.getTable(scope.windowTabInfo.idTable);
                            scope.keys = [];
                            scope.globalParameters = {};
                            hookService.execHooks('AD_GetGlobalParameters', { item: scope.globalParameters }).then(function () {
                                cacheService.loadTempTabledir(table, true, undefined, scope).then(function () {

                                    var columnsToCheck = _.filter(table.columns, function (current) {
                                        return _.find(scope.windowTabInfo.fields, function (field) {
                                            return field.idColumn === current.idColumn;
                                        });
                                    });
                                    _.each(columnsToCheck, function (column) {
                                        var item = scope.itemData;
                                        if (column.reference.rtype === 'YESNO') {
                                            scope.itemData[column.standardName] = scope.itemData[column.standardName] ? $rootScope.getMessage('AD_labelYes') : $rootScope.getMessage('AD_labelNo');
                                        } else if (column.reference.rtype === 'LIST') {
                                            scope.itemData[column.standardName] = cacheService.getListValue(column.idReferenceValue, scope.itemData[column.standardName]);
                                        } else if (column.reference.rtype === 'TABLE' || column.reference.rtype == 'TABLEDIR' || column.reference.rtype == 'SEARCH') {
                                            scope.itemData[column.standardName] = cacheService.getTableValue(column, item);
                                        } else if (column.reference.rtype === 'IMAGE') {
                                            scope.itemData[column.standardName + '_imgSrc'] = scope.stdPref.serverCacheUrl + scope.itemData['updated'] + '/files/' + table.name + '/' + scope.itemData.id + '/' + scope.itemData[column.standardName];
                                        }
                                    });
                                    scope.keys = _.keys(scope.itemData);
                                });
                            });
                        };

                        $log.info("Loading autogenSortableGridElement: " + scope.itemData.id);
                    }
                }
            }
        }
    }]);
});
