/**
 * This directive generates the window and complete tab
 * It receives the following parameters in the form of attributes
 *
 *      window-generator-id        ->  The idWindow key
 *
 * The following is an example usage of the directive
 *
 *  <autogen-window-tabs window-generator-id="48b71f4d50ebd5f20150ebdfc3580018"></autogen-window-tabs>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('autogenWindowTabs', ['$rootScope', '$compile', '$injector', 'cacheService', 'tabManagerService', '$log', function ($rootScope, $compile, $injector, cacheService, tabManagerService, $log) {
        return {
            restrict: 'E',
            require: '?ngModel',
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.window.tabs.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.hasRole = $rootScope.hasRole;
                        scope.hasPermission = $rootScope.hasPermission;
                        scope.adTab = {};
                        scope.windowGeneratorId = attributes.windowGeneratorId;
                        scope.rowId = null;

                        scope.showDirective = false;

                        scope.$on('cancel_to_parent', function (event, data) {
                            scope.showDirective = false;
                        });

                        scope.selectTab = function (tab) {
                            $('a[data-toggle="tab"]').unbind('shown.bs.tab').bind('shown.bs.tab', function (e) {
                                $rootScope.$broadcast(app.eventDefinitions.GLOBAL_TAB_SELECTED, {idTab: $(e.target)[0].id});
                            });
                        };

                        // Loading the row of ad_window
                        cacheService.loadWindow(scope.windowGeneratorId).then(function (data) {
                            scope.window = data;
                            tabManagerService.setTabHeading(scope.windowGeneratorId, scope.window.name);
                            var excludedSortable = _.filter(data.tabs, function (current) {
                                return current.tab && current.tab.uipattern !== "SORTABLE" && current.tab.runtime && current.tab.runtime.defaultTab;
                            });
                            var mainTabs = _.filter(excludedSortable, function (current) {
                                return (current.tab && current.tab.tablevel === 0);
                            });
                            scope.adTab = undefined;
                            if (mainTabs.length === 1) {
                                scope.adTab = mainTabs[0].tab;
                            } else if (mainTabs.length > 1) {
                                var found = _.find(mainTabs, function (current) {
                                    return current.tab.viewDefault;
                                });
                                if (!found) {
                                    found = mainTabs[0];
                                }
                                scope.adTab = found.tab;
                            }
                            if (!scope.adTab) {
                                $log.error("No tab of level 0 was found for window: " + scope.windowGeneratorId);
                                return;
                            }

                            if (scope.adTab.runtime)
                                scope.adTab.runtime.readonly = false;
                            scope.initialCharts = _.filter(excludedSortable, function (current) {
                                var result = current.tab.tablevel === 0;
                                var found = _.find(mainTabs, function (currentTab) {
                                    return current.tab.idTab === currentTab.tab.idTab && currentTab.tab.idTab !== scope.adTab.idTab;
                                });
                                if (found && current.tab.ttype !== "CHART") //Meaning that the currently analysed tab should not be displayed because there is already a default related tab
                                    result = false;
                                if (result) {
                                    var displaylogic = cacheService.replaceJSConditional(null, null, current.tab.displaylogic);
                                    if (displaylogic)
                                        result = eval(displaylogic);
                                }
                                return result;
                            });
                            scope.initialCharts = _.sortBy(scope.initialCharts, function (current) {
                                return current.tab.seqno;
                            });
                            scope.windowTitle = scope.window.name;
                            scope.tabLoaded = true;
                        }).catch(function (err) {
                            // TODO: Error handling
                        });
                    }
                }
            }
        }
    }]);
});
