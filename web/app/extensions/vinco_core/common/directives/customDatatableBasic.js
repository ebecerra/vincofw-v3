define(['app',
    'datatables',
    'datatables-responsive',
    'datatables-colvis',
    'datatables-tools',
    'datatables-bootstrap'
], function (app) {

    'use strict';

    var directive = app.registerDirective('customDatatableBasic', function ($rootScope, $compile, $timeout) {
        return {
            restrict: 'A',
            require: '?ngModel',
            scope: {
                gridData: '=grid',
                tableOptions: '='
            },
            compile: function (tElement, tAttributes) {


                return {
                    post: function (scope, element, attributes, ngModel) {
                        //$timeout(function () {
                        /* // DOM Position key index //

                         l - Length changing (dropdown)
                         f - Filtering input (search)
                         t - The Table! (datatable)
                         i - Information (records)
                         p - Pagination (paging)
                         r - pRocessing
                         < and > - div elements
                         <"#id" and > - div with an id
                         <"class" and > - div with a class
                         <"#id.class" and > - div with an id and class

                         Also see: http://legacy.datatables.net/usage/features
                         */
                        $rootScope.helper = {};
                        var options = {
                            "sDom": "t",
                            "bSort": false,
                            oLanguage: {
                                "sSearch": " ",
                                "sLengthMenu": "_MENU_"
                            },
                            "paging": false,
                            "autoWidth": false,
                            "smartResponsiveHelper": null,
                            "preDrawCallback": function () {
                                // Initialize the responsive datatables helper once.
                                if (!this.smartResponsiveHelper) {
                                    var breakpoints = {};
                                    for (var i = 2; i < 20; i++) {
                                        breakpoints["hide" + i] = 480 + (200 * (i - 2))
                                    }
                                    this.smartResponsiveHelper = new ResponsiveDatatablesHelper(element, breakpoints);
                                }
                                $rootScope.helper.smartResponsiveHelper = this.smartResponsiveHelper;
                            },
                            "rowCallback": function (nRow) {
                                $rootScope.helper.smartResponsiveHelper.createExpandIcon(nRow);
                            },
                            "drawCallback": function (oSettings) {

                                var tableWidth = $(element).css("width");
                                var restrictWidth = $(element).parentsUntil('.modal-body').css("width");
                                if (tableWidth > restrictWidth) {
                                    var tableId = element[0].id;
                                    // if ($rootScope.dataTableElements[tableId]) {
                                    //     $rootScope.dataTableElements[tableId].dataTable().fnDestroy();
                                    // }
                                    // $rootScope._dataTables[tableId] = $rootScope.dataTableElements[tableId].DataTable($rootScope.dataTableOptions);

                                }
                                $rootScope.helper.smartResponsiveHelper.respond();
                            },
                            "headerCallback": function (oSettings) {
                            },
                            "rowCreatedCallback": function (oSettings) {
                            }
                        };

                        if (attributes.tableOptions) {
                            options = angular.extend(options, scope.tableOptions)
                        }

                        var _dataTable;

                        var childFormat = element.find('.smart-datatable-child-format');
                        console.log(childFormat);
                        if (childFormat.length) {
                            var childFormatTemplate = childFormat.remove().html();
                            element.on('click', childFormat.data('childControl'), function () {
                                var tr = $(this).closest('tr');
                                var row = _dataTable.row(tr);
                                if (row.child.isShown()) {
                                    // This row is already open - close it
                                    row.child.hide();
                                    tr.removeClass('shown');
                                }
                                else {
                                    // Open this row
                                    var childScope = scope.$new();
                                    childScope.d = row.data();
                                    var html = $compile(childFormatTemplate)(childScope);
                                    row.child(html).show();
                                    tr.addClass('shown');
                                }
                            })
                        }

                        $timeout(function () {

                            //_dataTable = element.DataTable(options);
                            $rootScope.dataTableElement = element;
                            $rootScope.dataTableOptions = options;
                            if ($rootScope.dataTableElements === undefined)
                                $rootScope.dataTableElements = {};
                            var table = element[0];
                            var tableId = table.id;
                            $rootScope.dataTableElements[tableId] = element;

                            if (ngModel) {
                                ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.

                                    var table = element[0];
                                    var tableId = table.id;
                                    if ($rootScope._dataTables && $rootScope._dataTables[tableId]) {
                                        $rootScope._dataTables[tableId].destroy();
                                        $rootScope._dataTables[tableId] = null;
                                    }

                                    $rootScope.dataTableElements[tableId] = element;
                                    $rootScope.$broadcast(app.eventDefinitions.TABLE_RESPONSIVE_LOADED, {
                                        options: options,
                                        tableId: tableId
                                    });
                                };
                            }

                            $rootScope.$broadcast(app.eventDefinitions.TABLE_RESPONSIVE_LOADED, {
                                options: options,
                                tableId: tableId
                            });

                        }, 0, true);
                    }
                }
            }
        }
    });

    /**
     * This directive is to be used in pairs with customDatatableBasic directive.
     * It should be placed as an attribute for th element so we can apply the corresponding data-hide class
     * according to the header position in the table
     * Example usage:
     *  <th responsive-table-header ng-attr-data-hide="{{getHiddenClass($index)}}"></th>
     */
    directive = directive.registerDirective('responsiveTableHeader', function ($rootScope) {
        return {
            restrict: 'A',
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes) {
                        scope.getHiddenClass = function (index) {
                            if (index < 2)
                                return undefined;
                            var label = "hide2";
                            for (var i = 3; i <= index; i++) {
                                label += (",hide" + i);
                            }
                            return label;
                        }
                    }
                }
            }
        }
    });
    return directive;
});
