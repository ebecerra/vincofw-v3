/**
 * This directive should be used to obtain reference values from TABLE components. The following attributes
 * should be used with the directive. The directive applies ONLY as an attribute:
 *  *   field-attr          The field being processed. Should be a structure with the following information
 *                          {{
 *                              idReferenceValue: Reference value ID,
 *                              name: Field column name
 *                              idColumn: Unique identifier for the field
 *                              rtype: Reference type
 *                              sqlwhere: SQL where clause to add to the filter
 *                              sqlorderby: SQL order clause to use, in case it is needed
 *                           }}
 *  *   selected-fn-attr    A callback function that the caller scope should implement to receive the selected value
 *                           The callback will receive three parameters.
 *                              idColumnData: Unique identifier for the field (as received in **field-attr**
 *                              fieldValueData: Selected value
 *                              fieldDisplayData: Selected value's display representation
 *                        In order to guarantee the callback function is called, the rest of the following attributes
 *                          (including selected-fn-attr), should be present exactly as presented below:
 *
 *                              selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                              id-column-data-attr="idColumnData"
 *                              field-value-data-attr="fieldValueData"
 *                              field-display-data-attr="fieldDisplayData"
 *
 * The caller scope should implement the callback function like this:
 *
 *          scope.selectElement = function(idColumnData, fieldValueData, fieldDisplayData){
 *                                   // custom logic goes here
 *                                  };
 *
 *  Example usage
 *
 *      <div class="input input-file">
 *          <span class="button">
 *              <input type="text"
 *                      input-search-selector
 *                      field-attr="{{getFieldAttr(field)}}"
 *                      item-attr="{{fieldValue}}"
 *                      tab-id-attr="{{scope.tab}}"
 *                      selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                      id-column-data-attr="idColumnData"
 *                      field-value-data-attr="fieldValueData"
 *                      field-display-data-attr="fieldDisplayData">
 *              <i class="fa fa-search"></i>
 *          </span>
 *          <input type="text" placeholder="" readonly="" value="">
 *      </div>
 */
define(['app', 'moment'], function (app, moment) {

    'use strict';

    return app.registerDirective('inputSearchSelector', [
        '$templateRequest', '$compile', '$log', '$q', '$rootScope', 'cacheService', 'autogenService', 'referenceResolverService',  'coreConfigService',
        function ($templateRequest, $compile, $log, $q, $rootScope, cacheService, autogenService, referenceResolverService, coreConfigService) {
            return {
                restrict: 'A',
                scope: {
                    fieldAttrData: '@fieldAttr',
                    tabIdAttrData: '@tabIdAttr',
                    selectedElementFunction: '&selectedFnAttr'
                },
                require: 'ngModel',
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs, ngModel) {

                            scope.referenceData = {};
                            scope.maxSize = 3;
                            scope.tablePageSizeList = [10, 20, 40, 60, 100];
                            element.removeAttr('input-search-selector');
                            scope.field = JSON.parse(scope.fieldAttrData);
                            scope.complicateVisualFields = {};
                            scope.visualField = {
                                fieldReferenceValueId: scope.field.idReferenceValue,
                                type: scope.field.rtype,
                                key: null,
                                display: null,
                                columnName: scope.field.name,
                                objectValues: []
                            };
                            scope.yesNoValues = [
                                {value: null, name: ''},
                                {value: true, name: $rootScope.getMessage('AD_labelYes')},
                                {value: false, name: $rootScope.getMessage('AD_labelNo')}
                            ];
                            scope.referenceData = {
                                referencePageSize: 10,
                                referenceCurrentPage: 1,
                                currentFilterValue: {}
                            };

                            /**
                             * Sets display data and notifies selected item has been changed
                             */
                            scope.setVisualValue = function () {
                                var value = _.find(scope.visualField.objectValues, function (current) {
                                    return (ngModel.$viewValue === current[scope.visualField.key]);
                                });
                                if (value) {
                                    // Setting display information
                                    element.parent().next().val(value[scope.visualField.display]);
                                    // Notifying selected item was changed
                                    scope.selectedElementFunction({
                                        idColumnData: scope.field.idColumn,
                                        fieldValueData: value[scope.visualField.key],
                                        fieldDisplayData: value[scope.visualField.display]
                                    });
                                }
                            };

                            scope.watches= {
                                page: null,
                                value: null
                            };

                            scope.registerWatches = function () {

                                if (!scope.watches.page) {
                                    scope.watches.page = scope.$watchCollection("[referenceData.referenceCurrentPage]", function (oldValue, newValue) {
                                        if (JSON.stringify(oldValue) === JSON.stringify(newValue))
                                            return; // If filters haven't changed there is no need to request data from server
                                        if (scope.visualField) {
                                            scope.doFilterVisualFields(scope.referenceData.currentFilterValue, scope.referenceData.referencePageSize, scope.referenceData.referenceCurrentPage);
                                        }
                                    });
                                }
                                if (!scope.watches.value) {
                                    scope.watches.value = scope.$watchCollection("[referenceData.referencePageSize,referenceData.currentFilterValue]", function (oldValue, newValue) {
                                        if (JSON.stringify(oldValue) === JSON.stringify(newValue))
                                            return; // If filters haven't changed there is no need to request data from server
                                        var page = 1;
                                        scope.doFilterVisualFields(scope.referenceData.currentFilterValue, scope.referenceData.referencePageSize, page);
                                        // }
                                    });
                                }
                            };

                            /**
                             * Loads the reference's key and display information
                             *
                             * @param itemId If passed, requests the item that matches the required itemId according to
                             *              the reference information
                             */
                            scope.resolveData = function (itemId) {
                                var defered = $q.defer();
                                var promise = defered.promise;
                                referenceResolverService.resolveTableData(itemId, scope.field.idReferenceValue)
                                    .then(function (data) {
                                        scope.registerWatches();
                                        scope.visualField = data.visualField;
                                        scope.referenceTotalElements = data.referenceTotalElements;
                                        if (itemId) {
                                            scope.setVisualValue();
                                        }
                                        defered.resolve();
                                    })
                                    .catch(function (err) {
                                        scope.registerWatches();
                                        defered.reject(err);
                                        $log.error(err);
                                    });
                                return promise;
                            };

                            scope.ngModel = ngModel;
                            if (ngModel) {
                                ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                    if ((!scope.visualField.objectValues
                                        || !scope.visualField.objectValues
                                        || scope.visualField.objectValues.length === 0) && ngModel.$viewValue) {
                                        scope.resolveData(ngModel.$viewValue);
                                    } else {
                                        scope.setVisualValue();
                                    }
                                };

                            }

                            scope.$on(coreConfigService.events.NAVIGATION_REFRESH_REFERENCE, function (event, data) {
                                if (data.idColumn === scope.field.idColumn) {
                                    if (data.value) {
                                        scope.resolveData(scope.currentFilterValue);
                                    }
                                    else
                                        scope.setVisualValue();
                                }
                            });

                            scope.getColumnCaption = $rootScope.getColumnCaption;
                            scope.getMessage = $rootScope.getMessage;


                            var idReferenceValue = scope.field.idReferenceValue;
                            var refTable = cacheService.getReference(idReferenceValue);
                            if (refTable) {
                                scope.searchColumns = _.sortBy(refTable.info.searchColumns, function (current) {
                                    return current.seqno;
                                });
                                var table = cacheService.getTable(refTable.info.idTable);
                                cacheService.loadTempTabledir(table, true, scope.searchColumns).then(function () {

                                    // Showing selector dialog when clicking on textbox
                                    var clickEvent = function (event) {
                                        scope.$apply(function () {
                                            scope.currentColumnName = scope.field.name;
                                            scope.referenceData.referencePageSize = 10;
                                            scope.referenceData.referenceCurrentPage = 1;
                                            scope.referenceData.currentFilterValue = {};
                                            scope.tableFilterReverseSort = false;

                                            $('#' + scope.searchInputDialogId).on("shown.bs.modal", function () {
                                                //TODO
                                                if (scope.searchColumns)
                                                    $('#searchFilter' + scope.searchColumns[0].name)[0].focus();
                                                if (ngModel.$viewValue === null || ngModel.$viewValue === undefined) {
                                                    scope.resolveData()
                                                        .then(function () {
                                                            scope.doFilterVisualFields('', scope.referenceData.referencePageSize, scope.referenceData.referenceCurrentPage);
                                                        });
                                                }
                                                else {
                                                    scope.doFilterVisualFields('', scope.referenceData.referencePageSize, scope.referenceData.referenceCurrentPage);
                                                }
                                            });
                                            $('#' + scope.searchInputDialogId).modal();

                                        });
                                    };
                                    element.on('click',
                                        clickEvent
                                    );
                                    element.parent().siblings().on('click',
                                        clickEvent
                                    );

                                    scope.searchInputDialogId = "search_dialog" + scope.field.idColumn;
                                    scope.searchInputWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.input.search.selector.window.html?v=' + FW_VERSION;

                                    $templateRequest(scope.searchInputWindowDialogTemplate).then(function (html) {
                                        var template = angular.element(html);
                                        element.closest("#content").append(template);
                                        scope.currentColumnName = scope.field.name;
                                        scope.referenceData.referencePageSize = 10;
                                        scope.referenceData.referenceCurrentPage = 1;
                                        scope.referenceData.currentFilterValue = {};
                                        scope.tableFilterReverseSort = false;
                                        $compile(template)(scope);
                                    });

                                    scope.updateFilters = function () {
                                        var page = 1;
                                        scope.doFilterVisualFields(scope.referenceData.currentFilterValue, scope.referenceData.referencePageSize, page);
                                    };

                                    /**
                                     * Calls the callback function to return the selected value
                                     * Returns a structure with the following information
                                     *  - idColumnData: The id of the field as passed from the caller
                                     *  - fieldValueData: The selected value
                                     *  - fieldDisplayData: The selected value String representation                         *
                                     *
                                     * @param item Selected item
                                     */
                                    scope.selectTableValue = function (item) {
                                        var key = scope.visualField.key;
                                        var col = scope.visualField.display;
                                        element.parent().next().val(item[col]);
                                        scope.selectedElementFunction(
                                            {
                                                idColumnData: scope.field.idColumn,
                                                fieldValueData: item[key],
                                                fieldDisplayData: item[col]
                                            });
                                        $('#' + scope.searchInputDialogId).modal('hide');
                                    };

                                    /**
                                     * Requests a page to display in the dialog
                                     *
                                     * @param filter Filter information
                                     * @param pageSize Page size
                                     * @param currentPage Page number. If not present, 1 will be assumed
                                     */
                                    scope.doFilterVisualFields = function (filter, pageSize, currentPage) {
                                        // if (scope.visualField.display) {
                                        if (currentPage) {
                                            scope.referenceData.referenceCurrentPage = currentPage;
                                        } else {
                                            scope.referenceData.referenceCurrentPage = 1;
                                        }
                                        scope.referenceData.referencePageSize = pageSize;
                                        var filterKeys = _.keys(filter);
                                        scope.searchFilter = "";
                                        _.each(filterKeys, function (current) {
                                            if (filter[current] === undefined || filter[current] === null || filter[current] === "") return;
                                            var column = _.find(scope.searchColumns, function (column) {
                                                return column.standardName == current;
                                            });
                                            var filterValue = "%" + filter[current] + "%";
                                            if (column &&
                                                (column.columnInfo.ctype === 'INTEGER'
                                                || column.columnInfo.ctype === 'NUMBER'
                                                || column.columnInfo.reference.rtype === 'YESNO' )
                                            ) {
                                                filterValue = filter[current];
                                            }
                                            scope.searchFilter += current + "=" + filterValue + ",";
                                        });
                                        scope.loadFilterData(scope.searchFilter);
                                        // }
                                    };

                                    /**
                                     * Loads dialog page with filter information
                                     *
                                     * @param customFilter Filter information
                                     */
                                    scope.loadFilterData = function (customFilter) {
                                        var q = customFilter;
                                        var idReferenceValue = scope.field.idReferenceValue;
                                        if (!customFilter) {
                                            q = scope.field.sqlwhere;
                                        }

                                        var sort = scope.visualField.displaySource == 'DATABASE' ? scope.visualField.display : null;
                                        if (scope.tableOrderByField)
                                            sort = scope.tableOrderByField;
                                        if (scope.field.sqlorderby) {
                                            sort = scope.field.sqlorderby;
                                        }
                                        var _service = null;
                                        var refTable = cacheService.getReference(idReferenceValue);
                                        if (refTable) {
                                            scope.searchColumns = _.sortBy(refTable.info.searchColumns, function (current) {
                                                return current.seqno;
                                            });
                                            if (cacheService.isCompleteConditional(refTable.info.sqlwhere)) {
                                                var table = cacheService.getTable(refTable.info.idTable);
                                                try {
                                                    _service = autogenService.getAutogenService(table);
                                                } catch (e) {
                                                    $log.error(e);
                                                }
                                                $log.info("Requesting data from server for table: " + table.name + " with query: " + q);
                                                _service.query(
                                                    {
                                                        idClient: cacheService.loggedIdClient(),
                                                        q: q,
                                                        sort: sort ? '[{ "property":"' + sort + '","direction":"' + (scope.tableFilterReverseSort ? 'DESC' : 'ASC') + '" }]' : null,
                                                        page: scope.referenceData.referenceCurrentPage,
                                                        limit: scope.referenceData.referencePageSize
                                                    },
                                                    function (data2) {
                                                        var empty = {};
                                                        empty[scope.visualField.key] = null;
                                                        empty[scope.visualField.display] = null;
                                                        var contentData = [empty];
                                                        _.each(data2.content, function (current) {
                                                            _.each(scope.searchColumns, function (currentColumn) {
                                                                var column = currentColumn.columnInfo;
                                                                if (column === undefined || column === null)
                                                                    column = _.find(table.columns, function (column) {
                                                                        return column.standardName == currentColumn.standardName;
                                                                    });
                                                                if (column) {
                                                                    currentColumn.columnInfo = column;
                                                                    if (column.reference.rtype == 'YESNO') {
                                                                        current[column.standardName] = current[column.standardName] ? $rootScope.getMessage('AD_labelYes') : $rootScope.getMessage('AD_labelNo');
                                                                    } else if (column.reference.rtype == 'LIST') {
                                                                        current[column.standardName] = cacheService.getListValue(column.idReferenceValue, current[column.standardName]);
                                                                    } else if (column.reference.rtype == 'TABLE' || column.reference.rtype == 'TABLEDIR') {
                                                                        current[column.standardName] = cacheService.getTableValue(column, current);
                                                                    } else if (column.reference.rtype == 'IMAGE') {
                                                                        current[column.standardName + '_imgSrc'] = scope.stdPref.serverCacheUrl + current['updated'] + '/files/' + table.name + '/' + current.id + '/' + current[column.standardName];
                                                                    }
                                                                }
                                                            })
                                                        });
                                                        contentData = contentData.concat(data2.content);
                                                        if (!scope.field.required) {
                                                            scope.visualField.objectValues = contentData;
                                                            scope.referenceTotalElements = data2.totalElements + 1;
                                                        } else {
                                                            scope.visualField.objectValues = data2.content;
                                                            scope.referenceTotalElements = data2.totalElements;
                                                        }

                                                        _.each(scope.searchColumns, function (currentColumn) {
                                                            cacheService.loadReferenceData(currentColumn.idReference).then(function (rtype) {
                                                                cacheService.loadReferenceData(currentColumn.idReference, rtype).then(function (data) {

                                                                })
                                                            })
                                                        });
                                                    },
                                                    function (err) {
                                                        $log.error(err);
                                                    }
                                                );
                                            }
                                        }

                                    };

                                    /**
                                     * Executes a table sort
                                     *
                                     * @param item Sort details
                                     */
                                    scope.orderByFieldFnFilter = function (item, current, filter, pageSize) {
                                        scope.tableFilterReverseSort = !scope.tableFilterReverseSort;
                                        scope.tableOrderByField = item.name;
                                        if (scope.currentFilterValue === undefined) {
                                            scope.currentFilterValue = "";
                                        }
                                        scope.doFilterVisualFields(scope.currentFilterValue, pageSize);
                                    };

                                    /**
                                     * Gets the data needed for the input of the TABLE, SEARCH, LIST AND TABLEDIR search component
                                     *
                                     * @param currentColumn Field to obtain data from
                                     * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                                     */
                                    scope.getListFieldAttr = function (currentColumn) {
                                        return {
                                            idReferenceValue: currentColumn.columnInfo.idReferenceValue,
                                            name: currentColumn.columnInfo.name,
                                            idColumn: currentColumn.columnInfo.idColumn,
                                            rtype: currentColumn.columnInfo.reference.rtype,
                                            readOnly: false,
                                            required: false
                                        };
                                    };

                                    /**
                                     * Callback to be used by components SEARCH, LIST, TABLE and TABLEDIR.
                                     * Obtains the selected filter value from components SEARCH, LIST, TABLE and TABLEDIR
                                     *
                                     * @param idColumnData Selected row id
                                     * @param fieldValueData Selected row value
                                     * @param fieldDisplayData Currently unused.
                                     */
                                    scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                                        var column = _.find(this.searchColumns, function (current) {
                                            return current.idColumn == idColumnData
                                        });
                                        if (column) {
                                            this.referenceData.currentFilterValue[column.standardName] = fieldValueData;
                                            this.doFilterVisualFields(this.referenceData.currentFilterValue, this.referenceData.referencePageSize, 1);
                                        }
                                    };
                                });
                            }
                        }
                    }
                }
            }
        }]);
});
