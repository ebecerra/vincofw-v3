/**
 * This directive generates a grid corresponding to the data defined in the given tab
 * It receives the following parameters in the form of attributes
 *
 *      tab-generator-id        ->  The idTab key
 *      table-generator-title   ->  The text to show in the title of the table
 *      table-generator-icon    ->  The icon class to use in the table header
 *      table-reference-item    ->  Parent item associated to the grid (the items of the grid by this item's reference id)
 *      hide-header             ->  Hides the header caption and icon
 *      table-filter            ->  Linked column filter value
 *
 * The following is an example usage of the directive
 *
 *  <autogen-table-generator tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018"
 *      table-generator-title="window.title"
 *      table-generator-icon = "fa-list-alt"></autogen-table-generator>
 */
define(['app', 'lodash'], function (app, _) {

    'use strict';

    return app.registerDirective('autogenTableGridGenerator', [
        '$rootScope', '$window', '$timeout', '$log', '$state', '$modal', '$compile', 'Authentication',
        'commonService', 'cacheService', 'autogenService', 'coreConfigService', 'hookService', '$stateParams',
        function ($rootScope, $window, $timeout, $log, $state, $modal, $compile, Authentication,
                  commonService, cacheService, autogenService, coreConfigService, hookService, $stateParams) {
            return {
                restrict: 'E',
                scope: {
                    tableElements: '=',
                    start: '=',
                    step: '=',
                    tabGeneratorId: '=',
                    tableOrderByField: '=',
                    tableReverseSort: '=',
                    relatedTabGeneratorId: '='
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.table.grid.generator.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes) {
                            angular.element(element).addClass('active in');
                            var thatScope = scope;

                            scope.checkAll = false;
                            scope.yesNoValues = [
                                { value: null, name: '' },
                                { value: true, name: $rootScope.getMessage('AD_labelYes') },
                                { value: false, name: $rootScope.getMessage('AD_labelNo') }
                            ];

                            scope.$on(coreConfigService.events.NAVIGATION_REFRESH,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        cacheService.loadTab(scope.tabGeneratorId, data.reloadColumns).then(function(){
                                            scope.loadTabInformation();
                                        });
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_CHANGE_VIEW_MODE,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.showCardMode = !scope.showCardMode;
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_APPLY_SORT,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.showSorting()
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_CANCEL_SORT,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.showSorting()
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_REFRESH_SORT_VALUES, function (event, data) {
                                scope.tableData.sortStep = data.step;
                                scope.tableData.sortStart = data.start;
                            });

                            scope.$on('$destroy', function () {
                                // disable the listener
                                scope.clearTimeouts();
                                cacheService.clearReferenceTableDataValue(scope.$id);
                            });

                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            /**
                             * Loads the corresponding tab information
                             */
                            scope.loadTabInformation = function () {
                                var table = cacheService.getTable(scope.tabInformation.idTable);
                                if (table) {
                                    scope.tableInfo = table;

                                    _.each(scope.tableInfo.columns, function (currentColumn) {
                                        _.each(scope.tabInformation.fields, function (field) {
                                            if (currentColumn.idColumn === field.column.idColumn)
                                                currentColumn.field = field;
                                        });
                                    });
                                    // Get grid columns
                                    scope.gridColumns = _.filter(scope.tabInformation.fields, function (current) {
                                        return current.showingrid;
                                    });
                                    scope.gridColumns = _.sortBy(scope.gridColumns, function (current) {
                                        return current.gridSeqno;
                                    });
                                    _.each(scope.gridColumns, function (column) {
                                        column.tableName = scope.tableInfo.name;
                                        var col = _.find(scope.tableInfo.columns, function (c) {
                                            return c.name === column.name;
                                        });
                                        if (col) {
                                            column.rtype = col.reference.rtype;
                                        }
                                    });
                                }
                            };

                            scope.getActionHint = function (action, item) {
                                return cacheService.replaceJSConditional(null, null, action.name, item);
                            };

                            scope.getTableValue = function (colName, item) {
                                var col = _.find(scope.tableInfo.columns, function (c) {
                                    return c.name == colName;
                                });
                                return col ? cacheService.getTableValue(col, item) : item[colName];
                            };

                            scope.getFieldType = function (column) {
                                if (column.reference) {
                                    return column.reference.rtype;
                                }
                                return column.ctype;
                            };

                            scope.throwEditClick = function (item) {
                                scope.$emit(coreConfigService.events.NAVIGATION_EDIT_ITEM, {
                                    idTab: scope.relatedTabGeneratorId,
                                    item: item
                                });
                            };

                            scope.execAction = function(){
                                var action = scope.execActionData.action;
                                var item = scope.execActionData.item;
                                if (action.atype === 'PROCESS') {
                                    cacheService.loadProcess(action.idProcess).then(function(process){
                                        action.approvals = [];
                                        _.each(scope.execActionData.approvals, function(newApproval){
                                            var existed = _.find(action.approvals, function(current){
                                                return newApproval.privilege === current.privilege;
                                            });
                                            if (!existed){
                                                action.approvals.push(newApproval);
                                            }
                                        });
                                        var cloned = JSON.parse(JSON.stringify(process));
                                        cloned.approvals = action.approvals;
                                        // cloned.privilege = action.privilege;
                                        scope.selectedProcess = cloned;
                                        if (action.uipattern === "DIALOG") {
                                            scope.item = item;
                                            $rootScope.modalProcessTab = scope.tabInformation;
                                            scope.dialogProcess = cloned;
                                            $('#' + scope.processDialogId).modal();
                                        } else {
                                            $rootScope.tabbedProcessItem = item;
                                            $rootScope.tabbedProcess = cloned;
                                            $state.go('app.process.form');
                                        }
                                    }).catch(function(error){
                                        $log.error(error);
                                    })

                                } else if (action.atype === 'WINDOW') {
                                    scope.actionWindowId = action.idWindow;
                                    scope.actionWindowTitle = cacheService.replaceJSConditional(null, null, action.name, item);
                                    if (action.uipattern === "DIALOG") {
                                        $rootScope.modalParentItem = item;
                                        $modal.open({
                                            scope: scope,
                                            templateUrl: scope.windowDialogTemplate, // loads the template
                                            backdrop: false,
                                            windowClass: 'app-modal-window',
                                            controller: function ($scope, $modalInstance) {
                                                $scope.$parent.modalInstance = $modalInstance;

                                                $scope.close = function () {
                                                    $modalInstance.dismiss('cancel');
                                                }
                                            }
                                        });
                                    } else {
                                        // TODO: Show Tab Window
                                    }
                                } else if (action.atype === 'CUSTOM') {
                                    var state = $state.get(action.restCommand);
                                    var views = _.keys(state.views);
                                    var controller = state.views[views[0]].controller;
                                    if (state.views[views[0]].url) {
                                        // If the state has a url defined, we tell angular to move to that state
                                        $state.go(action.restCommand, {itemRow: item});
                                    } else {
                                        // If no url is defined, then we compile the associated controller and append it a hidden DIV,
                                        // so the controller can perform its logic
                                        $('<div id="ctrl" ng-controller="' + controller + '" >').appendTo('#custom-action-container');
                                        $stateParams.itemRow = item;
                                        $compile($('#custom-action-container'))(scope.$new(true));
                                    }
                                } else if (action.atype === 'LINK') {
                                    var url = cacheService.replaceJSItemConditional(item, action.url);
                                    $window.open(url, action.target);
                                }
                            };

                            scope.throwActionClick = function (action, item) {
                                cacheService.loadPrivileges().then(function(privileges) {
                                    var privilege = _.find(privileges, function (current) {
                                        return current.idPrivilege === action.privilege;
                                    });
                                    scope.execActionData = { action: action, item: item };
                                    var found = undefined;
                                    if (privilege) {
                                        found = _.find(Authentication.getLoginData().privileges, function (current) {
                                            return current.idPrivilege === privilege.idPrivilege;
                                        });
                                    }
                                    if (action.privilege && !found) {
                                        var privilege = _.find(privileges, function(current){
                                            return current.idPrivilege === action.privilege;
                                        });
                                        scope.privilegeToValidate = privilege.name;
                                        scope.displayMessage = action.privilegeDesc;
                                        scope.authorizationModal = $modal.open({
                                            template: ' <autogen-form-authorization privilege="privilegeToValidate" message="displayMessage" fn-on-approval="doOnApproval(result)"></autogen-form-authorization>',
                                            size: 'md',
                                            scope: scope

                                        });
                                        // Where are setting this line because, when the dialog is being opened inside another dialog,
                                        // the password input for the Authentication dialog loses the focus and can't be accessed
                                        scope.authorizationModal.opened.then(function(){
                                            $(document).off('focusin.modal');
                                        });
                                    } else {
                                        scope.execActionData.approvals = scope.approvals;
                                        scope.execAction();
                                    }
                                }).catch(function(error){
                                    $log.error(error);
                                });
                            };

                            /**
                             * Recalls the backend with the approval information added
                             * @param {object}  result Validation result
                             * @param {boolean} result.validated True if the user was validated. False i.o.c.
                             * @param {string}  result.idUser Id of the user that was validated.
                             */
                            scope.doOnApproval = function(result){
                                scope.authorizationModal.close();
                                if (result.validated) {
                                    var approvals = [];
                                    approvals.push({
                                        idUser: result.idUser,
                                        idClient: cacheService.loggedIdClient(),
                                        privilege: scope.privilegeToValidate,
                                        idApproval: commonService.generateUID(),
                                        idTableAction: scope.execActionData.action.idTableAction,
                                        rowkey: scope.execActionData.item.id,
                                        action: "TABLE_ACTION"
                                    });
                                    scope.execActionData.approvals = approvals;
                                    scope.execAction();
                                }
                            };

                            /**
                             * Executes a table sort
                             *
                             * @param item Sort details
                             */
                            scope.orderByFieldFn = function (item) {
                                scope.$emit(coreConfigService.events.NAVIGATION_GRID_SORT, {
                                    idTab: scope.relatedTabGeneratorId,
                                    column: item
                                });
                            };

                            /**
                             * Verifies if the delete button should be shown
                             * @param tabData
                             */
                            scope.showDelete = function (tabData) {
                                if (_.includes(tabData.uipattern, "DELETE")) {
                                    return true;
                                }
                                return tabData.uipattern === "STANDARD";
                            };

                            /**
                             * Verifies if the edit button should be shown
                             * @param tabData
                             */
                            scope.showEdit = function (tabData) {
                                if (_.includes(tabData.uipattern, "EDIT")) {
                                    return true;
                                }
                                return tabData.uipattern === "STANDARD";
                            };

                            /**
                             * Verifies if Readonly button should be shown
                             * @param tabData
                             */
                            scope.showReadonly = function (tabData) {
                                return (tabData.uipattern === 'READONLY') || ((scope.showEdit(tabData)) && tabData.runtime.readonly);
                            };

                            /**
                             * Returns the associated table service
                             *
                             * @returns {*} Service
                             */
                            scope.getTableService = function () {
                                var service = null;
                                try {
                                    service = autogenService.getAutogenService(scope.tableInfo);
                                } catch (e) {
                                    $log.error(e);
                                }
                                return service;
                            };

                            /**
                             * Triggers the selection of an item through the DoubleClick event
                             *
                             * @param item Item to be edited
                             */
                            scope.triggerDoubleClick = function (item) {
                                if (scope.showEdit(scope.tabInformation) || scope.showReadonly(scope.tabInformation)) {
                                    scope.throwEditClick(item);
                                }
                            };

                            /**
                             * Performs the CheckAll/UncheckAll logic
                             *
                             * @param checkAll CheckAll Status
                             */
                            scope.verifySelection = function (checkAll) {
                                thatScope.checkAll = checkAll;
                                _.each(scope.tableElements, function (current) {
                                    current.isCheckedGrid = checkAll;
                                });
                            };

                            /**
                             * Checks the item and deselects all the other items
                             *
                             * @param item Item to check
                             */
                            scope.checkItem = function (item) {
                                _.each(scope.tableElements, function (current) {
                                    current.isCheckedGrid = false;
                                });
                                item.isCheckedGrid = true;
                                scope.checkAll = false;
                            };

                            /**
                             * Updates the CheckAll status if any of the elments is deselected
                             */
                            scope.updateCheckAll = function () {
                                thatScope.checkAll = !_.some(scope.tableElements, function (current) {
                                    return !current.isCheckedGrid;
                                })
                            };

                            /**
                             * Show table selector dialog
                             *
                             * @param field Origin field
                             */
                            scope.showTableWindow = function (field) {
                                scope.currentIdReferenceValue = field.idReferenceValue;
                                scope.currentColumnName = field.name;
                                scope.referenceData.referencePageSize = 10;
                                scope.referenceData.referenceCurrentPage = 1;
                                scope.referenceData.currentFilterValue = "";
                                scope.tableFilterReverseSort = false;
                                scope.loadFilterData(scope.complicateVisualFields[scope.currentIdReferenceValue], '');
                                $("#" + scope.tableSearchDialogId).modal();
                            };

                            /**
                             * Hides the process dialog
                             */
                            scope.hideProcessDialog = function () {
                                $('#' + scope.processDialogId).modal('hide');
                            };

                            // Cleanup
                            // here is where the cleanup happens
                            scope.clearTimeouts = function () {
                                if (scope.timeoutFunction) {
                                    $timeout.cancel(scope.timeoutFunction);
                                }
                            };

                            scope.loadFieldValues = function (fieldReferenceValueId, type, result) {
                                scope.complicateVisualFields[fieldReferenceValueId].key = result.colKey.standardName;
                                scope.complicateVisualFields[fieldReferenceValueId].display = result.colDisplay.standardName;
                                scope.complicateVisualFields[fieldReferenceValueId].displayCaption = result.colDisplay.name;
                                scope.complicateVisualFields[fieldReferenceValueId].table = result.table;
                                scope.complicateVisualFields[fieldReferenceValueId].sqlwhere = result.sqlwhere;
                                scope.complicateVisualFields[fieldReferenceValueId].sqlorderby = result.sqlorderby;
                                scope.complicateVisualFields[fieldReferenceValueId].objectValues = result.data.content;
                                var empty = {};
                                empty[result.colKey.standardName] = '';
                                empty[result.colDisplay.standardName] = '';
                                scope.complicateVisualFields[fieldReferenceValueId].objectValues.unshift(empty);
                                scope.referenceTotalElements = result.data.totalElements;
                                // Restore filters in case they where selected
                                _.each(scope.filterInfo.filtersReference, function (current) {
                                    if (scope.complicateVisualFields[current.reference]) {
                                        var key = scope.complicateVisualFields[current.reference].key;
                                        var col = scope.complicateVisualFields[current.reference].display;
                                        scope.tableFilterValue = current.item[col];
                                        scope.filterInfo.filters[commonService.removeUnderscores(current.columnName)] = current.item[key];
                                    }
                                });
                            };

                            scope.changeColumn = function (column) {
                                _.each(scope.tableInfo.columns, function (current) {
                                    scope.notifyFieldChange(current, column);
                                });
                            };

                            /**
                             * Notifies that a change has occurred in the sort data info
                             * @param sortData
                             */
                            scope.notifySortChange = function (sortData) {
                                $rootScope.$broadcast(coreConfigService.events.NAVIGATION_REFRESH_SORT_VALUES, {
                                    idTab: scope.tabGeneratorId,
                                    start: sortData.sortStart,
                                    step: sortData.sortStep
                                });
                            };

                            /**
                             * Notify a field of change on other
                             *
                             * @param field Field notified
                             * @param changedField Change field
                             */
                            scope.notifyFieldChange = function (field, changedField) {
                                if (field.reference.rtype === 'TABLEDIR') {
                                    if (!field.referenceValue) {
                                        field.referenceValue = cacheService.getReference(field.idReferenceValue);
                                    }
                                    if (field.referenceValue.info.sqlwhere && cacheService.isFieldInConditional({column: changedField}, field.referenceValue.info.sqlwhere)) {
                                        if (field.runtime === undefined) {
                                            field.runtime = {}
                                        }
                                        field.runtime.sqlwhere = field.referenceValue.info.sqlwhere;
                                        if (!field.runtime.sqlwhere ||
                                            field.runtime.sqlwhere !== field.referenceValue.info.sqlwhere) {
                                            field.runtime.sqlwhere = field.referenceValue.info.sqlwhere;
                                        }
                                        var value = scope.filterInfo.filters[changedField.standardName];
                                        if (value) {
                                            var found = _.find(scope.filterSet, function (current) {
                                                return current.idColumn === field.idColumn;
                                            });
                                            if (found) {
                                                found.runtime = {
                                                    sqlwhere: cacheService.replaceSQLConditional({column: changedField}, value, field.runtime.sqlwhere)
                                                };
                                            }
                                        }
                                    }
                                }
                            };

                            cacheService.loadTab(scope.tabGeneratorId).then(function (tab) {
                                scope.tabInformation = tab;

                                scope.loadTabInformation();

                                scope.editingWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.editing.window.html?v=' + FW_VERSION;
                                scope.processDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.dialog.html?v=' + FW_VERSION;
                                scope.windowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.window.dialog.html?v=' + FW_VERSION;
                                scope.tableGeneratorTitle = attributes.tableGeneratorTitle;
                                scope.processDialogId = 'processModal' + scope.tabGeneratorId + scope.$id;
                                scope.windowDialogId = 'windowModal' + scope.tabGeneratorId + scope.$id;
                                scope.tableReferenceItem = null;
                                scope.tableSearchDialogId = 'tableModal' + scope.tabGeneratorId;
                                scope.windowGeneratorId = attributes.windowGeneratorId;
                                scope.sortMode = false;
                                scope.showCardMode = false;

                                /**
                                 * Defines if the sortable grid should be in listing mode or sortable mode. Listing mode prevents the sort actions
                                 * @type {boolean}
                                 */
                                scope.cardMode = false;
                                if (attributes.tableReferenceItem) {
                                    scope.tableReferenceItem = JSON.parse(attributes.tableReferenceItem);
                                }
                                if (attributes.tabbedCharts) {
                                    scope.tabbedCharts = JSON.parse(attributes.tabbedCharts);
                                } else {
                                    scope.tabbedCharts = [];
                                }
                                scope.linkFilters = {};
                                // Sort order. False equals ASC, True equals DESC
                                scope.tableReverseSort = false;
                                scope.tableFilterReverseSort = false;
                                // Order by field
                                scope.valueFilterTableDir = '';
                                //For filter type TABLE
                                scope.complicateVisualFields = {};
                                scope.currentIdReferenceValue = null;
                                scope.currentColumnName = null;
                                scope.referenceData = {
                                    referencePageSize: 10,
                                    referenceCurrentPage: 1,
                                    currentFilterValue: ""
                                };
                                scope.tableFilterValue = '';

                                /**
                                 * Reloads table data
                                 *
                                 * @param {Boolean} reloadColumns  Defines if the columns should be reloaded as well
                                 */
                                scope.reloadTableData = function (reloadColumns) {
                                    function loadTableInformation(){
                                        var table = cacheService.getTable(scope.tabInformation.idTable);
                                        scope.tableActions = table.actions;
                                        // Actions
                                        _.each(scope.tableElements, function (elem) {
                                            var actions = [];
                                            _.each(table.actions, function (action) {
                                                var displayed = true;
                                                if (action.displaylogic) {
                                                    var displaylogic = cacheService.replaceJSItemConditional(elem, action.displaylogic);
                                                    if (cacheService.isCompleteConditional(displaylogic)) {
                                                        displayed = eval(displaylogic);
                                                    } else {
                                                        displayed = false;
                                                    }
                                                }

                                                if (displayed) {
                                                    actions.push(action);
                                                }
                                            });
                                            elem['$$actions'] = actions;
                                        });

                                        scope.globalParameters = {};
                                        hookService.execHooks('AD_GetGlobalParameters', { item: scope.globalParameters }).then(function () {
                                            // Values replacement
                                            cacheService.loadTempTabledir(table, true, undefined, scope).then(function () {
                                                _.each(scope.gridColumns, function (column) {
                                                    var tableColumn = _.find(table.columns, function (col) {
                                                        return col.idColumn === column.idColumn;
                                                    });
                                                    if (tableColumn) {
                                                        if (tableColumn.reference.rtype === 'YESNO') {
                                                            _.each(scope.tableElements, function (elem) {
                                                                elem[column.standardName] = elem[column.standardName] ? $rootScope.getMessage('AD_labelYes') : $rootScope.getMessage('AD_labelNo');
                                                            });
                                                        } else if (tableColumn.reference.rtype === 'LIST') {
                                                            _.each(scope.tableElements, function (elem) {
                                                                elem[column.standardName] = cacheService.getListValue(tableColumn.idReferenceValue, elem[column.standardName]);
                                                            });
                                                        } else if (tableColumn.reference.rtype === 'TABLE' || tableColumn.reference.rtype === 'TABLEDIR' || tableColumn.reference.rtype === 'SEARCH') {
                                                            _.each(scope.tableElements, function (elem) {
                                                                elem[column.standardName] = cacheService.getTableValue(tableColumn, elem);
                                                            });
                                                        }
                                                    }
                                                });
                                            }).catch(function (err) {
                                                $log.error(err);
                                            });
                                        });
                                    }
                                    if (!reloadColumns){
                                        loadTableInformation();
                                    } else {
                                        cacheService.loadTab(scope.tabGeneratorId, true).then(function(){
                                            loadTableInformation();
                                        })
                                    }
                                };

                                if (scope.windowTabInfo) {
                                    scope.parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel
                                    });
                                }

                                scope.$watchCollection('[tableElements]', function () {
                                    scope.reloadTableData();
                                });
                            }).catch(function (error) {
                                $log.error(error);
                            });
                        }
                    }
                }
            }
        }
    ]);
});
