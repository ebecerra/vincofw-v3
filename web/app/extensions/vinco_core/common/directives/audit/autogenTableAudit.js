define(['app', 'lodash'], function (app, _) {

    /**
     * @ngdoc directive
     * @name app.directive:autogenTableAudit
     * @description Generates the view for the Audit tab
     * @restrict E
     * @param {Object} tableReferenceItem Item for which we are demanding the audit list
     * @param {string} idTable Table ID of the item being analyzed
     */
    'use strict';

    return app.registerDirective('autogenTableAudit', ['Authentication', 'adApprovalService', 'commonService', 'cacheService', '$log', '$rootScope', 'autogenService',
        function (Authentication, adApprovalService, commonService, cacheService, $log, $rootScope, autogenService) {
            return {
                restrict: 'E',
                scope: {
                    tableReferenceItem: "=",
                    idTable: "="
                },
                replace: true,
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/audit/autogen.table.audit.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            /********************************************************************
                             * Variables
                             ********************************************************************/
                            /**
                             * Template to show the global audit information for the item
                             */
                            scope.auditTemplateUrl = APP_EXTENSION_VINCO_CORE + 'common/views/audit/autogen.audit.content.horizontal.html?v=' + FW_VERSION;
                            /**
                             * Selected approval for which to display the details
                             */
                            scope.selectedAudit = null;

                            /**
                             * Set to true to display child elements
                             * @type {boolean}
                             */
                            scope.initialized = false;
                            /********************************************************************
                             * End Variables
                             ********************************************************************/

                            scope.unWatchVisible = scope.$watch(function () {
                                    return $(element).is(":visible");
                                },  function(newValue, oldValue){
                                    if (newValue && newValue!==oldValue){
                                        scope.initializeElement(true);
                                    }
                                },
                                true);

                            scope.initializeElement = function(initialzed){
                                scope.initialized = true;
                            };

                            /**
                             * Sets the selected audit item
                             * @param {Object} audit Audit information
                             */
                            scope.selectAudit = function(audit){
                                scope.selectedAudit = audit;
                            };


                            scope.$on('$destroy', function () {
                                if (scope.unWatchVisible)
                                    scope.unWatchVisible();
                            });
                        }
                    }
                }
            }
        }
    ]);
});