define(['app', 'moment'], function (app, moment) {
    /**
     * @ngdoc directive
     * @name app.directive:autogenTableAuditGrid
     * @description Renders the view to show the list of audits
     * @restrict E
     * @param {Object} item Item for which to get the audit data
     * @param {Function} onSelectAudit Callback to execute to notify an item has being selected
     */
    'use strict';

    return app.registerDirective('autogenTableAuditGrid', ['Authentication', '$log', 'cacheService', 'adAuditService', 'coreConfigService', '$rootScope',
        function (Authentication, $log, cacheService, adAuditService, coreConfigService, $rootScope) {
            return {
                restrict: 'E',
                scope:{
                    item: "=",
                    onSelectAudit:"&?"
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/audit/autogen.table.audit.grid.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs, ngModel) {
                            /********************************************************************
                             * Variables
                             ********************************************************************/
                            // Filter information for the grid
                            scope.filter= {
                                idUser: null,
                                date: null
                            };
                            // User reference
                            scope.userReference = null;
                            // Reference field to pass to the inputTableSelectorRemote component
                            scope.referenceField = null;
                            // Pagination information
                            scope.tableData = {
                                tableCurrentPage: 1,
                                tablePageSize: 10
                            };
                            // Order by field
                            scope.tableOrderByField = "created";
                            // Sort order. True sorts ascending, false sorts descending
                            scope.tableReverseSort = false;
                            // Maximum entries to show in the table pager index
                            scope.maxSize = 5;
                            /********************************************************************
                             * End variables section
                             ********************************************************************/
                            /**
                             * Loading the reference information for Users selection
                             */
                            cacheService.loadReferenceData(coreConfigService.adReference.user).then(function () {
                                scope.userReference = cacheService.getReference(coreConfigService.adReference.user);
                                scope.referenceField =  {
                                    idReferenceValue: coreConfigService.adReference.user,
                                    name: "idUser",
                                    idColumn: "",
                                    rtype: scope.userReference.rtype,
                                    display: "name",
                                    required: false,
                                    readonly: false
                                };
                            });


                            var watchCollection = "[tableData.tableCurrentPage, tableData.tablePageSize]";
                            scope.unwatch = scope.$watchCollection(watchCollection, function (oldData, newData) {
                                if (JSON.stringify(oldData) !== JSON.stringify(newData))
                                    scope.getAudits();
                            });

                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            /**
                             * Notifies the selection of the item
                             * @param item Item being selected
                             */
                            scope.checkItem = function(item){
                                if (scope.onSelectAudit){
                                    scope.onSelectAudit({
                                        audit: item
                                    });
                                }
                            };

                            /**
                             * Obtains the user filter
                             * @param idColumnData
                             * @param fieldValueData User ID
                             * @param fieldDisplayData
                             */
                            scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                                scope.filter.idUser = fieldValueData;
                                scope.getAudits();
                            };

                            /**
                             * Gets the desired audits
                             */
                            scope.getAudits = function(){
                                var dateFormat = 'DD/MM/YYYY';
                                var q = "idRecord="+scope.item.id;
                                if (scope.idTable)
                                    q += (",idTable="+scope.idTable);
                                if (scope.filter.idUser)
                                    q+=(",idUser="+scope.filter.idUser);
                                if (scope.filter.date) {
                                    var date = null;
                                    if (moment.isMoment(scope.filter.date)) {
                                        date = scope.filter.date.format(dateFormat);
                                    } else if (_.isDate(scope.filter.date)) {
                                        date = moment(scope.filter.date).format(dateFormat);
                                    } else {
                                        date = scope.filter.date;
                                    }
                                    q+= (',created=' + date + "~" + date + ',');
                                }

                                adAuditService.query({
                                    idClient: cacheService.loggedIdClient(),
                                    q: q,
                                    page: scope.tableData.tableCurrentPage,
                                    limit: scope.tableData.tablePageSize,
                                    sort: scope.sortData
                                }, function(data){
                                    scope.audits = data.content;
                                    scope.tableTotalElements = data.totalElements;
                                    scope.onSelectAudit({
                                        audit: null
                                    });

                                    // Values replacement
                                    cacheService.loadTable("ad_audit").then(function(table){
                                        scope.adAuditTable = table;
                                        return cacheService.loadTempTabledir(table, false, undefined, scope);
                                    }).then(function(){
                                        return cacheService.loadTable("ad_user");
                                    }).then(function(table){
                                        scope.adUserTable = table;
                                        return cacheService.loadTempTabledir(table, false, undefined, scope);
                                    })
                                    .then(function () {
                                        _.each(scope.adAuditTable.columns, function (column) {
                                            var item = scope.audits[0];
                                            if (column.reference.rtype === 'YESNO') {
                                                // _.each(scope.tableElements, function (elem) {
                                                item[column.standardName] = item[column.standardName] ? $rootScope.getMessage('AD_labelYes') : $rootScope.getMessage('AD_labelNo');
                                                // });
                                            } else if (column.reference.rtype === 'LIST') {
                                                // _.each(scope.tableElements, function (elem) {
                                                item[column.standardName] = cacheService.getListValue(column.idReferenceValue, item[column.standardName]);
                                                // });
                                            } else if (column.reference.rtype === 'TABLE' || column.reference.rtype == 'TABLEDIR' || column.reference.rtype == 'SEARCH') {
                                                // _.each(scope.tableElements, function (elem) {
                                                item[column.standardName] = cacheService.getTableValue(column, item);
                                                // });
                                            } else if (column.reference.rtype === 'IMAGE') {
                                                // _.each(scope.tableElements, function (elem) {
                                                item[column.standardName + '_imgSrc'] = scope.stdPref.serverCacheUrl + item['updated'] + '/files/' + table.name + '/' + item.id + '/' + item[column.standardName];
                                                // });
                                            }
                                        });
                                    }).catch(function(error){
                                        $log.error(error);
                                    });
                                }, function(error){
                                    $log.error(error);
                                })
                            };

                            /**
                             * Sets the sort parameter
                             * @param field Sort field name
                             */
                            scope.orderByFieldFn = function(field){
                                if (field === scope.tableOrderByField){
                                    scope.tableReverseSort = !scope.tableReverseSort;
                                }
                                else{
                                    scope.tableReverseSort = false;
                                }
                                scope.tableOrderByField = field;
                                scope.sortData = '[{ "property":"' + scope.tableOrderByField  + '","direction":"' + (scope.tableReverseSort ? 'DESC' : 'ASC') + '" }]';
                                scope.getAudits();
                            };

                            /**
                             * Gets the information when the user name of the audit was retrieved from the backend
                             * @param idColumnData Audit ID
                             * @param fieldValueData User ID
                             * @param fieldDisplayData User Name
                             */
                            scope.selectElementGrid = function(idColumnData, fieldValueData, fieldDisplayData){
                                _.each(scope.audits, function(current){
                                    if (current.idAudit === idColumnData){
                                        current.userName = fieldDisplayData;
                                    }
                                })
                            };

                            /**
                             * Gets the required information to display the referenced user name
                             * @param currentColumn User ID column
                             * @param item Audit item being analyzed
                             * @returns {*}
                             */
                            scope.getFieldAttr = function (currentColumn, item) {
                                var column = _.find(scope.adAuditTable.columns, function(current){
                                    return current.standardName === "idUser";
                                });
                                if (column) {
                                    var field = {
                                        idReferenceValue: column.idReferenceValue,
                                        name: column.name,
                                        idColumn: item.idAudit,
                                        rtype: column.reference.rtype,
                                        readonly: true,
                                        required: false
                                    };
                                    return field;
                                }
                                return null;
                            };

                            scope.orderByFieldFn(scope.tableOrderByField);

                            scope.$on('$destroy', function () {
                                if (scope.unwatch)
                                    scope.unwatch();
                            });

                        }
                    }
                }
            }
        }])
});