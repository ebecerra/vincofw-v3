define(['app', 'moment'], function (app, moment) {
    /**
     * @ngdoc directive
     * @name app.directive:autogenAuditContent
     * @description Generates the content of the Audit information for an item
     * @restrict E
     * @param {object} item Item to get the Audit information from
     * @param {string} idTab Tab ID of the item being edited, from which to obtain the related table information
     * @param {string} idTable Table ID of the item being analyzed
     * @param {string} customTemplateUrl Custom template to render, instead of the default one
     */
    'use strict';

    return app.registerDirective('autogenAuditContent', ['Authentication', '$log', 'cacheService', 'autogenService',
        '$http', '$templateCache', '$compile',
        function (Authentication, $log, cacheService, autogenService,$http, $templateCache, $compile) {
            return {
                restrict: 'E',
                scope:{
                    item: "=",
                    idTab: "=?",
                    idTable: "=?",
                    customTemplateUrl: "="
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/audit/autogen.audit.content.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs, ngModel) {
                            if (scope.customTemplateUrl) {
                                $http.get(scope.customTemplateUrl, {cache: $templateCache}).success(function (tplContent) {
                                    element.replaceWith($compile(tplContent)(scope));
                                });
                            }

                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            scope.audits = null;
                            scope.$watch('item', function(){
                                if (scope.item){
                                    scope.loadAudits();
                                }
                            });
                            scope.loadAudits = function(){
                                if (!scope.idTable) {
                                    cacheService.loadTab(scope.idTab).then(function (data) {
                                        scope.idTable = data.idTable;
                                        scope.processAudit();
                                    }).catch(function (error) {
                                        $log.error(error);
                                    });
                                }
                                else {
                                    scope.processAudit();
                                }
                            };

                            scope.processAudit = function () {
                                if (scope.item.id) {
                                    var table = cacheService.getTable(scope.idTable);
                                    var service = autogenService.getAutogenService(table);
                                    service.rowAudit({
                                        idClient: cacheService.loggedIdClient(),
                                        id: scope.item.id
                                    }, function(data){
                                        scope.audits = data.properties;
                                    }, function(error){
                                        $log.error(error);
                                    });
                                }
                            };

                            scope.$on(app.eventDefinitions.GLOBAL_ITEM_CREATED, function(event, data){
                                if (data.idTab === scope.idTab){
                                    scope.loadAudits();
                                }
                            });

                            scope.$on(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, function(event, data){
                                if (data.idTab === scope.idTab && data.operation === "UPDATE"){
                                    scope.loadAudits();
                                }
                            })
                        }
                    }
                }
            }
        }])
});