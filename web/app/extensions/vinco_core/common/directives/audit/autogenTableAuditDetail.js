define(['app', 'lodash'], function (app, lodash) {
    /**
     * @ngdoc directive
     * @name app.directive:autogenTableAuditDetail
     * @description Renders the view to show details of the Audit
     * @restrict E
     * @param {Object} item Audit details
     * @param {string} idTable Table ID of the item being analyzed
     */
    'use strict';

    return app.registerDirective('autogenTableAuditDetail', ['Authentication', '$log', 'cacheService', '$rootScope',
        function (Authentication, $log, cacheService, $rootScope) {
            return {
                restrict: 'E',
                scope:{
                    item: "=",
                    idTable:"="
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/audit/autogen.table.audit.detail.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs, ngModel) {
                            /**
                             * Variables
                             */
                            scope.table = cacheService.getTable(scope.idTable);
                            /**
                             * End Variables
                             */

                            /**
                             * Gets the old value associated with the column edited
                             * @param columnName Column name
                             * @returns {*} Old value associated with the column edited
                             */
                            scope.getOldValue = function(columnName){
                                return scope.oldValue[columnName];
                            };


                            /**
                             * Gets the new value associated with the column edited
                             * @param columnName Column name
                             * @returns {*} New value associated with the column edited
                             */
                            scope.getNewValue = function(columnName){
                                return scope.newValue[columnName];
                            };

                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            scope.unwatch = scope.$watch("item", function(){
                                if (scope.item){
                                    scope.oldValue = JSON.parse(scope.item.oldValue);
                                    scope.newValue = JSON.parse(scope.item.newValue);
                                    scope.keys = _.merge(scope.keys,  _.keys(scope.newValue));
                                    scope.modifiedColumns = scope.table.columns;
                                    scope.modifiedColumns = _.filter(scope.table.columns, function(column){
                                        var found = _.find(scope.keys, function(key){
                                            return column.standardName === key;
                                        });
                                        return (found && scope.oldValue[column.standardName] !== scope.newValue[column.standardName]);
                                    });
                                }
                                else {
                                    scope.oldValue = null;
                                    scope.newValue = null;
                                    scope.keys = [];
                                    scope.columns = [];
                                }
                            });

                            scope.$on('$destroy', function () {
                                if (scope.unwatch)
                                    scope.unwatch();
                            });
                        }
                    }
                }
            }
        }])
});