/**
 * This directive defines the visualization of the report execution
 * The directive receives the following atributes:
 *  *   item-attr           The item that contains the report
 *  *   process-attr        The report data
 *  *   cancel-fn-attr      The function to call when cancel button is clicked
 *  *   show-cancel         Attribute to define if the cancel button will be shown
 *
 *  Example usage
 *    <process-notification item-attr='{{item}}' process-attr='{{dialogProcess}}' cancel-fn-attr='hideProcessDialog()'></process-notification>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('reportNotification', function (
        $rootScope, $log, $sce, Authentication, commonService, hookService, processSocketService,
        utilsService, adProcessParamService, adProcessService, adReferenceService, processParamManagerService, cacheService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                itemAttrData: '@itemAttr',
                processAttrData: '@processAttr',
                cancelFunction: '&cancelFnAttr'
            },
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/process.notification.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        if (angular.isDefined($rootScope.getProcessParam)) {
                            scope.getProcessParam = $rootScope.getProcessParam;
                        } else {
                            $rootScope.$on(app.eventDefinitions.TRANSLATIONS_READY, function () {
                                scope.getProcessParam = $rootScope.getProcessParam;
                            });
                        }
                        scope.showCancel = false;
                        scope.showCancel = !(attributes.showCancel == "false" || attributes.showCancel == false)
                        element.removeAttrs("show-cancel");
                        scope.hasRole = $rootScope.hasRole;
                        scope.hasPermission = $rootScope.hasPermission;
                        scope.$watchCollection("processAttrData", function () {
                            scope.doProcess();
                        });

                        scope.yesNoValues = [
                            { value: null, name: '' },
                            { value: true, name: $rootScope.getMessage('AD_labelYes') },
                            { value: false, name: $rootScope.getMessage('AD_labelNo') }
                        ];

                        /**
                         * Starts websocket listener
                         */
                        scope.startListener = function (callback) {
                            processSocketService.initialize(scope.process.idProcess, scope.messageReceived).then(function () {
                                $log.log("WebSocket connected.");
                                if (callback)
                                    callback();
                            });
                        };

                        /**
                         * Receives websocket message
                         *
                         * @param event Datos del mensaje
                         */
                        scope.messageReceived = function (event) {
                            if (scope.process.idProcess != event.idProcess)
                                return;
                            if (!event.finish) {
                                if (event.adProcessLog) {
                                    event.adProcessLog.date = new moment(event.adProcessLog.created);
                                    scope.processLogs.push(event.adProcessLog);
                                }
                                if (event.outParamName) {
                                    var param = _.find(scope.processOutputParams, function (current) {
                                        return current.name == event.outParamName;
                                    });
                                    if (param)
                                        param.value = event.outParamValue;
                                }
                            } else {
                                if (event.success) {
                                    commonService.showSuccesNotification($rootScope.getMessage('AD_ProcessFinished'));
                                } else {
                                    commonService.showRejectNotification($rootScope.getMessage('AD_ProcessFinishedError'));
                                }
                            }
                        };

                        /**
                         * Clears all the logs
                         */
                        scope.clearLogs = function () {
                            scope.processLogs = [];
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param param Param to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getFieldAttr = function (param) {
                            return {
                                idReferenceValue: param.idReferenceValue,
                                name: param.name,
                                idColumn: param.idProcessParam,
                                rtype: param.reference.rtype,
                                display: true,
                                required: param.mandatory
                            };
                        };

                        /**
                         * Executes the selected process
                         *
                         * @param item Item being edited
                         * @param process Process being handled
                         */
                        scope.loadProcessData = function (item, process) {
                            adProcessParamService.query({
                                idClient: process.idClient,
                                q: "idProcess=" + process.idProcess
                            }, function (data) {
                                scope.selectedProcessParams = "";
                                var paramsError = false;
                                var parameters = _.sortBy(data.content, function (current) {
                                    return current.seqno;
                                });
                                _.each(parameters, function (current) {
                                    if (current.ptype == "IN") {
                                        adReferenceService.filter(
                                            {idClient: process.idClient},
                                            {idReference: current.idReference}
                                        ).then(function (data) {
                                            if (data.length > 0)
                                                current.reference = data[0];
                                            if (current.reference.rtype == 'TABLEDIR' || current.reference.rtype == 'TABLE' || current.reference.rtype == 'SEARCH') {
                                                cacheService.loadReference(current.idReferenceValue, current.reference.rtype)
                                            }
                                        });

                                        current.runtime = {
                                            displaylogic: current.displaylogic
                                        };
                                        if (current.valuedefault) {
                                            if (_.keys(scope.item).length !=0) {
                                                if (_.has(item, current.valuedefault)) {
                                                    current.value = item[current.valuedefault];
                                                } else {
                                                    $log.error("Can't build process parameter: " + current.name);
                                                    commonService.showRejectNotification($rootScope.getMessage('AD_ProcessMsgOperationError', [current.name]));
                                                    paramsError = true;
                                                }
                                            }
                                        }
                                        _.each(scope.processInputParams, function (param) {
                                            if (param.name != current.name) {
                                                scope.notifyFieldChange(param, current);
                                            }
                                        });

                                        var stored = processParamManagerService.loadParameter(current);
                                        if (stored != null && !current.ranged)
                                            current.value = stored;
                                        scope.processInputParams.push(current);
                                        if (current.ranged) {
                                            scope.rangedInput[current.name] = {
                                                start: null,
                                                end: null
                                            };
                                            if (stored != null) {
                                                scope.rangedInput[current.name].start = stored.start;
                                                scope.rangedInput[current.name].end = stored.end;
                                            }
                                        }
                                    } else {
                                        scope.processOutputParams.push(current);
                                    }
                                });

                                _.each(scope.processInputParams, function (current) {
                                    _.each(scope.processInputParams, function (param) {
                                        if (param.name != current.name) {
                                            scope.notifyFieldChange(param, current);
                                        }
                                    });
                                });
                            }, function (err) {
                                $log.error(err);
                            });
                        };

                        /**
                         * Gets the data needed for the input of the LIST search component
                         *
                         * @param param Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getListFieldAttr = function (param) {
                            return utilsService.getListFieldAttr(param);
                        };

                        scope.updateFieldChange = function (field, start) {
                            _.each(scope.processInputParams, function (param) {
                                if (param.name != field.name) {
                                    scope.notifyFieldChange(param, field);
                                }
                            });
                            scope.persistProcessParam(field, start);
                        };

                        scope.notifyFieldChange = function (field, changedField) {
                            utilsService.applyParamDisplayLogic(changedField, field);
                            utilsService.updateParamSqlWhere(changedField, field);
                        };

                        scope.selectElement = function (idProcessParam, fieldValueData, fieldDisplayData) {
                            var field = _.find(scope.processInputParams, function (current) {
                                return current.idProcessParam == idProcessParam
                            });
                            if (field) {
                                field.value = fieldValueData;
                                scope.updateFieldChange(field, true);
                            }
                        };

                        /**
                         * Persists the parameter value
                         * @param parameter
                         * @param start
                         */
                        scope.persistProcessParam = function (parameter, start) {
                            var paramToSave = _.clone(parameter);
                            if (!parameter.ranged) {
                                processParamManagerService.storeParameter(paramToSave);
                            } else {
                                if (start) {
                                    paramToSave.value = scope.rangedInput[parameter.name].start;
                                } else {
                                    paramToSave.value = scope.rangedInput[parameter.name].end;
                                }
                                processParamManagerService.storeParameter(paramToSave, start);
                            }
                        };

                        /**
                         * Show confirmation dialog
                         *
                         * @param callback Called when is OK
                         */
                        scope.doConfirmation = function (callback) {
                            if (scope.process.showConfirm) {
                                commonService.showConfirm(scope.process.confirmMsg, scope, function () {
                                    callback();
                                });
                            } else {
                                callback();
                            }
                        };

                        /**
                         * Executes the selected process
                         *
                         * @param item Item being edited
                         * @param form Form containing the input parameters
                         * @param process Associated process
                         * @param format Export format
                         */
                        scope.startProcess = function (item, form, process, format) {
                            scope.preview = null;
                            scope.clearLogs();
                            scope.processLogs.push({
                                log: $rootScope.getMessage('AD_ProcessStarting'),
                                date: new moment(),
                                ltype: "SUCCESS"
                            });
                            scope.globalParameters = {};
                            hookService.execHooks('AD_GetGlobalParameters', { item: scope.globalParameters }).then(function () {
                                scope.assembleParams(item, format);
                                form.$dirty = true;
                                if (form.$valid) {
                                    scope.doConfirmation(function () {
                                        scope.startListener(function () {
                                            adProcessService.exec({
                                                id: scope.process.idProcess,
                                                idLanguage: Authentication.getCurrentLanguage(),
                                                idUserLogged: Authentication.getLoginData().idUserLogged,
                                                idClient: cacheService.loggedIdClient(),
                                                params: scope.selectedProcessParam
                                            }, function (data) {
                                                var arrayBuffer2String = function (buf, callback) {
                                                    var f = new FileReader();
                                                    f.onload = function (e) {
                                                        callback(e.target.result)
                                                    };
                                                    var blob = new Blob([data.result], {type: "octet/stream"});
                                                    f.readAsText(blob);
                                                };
                                                arrayBuffer2String(data.result,
                                                    function (string) {
                                                        var parsed = null;
                                                        try {
                                                            parsed = JSON.parse(string);
                                                        } catch (e) {
                                                        }
                                                        if (!parsed || parsed.success !== false) {
                                                            var blob = new Blob([data.result], {type: "octet/stream"});
                                                            var link = document.createElement('a');
                                                            link.href = window.URL.createObjectURL(blob);
                                                            link.download = process.name + "_" + new moment().format("DDMMYYYY_hhmmss") + "." + format;
                                                            link.click();
                                                        }
                                                    }
                                                );
                                            }, function (err) {
                                                $log.error(err);
                                            });
                                        });
                                    });
                                }
                            });
                        };

                        /**
                         * Executes the selected process
                         *
                         * @param item Item being edited
                         * @param form Form containing the input parameters
                         */
                        scope.previsualize = function (item, form, process, format) {
                            form.$dirty = true;
                            if (form.$valid) {
                                scope.doConfirmation(function () {
                                    scope.preview = null;
                                    scope.clearLogs();
                                    scope.processLogs.push({
                                        log: $rootScope.getMessage('AD_ProcessStarting'),
                                        date: new moment(),
                                        ltype: "SUCCESS"
                                    });
                                    scope.startListener(function () {
                                        scope.globalParameters = {};
                                        hookService.execHooks('AD_GetGlobalParameters', { item: scope.globalParameters }).then(function () {
                                            scope.assembleParams(item, 'html');
                                            adProcessService.exec({
                                                id: scope.process.idProcess,
                                                idClient: cacheService.loggedIdClient(),
                                                idUserLogged: Authentication.getLoginData().idUserLogged,
                                                idLanguage: Authentication.getCurrentLanguage(),
                                                params: scope.selectedProcessParam
                                            }, function (data) {
                                                var arrayBuffer2String = function (buf, callback) {
                                                    var f = new FileReader();
                                                    f.onload = function (e) {
                                                        callback(e.target.result)
                                                    };
                                                    var blob = new Blob([data.result], {type: "octet/stream"});
                                                    f.readAsText(blob);
                                                };
                                                arrayBuffer2String(data.result,
                                                    function (string) {
                                                        var parsed = null;
                                                        try {
                                                            parsed = JSON.parse(string);
                                                        }
                                                        catch (e) {
                                                        }
                                                        if (!parsed || parsed.success !== false) {
                                                            scope.preview = $sce.trustAsHtml(string);
                                                        }
                                                    }
                                                );
                                            }, function (err) {
                                                $log.error(err);
                                            });
                                        });
                                    });
                                });
                            }
                        };

                        /**
                         * Assembles the parameters required to generate the report
                         *
                         * @param item Associated item
                         */
                        scope.assembleParams = function (item, format) {
                            scope.selectedProcessParam = "";
                            _.each(scope.processOutputParams, function (current) {
                                current.value = null;
                            });
                            _.each(scope.processInputParams, function (current) {
                                if ((!current.ranged)) {
                                    if (current.valuedefault) {
                                        var resultDefault = processParamManagerService.getParameterDefaultValue(item, null, scope.globalParameters, current.valuedefault);
                                        if (resultDefault.success) {
                                            current.value = resultDefault.defaultValue;
                                        } else {
                                            $log.error("Can't build process parameter: " + current.name);
                                            commonService.showRejectNotification($rootScope.getMessage('AD_ProcessMsgOperationError', [current.name]));
                                        }
                                    }
                                    switch (current.reference.rtype) {
                                        case 'DATE':
                                            if (current.value) {
                                                if (moment.isMoment(current.value)) {
                                                    scope.selectedProcessParam += ("," + current.name + "=" + current.value.format('DD/MM/YYYY'));
                                                } else {
                                                    scope.selectedProcessParam += ("," + current.name + "=" + current.value);
                                                }
                                            } else if (current.value == null && (item[current.valuedefault] != undefined)) {
                                                scope.selectedProcessParam += ("," + current.name + "=" + item[current.valuedefault]);
                                            }
                                            break;
                                        default:
                                            if (current.value) {
                                                scope.selectedProcessParam += ("," + current.name + "=" + current.value);
                                            } else if (current.value == null && (item[current.valuedefault] != undefined)) {
                                                scope.selectedProcessParam += ("," + current.name + "=" + item[current.valuedefault]);
                                            }
                                            break;
                                    }
                                } else if (current.ranged) {
                                    var startStr = '';
                                    var endStr = '';
                                    switch (current.reference.rtype) {
                                        case 'DATE':
                                            if (moment.isMoment(scope.rangedInput[current.name].start)) {
                                                startStr = scope.rangedInput[current.name].start.format('DD/MM/YYYY');
                                            } else {
                                                startStr = scope.rangedInput[current.name].start;
                                            }
                                            if (moment.isMoment(scope.rangedInput[current.name].end)) {
                                                endStr = scope.rangedInput[current.name].end.format('DD/MM/YYYY');
                                            } else {
                                                endStr = scope.rangedInput[current.name].end;
                                            }
                                            if (startStr === undefined)
                                                startStr = moment(new Date()).format('DD/MM/YYYY');
                                            if (endStr === undefined)
                                                endStr = moment(new Date()).format('DD/MM/YYYY');
                                            break;
                                        default:
                                            startStr = scope.rangedInput[current.name].start;
                                            endStr = scope.rangedInput[current.name].end;
                                            if (startStr === undefined)
                                                startStr = '';
                                            if (endStr === undefined)
                                                endStr = '';
                                            break;
                                    }
                                    var rangedParam = vsprintf("%1$sFrom=%2$s,%1$sTo=%3$s", [current.name, startStr, endStr]);
                                    scope.selectedProcessParam += ("," + rangedParam );
                                }
                            });
                            scope.selectedProcessParam += (",outputType=" + format);
                        };

                        /**
                         * Perfoms the final wiring from the data received
                         */
                        scope.doProcess = function () {
                            scope.item = JSON.parse(scope.itemAttrData);
                            scope.process = JSON.parse(scope.processAttrData);
                            scope.clearLogs();
                            scope.processOutputParams = [];
                            scope.processInputParams = [];
                            scope.rangedInput = [];
                            scope.getMessage = $rootScope.getMessage;

                            if (scope.item && scope.process) {
                                scope.loadProcessData(scope.item, scope.process);
                            }
                        };
                    }
                }
            }
        }
    });
});
