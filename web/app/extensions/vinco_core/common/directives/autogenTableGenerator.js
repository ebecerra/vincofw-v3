/**
 * This directive generates a grid corresponding to the data defined in the given tab
 * It receives the following parameters in the form of attributes
 *
 *      tab-generator-id        ->  The idTab key
 *      table-generator-title   ->  The text to show in the title of the table
 *      table-generator-icon    ->  The icon class to use in the table header
 *      table-reference-item    ->  Parent item associated to the grid (the items of the grid by this item's reference id)
 *      hide-header             ->  Hides the header caption and icon
 *      table-filter            ->  Linked column filter value
 *
 * The following is an example usage of the directive
 *
 *  <autogen-table-generator tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018"
 *      table-generator-title="window.title"
 *      table-generator-icon = "fa-list-alt"></autogen-table-generator>
 */
define(['app', 'lodash'], function (app, _) {

    'use strict';

    return app.registerDirective('autogenTableGenerator', ['$rootScope', '$log', '$timeout', '$state', 'cacheService', 'hookService', 'commonService', 'coreConfigService', 'autogenService', 'tabManagerService', 'utilsService',
        function ($rootScope, $log, $timeout, $state, cacheService, hookService, commonService, coreConfigService, autogenService, tabManagerService, utilsService) {
            return {
                restrict: 'E',
                require: '?ngModel',
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.table.generator.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            angular.element(element).addClass('active in');

                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            scope.filterTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.table.generator.filter.html?v=' + FW_VERSION;
                            scope.headerFilterTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.table.generator.header.filter.html?v=' + FW_VERSION;
                            scope.editingWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.editing.window.html?v=' + FW_VERSION;
                            scope.processDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.dialog.html?v=' + FW_VERSION;
                            scope.tableGeneratorTitle = attributes.tableGeneratorTitle;
                            scope.tableGeneratorIcon = attributes.tableGeneratorIcon;
                            scope.tabGeneratorId = attributes.tabGeneratorId;
                            scope.hideHeader = attributes.hideHeader !== undefined;
                            scope.tableFilter = attributes.tableFilter;
                            scope.processDialogId = 'processModal' + scope.tabGeneratorId;
                            scope.tableReferenceItem = null;
                            scope.tableSearchDialogId = 'tableModal' + scope.tabGeneratorId;
                            scope.windowGeneratorId = attributes.windowGeneratorId;
                            scope.sortMode = false;
                            scope.showCardMode = false;
                            scope.gridEnabled = true;
                            /**
                             * Defines if the sortable grid should be in listing mode or sortable mode. Listing mode prevents the sort actions
                             * @type {boolean}
                             */
                            scope.cardMode = false;
                            scope.yesNoValues = [
                                {value: null, name: ''},
                                {value: true, name: $rootScope.getMessage('AD_labelYes')},
                                {value: false, name: $rootScope.getMessage('AD_labelNo')}
                            ];
                            if (attributes.tableReferenceItem) {
                                scope.tableReferenceItem = JSON.parse(attributes.tableReferenceItem);
                            }
                            if (attributes.tabbedCharts) {
                                scope.tabbedCharts = JSON.parse(attributes.tabbedCharts);
                            } else {
                                scope.tabbedCharts = [];
                            }
                            scope.linkFilters = {};
                            // Sort order. False equals ASC, True equals DESC
                            scope.tableReverseSort = false;
                            scope.tableFilterReverseSort = false;
                            // Order by field
                            scope.tableOrderByField = '';
                            // Page options
                            scope.tablePageSizeList = [10, 20, 40, 60, 100];
                            scope.gridViewSortStepList = [1, 5, 10, 50, 100, 500, 1000];
                            scope.tableData = {
                                tableCurrentPage: 1,
                                tablePageSize: 20,
                                sortStep: 1,
                                sortStart: 10,
                                checkAll: false
                            };
                            // Max amount of selectable pages in paginator. More than this will be displayed as ...
                            scope.maxSize = 5;
                            scope.filterInfo = {
                                filters: {},
                                filtersRange: {},
                                filtersList: {},
                                filtersReference: []
                            };
                            scope.valueFilterTableDir = '';
                            //For filter type TABLE
                            scope.complicateVisualFields = {};
                            scope.referenceData = {
                                referencePageSize: 10,
                                referenceCurrentPage: 1,
                                currentFilterValue: ""
                            };
                            scope.tableFilterValue = '';

                            /**
                             * Reloads the grid state
                             */
                            scope.getPersistData = function () {
                                scope.tabData = tabManagerService.getTabContent(scope.tabData, scope.tabGeneratorId);
                                if (scope.tabData && scope.tabData.filters && scope.tabData.pagesize && scope.tabData.idTab == scope.tabGeneratorId) {
                                    scope.showCardMode = scope.tabData.showCardMode;
                                    //Checking if new search keys were added after the last time the cache was stored
                                    var cachedKeys = _.keys(scope.tabData.filters.filtersRange);
                                    var newKeys = _.filter(_.keys(scope.filterInfo.filtersRange), function (current) {
                                        return !_.find(cachedKeys, function (key) {
                                            return key === current;
                                        });
                                    });
                                    _.each(newKeys, function (key) {
                                        scope.tabData.filters.filtersRange[key] = scope.filterInfo.filtersRange[key];
                                    });

                                    scope.filterInfo.filtersRange = scope.tabData.filters.filtersRange;
                                    scope.filterInfo.filters = scope.tabData.filters.filters;
                                    scope.filterInfo.filtersReference = scope.tabData.filters.filtersReference;
                                    scope.tableData.tablePageSize = scope.tabData.pagesize;
                                    scope.tableData.tableCurrentPage = scope.tabData.currentPage;
                                    scope.tableReverseSort = scope.tabData.tableReverseSort;
                                    var sortColumn = _.find(scope.gridColumns, function (current) {
                                        return current.standardName === scope.tabData.tableOrderByField && current.sortable;
                                    });
                                    if (sortColumn) {
                                        scope.tableOrderByField = scope.tabData.tableOrderByField;
                                    } else {
                                        var firstSortable = _.find(scope.gridColumns, function (current) {
                                            return current.sortable;
                                        });
                                        if (firstSortable) {
                                            scope.tableOrderByField = firstSortable.name;
                                        }
                                    }
                                    scope.filtersRange = scope.tabData.filters.filtersRange;
                                }
                            };

                            scope.$on(coreConfigService.events.NAVIGATION_REFRESH,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.reloadTableData();
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_REFRESH_COLUMNS,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        if (data.reloadColumns) {
                                            scope.gridEnabled = false;
                                            cacheService.loadTab(scope.tabGeneratorId, true).then(function () {
                                                $timeout(function () {
                                                    scope.gridEnabled = true;
                                                }, 0);
                                            });
                                        }
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_CHANGE_VIEW_MODE,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.showCardMode = !scope.showCardMode;
                                        scope.persistData();
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_UPDATE_CHECK_ALL,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.tableData.checkAll = data.checkAll;
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_GRID_SORT,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        if (scope.tableOrderByField !== data.column.standardName) {
                                            scope.tableReverseSort = false;
                                        } else {
                                            scope.tableReverseSort = !scope.tableReverseSort;
                                        }
                                        scope.tableOrderByField = data.column.standardName;
                                        scope.persistData();
                                        scope.reloadTableData();
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_APPLY_SORT,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.showSorting()
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_CANCEL_SORT,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.showSorting()
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_EDIT_ITEM,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        scope.persistData();
                                        // Defining which tab should be used for editing visualization
                                        if (scope.listViewIdTab) {
                                            data.item.idTab = scope.listViewIdTab;
                                        }
                                        else if (scope.cardViewIdTab) {
                                            data.item.idTab = scope.cardViewIdTab;
                                        } else {
                                            data.item.idTab = scope.tabGeneratorId;
                                        }
                                        if (scope.tabInformation.tablevel && scope.tabInformation.tablevel > 0) {

                                            cacheService.storeTabItems(scope.tabGeneratorId, scope.tableElements);
                                            $rootScope.$broadcast(app.eventDefinitions.SUB_ITEM_EDIT, {
                                                item: data.item,
                                                allItems: scope.tableElements,
                                                linkData: scope.linkFilters
                                            });
                                        } else {
                                            cacheService.storeTabItems(scope.tabGeneratorId, scope.tableElements);
                                            cacheService.clearFormInfo(scope.tabGeneratorId);
                                            $rootScope.$broadcast(app.eventDefinitions.GLOBAL_ITEM_EDIT, {
                                                item: data.item,
                                                linkData: scope.linkFilters,
                                                filterInfo: scope.filterInfo
                                            });
                                        }
                                    }
                                }
                            );

                            scope.$on(coreConfigService.events.NAVIGATION_DELETE_GRID_ITEM,
                                function (event, data) {
                                    if (data.idTab === scope.tabGeneratorId) {
                                        $.SmartMessageBox({
                                            title: $rootScope.getMessage('AD_msgConfirmDelete'),
                                            buttons: '[' + $rootScope.getMessage('AD_btnDelete') + '][' + $rootScope.getMessage('AD_btnCancel') + ']'
                                        }, function (ButtonPressed) {
                                            setTimeout(function () {
                                                if (ButtonPressed === $rootScope.getMessage('AD_btnDelete')) {

                                                    var service = scope.getTableService();
                                                    var itemToDelete = {
                                                        id: data.rowItem.id,
                                                        idClient: data.rowItem.idClient
                                                    };
                                                    _.each(scope.tableInfo.columns, function (column) {
                                                        if (column.primaryKey) {
                                                            itemToDelete[column.standardName] = data.rowItem[column.standardName];
                                                        }
                                                    });
                                                    service.delete(
                                                        itemToDelete,
                                                        function (data) {
                                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataDelete'));
                                                            scope.reloadTableData();
                                                        }, function (err) {
                                                            $log.error('Status:' + err.status + ', Error: ' + err.statusText);
                                                            $rootScope.showValidationsErrors(err, scope.tabInformation.fields);
                                                        }
                                                    );
                                                }
                                            }, 10);
                                        });
                                        // The following lines are needed to prevent the dialog from
                                        // being shown again by pressing Enter
                                        var $focused = $(':focus');
                                        $focused.blur();
                                    }
                                }
                            );

                            scope.$on(app.eventDefinitions.GLOBAL_PROCESS_FINISHED,
                                function (event, data) {
                                    if (data.windowGeneratorId && data.windowGeneratorId == scope.tabInformation.idWindow && (data.tablevel == scope.tabInformation.tablevel || data.tablevel + 1 == scope.tabInformation.tablevel)) {
                                        scope.reloadTableData();
                                    }
                                }
                            );

                            scope.getTableValue = function (colName, item) {
                                var col = _.find(scope.tableInfo.columns, function (c) {
                                    return c.name == colName;
                                });
                                return col ? cacheService.getTableValue(col, item) : item[colName];
                            };

                            scope.getFieldType = function (column) {
                                if (column.reference) {
                                    return column.reference.rtype;
                                }
                                return column.ctype;
                            };

                            /**
                             * Persists the grid state in memory to be retrieved when returning from child editing form
                             */
                            scope.persistData = function () {
                                var filters = {};
                                var filterKeys = _.keys(scope.filterInfo.filters);
                                _.each(filterKeys, function (key) {
                                    if (scope.filterInfo.filters[key] && typeof scope.filterInfo.filters[key] === 'object' && scope.filterInfo.filters[key].isValid && scope.filterInfo.filters[key].isValid())
                                        filters[key] = scope.filterInfo.filters[key].format('D/M/YYYY');
                                    else
                                        filters[key] = scope.filterInfo.filters[key];
                                });
                                var filtersData = {
                                    filters: {
                                        filtersRange: scope.filterInfo.filtersRange,
                                        filters: filters,
                                        filtersReference: scope.filterInfo.filtersReference
                                    },
                                    pagesize: scope.tableData.tablePageSize,
                                    idTab: scope.tabGeneratorId,
                                    idWindow: scope.windowGeneratorId,
                                    currentPage: scope.tableData.tableCurrentPage,
                                    tableReverseSort: scope.tableReverseSort,
                                    tableOrderByField: scope.tableOrderByField,
                                    showCardMode: scope.showCardMode
                                };
                                scope.tabData = tabManagerService.setTabContent(filtersData, scope.tabGeneratorId);
                            };

                            scope.changeFilter = function (filterName, value) {
                                scope.filterInfo.filters[filterName] = value;
                            };

                            scope.throwEditClick = function () {
                                var elementsToEdit = _.filter(scope.tableElements, function (current) {
                                    return current.isCheckedGrid;
                                });

                                if (elementsToEdit.length === 0) {
                                    commonService.showHint($rootScope.getMessage('AD_msgSelectItem'));
                                } else {
                                    var item = elementsToEdit[0];
                                    if (scope.listViewIdTab) {
                                        item.idTab = scope.listViewIdTab;
                                    } else if (scope.cardViewIdTab) {
                                        item.idTab = scope.cardViewIdTab;
                                    } else {
                                        item.idTab = scope.tabGeneratorId;
                                    }
                                    cacheService.storeTabItems(item.idTab, elementsToEdit);
                                    scope.persistData();
                                    if (scope.tabInformation.tablevel && scope.tabInformation.tablevel > 0) {
                                        // Defining which tab should be used for editing visualization

                                        $rootScope.$broadcast(app.eventDefinitions.SUB_ITEM_EDIT, {
                                            item: item,
                                            allItems: elementsToEdit,
                                            linkData: scope.linkFilters
                                        });
                                    } else {
                                        cacheService.clearFormInfo(item.idTab);
                                        $rootScope.$broadcast(app.eventDefinitions.GLOBAL_ITEM_EDIT, {
                                            item: item,
                                            linkData: scope.linkFilters,
                                            allItems: elementsToEdit,
                                            filterInfo: scope.filterInfo
                                        });
                                    }
                                }
                            };

                            scope.throwNewClick = function (item) {
                                scope.persistData();
                                if (scope.tabInformation.tablevel && scope.tabInformation.tablevel > 0) {
                                    $rootScope.$broadcast(app.eventDefinitions.SUB_ITEM_EDIT, {
                                        item: {
                                            id: null,
                                            idTab: scope.tabGeneratorId
                                        }, linkData: scope.linkFilters
                                    });
                                } else {
                                    $rootScope.$broadcast(app.eventDefinitions.GLOBAL_ITEM_EDIT, {
                                        item: {
                                            id: null,
                                            idTab: scope.tabGeneratorId
                                        }, linkData: scope.linkFilters
                                    });
                                }
                            };

                            scope.throwMultipleNewClick = function () {
                                scope.persistData();
                                if (scope.tabInformation.tablevel && scope.tabInformation.tablevel > 0) {
                                    $rootScope.$broadcast(app.eventDefinitions.SUB_ITEM_EDIT, {
                                        item: {
                                            id: null,
                                            idTab: scope.tabGeneratorId
                                        }, linkData: scope.linkFilters,
                                        multiple: true
                                    });
                                } else {
                                    $rootScope.$broadcast(app.eventDefinitions.GLOBAL_ITEM_EDIT, {
                                        item: {
                                            id: null,
                                            idTab: scope.tabGeneratorId
                                        }, linkData: scope.linkFilters,
                                        multiple: true
                                    });
                                }
                            };

                            /**
                             * Deletes all selected rows
                             *
                             */
                            scope.deleteItems = function () {
                                var elementsToDelete = _.filter(scope.tableElements, function (current) {
                                    return current.isCheckedGrid;
                                });

                                if (elementsToDelete.length === 0) {
                                    commonService.showHint($rootScope.getMessage('AD_msgSelectItem'));
                                }
                                else {
                                    $.SmartMessageBox({
                                        title: elementsToDelete.length > 1 ? $rootScope.getMessage('AD_msgConfirmDeleteMultiple', [elementsToDelete.length]) : $rootScope.getMessage('AD_msgConfirmDelete'),
                                        buttons: '[' + $rootScope.getMessage('AD_btnDelete') + '][' + $rootScope.getMessage('AD_btnCancel') + ']'
                                    }, function (ButtonPressed) {
                                        setTimeout(function () {
                                            if (ButtonPressed === $rootScope.getMessage('AD_btnDelete')) {
                                                var ids = _.map(elementsToDelete, 'id');
                                                var service = scope.getTableService();
                                                service.deleteBatch({
                                                    idClient: cacheService.loggedIdClient(),
                                                    ids: ids
                                                }, function (data) {
                                                    scope.reloadTableData();
                                                }, function (err) {
                                                    $log.error('Status:' + err.status + ', Error: ' + err.statusText);
                                                    $rootScope.showValidationsErrors(err, scope.tabInformation.fields);
                                                });
                                            }
                                        }, 10);
                                    });
                                    // The following lines are needed to prevent the dialog from
                                    // being shown again by pressing Enter
                                    var $focused = $(':focus');
                                    $focused.blur();
                                }
                            };

                            /**
                             * Verifies if the edit button should be shown
                             * @param tabData
                             */
                            scope.showEdit = function (tabData) {
                                if (!tabData)
                                    return false;
                                if (_.includes(tabData.uipattern, "EDIT")) {
                                    return true;
                                }
                                return tabData.uipattern === "STANDARD";
                            };

                            /**
                             * Verifies if the delete button should be shown
                             * @param tabData
                             */
                            scope.showDelete = function (tabData) {
                                if (!tabData)
                                    return false;
                                if (tabData.uipattern == "DELETE" || tabData.uipattern == "EDIT_DELETE") {
                                    return true;
                                }
                                return tabData.uipattern == "STANDARD";
                            };

                            /**
                             * Verifies if Readonly button should be shown
                             * @param tabData
                             */
                            scope.showReadonly = function (tabData) {
                                if (!tabData)
                                    return false;
                                return (tabData.uipattern == 'READONLY') || ((scope.showEdit(tabData)) && tabData.runtime.readonly);
                            };

                            /**
                             * Returns the associated table service
                             *
                             * @returns {*} Service
                             */
                            scope.getTableService = function () {
                                var service = null;
                                try {
                                    if (scope.tableInfo) {
                                        service = autogenService.getAutogenService(scope.tableInfo);
                                    }
                                } catch (e) {
                                    $log.error(e);
                                }
                                return service;
                            };

                            scope.$on(coreConfigService.events.NAVIGATION_REFRESH_SORT_VALUES, function (event, data) {
                                scope.tableData.sortStep = data.step;
                                scope.tableData.sortStart = data.start;
                            });

                            /**
                             * Reloads table data
                             */
                            scope.reloadTableData = function () {
                                if (!scope.isTableType())
                                    return;
                                scope.getPersistData();
                                var service = scope.getTableService();
                                if (service) {
                                    var sortQueryRequest = scope.getSortAndQuery();
                                    // Perform data request
                                    $log.info("Requesting data from server for table: " + scope.tableInfo.name + " with query: " + sortQueryRequest.query);
                                    service.query(
                                        {
                                            idClient: cacheService.loggedIdClient(),
                                            q: sortQueryRequest.query,
                                            sort: sortQueryRequest.sort,
                                            page: scope.tableData.tableCurrentPage,
                                            limit: scope.tableData.tablePageSize
                                        },
                                        function (data) {
                                            scope.tableElements = data.content;
                                            scope.tableTotalElements = data.totalElements;
                                        },
                                        function (err) {
                                            $log.error(err);
                                            if (err.status === 500) {
                                                commonService.showError($rootScope.getMessage('AD_ErrLoadPageServerError', [err.config.url]), 50000);
                                            } else {
                                                commonService.showError($rootScope.getMessage('AD_ErrValidationServerError'), 50000);
                                            }
                                        }
                                    );
                                }
                            };

                            /**
                             * Loads the corresponding tab information
                             */
                            scope.loadTabInformation = function () {
                                var table = cacheService.getTable(scope.tabInformation.idTable);
                                if (table) {
                                    scope.tableInfo = table;

                                    _.each(scope.tableInfo.columns, function (currentColumn) {
                                        _.each(scope.tabInformation.fields, function (field) {
                                            if (currentColumn.idColumn === field.column.idColumn)
                                                currentColumn.field = field;
                                        });
                                    });
                                    // Get grid columns
                                    scope.gridColumns = _.filter(scope.tabInformation.fields, function (current) {
                                        return current.showingrid;
                                    });
                                    scope.gridColumns = _.sortBy(scope.gridColumns, function (current) {
                                        return current.gridSeqno;
                                    });
                                    _.each(scope.gridColumns, function (column) {
                                        column.tableName = scope.tableInfo.name;
                                        var col = _.find(scope.tableInfo.columns, function (c) {
                                            return c.name == column.name;
                                        });
                                        if (col) {
                                            column.rtype = col.reference.rtype;
                                        }
                                    });
                                    // Setting the first sortable column as the default sort
                                    var firstSortable = _.find(scope.gridColumns, function (current) {
                                        return current.sortable;
                                    });
                                    if (firstSortable) {
                                        scope.tableOrderByField = firstSortable.standardName;
                                    }
                                    // Get search filters
                                    scope.groupedSearch = _.some(scope.tableInfo.columns, function (current) {
                                        return current.groupedsearch == true;
                                    });
                                    scope.filterRangeSet = _.filter(scope.tabInformation.fields, function (current) {
                                        var column = _.find(scope.tableInfo.columns, function (col) {
                                            return col.name == current.name;
                                        });
                                        return current.filterRange && column.showinsearch
                                    });
                                    scope.filterSet = _.filter(scope.tableInfo.columns, function (current) {
                                        return (!current.groupedsearch && current.showinsearch &&
                                            undefined === _.find(scope.filterRangeSet, function (currentField) {
                                                return currentField.idColumn == current.idColumn;
                                            })
                                        );
                                    });
                                    _.each(scope.filterSet, function (current) {
                                        current.tableName = scope.tableInfo.name;
                                        if (current.reference && !scope.filterInfo.filtersList[current.idReferenceValue]) {
                                            if (current.reference.rtype == "LIST") {
                                                // Loading the referenced SELECT values for the filters of type LIST
                                                var refList = cacheService.getReference(current.idReferenceValue);
                                                if (refList) {
                                                    var data = [{value: '', name: ''}];
                                                    _.each(refList.values, function (val) {
                                                        data.push({value: val.value, name: val.name});
                                                    });
                                                    scope.filterInfo.filtersList[refList.idReference] = {};
                                                    scope.filterInfo.filtersList[refList.idReference].data = data;
                                                } else {
                                                    // TODO: Error handling
                                                }
                                            } else if (current.reference.rtype == "TABLE") {
                                                var refTable = cacheService.getReference(current.idReferenceValue);
                                                if (refTable) {
                                                    if (cacheService.isCompleteConditional(refTable.info.sqlwhere)) {
                                                        cacheService.loadReferenceTableData(current.idReferenceValue, refTable.info.sqlwhere, scope).then(function (result) {
                                                            // Initializing structure to store table list values
                                                            var compl = {
                                                                fieldReferenceValueId: current.idReferenceValue,
                                                                type: current.reference.rtype,
                                                                key: null,
                                                                display: null,
                                                                displayCaption: null,
                                                                columnName: current.name,
                                                                objectValues: []
                                                            };
                                                            scope.complicateVisualFields[current.idReferenceValue] = compl;
                                                            scope.loadFieldValues(current.idReferenceValue, current.type, result)
                                                        }).catch(function (err) {
                                                            $log.error(err);
                                                        });
                                                    }
                                                } else {
                                                    // TODO: Error handling
                                                }
                                            }
                                        }
                                    });

                                    scope.allFilters = [];
                                    _.each(scope.filterSet, function (current) {
                                        current.filterType = 'SINGLE';
                                        scope.allFilters.push(current);
                                    });
                                    _.each(scope.filterRangeSet, function (current) {
                                        current.filterType = 'RANGED';
                                        scope.allFilters.push(current);
                                    });
                                    scope.allFilters = _.sortBy(scope.allFilters, function (current) {
                                        if (current.field)
                                            return current.field.seqno;
                                        else if (current.column && current.column.field)
                                            return current.column.field.seqno;
                                        return null;
                                    });

                                    var watchCollection = "[tableData.tableCurrentPage, tableData.tablePageSize";
                                    if (scope.groupedSearch) {
                                        scope.filterInfo.filters.search = '';
                                        watchCollection += ",filterInfo.filters.search";
                                    }
                                    _.each(scope.filterSet, function (current) {
                                        var filterName = commonService.removeUnderscores(current.name);
                                        current.filterName = filterName;
                                        current.tableName = scope.tableInfo.name;
                                        switch (current.ctype) {
                                            case 'DATE':
                                                scope.filterInfo.filters[filterName] = new Date();
                                                break;
                                            case 'BOOLEAN':
                                                scope.filterInfo.filters[filterName] = false;
                                                break;
                                            default:
                                                scope.filterInfo.filters[filterName] = null;
                                                break;
                                        }
                                        watchCollection += (",filterInfo.filters." + filterName);
                                    });

                                    _.each(scope.filterRangeSet, function (current) {
                                        scope.filterInfo.filtersRange[current.standardName] = {};
                                        switch (current.ctype) {
                                            case 'DATETIME':
                                            case 'TIMESTAMP':
                                            case 'DATE':
                                                scope.filterInfo.filtersRange[current.standardName].start = null;
                                                scope.filterInfo.filtersRange[current.standardName].end = null;
                                                break;
                                            default:
                                                scope.filterInfo.filtersRange[current.standardName].start = '';
                                                scope.filterInfo.filtersRange[current.standardName].end = '';
                                                break;
                                        }
                                    });

                                    watchCollection += "]";
                                    scope.getPersistData();
                                    scope.$watchCollection(watchCollection, function (oldData, newData) {
                                        scope.persistData();
                                        scope.clearTimeouts();
                                        scope.timeoutFunction = $timeout(function (p) {
                                            var newFilter = JSON.stringify(scope.filterInfo);
                                            // Preventing request if the user is still typing
                                            if (newFilter === p.currentFilter) {
                                                scope.reloadTableData();
                                            }
                                        }, 1000, true, {currentFilter: JSON.stringify(scope.filterInfo)});
                                    });
                                } else {
                                }
                            };

                            /**
                             * Verifies if the tab is required to load the list of items (e.g. for TABLE types)
                             * @returns {boolean}
                             */
                            scope.isTableType = function(){
                                return !(scope.tabInformation.ttype === 'CHART' ||
                                    scope.tabInformation.ttype === 'CHARACTERISTIC' ||
                                    scope.tabInformation.ttype === 'CHARACTERISTIC_ADVANCED' ||
                                    scope.tabInformation.ttype === 'USERDEFINED' ||
                                    scope.tabInformation.ttype === 'LISTCHART');
                            };

                            /**
                             * Notifies that a change has occurred in the sort data info
                             * @param sortData
                             */
                            scope.notifySortChange = function (sortData) {
                                $rootScope.$broadcast(coreConfigService.events.NAVIGATION_REFRESH_SORT_VALUES, {
                                    idTab: scope.tabGeneratorId,
                                    start: sortData.sortStart,
                                    step: sortData.sortStep
                                });
                            };

                            /**
                             * Show table selector dialog
                             *
                             * @param field Origin field
                             */
                            scope.showTableWindow = function (field) {
                                scope.currentIdReferenceValue = field.idReferenceValue;
                                scope.currentColumnName = field.name;
                                scope.referenceData.referencePageSize = 10;
                                scope.referenceData.referenceCurrentPage = 1;
                                scope.referenceData.currentFilterValue = "";
                                scope.tableFilterReverseSort = false;
                                scope.loadFilterData(scope.complicateVisualFields[scope.currentIdReferenceValue], '');
                                $("#" + scope.tableSearchDialogId).modal();

                            };

                            scope.changeFilterRange = function (value, fieldName, property) {
                                if (moment.isMoment(value))
                                    scope.filterInfo.filtersRange[fieldName][property] = value.format('DD/MM/YYYY');
                                else
                                    scope.filterInfo.filtersRange[fieldName][property] = value;
                                scope.persistData();
                                scope.reloadTableData();
                            };

                            scope.selectSearchValue = function (item) {
                                var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                                var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                                scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                                scope.fieldValue[scope.currentColumnName] = item[commonService.removeUnderscores(col)];
                                scope.changeValue(scope.currentField);
                                scope.persistData();
                                $('#searchModal').modal('hide');
                            };

                            scope.selectTableValue = function (item) {
                                var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                                var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                                scope.tableFilterValue = item[col];
                                scope.filterInfo.filters[commonService.removeUnderscores(scope.currentColumnName)] = item[key];
                                $("#" + scope.tableSearchDialogId).modal('hide');

                                var selected = _.find(scope.filterInfo.filtersReference, function (current) {
                                    return (current.reference == scope.currentIdReferenceValue);
                                });
                                if (!selected) {
                                    selected = {
                                        item: item,
                                        reference: scope.currentIdReferenceValue,
                                        columnName: scope.currentColumnName
                                    };
                                    scope.filterInfo.filtersReference.push(selected);
                                }
                                else {
                                    selected.reference = scope.currentIdReferenceValue;
                                }
                                scope.persistData();
                            };

                            scope.loadFieldValues = function (fieldReferenceValueId, type, result) {
                                scope.complicateVisualFields[fieldReferenceValueId].key = result.colKey.standardName;
                                scope.complicateVisualFields[fieldReferenceValueId].display = result.colDisplay.standardName;
                                scope.complicateVisualFields[fieldReferenceValueId].displayCaption = result.colDisplay.name;
                                scope.complicateVisualFields[fieldReferenceValueId].table = result.table;
                                scope.complicateVisualFields[fieldReferenceValueId].sqlwhere = result.sqlwhere;
                                scope.complicateVisualFields[fieldReferenceValueId].sqlorderby = result.sqlorderby;
                                scope.complicateVisualFields[fieldReferenceValueId].objectValues = result.data.content;
                                var empty = {};
                                empty[result.colKey.standardName] = '';
                                empty[result.colDisplay.standardName] = '';
                                scope.complicateVisualFields[fieldReferenceValueId].objectValues.unshift(empty);
                                scope.referenceTotalElements = result.data.totalElements;
                                // Restore filters in case they where selected
                                _.each(scope.filterInfo.filtersReference, function (current) {
                                    if (scope.complicateVisualFields[current.reference]) {
                                        var key = scope.complicateVisualFields[current.reference].key;
                                        var col = scope.complicateVisualFields[current.reference].display;
                                        scope.tableFilterValue = current.item[col];
                                        scope.filterInfo.filters[commonService.removeUnderscores(current.columnName)] = current.item[key];
                                    }
                                });
                            };

                            scope.$watchCollection(
                                "[referenceData.referenceCurrentPage,referenceData.referencePageSize,referenceData.currentFilterValue]",
                                function (old, newData) {
                                    if (scope.complicateVisualFields[scope.currentIdReferenceValue]) {
                                        var page = (old[0] == newData[0]) ? 1 : scope.referenceData.referenceCurrentPage;
                                        scope.doFilterVisualFields(scope.complicateVisualFields[scope.currentIdReferenceValue], scope.referenceData.currentFilterValue, scope.referenceData.referencePageSize, page);
                                    }
                                }
                            );

                            /**
                             * Filters the search dialog
                             *
                             * @param current Item being filtered
                             * @param filter Filter value
                             * @param pageSize Page size
                             * @param currentPage Page number
                             */
                            scope.doFilterVisualFields = function (current, filter, pageSize, currentPage) {
                                if (currentPage) {
                                    scope.referenceData.referenceCurrentPage = currentPage;
                                } else {
                                    scope.referenceData.referenceCurrentPage = 1;
                                }
                                scope.referenceData.referencePageSize = pageSize;
                                var customFilter = null;
                                if (filter && filter != '') {
                                    customFilter = current.display + "=%" + filter + "%";
                                }
                                scope.loadFilterData(current, customFilter);
                            };

                            /**
                             * Loads data rows for TABLE and TABLEDIR dialog
                             *
                             * @param current Field being analyzed
                             * @param customFilter Filter information
                             */
                            scope.loadFilterData = function (current, customFilter) {
                                var q = customFilter;
                                if (!customFilter) {
                                    q = scope.complicateVisualFields[current.fieldReferenceValueId].sqlwhere;
                                }
                                var sort = '[{ "property":"' + scope.complicateVisualFields[current.fieldReferenceValueId].display + '","direction":"' + (scope.tableFilterReverseSort ? 'DESC' : 'ASC') + '" }]';
                                if (scope.complicateVisualFields[current.fieldReferenceValueId].sqlorderby) {
                                    sort = scope.complicateVisualFields[current.fieldReferenceValueId].sqlorderby;
                                }

                                var table = scope.complicateVisualFields[current.fieldReferenceValueId].table;
                                var _service = null;
                                try {
                                    _service = autogenService.getAutogenService(table);
                                } catch (e) {
                                    $log.error(e);
                                }
                                $log.info("Requesting data from server for table: " + table.name + " with query: " + q);
                                _service.query(
                                    {
                                        idClient: cacheService.loggedIdClient(),
                                        q: q,
                                        sort: sort,
                                        page: scope.referenceData.referenceCurrentPage,
                                        limit: scope.referenceData.referencePageSize
                                    },
                                    function (data2) {
                                        //Armando los datos en el scope
                                        scope.complicateVisualFields[current.fieldReferenceValueId].objectValues = data2.content;
                                        var empty = {};
                                        empty[scope.complicateVisualFields[current.fieldReferenceValueId].key] = '';
                                        empty[scope.complicateVisualFields[current.fieldReferenceValueId].display] = '';
                                        scope.complicateVisualFields[current.fieldReferenceValueId].objectValues.unshift(empty);
                                        scope.referenceTotalElements = data2.totalElements;
                                    },
                                    function (data2) {
                                    }
                                );
                            };

                            if (scope.windowTabInfo) {
                                scope.parentItem = cacheService.getFormItem({
                                    idWindow: scope.windowTabInfo.idWindow,
                                    tablevel: scope.windowTabInfo.tablevel
                                });
                            }

                            scope.searchTab = function (forceReload) {
                                if (scope.tabGeneratorId !== undefined && scope.tabGeneratorId != "") {
                                    cacheService.loadTab(scope.tabGeneratorId, forceReload).then(function (data) {
                                        scope.tabInformation = data;
                                        if (scope.tabInformation.runtime === undefined)
                                            scope.tabInformation.runtime = {readonly: false};
                                        if (data.runtime.sortable)
                                            scope.sortableTabId = data.runtime.sortable.idTab;
                                        scope.cardMode = scope.tabInformation.runtime.defaultIdTab !== scope.tabInformation.idTab;
                                        scope.listViewIdTab = undefined;
                                        scope.cardViewIdTab = undefined;
                                        if (scope.tabInformation.viewMode !== 'CARD') {
                                            scope.listViewIdTab = scope.tabInformation.idTab;
                                        } else if (scope.tabInformation.runtime.relatedTab) {
                                            scope.listViewIdTab = scope.tabInformation.runtime.relatedTab;
                                        }
                                        if (scope.tabInformation.viewMode === 'CARD') {
                                            scope.cardMode = (scope.tabInformation.viewDefault === true);
                                            scope.cardViewIdTab = scope.tabInformation.idTab;
                                        } else if (scope.tabInformation.runtime.relatedTab) {
                                            scope.cardViewIdTab = scope.tabInformation.runtime.relatedTab;
                                        }

                                        scope.relatedCardId = (scope.cardMode || (!scope.tabInformation.runtime.relatedTab)) ? scope.tabGeneratorId : scope.tabInformation.runtime.relatedTab;
                                        scope.showCardMode = scope.cardMode;
                                        scope.supportsMultipleViewMode = scope.tabInformation.supportsMultipleViewMode;
                                        scope.tabInformation.runtime.readonly = (scope.tabInformation.runtime.readonly || scope.tabInformation.uipattern == 'READONLY');
                                        var table = cacheService.getTable(scope.tabInformation.idTable);
                                        scope.displayTable = table;

                                        if (scope.displayTable) {
                                            _.each(scope.tabInformation.fields, function (current) {
                                                current.tableName = scope.displayTable.name;
                                                current.column = _.find(scope.displayTable.columns, function (col) {
                                                    return col.idColumn == current.idColumn;
                                                });
                                            });
                                        }

                                        // Get defined buttons
                                        _.each(scope.tabInformation.fields, function (field) {
                                            if (field.column && field.column.reference && field.column.reference.rtype == 'BUTTON') {
                                                field.column.refButton = cacheService.getReference(field.column.idReferenceValue);
                                            }
                                        });

                                        scope.extraFieldButtons = _.filter(scope.tabInformation.fields, function (field) {
                                            return field.displayed && field.column.refButton && field.column.refButton.info.showMode === 'IN_LIST';
                                        });

                                        if (scope.tableFilter) {
                                            if (table) {
                                                var multipleColumns = _.filter(table.columns, function (column) {
                                                    return column.primaryKey;
                                                }).length > 1;
                                                var linkColumns = _.filter(table.columns, function (column) {
                                                    return column.linkParent || column.linkColumn != null;
                                                });
                                                _.each(linkColumns, function (linkColumn) {
                                                    var parentColumn = "";
                                                    if (linkColumn.linkColumn) {
                                                        parentColumn = commonService.removeUnderscores(linkColumn.linkColumn);
                                                    } else if (linkColumn.linkParent) {
                                                        parentColumn = linkColumn.standardName;
                                                    }
                                                    if (parentColumn === "") {
                                                        $log.error("Could not extract link column for: ", linkColumn);
                                                    } else {
                                                        var linkFilterName = linkColumn.standardName;
                                                        if (linkColumn.linkColumn && multipleColumns && linkColumn.primaryKey) {
                                                            linkFilterName = "id." + linkFilterName;
                                                        }
                                                        if (linkColumn.reference.rtype === 'TABLE' || linkColumn.reference.rtype === 'TABLEDIR') {
                                                            scope.linkFilters[linkFilterName] = scope.tableReferenceItem[parentColumn];
                                                        } else if (linkColumn.reference.rtype === 'STRING' && (scope.tabInformation.ttype === 'IMAGE' || scope.tabInformation.ttype === 'FILE' || scope.tabInformation.idParentTable)) {
                                                            if ((scope.tabInformation.ttype === 'IMAGE' || scope.tabInformation.ttype === 'FILE') && scope.tabInformation.idParentTable){
                                                                scope.linkFilters.idTable = scope.tabInformation.idParentTable;
                                                            }
                                                            if (scope.parentItem) {
                                                                var field = _.find(scope.parentItem.fields, function (field) {
                                                                    return field.column.primaryKey;
                                                                });
                                                                if (field) {
                                                                    scope.linkFilters[linkFilterName] = scope.tableReferenceItem[field.column.standardName];
                                                                } else {
                                                                    $log.error("No field marked as primary key in parent item", linkColumn.idReference, table, scope.tabInformation);
                                                                }
                                                            } else {
                                                                $log.error("This grid should contain a related form and it doesn't", linkColumn.idReference, table, scope.tabInformation);
                                                            }
                                                        } else {
                                                            $log.error("There are no linked table references with the following filter: idReference=" + linkColumn.idReference, table, scope.tabInformation);
                                                        }
                                                    }
                                                });
                                                scope.loadTabInformation();
                                            } else {
                                                $log.error("Could not load table information from cache" + scope.tabInformation.idTable);
                                            }
                                        } else {
                                            scope.loadTabInformation();
                                        }
                                    }).catch(function (err) {
                                        $log.error(err);
                                    });
                                } else {
                                    // TODO: Error handling
                                }
                            };

                            /**
                             * Exports the table to XLS file
                             */
                            scope.exportTable = function () {
                                var service = scope.getTableService();
                                if (service) {
                                    var sortQueryRequest = scope.getSortAndQuery();

                                    // Perform data request
                                    service.export(
                                        {
                                            idClient: cacheService.loggedIdClient(),
                                            idTab: scope.tabGeneratorId,
                                            idUser: cacheService.loggedIdUser(),
                                            idLanguage: $rootScope.selectedLanguage.id,
                                            q: sortQueryRequest.query,
                                            sort: sortQueryRequest.sort
                                        },
                                        function (data) {
                                            commonService.downloadMemoryFile(data.result, data.fileName);
                                        },
                                        function (err) {
                                            $log.error(err);
                                        }
                                    );
                                }
                            };

                            /**
                             * Assembles the query and sort parameters
                             *
                             * @returns {{query: string, sort: string}}
                             */
                            scope.getSortAndQuery = function () {
                                var result = { query: '', sort: '' };
                                var query = "";
                                // Add filters
                                var filterKeys = _.keys(scope.filterInfo.filters);
                                _.each(filterKeys, function (current) {
                                    if (scope.filterInfo.filters[current] === "" || scope.filterInfo.filters[current] === null) {
                                        return result;
                                    }
                                    if (current == "search") {
                                        if (scope.filterInfo.filters[current] && scope.filterInfo.filters[current] != "") {
                                            query += ('search=%' + scope.filterInfo.filters[current] + "%,");
                                        }
                                        return result;
                                    }
                                    var filter = _.find(scope.filterSet, function (currentElement) {
                                        return currentElement.filterName == current;
                                    });
                                    if (filter) {
                                        var type = filter.ctype;
                                        if (!filter.reference.base) {
                                            switch (filter.reference.rtype) {
                                                case 'LIST':
                                                case 'TABLEDIR':
                                                    query += (current + '=' + scope.filterInfo.filters[current] + ",");
                                                    break;
                                            }
                                        } else {
                                            switch (type) {
                                                case 'DATE':
                                                case 'DATETIME':
                                                case 'TIMESTAMP':
                                                    query += (current + '=' + commonService.getFilterDate(scope.filterInfo.filters[current]) + ",");
                                                    break;
                                                case 'BOOLEAN':
                                                case 'INTEGER':
                                                case 'NUMBER':
                                                    query += (current + '=' + scope.filterInfo.filters[current] + ",");
                                                    break;
                                                default:
                                                    if (scope.filterInfo.filters[current] && scope.filterInfo.filters[current] != "") {
                                                        if (filter.reference.rtype == 'LIST' || filter.reference.rtype == 'TABLE'
                                                            || filter.reference.rtype == 'TABLEDIR' || filter.reference.rtype == 'SEARCH') {
                                                            if (filter.field.filterType == "MULTISELECT" && scope.filterInfo.filters[current] && scope.filterInfo.filters[current].length > 0) {
                                                                var filterResult = "";
                                                                for (var i = 0; i < scope.filterInfo.filters[current].length; i++) {
                                                                    filterResult += scope.filterInfo.filters[current][i];
                                                                    if (i + 1 < scope.filterInfo.filters[current].length)
                                                                        filterResult += ";"
                                                                }
                                                                query += (current + '=[' + filterResult + "],");
                                                            } else {
                                                                query += (current + '=' + scope.filterInfo.filters[current] + ",");
                                                            }
                                                        } else {
                                                            query += (current + '=%' + scope.filterInfo.filters[current] + "%,");
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                });

                                // Add link column filter
                                var linkedFilter = _.keys(scope.linkFilters);
                                _.each(linkedFilter, function (current) {
                                    if (scope.linkFilters[current] && scope.linkFilters[current] != "") {
                                        query += (current + '=' + scope.linkFilters[current] + ",");
                                    }
                                });

                                // Add range filters
                                var filterRangeKeys = _.keys(scope.filterInfo.filtersRange);
                                _.each(filterRangeKeys, function (current) {
                                    var starStr = '';
                                    var endStr = '';
                                    var filter = _.find(scope.filterRangeSet, function (currentElement) {
                                        return currentElement.standardName == current;
                                    });
                                    if (current != 'undefined'
                                        && scope.filterInfo.filtersRange[current]
                                        && scope.filterInfo.filtersRange[current].start
                                        && scope.filterInfo.filtersRange[current].start != '') {
                                        switch (filter.ctype) {
                                            case 'DATE':
                                                if (moment.isMoment(scope.filterInfo.filtersRange[current].start)) {
                                                    starStr = scope.filterInfo.filtersRange[current].start.format('DD/MM/YYYY');
                                                } else if (_.isDate(scope.filterInfo.filtersRange[current].start)) {
                                                    starStr = moment(scope.filterInfo.filtersRange[current].start).format('DD/MM/YYYY');
                                                } else {
                                                    starStr = scope.filterInfo.filtersRange[current].start;
                                                }
                                                break;
                                            case 'DATETIME':
                                            case 'TIMESTAMP':
                                                if (moment.isMoment(scope.filterInfo.filtersRange[current].start)) {
                                                    starStr = scope.filterInfo.filtersRange[current].start.format('DD/MM/YYYY HH:mm:ss');
                                                } else if (_.isDate(scope.filterInfo.filtersRange[current].start)) {
                                                    starStr = moment(scope.filterInfo.filtersRange[current].start).format('DD/MM/YYYY HH:mm:ss');
                                                } else {
                                                    starStr = scope.filterInfo.filtersRange[current].start;
                                                }
                                                break;
                                            default:
                                                starStr = scope.filterInfo.filtersRange[current].start;
                                                break;
                                        }
                                    }
                                    if (current != 'undefined' && scope.filterInfo.filtersRange[current]
                                        && scope.filterInfo.filtersRange[current].end && scope.filterInfo.filtersRange[current].end != ''
                                    ) {
                                        switch (filter.ctype) {
                                            case 'DATE':
                                                if (moment.isMoment(scope.filterInfo.filtersRange[current].end)) {
                                                    endStr = scope.filterInfo.filtersRange[current].end.format('DD/MM/YYYY');
                                                } else if (_.isDate(scope.filterInfo.filtersRange[current].end)) {
                                                    endStr = moment(scope.filterInfo.filtersRange[current].end).format('DD/MM/YYYY');
                                                } else {
                                                    endStr = scope.filterInfo.filtersRange[current].end;
                                                }
                                                break;
                                            case 'DATETIME':
                                            case 'TIMESTAMP':
                                                if (moment.isMoment(scope.filterInfo.filtersRange[current].end)) {
                                                    endStr = scope.filterInfo.filtersRange[current].end.format('DD/MM/YYYY HH:mm:ss');
                                                } else if (_.isDate(scope.filterInfo.filtersRange[current].end)) {
                                                    endStr = moment(scope.filterInfo.filtersRange[current].end).format('DD/MM/YYYY HH:mm:ss');
                                                } else {
                                                    endStr = scope.filterInfo.filtersRange[current].end;
                                                }
                                                break;
                                            default:
                                                endStr = scope.filterInfo.filtersRange[current].end;
                                                break;
                                        }
                                    }
                                    if ((starStr != '') || (endStr != '')) {
                                        query += (current + '=' + starStr + "~" + endStr + ',');
                                    }
                                });

                                if (scope.tabInformation.sqlwhere) {
                                    if (!cacheService.isCompleteConditional(scope.tabInformation.sqlwhere)) {
                                        scope.tabInformation.sqlwhere = cacheService.replaceJSConditional(null, null, scope.tabInformation.sqlwhere)
                                    }
                                    query += (',$extended$={' + scope.tabInformation.sqlwhere + "}")
                                }
                                result.query = query;

                                // Assemble sort request
                                var sort = '';

                                var table = cacheService.getTable(scope.tabInformation.idTable);
                                var multipleKeys = _.filter(table.columns, function (column) {
                                    return column.primaryKey;
                                }).length > 1;
                                var found = _.find(table.columns, function (column) {
                                    return column.standardName == scope.tableOrderByField;
                                });
                                if (scope.tableOrderByField != '') {
                                    var orderField = scope.tableOrderByField;
                                    // Attaching "id." prefix for tables with multiple keys
                                    if (found) {
                                        if (found.primaryKey && multipleKeys) {
                                            orderField = "id." + orderField;
                                        } else if (found.reference && found.reference.rtype == 'TABLE') {
                                            var refTable = cacheService.getReference(found.idReferenceValue);
                                            if (refTable) {
                                                var table = cacheService.getTable(refTable.info.idTable);
                                                if (table) {
                                                    var col = _.find(table.columns, function (c) {
                                                        return c.idColumn == refTable.info.idDisplay;
                                                    });
                                                    if (col && col.csource == 'DATABASE') {
                                                        var i, nameTokens = table.name.split('_');
                                                        if (nameTokens.length > 1) {
                                                            orderField = '';
                                                            for (i = 1; i < nameTokens.length; i++) {
                                                                if (i > 1) {
                                                                    orderField += nameTokens[i].substring(0, 1).toUpperCase() + nameTokens[i].substring(1);
                                                                } else {
                                                                    orderField += nameTokens[i];
                                                                }
                                                            }
                                                            orderField += '.' + col.standardName;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    sort = '[{ "property":"' + orderField + '","direction":"' + (scope.tableReverseSort ? 'DESC' : 'ASC') + '" }]';
                                }

                                result.sort = sort;
                                return result;
                            };

                            /**
                             * Gets the data needed for the input of the TABLE search component
                             *
                             * @param currentColumn Current column information
                             * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                             */
                            scope.getListFieldAttr = function (currentColumn) {
                                return {
                                    idReferenceValue: currentColumn.idReferenceValue,
                                    name: currentColumn.name,
                                    idColumn: currentColumn.idColumn,
                                    rtype: currentColumn.reference.rtype,
                                    readOnly: false,
                                    required: false
                                };
                            };

                            /**
                             * Gets the data needed for the input of the TABLE search component
                             *
                             * @param currentColumn Current column information
                             * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                             */
                            scope.getFieldAttr = function (currentColumn) {
                                return {
                                    idReferenceValue: currentColumn.idReferenceValue,
                                    name: currentColumn.name,
                                    idColumn: currentColumn.idColumn,
                                    rtype: currentColumn.reference.rtype,
                                    readOnly: false,
                                    required: false
                                };
                            };

                            scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                                if (scope.tableInfo) {
                                    var field = _.find(scope.tableInfo.columns, function (current) {
                                        return current.idColumn == idColumnData
                                    });
                                    if (field) {
                                        scope.filterInfo.filters[field.standardName] = fieldValueData;
                                        scope.changeColumn(field);
                                    }
                                }
                            };

                            scope.selectMultipleElement = function (idColumnData, fieldValueData, fieldDisplayData, addedData) {
                                var field = _.find(scope.tableInfo.columns, function (current) {
                                    return current.idColumn == idColumnData
                                });
                                if (field) {
                                    if (addedData) {
                                        if (scope.filterInfo.filters[field.standardName] === null || scope.filterInfo.filters[field.standardName] === undefined)
                                            scope.filterInfo.filters[field.standardName] = [];
                                        var found = _.find(scope.filterInfo.filters[field.standardName], function (current) {
                                            return current == fieldValueData;
                                        });
                                        if (!found) {
                                            scope.filterInfo.filters[field.standardName].push(fieldValueData);
                                            var duplicated = JSON.parse(JSON.stringify(scope.filterInfo.filters[field.standardName]))
                                            scope.filterInfo.filters[field.standardName] = duplicated;
                                        }
                                    }
                                    else if (scope.filterInfo.filters[field.standardName] && scope.filterInfo.filters[field.standardName].length > 0) {
                                        var removed = _.filter(scope.filterInfo.filters[field.standardName], function (current) {
                                            return current != fieldValueData;
                                        });
                                        scope.filterInfo.filters[field.standardName] = removed;
                                    }
                                    scope.persistData();
                                    // scope.reloadTableData();
                                }
                            };

                            scope.changeColumn = function (column) {
                                _.each(scope.tableInfo.columns, function (current) {
                                    scope.notifyFieldChange(current, column);
                                });
                            };

                            /**
                             * Notify a field of change on other
                             *
                             * @param field Field notified
                             * @param changedField Change field
                             */
                            scope.notifyFieldChange = function (field, changedField) {
                                if (field.reference.rtype == 'TABLEDIR') {
                                    if (!field.referenceValue) {
                                        field.referenceValue = cacheService.getReference(field.idReferenceValue);
                                    }
                                    if (field.referenceValue.info.sqlwhere && cacheService.isFieldInConditional({column: changedField}, field.referenceValue.info.sqlwhere)) {
                                        if (field.runtime === undefined) {
                                            field.runtime = {}
                                        }
                                        field.runtime.sqlwhere = field.referenceValue.info.sqlwhere;
                                        if (!field.runtime.sqlwhere ||
                                            field.runtime.sqlwhere != field.referenceValue.info.sqlwhere) {
                                            field.runtime.sqlwhere = field.referenceValue.info.sqlwhere;
                                        }
                                        var value = scope.filterInfo.filters[changedField.standardName];
                                        if (value) {
                                            var found = _.find(scope.filterSet, function (current) {
                                                return current.idColumn == field.idColumn;
                                            });
                                            if (found) {
                                                found.runtime = {
                                                    sqlwhere: cacheService.replaceSQLConditional({column: changedField}, value, field.runtime.sqlwhere)
                                                };
                                            }
                                        }
                                    }
                                }
                            };

                            /**
                             * Executes the selected process
                             *
                             * @param field Field clicked
                             */
                            scope.executeProcess = function (field) {
                                utilsService.executeServerProccess(scope, field, {});
                            };

                            /**
                             * Executes the JavaScript Code
                             *
                             * @param field Field clicked
                             */
                            scope.executeJSCode  = function (field) {
                                utilsService.executeJavaScriptCode(scope, field, 'LIST');
                            };

                            /**
                             * Hides the process dialog
                             */
                            scope.hideProcessDialog = function () {
                                $('#' + scope.processDialogId).modal('hide');
                            };

                            scope.verifySelection = function () {
                                _.each(scope.tableElements, function (current) {
                                    current.isCheckedGrid = scope.tableData.checkAll;
                                });
                                scope.$broadcast(coreConfigService.events.NAVIGATION_CHECK_ALL, {
                                    idTab: scope.tabGeneratorId,
                                    checkAll: scope.tableData.checkAll
                                });
                            };

                            scope.searchTab();

                            scope.showSorting = function () {
                                scope.sortMode = !scope.sortMode;
                                scope.cardMode = !scope.cardMode;
                                if (!scope.sortMode) {
                                    scope.reloadTableData();
                                }
                            };

                            // Cleanup
                            // here is where the cleanup happens
                            scope.$on('$destroy', function () {
                                // disable the listener
                                scope.clearTimeouts();
                                cacheService.clearReferenceTableDataValue(scope.$id);
                            });

                            scope.clearTimeouts = function () {
                                if (scope.timeoutFunction) {
                                    $timeout.cancel(scope.timeoutFunction);
                                }
                            };
                        }
                    }
                }
            }
        }]);
});
