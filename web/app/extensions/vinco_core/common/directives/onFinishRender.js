/**
 * This directive should be used in conjunction with customDatatableBasic directive
 * It should be placed in the last element of the last row generated using ng-repeat
 */
define(['app'], function (app) {

    'use strict';


    return app.registerDirective('onFinishRender', function ($timeout, $rootScope) {
        return {
            restrict: 'A',
            compile: function (tElement, tAttributes) {

                return {
                    post: function (scope, element, attr) {
                        var table = element.parentsUntil("table").last();
                        var isHidden = $(table).is(":hidden");
                        scope.setDataTable = function(){
                            if (scope.$last === true) {
                                $timeout(function () {
                                    if ($rootScope._dataTables === undefined)
                                        $rootScope._dataTables = {};
                                    var table = element.parentsUntil("table").last();
                                    var tableId;
                                    if (table) {
                                        table = $(element.parentsUntil("table").last().parent())[0];
                                        if (table)
                                            tableId = table.id;
                                        else
                                            return;
                                    }
                                    else return;
                                    if ($rootScope.dataTableElements[tableId]) {
                                        $rootScope.dataTableElements[tableId].dataTable().fnDestroy();
                                    }
                                    //else
                                    if (tableId!=="" && $rootScope.dataTableElements && $rootScope.dataTableElements[tableId])
                                        $rootScope._dataTables[tableId] = $rootScope.dataTableElements[tableId].DataTable($rootScope.dataTableOptions);
                                    else
                                        $rootScope.$on(app.eventDefinitions.TABLE_RESPONSIVE_LOADED, function (event, data) {
                                                if (data.tableId == tableId) {
                                                    if ($rootScope.dataTableElements[tableId]) {
                                                        $rootScope.dataTableElements[tableId].dataTable().fnDestroy();
                                                    }
                                                    $rootScope._dataTables[tableId] = $rootScope.dataTableElements[tableId].DataTable($rootScope.dataTableOptions);
                                                }
                                            }
                                        );
                                }, 0, true);
                            }
                        };

                        if (!isHidden){
                            scope.setDataTable();
                        } else {
                            scope.$watch(function (scope) {
                                var table = element.parentsUntil("table").last();
                                return $(table).is(":hidden");
                            }, function (isVisible) {
                                if (isVisible) {
                                    scope.setDataTable();
                                }
                            }, true);
                        }
                    }
                }
            }

        }
    });
});
