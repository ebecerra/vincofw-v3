define(['app', 'lodash'], function (app, _) {

    /**
     * @ngdoc directive
     * @name app.directive:autogenFormAuthorization
     * @description Generates the content of the Authorization Dialog
     * @restrict E
     * @param {String} privilege Name of the privilege demanded
     * @param {String} message HTML message to show at the header of the dialog
     * @param {function} fnOnApproval Callback function to call when the authorization result is obtained.
     *                                      It receives one parameter, boolean, that defines whether the authorization
     *                                      was successful or not .
     */
    'use strict';

    return app.registerDirective('autogenFormAuthorization', ['Authentication', 'adPrivilegeService', 'commonService', 'cacheService', '$log', '$rootScope',
        function (Authentication, adPrivilegeService, commonService, cacheService, $log, $rootScope) {
            return {
                restrict: 'E',
                scope: {
                    privilege: "=",
                    message: "=",
                    onApprovalFunction: '&fnOnApproval'
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.authorization.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {

                            scope.authorisationData = {
                                user: null,
                                password: null
                            };

                            scope.unWatchVisible = scope.$watch('privilege', function (newValue, oldValue) {
                                if (newValue && newValue !== oldValue) {
                                    scope.retrieveUsers();
                                }
                            }, true);

                            /**
                             * Retrieves the users to be displayed
                             */
                            scope.retrieveUsers = function () {
                                adPrivilegeService.users({
                                    idClient: cacheService.loggedIdClient(),
                                    privilege: scope.privilege
                                }, function (data) {
                                    scope.authorizationUsers = data;
                                }, function (error) {
                                    $log.error(error);
                                });
                            };

                            /**
                             * Notifies that the user canceled the request
                             */
                            scope.doCancel = function() {
                                scope.onApprovalFunction({
                                    validated: false
                                });
                            };

                            /**
                             * Performs the authorization logic. Calls the backend with the information provided by the user
                             * @param authorizationForm Form containing the information
                             * @param user User
                             * @param password Password
                             */
                            scope.doApproval = function (authorizationForm, user, password) {
                                authorizationForm.$dirty = true;
                                if (authorizationForm.$valid) {
                                    Authentication.validateUser(user, password, scope.privilege)
                                        .then(function (result) {
                                            var selectedUser = _.find(scope.authorizationUsers, function(current){
                                               return current.username === user;
                                            });
                                            scope.onApprovalFunction({
                                                result: {
                                                    validated: result.validated,
                                                    idUser: selectedUser ? selectedUser.idUser : null
                                                }
                                            });
                                        }).catch(function (error) {
                                            $log.error(error);
                                            if (error.code === 403) {
                                                commonService.showError($rootScope.getMessage('AD_GlobalUserNotAuth'));
                                            } else {
                                                commonService.showError($rootScope.getMessage('AD_ErrValidationUnknow'));
                                            }
                                        }
                                    );
                                }
                            };

                            if (scope.privilege){
                                scope.retrieveUsers();
                            }

                            scope.$on('$destroy', function () {
                                scope.unWatchVisible();
                            });
                        }
                    }
                }
            }
        }
    ]);
});