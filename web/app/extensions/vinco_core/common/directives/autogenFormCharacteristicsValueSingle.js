define(['app', 'moment', 'modules/forms/directives/editors/smartCkEditor'], function (app, moment) {

    /**
     * This directive generates the HTML for editing an Advanced Characteristic whose type is different to SELECT_LIST
     * The following are the parameters to be passed:
     *
     *      characteristic        ->  The associated characteristic
     *      parent-item           ->  The item to whom the characteristic belongs to
     *
     * The following is an example usage of the directive
     *
     *      <autogen-form-characteristics-value-single
     *          characteristic="field"
     *          parent-item="parentItem">
     *      </autogen-form-characteristics-value-single>
     */
    'use strict';

    return app.registerDirective('autogenFormCharacteristicsValueSingle', function ($rootScope, $compile, $timeout, $injector, $log, $state, $q,
                                                                                    Authentication, commonService, cacheService, adCharacteristicService,
                                                                                    adCharacteristicValueService, adTableService, adColumnService,
                                                                                    adTranslationService, adClientService) {
        return {
            restrict: 'E',
            scope: {
                characteristic: '=',
                parentItem: '='
            },
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.characteristics.value.single.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.translationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.translation.dialog.html?v=' + FW_VERSION;
                        scope.translationDialogId = 'translationModal' + scope.tabGeneratorId;

                        /**
                         * Loads the column information for the "value" column
                         * This is for translation purposes
                         */
                        scope.loadValueColumnDefinition = function () {
                            cacheService.loadTable('ad_characteristic_value')
                                .then(function () {
                                    var table = cacheService.getTableByName('ad_characteristic_value');
                                    var column = _.find(table.columns, function (current) {
                                        return current.name == 'value';
                                    });
                                    if (column)
                                        scope.valueColumn = column;
                                })
                                .catch(function (err) {
                                    $log.error(err);
                                });
                        };

                        scope.loadValueColumnDefinition();

                        scope.translation = {};
                        scope.translationProperty = {
                            translationClientId: null,
                            translationInitClientId: null
                        };
                        scope.getMessage = $rootScope.getMessage;
                        scope.languages = $rootScope.languages;
                        scope.idClient = scope.parentItem.item.idClient;
                        scope.keyValue = scope.parentItem.item.id;

                        /**
                         * Loads the characteristic value
                         */
                        scope.loadValue = function () {
                            adCharacteristicValueService.query({
                                idClient: scope.idClient,
                                q: 'idCharacteristic=' + scope.characteristic.idCharacteristic + ",keyValue=" + scope.keyValue,
                                sort: '[{ "property": "seqno", "direction": "ASC" }]'
                            }, function (data) {
                                if (data.content.length > 0) {
                                    scope.item = data.content[0];
                                    scope.item.idField = scope.item.id;
                                    scope.saveCharacteristic(true);
                                } else {
                                    scope.item = {
                                        idClient: scope.idClient,
                                        idModule: scope.characteristic.idModule,
                                        idCharacteristic: scope.characteristic.idCharacteristic,
                                        keyValue: scope.keyValue,
                                        value: '',
                                        active: true
                                    }
                                }
                            }, function (err) {
                                $log.error(err);
                            });
                        };

                        scope.loadValue();

                        /**
                         * Creates or updates the characteristic value
                         */
                        scope.saveCharacteristic = function (hideMessage) {
                            if (scope.item.id) {
                                adCharacteristicValueService.update(scope.item
                                    , function () {
                                        if (!hideMessage) {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                        }
                                    }, function (err) {
                                        $log.error(err);
                                    });
                            } else {
                                adCharacteristicValueService.create(scope.item
                                    , function (data) {
                                        if (!hideMessage) {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                        }
                                        scope.item.id = data.id;
                                        scope.item.idCharacteristicValue = data.id;
                                        scope.item.idField = data.id;
                                    }, function (err) {
                                        $log.error(err);
                                    });
                            }
                        };


                        /**
                         * Shows translation dialog
                         *
                         * @param item Field to be translated
                         */
                        scope.showTranslation = function (item) {
                            scope.translationHasSystem = _.some(Authentication.getLoginData().roles, function (current) {
                                return (current == "ROL_SYSTEM");
                            });
                            scope.translatableItem = item;
                            scope.translationClients = [];
                            if (scope.translationHasSystem) {
                                cacheService.loadClients().then(
                                    function (data) {
                                        scope.translationClients = data;
                                    }
                                ).catch(function (err) {
                                    $log.error(err);
                                });
                            }
                            else {
                                scope.translationProperty.translationClientId = cacheService.loggedIdClient();
                                scope.translationProperty.translationInitClientId = cacheService.loggedIdClient();
                            }
                            scope.translationInfos = {};
                            scope.translatableField = item;
                            scope.translatableField.idField = item.idCharacteristicValue;

                            scope.loadTranslations(cacheService.loggedIdClient()).then(function(){
                                $('#' + scope.translationDialogId).modal();
                            }).catch(function(err){
                                $log.error(err);
                            });
                        };

                        scope.loadTranslations = function(idClient){
                            var defered = $q.defer();
                            var promise = defered.promise;
                            scope.translationInfos = {};

                            adTranslationService.query({
                                idClient: cacheService.loggedIdClient(),
                                q: "rowkey=" + scope.item.idCharacteristicValue +
                                ",idTable=" + scope.valueColumn.idTable +
                                ",idColumn=" + scope.valueColumn.idColumn
                            }, function (data) {
                                scope.translation[scope.translatableItem.idCharacteristicValue] = {};
                                _.each($rootScope.languages, function (language) {
                                    _.each(data.content, function (translation) {
                                        if (translation.idLanguage === language.idLanguage
                                            && translation.idClient===idClient) {
                                            scope.translationInfos[language.countrycode] = translation;
                                            scope.translation[item.idCharacteristicValue][language.countrycode] = translation.translation;
                                        }
                                    });
                                });
                                if (scope.translationProperty.translationClientId !== idClient
                                    || scope.translationProperty.translationClientId === null) {
                                    $timeout(function () {
                                        scope.translationProperty.translationClientId = idClient;
                                        scope.translationProperty.translationInitClientId = idClient;
                                    }, 0, true);
                                }
                                defered.resolve();
                            }, function (err) {
                                defered.reject(err);
                            });
                            return promise;
                        };

                        /**
                         * Stores translations
                         *
                         * @param field Field to be translated
                         */
                        scope.doTranslation = function (field) {
                            var counterSent = 0;
                            var counter = 0;
                            _.each($rootScope.languages, function (language) {
                                if (scope.translation[field.idField][language.countrycode]) {
                                    counterSent++;
                                }
                            });
                            scope.translationProperty.translationInitClientId = scope.translationProperty.translationClientId;
                            _.each($rootScope.languages, function (language) {
                                var request = {
                                    idClient: scope.translationProperty.translationClientId,
                                    idModule: scope.item.idModule,
                                    idTable: scope.valueColumn.idTable,
                                    idColumn: scope.valueColumn.idColumn,
                                    active: true,
                                    rowkey: scope.item.idCharacteristicValue
                                };

                                var methodCreate = adTranslationService.create;
                                var methodUpdate = adTranslationService.update;
                                var methodDelete = adTranslationService.delete;
                                if ((scope.characteristic.reference &&
                                    ((scope.characteristic.reference.rtype == "IMAGE") ||
                                    (scope.characteristic.reference.rtype == "FILE")))) {
                                    methodCreate = adTranslationService.createFile;
                                    methodUpdate = adTranslationService.updateFile;
                                    methodDelete = adTranslationService.deleteFile;
                                }

                                if (scope.translation[field.idCharacteristicValue][language.countrycode]) {
                                    request.translation = scope.translation[field.idCharacteristicValue][language.countrycode];
                                    request.idLanguage = language.idLanguage;
                                    if (scope.translationInfos[language.countrycode]) {
                                        request.idTranslation = scope.translationInfos[language.countrycode].idTranslation;
                                        request.created = scope.translationInfos[language.countrycode].created;
                                        request.active = scope.translationInfos[language.countrycode].active;
                                        methodUpdate(
                                            request,
                                            function (data) {
                                                counter++;
                                                if (counter == counterSent) {
                                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                    scope.translationClients = null;
                                                    $('#' + scope.translationDialogId).modal('hide');
                                                }
                                            }, function (data) {
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                            }
                                        );
                                    }
                                    else {
                                        request.idTranslation = null;
                                        request.created = null;
                                        request.active = true;
                                        methodCreate(
                                            request,
                                            function (data) {
                                                counter++;
                                                if (counter == counterSent) {
                                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                    $('#' + scope.translationDialogId).modal('hide');
                                                }
                                            }, function (data) {
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                            }
                                        );
                                    }
                                }
                                else if (scope.translationInfos[language.countrycode] && scope.translationInfos[language.countrycode].idTranslation) {
                                    methodDelete(
                                        {
                                            idTranslation: scope.translationInfos[language.countrycode].idTranslation,
                                            idClient: cacheService.loggedIdClient()
                                        },
                                        function () {
                                            scope.translation[field.idField][language] = null;
                                            scope.translationInfos[language] = null;
                                            counter++;
                                            if (counter === counterSent) {
                                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                scope.translationClients = null;
                                                $('#' + scope.translationDialogId).modal('hide');
                                            }
                                        }, function (data) {
                                            commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                        }
                                    );
                                }
                            });
                        };

                        /**
                         * Hides translation dialog
                         */
                        scope.cancelTranslation = function () {
                            $('#' + scope.translationDialogId).modal('hide');
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getFieldAttr = function (item) {
                            return {
                                idReferenceValue: scope.characteristic.idReferenceValue,
                                name: item.name,
                                idColumn: item.idCharacteristicValue,
                                rtype: scope.characteristic.reference.rtype,
                                readOnly: false,
                                required: true
                            };
                        };

                        scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                            scope.item.value = fieldValueData;
                        };

                        if (scope.characteristic && scope.characteristic.characteristic
                            && scope.characteristic.characteristic.description) {
                            scope.characteristic.finalName = scope.characteristic.characteristic.description;
                        }
                        else
                            scope.characteristic.name = scope.characteristic.finalName;
                    }
                }
            }
        }
    });
});