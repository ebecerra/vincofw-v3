define(['app', 'moment', 'modules/forms/directives/editors/smartCkEditor'], function (app, moment) {

    /**
     * This directive generates the HTML for editing an Advanced Characteristic whose type is SELECT_LIST
     * The following are the parameters to be passed:
     *
     *      characteristic        ->  The associated characteristic
     *      parent-item           ->  The item to whom the characteristic belongs to
     *
     * The following is an example usage of the directive
     *
     *      <autogen-form-characteristics-value-list
     *          characteristic="field"
     *          parent-item="parentItem">
     *      </autogen-form-characteristics-value-list>
     */
    'use strict';

    return app.registerDirective('autogenFormCharacteristicsValueList', function ($rootScope, $compile, $timeout, $injector, $log, $state, $q,
                                                                                  Authentication, commonService, cacheService, adCharacteristicService,
                                                                                  adCharacteristicValueService, adTableService, adColumnService,
                                                                                  adTranslationService, adClientService) {
        return {
            restrict: 'E',
            scope: {
                characteristic: '=',
                parentItem: '='
            },
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.characteristics.value.list.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        var REFERENCE_TYPE = 'ff808081536fb26301536fd76fbc0011';
                        scope.editCharacteristicTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.characteristics.value.edit.html?v=' + FW_VERSION;
                        scope.characteristicDialogId = "dialog_characteristic_" + scope.characteristic.idCharacteristic;
                        scope.characteristicFormId = "form_characteristic_" + scope.characteristic.idCharacteristic;
                        scope.translationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.translation.dialog.html?v=' + FW_VERSION;
                        scope.inputsTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.inputs.html?v=' + FW_VERSION;
                        scope.getMessage = $rootScope.getMessage;
                        scope.languages = $rootScope.languages;
                        scope.sqlwhere = "";
                        scope.idClient = scope.parentItem.item.idClient;
                        scope.keyValue = scope.parentItem.item.id;

                        /**
                         * Loads the associated column's ID for translation purposes
                         */
                        scope.loadTableInfo = function () {
                            adTableService.query({
                                idClient: cacheService.loggedIdClient(),
                                q: 'name=ad_characteristic_value'
                            }, function (tableData) {
                                if (tableData.content.length > 0) {
                                    scope.table = tableData.content[0];
                                    adColumnService.query({
                                        idClient: cacheService.loggedIdClient(),
                                        q: 'idTable=' + scope.table.idTable
                                    }, function (columnData) {
                                        if (columnData.content.length > 0) {
                                            scope.columns = columnData.content;
                                            scope.translationColumn = _.find(scope.columns, function (current) {
                                                return current.name == 'name';
                                            });
                                        }
                                    }, function (err) {
                                        $log.error(err);
                                    });
                                }
                            }, function (err) {
                                $log.error(err);
                            });
                        };

                        /**
                         * Preload preferences
                         */
                        scope.stdPref = cacheService.stdPref;

                        scope.loadTableInfo();

                        /**
                         * Loads the characteristic values
                         */
                        scope.loadValues = function () {
                            adCharacteristicValueService.query({
                                idClient: scope.idClient,
                                q: 'idCharacteristic=' + scope.characteristic.idCharacteristic + ",keyValue=" + scope.keyValue,
                                sort: '[{ "property": "seqno", "direction": "ASC" }]'
                            }, function (data) {
                                scope.values = data.content;
                                var thatScope = scope;
                                cacheService.loadReferenceInfo(REFERENCE_TYPE)
                                    .then(function(loadedRef){
                                        _.each(thatScope.values, function(current){
                                            _.each(loadedRef.values, function(ref){
                                                if (current.vtype==ref.value)
                                                    current.typeValue = ref.name;
                                            })
                                        });
                                    })
                                    .catch(function(err){
                                        $log.error(err);
                                    });
                            }, function (err) {
                                $log.error(err);
                            });
                        };

                        scope.loadValues();

                        /**
                         * Shows the dialog to add a new characteristic value
                         */
                        scope.addCharacteristicValue = function () {
                            scope.item = {
                                idClient: scope.idClient,
                                idModule: scope.characteristic.idModule,
                                idCharacteristic: scope.characteristic.idCharacteristic,
                                active: true,
                                isDefault: false,
                                keyValue: scope.keyValue,
                                vtype: null,
                                seqno: 0
                            };
                            $('#' + scope.characteristicDialogId).modal();
                        };

                        /**
                         * Shows the dialog to edit an existing characteristic value
                         *
                         * @param item Characteristic value being edited
                         */
                        scope.editCharacteristicValue = function (item) {
                            scope.item = item;
                            scope.translationDialogId = 'dialog_translation_' + scope.characteristic.idCharacteristicValue;
                            if (scope.item.vtype == 'IMAGE') {
                                scope.item.imgSrc = scope.serverCacheUrl + scope.parentItem.item['updated'] + '/files/' + scope.parentItem.tab['table'].name + '/' + scope.parentItem.item['id'] + '/characteristic/' + scope.parentItem.item['id'];
                            }
                            $('#' + scope.characteristicDialogId).modal();
                        };

                        /**
                         * Deletes a Characteristic value
                         *
                         * @param item Characteristic value being deleted
                         */
                        scope.deleteCharacteristicValue = function (item) {
                            $.SmartMessageBox({
                                title: $rootScope.getMessage('AD_msgConfirmDelete'),
                                buttons: '[' + $rootScope.getMessage('AD_btnDelete') + '][' + $rootScope.getMessage('AD_btnCancel') + ']'

                            }, function (ButtonPressed) {
                                setTimeout(function () {
                                    if (ButtonPressed === $rootScope.getMessage('AD_btnDelete')) {
                                        adCharacteristicValueService.delete({
                                            idClient: item.idClient,
                                            id: item.id
                                        }, function (data) {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            scope.loadValues();
                                        }, function (err) {
                                            $log.error(err);
                                        });
                                    }
                                }, 10);
                            });
                            // The following lines are needed to prevent the dialog from
                            // being shown again by pressing Enter
                            var $focused = $(':focus');
                            $focused.blur();

                        };

                        /**
                         * Closes the Create/Edit Characteristic dialog
                         *
                         * @param form Form being closed
                         */
                        scope.cancelEditCharacteristic = function (form) {
                            form.$dirty = false;
                            form.$valid = true;
                            $('#' + scope.characteristicDialogId).modal('hide');
                        };

                        /**
                         * Creates or updates a characteristic value
                         *
                         * @param form Form being used
                         */
                        scope.saveCharacteristic = function (form) {
                            form.$dirty = true;
                            scope.validate(form);
                            if (form.$valid) {
                                if (scope.item.id) {
                                    var updateFn = adCharacteristicValueService.update;
                                    var hasFile = false;
                                    if (scope.item.image instanceof (FileList)) {
                                        updateFn = adCharacteristicValueService.updateFile;
                                        hasFile = true;
                                    }
                                    updateFn(
                                        scope.item,
                                        function () {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            form.$dirty = false;
                                            form.$valid = true;
                                            if (hasFile) {
                                                scope.item.imgSrc = scope.serverCacheUrl + scope.parentItem.item['updated'] + '/files/' + scope.parentItem.tab['table'].name + '/' + scope.parentItem.item['id'] + '/characteristic/' + scope.parentItem.item['id'];
                                            }
                                            $('#' + scope.characteristicDialogId).modal('hide');
                                        }, function (err) {
                                            $log.error(err);
                                        }
                                    );
                                } else {
                                    adCharacteristicValueService.create(
                                        scope.item,
                                        function (data) {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            scope.item.id = data.id;
                                            scope.item.idCharacteristicValue = data.id;
                                            scope.translationDialogId = 'dialog_translation_' + scope.characteristic.idCharacteristicValue;
                                            scope.loadValues();
                                            $('#' + scope.characteristicDialogId).modal('hide');
                                        }, function (err) {
                                            $log.error(err);
                                        }
                                    );
                                }
                            }
                        };

                        /**
                         * Performs form validation
                         *
                         * @param form Form being validated
                         */
                        scope.validate = function (form) {
                            form.$valid = true;
                            if (scope.checkError(form, scope.item.vtype)) {
                                form.$valid = false;
                            }
                            if (scope.checkError(form, scope.item.name)) {
                                form.$valid = false;
                            }
                        };

                        /**
                         * Checks for required value
                         *
                         * @param form Form being analyzed
                         * @param value Value being analyzed
                         * @returns {boolean} True if the value is required and is empty, false IOC
                         */
                        scope.checkError = function (form, value) {
                            return form.$dirty && (value === undefined || value === null || value == '');
                        };

                        scope.translation = {};
                        scope.translationProperty = {
                            translationClientId: null,
                            translationInitClientId: null
                        };
                        /**
                         * Shows translation dialog
                         *
                         * @param item Characteristic value to be translated
                         */
                        scope.showTranslation = function (item) {
                            scope.translationHasSystem = _.some(Authentication.getLoginData().roles, function (current) {
                                return (current == "ROL_SYSTEM");
                            });
                            scope.translationClients = [];
                            scope.translationProperty = {};
                            if (scope.translationHasSystem) {
                                cacheService.loadClients().then(
                                    function (data) {
                                        scope.translationClients = data;
                                    }
                                ).catch(function (err) {
                                    $log.error(err);
                                });
                            }
                            else {
                                scope.translationProperty.translationClientId = cacheService.loggedIdClient();
                                scope.translationProperty.translationInitClientId = cacheService.loggedIdClient();
                            }
                            scope.translationInfos = {};
                            scope.translatableField = {
                                idField: scope.item.idCharacteristicValue,
                                column: scope.translationColumn
                            };
                            scope.loadTranslations(cacheService.loggedIdClient()).then(function(){
                                $('#' + scope.translationDialogId).modal();
                            }).catch(function(err){
                                $log.error(err);
                            });
                        };

                        scope.loadTranslations = function(idClient){
                            var defered = $q.defer();
                            var promise = defered.promise;
                            scope.translationInfos = {};
                            adTranslationService.query({
                                idClient: idClient,
                                q: "rowkey=" + scope.item.idCharacteristicValue +
                                ",idTable=" + scope.table.idTable +
                                ",idColumn=" + scope.translationColumn.idColumn
                            }, function (data) {
                                scope.translation[scope.item.idCharacteristicValue] = {};
                                _.each($rootScope.languages, function (language) {
                                    _.each(data.content, function (translation) {
                                        if (translation.idLanguage === language.idLanguage
                                            && translation.idClient===idClient) {
                                            scope.translationInfos[language.countrycode] = translation;
                                            scope.translation[scope.item.idCharacteristicValue][language.countrycode] = translation.translation;

                                        }
                                    });
                                });
                                if (scope.translationProperty.translationClientId !== idClient
                                    || scope.translationProperty.translationClientId === null) {
                                    $timeout(function () {
                                        scope.translationProperty.translationClientId = translation.idClient;
                                        scope.translationProperty.translationInitClientId = translation.idClient;
                                    }, 0, true);
                                }
                                defered.resolve();
                            }, function (err) {
                                defered.reject(err);
                            });
                            return promise;
                        };

                        /**
                         * Hides translation dialog
                         */
                        scope.cancelTranslation = function (form) {
                            $('#' + scope.translationDialogId).modal('hide');
                        };


                        /**
                         * Stores translations
                         *
                         * @param field Field to be translated
                         */
                        scope.doTranslation = function (field) {
                            var counterSent = 0;
                            var counter = 0;
                            _.each($rootScope.languages, function (language) {
                                if (scope.translation[scope.item.idCharacteristicValue][language.countrycode]) {
                                    counterSent++;
                                }
                            });
                            scope.translationProperty.translationInitClientId = scope.translationProperty.translationClientId;
                            _.each($rootScope.languages, function (language) {
                                var request = {
                                    idClient: scope.translationProperty.translationClientId,
                                    idModule: scope.table.module.idModule,
                                    idTable: scope.table.idTable,
                                    idColumn: scope.translationColumn.idColumn,
                                    active: true,
                                    rowkey: scope.item.idCharacteristicValue
                                };
                                if (scope.translation[scope.item.idCharacteristicValue][language.countrycode]) {
                                    request.translation = scope.translation[scope.item.idCharacteristicValue][language.countrycode];
                                    request.idLanguage = language.idLanguage;
                                    if (scope.translationInfos[language.countrycode]) {
                                        request.idTranslation = scope.translationInfos[language.countrycode].idTranslation;
                                        request.created = scope.translationInfos[language.countrycode].created;
                                        request.active = scope.translationInfos[language.countrycode].active;
                                        adTranslationService.update(
                                            request,
                                            function (data) {
                                                counter++;
                                                if (counter === counterSent) {
                                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                    scope.translationClients = null;
                                                    $('#' + scope.translationDialogId).modal('hide');
                                                }
                                            }, function (data) {
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                            }
                                        );
                                    }
                                    else {
                                        request.idTranslation = null;
                                        request.created = null;
                                        request.active = true;
                                        adTranslationService.create(
                                            request,
                                            function (data) {
                                                counter++;
                                                if (counter === counterSent) {
                                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                     $('#' + scope.translationDialogId).modal('hide');
                                                }
                                            }, function (data) {
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                            }
                                        );
                                    }
                                }
                                else if (scope.translationInfos[language.countrycode] && scope.translationInfos[language.countrycode].idTranslation) {
                                    adTranslationService.delete(
                                        {
                                            idTranslation: scope.translationInfos[language.countrycode].idTranslation,
                                            idClient: cacheService.loggedIdClient()
                                        },
                                        function () {
                                            scope.translation[field.idField][language] = null;
                                            scope.translationInfos[language] = null;
                                            counter++;
                                            if (counter === counterSent) {
                                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                scope.translationClients = null;
                                                $('#' + scope.translationDialogId).modal('hide');
                                            }
                                        }, function (data) {
                                            commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                        }
                                    );
                                }
                            });
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getListFieldAttr = function (field) {
                            return {
                                idReferenceValue: REFERENCE_TYPE,
                                name: 'vtype',
                                idColumn: 'vtype',
                                rtype: 'LIST',
                                readOnly: false,
                                required: true
                            };
                        };

                        /**
                         * Updates the item with the value selected
                         *
                         * @param idColumnData Item's column (vtype)
                         * @param fieldValueData Item's column's value (item.vtype value)
                         * @param fieldDisplayData Unused (required by the visual component)
                         */
                        scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                            scope.item[idColumnData] = fieldValueData;
                        };
                    }


                }
            }
        }
    });
});