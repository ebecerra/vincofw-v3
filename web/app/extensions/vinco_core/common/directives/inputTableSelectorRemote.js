/**
 * This directive should be used to obtain reference values from TABLE components. The following attributes
 * should be used with the directive. The directive applies ONLY as an attribute:
 *  *   field-attr          The field being processed. Should be a structure with the following information
 *                          {{
 *                              idReferenceValue: Reference value ID,
 *                              name: Field column name
 *                              idColumn: Unique identifier for the field
 *                              rtype: Reference type
 *                              sqlwhere: SQL where clause to add to the filter
 *                              sqlorderby: SQL order clause to use, in case it is needed
 *                           }}
 *  *   fn-on-focus         A callback function to be notified when the component has focus
 *  *   selected-fn-attr    A callback function that the caller scope should implement to receive the selected value
 *                           The callback will receive three parameters.
 *                              idColumnData: Unique identifier for the field (as received in **field-attr**
 *                              fieldValueData: Selected value
 *                              fieldDisplayData: Selected value's display representation
 *                        In order to guarantee the callback function is called, the rest of the following attributes
 *                          (including selected-fn-attr), should be present exactly as presented below:
 *
 *                              selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                              id-column-data-attr="idColumnData"
 *                              field-value-data-attr="fieldValueData"
 *                              field-display-data-attr="fieldDisplayData"
 *
 * The caller scope should implement the callback function like this:
 *
 *          scope.selectElement = function(idColumnData, fieldValueData, fieldDisplayData){
 *                                   // custom logic goes here
 *                                  };
 *
 *  Example usage
 *
 *      <div class="input input-file">
 *              <input type="hidden"
 *                      table-selector-remote
 *                      field-attr="{{getFieldAttr(field)}}"
 *                      item-attr="{{fieldValue}}"
 *                      tab-id-attr="{{scope.tab}}"
 *                      fn-on-focus="doComponentEnter(name)"
 *                      selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                      id-column-data-attr="idColumnData"
 *                      field-value-data-attr="fieldValueData"
 *                      field-display-data-attr="fieldDisplayData"/>
 *      </div>
 */
define(['app', 'select2'], function (app) {

    'use strict';

    return app.registerDirective('inputTableSelectorRemote',
        ['$log', '$q', '$timeout', 'hookService', 'cacheService', 'autogenService', 'referenceResolverService', 'coreConfigService',
            function ($log, $q, $timeout, hookService, cacheService, autogenService, referenceResolverService, coreConfigService) {
                return {
                    restrict: 'A',
                    scope: {
                        fieldAttrData: '@fieldAttr',
                        tabIdAttrData: '@tabIdAttr',
                        selectedElementFunction: '&selectedFnAttr',
                        onFocusFunction: '&fnOnFocus'
                    },
                    require: 'ngModel',
                    compile: function (tElement, tAttributes) {
                        return {
                            post: function (scope, element, attrs, ngModel) {
                                scope.pageSize = 10;
                                element.removeAttr('input-table-selector-remote');
                                scope.field = JSON.parse(scope.fieldAttrData);
                                scope.currentFilter = null;
                                scope.visualField = {
                                    fieldReferenceValueId: scope.field.idReferenceValue,
                                    type: scope.field.rtype,
                                    key: null,
                                    display: null,
                                    columnName: scope.field.name,
                                    objectValues: []
                                };

                                /**
                                 * Performs server request
                                 *
                                 * @param customFilter Filter information
                                 * @param select2Options
                                 * @param callback
                                 */
                                scope.loadFilterData = function (customFilter, select2Options, callback) {
                                    var idReferenceValue = scope.field.idReferenceValue;
                                    var sort = scope.visualField.displaySource == 'DATABASE' ? scope.visualField.display : null;
                                    if (scope.field.sqlorderby) {
                                        sort = scope.field.sqlorderby;
                                    }
                                    var _service = null;
                                    var refTable = cacheService.getReference(idReferenceValue);
                                    if (refTable) {
                                        hookService.execHooks('AD_ParamsTableSelectorRemote', { item: scope.field }).then(function () {
                                            var sqlwhere = cacheService.replaceJSConditional(scope.field, null, refTable.info.sqlwhere);
                                            if (cacheService.isCompleteConditional(sqlwhere)) {
                                                var table = cacheService.getTable(refTable.info.idTable);
                                                try {
                                                    _service = autogenService.getAutogenService(table);
                                                } catch (e) {
                                                    $log.error(e);
                                                }
                                                scope.currentFilter = customFilter + (sqlwhere ? ',$extended$={' + sqlwhere + '}' : '');
                                                $timeout(function (p) {
                                                    // Preventing request if the user is still typing
                                                    if (scope.currentFilter == p.currentFilter) {
                                                        $log.info("Requesting data from server for table: " + refTable.info.name + " with query: " + p.currentFilter);
                                                        _service.query(
                                                            {
                                                                idClient: cacheService.loggedIdClient(),
                                                                q: p.currentFilter,
                                                                sort: sort ? '[{ "property":"' + sort + '","direction":"' + (scope.tableFilterReverseSort ? 'DESC' : 'ASC') + '" }]' : null,
                                                                page: select2Options ? select2Options.page : null,
                                                                limit: scope.pageSize
                                                            },
                                                            function (data2) {
                                                                callback({
                                                                    items: data2.content,
                                                                    last: data2.last
                                                                }, scope);
                                                            },
                                                            function (err) {
                                                                $log.error(err);
                                                            }
                                                        );
                                                    }
                                                }, 1000, true, { currentFilter: scope.currentFilter });
                                            }
                                        });
                                    }
                                };

                                /**
                                 * Notifies the client that the component is on focus
                                 *
                                 * @param field Field information associated to the component
                                 */
                                scope.doEnter = function (field) {
                                    scope.onFocusFunction({
                                        name: field.name
                                    });
                                };

                                // Listening to the change event of the element, so we can call the select function callback,
                                // so the component's client gets a notification of the changed value
                                $(element).on("change", function (e) {
                                    if (e.added) {
                                        scope.currentValue = e.added.id;
                                    } else {
                                        scope.currentValue = null;
                                    }
                                    scope.selectedElementFunction({
                                        idColumnData: scope.field.idColumn,
                                        fieldValueData: e.added ? e.added.id : null,
                                        fieldDisplayData: e.added ? e.added.text : null
                                    });
                                });

                                // Listening to the change event of the element, so we can call the select function callback,
                                // so the component's client gets a notification of the changed value
                                $(element).on("focus", function (e) {
                                    alert("Focus");
                                });

                                /**
                                 * Loads the reference's key and display information
                                 *
                                 * @param itemId If passed, requests the item that matches the required itemId according to
                                 *              the reference information
                                 */
                                scope.resolveData = function (itemId) {
                                    var defered = $q.defer();
                                    var promise = defered.promise;
                                    referenceResolverService.resolveTableData(itemId, scope.field.idReferenceValue, scope)
                                        .then(function (data) {
                                            scope.visualField = data.visualField;
                                            scope.referenceTotalElements = data.referenceTotalElements;
                                            if (itemId) {
                                                if (data.visualField.objectValues.length > 0) {
                                                    var data = {
                                                        id: data.visualField.objectValues[0][data.visualField.key],
                                                        text: data.visualField.objectValues[0][data.visualField.display]
                                                    };

                                                    if (scope.currentValue === undefined || scope.currentValue != data.id) {
                                                        scope.currentValue = data.id;
                                                        scope.selectedElementFunction({
                                                            idColumnData: scope.field.idColumn,
                                                            fieldValueData: data.id,
                                                            fieldDisplayData: data.text
                                                        });
                                                    }
                                                    $(element).select2("data", data);
                                                }
                                            }
                                            defered.resolve();
                                        })
                                        .catch(function (err) {
                                            defered.reject(err);
                                            $log.error(err);
                                        });
                                    return promise;
                                };

                                scope.ngModel = ngModel;
                                if (ngModel) {
                                    ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                        if (scope.currentValue !== ngModel.$viewValue) {
                                            if ((!scope.visualField.objectValues
                                                    || !scope.visualField.objectValues
                                                    || scope.visualField.objectValues.length === 0) && ngModel.$viewValue) {
                                                scope.resolveData(ngModel.$viewValue);
                                            } else if (!ngModel.$viewValue) {
                                                $(element).select2("val", "");
                                            } else {
                                                scope.resolveData(ngModel.$viewValue);
                                            }
                                        }
                                    };
                                }

                                scope.$on(coreConfigService.events.NAVIGATION_REFRESH_REFERENCE, function(event, data){
                                    if (data.idColumn === scope.field.idColumn){
                                        if (data.value)
                                            scope.resolveData(data.value);
                                        else
                                            scope.resolveData();
                                    }
                                });

                                if (!scope.field.readonly || scope.field.readonly === undefined || scope.field.readonly === null) {
                                    // Initializing select2 logic
                                    $(element).select2({
                                        placeholder: scope.field.required ? '' : ' ',
                                        allowClear: !scope.field.required,
                                        query: function (options) {
                                            scope.resolveData()
                                            .then(function () {
                                                scope.loadFilterData('search=%' + options.term + '%', options, function (data, scope) {
                                                    var result = {
                                                        results: []
                                                    };
                                                    _.each(data.items, function (current) {
                                                        result.results.push({
                                                            id: current[scope.visualField.key],
                                                            text: current[scope.visualField.display]
                                                        })
                                                    });
                                                    result.more = data.last !== undefined ? !data.last : false;
                                                    options.callback(result);
                                                })
                                            });
                                        }
                                    }).on("select2-focus", function(){
                                        scope.doEnter(scope.field);
                                    });
                                }
                            }
                        }
                    }
                }
            }]);
});
