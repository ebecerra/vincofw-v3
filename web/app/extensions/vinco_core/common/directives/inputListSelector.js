/**
 * This directive should be used to obtain reference values from LIST components. The following attributes
 * should be used with the directive. The directive applies ONLY as an attribute:
 *  *   field-attr          The field being processed. Should be a structure with the following information
 *                          {{
 *                              idReferenceValue: Reference value ID,
 *                              name: Field column name
 *                              idColumn: Unique identifier for the field
 *                              rtype: Reference type
 *                              sqlwhere: SQL where clause to add to the filter
 *                              sqlorderby: SQL order clause to use, in case it is needed
 *                           }}
 *  *   fn-on-focus         A callback function to be notified when the component has focus
 *  *   selected-fn-attr    A callback function that the caller scope should implement to receive the selected value
 *                          The callback will receive three parameters.
 *                              idColumnData: Unique identifier for the field (as received in **field-attr**
 *                              fieldValueData: Selected value
 *                              fieldDisplayData: Selected value (this parameter is present to guarantee parameter
 *                                                                  compatibility with other selectors (see **inputTableSelector**)
 *                          In order to guarantee the callback function is called, the rest of the following attributes
 *                          (including selected-fn-attr), should be present exactly as presented below:
 *
 *                              selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                              id-column-data-attr="idColumnData"
 *                              field-value-data-attr="fieldValueData"
 *                              field-display-data-attr="fieldDisplayData"
 *
 * The caller scope should implement the callback function like this:
 *
 *          scope.selectElement = function(idColumnData, fieldValueData, fieldDisplayData){
 *                                   // custom logic goes here
 *                                  };
 *
 *  Example usage
 *
 *      <div input-list-selector
 *                  ng-model="fieldValue[field.column.name]"
 *                  name="{{field.column.name}}"
 *                  field-attr="{{getListFieldAttr(field)}}"
 *                  item-attr="{{fieldValue}}"
 *                  tab-id-attr="{{scope.tab}}"
 *                  id-column-data-attr="idColumnData"
 *                  field-value-data-attr="fieldValueData"
 *                  field-display-data-attr="fieldDisplayData"
 *                  selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                  fn-on-focus="doComponentEnter(name)"
 *                  ng-required="{{field.column.mandatory}}"
 *                  ng-change="changeValue(field)">
 *       </div>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('inputListSelector', function ($templateRequest, $compile, $rootScope, cacheService) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                fieldAttrData: '@fieldAttr',
                tabIdAttrData: '@tabIdAttr',
                selectedElementFunction: '&selectedFnAttr',
                onFocusFunction: '&fnOnFocus'
            },
            require: '?ngModel',
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {
                        scope.disabled = 'disabled' in attrs;
                        scope.referenceData = {};
                        element.removeAttr('table-selector');
                        scope.field = JSON.parse(scope.fieldAttrData);
                        scope.visualField = {
                            fieldReferenceValueId: scope.field.idReferenceValue,
                            type: scope.field.rtype,
                            key: null,
                            display: null,
                            columnName: scope.field.name,
                            objectValues: []
                        };

                        scope.templateUrl = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.input.list.selector.html?v=' + FW_VERSION;
                        $templateRequest(scope.templateUrl).then(function (html) {
                            var template = angular.element(html);
                            element.append(template);
                            scope.currentIdReferenceValue = scope.field.idReferenceValue;
                            $compile(template)(scope);
                        });

                        scope.ngModel = ngModel;
                        if (ngModel) {
                            ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                scope.selectedField = ngModel.$viewValue;
                            };
                        }

                        scope.getDescription = function () {
                            var key = scope.field.tableName + '$' +  scope.field.name;
                            return $rootScope.tranlationsFieldDescriptions[key];
                        };

                        scope.resolveData = function () {
                            var idReferenceValue = scope.field.idReferenceValue;
                            cacheService.loadReferenceData(idReferenceValue).then(function () {
                                var refList = cacheService.getReference(idReferenceValue);
                                if (refList) {
                                    var sorted = _.sortBy(refList.values, function(current){
                                        return current.seqno;
                                    });
                                    if (!scope.field.required && refList.values.length > 0) {
                                        var keys = _.keys(refList.values[0]);
                                        var empty = {};
                                        _.each(keys, function(key){
                                            empty[key] = "";
                                        });
                                        var contentData = [empty];
                                        scope.visualField.objectValues = contentData.concat(sorted);
                                    } else {
                                        scope.visualField.objectValues = sorted;
                                    }
                                    if (scope.selectedField){
                                        var found = _.find(scope.visualField.objectValues, function(current){
                                            return current.value === scope.selectedField;
                                        });
                                        if (!found){
                                            scope.selectedField = null;
                                            scope.selectedElementFunction({
                                                idColumnData: scope.field.idColumn,
                                                fieldValueData: null,
                                                fieldDisplayData: null
                                            });
                                        }
                                    }
                                } else {
                                    // TODO: Error handling
                                }
                            });
                        };

                        /**
                         * Notifies the client that the component is on focus
                         *
                         * @param field Field information associated to the component
                         */
                        scope.doEnter = function (field) {
                            scope.onFocusFunction({
                                name: field.name
                            });
                        };

                        /**
                         * Calls the callback function to return the selected value
                         * Returns a structure with the following information
                         *  - idColumnData: The id of the field as passed from the caller
                         *  - fieldValueData: The selected value
                         *  - fieldDisplayData: The selected value                       *
                         *
                         * @param item Selected item
                         */
                        scope.selectValue = function (item) {
                            scope.selectedElementFunction({
                                idColumnData: scope.field.idColumn,
                                fieldValueData: item,
                                fieldDisplayData: item
                            });
                         };

                        scope.resolveData();
                    }
                }
            }
        }
    });
});
