/**
 * @ngdoc directive
 * @name app.directive:processNotification
 * @description
 * This directive defines the visualization of the process execution and notification
 *
 * @restrict E
 * @param {Object} itemAttr The item that contains the process
 * @param {Object} processAttr The process data
 * @param {Function} cancelFnAttr The function to call when cancel button is clicked
 * @param {Object=} parentItem The parent item assigned to the process' item. This is an optional parameter
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('processNotification', [
            '$rootScope', '$log', '$modal', 'Authentication', 'cacheService', 'commonService', 'hookService', 'processSocketService', 'utilsService', 'adProcessParamService',
            'adProcessService', 'adReferenceService', 'adProcessOutputService', 'referenceResolverService', 'processParamManagerService',
            function ($rootScope, $log, $modal, Authentication, cacheService, commonService, hookService, processSocketService, utilsService, adProcessParamService,
                      adProcessService, adReferenceService, adProcessOutputService, referenceResolverService, processParamManagerService) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        itemAttrData: '@itemAttr',
                        parentItem: '=?',
                        processAttrData: '@processAttr',
                        cancelFunction: '&cancelFnAttr'
                    },
                    templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/process.notification.html?v=' + FW_VERSION,
                    compile: function (tElement, tAttributes) {
                        return {
                            post: function (scope, element, attributes, ngModel) {
                                scope.authorizationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.authorization.dialog.html?v=' + FW_VERSION;
                                scope.authorizationDialogId = 'authorizationModal' + scope.tabGeneratorId+ scope.$id;
                                scope.oldItemId = "<@no$item$id@>";
                                scope.triggerItemAttr = false;
                                scope.glued = true;
                                scope.showCancel = true;
                                scope.processLogs = [];
                                scope.relatedItem = {};
                                scope.processInputParams = [];
                                scope.hasRole = $rootScope.hasRole;
                                scope.hasPermission = $rootScope.hasPermission;
                                scope.$watchCollection("itemAttrData", function () {
                                    if (scope.itemAttrData)
                                        scope.item = JSON.parse(scope.itemAttrData);
                                    if (scope.item && scope.item.id !== scope.oldItemId) {
                                        scope.oldItemId = scope.item.id;
                                        scope.triggerItemAttr = true;
                                        scope.doProcess();
                                    }
                                });
                                scope.$watchCollection("processAttrData", function () {
                                    if (!scope.triggerItemAttr) {
                                        scope.doProcess();
                                    } else {
                                        scope.triggerItemAttr = false;
                                    }
                                });

                                scope.yesNoValues = [
                                    { value: null, name: '' },
                                    { value: true, name: $rootScope.getMessage('AD_labelYes') },
                                    { value: false, name: $rootScope.getMessage('AD_labelNo') }
                                ];

                                if (cacheService.getFormInfo(scope.tabGeneratorId)) {
                                    scope.item = cacheService.getFormInfo(scope.tabGeneratorId).item;
                                    if (scope.item && scope.item.running) {
                                        scope.processLogs = scope.item.processLogs;
                                        if (scope.item.running) {
                                            scope.startListener();
                                        }
                                    }
                                }

                                /**
                                 * Starts websocket listener
                                 *
                                 * @param callback Callback when websocket is connected
                                 */
                                scope.startListener = function (callback) {
                                    processSocketService.initialize(scope.process.idProcess, scope.messageReceived).then(function () {
                                        $log.log("WebSocket connected.");
                                        if (callback)
                                            callback();
                                    });
                                };

                                /**
                                 * Receives websocket message
                                 *
                                 * @param event Datos del mensaje
                                 */
                                scope.messageReceived = function (event) {
                                    if (scope.process.idProcess !== event.idProcess) {
                                        scope.storeInCachedProcess(event);
                                        return;
                                    }
                                    if (!event.finish) {
                                        if (event.adProcessLog) {
                                            event.adProcessLog.date = new moment(event.adProcessLog.created);
                                            scope.processLogs.push(event.adProcessLog);
                                        }
                                        if (event.outParamName) {
                                            var param = _.find(scope.processOutputParams, function (current) {
                                                return current.name === event.outParamName;
                                            });
                                            if (param) {
                                                param.value = event.outParamValue;
                                            }
                                        }
                                        if (event.info) {
                                            scope.processLogs.push({
                                                log: event.info,
                                                ltype: event.success ? 'INFO' : 'ERROR',
                                                date: event.date
                                            });
                                        }
                                    } else {
                                        if (event.success) {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_ProcessFinished'));
                                            if ($rootScope.modalProcessTab) {
                                                $rootScope.$broadcast(app.eventDefinitions.GLOBAL_PROCESS_FINISHED, {
                                                    windowGeneratorId: $rootScope.modalProcessTab.idWindow,
                                                    tabGeneratorId: $rootScope.modalProcessTab.idTab,
                                                    tablevel: $rootScope.modalProcessTab.tablevel,
                                                    idProcess: event.idProcess
                                                });
                                            } else {
                                                $rootScope.$broadcast(app.eventDefinitions.GLOBAL_PROCESS_FINISHED, { idProcess: event.idProcess });
                                            }
                                        } else {
                                            commonService.showRejectNotification($rootScope.getMessage('AD_ProcessFinishedError'));
                                        }
                                    }
                                    scope.storeProcessData(!event.finish);
                                };

                                /**
                                 * Clear log and closes dialog or tab
                                 */
                                scope.doCancel = function () {
                                    scope.clearLogs();
                                    scope.cancelFunction();
                                };

                                /**
                                 * Clears all the logs
                                 */
                                scope.clearLogs = function () {
                                    scope.processLogs = [];
                                };

                                scope.updateFieldChange = function (field, start) {
                                    _.each(scope.processInputParams, function (param) {
                                        if (param.name !== field.name) {
                                            scope.notifyFieldChange(param, field);
                                        }
                                    });
                                    scope.persistProcessParam(field, start);
                                };

                                scope.notifyFieldChange = function (field, changedField) {
                                    utilsService.applyParamDisplayLogic(changedField, field);
                                    utilsService.updateParamSqlWhere(changedField, field);
                                };

                                /**
                                 * Persists the parameter value
                                 * @param parameter
                                 * @param start
                                 */
                                scope.persistProcessParam = function (parameter, start) {
                                    var paramToSave = _.clone(parameter);
                                    if (!parameter.ranged) {
                                        processParamManagerService.storeParameter(paramToSave);
                                    } else {
                                        if (start) {
                                            paramToSave.value = scope.rangedInput[parameter.name].start;
                                        } else {
                                            paramToSave.value = scope.rangedInput[parameter.name].end;
                                        }
                                        processParamManagerService.storeParameter(paramToSave, start);
                                    }
                                };

                                scope.setFieldValue = function (item, current) {
                                    if (current.ptype === "IN") {
                                        var value = "";
                                        current.runtime = {
                                            displaylogic: current.displaylogic
                                        };
                                        scope.notifyFieldChange(current, null);
                                        var stored = processParamManagerService.loadParameter(current);
                                        if (stored !== null && !current.ranged) {
                                            value = stored;
                                        } else {
                                            if (current.valuedefault) {
                                                var resultDefault = processParamManagerService.getParameterDefaultValue(item, scope.parentItem, scope.globalParameters, current.valuedefault);
                                                if (resultDefault.success) {
                                                    value = resultDefault.defaultValue;
                                                } else {
                                                    $log.error("Can't build process parameter: " + current.name);
                                                    commonService.showRejectNotification($rootScope.getMessage('AD_ProcessMsgOperationError', [current.name]));
                                                }
                                            }
                                        }
                                        if (current.reference.rtype === 'INTEGER') {
                                            current.value = parseInt(value);
                                        } else if (current.reference.rtype === 'NUMBER') {
                                            current.value = parseFloat(value);
                                        } else {
                                            current.value = value;
                                        }
                                    }
                                };

                                /**
                                 * Load process information
                                 *
                                 * @param item Item being edited
                                 * @param process Process being handled
                                 */
                                scope.loadProcessData = function (item, process) {
                                    adProcessOutputService.query({
                                            idClient: process.idClient,
                                            q: "idProcess=" + process.idProcess + ",otype=HTTP"
                                        }, function (data) {
                                            scope.processOutputs = data.content;
                                            adProcessParamService.query({
                                                idClient: process.idClient,
                                                q: "idProcess=" + process.idProcess
                                            }, function (data) {
                                                scope.selectedProcessParams = "";
                                                _.each(data.content, function (current) {
                                                    if (current.ptype === "IN") {
                                                        adReferenceService.filter(
                                                            {idClient: process.idClient},
                                                            {idReference: current.idReference}
                                                        ).then(function (dataFilter) {
                                                            if (dataFilter.length > 0) {
                                                                current.reference = dataFilter[0];
                                                                _.each(scope.processInputParams, function (param) {
                                                                    if (param.name !== current.name) {
                                                                        scope.notifyFieldChange(param, current);
                                                                    }
                                                                });
                                                                if (current.reference.rtype === "TABLE" || current.reference.rtype === "TABLEDIR") {
                                                                    referenceResolverService.resolveTableData(current.value, current.idReferenceValue)
                                                                        .then(function (dataFilter) {
                                                                            if (dataFilter.visualField.objectValues &&
                                                                                dataFilter.visualField.objectValues.length > 0) {
                                                                                current.value = dataFilter.visualField.objectValues[0][dataFilter.visualField.objectValues.key];
                                                                            }
                                                                            scope.setFieldValue(item, current);
                                                                        })
                                                                        .catch(function (err) {
                                                                            $log.error("Can't build process parameter: " + current.name + ".Error", err);
                                                                        });
                                                                } else {
                                                                    scope.setFieldValue(item, current);
                                                                }
                                                            }
                                                        });
                                                        scope.processInputParams.push(current);
                                                    } else {
                                                        scope.processOutputParams.push(current);
                                                    }
                                                });
                                                scope.updateRelatedItem();
                                                _.each(scope.processInputParams, function (current) {
                                                    _.each(scope.processInputParams, function (param) {
                                                        if (param.name !== current.name) {
                                                            scope.notifyFieldChange(param, current);
                                                        }
                                                    });
                                                });
                                            }, function (err) {
                                                $log.error(err);
                                            });
                                        },
                                        function (err) {
                                            $log.error(err);
                                        }
                                    );
                                };

                                /**
                                 * Execute the process
                                 *
                                 * @param parameterData Process parameters
                                 */
                                scope.executeProcess = function (parameterData) {
                                    cacheService.loadPrivileges().then(function(privileges) {
                                        var privilege = _.find(privileges, function (current) {
                                            return current.idPrivilege === scope.process.privilege;
                                        });
                                        var found = _.find(scope.approvals, function(current){
                                            return current.privilege === privilege.name;
                                        });
                                        if (!found) {
                                            found = _.find(Authentication.getLoginData().privileges, function (current) {
                                                return current.idPrivilege === scope.process.privilege;
                                            });
                                        }
                                        if (scope.process.privilege && !found){
                                            scope.privilegeToValidate = privilege.name;
                                            scope.displayMessage = scope.process.privilegeDesc;
                                            scope.authorizationModal = $modal.open({
                                                template: ' <autogen-form-authorization privilege="privilegeToValidate" message="displayMessage" fn-on-approval="doOnApproval(result)"></autogen-form-authorization>',
                                                size: 'md',
                                                scope: scope
                                            });
                                            // Where are setting this line because, when the dialog is being opened inside another dialog,
                                            // the password input for the Authentication dialog loses the focus and can't be accessed
                                            scope.authorizationModal.opened.then(function(){
                                                $(document).off('focusin.modal');
                                            });
                                            scope.execParameterData = parameterData;
                                        } else {
                                            parameterData.approvals = scope.approvals;
                                            scope.execProcedure(parameterData);
                                        }
                                    }).catch(function(error){
                                        $log.error(error);
                                    });
                                };

                                /**
                                 * Executes the selected process
                                 *
                                 * @param item Item being edited
                                 * @param form Form containing the input parameters
                                 */
                                scope.startProcess = function (item, form) {
                                    scope.processLogs = [];
                                    scope.selectedProcessParam = "";
                                    _.each(scope.processOutputParams, function (current) {
                                        current.value = null;
                                    });
                                    scope.containsFiles = false;
                                    var parameterData = {
                                        id: scope.process.idProcess,
                                        idUserLogged: Authentication.getLoginData().idUserLogged,
                                        idClient: cacheService.loggedIdClient(),
                                        params: scope.selectedProcessParam
                                    };
                                    scope.fileMapping = "";
                                    _.each(scope.processInputParams, function (current) {
                                        if (current.reference && current.reference.rtype === "FILE") {
                                            scope.containsFiles = true;
                                            parameterData.file = current.value;
                                            scope.fileMapping += (current.name + "=" + current.value[0].name + ",");
                                        } else {
                                            scope.selectedProcessParam += (current.name + "=" + current.value + ",");
                                        }
                                    });
                                    parameterData.params = scope.selectedProcessParam;
                                    form.$dirty = true;
                                    if (form.$valid) {
                                        if (scope.process.showConfirm) {
                                            commonService.showConfirm(scope.process.confirmMsg, scope, function () {
                                                scope.executeProcess(parameterData);
                                            });
                                        } else {
                                            scope.executeProcess(parameterData);
                                        }
                                    }
                                };

                                scope.execProcedure = function(parameterData){
                                    scope.processLogs.push({
                                        log: $rootScope.getMessage('AD_ProcessStarting'),
                                        date: new moment(),
                                        ltype: "SUCCESS"
                                    });
                                    if (scope.process.approvals) {
                                        // If the process already has approvals, send original approvals and new ones
                                        parameterData.approvals = scope.process.approvals;
                                        _.each(scope.approvals, function(current){
                                            parameterData.approvals.push(current);
                                        })
                                    } else if (scope.approvals){
                                        // If the process did not have approvals, send the newly added approvals
                                        parameterData.approvals = scope.approvals;
                                    }
                                    scope.startListener(function () {
                                        scope.methodExec = adProcessService.exec;
                                        if (scope.containsFiles) {
                                            scope.methodExec = adProcessService.execFile;
                                            parameterData.fileMapping = scope.fileMapping;
                                        }
                                        scope.methodExec(parameterData, function (data) {
                                            if (scope.processOutputs.length > 0) {
                                                commonService.downloadMemoryFile(data.result, data.fileName);
                                            }
                                        }, function (err) {
                                            $log.error(err);
                                        });
                                    });
                                };

                                /**
                                 * Recalls the backend with the approval information added
                                 * @param {object}  result Validation result
                                 * @param {boolean} result.validated True if the user was validated. False i.o.c.
                                 * @param {string}  result.idUser Id of the user that was validated.
                                 */
                                scope.doOnApproval = function(result){
                                    scope.authorizationModal.close();
                                    if (result.validated) {
                                        var approvals = [];
                                        var approval = {
                                            idUser: result.idUser,
                                            idClient: cacheService.loggedIdClient(),
                                            privilege: scope.privilegeToValidate,
                                            idApproval: commonService.generateUID(),
                                            action: scope.process ? "PROCESS" : "TABLE_ACTION"
                                        };
                                        if (scope.process){
                                            approval.idProcess = scope.process.idProcess;
                                            approval.rowkey = scope.process.idProcess;
                                        }
                                        approvals.push(approval);
                                        scope.execParameterData.approvals = approvals;
                                        scope.execProcedure(scope.execParameterData);
                                    }
                                };

                                /**
                                 * Gets the data needed for the input of the TABLE search component
                                 *
                                 * @param param Param to obtain data from
                                 * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                                 */
                                scope.getFieldAttr = function (param) {
                                    var parametrizedItem = {};
                                    var total = 0;
                                    _.each(this.processInputParams, function (current) {
                                        if (current.value) {
                                            total++;
                                            parametrizedItem[current.name] = current.value;
                                        }
                                    });
                                    var result = {
                                        idReferenceValue: param.idReferenceValue,
                                        name: param.name,
                                        idColumn: param.idProcessParam,
                                        rtype: param.reference.rtype,
                                        display: true,
                                        required: param.mandatory
                                    };
                                    if (total > 0) {
                                        result.relatedItem = parametrizedItem;
                                    }
                                    return result;
                                };

                                /**
                                 * Gets the data needed for the input of the LIST search component
                                 *
                                 * @param param Field to obtain data from
                                 * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                                 */
                                scope.getListFieldAttr = function (param) {
                                    return utilsService.getListFieldAttr(param);
                                };

                                /**
                                 * Updates the related item
                                 */
                                scope.updateRelatedItem = function(){
                                    $log.info("Executing processNotification.selectElement");
                                    var total = 0;
                                    scope.relatedItem = {};
                                    _.each(scope.processInputParams, function (current) {
                                        if (current.value) {
                                            total++;
                                            scope.relatedItem[current.name] = current.value;
                                        }
                                    });
                                    if (total === 0) {
                                        scope.relatedItem = null;
                                    }
                                };

                                scope.selectElement = function (idProcessParam, fieldValueData, fieldDisplayData) {
                                    var field = _.find(scope.processInputParams, function (current) {
                                        return current.idProcessParam === idProcessParam
                                    });
                                    if (field) {
                                        scope.persistProcessParam(field, true);
                                        field.value = fieldValueData;
                                    }
                                    scope.updateRelatedItem();
                                };

                                /**
                                 * Stores the event in the corresponding cached process so it can be retrieved when changing tabs
                                 *
                                 * @param event Event information
                                 */
                                scope.storeInCachedProcess = function (event) {
                                    if (!event.finish) {
                                        if (scope.process && scope.process.uipattern === "TAB") {
                                            var cached = cacheService.getFormInfo(event.idProcess);
                                            if (cached) {
                                                var processData = cached.item;
                                                if (event.adProcessLog) {
                                                    event.adProcessLog.date = new moment(event.adProcessLog.created);
                                                    cached.processLogs.push(event.adProcessLog);
                                                }
                                                if (event.info) {
                                                    cached.processLogs.push({
                                                        log: event.info,
                                                        ltype: event.success ? 'INFO' : 'ERROR',
                                                        date: event.date
                                                    });
                                                }
                                                cacheService.storeFormInfo(event.idProcess,
                                                    {
                                                        running: true,
                                                        processLogs: cached.processLogs
                                                    }
                                                );
                                            }
                                        }
                                    }
                                };
                                /**
                                 * Stores the process information in cache
                                 */
                                scope.storeProcessData = function (running) {
                                    if (scope.process.uipattern === "TAB") {
                                        cacheService.storeFormInfo(scope.process.idProcess,
                                            {
                                                running: running,
                                                processLogs: scope.processLogs
                                            }
                                        );
                                    }
                                };
                                /**
                                 * Stores the process information in cache
                                 */
                                scope.retrievesProcessData = function () {
                                    if (scope.process && scope.process.uipattern === "TAB") {
                                        var cached = cacheService.getFormInfo(scope.process.idProcess);
                                        if (cached) {
                                            var processData = cached.item;
                                            scope.processLogs = processData.processLogs;
                                            if (processData && processData.running) {
                                                if (processData.running) {
                                                    scope.startListener();
                                                }
                                            }
                                        }
                                    }
                                };

                                /**
                                 * Perfoms the final wiring from the data received
                                 */
                                scope.doProcess = function () {
                                    scope.process = JSON.parse(scope.processAttrData);
                                    scope.retrievesProcessData();
                                    scope.storeProcessData();
                                    scope.processOutputParams = [];
                                    scope.processOutputs = [];
                                    scope.processInputParams = [];
                                    scope.getMessage = $rootScope.getMessage;

                                    if (scope.process) {
                                        scope.globalParameters = {};
                                        hookService.execHooks('AD_GetGlobalParameters', { item: scope.globalParameters }).then(function () {
                                            scope.loadProcessData(scope.item, scope.process);
                                        });
                                    }
                                };
                            }
                        }
                    }
                }
            }
        ]
    );
});
