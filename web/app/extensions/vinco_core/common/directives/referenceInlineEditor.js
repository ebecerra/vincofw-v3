define(['app', 'modules/forms/directives/editors/smartCkEditor'], function (app) {

    'use strict';

    return app.registerDirective('referenceInlineEditor', [
        '$templateRequest', '$compile', '$log', '$timeout', '$modal', '$rootScope', 'commonService', 'adTabService', 'cacheService', 'Authentication',
        function ($templateRequest, $compile, $log, $timeout, $modal, $rootScope, commonService, adTabService, cacheService, Authentication) {
            return {
                restrict: 'E',
                scope: {
                    field: "=",
                    value: "=",
                    createCallback: '&?'
                },
                replace: true,
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/reference.inline.editor.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs) {
                            /******************************************************************
                             * Variables
                             ******************************************************************/
                            scope.disabled = 'disabled' in attrs;
                            /**
                             * Dialog template URL
                             */
                            scope.dialogTemplateUrl = APP_EXTENSION_VINCO_CORE + 'common/views/reference.inline.editor.dialog.html?v=' + FW_VERSION;
                            /**
                             * HTML template element
                             */
                            scope.templateElement = undefined;
                            /**
                             * Dialog ID
                             */
                            scope.dialogId = "reference-editor-" + scope.$id;
                            /**
                             * Instance of the bootstrap modal dialog
                             */
                            scope.modalInstance = null;

                            scope.windowTabInfo = {
                                idWindow: '',
                                tabLevel: 0
                            };


                            /******************************************************************
                             * Methods
                             ******************************************************************/
                            scope.$on(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, function (event, data) {
                                if (scope.tab && data.tabGeneratorId === scope.tab.idTab && scope.modalInstance) {
                                    scope.modalInstance.dismiss('cancel');
                                    if (data.operation) {
                                        var parameter = {
                                            field: scope.field
                                        };
                                        if (data.itemId) {
                                            parameter.value = data.itemId;
                                        }
                                        if (scope.createCallback) {
                                            scope.createCallback(parameter);
                                        }
                                    }
                                }
                            });

                            /**
                             * Shows the modal dialog
                             */
                            scope.openDialog = function () {
                                if (scope.tab.uipattern !== "STANDARD" && !scope.value) {
                                    commonService.showHint($rootScope.getMessage('AD_msgErrorShouldSelectItemFirst'));
                                } else {
                                    $modal.open({
                                        scope: scope,
                                        templateUrl: scope.dialogTemplateUrl, // loads the template
                                        backdrop: false,
                                        windowClass: 'app-modal-window',
                                        controller: function ($scope, $modalInstance) {
                                            $scope.$parent.modalInstance = $modalInstance;
                                        }
                                    });
                                }
                            };

                            /**
                             * Shows the modal dialog
                             */
                            scope.showEditingWindow = function (param) {
                                if (scope.disabled)
                                    return;
                                if (!scope.tab) {
                                    var idTable = null;
                                    switch (scope.field.column.reference.rtype) {
                                        case "TABLE":
                                            idTable = cacheService.getReference(scope.field.column.idReferenceValue).info.idTable;
                                            break;
                                        case "TABLEDIR":
                                            idTable = scope.field.column.referenceValue.info.idTable;
                                            break;
                                        case "SEARCH":
                                            idTable = cacheService.getReference(scope.field.column.idReferenceValue).info.idTable;
                                            break;
                                        default:
                                            idTable = null;
                                            break;
                                    }
                                    if (idTable) {
                                        adTabService.editableTab({
                                            idClient: cacheService.loggedIdClient(),
                                            idUser: cacheService.loggedIdUser(),
                                            idRole: Authentication.getCurrentRole(),
                                            idTable: idTable
                                        }, function (data) {
                                            if (data.properties && data.properties.tab) {
                                                scope.tab = data.properties.tab;
                                                scope.openDialog();
                                            } else {
                                                commonService.showHint($rootScope.getMessage('AD_ErrFormOpenTableEditor'));
                                            }
                                        }, function (error) {
                                            $log.error(error);
                                        });
                                    } else {
                                        $log.error("Unsupported reference type for inline editing");
                                        // TODO
                                    }
                                } else {
                                    scope.openDialog();
                                }
                                $log.info(scope.field);
                                $log.info(scope.value);
                            };

                            /**
                             * Cleanup
                             */
                            scope.$on('$destroy', function () {
                            });
                        }
                    }
                }
            }
        }
    ]);
});