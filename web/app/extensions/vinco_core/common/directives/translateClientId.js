define(['app', 'jquery'], function (app, $) {

    'use strict';

    return app.registerDirective('translateClientId', ['Authentication', '$log', 'cacheService',
        function (Authentication, $log, cacheService) {
            return {
                restrict: 'A',
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs, ngModel) {
                            if (Authentication.getLoginData() && Authentication.getLoginData().roles) {
                                var translationHasSystem = _.some(Authentication.getLoginData().roles, function (current) {
                                    return (current.name == "ROL_SYSTEM") || (current.name == "ROLE_SUPERADMIN");
                                });
                                if (!translationHasSystem) {
                                    $(element).addClass('hide');
                                } else {
                                    $(element).removeClass('hide');
                                    cacheService.loadClients().then(
                                        function (data) {
                                            scope.translationClients = data;
                                        }
                                    ).catch(function (err) {
                                        $log.error(err);
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    ]);
});