/**
 * This directive should be used to obtain reference values from LIST components. The following attributes
 * should be used with the directive. The directive applies ONLY as an attribute:
 *  *   field-attr          The field being processed. Should be a structure with the following information
 *                          {{
 *                              idReferenceValue: Reference value ID,
 *                              name: Field column name
 *                              idColumn: Unique identifier for the field
 *                              rtype: Reference type
 *                              sqlwhere: SQL where clause to add to the filter
 *                              sqlorderby: SQL order clause to use, in case it is needed
 *                           }}
 *  *   selected-fn-attr    A callback function that the caller scope should implement to receive the selected value
 *                          The callback will receive three parameters.
 *                              idColumnData: Unique identifier for the field (as received in **field-attr**
 *                              fieldValueData: Selected value
 *                              fieldDisplayData: Selected value (this parameter is present to guarantee parameter
 *                                                                  compatibility with other selectors (see **inputTableSelector**)
 *                          In order to guarantee the callback function is called, the rest of the following attributes
 *                          (including selected-fn-attr), should be present exactly as presented below:
 *
 *                              selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                              id-column-data-attr="idColumnData"
 *                              field-value-data-attr="fieldValueData"
 *                              field-display-data-attr="fieldDisplayData"
 *
 * The caller scope should implement the callback function like this:
 *
 *          scope.selectElement = function(idColumnData, fieldValueData, fieldDisplayData){
 *                                   // custom logic goes here
 *                                  };
 *
 *  Example usage
 *
 *      <div input-list-selector-remote
 *                  ng-model="fieldValue[field.column.name]"
 *                  name="{{field.column.name}}"
 *                  field-attr="{{getListFieldAttr(field)}}"
 *                  item-attr="{{fieldValue}}"
 *                  tab-id-attr="{{scope.tab}}"
 *                  id-column-data-attr="idColumnData"
 *                  field-value-data-attr="fieldValueData"
 *                  field-display-data-attr="fieldDisplayData"
 *                  selected-fn-attr="selectElement(idColumnData, fieldValueData, fieldDisplayData)"
 *                  fn-on-focus="doComponentEnter(name)"
 *                  ng-required="{{field.column.mandatory}}"
 *                  ng-change="changeValue(field)">
 *       </div>
 */
define(['app'], function (app) {

    'use strict';

    return app.registerDirective('inputListSelectorRemote', ['$q', 'cacheService',
        function ($q, cacheService) {
            return {
                restrict: 'A',
                replace: true,
                scope: {
                    fieldAttrData: '@fieldAttr',
                    tabIdAttrData: '@tabIdAttr',
                    selectedElementFunction: '&selectedFnAttr',
                    onFocusFunction: '&fnOnFocus'
                },
                require: '?ngModel',
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attrs, ngModel) {
                            scope.pageSize = 10;
                            scope.referenceData = {};
                            element.removeAttr('input-list-selector-remote');
                            scope.field = JSON.parse(scope.fieldAttrData);
                            scope.visualField = {
                                fieldReferenceValueId: scope.field.idReferenceValue,
                                type: scope.field.rtype,
                                key: null,
                                display: null,
                                columnName: scope.field.name,
                                objectValues: []
                            };

                            scope.ngModel = ngModel;
                            if (ngModel) {
                                ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                                    scope.resolveData(ngModel.$viewValue);
                                };
                            }

                            scope.resolveData = function (values) {
                                var defered = $q.defer();
                                var promise = defered.promise;
                                var idReferenceValue = scope.field.idReferenceValue;
                                cacheService.loadReferenceData(idReferenceValue).then(function () {
                                    var refList = cacheService.getReference(idReferenceValue);
                                    if (refList) {
                                        var sorted = _.sortBy(refList.values, function (current) {
                                            return current.seqno;
                                        });
                                        scope.visualField.objectValues = sorted;
                                        if (values) {
                                            var data = [];
                                            _.each(values, function (current) {
                                                var found = _.find(scope.visualField.objectValues, function (item) {
                                                    return (item.value === current);
                                                });
                                                if (found) {
                                                    data.push({
                                                        id: found.value,
                                                        text: found.name
                                                    });
                                                    scope.selectedElementFunction(
                                                        {
                                                            idColumnData: scope.field.idColumn,
                                                            fieldValueData: found.value,
                                                            fieldDisplayData: found.name,
                                                            addedData: true
                                                        });
                                                }
                                                else {
                                                    scope.selectedElementFunction(
                                                        {
                                                            idColumnData: scope.field.idColumn,
                                                            fieldValueData: current,
                                                            fieldDisplayData: "",
                                                            addedData: false
                                                        });
                                                }
                                                //     _.each(scope.visualField.objectValues, function(item){
                                                //         if (item.value == current){
                                                //             data.push({
                                                //                 id: item.value,
                                                //                 text: item.name
                                                //             });
                                                //             scope.selectedElementFunction(
                                                //                 {
                                                //                     idColumnData: scope.field.idColumn,
                                                //                     fieldValueData: item.value,
                                                //                     fieldDisplayData: item.name,
                                                //                     addedData: true
                                                //                 });
                                                //         }
                                                //     })
                                            });
                                            if (data.length > 0)
                                                $(element).select2("data", data);
                                        }
                                        defered.resolve(scope.visualField.objectValues);
                                    } else {
                                        defered.reject();
                                    }
                                });
                                return promise;
                            };

                            /**
                             * Notifies the client that the component is on focus
                             *
                             * @param field Field information associated to the component
                             */
                            scope.doEnter = function (field) {
                                scope.onFocusFunction({
                                    name: field.name
                                });
                            };

                            /**
                             * Calls the callback function to return the selected value
                             * Returns a structure with the following information
                             *  - idColumnData: The id of the field as passed from the caller
                             *  - fieldValueData: The selected value
                             *  - fieldDisplayData: The selected value                       *
                             *
                             * @param item Selected item
                             */
                            scope.selectValue = function (item) {
                                scope.selectedElementFunction(
                                    {
                                        idColumnData: scope.field.idColumn,
                                        fieldValueData: item,
                                        fieldDisplayData: item
                                    });
                            };

                            $(element).select2({
                                multiple: true,
                                placeholder: scope.field.required ? '' : ' ',
                                allowClear: !scope.field.required,
                                query: function (options) {
                                    scope.resolveData()
                                        .then(function (items) {
                                                var tags = [];
                                                var result = {
                                                    results: []
                                                };
                                                _.each(items, function (current) {
                                                    result.results.push({
                                                        id: current['value'],
                                                        text: current['name']
                                                    })
                                                });
                                                result.more = false;
                                                options.callback(result);
                                            }
                                        );
                                }
                            }).on("select2-focus", function(){
                                scope.doEnter(scope.field);
                            });

                            // Listening to the change event of the element, so we can call the select function callback,
                            // so the component's client gets a notification of the changed value
                            $(element).on("change", function (e) {
                                    scope.selectedElementFunction(
                                        {
                                            idColumnData: scope.field.idColumn,
                                            fieldValueData: e.added ? e.added.id : (e.removed ? e.removed.id : null),
                                            fieldDisplayData: e.added ? e.added.text : (e.removed ? e.removed.text : null),
                                            addedData: e.added ? true : false
                                        });
                                }
                            );

                        }
                    }
                }
            }
        }]);
});
