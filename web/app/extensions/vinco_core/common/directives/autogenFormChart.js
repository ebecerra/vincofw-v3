define(['app', 'moment'],
    function (app, moment) {

        /**
         * This directive generates the chart tab type
         * It receives the following parameters in the form of attributes
         *
         *      tab-generator-id        ->  The idTab key
         *      table-generator-title   ->  The title of the tab
         *      table-reference-item    ->  The item (if applies) that is related to this tab
         *      window-generator-id     ->  The id of the window parent of this tab
         *
         * The following is an example usage of the directive
         *
         *  <autogen-form-chart tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018" row-id=""></autogen-form-chart>
         */
        'use strict';

        return app.registerDirective('autogenFormChart', ['$rootScope', '$compile', '$timeout', '$injector', '$log', '$state', '$q', 'Authentication', 'commonService', 'cacheService', 'tabManagerService', 'coreConfigService',
            function ($rootScope, $compile, $timeout, $injector, $log, $state, $q, Authentication, commonService, cacheService, tabManagerService, coreConfigService) {
                return {
                    restrict: 'E',
                    require: '?ngModel',
                    templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.chart.html?v=' + FW_VERSION,
                    compile: function (tElement, tAttributes) {
                        return {
                            post: function (scope, element, attributes) {
                                scope.hasRole = $rootScope.hasRole;
                                scope.hasPermission = $rootScope.hasPermission;

                                scope.allFilters = [];
                                scope.filterTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.table.generator.filter.html?v=' + FW_VERSION;
                                scope.headerFilterTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.table.generator.header.filter.html?v=' + FW_VERSION;

                                if (attributes.parentIdTab) {
                                    scope.parentIdTab = attributes.parentIdTab;
                                    cacheService.loadTab(scope.parentIdTab).then(function (data) {
                                        scope.parentTab = data;
                                    });
                                }
                                scope.chartConfig = {};
                                scope.filterInfo = {
                                    filters: {},
                                    filtersRange: {},
                                    filtersList: {},
                                    filtersReference: []
                                };
                                scope.filterRangeSet = [];
                                // Hides extra buttons from filter toolbar
                                scope.hideExtraButtons = true;
                                scope.windowGeneratorId = attributes.windowGeneratorId;
                                scope.tabGeneratorId = attributes.tabGeneratorId;

                                if (scope.windowTabInfo) {
                                    scope.parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel
                                    });
                                }

                                cacheService.loadTab(scope.tabGeneratorId).then(function (data) {
                                    scope.tabInfo = data;
                                    scope.tabInformation = scope.tabInfo;
                                    scope.hideExtraButtons = false;
                                    scope.charts = scope.tabInfo.charts;
                                    _.each(scope.charts, function (current) {
                                        current.runtime.displaylogic = eval(cacheService.replaceJSConditional(null, null, current.displaylogic));
                                        current.bootstrapCol = (12 / scope.tabInfo.columns) * current.span;
                                    });

                                    scope.charts = _.filter(scope.charts, function (current) {
                                        return current.runtime.displaylogic === null
                                            || current.runtime.displaylogic === undefined
                                            || current.runtime.displaylogic === true
                                            || current.runtime.displaylogic === "true";
                                    });

                                    scope.charts = _.sortBy(scope.charts, function (current) {
                                        return current.seqno;
                                    });

                                    scope.groupCharts();

                                    _.each(scope.tabInfo.chartFilters, function (current) {
                                        if (current.displayed) {
                                            current.runtime = { displaylogic: cacheService.replaceJSConditional(null, null, current.displaylogic) };
                                            if (cacheService.isCompleteConditional(current.runtime.displaylogic)) {
                                                current.displayed = eval(current.runtime.displaylogic);
                                                current.runtime.displaylogic = current.displaylogic;
                                                if (current.displayed == null || current.displayed == undefined) {
                                                    current.displayed = true;
                                                }
                                            }
                                        }
                                        current.hidden = !current.displayed;
                                    });

                                    scope.clearChartFilterDesc();

                                    _.each(scope.tabInfo.chartFilters, function (current) {
                                        scope.notifyFieldChange(current, null);

                                        cacheService.loadReferenceInfo(current.idReference).then(function (referenceData) {
                                            if (referenceData) {
                                                var filter = _.find(scope.tabInfo.chartFilters, function (filter) {
                                                    return filter.idReference == referenceData.idReference;
                                                });
                                                // TODO: Cargar las referencia de forma sincrona
                                                /*
                                                 if (filter) {
                                                 filter.reference = referenceData;
                                                 }
                                                 */
                                                scope.getPersistData();

                                                _.each(scope.filters, function (current) {
                                                    if (current.filterName != filter.fieldName) {
                                                        scope.notifyFieldChange(current, filter);
                                                    }
                                                });
                                            }
                                        }).catch(function (err) {
                                            $log.error(err);
                                        });
                                    });

                                    scope.tabInfo.chartFilters = _.sortBy(scope.tabInfo.chartFilters, function (current) {
                                        return current.seqno;
                                    });

                                    scope.filterRangeSet = _.filter(scope.tabInfo.chartFilters, function (current) {
                                        return current.ranged;
                                    });

                                    scope.filterSet = _.filter(scope.tabInfo.chartFilters, function (current) {
                                        current.filterName = current.standardName;
                                        return !current.ranged;
                                    });

                                    _.each(scope.filterRangeSet, function (current) {
                                        scope.filterInfo.filtersRange[current.standardName] = {};
                                        switch (current.ctype) {
                                            case 'DATE':
                                                scope.filterInfo.filtersRange[current.standardName].start = null;
                                                scope.filterInfo.filtersRange[current.standardName].end = null;
                                                break;
                                            case 'TIMESTAMP':
                                                scope.filterInfo.filtersRange[current.standardName].start = null;
                                                scope.filterInfo.filtersRange[current.standardName].end = null;
                                                break;
                                            default:
                                                scope.filterInfo.filtersRange[current.standardName].start = '';
                                                scope.filterInfo.filtersRange[current.standardName].end = '';
                                                break;
                                        }
                                    });

                                    scope.filters = [];
                                    _.each(scope.filterSet, function (current) {
                                        current.filterType = 'SINGLE';
                                        scope.filters.push(current);
                                    });
                                    _.each(scope.filterRangeSet, function (current) {
                                        current.filterType = 'RANGED';
                                        scope.filters.push(current);
                                    });
                                    scope.filters = _.sortBy(scope.filters, function (current) {
                                        return current.seqno;
                                    });
                                    scope.setFilters();
                                });

                                scope.getColumnCaption = function (tableName, columnName) {
                                    if (columnName) {
                                        var filter = _.find(scope.filters, function (filter) {
                                            return filter.name == columnName;
                                        });
                                        return filter ? filter.caption : '';
                                    }
                                    return '';
                                };

                                /**
                                 * Sets the visible filters
                                 */
                                scope.setFilters = function () {
                                    scope.allFilters = _.filter(scope.filters, function (current) {
                                        return (current.displayed === undefined || current.displayed)
                                    });
                                };

                                /**
                                 * Groups the charts in columns
                                 */
                                scope.groupCharts = function () {
                                    scope.chartRows = [];
                                    var colSpan = 12 / scope.tabInfo.columns;
                                    var remainingSpace = 12;
                                    var currentRow = [];
                                    scope.chartRows.push(currentRow);
                                    _.each(scope.charts, function (current) {
                                        remainingSpace = remainingSpace - current.bootstrapCol;
                                        if (current.startnewrow || remainingSpace < 0) {
                                            currentRow = [];
                                            scope.chartRows.push(currentRow);
                                            remainingSpace = 12 - current.bootstrapCol;
                                        }
                                        current.colSpan = colSpan;
                                        currentRow.push(current);
                                    });
                                    scope.charts = scope.charts;
                                };

                                /**
                                 * Returns the referenced type to be rendered
                                 *
                                 * @param filter Filter to analyze
                                 * @returns {*} Reference type
                                 */
                                scope.getFieldType = function (filter) {
                                    if (filter.reference)
                                        return filter.reference.rtype;
                                    return undefined;
                                };

                                /**
                                 * Persists the filters in memory
                                 */
                                scope.persistData = function () {
                                    var filtersData = {
                                        filters: {
                                            filtersRange: scope.filterInfo.filtersRange,
                                            filters: scope.filterInfo.filters,
                                            filtersReference: scope.filterInfo.filtersReference
                                        },
                                        idTab: scope.tabGeneratorId,
                                        idWindow: scope.windowGeneratorId
                                    };
                                    filtersData = JSON.parse(JSON.stringify(filtersData));
                                    _.each(scope.tabInfo.chartFilters, function (current) {
                                        if (current.saveType == 'NO') {
                                            delete filtersData.filters.filters[current.standardName];
                                            delete filtersData.filters.filtersRange[current.standardName];
                                        }
                                    });
                                    tabManagerService.setTabContent(filtersData, scope.tabGeneratorId);
                                };

                                /**
                                 * Reloads filters
                                 */
                                scope.getPersistData = function () {
                                    scope.tabData = tabManagerService.getTabContent(scope.tabData, scope.tabGeneratorId);
                                    if (scope.tabData && scope.tabData.filters && scope.tabData.idTab == scope.tabGeneratorId) {
                                        //Checking if new search keys were added after the last time the cache was stored
                                        var cachedKeys = _.keys(scope.tabData.filters.filtersRange);
                                        var newKeys = _.filter(_.keys(scope.filterInfo.filtersRange), function (current) {
                                            return !_.find(cachedKeys, function (key) {
                                                return key === current;
                                            });
                                        });
                                        _.each(newKeys, function (key) {
                                            scope.tabData.filters.filtersRange[key] = scope.filterInfo.filtersRange[key];
                                        });

                                        _.each(_.keys(scope.filterInfo.filtersRange), function (currentKey) {
                                            var dateField = _.find(scope.tabInfo.chartFilters, function (current) {
                                                return (current.filterName == currentKey && current.reference && (current.reference.rtype == 'DATE' || current.reference.rtype == 'TIMESTAMP'));
                                            });
                                            if (dateField) {
                                                var timePart = (dateField.reference.rtype == 'TIMESTAMP') ? " HH:mm:ss" : '';
                                                if (moment(scope.tabData.filters.filtersRange[currentKey].start, 'DD/MM/YYYY' + timePart).isValid()) {
                                                    scope.tabData.filters.filtersRange[currentKey].start = moment(scope.tabData.filters.filtersRange[currentKey].start, 'DD/MM/YYYY' + timePart);
                                                }
                                                if (moment(scope.tabData.filters.filtersRange[currentKey].end, 'DD/MM/YYYY' + timePart).isValid()) {
                                                    scope.tabData.filters.filtersRange[currentKey].end = moment(scope.tabData.filters.filtersRange[currentKey].end, 'DD/MM/YYYY' + timePart);
                                                }
                                            }
                                        });
                                        scope.filterInfo.filtersRange = scope.tabData.filters.filtersRange;
                                        scope.filterInfo.filters = scope.tabData.filters.filters;
                                        scope.filterInfo.filtersReference = scope.tabData.filters.filtersReference;
                                    }

                                    _.each(scope.tabInfo.chartFilters, function (current) {
                                        if (current.saveType === 'NO') {
                                            var itemValue = $rootScope.modalParentItem;
                                            if (!itemValue && scope.parentItem) {
                                                itemValue = scope.parentItem.item;
                                            }
                                            if (current.ranged) {
                                                if (scope.filterInfo.filtersRange[current.standardName] === undefined
                                                    || scope.filterInfo.filtersRange[current.standardName] === null
                                                    || scope.filterInfo.filtersRange[current.standardName].start === undefined
                                                    || scope.filterInfo.filtersRange[current.standardName].start === ''
                                                    || scope.filterInfo.filtersRange[current.standardName].end === undefined
                                                    || scope.filterInfo.filtersRange[current.standardName].end === '') {
                                                    var rangeFrom = cacheService.replaceJSConditional(null, null, current.valuedefault, itemValue);
                                                    var rangeTo = cacheService.replaceJSConditional(null, null, current.valuedefault2, itemValue);
                                                    $log.debug("rangeFrom: " + rangeFrom + ", rangeTo: " + rangeTo);
                                                    switch (current.reference.rtype) {
                                                        case 'DATE':
                                                            scope.filterInfo.filtersRange[current.standardName].start = commonService.getMomentDate(rangeFrom).format('DD/MM/YYYY');
                                                            scope.filterInfo.filtersRange[current.standardName].end = commonService.getMomentDate(rangeTo).format('DD/MM/YYYY');
                                                            break;
                                                        case 'TIMESTAMP':
                                                            scope.filterInfo.filtersRange[current.standardName].start = commonService.getMomentDate(rangeFrom).format('DD/MM/YYYY HH:mm:ss');
                                                            scope.filterInfo.filtersRange[current.standardName].end = commonService.getMomentDate(rangeTo).format('DD/MM/YYYY HH:mm:ss');
                                                            break;
                                                        default:
                                                            scope.filterInfo.filtersRange[current.standardName].start = rangeFrom;
                                                            scope.filterInfo.filtersRange[current.standardName].end = rangeTo;
                                                            break;
                                                    }
                                                }
                                            } else {
                                                if (scope.filterInfo.filters[current.standardName] === undefined) {
                                                    var from = cacheService.replaceJSConditional(null, null, current.valuedefault, itemValue);
                                                    scope.filterInfo.filters[current.standardName] = from;
                                                }
                                            }
                                        }
                                    });
                                    scope.filtersRange = scope.filterInfo.filtersRange;
                                    scope.filtersLoaded = true;
                                };

                                scope.changeFilter = function (filterName, value) {
                                    scope.filterInfo.filters[filterName] = value;
                                    scope.reloadChartInfo();
                                };

                                scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData, filterName) {
                                    if (idColumnData) {
                                        filterName = idColumnData;
                                    }
                                    var changedField = _.find(scope.filters, function (current) {
                                        return current.filterName == filterName;
                                    });
                                    _.each(scope.filters, function (current) {
                                        if (current.filterName != filterName) {
                                            scope.notifyFieldChange(current, changedField);
                                        }
                                    });
                                    scope.filterInfo = JSON.parse(JSON.stringify(scope.filterInfo));
                                    scope.filterInfo.filters[filterName] = fieldValueData;
                                    scope.reloadChartInfo();
                                };

                                scope.changeFilterRange = function (value, fieldName, property) {
                                    var filter = _.find(scope.tabInfo.chartFilters, function (current) {
                                        return current.standardName = fieldName;
                                    });
                                    if (filter) {
                                        if (moment(value).isValid()) {
                                            if (filter.reference.rtype=="TIMESTAMP")
                                                scope.filterInfo.filtersRange[fieldName][property] = moment(value).format('DD/MM/YYYY HH:mm:ss');
                                            else
                                                scope.filterInfo.filtersRange[fieldName][property] = moment(value).format('DD/MM/YYYY');
                                        } else {
                                            scope.filterInfo.filtersRange[fieldName][property] = value;
                                        }
                                    } else {
                                        scope.filterInfo.filtersRange[fieldName][property] = value;
                                    }
                                    scope.filterInfo = JSON.parse(JSON.stringify(scope.filterInfo));

                                    _.each(scope.filters, function (current) {
                                        if (current.filterName != filter.fieldName) {
                                            scope.notifyFieldChange(current, filter);
                                        }
                                    });
                                    scope.reloadChartInfo();
                                };

                                scope.$on(app.eventDefinitions.GLOBAL_TAB_SELECTED, function (event, data) {
                                    var chart = _.find(scope.charts, function (ch) {
                                        return ch.idTab == data.idTab;
                                    });
                                    if (chart) {
                                        scope.clearChartFilterDesc();
                                    }
                                });

                                scope.$on(coreConfigService.events.NAVIGATION_REFRESH_CHART, function (event, data) {
                                    if (scope.tabGeneratorId === data.idTab) {
                                        scope.resetSearch();
                                    }
                                });

                                scope.$on(app.eventDefinitions.CHART_UPDATE_FILTER_DESC, function (event, data) {
                                    var clickedChart = _.find(scope.charts, function (chart) {
                                        return chart.idChart == data.idClickedChart;
                                    });
                                    var updateChart = _.find(scope.charts, function (chart) {
                                        return chart.idChart == data.idChart;
                                    });
                                    if (clickedChart && updateChart) {
                                        var filterValue = data.value;
                                        if (clickedChart.mode.indexOf("DATE") == 0) {
                                            if (filterValue.length == 8)
                                                filterValue = moment(data.value, 'YYYYMMDD').format('DD/MM/YYYY');
                                            else if (filterValue.length == 6)
                                                filterValue = $rootScope.getMessage('AD_MonthName_' + parseInt(filterValue.substr(4)));
                                        } else {
                                            var valueDesc = _.find(clickedChart.runtime.values, function (item) {
                                                return item.id == data.value;
                                            });
                                            filterValue = valueDesc.name || filterValue;
                                        }
                                        updateChart.filterDesc = (clickedChart.ctype == 'HORIZONTAL_BAR' ? clickedChart.titleY : clickedChart.titleX) + ': ' + filterValue;
                                    }
                                });

                                scope.clearChartFilterDesc = function () {
                                    _.each(scope.charts, function (chart) {
                                        chart.filterDesc = '';
                                    });
                                };

                                scope.resetSearch = function () {
                                    scope.clearChartFilterDesc();
                                    $rootScope.$broadcast(app.eventDefinitions.CHART_RESET_SEARCH, {
                                        idTab: scope.tabInfo.idTab
                                    });
                                };

                                /**
                                 * Gets the data needed for the input of the TABLE search component
                                 *
                                 * @param field Field to obtain data from
                                 * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                                 */
                                scope.getFieldAttr = function (column) {
                                    return {
                                        idReferenceValue: column.idReferenceValue,
                                        name: column.name,
                                        idColumn: column.filterName,
                                        rtype: column.reference.rtype,
                                        display: true,
                                        required: false
                                    };
                                };

                                /**
                                 * Refreshes the visibility of the filters according to a filter change
                                 *
                                 * @param field Field to notify of the change
                                 * @param changedField Changed field
                                 */
                                scope.notifyFieldChange = function (field, changedField) {
                                    if (field.displaylogic) {
                                        if (!field.runtime.displaylogic) {
                                            field.runtime.displaylogic = field.displaylogic;
                                        }
                                        var value = null;
                                        if (changedField && changedField.ranged) {
                                            var valueStart = scope.filterInfo.filtersRange[changedField.standardName].start;
                                            var valueEnd = scope.filterInfo.filtersRange[changedField.standardName].end;
                                            if (changedField.reference && changedField.reference.rtype == 'DATE') {
                                                if (moment.isMoment(valueStart)) {
                                                    valueStart = valueStart.format('DD/MM/YYYY')
                                                }
                                                if (moment.isMoment(valueEnd)) {
                                                    valueEnd = valueEnd.format('DD/MM/YYYY')
                                                }
                                            } else
                                            if (changedField.reference && changedField.reference.rtype == 'TIMESTAMP') {
                                                if (moment.isMoment(valueStart)) {
                                                    valueStart = valueStart.format('DD/MM/YYYY HH:mm:ss')
                                                }
                                                if (moment.isMoment(valueEnd)) {
                                                    valueEnd = valueEnd.format('DD/MM/YYYY HH:mm:ss')
                                                }
                                            }
                                            field.runtime.displaylogic = cacheService.replaceJSConditional({column: {standardName: changedField.standardName + 'Start'}}, valueStart, field.runtime.displaylogic);
                                            field.runtime.displaylogic = cacheService.replaceJSConditional({column: {standardName: changedField.standardName + 'End'}}, valueEnd, field.runtime.displaylogic);
                                            if (cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                                                field.displayed = eval(field.runtime.displaylogic);
                                                field.runtime.displaylogic = field.displaylogic;
                                                var fieldToUpdate = _.find(scope.filters, function (current) {
                                                    return current.standardName == field.standardName;
                                                });
                                                if (fieldToUpdate)
                                                    fieldToUpdate.displayed = field.displayed;
                                            }
                                        } else {
                                            if (changedField) {
                                                value = changedField.value;
                                                field.runtime.displaylogic = cacheService.replaceJSConditional({column: {standardName: changedField.name}}, value, field.runtime.displaylogic);
                                            } else {
                                                field.runtime.displaylogic = cacheService.replaceJSConditional(null, null, field.runtime.displaylogic);
                                            }
                                            if (cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                                                var fieldToUpdate = _.find(scope.filters, function (current) {
                                                    return current.standardName == field.standardName;
                                                });

                                                field.displayed = eval(field.runtime.displaylogic);
                                                if (fieldToUpdate)
                                                    fieldToUpdate.displayed = field.displayed;
                                                field.runtime.displaylogic = field.displaylogic;
                                            }
                                        }
                                    }
                                    scope.setFilters();
                                };

                                /**
                                 * Reloads the chart information
                                 */
                                scope.reloadChartInfo = function () {
                                    scope.clearChartFilterDesc();
                                    scope.persistData();
                                };
                            }
                        }
                    }
                }
            }]);
    });