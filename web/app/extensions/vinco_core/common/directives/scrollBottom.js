define(['app'], function (app) {

    'use strict';

    return app.registerDirective('scrollBottom', function ($interval, $log) {
        return {
            restrict: 'A',
            require: '?ngModel',
            scope: {
                modelData: '='
            },
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {
                        element.removeAttr('scroll-bottom');
                        scope.$watchCollection("modelData", function(){
                            element[0].scrollTop = element[0].scrollHeight
                        });
                    }
                }
            }
        }
    })
});