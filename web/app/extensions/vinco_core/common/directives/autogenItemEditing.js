/**
 * This directive shows the list of subitems relative to a main item ( a mapped one to many relationship )
 * Initially, the list of subitems will be shown, allowing the edition of a single item,
 * in which case, the edition form will be shown in the same tab the subitem list was being showed
 *
 * It receives the following parameters in the form of attributes
 *
 *      tab-generator-id        ->  The idTab key
 *      row-id                  ->  The primary key of row to edit
 *      link-data               ->  The reference keys if this form belongs to an entity that contains foreign keys
 *
 * The following is an example usage of the directive
 *
 *  <autogen-item-editing tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018" row-id=""></autogen-item-editing>
 */
define(['app', 'moment', 'modules/forms/directives/editors/smartCkEditor'], function (app, moment) {

    'use strict';

    return app.registerDirective('autogenItemEditing', function ($rootScope, $compile, tabManagerService) {
        return {
            restrict: 'E',
            templateUrl: APP_EXTENSION_VINCO_CORE + '/common/views/autogen.item.editing.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes) {
                        scope.tableGeneratorTitle = attributes.tableGeneratorTitle;
                        scope.tableGeneratorIcon = attributes.tableGeneratorIcon;
                        scope.tabGeneratorId = attributes.tabGeneratorId;
                        scope.hideHeader = attributes.hideHeader !== undefined;
                        scope.tableFilter = attributes.tableFilter;
                        scope.tableReferenceItem = null;
                        scope.editingItem = [];
                        scope.editingItemTabs = [];
                        // Link item keys. The keys of the parent item (e.g. in ad_tab, it will contain the id_window field)
                        scope.linkData = null;
                        scope.windowGeneratorId = attributes.windowGeneratorId;
                        if (attributes.tableReferenceItem)
                            scope.tableReferenceItem = JSON.parse(attributes.tableReferenceItem);

                        scope.$on(app.eventDefinitions.SUB_ITEM_EDIT, function (event, data) {
                            var item = data.item;
                            scope.multiple = false;
                            if (scope.tabGeneratorId === item.idTab) {
                                scope.linkData = data.linkData;
                                scope.editingItemId = item.id;
                                if (!_.includes(scope.editingItemTabs, item.idTab))
                                    scope.editingItemTabs.push(item.idTab);
                                scope.editingItemTab = scope.tabGeneratorId;
                                scope.editingItem[item.idTab] = true;
                                scope.editingSiblings = data.allItems;
                                tabManagerService.setSubItemEdited({
                                    windowGeneratorId: scope.windowGeneratorId,
                                    tabGeneratorId: scope.tabGeneratorId,
                                    editingItemId: scope.editingItemId,
                                    allItems: data.allItems,
                                    tablevel: scope.windowTabInfo.tablevel
                                });
                                scope.multiple = data.multiple
                            }
                        });

                        scope.$on(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, function (event, data) {
                            if (data.tabGeneratorId == scope.tabGeneratorId) {
                                scope.editingItemTabs = _.filter(scope.editingItemTabs, function (current) {
                                    return current != data.tabGeneratorId;
                                });
                                scope.editingItem[data.tabGeneratorId] = false;
                                tabManagerService.removeSubItemEdited({
                                    windowGeneratorId: scope.windowGeneratorId,
                                    tabGeneratorId: scope.tabGeneratorId
                                });
                            }
                        });

                        var storedItem = tabManagerService.getSubItemEdited({
                            windowGeneratorId: scope.windowGeneratorId,
                            tabGeneratorId: scope.tabGeneratorId
                        });
                        if (storedItem) {
                            scope.editingItemId = storedItem.editingItemId;
                            scope.editingItemTab = storedItem.tabGeneratorId;
                            scope.editingItem[storedItem.tabGeneratorId] = true;
                        }
                        scope.showEditing = function () {
                        };

                        element.append("<autogen-asociated-tabs ng-if='editingItem[editingItemTab]' tab-generator-id='{{tabGeneratorId}}' window-title=''" +
                            "window-generator-id='{{windowGeneratorId}}'" +
                            "parent-id-tab='{{editingItemTab}}' is-multiple='{{multiple}}'" +
                            "row-id='{{editingItemId}}' sub-item='true' link-data='{{linkData}}'></autogen-asociated-tabs>");
                        // we need to tell angular to render the directive
                        $compile(element.contents())(scope);
                    }
                }
            }
        }
    });
});