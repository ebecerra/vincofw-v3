/**
 * This directive generates the column selector button in a grid
 * It receives the following parameters in the form of attributes
 *
 *      tab-generator-id        ->  The idTab key
 *      information-saved       ->  Callback to use when the information was successfully saved
 *
 * The following is an example usage of the directive
 *
 *  <autogen-table-grid-column-selector tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018">
 *  </autogen-table-grid-column-selector>
 */
define(['app', 'jquery', 'lodash'], function (app, $, _) {

    'use strict';

    return app.registerDirective('autogenTableGridColumnSelector', [
        '$rootScope', '$compile', 'adFieldGridService', 'Authentication', 'commonService', '$log', 'tabManagerService', 'cacheService', 'coreConfigService',
        function ($rootScope, $compile, adFieldGridService, Authentication, commonService, $log, tabManagerService, cacheService, coreConfigService) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    tabGeneratorId: '=',
                    informationSaved: '&?'
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.table.grid.column.selector.html',
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes) {
                            ///**********************************************
                            // Scope variables
                            ///**********************************************
                            /**
                             * Grid columns
                             */
                            scope.columns = undefined;

                            ///**********************************************
                            // Scope methods
                            ///**********************************************
                            /**
                             * Closes the dialog
                             */
                            scope.closeDialog = function () {
                                $(".dropdown-tabbed.grid-column" + scope.$id + ".open").removeClass('open');
                            };

                            /**
                             * Saves the information into the backend
                             */
                            scope.saveOrder = function () {
                                var fields = _.filter(scope.columns, function (c) {
                                    return c.checked;
                                });
                                fields = _.sortBy(fields, 'seqno');
                                var ids = "";
                                _.each(fields, function (f, indx) {
                                    ids += (indx == 0 ? "" : ";") + f.field.id;
                                });
                                adFieldGridService.save({
                                    idClient: cacheService.loggedIdClient(),
                                    idTab: scope.gridTabId,
                                    idUser: cacheService.loggedIdUser(),
                                    fields: ids
                                }, function (data) {
                                    $(".dropdown-tabbed.grid-column" + scope.$id + ".open").removeClass('open');
                                    $rootScope.$broadcast(coreConfigService.events.NAVIGATION_REFRESH_COLUMNS, {idTab: scope.tabGeneratorId, reloadColumns: true});
                                    if (scope.informationSaved)
                                        scope.informationSaved();
                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                }, function (err) {
                                    $log.error(err);
                                    commonService.showError($rootScope.getMessage('AD_msgDataSaveError'), 10000);
                                });
                            };

                            ///**********************************************
                            // Scope business logic
                            ///**********************************************
                            scope.toggleMenu = function () {
                                $(".dropdown-tabbed.grid-column"+scope.$id).toggleClass('open');
                            };

                            ///**********************************************
                            // Scope business logic
                            ///**********************************************
                            scope.clearSelection = function () {
                                _.each(scope.columns, function(current){
                                   current.checked = false;
                                });
                            };

                            cacheService.loadTab(scope.tabGeneratorId).then(function(tabInfo){
                                scope.gridTabId = tabInfo.viewMode == 'CARD' ? tabInfo.runtime.relatedTab : scope.tabGeneratorId;

                                cacheService.loadGridColumn(scope.gridTabId).then(function(data){
                                    scope.originalColumns = data;
                                    scope.columns = _.clone(scope.originalColumns);
                                    $(".grid").sortable({
                                        tolerance: 'pointer',
                                        revert: 'invalid',
                                        placeholder: 'placeholder tile',
                                        forceHelperSize: true,
                                        cursorAt: {top: 10, left: 10},
                                        start: function (e, ui) {
                                            ui.placeholder.height(ui.helper.outerHeight());
                                        },
                                        stop: function (event, ui) {
                                            scope.$apply(function () {
                                                try {
                                                    var itemIds = $(".grid").sortable("toArray");
                                                    var seqno = 10;
                                                    _.each(itemIds, function (id) {
                                                        _.each(scope.columns, function (column) {
                                                            if (column.field.id === id) {
                                                                column.seqno = seqno;
                                                                seqno += 10;
                                                            }
                                                        });
                                                    });
                                                } catch (ex) {
                                                }
                                            })
                                        }
                                    });
                                }).catch(function(error){
                                    $log.error(err);
                                });
                            }).catch(function(error){
                                $log.error(err);
                            });

                        }
                    }
                }
            }
        }]
    );

});
