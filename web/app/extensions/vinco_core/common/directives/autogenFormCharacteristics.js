define(['app', 'moment', 'modules/forms/directives/editors/smartCkEditor'], function (app, moment) {

    /**
     * This directive generates a form corresponding to the data defined in the given tab
     * It receives the following parameters in the form of attributes
     *
     *      tab-generator-id        ->  The idTab key
     *      row-id                  ->  The primary key of row to edit
     *      sub-item                ->  Accepts true or false. Used to define if the edited item is a main item or a subitem
     *                                  of a main item. Default value is false
     *      link-data               ->  The reference keys if this form belongs to an entity that contains foreign keys
     *                                  (e.g. the "Fields" tab when editing a Tab, the number of siblings will be 1)
     *                                  Default value is 0
     *      parent-id-tab           ->  The ID of the parent tab
     *
     * The following is an example usage of the directive
     *
     *  <autogen-form-characteristics tab-generator-id="48b71f4d50ebd5f20150ebdfc3580018" row-id=""></autogen-form-characteristics>
     */
    'use strict';

    return app.registerDirective('autogenFormCharacteristics', function ($rootScope, $compile, $timeout, $injector, $log, $state, $q, Authentication, commonService, cacheService, adCharacteristicService, adCharacteristicValueService) {
        return {
            restrict: 'E',
            require: '?ngModel',
            templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.characteristics.html?v=' + FW_VERSION,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        scope.inputsTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.inputs.html?v=' + FW_VERSION;
                        scope.translationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.translation.dialog.html?v=' + FW_VERSION;
                        scope.tabGeneratorId = attributes.tabGeneratorId;
                        scope.translationDialogId = 'translationModal' + scope.tabGeneratorId;
                        scope.tableSearchDialogId = 'tableModal' + scope.tabGeneratorId;
                        scope.searchInputDialogId = 'searchModal' + scope.tabGeneratorId;
                        scope.rowId = attributes.rowId;
                        scope.parentIdTab = null;
                        scope.parentTab = null;
                        scope.fieldRows = [];
                        if (attributes.parentIdTab) {
                            scope.parentIdTab = attributes.parentIdTab;
                            cacheService.loadTab(scope.parentIdTab).then(function (data) {
                                scope.parentTab = data;
                            });
                        }
                        scope.linkData = null;
                        if (attributes.linkData) {
                            scope.linkData = JSON.parse(attributes.linkData);
                        }
                        scope.fields = [];
                        scope.fieldValue = {};
                        scope.item = {};
                        scope.windowGeneratorId = attributes.windowGeneratorId;
                        scope.complicateVisualFields = {};
                        scope.translation = {};
                        scope.translationProperty = {
                            translationClientId: null,
                            translationInitClientId: null
                        };

                        /**
                         * Starts loading the form
                         */
                        scope.loadCharacteristicForm = function () {
                            if (scope.tabGeneratorId !== undefined && scope.tabGeneratorId != "") {
                                cacheService.loadTab(scope.tabGeneratorId).then(function (data) {
                                    scope.tabInformation = data;
                                    scope.adTab = scope.tabInformation;
                                    if (scope.adTab.runtime === undefined)
                                        scope.adTab.runtime = { readonly: false};
                                    scope.parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowGeneratorId,
                                        tablevel: scope.tabInformation.tablevel - 1
                                    });
                                    if (scope.parentItem) {
                                        adCharacteristicService.fields({
                                            idClient: scope.parentItem.item.idClient,
                                            idTable: scope.parentItem.tab.table.idTable,
                                            dtype: scope.parentItem.item.characteristicType ? scope.parentItem.item.characteristicType : scope.parentItem.tab.table.name.toUpperCase(),
                                            mode: 'STANDARD'
                                        }, function (data) {
                                            scope.fields = _.sortBy(data, function (item) {
                                                return item.seqno;
                                            });
                                            _.each(scope.fields, function(current){
                                                current.readonly = scope.adTab.runtime.readonly;
                                            });
                                            scope.loadFieldReferences(0);
                                        }, function (err) {
                                            // TODO
                                        });
                                    }

                                }).catch(function (err) {
                                    // TODO: Error handling
                                    $log.error(err);
                                });
                            } else {
                                // TODO: Error handling
                            }
                        };

                        scope.changeValue = function(field){
                            scope.fieldValue[field.column.name] = this.fieldValue[field.column.name];
                            $log.log(field);
                        };

                        /**
                         * Loads reference information for a given characteristic
                         *
                         * @param index Characteristic index
                         */
                        scope.loadFieldReferences = function (index) {
                            if (index < scope.fields.length) {
                                scope.fields[index].bootstrapGroupColumns = (12 / scope.tabInformation.columns);
                                var idReferenceVal = scope.fields[index].idReferenceValue?scope.fields[index].idReferenceValue:scope.fields[index].idReference;
                                cacheService.loadReferenceData(idReferenceVal).then(
                                    function () {
                                        _.each(scope.fields, function (currentField) {
                                            if (currentField.characteristic.idCharacteristic == scope.fields[index].characteristic.idCharacteristic) {
                                                var idReferenceVal = currentField.idReferenceValue?currentField.idReferenceValue:currentField.idReference;
                                                currentField.display = true;
                                                currentField.span = 1;
                                                currentField.column = {
                                                    name: "field_" + index,
                                                    mandatory: currentField.mandatory,
                                                    reference: cacheService.getReference(idReferenceVal)
                                                };
                                                currentField.reference = cacheService.getReference(idReferenceVal);
                                            }
                                        });
                                        scope.loadFieldReferences(index + 1);
                                    }
                                ).catch(function (err) {
                                    _.each(scope.fields, function (currentField) {
                                        if (currentField.characteristic.idCharacteristic == scope.fields[index].characteristic.idCharacteristic) {
                                            var idReferenceVal = currentField.idReferenceValue?currentField.idReferenceValue:currentField.idReference;
                                            currentField.display = true;
                                            currentField.span = 1;
                                            currentField.column = {
                                                name: "field_" + index,
                                                mandatory: currentField.mandatory,
                                                reference: cacheService.getReference(idReferenceVal)
                                            };
                                            currentField.reference = cacheService.getReference(idReferenceVal);
                                        }
                                    });
                                    scope.loadFieldReferences(index + 1);
                                    $log.error(err);
                                });
                            }
                            else if (index == scope.fields.length) {
                                for (var i = 0; i < scope.fields.length; i++) {
                                    if (!scope.fieldRows[Number.parseInt(i / (scope.adTab.columns))]) {
                                        scope.fieldRows[Number.parseInt(i / (scope.adTab.columns))] = {fields: []};
                                    }
                                    scope.fieldRows[Number.parseInt(i / (scope.adTab.columns))].fields.push(scope.fields[i]);
                                }
                                // Load characteristic values from backend
                                _.each(scope.fields, function (field) {
                                    adCharacteristicValueService.query({
                                        idClient: cacheService.loggedIdClient(),
                                        q: 'idCharacteristic=' + field.characteristic.idCharacteristic + ",keyValue=" + scope.parentItem.item.id
                                    }, function (data) {
                                        if (data.content.length > 0) {
                                            var selected = _.find(scope.fields, function (currentField) {
                                                return currentField.characteristic.idCharacteristic == data.content[0].idCharacteristic;
                                            });
                                            if (selected) {
                                                selected.idCharacteristicValue = data.content[0].idCharacteristicValue;
                                                selected.characteristicValue = data.content[0];
                                                scope.fieldValue[selected.column.name] = data.content[0].value;
                                                scope.processFieldValue(field);
                                            }
                                        }
                                    })
                                });
                            }
                        };

                        /**
                         * Process reference information for a given characteristic
                         *
                         * @param field Characteristic field to analyze
                         */
                        scope.processFieldValue = function (field) {
                            if (field.column.reference.rtype == 'INTEGER') {
                                scope.fieldValue[field.column.name] = Number(scope.fieldValue[field.column.name]);
                            }
                            else if (field.column.reference.rtype == 'YESNO') {
                                scope.fieldValue[field.column.name] = ((scope.fieldValue[field.column.name] == true) ||
                                (scope.fieldValue[field.column.name] == 'S') ||
                                (scope.fieldValue[field.column.name] == 'YES') ||
                                (scope.fieldValue[field.column.name] == 'SI') ||
                                (scope.fieldValue[field.column.name] == 'true'));
                            }
                        };

                        /**
                         * Saves the characteristics
                         */
                        scope.saveCharacteristic = function () {
                            var valueNames = _.keys(scope.fieldValue);
                            var saved = 0;
                            _.each(valueNames, function (keyName) {
                                var fValue = scope.fieldValue[keyName];
                                var field = _.find(scope.fields, function (field) {
                                    return field.column.name == keyName;
                                });
                                if (!field) return;
                                var valueData = fValue;
                                if (scope.complicateVisualFields[field.idReferenceValue] &&
                                    (scope.complicateVisualFields[field.idReferenceValue].type == 'SEARCH')) {
                                    valueData = scope.fieldValue["KEY_" + scope.complicateVisualFields[field.idReferenceValue].columnName];
                                }
                                if (scope.fieldValue["KEY_" +keyName ]){
                                    valueData = scope.fieldValue["KEY_" +keyName ];
                                }
                                if (field.idCharacteristicValue) {
                                    var updateFunction = adCharacteristicValueService.update;
                                    if (field.column.reference.rtype == 'FILE') {
                                        updateFunction = adCharacteristicValueService.updateFile;
                                    }
                                    if (!field.characteristicValue.idCharacteristicValue)
                                        field.characteristicValue.idCharacteristicValue = field.idCharacteristicValue.id;
                                    field.characteristicValue.value = valueData;
                                    var cloned = JSON.parse(JSON.stringify(field.characteristicValue));
                                    if (cloned.characteristic)
                                        delete cloned.characteristic;
                                    if (cloned.client)
                                        delete cloned.client;
                                    if (cloned.module)
                                        delete cloned.module;
                                    cloned.value = valueData;
                                    updateFunction(cloned,
                                        function (data) {
                                            scope.checkFinishedSaving(++saved, true);
                                        }, function (err) {
                                            scope.checkFinishedSaving(++saved, false);
                                        });
                                }
                                else {
                                    var module = (scope.parentItem.item.idModule?scope.parentItem.item.idModule:field.characteristic.idModule);
                                    var client = (scope.parentItem.item.idClient?scope.parentItem.item.idClient:field.characteristic.idClient);
                                    field.characteristicValue = {
                                        idClient: client,
                                        idModule: module,
                                        idCharacteristic: field.characteristic.idCharacteristic,
                                        active: true,
                                        keyValue: scope.parentItem.item.id,
                                        value: valueData
                                    };
                                    var currentField = _.find(scope.fields, function (field) {
                                        return field.column.name == keyName;
                                    });
                                    var createFunction = adCharacteristicValueService.create;
                                    if (currentField.column.reference.rtype == 'FILE') {
                                        createFunction = adCharacteristicValueService.createFile;
                                    }
                                    field.idCharacteristicValue = createFunction(field.characteristicValue, function (data) {
                                        scope.checkFinishedSaving(++saved, true);
                                    }, function (err) {
                                        scope.checkFinishedSaving(++saved, false);
                                    });
                                }
                            });

                        };

                        scope.checkFinishedSaving = function (index, ok) {
                            var actual = 0;
                            var valueNames = _.keys(scope.fieldValue);
                            _.each(valueNames, function (keyName) {
                                var field = _.find(scope.fields, function (field) {
                                    return field.column.name == keyName;
                                });
                                if (field) actual++;
                            });
                            if ((((index + 1) >= scope.fields.length) || ((index ) >= actual)) && ok) {
                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                            }
                        };

                        scope.selectTableValue = function (item) {
                            var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                            var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                            scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                            scope.fieldValue[scope.currentColumnName] = item[col];
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getFieldAttr = function (field) {
                            return {
                                idReferenceValue: field.idReferenceValue,
                                name: field.column.name,
                                idColumn: field.characteristic.id,
                                rtype: field.column.reference.rtype,
                                display: true,
                                required: field.column.mandatory
                            };
                        };

                        /**
                         * Gets the data needed for the input of the TABLE search component
                         *
                         * @param field Field to obtain data from
                         * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                         */
                        scope.getListFieldAttr = function (field) {
                            return {
                                idReferenceValue: field.idReferenceValue,
                                name: field.column.name,
                                idColumn: field.characteristic.id,
                                rtype: field.column.reference.rtype,
                                readonly: field.readonly,
                                required: field.column.mandatory
                            };
                        };

                        scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                            var field = _.find(scope.fields, function (current) {
                                return current.characteristic.id == idColumnData
                            });
                            scope.fieldValue['KEY_' + field.column.name] = fieldValueData;
                            scope.fieldValue[field.column.name] = fieldDisplayData;
                        };
                        scope.loadCharacteristicForm();
                    }


                }
            }
        }
    });
});