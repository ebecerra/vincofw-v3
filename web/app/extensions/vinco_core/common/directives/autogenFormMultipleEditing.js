define(['app', 'moment', 'lodash', 'modules/forms/directives/editors/smartCkEditor'], function (app, moment, _) {

    'use strict';

    /**
     * @ngdoc directive
     * @name app.directive:autogenFormMultipleEditing
     * @restrict E
     * @description
     * This directive generates a form that allows to save multiple items at once
     * It receives the following parameters in the form of attributes
     *
     * @param {string} tabGeneratorId The idTab key. It then searches for a sibling tab with uipattern of MULTISELECT
     * @param {string} parentIdTab The ID of the parent tab
     * @param {Object} linkData The reference keys if this form belongs to an entity that contains foreign keys
     * @param {Object} windowTabInfo Tab information about the tab containing this form
     * @param {Boolean} Used to define if the edited item is a main item or a subitem of a main item. Default value is false
     */
    return app.registerDirective('autogenFormMultipleEditing', [
        '$rootScope', '$compile', '$timeout', '$injector', '$log', '$state', '$q', 'Authentication', 'commonService', 'cacheService', 'autogenService',
        'adColumnService', 'adTranslationService', 'adClientService', 'tabManagerService', 'adHtmlTemplateService', 'coreConfigService', 'adFieldService',
        function ($rootScope, $compile, $timeout, $injector, $log, $state, $q, Authentication, commonService, cacheService, autogenService,
                  adColumnService, adTranslationService, adClientService, tabManagerService, adHtmlTemplateService, coreConfigService, adFieldService) {
            return {
                restrict: 'E',
                scope: {
                    tabGeneratorId: '=',
                    parentIdTab: '=',
                    subItem: '=?',
                    linkData: '=?',
                    windowTabInfo: '='
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.multiple.editing.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes) {
                            scope.inputsTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.inputs.html?v=' + FW_VERSION;
                            scope.processDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.dialog.html?v=' + FW_VERSION;
                            scope.translationDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.process.translation.dialog.html?v=' + FW_VERSION;
                            scope.editingWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.editing.window.html?v=' + FW_VERSION;
                            scope.searchWindowDialogTemplate = APP_EXTENSION_VINCO_CORE + 'common/views/autogen.form.editing.search.window.html?v=' + FW_VERSION;
                            scope.translationDialogId = 'translationModal' + scope.tabGeneratorId;
                            scope.processDialogId = 'processModal' + scope.tabGeneratorId;
                            scope.tableSearchDialogId = 'tableModal' + scope.tabGeneratorId;
                            scope.searchInputDialogId = 'searchModal' + scope.tabGeneratorId;
                            scope.parentIdTab = null;
                            scope.parentTab = null;
                            scope.fieldReadOnlyRuntime = [];
                            /**
                             * Properties to use to filter data request from server to obtain already linked elements to marked them in the table
                             * @type {Object}
                             */
                            scope.linkFilters = {};
                            scope.hasPermission = $rootScope.hasPermission;

                            /**
                             * Defines if the buttons should be placed in a separate row at the end of the fields, or if they should be included
                             * as part of the common bootstrap rendering logic.
                             * @type {boolean} True to use the default rendering logic of all the fields. False to place buttons at the end of the form
                             */
                            scope.includeButtonsInBootstrap = false;
                            scope.allItems = cacheService.getTabItems(scope.tabGeneratorId);

                            scope.$on(coreConfigService.events.NAVIGATION_RETURN_TO_LIST, function (event, data) {
                                if (data.idTab === scope.tabGeneratorId) {
                                    scope.throwCancelClick();
                                }
                            });

                            scope.$on(coreConfigService.events.NAVIGATION_CANCEL, function (event, data) {
                                if (data.idTab === scope.tabGeneratorId) {
                                    scope.throwCancelClick();
                                }
                            });

                            scope.$on(coreConfigService.events.NAVIGATION_APPLY, function (event, data) {
                                if (data.idTab === scope.tabGeneratorId) {
                                    scope.save();
                                }
                            });

                            scope.$on(app.eventDefinitions.GLOBAL_PROCESS_FINISHED, function (event, data) {
                                    if (data.tabGeneratorId && data.tabGeneratorId === scope.adTab.idTab) {
                                        var restPath = scope.adTab.table.module.restPath;
                                        if (onEvents && onEvents[restPath] && onEvents[restPath]['onGlobalProcessFinished']) {
                                            onEvents[restPath]['onGlobalProcessFinished'].call(this, data.idProcess, cacheService, $log, scope);
                                        }
                                    }
                                }
                            );

                            scope.parentItem = cacheService.getFormItem({
                                idWindow: scope.windowTabInfo.idWindow,
                                tablevel: scope.windowTabInfo.tablevel - 1
                            });

                            scope.htmlTemplates = null;
                            cacheService.loadHtmlTemplates().then(function(data){
                                scope.htmlTemplates = data;
                            }).catch(function(err){
                                scope.htmlTemplates = [];
                                $log.error(err);
                            });

                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            if (scope.parentIdTab) {
                                cacheService.loadTab(scope.parentIdTab).then(function (data) {
                                    scope.parentTab = data;
                                });
                            }

                            scope.fieldInitializations = function () {
                                scope.cols = 1;
                                scope.fields = [];
                                scope.primaryFields = [];
                                scope.groups = [];
                                scope.posField = [];
                                scope.fieldValue = {};
                                scope.service = null;
                                scope.allVisibleFields = [];
                                scope.allFields = [];
                                scope.item = {};
                                scope.windowGeneratorId = attributes.windowGeneratorId;
                                scope.complicateVisualFields = {};
                                scope.currentIdReferenceValue = null;
                                scope.currentColumnName = null;
                                scope.sortDirection = 'ASC';
                                scope.searchFilter = '';
                                scope.referenceData = null;
                            };

                            scope.fieldInitializations();

                            scope.translation = {};
                            scope.translationProperty = {
                                translationClientId: null,
                                translationInitClientId: null
                            };

                            // Sort order. False equals ASC, True equals DESC
                            scope.tableReverseSort = false;
                            // Order by field
                            scope.tableOrderByField = '';
                            // Page options
                            scope.tablePageSizeList = [10, 20, 40, 60, 100];
                            scope.referenceData = {
                                referencePageSize: 10,
                                referenceCurrentPage: 1,
                                currentFilterValue: ""
                            };
                            scope.maxSize = 5;

                            scope.filterName = function (item) {
                                if (item[scope.complicateVisualFields[scope.currentIdReferenceValue].display] && scope.currentFilterValue) {
                                    return item[scope.complicateVisualFields[scope.currentIdReferenceValue].display].toLowerCase().indexOf(scope.currentFilterValue.toLowerCase()) >= 0;
                                }
                            };

                            /**
                             * Back to previous window
                             */
                            scope.goBack = function () {
                                if (!scope.subItem) {
                                    $rootScope.$broadcast('cancel_edit_form', 1);
                                } else {
                                    $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, {
                                        tabGeneratorId: scope.tabGeneratorId,
                                        tablevel: scope.windowTabInfo.tablevel
                                    });
                                }
                            };

                            scope.throwCancelClick = function () {
                                cacheService.clearFormInfo(scope.tabGeneratorId);
                                if (!scope.subItem) {
                                    $rootScope.$broadcast('cancel_to_parent', 1);
                                } else {
                                    $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, {
                                        tabGeneratorId: scope.tabGeneratorId,
                                        tablevel: scope.windowTabInfo.tablevel
                                    });
                                }
                            };

                            /**
                             * Changes current grid page size
                             *
                             * @param pageSize Grid page size
                             */
                            scope.changeTablePage = function (pageSize) {
                                scope.referenceData.referenceCurrentPage = 1;
                                scope.scope.referenceData.referencePageSize = pageSize;
                            };

                            /**
                             * Executes a table sort
                             *
                             * @param item Sort details
                             */
                            scope.orderByFieldFn = function (item) {
                                console.log(item);
                                if (scope.tableOrderByField != commonService.removeUnderscores(item.name)) {
                                    scope.tableReverseSort = false;
                                } else {
                                    scope.tableReverseSort = !scope.tableReverseSort;
                                }
                                scope.tableOrderByField = commonService.removeUnderscores(item.name);
                                scope.loadSearchData(scope.currentIdReferenceValue);
                            };

                            /**
                             * Show/Hide a group of fields
                             *
                             * @param id Group field id.
                             */
                            scope.toggleGroup = function (id) {
                                var pos = scope.posGroup(scope.groups, id);
                                scope.groups[pos].collapsed = !scope.groups[pos].collapsed;
                            };

                            scope.selectTableValue = function (item) {
                                var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                                var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                                scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                                scope.fieldValue[scope.currentColumnName] = item[col];
                                scope.changeValue(scope.currentField);
                                $('#' + scope.tableSearchDialogId).modal('hide');
                            };

                            /**
                             * Show search selector dialog
                             *
                             * @param field Origin field
                             */
                            scope.showSearchWindow = function (field) {
                                scope.currentIdReferenceValue = field.column.idReferenceValue;
                                scope.currentColumnName = field.column.name;
                                scope.currentField = field;
                                scope.referenceData.referencePageSize = 10;
                                scope.referenceData.referenceCurrentPage = 1;
                                $('#searchModal').modal();
                            };

                            scope.selectSearchValue = function (item) {
                                var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                                var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                                scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                                scope.fieldValue[scope.currentColumnName] = item[commonService.removeUnderscores(col)];
                                scope.changeValue(scope.currentField);
                                $('#searchModal').modal('hide');
                            };

                            /**
                             * Hides the process dialog
                             */
                            scope.hideProcessDialog = function () {
                                $('#' + scope.processDialogId).modal('hide');
                            };

                            /**
                             * Find the primary key column name in the fields array
                             *
                             * @param fields
                             */
                            scope.getPrimaryKeyColumnName = function (fields) {
                                var columnName = null;
                                _.each(fields, function (current, index) {
                                    if (current.column.primaryKey) {
                                        columnName = current.column.name;
                                    }
                                });
                                return columnName;
                            };
                            /**
                             * Assembles the query
                             *
                             * @returns {String} Query
                             */
                            scope.getDependantQuery = function () {
                                if (!scope.adTab)
                                    return;
                                var table = cacheService.getTable(scope.adTab.idTable);
                                if (table) {
                                    var multipleColumns = _.filter(table.columns, function (column) {
                                            return column.primaryKey;
                                        }).length > 1;
                                    var linkColumns = _.filter(table.columns, function (column) {
                                        return column.linkParent || column.linkColumn != null;
                                    });
                                    _.each(linkColumns, function (linkColumn) {
                                        var parentColumn = "";
                                        if (linkColumn.linkColumn) {
                                            parentColumn = commonService.removeUnderscores(linkColumn.linkColumn);
                                        } else if (linkColumn.linkParent) {
                                            parentColumn = linkColumn.standardName;
                                        }
                                        if (parentColumn === "") {
                                            $log.error("Could not extract link column for: ", linkColumn);
                                        } else {
                                            var linkFilterName = linkColumn.standardName;
                                            if (linkColumn.linkColumn && multipleColumns && linkColumn.primaryKey) {
                                                linkFilterName = "id." + linkFilterName;
                                            }
                                            if (linkColumn.reference.rtype == 'TABLE' || linkColumn.reference.rtype == 'TABLEDIR' && scope.parentItem) {
                                                scope.linkFilters[linkFilterName] = scope.parentItem.item[parentColumn];
                                            } else if (linkColumn.reference.rtype == 'STRING' && (scope.tabInformation.ttype == 'IMAGE' || scope.tabInformation.idParentTable)) {
                                                if (scope.parentItem) {
                                                    var field = _.find(scope.parentItem.fields, function (field) {
                                                        return field.column.primaryKey;
                                                    });
                                                    if (field) {
                                                        scope.linkFilters[linkFilterName] = scope.parentItem.item[field.column.standardName];
                                                    } else {
                                                        $log.error("No field marked as primary key in parent item", linkColumn.idReference, table, scope.tabInformation);
                                                    }
                                                } else {
                                                    $log.error("This grid should contain a related form and it doesn't", linkColumn.idReference, table, scope.tabInformation);
                                                }
                                            } else {
                                                commonService.showRejectNotification($rootScope.getMessage('AD_errDataColumnLinkedInvalid'));
                                                $log.error("There are no linked table references with the following filter: idReference=" + linkColumn.idReference, table, scope.tabInformation);
                                            }
                                        }
                                    });
                                    // scope.loadTabInformation();
                                } else {
                                    $log.error("Could not load table information from cache" + scope.tabInformation.idTable);
                                }
                                var query = "";
                                // Add link column filter
                                var linkedFilter = _.keys(scope.linkFilters);
                                _.each(linkedFilter, function (current) {
                                    if (scope.linkFilters[current] && scope.linkFilters[current] != "") {
                                        query += (current + '=%' + scope.linkFilters[current] + "%,");
                                    }
                                });
                                return query;
                            };
                            /**
                             * Load the row to edit or create and prepared the service
                             */
                            scope.reloadBusinessData = function () {
                                var defered = $q.defer();
                                var promise = defered.promise;
                                scope.initialLoading = true;

                                function finishLoading() {
                                    scope.checkLinkColumns();
                                    scope.initialLoading = false;
                                    _.each(scope.allFields, function (current) {
                                        scope.fieldValue['old_' + current.standardName] = null;
                                        if (current.column.reference.rtype == 'INTEGER' && scope.fieldValue[current.standardName]) {
                                            scope.fieldValue[current.standardName] = Number(scope.fieldValue[current.standardName]);
                                        }
                                        scope.changeValue(current);
                                    });
                                    scope.checkPasswordField();
                                }

                                // Initializing field values
                                _.each(scope.allFields, function (current) {

                                    if (current.column && current.column.valuedefault) {
                                        // If the column is id_client
                                        if (scope.parentItem && current.column.idColumn == "ff808081526d9f5c01526dcc3572005f") {
                                            scope.fieldValue[current.column.name] = null;
                                            return;
                                        }
                                        var defValue = cacheService.replaceJSConditional(current, '', current.column.valuedefault);
                                        switch (current.column.ctype) {
                                            case 'INTEGER':
                                            case 'NUMBER':
                                                scope.fieldValue[current.column.name] = parseFloat(current.column.valuedefault);
                                                break;
                                            case 'BOOLEAN':
                                                scope.fieldValue[current.column.name] = eval(defValue);
                                                break;
                                            case 'DATE':
                                                scope.fieldValue[current.column.name] = moment(defValue);
                                                break;
                                            default:
                                                scope.fieldValue[current.column.name] = defValue;
                                        }
                                    } else {
                                        scope.fieldValue[current.column.name] = null;
                                    }
                                });

                                // Checking if there is any field in the parent that should be replaced in the sql where of the child fields
                                if (scope.parentItem) {
                                    _.each(scope.allFields, function (field) {
                                        _.each(scope.parentItem.fields, function (current) {
                                            if (field.column.reference.rtype === 'TABLEDIR') {
                                                if (!field.column.referenceValue) {
                                                    field.column.referenceValue = cacheService.getReference(field.column.idReferenceValue);
                                                }
                                                if (field.column.referenceValue.info.sqlwhere && cacheService.isFieldInConditional(current, field.column.referenceValue.info.sqlwhere)) {
                                                    if (field.runtime.sqlwhere === undefined || !cacheService.isCompleteConditional(field.runtime.sqlwhere)) {
                                                        field.runtime.sqlwhere = field.column.referenceValue.info.sqlwhere;
                                                        field.runtime.sqlwhere = cacheService.replaceSQLConditional(current, scope.parentItem.item[current.column.standardName], field.runtime.sqlwhere);
                                                    }
                                                }
                                            }
                                        });
                                    });
                                }
                                scope.service = null;
                                try {
                                    scope.service = autogenService.getAutogenService(scope.adTab.table);
                                } catch (e) {
                                    // TODO: Error handling
                                    $log.error(e);
                                    scope.initialLoading = false;
                                    defered.reject();
                                }
                                if (scope.service) {
                                    finishLoading();
                                    defered.resolve();
                                }
                                return promise;
                            };

                            /**
                             * Initializes Password field value to an empty string.
                             * This method will make sense only when editing an user, hence the need to check
                             * if we are editing the tab that matches the user editing tab form
                             */
                            scope.checkPasswordField = function () {
                                _.each(scope.allFields, function (field) {
                                    if (field.column.reference.rtype !== 'PASSWORD')
                                        return;
                                    scope.fieldValue[field.column.standardName] = null;
                                    field.column.mandatory = !scope.isEditing;
                                });
                            };

                            /**
                             * Sets the sending password as an md5 hash.
                             * This method will make sense only when editing an user, hence the need to check
                             * if we are editing the tab that matches the user editing tab form
                             */
                            scope.checkPasswordToSend = function () {
                                _.each(scope.allFields, function (field) {
                                    if (field.column.reference.rtype !== 'PASSWORD')
                                        return;
                                    if (scope.item[field.column.standardName] && scope.item[field.column.standardName] !== "")
                                        scope.item[field.column.standardName] = hex_md5(scope.item[field.column.standardName]);
                                });
                            };

                            /**
                             * Checks the fields marked as "UsedInChild" in the parent
                             * and the fields marked as "LinkParent" in the child
                             */
                            scope.checkLinkColumns = function () {
                                for (var i = 0; i < scope.windowTabInfo.tablevel; i++) {
                                    var parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel - (i + 1)
                                    });
                                    if (parentItem) {
                                        _.each(scope.allFields, function (field) {
                                            if (field.column && field.column.linkParent && parentItem.item[field.column.standardName]) {
                                                var parent = parentItem;
                                                cacheService.loadReferenceData(field.column.idReferenceValue).then(function () {
                                                    var ref = cacheService.getReference(field.column.idReferenceValue);
                                                    var foundField = _.find(parent.fields, function (fieldData) {
                                                        return fieldData.idColumn === ref.info.idDisplay;
                                                    });
                                                    if (foundField) {
                                                        scope.formName = parent.item[foundField.column.standardName];
                                                    }
                                                })
                                            }
                                        });
                                        // Checking "linkParent" restriction only when this is a new Item
                                        if (!scope.isEditing) {
                                            _.each(scope.allFields, function (field) {
                                                if (field.column && field.column.linkParent && parentItem.item[field.column.standardName]) {
                                                    scope.fieldValue[field.column.name] = parentItem.item[field.column.standardName];
                                                    scope.changeValue(field);
                                                }
                                            });
                                        }
                                    }
                                }
                                scope.checkLinkColumnValues();
                            };

                            /**
                             * Checks and assigns the values of the parent item's field that are marekd
                             * as "useInChild"
                             *
                             * @param ignoreChange If true, it won't trigger a change check cycle
                             */
                            scope.checkLinkColumnValues = function (ignoreChange) {
                                var parentItem;
                                for (var i = 0; i < scope.windowTabInfo.tablevel; i++) {
                                    parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel - (i + 1)
                                    });
                                    if (parentItem) {
                                        _.each(parentItem.fields, function (field) {
                                            if (field.usedInChild && !scope.isEditing) {
                                                var any = _.find(scope.allFields, function (current) {
                                                    return current.column.name == field.column.name;
                                                });
                                                if (any) {
                                                    scope.fieldValue[any.column.name] = parentItem.item[any.column.standardName];
                                                    scope.item[any.column.standardName] = parentItem.item[any.column.standardName];
                                                    if (!ignoreChange)
                                                        scope.changeValue(any);
                                                } else {
                                                    scope.allFields.push({
                                                        column: field.column,
                                                        display: false,
                                                        displayed: false,
                                                        standardName: field.standardName
                                                    });
                                                    scope.fieldValue[field.column.name] = parentItem.item[field.column.standardName];
                                                    scope.item[field.column.standardName] = parentItem.item[field.column.standardName];
                                                    if (!ignoreChange)
                                                        scope.changeValue(field);
                                                }
                                            }
                                        });
                                    }
                                }

                                parentItem = cacheService.getFormItem({
                                    idWindow: scope.windowTabInfo.idWindow,
                                    tablevel: scope.windowTabInfo.tablevel - 1
                                });
                                var tableField = _.find(scope.allFields, function (current) {
                                    return current.column.standardName === "idTable";
                                });
                                var idRowField = _.find(scope.allFields, function (current) {
                                    return current.column.standardName === "idRow";
                                });
                                if (parentItem) {
                                    if (scope.adTab.idParentTable && tableField) {
                                        scope.fieldValue["id_table"] = parentItem.tab.idTable;
                                        scope.item["idTable"] = parentItem.tab.idTable;
                                    }
                                    if (idRowField) {
                                        scope.fieldValue["id_row"] = parentItem.item.id;
                                        scope.item["idRow"] = parentItem.item.id;
                                    }
                                }
                            };

                            /**
                             * Prepare form data to send
                             *
                             * @returns {boolean}
                             */
                            scope.prepareToSend = function () {
                                _.each(scope.allFields, function (current) {
                                    var columName = current.column.standardName;
                                    scope.item[columName] = scope.fieldValue[current.column.name];
                                    if (scope.item[columName] === undefined || scope.item[columName] === "") {
                                        scope.item[columName] = null;
                                    }
                                    if (current.column.ctype === 'DATE' && !moment(scope.item[columName]).isValid() && scope.item[columName] !== null) {
                                        scope.item[columName] = new moment(scope.item[columName], "DD/MM/YYYY");
                                    }
                                    if ((scope.item[columName]) && (current.column.reference.rtype === 'TABLE' || current.column.reference.rtype === 'SEARCH')) {
                                        scope.item['DISPLAY_' + columName] = scope.item[columName];
                                        if (_.has(scope.fieldValue, 'KEY_' + current.column.name)) {
                                            scope.item[columName] = scope.fieldValue['KEY_' + current.column.name];
                                        }
                                    }
                                    if (current.column.reference.rtype === 'YESNO' && !(scope.item[columName])) {
                                        scope.item[columName] = false;
                                    }

                                    if (current.column.primaryKey) {
                                        scope.item[columName] = scope.item.id;
                                    }
                                    if (!scope.isEditing && current.column.primaryKey) {
                                        scope.item[columName] = null;
                                    }
                                });
                                if (scope.adTab.idParentTable) {
                                    var parentTab = _.find(scope.associatedWindow.tabs, function (current) {
                                        return current.tab.idTable === scope.adTab.idParentTable;
                                    });
                                    if (parentTab) {
                                        scope.item['idTable'] = parentTab.tab.idTable;
                                    } else {
                                        scope.item['idTable'] = scope.adTab.idTable;
                                    }
                                }

                                // Load FK
                                var result = cacheService.getPrimaryKey();
                                _.each(result, function (current) {
                                    if (!scope.item[current.name]) {
                                        scope.item[current.name] = current.value;
                                    }
                                });
                                if (scope.linkData) {
                                    var keys = _.keys(scope.linkData);
                                    _.each(keys, function (current) {
                                        scope.item[current] = scope.linkData[current];
                                    });
                                }

                                if ((scope.parentTab && (scope.parentTab.ttype === 'IMAGE' || scope.parentTab.ttype === 'FILE') && parentItem) ||
                                    scope.adTab.idParentTable) {
                                    scope.item.idTable = parentItem.tab.idTable;
                                }
                                scope.checkPasswordToSend();
                            };

                            /**
                             * Restore TABLE and SEARCH values
                             */
                            scope.restoreValues = function () {
                                _.each(scope.allVisibleFields, function (current) {
                                    var columName = current.column.standardName;
                                    if ((scope.item[columName]) && (current.column.reference.rtype === 'TABLE' || current.column.reference.rtype === 'SEARCH')) {
                                        scope.item[columName] = scope.item['DISPLAY_' + columName];
                                    }
                                });
                            };

                            /**
                             * Persist data into the table
                             */
                            scope.save = function () {
                                scope.tableForm.$dirty = true;
                                if (scope.tableForm.$valid && scope.service) {
                                    scope.prepareToSend();
                                    var containsFiles = false;
                                    // Checking if any field was generated as <input type="file"/>
                                    _.each(scope.allFields, function (field) {
                                        if (field.column.reference && (field.column.reference.rtype === "IMAGE" || field.column.reference.rtype === "FILE")) {
                                            containsFiles = true;
                                        }
                                    });

                                    _.each(scope.allFields, function (current) {
                                        if (current.column.primaryKey) {
                                            scope.item[current.column.standardName] = null;
                                        }
                                    });

                                    var parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel - 1
                                    });
                                    if (!scope.item.idClient) {
                                        if (parentItem) {
                                            scope.item.idClient = parentItem.item.idClient;
                                        } else {
                                            scope.item.idClient = cacheService.loggedIdClient();
                                        }
                                    }
                                    // Create the row
                                    if (scope.adTab.table.name === 'ad_table') {
                                        scope.item.idTable = null;
                                    }
                                    if (scope.adTab.table.name === 'ad_client') {
                                        scope.item.idClient = null;
                                    }
                                    if (scope.adTab.table.name === 'ad_module') {
                                        scope.item.idModule = null;
                                    }

                                    var methodCreate = scope.service.saveMultiple;
                                    // if (containsFiles) {
                                    //     methodCreate = scope.service.createFile;
                                    // }

                                    scope.item.ids = [];
                                    _.each(scope.multipleItems, function (current) {
                                        if (current.selected) {
                                            scope.item.ids.push(current.id);
                                        }
                                    });
                                    methodCreate(scope.item,
                                        function () {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, {
                                                tabGeneratorId: scope.tabGeneratorId,
                                                tablevel: scope.windowTabInfo.tablevel
                                            });
                                        }, function (err) {
                                            $log.error('Status: ' + err.status + ', Error: ' + err.statusText);
                                            scope.restoreValues();
                                            $rootScope.showValidationsErrors(err, scope.allFields);
                                        }
                                    );

                                } else {
                                    commonService.showRejectNotification($rootScope.getMessage('AD_msgErrorCheckFields'));
                                }
                            };

                            /**
                             * Performs a refresh of the readonly property
                             */
                            scope.notifyItemSaved = function () {
                                scope.fieldReadOnlyRuntime = [];
                                _.each(scope.allFields, function (current) {
                                    if (current.column) {
                                        if (current.column.readonlylogic) {
                                            scope.fieldReadOnlyRuntime[current.name] = scope.$eval(current.column.readonlylogic) !== undefined
                                                && scope.$eval(current.column.readonlylogic) !== false
                                                && scope.$eval(current.column.readonlylogic) !== 0;
                                        }
                                        if (current.displaylogic) {
                                            if (!current.runtime.displaylogic) {
                                                current.runtime.displaylogic = current.displaylogic;
                                            }
                                            current.runtime.displaylogic = cacheService.replaceJSConditional(null, null, current.runtime.displaylogic);
                                            if (cacheService.isCompleteConditional(current.runtime.displaylogic)) {
                                                current.display = eval(current.runtime.displaylogic);
                                                current.runtime.displaylogic = current.displaylogic;
                                            }
                                        }
                                    }
                                });
                            };

                            /**
                             * Find the position of group in the group array
                             * @param groups
                             * @param idGroup
                             */
                            scope.posGroup = function (groups, idGroup) {
                                var outIndex = -1;
                                _.each(groups, function (current, index) {
                                    if (current.id === idGroup) {
                                        outIndex = index;
                                    }
                                });
                                return outIndex;
                            };

                            scope.filterSearchData = function () {
                                scope.loadSearchData(scope.currentIdReferenceValue);
                            };

                            scope.loadSearchData = function (idReferenceValue) {
                                var table = scope.complicateVisualFields[idReferenceValue].table;
                                var _service = null;
                                try {
                                    _service = autogenService.getAutogenService(table);
                                } catch (e) {
                                    $log.error(e);
                                }
                                if (_service) {
                                    var sort = '';
                                    if (scope.tableOrderByField !== '') {
                                        sort = '[{ "property":"' + scope.tableOrderByField + '","direction":"' + (scope.tableReverseSort ? 'DESC' : 'ASC') + '" }]';
                                    }
                                    // Checking filters
                                    var query = "";
                                    if (scope.complicateVisualFields[idReferenceValue].columns) {
                                        _.each(scope.complicateVisualFields[idReferenceValue].columns, function (current) {
                                            if (scope.complicateVisualFields[idReferenceValue].filterValues[current.name]) {
                                                query += (current.name + '=%' + scope.complicateVisualFields[idReferenceValue].filterValues[current.name] + "%,");
                                            }
                                        });
                                    } else {
                                        query += scope.searchFilter;
                                    }

                                    // Loading data
                                    $log.info("Requesting data from server for table: " + table.name + " with query: " + query);
                                    _service.query(
                                        {
                                            idClient: cacheService.loggedIdClient(),
                                            q: query,
                                            sort: sort,
                                            page: page,
                                            limit: scope.referenceData.referencePageSize
                                        },
                                        function (data4) {
                                            //Armando los datos en el scope
                                            scope.complicateVisualFields[idReferenceValue].objectValues = data4.content;
                                            scope.referenceTotalElements = data4.totalElements;
                                        },
                                        function (err) {
                                            $log.error(err);
                                        }
                                    );
                                }
                            };

                            /**
                             * Notify a field of change on other
                             *
                             * @param field Field notified
                             * @param changedField Change field
                             */
                            scope.notifyFieldChange = function (field, changedField) {
                                if (field.displaylogic) {
                                    if (!field.runtime.displaylogic) {
                                        field.runtime.displaylogic = field.displaylogic;
                                    }
                                    field.runtime.displaylogic = cacheService.replaceJSConditional(changedField, scope.fieldValue[changedField.column.name], field.runtime.displaylogic);
                                    if (!cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                                        _.each(scope.allFields, function (current) {
                                            field.runtime.displaylogic = cacheService.replaceJSConditional(current, scope.fieldValue[current.column.name], field.runtime.displaylogic);
                                        });
                                    }
                                    if (cacheService.isCompleteConditional(field.runtime.displaylogic)) {
                                        field.display = eval(field.runtime.displaylogic);
                                        field.runtime.displaylogic = field.displaylogic;
                                    }
                                }
                                if (field.column.reference.rtype === 'TABLEDIR') {
                                    if (!field.column.referenceValue) {
                                        field.column.referenceValue = cacheService.getReference(field.column.idReferenceValue);
                                    }
                                    if (field.column.referenceValue.info.sqlwhere && cacheService.isFieldInConditional(changedField, field.column.referenceValue.info.sqlwhere)) {
                                        field.runtime.sqlwhere = field.column.referenceValue.info.sqlwhere;
                                        if (!field.runtime.sqlwhere ||
                                            field.runtime.sqlwhere !== field.column.referenceValue.info.sqlwhere) {
                                            field.runtime.sqlwhere = field.column.referenceValue.info.sqlwhere;
                                        }
                                        var value = scope.fieldValue[changedField.column.name];
                                        if (changedField.column.idReferenceValue && (changedField.column.reference.rtype === 'TABLE' || changedField.column.reference.rtype === 'SEARCH')) {
                                            if (scope.fieldValue['KEY_' + changedField.column.name]) {
                                                value = scope.fieldValue['KEY_' + changedField.column.name];
                                            }
                                        }
                                        if (value) {
                                            field.runtime.sqlwhere = cacheService.replaceSQLConditional(changedField, value, field.runtime.sqlwhere);
                                        }
                                    }
                                }
                            };

                            /**
                             * Notify a change value on a field to row elements
                             *
                             * @param fieldRows Row of fields
                             * @param field Change field
                             */
                            scope.changeValueRow = function (fieldRows, field) {
                                $rootScope.$broadcast(app.eventDefinitions.TAB_VISUALIZATION_NOTIFICATION, {
                                    idWindow: scope.windowTabInfo.idWindow,
                                    field: field,
                                    value: scope.fieldValue[field.column.name]
                                });
                                _.each(fieldRows, function (fRow) {
                                    _.each(fRow.fields, function (f) {
                                        if (f.standardName !== field.standardName) {
                                            scope.notifyFieldChange(f, field);
                                        }
                                    });
                                });
                                _.each(scope.groups, function (group) {
                                    scope.notifyFieldGroupChange(group, field);
                                });
                            };

                            /**
                             * Notify a group of change on a field
                             *
                             * @param group Group notified
                             * @param changedField Changed field
                             */
                            scope.notifyFieldGroupChange = function (group, changedField) {
                                if (group.displaylogic) {
                                    if (!group.runtime)
                                        group.runtime = {};
                                    if (!group.runtime.displaylogic) {
                                        group.runtime.displaylogic = group.displaylogic;
                                    }
                                    group.runtime.displaylogic = cacheService.replaceJSConditional(changedField, scope.fieldValue[changedField.column.name], group.runtime.displaylogic);
                                    if (cacheService.isCompleteConditional(group.runtime.displaylogic)) {
                                        group.display = eval(group.runtime.displaylogic);
                                        group.runtime.displaylogic = group.displaylogic;
                                    }
                                }
                            };

                            /**
                             * Change value of field
                             *
                             * @param value Value being changed
                             * @param field Field information
                             */
                            scope.changeFile = function (value, field) {
                                this.parentNode.nextSibling.value = this.value;
                                scope.changeValue(field);
                            };

                            /**
                             * Change value of field
                             *
                             * @param field Field information
                             */
                            scope.changeValue = function (field) {
                                scope.changeValueRow(scope.fieldRows, field);
                                _.each(scope.groups, function (group) {
                                    scope.changeValueRow(group.fieldRows, field);
                                });
                                var value = scope.fieldValue[field.name];
                                if ((field.rtype === 'TABLE' || field.rtype === 'SEARCH') && scope.fieldValue['KEY_' + field.name]) {
                                    value = scope.fieldValue['KEY_' + field.name];
                                }
                                if (field.onchangefunction && !scope.initialLoading && scope.fieldValue['old_' + field.standardName] !== value) {
                                    var table = cacheService.getTableByName(field.tableName);
                                    if (table && onChangeFunction) {
                                        onChangeFunction[table.module.restPath][field.onchangefunction].call(this, scope, $log, cacheService, autogenService, commonService);
                                    }
                                }
                                scope.fieldValue['old_' + field.standardName] = value;
                                scope.persistFormData();
                            };

                            scope.setFieldValue = function (name, value) {
                                var field = _.find(scope.allFields, function (fld) {
                                    return fld.standardName === name;
                                });
                                if (field) {
                                    scope.fieldValue[field.name] = value;
                                    scope.item[name] = value;
                                    scope.changeValue(field);
                                }
                            };

                            /**
                             * Persists the form information in cache for tab changing purposes
                             */
                            scope.persistFormData = function () {
                                cacheService.storeFormInfo(scope.tabGeneratorId, scope.item, scope.fieldValue, scope.adTab);
                            };

                            scope.loadFieldValues = function (fieldReferenceValueId, type, result) {
                                if (scope.complicateVisualFields[fieldReferenceValueId]) {
                                    scope.complicateVisualFields[fieldReferenceValueId].key = result.colKey.standardName;
                                    scope.complicateVisualFields[fieldReferenceValueId].display = result.colDisplay.standardName;
                                    scope.complicateVisualFields[fieldReferenceValueId].table = result.table;
                                    scope.complicateVisualFields[fieldReferenceValueId].sqlwhere = result.sqlwhere;
                                    scope.complicateVisualFields[fieldReferenceValueId].sqlorderby = result.sqlorderby;
                                    scope.complicateVisualFields[fieldReferenceValueId].objectValues = result.data.content;
                                    scope.complicateVisualFields[fieldReferenceValueId].displayCaption = result.colDisplay.name;

                                    scope.referenceTotalElements = result.data.totalElements;
                                    if (scope.complicateVisualFields[fieldReferenceValueId].type === 'SEARCH') {
                                        var tValue = _.find(result.data.content, function (element) {
                                            var a = element[scope.complicateVisualFields[fieldReferenceValueId].key];
                                            var b = scope.fieldValue[scope.complicateVisualFields[fieldReferenceValueId].columnName];
                                            return a === b;
                                        });
                                        if (tValue) {
                                            scope.fieldValue['KEY_' + scope.complicateVisualFields[fieldReferenceValueId].columnName] = tValue[scope.complicateVisualFields[fieldReferenceValueId].key];
                                            scope.fieldValue[scope.complicateVisualFields[fieldReferenceValueId].columnName] = tValue[scope.complicateVisualFields[fieldReferenceValueId].display];
                                        }
                                    }
                                }
                            };

                            scope.reloadSelectedReferences = function () {
                                if (!scope.adTab)
                                    return;
                                scope.multipleItems = [];
                                if (scope.multiselectField.column.reference.rtype === "TABLE" ||
                                    scope.multiselectField.column.reference.rtype === "TABLEDIR" ||
                                    scope.multiselectField.column.reference.rtype === "SEARCH") {
                                    var parentKeyField = _.find(scope.parentItem.fields, function (current) {
                                        return current.column.primaryKey;
                                    });
                                    if (!parentKeyField) {
                                        $log.error("No primary key defined for the parent item");
                                    }
                                    adFieldService.multiselect({
                                        idClient: cacheService.loggedIdClient(),
                                        id: scope.multiselectField.idField,
                                        parentFieldId: parentKeyField.idField,
                                        rowKey: scope.parentItem.item[parentKeyField.column.standardName]
                                    }, function (data) {
                                        scope.multipleItems = data.content;
                                        scope.multipleItems = data.content;
                                        scope.multipleTotalElements = data.content.length;
                                    }, function (error) {
                                        $log.error(error);
                                    });
                                }
                                else {
                                    $log.error("Not supported reference " + scope.multiselectField.column.reference.rtype + ". Can not perform link in multiple select.")
                                }
                            };


                            /**
                             * Performs the CheckAll/UncheckAll logic
                             *
                             * @param checkAll CheckAll Status
                             */
                            scope.verifySelection = function (checkAll) {
                                scope.checkAll = checkAll;
                                _.each(scope.multipleItems, function (current) {
                                    current.selected = checkAll;
                                });
                            };

                            /**
                             * Checks the item and deselects all the other items
                             *
                             * @param item Item to check
                             */
                            scope.checkItem = function (item) {
                                _.each(scope.multipleItems, function (current) {
                                    current.selected = false;
                                });
                                item.selected = true;
                                scope.checkAll = false;
                            };

                            /**
                             * Updates the CheckAll status if any of the elments is deselected
                             */
                            scope.updateCheckAll = function () {
                                scope.checkAll = !_.some(scope.multipleItems, function (current) {
                                    return !current.selected;
                                })
                            };

                            /**
                             * Loads and builds form structure
                             */
                            scope.loadForm = function () {
                                if (scope.tabGeneratorId !== undefined && scope.tabGeneratorId !== "") {
                                    var loadTime = Date.now();
                                    cacheService.loadRelatedMultiselectTab(scope.tabGeneratorId).then(function (tab) {
                                        return tab;
                                    }).then(function (tab) {
                                        return cacheService.loadTab(tab.idTab)
                                    }).then(function (data) {
                                        scope.adTab = data;
                                        if (scope.adTab.runtime === undefined)
                                            scope.adTab.runtime = {readonly: false};
                                        scope.adTab.runtime.readonly = (scope.adTab.runtime.readonly || scope.adTab.uipattern === 'READONLY' || scope.forceReadOnly);
                                        return scope.adTab.idWindow
                                    }).then(function () {
                                        return cacheService.loadWindow(scope.adTab.idWindow)
                                    }).then(function (data) {
                                        scope.associatedWindow = data;
                                        scope.formName = data.name;
                                        scope.adTab.table = cacheService.getTable(scope.adTab.idTable);
                                        scope.allFields = _.sortBy(scope.adTab.fields, function (current) {
                                            return current.seqno;
                                        });
                                        _.each(scope.allFields, function (current) {
                                            current.tableName = scope.adTab.table.name;
                                            current.display = true;
                                            current.column = _.find(scope.adTab.table.columns, function (col) {
                                                return col.idColumn === current.idColumn;
                                            });
                                        });
                                        scope.checkLinkColumnValues(true);

                                        var visible = _.filter(scope.allFields, function (current) {
                                            return current.displayed;
                                        });

                                        scope.multiselectField = _.find(visible, function (current) {
                                            return current.multiSelect;
                                        });
                                        if (_.filter(visible, function (current) {
                                                return current.multiSelect;
                                            }).length > 1) {
                                            $log.error("More than one field is marked as multiselect for tab: " + scope.adTab.idTab);
                                            return;
                                        }

                                        visible = _.filter(visible, function (current) {
                                            return !current.multiSelect;
                                        });
                                        // Get defined buttons
                                        _.each(visible, function (field) {
                                            if (field.column) {
                                                if (field.column.reference.rtype === 'BUTTON') {
                                                    field.column.refButton = cacheService.getReference(field.column.idReferenceValue);
                                                    if (field.column.refButton.info.showMode !== 'IN_FORM') {
                                                        field.ignoreButton = true;
                                                    }
                                                }
                                            }
                                        });
                                        // Excluding buttons where SHOW_MODE != IN_FORM
                                        visible = _.filter(visible, function (field) {
                                            return !field.ignoreButton;
                                        });

                                        _.each(visible, function (current, index) {
                                            scope.allVisibleFields.push(current);
                                            if (current.column) {
                                                scope.posField.push(index);

                                                if (current.column.primaryKey) {
                                                    scope.primaryFields.push(current);
                                                } else if (!current.idFieldGroup) {
                                                    current.bootstrapCol = (12 / scope.adTab.columns) * current.span;
                                                    scope.fields.push(current);
                                                } else {
                                                    if (scope.posGroup(scope.groups, current.idFieldGroup) === -1) {
                                                        var group = {
                                                            name: current.name,
                                                            id: current.idFieldGroup,
                                                            fields: [],
                                                            fieldRows: []
                                                        };
                                                        group.fields.push(current);
                                                        scope.groups.push(group);
                                                    } else {
                                                        var pos = scope.posGroup(scope.groups, current.idFieldGroup);
                                                        scope.groups[pos].fields.push(current);
                                                    }
                                                }
                                                if (current.column.reference.rtype === 'SEARCH') {
                                                    // Initializing structure to store select list values
                                                    scope.complicateVisualFields[current.column.idReferenceValue] = {
                                                        fieldReferenceValueId: current.column.idReferenceValue,
                                                        type: current.column.reference.rtype,
                                                        key: null,
                                                        display: null,
                                                        columnName: current.column.name,
                                                        objectValues: []
                                                    };
                                                }
                                            }
                                        });

                                        // Loading the group name for each group
                                        _.each(scope.groups, function (current) {
                                            var fieldGroup = cacheService.getFieldGroup(current.id);
                                            var pos = scope.posGroup(scope.groups, current.id);
                                            scope.groups[pos].name = fieldGroup.name;
                                            scope.groups[pos].display = true;
                                            scope.groups[pos].columns = fieldGroup.columns;
                                            scope.groups[pos].collapsed = fieldGroup.collapsed;
                                            scope.groups[pos].displaylogic = fieldGroup.displaylogic;
                                            _.each(scope.groups[pos].fields, function (current) {
                                                current.bootstrapGroupColumns = (12 / (fieldGroup.columns ? fieldGroup.columns : scope.adTab.columns)) * current.span;
                                            });
                                        });

                                        // Here we define the rows according to the rules.
                                        // A new row is started if any of the following applies:
                                        //      1. If it is the first element
                                        //      2. If the field explicitly states it starts a new row
                                        //      3. If there is no more space left in the row
                                        scope.fieldRows = [];
                                        var currentRow;
                                        var availableSpace = 12;
                                        scope.fields = _.sortBy(scope.fields, function (current) {
                                            return current.seqno;
                                        });
                                        _.each(scope.fields, function (current, index) {
                                            var newRow = {
                                                fields: []
                                            };
                                            var neededSpace = ((12 / scope.adTab.columns) * current.span);
                                            if (index === 0 || current.startnewrow || availableSpace < neededSpace) {
                                                currentRow = newRow;
                                                currentRow.fields.push(current);
                                                scope.fieldRows.push(currentRow);
                                                availableSpace = 12 - neededSpace;
                                            } else {
                                                currentRow.fields.push(current);
                                                availableSpace -= neededSpace;
                                            }
                                        });

                                        // Organizing group field to show by row
                                        _.each(scope.groups, function (currentG) {
                                            var currentRowG;
                                            var columnSpaceG = 12;
                                            _.each(currentG.fields, function (currentF, indexF) {
                                                var newRow = {
                                                    fields: []
                                                };
                                                var needcolumnSpaceG = ((12 / (currentG.columns ? currentG.columns : scope.adTab.columns)) * currentF.span);
                                                if (indexF === 0 || currentF.startnewrow || columnSpaceG < needcolumnSpaceG) {
                                                    currentRowG = newRow;
                                                    currentRowG.fields.push(currentF);
                                                    currentG.fieldRows.push(currentRowG);
                                                    columnSpaceG = 12 - needcolumnSpaceG;
                                                } else {
                                                    currentRowG.fields.push(currentF);
                                                    columnSpaceG -= needcolumnSpaceG;
                                                }
                                            });
                                            currentG.fields = null;
                                        });

                                        // Loading data or business row
                                        scope.reloadBusinessData().then(function () {
                                            scope.reloadSelectedReferences();
                                            $log.debug("loadForm in: " + (Date.now() - loadTime));
                                        }).catch(function (err) {
                                            $log.error(err);
                                        });
                                    }).catch(function (err) {
                                        $log.error(err);
                                    });
                                }
                                else {
                                    // TODO: Error handling
                                }
                            };


                            scope.getBootrapWidth = function (field, item) {
                                if (field.column.translatable && item[field.standardName] && item.id) {
                                    return 7;
                                }
                                if (field.column.translatable || (item[field.standardName] && item.id)) {
                                    return 9;
                                }
                                return 12;
                            };

                            scope.deleteImage = function (field, item) {
                                scope.service.deleteImage({
                                    idClient: cacheService.loggedIdClient(),
                                    id: item.id,
                                    fieldName: field.name,
                                    file: item[field.standardName]
                                }, function () {
                                    scope.fieldValue[field.column.name] = null;
                                    scope.item[field.standardName] = null;
                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));

                                }, function (error) {
                                    $log.error('Status: ' + error.status + ', Error: ' + error.statusText);
                                    commonService.showRejectNotification($rootScope.getMessage('AD_msgDataDeleteError'));
                                });

                            };
                            /**
                             * Shows translation dialog
                             *
                             * @param field Field to be translated
                             */
                            scope.showTranslation = function (field) {
                                scope.translationHasSystem = _.some(Authentication.getLoginData().roles, function (current) {
                                    return (current.name === "ROL_SYSTEM") || (current.name === "ROLE_SUPERADMIN");
                                });
                                scope.translationClients = [];
                                if (!scope.translationHasSystem) {
                                    scope.translationProperty.translationClientId = cacheService.loggedIdClient();
                                    scope.translationProperty.translationInitClientId = cacheService.loggedIdClient();
                                }
                                scope.translationInfos = {};
                                scope.translatableField = field;

                                scope.loadTranslations(cacheService.loggedIdClient()).then(function () {
                                    $('#' + scope.translationDialogId).modal();
                                }).catch(function (err) {
                                    $log.error(err);
                                });
                            };

                            scope.loadTranslations = function (idClient) {
                                var defered = $q.defer();
                                var promise = defered.promise;
                                var thatScope = scope;

                                scope.translationInfos = {};
                                adTranslationService.query({
                                    idClient: idClient,
                                    q: "rowkey=" + scope.item.id +
                                    ",idTable=" + scope.adTab.table.idTable +
                                    ",idColumn=" + scope.translatableField.column.idColumn
                                }, function (data) {
                                    scope.translation[scope.translatableField.idField] = {};
                                    _.each($rootScope.languages, function (language) {
                                        _.each(data.content, function (translation) {
                                            if (translation.idLanguage === language.idLanguage
                                                && translation.idClient === idClient) {
                                                scope.translationInfos[language.countrycode] = translation;
                                                scope.translation[scope.translatableField.idField][language.countrycode] = translation.translation;
                                            }
                                        });
                                    });
                                    if (scope.translationProperty.translationClientId !== idClient
                                        || scope.translationProperty.translationClientId === null) {
                                        $timeout(function () {
                                            scope.translationProperty.translationClientId = idClient;
                                            scope.translationProperty.translationInitClientId = idClient;
                                            $log.info('Client ID: ' + thatScope.translationProperty.translationClientId)
                                        }, 0, true);
                                    }
                                    defered.resolve();
                                }, function (err) {
                                    defered.reject(err);
                                });
                                return promise;
                            };

                            /**
                             * Hides translation dialog
                             */
                            scope.cancelTranslation = function () {
                                $('#' + scope.translationDialogId).modal('hide');
                            };

                            /**
                             * Stores translations
                             *
                             * @param field Field to be translated
                             * @param language Language to obtain the translation
                             */
                            scope.deleteTranslation = function (field, language) {
                                $.SmartMessageBox({
                                    title: $rootScope.getMessage('AD_msgConfirmDelete'),
                                    buttons: '[' + $rootScope.getMessage('AD_labelNo') + '][' + $rootScope.getMessage('AD_labelYes') + ']'
                                }, function (ButtonPressed) {
                                    if (ButtonPressed === $rootScope.getMessage('AD_labelYes')) {
                                        var translation = scope.translationInfos[language];
                                        adTranslationService.deleteFile(
                                            {
                                                idTranslation: translation.idTranslation,
                                                idClient: scope.translationProperty.translationClientId
                                            },
                                            function () {
                                                commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                scope.translation[field.idField][language] = null;
                                                scope.translationInfos[language] = null;
                                            }, function (error) {
                                                $log.error(error);
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataDeleteError'));
                                            }
                                        );
                                    }
                                });
                                // The following lines are needed to prevent the dialog from
                                // being shown again by pressing Enter
                                var $focused = $(':focus');
                                $focused.blur();
                            };

                            /**
                             * Stores translations
                             *
                             * @param field Field to be translated
                             */
                            scope.doTranslation = function (field) {
                                var counterSent = 0;
                                var counter = 0;
                                _.each($rootScope.languages, function (language) {
                                    if (scope.translation[field.idField][language.countrycode] !== null &&
                                        scope.translation[field.idField][language.countrycode] !== undefined) {
                                        counterSent++;
                                    }
                                });
                                var methodCreate = adTranslationService.create;
                                var methodUpdate = adTranslationService.update;
                                var methodDelete = adTranslationService.delete;
                                if (field.column.reference && (field.column.reference.rtype === "IMAGE" || field.column.reference.rtype === "FILE")) {
                                    methodCreate = adTranslationService.createFile;
                                    methodUpdate = adTranslationService.updateFile;
                                    methodDelete = adTranslationService.deleteFile;
                                }

                                scope.translationProperty.translationInitClientId = scope.translationProperty.translationClientId;
                                _.each($rootScope.languages, function (language) {
                                    var idModule = scope.fieldValue.id_module ? scope.fieldValue.id_module : scope.adTab.table.module.idModule;
                                    var request = {
                                        idClient: scope.translationProperty.translationClientId,
                                        idModule: idModule,
                                        idTable: scope.adTab.table.idTable,
                                        idColumn: field.column.idColumn,
                                        active: true,
                                        rowkey: scope.item.id
                                    };
                                    if (scope.translation[field.idField][language.countrycode]) {
                                        request.translation = scope.translation[field.idField][language.countrycode];
                                        request.idLanguage = language.idLanguage;
                                        if (scope.translationInfos[language.countrycode]) {
                                            request.idTranslation = scope.translationInfos[language.countrycode].idTranslation;
                                            request.created = scope.translationInfos[language.countrycode].created;
                                            request.active = scope.translationInfos[language.countrycode].active;
                                            methodUpdate(
                                                request,
                                                function () {
                                                    counter++;
                                                    if (counter === counterSent) {
                                                        commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                        scope.translationClients = null;
                                                        $('#' + scope.translationDialogId).modal('hide');
                                                    }
                                                }, function (error) {
                                                    $log.error(error);
                                                    commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                                }
                                            );
                                        } else {
                                            request.idTranslation = null;
                                            request.created = null;
                                            request.active = true;
                                            methodCreate(
                                                request,
                                                function () {
                                                    counter++;
                                                    if (counter === counterSent) {
                                                        commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                        $('#' + scope.translationDialogId).modal('hide');
                                                    }
                                                }, function (error) {
                                                    $log.error(error);
                                                    commonService.showRejectNotification($rootScope.getMessage('AD_msgDataSaveError'));
                                                }
                                            );
                                        }
                                    }
                                    else if (scope.translationInfos[language.countrycode] && scope.translationInfos[language.countrycode].idTranslation) {
                                        methodDelete(
                                            {
                                                idTranslation: scope.translationInfos[language.countrycode].idTranslation,
                                                idClient: cacheService.loggedIdClient()
                                            },
                                            function () {
                                                scope.translation[field.idField][language] = null;
                                                scope.translationInfos[language] = null;
                                                counter++;
                                                if (counter === counterSent) {
                                                    commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                                    scope.translationClients = null;
                                                    $('#' + scope.translationDialogId).modal('hide');
                                                }
                                            }, function (error) {
                                                $log.error(error);
                                                commonService.showRejectNotification($rootScope.getMessage('AD_msgDataDeleteError '));
                                            }
                                        );
                                    }
                                });
                            };

                            /**
                             * Gets the data needed for the input of the TABLE search component
                             *
                             * @param field Field to obtain data from
                             * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                             */
                            scope.getFieldAttr = function (field) {
                                return {
                                    idReferenceValue: field.column.idReferenceValue,
                                    name: field.column.name,
                                    idColumn: field.column.idColumn,
                                    rtype: field.column.reference.rtype,
                                    display: field.display,
                                    required: field.column.mandatory,
                                    readonly: field.readonly,
                                    relatedItem: this.item
                                };
                            };

                            /**
                             * Gets the data needed for the input of the TABLE search component
                             *
                             * @param field Field to obtain data from
                             * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: *, sqlorderby: *}}
                             */
                            scope.getListFieldAttr = function (field) {
                                return {
                                    idReferenceValue: field.column.idReferenceValue,
                                    name: field.column.name,
                                    idColumn: field.column.idColumn,
                                    rtype: field.column.reference.rtype,
                                    readonly: scope.fieldReadOnlyRuntime[field.name] !== undefined ? scope.fieldReadOnlyRuntime[field.name] : field.readonly,
                                    required: field.column.mandatory
                                };
                            };

                            scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                                var field = _.find(scope.fields, function (current) {
                                    return current.column.idColumn === idColumnData
                                });
                                scope.fieldValue['KEY_' + field.column.name] = fieldValueData;
                                scope.fieldValue[field.column.name] = fieldDisplayData;
                                scope.changeValue(field);
                            };

                            scope.selectMultipleElement = function (idColumnData, fieldValueData, fieldDisplayData, addedData) {
                                var field = _.find(scope.fields, function (current) {
                                    return current.column.idColumn === idColumnData
                                });
                                if (field) {
                                    if (addedData) {
                                        if (scope.fieldValue['KEY_' + field.column.name] === "" ||
                                            scope.fieldValue['KEY_' + field.column.name] === null ||
                                            scope.fieldValue['KEY_' + field.column.name] === undefined) {
                                            scope.fieldValue['KEY_' + field.column.name] = fieldValueData;
                                            scope.fieldValue[field.column.name] = fieldValueData;
                                            scope.fieldValue['ARRAY_' + field.column.name].push(fieldValueData);
                                        }
                                        else {
                                            // Verifying that the element is not already added. Done to prevent recursive calling
                                            if (!_.find(scope.fieldValue['ARRAY_' + field.column.name], function (current) {
                                                    return current === fieldValueData;
                                                })) {
                                                scope.fieldValue['KEY_' + field.column.name] += (";" + fieldValueData);
                                                scope.fieldValue['ARRAY_' + field.column.name].push(fieldValueData);
                                                scope.fieldValue[field.column.name] += (";" + fieldValueData);
                                            }
                                        }
                                    }
                                    else {
                                        var keys = _.split(scope.fieldValue['KEY_' + field.column.name], ";");
                                        var displays = _.split(scope.fieldValue[field.column.name], ";");
                                        keys = _.filter(keys, function (current) {
                                            return current !== fieldValueData;
                                        });
                                        displays = _.filter(displays, function (current) {
                                            return current !== fieldValueData;
                                        });
                                        scope.fieldValue['ARRAY_' + field.column.name] = _.filter(scope.fieldValue['ARRAY_' + field.column.name], function (current) {
                                            return current !== fieldValueData;
                                        });
                                        scope.fieldValue['KEY_' + field.column.name] = _.join(keys, ';');
                                        scope.fieldValue[field.column.name] = _.join(displays, ';');
                                    }
                                    scope.changeValue(field);
                                }
                            };

                            $timeout(function(){
                                scope.loadForm();
                            }, 0);
                        }
                    }
                }
            }
        }]);
});
