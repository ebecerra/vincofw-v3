define(['app'], function (app) {

    'use strict';

    return app.registerFilter('range', function () {
        return function(input, min, max) {
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i=min; i<=max; i++)
                input.push(i);
            return input;
        };
    });
});
