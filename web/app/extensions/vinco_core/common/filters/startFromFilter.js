define(['app'], function (app) {

    'use strict';

    return app.registerFilter('startFrom', function () {
        return function(input, start) {
            start = +start; //parse to int
            return input ? input.slice(start): [];
        }
    });
});
