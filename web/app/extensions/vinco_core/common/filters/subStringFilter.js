define(['app'], function (app) {

    'use strict';

    return app.registerFilter('subString', function () {
        return function(input, start, size) {
            if (input==null)
                return "";
            input = input.substr(start, size);
            return input;
        }
    });
});
