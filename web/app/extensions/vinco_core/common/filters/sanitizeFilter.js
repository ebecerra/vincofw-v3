define(['app'], function (app) {

    'use strict';

    return app.registerFilter('sanitize', function ($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        }
    });
});
