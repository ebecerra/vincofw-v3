define(['app'], function (app) {

    'use strict';

    /**
     * Returns the decimal places according to the input.
     * E.g. If decimalPlaces is 2, it returns 0.01
     * E.g. If decimalPlaces is 5, it returns 0.0001
     *
     * Default return is 0.1
     */
    return app.registerFilter('numberStep', function () {
        return function(decimalPlaces) {
            if (decimalPlaces && Number.isInteger(Number(decimalPlaces))){
               return 1 / Math.pow(10, decimalPlaces);
            }
            else
                return 0.1;
        }
    });
});
