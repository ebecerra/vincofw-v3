define(['app'], function (app) {

    'use strict';

    return app.registerDirective('processExecution', ['$log', '$rootScope',
        function ($log, $rootScope) {
            return {
                restrict: 'E',
                scope:{
                    parentItem: '='
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/process/view/process.execution.component.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.getMessage = $rootScope.getMessage;

                            scope.logs = scope.parentItem.item.extra && scope.parentItem.item.extra.logs ? scope.parentItem.item.extra.logs : [];
                        }
                    }
                }
            }
        }
    ]);
});