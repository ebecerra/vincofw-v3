define(['angular',
    'angular-couch-potato',
    'angular-ui-router',
    'datatables',
    'datatables-responsive',
    'datatables-colvis',
    'datatables-tools',
    'datatables-bootstrap'

], function (ng, couchPotato) {

    'use strict';

    var module = ng.module('app.process', ['ui.router']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
            .state('app.process', {
                abstract: true,
                data: { title: 'process.title' }
            })
            .state('app.process.form', {
                url: '/process/form',
                data: { title: '' },
                views: {
                    'content@app': {
                        controller: 'processController',
                        templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/process/view/process.form.html?v=' + FW_VERSION,
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                EXTENSION_VINCO_CORE + 'modules/process/controller/processController',
                                EXTENSION_VINCO_CORE + 'common/directives/processNotification'
                            ])
                        }
                    }
                }
            });
    });

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });
    return module;

});
