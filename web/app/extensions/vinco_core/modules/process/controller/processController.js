define(['./../module'
], function (app) {

    'use strict';

    return app.registerController('processController', ['$scope', '$rootScope', 'tabManagerService',
        function ($scope, $rootScope, tabManagerService) {
            $scope.processLogs = [];
            $scope.processOutputParams = [];
            $scope.processInputParams = [];

            $scope.item = $rootScope.tabbedProcessItem;
            $scope.process = $rootScope.tabbedProcess;

            /**
             * Closes the process tab
             */
            $scope.closeProcessForm = function () {
                tabManagerService.removeActive();
            };
        }
    ]);
});
