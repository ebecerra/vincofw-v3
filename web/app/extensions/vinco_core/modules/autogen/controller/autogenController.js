define(['app'
], function (app) {

    'use strict';

    return app.registerController('autogenController', ['$state', '$scope', '$stateParams', '$rootScope', 'tabManagerService',
        function ($state, $scope, $stateParams, $rootScope, tabManagerService) {
            $scope.hasWindowId = $stateParams && !$stateParams.idWindow;
            $scope.idWindow = $stateParams.idWindow;

            $scope.isHelp = false;

            if ($scope.idWindow === "help"){
                $scope.isHelp = true;
            }
            tabManagerService.setAutogenStateParams();
            tabManagerService.setTabHeading("help", $rootScope.getMessage("AD_WikiTitle"));
            $scope.$on(app.eventDefinitions.GLOBAL_ITEM_EDIT, function (event, data) {
                var item = data.item;
                tabManagerService.goAutogenForm($scope.idWindow, item, $scope.itemId, $scope.idTab, null, data.multiple);
            });
            $scope.$on(app.eventDefinitions.GLOBAL_DASHBOARD_LOADED, function (event, data) {

            });
        }
    ]);
});
