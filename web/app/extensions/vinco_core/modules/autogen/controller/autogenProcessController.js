define(['./../module'
], function (app) {

    'use strict';

    return app.registerController('autogenProcessController', ['$state', '$scope', '$stateParams', '$rootScope', 'Authentication', 'tabManagerService', 'adProcessService', '$timeout', 'cacheService',
        function ($state, $scope, $stateParams, $rootScope, Authentication, tabManagerService, adProcessService, $timeout, cacheService) {
            $scope.hasProcessId = $stateParams && $stateParams.idProcess;
            $scope.idProcess = $stateParams.idProcess;
            $scope.item = {};
            tabManagerService.setAutogenStateParams();
            $scope.process = null;

            if ($scope.hasProcessId)
                $timeout(function () {
                    adProcessService.query({
                        idClient: cacheService.loggedIdClient(),
                        idLanguage: Authentication.getCurrentLanguage(),
                        q: "idProcess=" + $scope.idProcess
                    }, function (data) {
                        if (data.content.length > 0) {
                            $scope.process = data.content[0];
                            tabManagerService.setTabHeading($scope.idProcess, $scope.process.name);
                        }
                    }, function (data) {

                    });
                }, 0);


            /**
             * Closes the process tab
             */
            $scope.closeProcessForm = function () {
                tabManagerService.removeActive();
            };
        }
    ]);
});
