define(['app'
], function (app) {

    'use strict';
    return app.registerController('initialLoaderController', ['$state', '$scope', '$rootScope', '$log', 'tabManagerService', 'cacheService', 'adUserService',
        function ($state, $scope, $rootScope, $log, tabManagerService, cacheService, adUserService) {

            $scope.$on(app.eventDefinitions.GLOBAL_LAYOUT_LOADED, function (event, data) {
                $scope.checkUserSecurityContraints();
            });

            $scope.openOnStart = function () {
                // Open Start Window
                cacheService.loadPreference('WindowOpenOnStart', null).then(function (pref) {
                    if (pref.value) {
                        $state.go('app.autogen.table', {idWindow: pref.value});
                    }
                });
            };

            $scope.checkUserSecurityContraints = function () {
                $("#mainPanelErrorCaption").html($rootScope.getMessage('AD_PasswordCheckSecurity'));
                if (localStorage['userPasswordSize'] && localStorage['userPasswordSatisfy']) {
                    // Check user security constraints
                    adUserService.checkSecurity({
                        idClient: cacheService.loggedIdClient(),
                        idUser: cacheService.loggedIdUser(),
                        idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : null,
                        size: localStorage['userPasswordSize'],
                        security: localStorage['userPasswordSatisfy']
                    }, function (data) {
                        if (data.success) {
                            $scope.openOnStart();
                        } else {
                            $("#left-panel").hide();
                            $("#mainPanelErrorMessage").html(data.message);
                            $("#mainPanelErrorHint").html($rootScope.getMessage('AD_PasswordCheckSecurityHint'));
                            $("#mainPanelError").show();
                        }
                    }, function (error) {
                        $log.error(error);
                        $("#left-panel").hide();
                        $("#mainPanelErrorMessage").html($rootScope.getMessage('AD_titleError'));
                        $("#mainPanelError").show();
                    });
                } else {
                    $("#left-panel").hide();
                    $("#mainPanelErrorCaption").html($rootScope.getMessage('AD_PasswordCheckSecurity'));
                    $("#mainPanelError").show();
                }
            };

            if (!tabManagerService.isClosingTabs) {
                cacheService.clearCache();
                cacheService.loadHtmlTemplates().then(function (templates) {
                    return cacheService.loadPreferences();
                }).then(function(){
                    return cacheService.loadClients();
                }).catch(function (error) {
                    $log.error(error);
                });
            }
        }
    ]);
});
