define(['app'
], function (app) {

    'use strict';

    return app.registerController('autogenFormController', ['$state', '$scope', '$stateParams', 'Authentication', 'adWindowService', 'adTabService', '$rootScope', 'tabManagerService', 'cacheService',
        function ($state, $scope, $stateParams, Authentication, adWindowService, adTabService, $rootScope, tabManagerService, cacheService) {
            $scope.isNew = $stateParams && !$stateParams.itemId;
            $scope.idWindow = $stateParams.idWindow;
            $scope.idTab = $stateParams.idTab;
            $scope.itemId = $stateParams.itemId;
            $scope.isMultiple = $stateParams.isMultiple ? JSON.parse($stateParams.isMultiple) : false;
            $scope.tabLoaded = false;
            $scope.parentItem = null;
            $scope.initialData = {};

            tabManagerService.setAutogenStateParams();
            $scope.$on(app.eventDefinitions.GLOBAL_ITEM_EDIT, function (event, data) {
                var item = data.item;
                $scope.parentItem = item;
                tabManagerService.goAutogenForm($scope.idWindow, item, $scope.itemId, $scope.idTab, item, $scope.isMultiple);
            });

            $scope.$on('cancel_to_parent', function (event, data) {
                tabManagerService.goAutogenBackContent($scope.idWindow);
            });

            var previousData = {};
            $scope.tabData = tabManagerService.getTabContent(previousData, $scope.idWindow);
            if ($scope.tabData.initialData)
                $scope.initialData = $scope.tabData.initialData;
            // Loading the row of ad_window
            cacheService.loadWindow($scope.idWindow).then(function (data) {
                $scope.window = data;
                tabManagerService.setTabHeading($scope.idWindow, $scope.window.name);

                var q = 'idWindow=' + $scope.idWindow;
                if ($scope.idTab)
                    q = 'idTab=' + $scope.idTab;
                //Loading the row of ad_tab
                adTabService.query(
                    {
                        idClient: cacheService.loggedIdClient(),
                        q: q
                    },
                    function (data) {
                        if (data.content.length > 0) {
                            $scope.adTab = data.content[0];
                            $scope.tabData.formIdTab = $scope.adTab.idTab;
                            $scope.tabLoaded = true;
                        }
                    },
                    function (data) {

                    });
            }).catch(function (err) {
                // TODO: Error handling
            });
        }
    ]);
});
