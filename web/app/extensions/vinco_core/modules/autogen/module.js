define(['angular',
    'angular-couch-potato',
    'app',
    'angular-ui-router',
    'datatables',
    'datatables-responsive',
    'datatables-colvis',
    'datatables-tools',
    'datatables-bootstrap', 'jquery-ui'

], function (ng, couchPotato, app) {

    'use strict';

    var module = ng.module('app.autogen', ['ui.router']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
            .state('app.autogen', {
                abstract: true,
                data: {
                    title: 'autogen.title'
                }
            })
            .state('app.home', {
                url: '/home',
                views: {
                    "content@app": {
                        controller: 'initialLoaderController',
                        templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/autogen/view/home.html?v=' + FW_VERSION,
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                EXTENSION_VINCO_CORE + 'modules/autogen/controller/initialLoaderController'
                            ])
                        }
                    }
                },
                data: {
                    title: 'Dashboard'
                }
            })
            .state('app.autogen.table', {
                url: '/autogen/:idWindow/table',
                data: {
                    title: ''
                },
                views: {
                    'content@app': {
                        controller: 'autogenController',
                        templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/autogen/view/autogen.list.html?v=' + FW_VERSION,
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                EXTENSION_VINCO_CORE + 'modules/autogen/controller/autogenController',
                                EXTENSION_VINCO_CORE + 'common/directives/autogenWindowTabs'
                            ])
                        }
                    }
                }
            })
            .state('app.autogen.process', {
                url: '/autogen/:idProcess/process',
                data: {
                    title: ''
                },
                views: {
                    'content@app': {
                        controller: 'autogenProcessController',
                        templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/autogen/view/autogen.process.html?v=' + FW_VERSION,
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                EXTENSION_VINCO_CORE + 'modules/autogen/controller/autogenProcessController',
                                EXTENSION_VINCO_CORE + 'common/directives/scrollBottom'
                            ])
                        }
                    }
                }
            })
            .state('app.autogen.form', {
                url: '/autogen/:idWindow/form/:idTab/:isMultiple/:itemId',
                data: {
                    title: ''
                },
                params: {
                    idWindow: '@idWindow',
                    itemId: null,
                    idTab: '@idTab',
                    isMultiple: '@isMultiple'
                },
                views: {
                    'content@app': {
                        controller: 'autogenFormController',
                        templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/autogen/view/autogen.form.html?v=' + FW_VERSION,
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                EXTENSION_VINCO_CORE + 'modules/autogen/controller/autogenFormController',
                                EXTENSION_VINCO_CORE + 'common/directives/autogenAsociatedTabs',
                                EXTENSION_VINCO_CORE + 'common/directives/scrollBottom'
                            ])
                        }
                    }
                }
            });
    });

    module.run(function ($couchPotato, $rootScope, $state) {
        module.lazy = $couchPotato;
        $rootScope.$on(app.eventDefinitions.HELP_REQUESTED, function(){
            $state.go("app.autogen.table", {idWindow: 'help'});
        });
    });
    return module;

});
