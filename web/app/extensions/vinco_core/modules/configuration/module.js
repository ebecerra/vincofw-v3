define(['angular',
    'angular-couch-potato',
    'angular-ui-router'

], function (ng, couchPotato) {

    'use strict';

    var module = ng.module('app.configuration', ['ui.router']);

    couchPotato.configureApp(module);

    module.config(function () {
    });

    module.run(function ($couchPotato, $rootScope, commonService) {
        module.lazy = $couchPotato;

        $rootScope.getServerFieldErrorExtraInfo = function (error, field) {
            var errors = error.split('.');
            switch (errors[0]) {
                case 'Size':
                    var message = "";
                    switch (field.column.ctype){
                        case "STRING":
                            if (field.column.lengthMin){
                                message += ( "<div>" + $rootScope.getMessage('AD_ErrValidationSizeLengthMin', [field.column.lengthMin])+"</div>");
                            }
                            if (field.column.lengthMax){
                                message +=( "<div>" + $rootScope.getMessage('AD_ErrValidationSizeLengthMax', [field.column.lengthMax])+"</div>");
                            }
                            break;
                        default:
                            if (field.column.valuemin){
                                message += ( "<div>" + $rootScope.getMessage('AD_ErrValidationSizeMin', [field.column.valuemin])+"</div>");
                            }
                            if (field.column.valuemax){
                                message += ( "<div>" + $rootScope.getMessage('AD_ErrValidationSizeMax', [field.column.valuemax])+"</div>");
                            }
                            break;
                    }
                    return message;
                default:
                    return "";
            }
        };

        $rootScope.getServerFieldError = function (error, args) {
            var errors = error.split('.');
            switch (errors[0]) {
                case 'NotNull':
                    return $rootScope.getMessage('AD_ErrValidationNotNull', args);
                case 'Email':
                    return $rootScope.getMessage('AD_ErrValidationEmail', args);
                case 'Size':
                    return $rootScope.getMessage('AD_ErrValidationSize', args);
                case 'Unique':
                    return $rootScope.getMessage('AD_ErrValidationUnique', args);
                case 'FkNotFound':
                    return $rootScope.getMessage('AD_ErrValidationFkNotFound', args);
                case 'FkNotDelete':
                    return $rootScope.getMessage('AD_ErrValidationFkNotDelete', args);
                case 'DataTruncation':
                    return $rootScope.getMessage('AD_ErrValidationDataTruncation', args);
                case 'UploadFile':
                    return $rootScope.getMessage('AD_ErrValidationUploadFile', args);
                case 'UploadFileSize':
                    args.push(errors[3]);
                    return $rootScope.getMessage('AD_ErrValidationUploadFileSize', args);
                case 'UploadFileType':
                    args.push(errors[3]);
                    return $rootScope.getMessage('AD_ErrValidationUploadFileType', args);
                case 'CheckValue':
                    return $rootScope.getMessage(errors[3], args);
                case 'InvalidValue':
                    return $rootScope.getMessage(errors[3], errors.splice(4, errors.length - 3));
            }
            return error;
        };

        $rootScope.getServerObjectError = function (error) {
            switch (error) {
                case 'Unique':
                    return $rootScope.getMessage('AD_ErrValidationContraintUnique');
                case 'InvalidValue':
                    return $rootScope.getMessage('AD_ErrValidationInvalidValue');
                case 'Unknow':
                    return $rootScope.getMessage('AD_ErrValidationUnknow');
            }
            return error;
        };

        /**
         * Format server errors
         *
         * @param errors Information error
         * @param allFields Table fields
         */
        $rootScope.getValidationsErrors = function (errors, allFields) {
            var errMsg = '';
            if (errors.data) {
                try {
                    var fieldErrors = errors.data.fieldErrors || [];
                    var objectErrors = errors.data.objectErrors || [];
                    if (!errors.data.fieldErrors && errors.data.id) {
                        var info = JSON.parse(errors.data.id);
                        fieldErrors = info.fieldErrors || [];
                        objectErrors = info.objectErrors || [];
                    }
                    if (fieldErrors.length == 0 && objectErrors.length == 0) {
                        errMsg = $rootScope.getMessage('AD_ErrValidationServerError');
                    } else {
                        var msgCount = fieldErrors.length + objectErrors.length;
                        errMsg += msgCount > 1 ? '<ul>' : '';
                        _.each(fieldErrors, function (fldErr) {
                            var field = _.find(allFields, function (fld) {
                                return fld.name == fldErr.field || fld.standardName == fldErr.field;
                            });
                            var extraMessageInfo = $rootScope.getServerFieldErrorExtraInfo(fldErr.message, field);
                            errMsg +=
                                (msgCount > 1 ? '<li>' : '') +
                                $rootScope.getServerFieldError(fldErr.message, ['<b style="color: yellow">' + (field ? field.caption : fldErr.field) + '</b>']) +
                                (extraMessageInfo ? "<div>"+extraMessageInfo+"</div>" : "") +
                                (msgCount > 1 ? '</li>': '');
                        });
                        _.each(objectErrors, function (fldErr) {
                            var msg = fldErr.code == 'Text' ? fldErr.message : (fldErr.code == 'Message' ? $rootScope.getMessage(fldErr.message) : $rootScope.getServerObjectError(fldErr.code) + '<br/><b style="color: yellow">' + fldErr.message + '</b>');
                            errMsg += (msgCount > 1 ? '<li>' : '') + msg + (msgCount > 1 ? '</li>': '');
                        });
                        errMsg += msgCount > 1 ? '</ul>' : '';
                    }
                } catch (e) {
                    errMsg = $rootScope.getMessage('AD_ErrValidationServerError');
                }
            } else {
                errMsg = $rootScope.getMessage('AD_ErrValidationServerError');
            }
            return errMsg;
        };

        /**
         * Show server errors
         *
         * @param errors Information error
         * @param allFields Table fields
         */
        $rootScope.showValidationsErrors = function (errors, allFields) {
            commonService.showError($rootScope.getValidationsErrors(errors, allFields), 90000);
        };

    });
    return module;

});
