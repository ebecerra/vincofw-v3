define(['./../module'
], function (module) {

    'use strict';

    return module.registerFactory('coreConfigService', function () {
        var service = {};

        /**
         * Enum that contains the available events to be triggered as part of the logic of the module
         * @type {{NAVIGATION_FIRST: string, NAVIGATION_LAST: string, NAVIGATION_NEXT: string, NAVIGATION_PREV: string, NAVIGATION_BEFORE_NEW: string, NAVIGATION_NEW: string, NAVIGATION_EDIT_ITEM: string, NAVIGATION_GRID_SORT: string, NAVIGATION_DELETE_GRID_ITEM: string, NAVIGATION_APPLY: string, NAVIGATION_CANCEL: string, NAVIGATION_REFRESH_SORT_VALUES: string, NAVIGATION_REFRESH: string, NAVIGATION_REFRESH_COLUMNS: string, NAVIGATION_RETURN_TO_LIST: string, NAVIGATION_APPLY_SORT: string, NAVIGATION_CANCEL_SORT: string, NAVIGATION_CHANGE_VIEW_MODE: string, NAVIGATION_CHECK_ALL: string, NAVIGATION_UPDATE_CHECK_ALL: string, NAVIGATION_UPDATE_REFERENCE: string}}
         */
        service.events = {
            NAVIGATION_FIRST: "EVENTS_CORE_NAVIGATION_FIRST",           // Triggered when the user navigates to the first item in the registry (while using the edit Form)
            NAVIGATION_LAST: "EVENTS_CORE_NAVIGATION_LAST",             // Triggered when the user navigates to the last item in the registry (while using the edit Form)
            NAVIGATION_NEXT: "EVENTS_CORE_NAVIGATION_NEXT",             // Triggered when the user navigates to the next item in the registry (while using the edit Form)
            NAVIGATION_PREV: "EVENTS_CORE_NAVIGATION_PREV",             // Triggered when the user navigates to the previous item in the registry (while using the edit Form)
            NAVIGATION_BEFORE_NEW: "EVENTS_CORE_NAVIGATION_BEFORE_NEW", // Triggered before the editing Form appears
            NAVIGATION_NEW: "EVENTS_CORE_NAVIGATION_NEW",               // Triggered when the user request the creation of a new item so the editing Form should appear
            NAVIGATION_EDIT_ITEM: "EVENTS_CORE_NAVIGATION_EDIT_ITEM",   // Triggered when the user request the edition of a new item so the editing Form should appear
            NAVIGATION_GRID_SORT: "EVENTS_CORE_NAVIGATION_GRID_SORT",   // Triggered when the user request showing the sorting view
            NAVIGATION_DELETE_GRID_ITEM: "EVENTS_CORE_NAVIGATION_DELETE_GRID_ITEM", // Triggered when the user request to deletes item from the grid view
            NAVIGATION_APPLY: "EVENTS_CORE_NAVIGATION_APPLY",           // Triggered when the user request to save the information in the edit Form
            NAVIGATION_CANCEL: "EVENTS_CORE_NAVIGATION_CANCEL",         // Triggered when the user cancels the edition in the Form

            NAVIGATION_REFRESH_SORT_VALUES: "EVENTS_CORE_NAVIGATION_REFRESH_SORT_VALUES",
            NAVIGATION_REFRESH_CHART: "EVENTS_CORE_NAVIGATION_REFRESH_CHART",
            NAVIGATION_REFRESH: "EVENTS_CORE_NAVIGATION_REFRESH",
            NAVIGATION_REFRESH_COLUMNS: "EVENTS_CORE_NAVIGATION_REFRESH_COLUMNS",
            NAVIGATION_RETURN_TO_LIST: "EVENTS_CORE_NAVIGATION_RETURN_TO_LIST",
            NAVIGATION_APPLY_SORT: "EVENTS_CORE_NAVIGATION_APPLY_SORT",
            NAVIGATION_CANCEL_SORT: "EVENTS_CORE_NAVIGATION_CANCEL_SORT",
            NAVIGATION_CHANGE_VIEW_MODE: "EVENTS_CORE_NAVIGATION_CHANGE_VIEW_MODE",
            NAVIGATION_CHECK_ALL: "EVENTS_CORE_NAVIGATION_CHECK_ALL",
            NAVIGATION_UPDATE_CHECK_ALL: "EVENTS_CORE_NAVIGATION_UPDATE_CHECK_ALL",
            NAVIGATION_UPDATE_REFERENCE: "EVENTS_CORE_NAVIGATION_UPDATE_REFERENCE",
            NAVIGATION_REFRESH_REFERENCE: "EVENTS_CORE_NAVIGATION_REFRESH_REFERENCE",

            CONTEXT_MENU_ITEM_SELECTED: "EVENTS_CORE_CONTEXT_MENU_ITEM_SELECTED", // Triggered when an entry in the context menu has been clicked

            BUTTON_EXECUTE_ACTION: "BUTTON_EXECUTE_ACTION"
        };

        service.idTable = {
            adField: "ff80808151382f2a0151385c9296000d",
            adAudit: "ff8081815f7c855f015f7c8e30d30004"
        };

        service.idColumn = {
            adClient: { idClient: "ff808081526d9f5c01526dcc3572005f" }
        };

        service.adReference = {
            user: "A05ACA9B2E1A435CA2829CD738279532",
            module: "A05ACA9B2E1A435CA2829CD738279513"
        };

        service.tabs = {
            PREFERENCE: 'ff8080815133364d01513339e077000b'
        };

        service.columns = {
            PREFERENCE_VISIBLEAT_CUSTOM: 'ff808181639681260163968244e50000'
        };

        /**
         * Identifies the special case tabs
         */
        service.SPECIAL_CASE_TAB = {
            AUDIT: "AUDIT"
        };

        service.auditIdTab = "AUDIT_TABLE";

        return service;
    });
});
