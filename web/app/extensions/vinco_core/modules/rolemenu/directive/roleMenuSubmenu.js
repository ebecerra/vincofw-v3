define(['app', 'lodash'], function (app,_) {

    'use strict';

    return app.registerDirective('roleMenuSubmenu', [ 'Authentication', 'cacheService', '$log', 'coreConfigService',
        function (Authentication, cacheService, $log, coreConfigService) {
            var templateUrl = APP_EXTENSION_VINCO_CORE + 'modules/rolemenu/view/role.menu.submenu.html?v=' + FW_VERSION;
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    submenu: "="
                },
                priority: 1,
                templateUrl: templateUrl,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.subMenuTemplate = templateUrl;

                            scope.updateIdModule = function(submenus, idColumnData, fieldValueData){
                                _.each(submenus, function(submenu){
                                    if (submenu.idMenu === idColumnData) {
                                        submenu.idModule = fieldValueData;
                                    }
                                    else if (submenu.subMenus) {
                                        scope.updateIdModule(submenu.subMenus, idColumnData, fieldValueData);
                                    }
                                });
                            };

                            scope.selectModule = function(menu, idModule){

                                if (scope.submenu.idMenu === menu.idMenu) {
                                    scope.submenu.idModule = idModule;
                                }
                                else if (scope.submenu.subMenus) {
                                    scope.updateIdModule(scope.submenu.subMenus, menu.idMenu, idModule);
                                }
                            };
                        }
                    }
                }
            }
        }]
    );
});
