define(['app', 'lodash'], function (app,_) {

    'use strict';

    return app.registerDirective('roleMenuContainer', [ 'adMenuService', 'Authentication', 'cacheService', '$log', 'coreConfigService', 'adMenuRolesService', 'commonService', '$rootScope',
        function (adMenuService, Authentication, cacheService, $log, coreConfigService, adMenuRolesService, commonService, $rootScope) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    role: "="
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/rolemenu/view/role.menu.container.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            /**************************************************
                             *  Variables
                             *************************************************/
                            /**
                             * Menus to update
                             */
                            scope.menus = null;
                            /**
                             * Original response from the server to be able to restore original information
                             */
                            scope.originalMenus = null;

                            /**************************************************
                             *  End variables section
                             *************************************************/

                            adMenuService.allMenu({
                                idClient: cacheService.loggedIdClient(),
                                idRole: scope.role.idRole
                            }, function(data){
                                scope.menus = data.properties.menu;
                                scope.originalMenus = JSON.parse(JSON.stringify(scope.menus));
                            }, function(error){
                                $log.error(error);
                            });

                            /**
                             * Resets the menus to the original structure
                             */
                            scope.resetMenu = function(){
                                scope.menus = JSON.parse(JSON.stringify(scope.originalMenus));
                            };

                            scope.assembleMenu = function(aggregate, menus){
                                _.each(menus, function(current){
                                    if (current.selected && !current.idModule){
                                        scope.hasInvalidMenu = true;
                                        current.isInvalid = true;
                                    }
                                    else {
                                        current.isInvalid = false;
                                        aggregate.push({
                                            idMenu: current.idMenu,
                                            idModule: current.idModule,
                                            selected: current.selected
                                        });
                                    }
                                    if (current.subMenus){
                                        scope.assembleMenu(aggregate, current.subMenus);
                                    }
                                })
                            };

                            /**
                             * Saves the menu structure in the backend
                             */
                            scope.saveMenu = function(){
                                var menu = [];
                                scope.hasInvalidMenu = false;
                                scope.assembleMenu(menu, scope.menus);
                                if (!scope.hasInvalidMenu ) {
                                    adMenuRolesService.saveMenu({
                                            idClient: cacheService.loggedIdClient(),
                                            idRole: scope.role.idRole,
                                            menu: menu
                                        }, function (data) {
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                            scope.originalMenus = JSON.parse(JSON.stringify(scope.menus));
                                        }, function (error) {
                                            commonService.showRejectNotification($rootScope.getMessage('AD_ErrValidationUnknow'));
                                            $log.error(error);
                                        }
                                    )
                                }
                            };

                            scope.selectModule = function(menu, idModule){
                                var menu = _.find(scope.menus, function(current){
                                    return current.idMenu === menu.idMenu;
                                });
                                if (menu){
                                    menu.idModule = idModule;
                                }
                            };
                        }
                    }
                }
            }
        }]
    );
});
