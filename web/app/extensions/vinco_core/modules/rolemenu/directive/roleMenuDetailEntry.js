define(['app', 'lodash'], function (app,_) {

    'use strict';

    return app.registerDirective('roleMenuDetailEntry', [ 'adMenuService', 'Authentication', 'cacheService', '$log', 'coreConfigService', 'adMenuRolesService', 'commonService', '$rootScope',
        function (adMenuService, Authentication, cacheService, $log, coreConfigService, adMenuRolesService, commonService, $rootScope) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    menu: "=",
                    onModuleSelected: "&"
                },
                templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/rolemenu/view/role.menu.detail.entry.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            /**************************************************
                             *  Variables
                             *************************************************/
                            /**
                             * URL of the Off checkbox image
                             */
                            scope.imageCheckOff = APP_EXTENSION_VINCO_CORE + 'images/checkbox-off.png';
                            /**
                             * URL of the On checkbox image
                             */
                            scope.imageCheckOn = APP_EXTENSION_VINCO_CORE + 'images/checkbox-on.png';

                            /**************************************************
                             *  End variables section
                             *************************************************/

                            /**
                             * Modifies the selection data
                             * @param current Current menu to update
                             */
                            scope.onMenuSelect = function(current){
                                current.selected = !current.selected;
                            };


                            scope.selectModule = function(idColumnData, fieldValueData, fieldDisplayData){
                                if (scope.onModuleSelected){
                                    scope.onModuleSelected({
                                        menu: scope.menu,
                                        idModule: fieldValueData
                                    })
                                }
                            };

                            /**
                             * Loading the reference information for Module selection
                             */
                            cacheService.loadReferenceData(coreConfigService.adReference.module).then(function () {
                                scope.moduleReference = cacheService.getReference(coreConfigService.adReference.module);

                            });

                            scope.getMenuField = function(current){
                                return {
                                    idReferenceValue: coreConfigService.adReference.module,
                                    name: "idModule",
                                    idColumn: current.idMenu,
                                    rtype: scope.moduleReference.rtype,
                                    display: "name",
                                    required: false,
                                    readonly: false
                                };
                            }

                        }
                    }
                }
            }
        }]
    );
});
