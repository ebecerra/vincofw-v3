/**
 * @ngdoc directive
 * @name app.directive:designerDraggable
 * @description
 * This directive handles the Drag & Drop logic of the Layout dialog. It should be applied to each element to be dragged (each field)
 * The directives takes the editing adField as the model
 *
 * @restrict A
 * @param {Function} refresh Callback function to use for notification. The parent scope should listen to this function to visually refresh the layout
 */
define(['app', 'jquery', 'angular', 'lodash'], function (app, $, angular, _) {

    'use strict';

    return app.registerDirective('designerDraggable', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    refresh: '&refresh'
                },
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                    ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                        if (ngModel.$viewValue)
                            $(element).data('id-field', ngModel.$viewValue);
                    };
                    element.draggable({
                        revert:true
                    });
                    //This makes an element Droppable
                    element.droppable({
                        hoverClass: "ui-state-active",
                        drop:function(event,ui) {
                            scope.$apply(function(){
                                var target = ngModel.$viewValue;
                                var source = angular.element(ui.draggable).data('id-field');
                                // Placing source one position before target
                                source.seqno = target.seqno - 1;
                                target.seqno = source.seqno + 1;
                                // Setting group property
                                if (target.idFieldGroup){
                                    source.idFieldGroup = target.idFieldGroup;
                                } else {
                                    source.idFieldGroup = null;
                                }
                                scope.refresh();

                            });
                        }
                    });
                }
            }
        }]);
});