define(['app', 'jquery', 'angular'], function (app, $, angular) {

    'use strict';

    return app.registerDirective('designerComponent', ['$rootScope', '$compile', '$timeout', '$injector', '$log', '$state', '$q', 'Authentication', 'commonService', 'cacheService', 'autogenService',
        'adColumnService', 'adTranslationService', 'adClientService', 'tabManagerService', 'adHtmlTemplateService', 'coreConfigService', 'adFieldService', 'adTabService', 'adFieldGroupService',
        function ($rootScope, $compile, $timeout, $injector, $log, $state, $q, Authentication, commonService, cacheService, autogenService,
                  adColumnService, adTranslationService, adClientService, tabManagerService, adHtmlTemplateService, coreConfigService, adFieldService, adTabService, adFieldGroupService) {
            return {
                restrict: 'E',
                require: '?ngModel',
                templateUrl: APP_EXTENSION_VINCO_CORE + 'modules/designer/view/designer.component.editing.html?v=' + FW_VERSION,
                compile: function (tElement, tAttributes) {
                    return {
                        post: function (scope, element, attributes, ngModel) {
                            scope.inputsTemplate = APP_EXTENSION_VINCO_CORE + 'modules/designer/view/designer.component.inputs.html?v=' + FW_VERSION;
                            if (!scope.parentItem || !scope.parentItem.item || !scope.parentItem.item.idTab){
                                $log.error("Unable to obtain all the required information. ParentItem information is missing or incomplete");
                                return;
                            }
                            scope.tabGeneratorId = scope.parentItem.item.idTab;
                            scope.parentIdTab = null;
                            scope.parentTab = null;
                            scope.fieldReadOnlyRuntime = [];
                            scope.validColumnValues = [1,2,3,4,6,12];

                            scope.fieldInformation = {
                                tableName: 'ad_field',
                                tableTabName: 'ad_tab',
                                columns: {
                                    idField: 'ff808081515345980151534791040014',
                                    name: 'columns'
                                },
                                gridSeqno:{
                                    idField: 'ff808081515345980151534864aa0027',
                                    name:'grid_seqno'
                                },
                                startNewRow:{
                                    idField: 'ff808081515345980151534864b9002b',
                                    name:'startnewrow'
                                },
                                seqno:{
                                    idField: 'ff808081515345980151534864a60026',
                                    name:'seqno'
                                },
                                span:{
                                    idField: 'ff808081515345980151534864c6002f',
                                    name:'span'
                                },
                                displayed:{
                                    idField: 'ff808081515345980151534864b20029',
                                    name:'displayed'
                                },
                                caption:{
                                    idField: 'ff80808151bef4eb0151bf2ead97003b',
                                    name:'caption'
                                },
                                showingrid:{
                                    idField: 'ff808081515345980151534864b5002a',
                                    name:'showingrid'
                                },
                                idFieldGroup:{
                                    idField: 'ff808081515345980151534864a00024',
                                    name:'id_field_group'
                                }
                            };

                            /**
                             * Gets the data needed for the input of the TABLE search component
                             *
                             * @returns {{idReferenceValue: *, name: *, idColumn: (*|string), rtype: *, sqlwhere: * }}
                             */
                            scope.getFieldAttr = function(){
                                return {
                                    idReferenceValue: 'A05ACA9B2E1A435CA2829CD738279521',
                                    name: 'id_field_group',
                                    idColumn: 'ff80808151382f2a0151385c9b810016',
                                    rtype: 'TABLE',
                                    display: true,
                                    required: false,
                                    readonly: false
                                };
                            };

                            /**
                             * Defines if the buttons should be placed in a separate row at the end of the fields, or if they should be included
                             * as part of the common bootstrap rendering logic.
                             * @type {boolean} True to use the default rendering logic of all the fields. False to place buttons at the end of the form
                             */
                            scope.includeButtonsInBootstrap = false;
                            scope.allItems = cacheService.getTabItems(scope.tabGeneratorId);


                            scope.$on(coreConfigService.events.NAVIGATION_RETURN_TO_LIST, function (event, data) {
                                if (data.idTab === scope.tabGeneratorId) {
                                    scope.throwCancelClick();
                                }
                            });

                            var parentItem = cacheService.getFormItem({
                                idWindow: scope.windowTabInfo.idWindow,
                                tablevel: scope.windowTabInfo.tablevel - 1
                            });
                            scope.parentItem = parentItem;

                            scope.htmlTemplates = null;
                            cacheService.loadHtmlTemplates(function(templates){
                                scope.htmlTemplates = templates;
                            }).catch(function(err){
                                    scope.htmlTemplates = [];
                                    $log.error(err);
                            });


                            /**
                             * Preload preferences
                             */
                            scope.stdPref = cacheService.stdPref;

                            if (attributes.parentIdTab) {
                                scope.parentIdTab = attributes.parentIdTab;
                                cacheService.loadTab(scope.parentIdTab).then(function (data) {
                                    scope.parentTab = data;
                                });
                            }
                            scope.disableInline = attributes.$attr.disableInline;
                            scope.linkData = null;
                            if (attributes.linkData) {
                                scope.linkData = JSON.parse(attributes.linkData);
                            }
                            scope.otherTabs = 0;
                            if (attributes.otherTabs) {
                                scope.otherTabs = JSON.parse(attributes.otherTabs);
                            }

                            scope.fieldInitializations = function () {
                                scope.cols = 1;
                                scope.fields = [];
                                scope.primaryFields = [];
                                scope.groups = [];
                                scope.posField = [];
                                scope.fieldValue = {};
                                scope.service = null;
                                scope.allVisibleFields = [];
                                scope.allFields = [];
                                scope.item = {};
                                scope.windowGeneratorId = attributes.windowGeneratorId;
                                scope.complicateVisualFields = {};
                                scope.currentIdReferenceValue = null;
                                scope.currentColumnName = null;
                                scope.sortDirection = 'ASC';
                                scope.searchFilter = '';
                                scope.referenceData = null;
                            };

                            scope.fieldInitializations();

                            if (attributes.subItem) {
                                scope.subItem = JSON.parse(attributes.subItem);
                            } else {
                                scope.subItem = false;
                            }
                            scope.translation = {};
                            scope.translationProperty = {
                                translationClientId: null,
                                translationInitClientId: null
                            };

                            // Sort order. False equals ASC, True equals DESC
                            scope.tableReverseSort = false;
                            // Order by field
                            scope.tableOrderByField = '';
                            // Page options
                            scope.tablePageSizeList = [10, 20, 40, 60, 100];
                            scope.referenceData = {
                                referencePageSize: 10,
                                referenceCurrentPage: 1,
                                currentFilterValue: ""
                            };
                            scope.maxSize = 5;

                            scope.filterName = function (item) {
                                if (item[scope.complicateVisualFields[scope.currentIdReferenceValue].display] && scope.currentFilterValue) {
                                    return item[scope.complicateVisualFields[scope.currentIdReferenceValue].display].toLowerCase().indexOf(scope.currentFilterValue.toLowerCase()) >= 0;
                                }
                            };

                            /**
                             * Back to previous window
                             */
                            scope.goBack = function () {
                                if (!scope.subItem) {
                                    $rootScope.$broadcast('cancel_edit_form', 1);
                                } else {
                                    $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, {
                                        tabGeneratorId: scope.tabGeneratorId,
                                        tablevel: scope.windowTabInfo.tablevel
                                    });
                                }
                            };

                            scope.throwCancelClick = function () {
                                cacheService.clearFormInfo(scope.tabGeneratorId);
                                if (!scope.subItem) {
                                    $rootScope.$broadcast('cancel_to_parent', 1);
                                } else {
                                    $rootScope.$broadcast(app.eventDefinitions.CANCEL_SUB_ITEM_EDIT, {
                                        tabGeneratorId: scope.tabGeneratorId,
                                        tablevel: scope.windowTabInfo.tablevel
                                    });
                                }
                            };

                            /**
                             * Show/Hide a group of fields
                             *
                             * @param id Group field id.
                             */
                            scope.toggleGroup = function (id) {
                                var pos = scope.posGroup(scope.groups, id);
                                scope.groups[pos].collapsed = !scope.groups[pos].collapsed;
                            };

                            scope.selectTableValue = function (item) {
                                var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                                var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                                scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                                scope.fieldValue[scope.currentColumnName] = item[col];
                                scope.changeValue(scope.currentField);
                                $('#' + scope.tableSearchDialogId).modal('hide');
                            };

                            /**
                             * Show search selector dialog
                             *
                             * @param field Origin field
                             */
                            scope.showSearchWindow = function (field) {
                                scope.currentIdReferenceValue = field.column.idReferenceValue;
                                scope.currentColumnName = field.column.name;
                                scope.currentField = field;
                                scope.referenceData.referencePageSize = 10;
                                scope.referenceData.referenceCurrentPage = 1;
                                $('#searchModal').modal();
                            };

                            scope.selectSearchValue = function (item) {
                                var key = scope.complicateVisualFields[scope.currentIdReferenceValue].key;
                                var col = scope.complicateVisualFields[scope.currentIdReferenceValue].display;
                                scope.fieldValue['KEY_' + scope.currentColumnName] = item[key];
                                scope.fieldValue[scope.currentColumnName] = item[commonService.removeUnderscores(col)];
                                scope.changeValue(scope.currentField);
                                $('#searchModal').modal('hide');
                            };

                            /**
                             * Hides the process dialog
                             */
                            scope.hideProcessDialog = function () {
                                $('#' + scope.processDialogId).modal('hide');
                            };

                            /**
                             * Find the primary key column name in the fields array
                             *
                             * @param fields
                             */
                            scope.getPrimaryKeyColumnName = function (fields) {
                                var columnName = null;
                                _.each(fields, function (current, index) {
                                    if (current.column.primaryKey) {
                                        columnName = current.column.name;
                                    }
                                });
                                return columnName;
                            };

                            /**
                             * Load the row to edit or create and prepared the service
                             */
                            scope.reloadBusinessData = function () {
                                var defered = $q.defer();
                                var promise = defered.promise;
                                scope.initialLoading = true;

                                function finishLoading() {
                                    scope.checkLinkColumns();
                                    scope.initialLoading = false;
                                    _.each(scope.allFields, function (current) {
                                        scope.fieldValue['old_' + current.standardName] = null;
                                        if (current.column.reference.rtype == 'INTEGER' && scope.fieldValue[current.standardName]) {
                                            scope.fieldValue[current.standardName] = Number(scope.fieldValue[current.standardName]);
                                        }
                                        scope.changeValue(current);
                                    });
                                    scope.checkPasswordField();
                                }

                                // Initializing field values
                                _.each(scope.allFields, function (current) {

                                    if (current.column && current.column.valuedefault) {
                                        // If the column is id_client
                                        if (parentItem && current.column.idColumn == "ff808081526d9f5c01526dcc3572005f") {
                                            scope.fieldValue[current.column.name] = null;
                                            return;
                                        }
                                        var defValue = cacheService.replaceJSConditional(current, '', current.column.valuedefault);
                                        switch (current.column.ctype) {
                                            case 'INTEGER':
                                            case 'NUMBER':
                                                scope.fieldValue[current.column.name] = parseFloat(defValue);
                                                break;
                                            case 'BOOLEAN':
                                                scope.fieldValue[current.column.name] = eval(defValue);
                                                break;
                                            case 'DATE':
                                                scope.fieldValue[current.column.name] = moment(defValue);
                                                break;
                                            default:
                                                scope.fieldValue[current.column.name] = defValue;
                                        }
                                    } else {
                                        scope.fieldValue[current.column.name] = null;
                                    }
                                });

                                // Checking if there is any field in the parent that should be replaced in the sql where of the child fields
                                if (parentItem) {
                                    _.each(scope.allFields, function (field) {
                                        _.each(parentItem.fields, function (current) {
                                            if (field.column.reference.rtype == 'TABLEDIR') {
                                                if (!field.column.referenceValue) {
                                                    field.column.referenceValue = cacheService.getReference(field.column.idReferenceValue);
                                                }
                                                if (field.column.referenceValue.info.sqlwhere && cacheService.isFieldInConditional(current, field.column.referenceValue.info.sqlwhere)) {
                                                    if (field.runtime.sqlwhere === undefined || !cacheService.isCompleteConditional(field.runtime.sqlwhere)) {
                                                        field.runtime.sqlwhere = field.column.referenceValue.info.sqlwhere;
                                                        field.runtime.sqlwhere = cacheService.replaceSQLConditional(current, parentItem.item[current.column.standardName], field.runtime.sqlwhere);
                                                    }
                                                }
                                            }
                                        });
                                    });
                                }
                                return promise;
                            };

                            /**
                             * Checks the fields marked as "UsedInChild" in the parent
                             * and the fields marked as "LinkParent" in the child
                             */
                            scope.checkLinkColumns = function () {
                                for (var i = 0; i < scope.windowTabInfo.tablevel; i++) {
                                    var parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel - (i + 1)
                                    });
                                    if (parentItem) {
                                        _.each(scope.allFields, function (field) {
                                            if (field.column && field.column.linkParent && parentItem.item[field.column.standardName]) {
                                                var parent = parentItem;
                                                cacheService.loadReferenceData(field.column.idReferenceValue).then(function () {
                                                    var ref = cacheService.getReference(field.column.idReferenceValue);
                                                    var foundField = _.find(parent.fields, function (fieldData) {
                                                        return fieldData.idColumn == ref.info.idDisplay;
                                                    });
                                                    if (foundField) {
                                                        scope.formName = parent.item[foundField.column.standardName];
                                                    }
                                                })
                                            }
                                        });
                                        // Checking "linkParent" restriction only when this is a new Item
                                        if (!scope.isEditing) {
                                            _.each(scope.allFields, function (field) {
                                                if (field.column && field.column.linkParent && parentItem.item[field.column.standardName]) {
                                                    scope.fieldValue[field.column.name] = parentItem.item[field.column.standardName];
                                                    scope.changeValue(field);
                                                }
                                            });
                                        }
                                    }
                                }
                                scope.checkLinkColumnValues();
                            };

                            /**
                             * Checks and assigns the values of the parent item's field that are marekd
                             * as "useInChild"
                             *
                             * @param ignoreChange If true, it won't trigger a change check cycle
                             */
                            scope.checkLinkColumnValues = function (ignoreChange) {
                                var parentItem;
                                for (var i = 0; i < scope.windowTabInfo.tablevel; i++) {
                                    parentItem = cacheService.getFormItem({
                                        idWindow: scope.windowTabInfo.idWindow,
                                        tablevel: scope.windowTabInfo.tablevel - (i + 1)
                                    });
                                    if (parentItem) {
                                        _.each(parentItem.fields, function (field) {
                                            if (field.usedInChild && !scope.isEditing) {
                                                var any = _.find(scope.allFields, function (current) {
                                                    return current.column.name == field.column.name;
                                                });
                                                if (any) {
                                                    scope.fieldValue[any.column.name] = parentItem.item[any.column.standardName];
                                                    scope.item[any.column.standardName] = parentItem.item[any.column.standardName];
                                                    if (!ignoreChange)
                                                        scope.changeValue(any);
                                                } else {
                                                    scope.allFields.push({
                                                        column: field.column,
                                                        display: false,
                                                        displayed: false,
                                                        standardName: field.standardName
                                                    });
                                                    scope.fieldValue[field.column.name] = parentItem.item[field.column.standardName];
                                                    scope.item[field.column.standardName] = parentItem.item[field.column.standardName];
                                                    if (!ignoreChange)
                                                        scope.changeValue(field);
                                                }
                                            }
                                        });
                                    }
                                }

                                parentItem = cacheService.getFormItem({
                                    idWindow: scope.windowTabInfo.idWindow,
                                    tablevel: scope.windowTabInfo.tablevel - 1
                                });
                                var tableField = _.find(scope.allFields, function (current) {
                                    return current.column.standardName === "idTable";
                                });
                                var idRowField = _.find(scope.allFields, function (current) {
                                    return current.column.standardName === "idRow";
                                });
                                if (parentItem) {
                                    if (scope.adTab.idParentTable && tableField) {
                                        scope.fieldValue["id_table"] = parentItem.tab.idTable;
                                        scope.item["idTable"] = parentItem.tab.idTable;
                                    }
                                    if (idRowField) {
                                        scope.fieldValue["id_row"] = parentItem.item.id;
                                        scope.item["idRow"] = parentItem.item.id;
                                    }
                                }
                            };

                            /**
                             * Find the position of group in the group array
                             * @param groups
                             * @param idGroup
                             */
                            scope.posGroup = function (groups, idGroup) {
                                var outIndex = -1;
                                _.each(groups, function (current, index) {
                                    if (current.id == idGroup) {
                                        outIndex = index;
                                    }
                                });
                                return outIndex;
                            };

                            /**
                             * Loads and builds form structure
                             */
                            scope.loadForm = function () {
                                if (scope.tabGeneratorId !== undefined && scope.tabGeneratorId !== "") {
                                    cacheService.loadTab(scope.tabGeneratorId).then(function (data) {
                                        scope.adTab = angular.copy(data);
                                        adTabService.query({
                                            idClient: cacheService.loggedIdClient(),
                                            q: "idTab="+scope.adTab.idTab
                                        }, function(data){
                                            scope.originalTab = angular.copy(data.content[0]);
                                            scope.modifiedTab = angular.copy(scope.originalTab);
                                            cacheService.loadWindow(scope.adTab.idWindow).then(function (data) {
                                                scope.associatedWindow = data;
                                                scope.formName = data.name;
                                                // scope.originalFields = $.extend({}, scope.adTab.fields);
                                                scope.adTab.table = cacheService.getTable(scope.adTab.idTable);
                                                adFieldGroupService.query({
                                                    idClient: cacheService.loggedIdClient()
                                                }, function(data){
                                                    scope.allGroups = data.content;
                                                    adFieldService.query({
                                                        idClient: cacheService.loggedIdClient(),
                                                        q: "idTab="+scope.adTab.idTab
                                                    }, function(data){
                                                        scope.originalFields = angular.copy(data.content);
                                                        scope.arrangeForm(angular.copy(scope.originalFields));
                                                    }, function(err){
                                                        $log.log(err);
                                                    });
                                                }, function(err){
                                                    $log.log(err);
                                                });

                                            }).catch(function (err) {
                                                $log.log(err);
                                            });
                                        }, function(err){
                                            $log.log(err);
                                        });
                                    }).catch(function (err) {
                                        $log.log(err);
                                    });
                                } else {
                                    // TODO: Error handling
                                }
                            };

                            scope.arrangeForm = function(fields){
                                scope.fields = [];
                                scope.groups = [];
                                if (!fields)
                                    fields = scope.allFields;
                                scope.allFields = _.sortBy(fields, function (current) {
                                    return current.seqno;
                                });
                                var initialSeqno = 10;
                                _.each(scope.allFields, function(current){
                                    current.seqno = initialSeqno;
                                    initialSeqno += 10;
                                });
                                _.each(scope.allFields, function (current) {
                                    current.tableName = scope.adTab.table.name;
                                    current.display = true;
                                    current.column = _.find(scope.adTab.table.columns, function (col) {
                                        return col.idColumn === current.idColumn;
                                    });
                                });
                                scope.checkLinkColumnValues(true);

                                var visible = _.filter(scope.allFields, function (current) {
                                    return current.displayed;
                                });

                                // Get defined buttons
                                _.each(visible, function (field, index) {
                                    if (field.column) {
                                        if (field.column.reference.rtype == 'BUTTON') {
                                            field.column.refButton = cacheService.getReference(field.column.idReferenceValue);
                                            if (field.column.refButton.info.showMode !== 'IN_FORM') {
                                                field.ignoreButton = true;
                                            }
                                        }
                                    }
                                });
                                // Excluding buttons where SHOW_MODE != IN_FORM
                                visible = _.filter(visible, function (field) {
                                    return !field.ignoreButton;
                                });

                                // Excluding buttons where SHOW_MODE != IN_FORM
                                scope.buttons = _.filter(visible, function (field) {
                                    return field.column && field.column.reference && field.column.reference.rtype === "BUTTON";
                                });

                                _.each(visible, function (current, index) {
                                    scope.allVisibleFields.push(current);
                                    if (current.column) {
                                        scope.posField.push(index);

                                        if (current.column.primaryKey) {
                                            scope.primaryFields.push(current);
                                        }
                                        if (!current.idFieldGroup) {
                                            current.bootstrapCol = (12 / scope.modifiedTab.columns) * current.span;
                                            scope.fields.push(current);
                                        } else {
                                            if (scope.posGroup(scope.groups, current.idFieldGroup) == -1) {
                                                var group = {
                                                    name: current.name,
                                                    id: current.idFieldGroup,
                                                    fields: [],
                                                    fieldRows: []
                                                };
                                                group.fields.push(current);
                                                scope.groups.push(group);
                                            } else {
                                                var pos = scope.posGroup(scope.groups, current.idFieldGroup);
                                                scope.groups[pos].fields.push(current);
                                            }
                                        }
                                    }
                                });

                                // Loading the group name for each group
                                _.each(scope.groups, function (current, index) {
                                    var fieldGroup = _.find(scope.allGroups, function(group){
                                        return group.id === current.id;
                                    });
                                    var pos = scope.posGroup(scope.groups, current.id);
                                    scope.groups[pos].name = fieldGroup.name;
                                    scope.groups[pos].display = true;
                                    scope.groups[pos].columns = fieldGroup.columns;
                                    scope.groups[pos].collapsed = fieldGroup.collapsed;
                                    scope.groups[pos].displaylogic = fieldGroup.displaylogic;
                                    _.each(scope.groups[pos].fields, function (current) {
                                        current.bootstrapGroupColumns = (12 / (fieldGroup.columns ? fieldGroup.columns : scope.modifiedTab.columns)) * current.span;
                                        // current.groupColumns = fieldGroup.columns;
                                    });
                                });

                                // Here we define the rows according to the rules.
                                // A new row is started if any of the following applies:
                                //      1. If it is the first element
                                //      2. If the field explicitly states it starts a new row
                                //      3. If there is no more space left in the row
                                scope.fieldRows = [];
                                var currentRow;
                                var availableSpace = 12;
                                scope.fields = _.sortBy(scope.fields, function (current) {
                                    return current.seqno;
                                });
                                _.each(scope.fields, function (current, index) {
                                    if (current.column.reference.rtype === "BUTTON")
                                        return;
                                    var newRow = {
                                        fields: []
                                    };
                                    var neededSpace = ((12 / scope.modifiedTab.columns) * current.span);
                                    if (index == 0 || current.startnewrow || availableSpace < neededSpace) {
                                        currentRow = newRow;
                                        currentRow.fields.push(current);
                                        scope.fieldRows.push(currentRow);
                                        availableSpace = 12 - neededSpace;
                                    } else {
                                        currentRow.fields.push(current);
                                        availableSpace -= neededSpace;
                                    }
                                });

                                // Organizing group field to show by row
                                _.each(scope.groups, function (currentG, indexG) {
                                    var currentRowG;
                                    var columnSpaceG = 12;
                                    _.each(currentG.fields, function (currentF, indexF) {
                                        var newRow = {
                                            fields: []
                                        };
                                        var needcolumnSpaceG = ((12 / (currentG.columns ? currentG.columns : scope.modifiedTab.columns)) * currentF.span);
                                        if (indexF == 0 || currentF.startnewrow || columnSpaceG < needcolumnSpaceG) {
                                            currentRowG = newRow;
                                            currentRowG.fields.push(currentF);
                                            currentG.fieldRows.push(currentRowG);
                                            columnSpaceG = 12 - needcolumnSpaceG;
                                        } else {
                                            currentRowG.fields.push(currentF);
                                            columnSpaceG -= needcolumnSpaceG;
                                        }
                                    });
                                    currentG.fields = null;
                                });
                            };

                            scope.loadForm();

                            scope.reloadForm = function(){
                                scope.fieldInitializations();
                                scope.loadForm();
                            };

                            scope.getBootrapWidth = function (field, item) {
                                if (field.column.translatable && item[field.standardName] && item.id) {
                                    return 7;
                                }
                                if (field.column.translatable || (item[field.standardName] && item.id)) {
                                    return 9;
                                }
                                return 12;
                            };

                            /**
                             * Selects a field and scrolls the field navigator
                             * @param field Field to select
                             */
                            scope.selectField = function(field, ignoreScroll){
                                var selectedIndex = 0;
                                _.each(scope.allFields, function (current, index) {
                                    if (!current.runtime){
                                        current.runtime = {};
                                    }
                                    if (current.idField === field.idField){
                                        selectedIndex = index;
                                    }
                                    current.runtime.isSelected = false;
                                });
                                if (!ignoreScroll) {
                                    var scroll = selectedIndex * $('.field-item').outerHeight(true);
                                    if ($('.field-navigator').height() < scroll || scroll < $('.field-navigator').scrollTop())
                                        $('.field-navigator').scrollTop(scroll);
                                }
                                field.runtime.isSelected = true;
                                scope.selectedField = field;
                            };

                            scope.selectElement = function (idColumnData, fieldValueData, fieldDisplayData) {
                                scope.selectedField.idFieldGroup = fieldValueData;
                                scope.arrangeForm();
                            };

                            /**
                             * Resets the form structure to the original distribution
                             */
                            scope.undo = function(){
                                scope.modifiedTab = angular.copy(scope.originalTab);
                                scope.arrangeForm(angular.copy(scope.originalFields));
                            };

                            /**
                             * Saves the information to the server
                             */
                            scope.save = function(){
                                scope.toSend = angular.copy(scope.allFields);
                                _.each(scope.toSend, function(current){
                                    if (current.column)
                                        delete current.column;
                                    if (current.runtime)
                                        delete current.runtime;
                                });
                                var loopSave = function(toSend, index){
                                    scope.toSend[index].idField = scope.toSend[index].id;
                                    adFieldService.update( scope.toSend[index], function(data){
                                        if (++index === scope.toSend.length){
                                            commonService.showSuccesNotification($rootScope.getMessage('AD_msgDataSave'));
                                        }
                                        else{
                                            loopSave(scope.toSend, index);
                                        }
                                        $log.info(data);
                                    }, function(error){
                                        $log.error(error);
                                        if (++index === scope.toSend.length){
                                            commonService.showError($rootScope.getMessage('AD_msgDataSaveError'), 10000);
                                        }
                                        else{
                                            loopSave(scope.toSend, index);
                                        }
                                    })
                                };
                                adTabService.update( scope.modifiedTab,
                                    function(data){
                                        loopSave(scope.toSend, 0);
                                    },
                                    function(err){
                                        commonService.showError($rootScope.getMessage('AD_msgDataSaveError'), 10000);
                                    }
                                )
                            };
                        }
                    }
                }
            }
        }]);
});