package ${package}.persistence.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
<#if table.keyDates>
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonSetter;

import ${package}.tools.Converter;
import java.util.Date;
</#if>

/**
 * Created by Vincomobile FW on ${date}.
 * Copyright © ${year} Vincomobile. All rights reserved.
 *
 * Interface for PK of ${table.name}
 */
@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class ${table.capitalizeStandardName}PK implements Cloneable, Serializable {

<#list table.keyFields as campo>
    private ${campo.javaType} ${campo.standardName};
</#list>

    public ${table.capitalizeStandardName}PK() {
    }

    public ${table.capitalizeStandardName}PK(<#list table.keyFields as campo>${campo.javaType} ${campo.standardName}<#if campo_has_next>, </#if></#list>) {
<#list table.keyFields as campo>
        this.${campo.standardName} = ${campo.standardName};
</#list>
    }

    @Transient
    @JsonIgnore
    public Class<?> getClassType() {
        return ${table.capitalizeStandardName}PK.class;
    }

    @Transient
    public String getKey() {
        return <#list table.keyFields as campo>${campo.standardName}<#if campo_has_next> + "_" + </#if></#list>;
    }

<#list table.keyFields as campo>
    @Column(name = "${campo.name}", nullable = false)
<#if campo.dateField>
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM/dd/yyyy HH:mm:ss")
</#if>
    public ${campo.javaType} get${campo.capitalizeStandardName}() {
        return ${campo.standardName};
    }

    public void set${campo.capitalizeStandardName}(${campo.javaType} ${campo.standardName}) {
        this.${campo.standardName} = ${campo.standardName};
    }

<#if campo.dateField>
    @JsonSetter("${campo.standardName}")
    public void set${campo.capitalizeStandardName}(String ${campo.standardName}) {
        this.${campo.standardName} = Converter.getDate(${campo.standardName}, "yyyy-MM-dd'T'HH:mm:ss");
    }

</#if>
</#list>

    /**
     * Deep copy.
     * @return cloned object
     * @throws CloneNotSupportedException on error
     */
    @Override
    public ${table.capitalizeStandardName}PK clone() throws CloneNotSupportedException {
        final ${table.capitalizeStandardName}PK copy = (${table.capitalizeStandardName}PK) super.clone();
<#list table.keyFields as campo>
        copy.set${campo.capitalizeStandardName}(this.${campo.standardName});
</#list>
        return copy;
    }

    /**
     * toString implementation
     *
     * @see java.lang.Object#toString()
     * @return String representation of this class.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
<#list table.keyFields as campo>
        sb.append("${campo.standardName}: ").append(this.${campo.standardName}).append(", ");
</#list>
        return sb.toString();
    }

    /**
     * Equals implementation
     *
     * @see java.lang.Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        Object proxyThat = aThat;

        if ( this == aThat ) {
            return true;
        }

        if (aThat == null)  {
            return false;
        }

        final ${table.capitalizeStandardName}PK that;
        try {
            that = (${table.capitalizeStandardName}PK) proxyThat;
            if ( !(that.getClassType().equals(this.getClassType()))){
                return false;
            }
        } catch (org.hibernate.ObjectNotFoundException e) {
            return false;
        } catch (ClassCastException e) {
            return false;
        }


        boolean result = true;
<#list table.keyFields as campo>
        result = result && (((get${campo.capitalizeStandardName}() == null) && (that.get${campo.capitalizeStandardName}() == null)) || (get${campo.capitalizeStandardName}() != null && get${campo.capitalizeStandardName}().equals(that.get${campo.capitalizeStandardName}())));
</#list>
        return result;
    }

    /**
     * Calculate hash code
     *
     * @see java.lang.Object#hashCode()
     * @return a calculated number
     */
    @Override
    public int hashCode() {
        int hash = 0;
<#list table.keyFields as campo>
        hash += get${campo.capitalizeStandardName}().hashCode();
</#list>
        return hash;
    }

}