package ${package}.persistence.services;

import com.vincomobile.fw.core.persistence.services.BaseService;
import ${package}.persistence.model.${table.capitalizeStandardName};
<#if table.multipleKey>
import ${package}.persistence.model.${table.capitalizeStandardName}PK;
</#if>

/**
 * Created by Vincomobile FW on ${date}.
 * Copyright © ${year} Vincomobile. All rights reserved.
 *
 * Service layer interface for ${table.name}
 */
public interface ${table.capitalizeStandardName}Service extends BaseService<${table.capitalizeStandardName}, <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if>> {

}


