package ${package}.rest.web.controllers;

<#if table.multipleKey>
import ${package}.persistence.model.${table.capitalizeStandardName}PK;
</#if>
import ${package}.persistence.model.${table.capitalizeStandardName};
import ${package}.persistence.services.${table.capitalizeStandardName}Service;

import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.rest.web.controllers.BaseController;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.core.persistence.services.PageSearch;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
<#if table.imageViews || table.fileUrls>
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import com.vincomobile.fw.core.tools.FileTool;
import com.vincomobile.fw.rest.exceptions.FWBadRequestException;
</#if>

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.Assert;
<#if table.imageViews || table.fileUrls>
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
<#else>
import javax.validation.Valid;
</#if>
import javax.inject.Inject;

/**
 * Created by Vincomobile FW on ${date}.
 * Copyright © ${year} Vincomobile. All rights reserved.
 *
 * Controller for table ${table.name}
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "${table.module.restPath}/${table.lowerName}/{idClient}")
class ${table.capitalizeStandardName}Controller extends BaseController {

    private Logger logger = LoggerFactory.getLogger(${table.capitalizeStandardName}Controller.class);

    private final ${table.capitalizeStandardName}Service service;

    @Inject
    public ${table.capitalizeStandardName}Controller(${table.capitalizeStandardName}Service service) {
        Assert.notNull(service, "${table.capitalizeStandardName}Service must not be null");
        this.service = service;
    }

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<${table.capitalizeStandardName}> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), ${table.capitalizeStandardName}.class);
<#if table.imageViews || table.fileUrls>
        Page<${table.capitalizeStandardName}> result = service.findAll(pageReq);
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (!Converter.isEmpty(apacheServerCacheUrl)) {
            for (${table.capitalizeStandardName} item : result.getContent()) {
<#list table.fileImageColumns as col>
<#if table.imageViews>
                if (Converter.isEmpty(item.get${col.capitalizeStandardName}())) {
                    item.set${col.capitalizeStandardName}Url("app/extensions/vinco_core/images/photo_error.png");
                } else {
                    item.set${col.capitalizeStandardName}Url(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/${table.name}/" + item.getId() + "/" + item.get${col.capitalizeStandardName}());
                }
<#else>
                if (!Converter.isEmpty(item.get${col.capitalizeStandardName}())) {
                    item.set${col.capitalizeStandardName}Url(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/${table.name}/" + item.getId() + "/" + item.get${col.capitalizeStandardName}());
                }
</#if>
</#list>
            }
        }
        return result;
<#else>
        return service.findAll(pageReq);
</#if>
    }

<#if table.imageViews || table.fileUrls>
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ${table.capitalizeStandardName} get(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if> id
    ) {
<#else>
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ${table.capitalizeStandardName} get(@PathVariable("id") <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if> id) {
</#if>
        logger.debug("GET get(" + id + ")");
<#if table.imageViews || table.fileUrls>
        ${table.capitalizeStandardName} item = RestPreconditions.checkNotNull(service.findById(id));
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
<#list table.fileImageColumns as col>
<#if table.imageViews>
        if (Converter.isEmpty(item.get${col.capitalizeStandardName}())) {
            item.set${col.capitalizeStandardName}Url("app/extensions/vinco_core/images/photo_error.png");
        } else {
            item.set${col.capitalizeStandardName}Url(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/${table.name}/" + item.getId() + "/" + item.get${col.capitalizeStandardName}());
        }
<#else>
        if (Converter.isEmpty(item.get${col.capitalizeStandardName}())) {
            item.set${col.capitalizeStandardName}Url(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/${table.name}/" + item.getId() + "/" + item.get${col.capitalizeStandardName}());
        }
</#if>
</#list>
        return item;
<#else>
        return RestPreconditions.checkNotNull(service.findById(id));
</#if>
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
<#if table.imageViews || table.fileUrls>
    public <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if> create(
            @PathVariable("idClient") String idClient,
            @RequestPart ${table.capitalizeStandardName} entity,
<#list table.fileImageColumns as col>
            @RequestParam(value = "file_${col.name}", required = false) MultipartFile ${col.standardName},
</#list>
            BindingResult bindingResults
    ) {
        logger.debug("POST create(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
<#list table.fileImageColumns as col>
        if (${col.standardName} != null)
            entity.set${col.capitalizeStandardName}(FileTool.cleanFileName(${col.standardName}.getOriginalFilename()));
</#list>
        String result = (String) createEntity(entity);
<#list table.fileImageColumns as col>
        if (!bindingResults.hasErrors() && ${col.standardName} != null) {
<#if table.imageViews>
            ControllerResult valid = saveFile(idClient, "${table.name}", entity.getId(), "image/jpeg;image/png;image/gif", 500L, null, ${col.standardName});
<#else>
            ControllerResult valid = saveFile(idClient, "${table.name}", entity.getId(), null, 500L, null, ${col.standardName});
</#if>
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("${col.standardName}", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            }
        }
</#list>
        return result;
    }
<#else>
    public <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if> create(@RequestBody @Valid ${table.capitalizeStandardName} entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }
</#if>

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
<#if table.imageViews || table.fileUrls>
    public void update(
            @PathVariable("idClient") String idClient,
            @RequestPart ${table.capitalizeStandardName} entity,
<#list table.fileImageColumns as col>
            @RequestParam(value = "file_${col.name}", required = false) MultipartFile ${col.standardName},
</#list>
            BindingResult bindingResults
    ) {
        logger.debug("PUT update(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        ${table.capitalizeStandardName} item = RestPreconditions.checkNotNull(service.findById(entity.getId()));
<#list table.fileImageColumns as col>
        if (!bindingResults.hasErrors() && ${col.standardName} != null) {
            entity.set${col.capitalizeStandardName}(item.get${col.capitalizeStandardName}());
<#if table.imageViews>
            ControllerResult valid = saveFile(idClient, "${table.name}", entity.getId(), "image/jpeg;image/png;image/gif", 500L, item.get${col.capitalizeStandardName}(), ${col.standardName});
<#else>
            ControllerResult valid = saveFile(idClient, "${table.name}", entity.getId(), null, 500L, item.get${col.capitalizeStandardName}(), ${col.standardName});
</#if>
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("${col.standardName}", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.set${col.capitalizeStandardName}(valid.getProperties().get("filename").toString());
            }
        }
</#list>
        updateEntity(entity);
    }
<#else>
    public void update(@RequestBody @Valid ${table.capitalizeStandardName} entity) {
        logger.debug("PUT update(" + entity + ")");
        updateEntity(entity);
    }
</#if>

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
<#if table.imageViews || table.fileUrls>
    public void delete(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if> id
    ) {
        logger.debug("DELETE delete(" + id + ")");
        ${table.capitalizeStandardName} item = RestPreconditions.checkNotNull(service.findById(id));
        String apacheServerCache = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (apacheServerCache != null) {
<#list table.fileImageColumns as col>
            FileTool.deleteFile(apacheServerCache + "/files/${table.name}/" + item.getId() + "/" + item.get${col.capitalizeStandardName}());
</#list>
        }
        deleteEntity(item);
    }
<#else>
    public void delete(@PathVariable("id") <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if> id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }
</#if>

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
<#if table.imageViews || table.fileUrls>
        for (String id : ids) {
            delete(idClient, id);
        }
<#else>
        deleteItems(idClient, ids);
</#if>
    }
<#if table.imageViews>

    @RequestMapping(value = "/delete_image/{id}/{field}/{fileName}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public void deleteImage(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id,
            @PathVariable("field") String field,
            @PathVariable("fileName") String fileName
    ) {
        logger.debug("POST deleteImage(" + id + ", " + field + ", " + fileName + ")");
        ${table.capitalizeStandardName} item = RestPreconditions.checkNotNull(service.findById(id));
        String apacheServerCache = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (apacheServerCache != null) {
            FileTool.deleteFile(apacheServerCache + "/files/${table.name}/" + item.getId() + "/" + fileName);
        }
        item.setPropertyNull(field, String.class);
        service.save(item);
    }
</#if>

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

}