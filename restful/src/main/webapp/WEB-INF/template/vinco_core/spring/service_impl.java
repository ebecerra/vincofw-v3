package ${package}.persistence.services;

import com.vincomobile.fw.core.persistence.services.BaseServiceImpl;
import ${package}.persistence.model.${table.capitalizeStandardName};
<#if table.multipleKey>
import ${package}.persistence.model.${table.capitalizeStandardName}PK;
</#if>

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on ${date}.
 * Copyright © ${year} Vincomobile. All rights reserved.
 *
 * Service layer implementation for ${table.name}
 */
@Repository
@Transactional(readOnly = true)
public class ${table.capitalizeStandardName}ServiceImpl extends BaseServiceImpl<${table.capitalizeStandardName}, <#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if>> implements ${table.capitalizeStandardName}Service {

    /**
     * Constructor.
     */
    public ${table.capitalizeStandardName}ServiceImpl() {
        super(${table.capitalizeStandardName}.class);
    }

}
