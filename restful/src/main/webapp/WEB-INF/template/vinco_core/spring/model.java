package ${package}.persistence.model;

import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
<#if table.multipleKey>
<#if table.noKeyDatesExclCore>
import java.util.Date;
</#if>
<#else>
<#if table.datesExclCore>
import java.util.Date;
</#if>
</#if>
<#if table.notNullConstraints>
import javax.validation.constraints.NotNull;
</#if>
<#if table.sizeConstraints>
import javax.validation.constraints.Size;
</#if>

/**
 * Created by Vincomobile FW on ${date}.
 * Copyright © ${year} Vincomobile. All rights reserved.
 *
 * Model for table ${table.name}
 */
@Entity
@Table(name = "${table.name}")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class ${table.capitalizeStandardName} extends EntityBean<<#if table.multipleKey>${table.capitalizeStandardName}PK<#else>${table.keyType}</#if>> {

<#if table.multipleKey>
    private ${table.capitalizeStandardName}PK id;
<#else>
<#list table.keyFields as campo>
    private ${campo.javaType} ${campo.standardName};
</#list>
</#if>
<#list table.noKeyFieldsExclCore as campo>
    private ${campo.javaType} ${campo.standardName};
</#list>

<#list table.transients as campo>
    private ${campo.javaType} ${campo.standardName};
</#list>
<#list table.tableReferences as ref>
    private ${ref.referenceType} ${ref.fieldName};
</#list>

    /*
     * Set/Get Methods
     */

<#if table.multipleKey>
    @Id
    public ${table.capitalizeStandardName}PK getId() {
        return id;
    }

    public void setId(${table.capitalizeStandardName}PK id) {
        this.id = id;
    }

<#else>
    @Override
    @Transient
    public ${table.keyFields[0].javaType} getId() {
        return ${table.keyFields[0].standardName};
    }

    @Override
    public void setId(${table.keyFields[0].javaType} id) {
            this.${table.keyFields[0].standardName} = id;
    }

<#list table.keyFields as campo>
    @Id
    @Column(name = "${campo.name}")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public ${campo.javaType} get${campo.capitalizeStandardName}() {
        return ${campo.standardName};
    }

    public void set${campo.capitalizeStandardName}(${campo.javaType} ${campo.standardName}) {
        this.${campo.standardName} = ${campo.standardName};
    }
</#list>
</#if>

<#list table.noKeyFieldsExclCore as campo>
    @Column(name = "${campo.name}"<#if campo.checkSize>, length = ${campo.lengthMax?string["0"]}</#if>)<#if campo.mandatory>
    @NotNull</#if><#if campo.checkSize>
    @Size(min = ${campo.lengthMin}, max = ${campo.lengthMax?string["0"]})</#if>
    public ${campo.javaType} get${campo.capitalizeStandardName}() {
        return ${campo.standardName};
    }

    public void set${campo.capitalizeStandardName}(${campo.javaType} ${campo.standardName}) {
        this.${campo.standardName} = ${campo.standardName};
    }

</#list>
<#list table.tableReferences as ref>
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "${ref.referencedName}", referencedColumnName = "${ref.referencedColumnName}", insertable = false, updatable = false)
    public ${ref.referenceType} get${ref.fieldNameCap}() {
        return ${ref.fieldName};
    }

    public void set${ref.fieldNameCap}(${ref.referenceType} ${ref.fieldName}) {
        this.${ref.fieldName} = ${ref.fieldName};
    }

</#list>
    /**
     * Equals implementation
     *
     * @see java.lang.Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof ${table.capitalizeStandardName})) return false;

        final ${table.capitalizeStandardName} that = (${table.capitalizeStandardName}) aThat;
        boolean result = super.equals(aThat);
<#if table.multipleKey>
        result = result && id.equals(that.getId());
<#else>
    <#list table.keyFields as campo>
        result = result && (((${campo.standardName} == null) && (that.${campo.standardName} == null)) || (${campo.standardName} != null && ${campo.standardName}.equals(that.${campo.standardName})));
    </#list>
</#if>
    <#list table.noKeyFieldsExclCore as campo>
        result = result && (((${campo.standardName} == null) && (that.${campo.standardName} == null)) || (${campo.standardName} != null && ${campo.standardName}.equals(that.${campo.standardName})));
    </#list>
        return result;
    }

<#list table.transients as campo>
    @Transient
    public ${campo.javaType} get${campo.capitalizeStandardName}() {
        return ${campo.standardName};
    }

    public void set${campo.capitalizeStandardName}(${campo.javaType} ${campo.standardName}) {
        this.${campo.standardName} = ${campo.standardName};
    }

</#list>

}

