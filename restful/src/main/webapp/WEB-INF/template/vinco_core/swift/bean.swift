//
//  Enterprise.swift
//  Consultant
//
//  Created by Vincomobile FW on ${date}.
//  Copyright © ${year} Vincomobile. All rights reserved.
//

import Foundation
import ObjectMapper

class ${table.table.capitalizeStandardName}: Mappable {
<#if table.multipleKey>
    var id: ${table.capitalizeStandardName}PK
<#else>
<#list table.keyFields as field>
    var ${field.standardName}: ${field.swiftType}
</#list>
</#if>
<#list table.noKeyFields as field>
    var ${field.standardName}: ${field.swiftType}
</#list>

    var columns: [SyncSchemaColumn] = []

    // Tabla fisica de SQLLite
    var dbTable: DBTable {
        get {
            if let table = DBTable.tables[name!] {
                return table
            }
            else {
                return DBTable(name: self.name!)
            }
        }
    }

    required init?(_ map: Map) {
    }

    // Adiciona una columna a la tabla
    func addDBColumns(builder: TableBuilder) {
        if !dbTable.addColumns(builder) {
            for column in columns {
                column.addColumn(builder)
            }
        }
    }

    // Mappable
    func mapping(map: Map) {
<#list table.allFields as field>
        ${field.standardName} <-- map["${field.standardName}"]
</#list>
    }

}
