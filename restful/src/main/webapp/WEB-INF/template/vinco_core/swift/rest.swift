//
//  Enterprise.swift
//  Consultant
//
//  Created by Vincomobile FW on ${date}.
//  Copyright © ${year} Vincomobile. All rights reserved.
//

import Foundation
import ObjectMapper
import SQLite

class DB${table.table.capitalizeStandardName}: DBTable {
<#list table.keyFields as field>
    var ${field.standardName} = Expression<${field.swiftType}>("${field.name}")
</#list>
<#list table.noKeyFields as field>
    var ${field.standardName} = Expression<${field.swiftType}?>("${field.name}")
</#list>

    init() {
        super.init(name: "${table.table.name}")
    }

    override func addColumns(builder: TableBuilder) -> Bool {
<#list table.keyFields as field>
        builder.column(${field.standardName}, primaryKey:true)
</#list>
<#list table.noKeyFields as field>
        builder.column(${field.standardName})
</#list>

        return true
    }

}
