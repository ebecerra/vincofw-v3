<html>
<head>
    <title>Spring MVC</title>
</head>
<body>
    <h1>${msg}</h1>
    <ul>
        <li>Session ID: <b>${sessionId}</b></li>
        <li>User Agent: ${userAgent}</li>
    </ul>
</body>
</html>