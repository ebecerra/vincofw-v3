package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.model.AdRole;
import com.vincomobile.fw.core.persistence.model.AdScheduler;
import com.vincomobile.fw.core.persistence.services.AdRoleService;
import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.core.persistence.services.PageSearch;
import com.vincomobile.fw.core.session.SessionInfo;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Devtools.
 * Controller para la tabla AD_CLIENT
 *
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_sessions/{idClient}")
public class AdSessionsController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdSessionsController.class);

    @Autowired
    AdRoleService roleService;

    @Override
    public BaseService getService() {
        return null;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<SessionInfo> list(
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        List<SessionInfo> users = FWConfig.session.getUsers();
        if (pageReq.getSort() != null) {
            int column = -1;
            if (sort.contains("user"))
                column = 0;
            else if (sort.contains("name"))
                column = 1;
            else if (sort.contains("connect"))
                column = 2;
            else if (sort.contains("lastUpdate"))
                column = 3;
            if (column >= 0) {
                final int columnIdx = column;
                final boolean dir = sort.contains("ASC");
                Collections.sort(users, new Comparator<SessionInfo>() {
                    public int compare(SessionInfo si1, SessionInfo si2) {
                        switch (columnIdx) {
                            case 0:
                                return dir ? si1.getUser().compareTo(si2.getUser()) : si2.getUser().compareTo(si1.getUser());
                            case 1:
                                return dir ? si1.getUserName().compareTo(si2.getUserName()) : si2.getUserName().compareTo(si1.getUserName());
                            case 2:
                                return dir ? si1.getConnect().compareTo(si2.getConnect()) : si2.getConnect().compareTo(si1.getConnect());
                            case 3:
                                return dir ? si1.getLastUpdate().compareTo(si2.getLastUpdate()) : si2.getLastUpdate().compareTo(si1.getLastUpdate());
                        }
                        return 0;
                    }
                });
            }
        }
        int offset = pageReq.getOffset();
        if (offset > 0) {
            List<SessionInfo> pageUsers = new ArrayList<>();
            for (int i = offset; i < offset + limit && i < users.size(); i++) {
                pageUsers.add(users.get(i));
            }
            setUserRole(pageUsers);
            return new PageImpl<>(pageUsers, pageReq, FWConfig.session.count());
        } else {
            setUserRole(users);
            return new PageImpl<>(users, pageReq, FWConfig.session.count());
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public SessionInfo get(@PathVariable("id") String id) {
        logger.debug("GET get("+id+")");
        return RestPreconditions.checkNotNull(FWConfig.session.getUser(id));
    }

    /**
     * Update user with role
     *
     * @param pageUsers Users page
     */
    private void setUserRole(List<SessionInfo> pageUsers) {
        for (SessionInfo info : pageUsers) {
            String usr = info.getUser();
            int indx = usr.indexOf("$");
            if (indx > 0) {
                String roleId = usr.substring(0, indx);
                AdRole role = roleService.findById(roleId);
                if (role != null) {
                    info.setUserRole(usr.substring(indx + 1) + " (" + role.getDescription() + ")");
                }
            } else {
                info.setUserRole(info.getUser());
            }
        }
    }
}