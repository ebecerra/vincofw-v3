package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.hooks.HookResult;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.exception.FWPasswordRecoveryException;
import com.vincomobile.fw.core.persistence.model.AdLanguage;
import com.vincomobile.fw.core.persistence.services.AdLanguageService;
import com.vincomobile.fw.core.persistence.services.AdMessageService;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import com.vincomobile.fw.core.persistence.services.ReportService;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.Datetool;
import com.vincomobile.fw.core.tools.Mailer;
import com.vincomobile.fw.core.persistence.beans.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public abstract class BaseUserController extends BaseController {
    private Logger logger = LoggerFactory.getLogger(BaseUserController.class);

    @Autowired
    AdLanguageService languageService;

    @Autowired
    AdMessageService messageService;

    @Autowired
    ServletContext context;

    /**
     * Send password change request
     *
     * @param request HTTP Servlet request
     * @param appPath Application path
     * @param mailer Mailer manager
     * @param language Language
     * @param user User information
     * @param userType User type
     * @throws FWPasswordRecoveryException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    protected void passwordRecovery(
            HttpServletRequest request, String appPath, Mailer mailer, String language, UserInfo user, String userType
    ) throws FWPasswordRecoveryException, UnsupportedEncodingException, NoSuchAlgorithmException {
        if (user != null && user.isActive()) {
            String date = Converter.formatDate(new Date(), "ddMMyyyy");
            String hash = Converter.getHashValue(Converter.HASH_SHA1, "MAIL_RECOVER_" + user.getName() + "_" + user.getId() + "_" + date + "_" + userType);
            StringBuffer url = new StringBuffer();
            // Lines for production server
            url.append(request.getScheme()).append("://").append(request.getLocalName()).append(":").append(request.getLocalPort()).append(appPath);
            url.append("resetPassword?userId=").append(user.getId()).append("&date=").append(date).append("&hash=").append(hash).append("&_d=").append(System.currentTimeMillis());

            Map<String, String> resources = new HashMap<>();
            Map<String, Object> model = new HashMap<>();
            String fromEmail = CacheManager.getPreferenceString(user.getIdClient(), AdPreferenceService.GLOBAL_FROM_EMAIL_NOTIFICATIONS);
            model.put("currentYear", Datetool.getCurrentYear());
            model.put("phone", Converter.getHTMLString(CacheManager.getPreferenceString(user.getIdClient(), ReportService.PREF_HEADER_PHONE)));
            model.put("email", fromEmail);
            model.put("company", Converter.getHTMLString(CacheManager.getPreferenceString(user.getIdClient(), ReportService.PREF_HEADER_COMPANY)));
            model.put("url", url);
            model.put("name", Converter.getHTMLString(user.getName()));
            model.put("logoUrl", CacheManager.getPreferenceString(user.getIdClient(), AdPreferenceService.GLOBAL_WEB_SERVER_URL) + "/" + CacheManager.getPreferenceString(user.getIdClient(), ReportService.PREF_HEADER_LOGO));
            AdLanguage lang = languageService.findByISO(language);
            String langId = lang != null ? lang.getId() : null;
            HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_USER_RECOVER_PASSWORD, model, resources, user, language, context.getRealPath("/"));
            String template = hookResult.hasKey(FWConfig.HOOK_USER_RECOVER_PWD_TEMPLATE) ? (String) hookResult.get(FWConfig.HOOK_USER_RECOVER_PWD_TEMPLATE) : context.getRealPath("/") + "WEB-INF/template/vinco_core/passwd_recover_" + language + ".vm";
            String subject = hookResult.hasKey(FWConfig.HOOK_USER_RECOVER_PWD_SUBJECT) ? (String) hookResult.get(FWConfig.HOOK_USER_RECOVER_PWD_SUBJECT) : messageService.getMessage(user.getIdClient(), langId, "AD_GlobalMailPasswordUpdating", user.getName());
            JavaMailSender customSender = hookResult.hasKey(FWConfig.HOOK_USER_RECOVER_PWD_MAIL_SENDER) ? (JavaMailSender) hookResult.get(FWConfig.HOOK_USER_RECOVER_PWD_MAIL_SENDER) : null;
            if (!mailer.sendTemplateEmail(fromEmail, user.getEmail(), subject, template, model, resources, null, customSender)) {
                logger.error("Could not send password recovery mail: " + user.getEmail());
                throw new FWPasswordRecoveryException("AD_msgErrorCouldNotSendMail");
            }
        } else {
            throw new FWPasswordRecoveryException("AD_msgErrorInvalidUser");
        }
    }

    /**
     * Reset password for user
     *
     * @param user User information
     * @param userType User type
     * @param date Change password request date
     * @param hash Hash value
     * @param password Password
     * @throws FWPasswordRecoveryException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    protected void resetPassword(UserInfo user, String userType, String date, String hash, String password) throws FWPasswordRecoveryException, UnsupportedEncodingException, NoSuchAlgorithmException {
        if (user != null) {
            String calcHash = Converter.getHashValue(Converter.HASH_SHA1, "MAIL_RECOVER_" + user.getName() + "_" + user.getId() + "_" + date + "_" + userType);
            if (calcHash.equals(hash)) {
                Date changed = Converter.getDate(date, "ddMMyyyy");
                int expiredDays = Converter.getInt(CacheManager.getPreferenceString(user.getIdClient(), AdPreferenceService.GLOBAL_RESET_PASSWORD_VALID_DAYS), 1);
                if (Datetool.getDaysBetween(changed, new Date()) > expiredDays) {
                    throw new FWPasswordRecoveryException("AD_GlobalPasswordTooOld");
                } else {
                    user.setPassword(password);
                }
            } else {
                throw new FWPasswordRecoveryException("AD_GlobalPasswordHashDoNotMatch");
            }
        } else {
            throw new FWPasswordRecoveryException("AD_msgErrorInvalidUser");
        }
    }

}
