package com.vincomobile.fw.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown when user is forbidden to execute specified operation or access specified data.
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class FWForbiddenException extends RuntimeException {

    public FWForbiddenException() {
        super();
    }

    public FWForbiddenException(final String message) {
        super(message);
    }

    public FWForbiddenException(final Throwable cause) {
        super(cause);
    }

}