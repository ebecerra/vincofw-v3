package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.cache.CacheTable;
import com.vincomobile.fw.core.persistence.model.AdPreference;
import com.vincomobile.fw.core.persistence.model.AdTable;
import com.vincomobile.fw.core.persistence.model.AdTranslation;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.rest.exceptions.FWBadRequestException;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Devtools.
 * Controller para la tabla AD_TRANSLATION
 * <p/>
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_translation/{idClient}")
public class AdTranslationController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdTranslationController.class);

    @Autowired
    AdTranslationService service;

    @Autowired
    AdTableService adTableService;

    @Autowired
    AdUserService userService;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<AdTranslation> list(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdTranslation.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AdTranslation get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdTranslation entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/file_create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String createWithFile(
            @PathVariable("idClient") String idClient,
            @RequestPart @Valid AdTranslation entity,
            @RequestParam(value = "file_translation", required = false) MultipartFile translation,
            BindingResult bindingResults) {
        logger.debug("POST createWithFile(" + entity + ")");
        String result = (String) createEntity(entity);
        if (!bindingResults.hasErrors() && translation != null) {
            AdTable table = adTableService.findById(entity.getIdTable());
            ControllerResult valid = saveFile(idClient, table.getName(), entity.getRowkey() + "/translation/" + entity.getIdLanguage(), null, null, null, translation);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("translation", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.setTranslation(valid.getProperties().get("filename").toString());
            }
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdTranslation entity) {
        logger.debug("PUT update(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdTranslation dbEntity = service.findById(entity.getId());
        RestPreconditions.checkNotNull(dbEntity);
        entity.setCreated(dbEntity.getCreated());
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}/file_update", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void updateWithFile(
            @PathVariable("idClient") String idClient,
            @RequestPart("entity")  @Valid AdTranslation entity,
            @RequestParam(value = "file_translation", required = false) MultipartFile translation,
            BindingResult bindingResults
    ) {
        logger.debug("PUT updateWithFile(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdTranslation item = service.findById(entity.getId());
        RestPreconditions.checkNotNull(item);
        if (!bindingResults.hasErrors() && translation != null) {
            entity.setTranslation(item.getTranslation());
            AdTable table = adTableService.findById(entity.getIdTable());
            ControllerResult valid = saveFile(idClient, table.getName(), entity.getRowkey() + "/translation/" + entity.getIdLanguage(), null, null, entity.getTranslation(), translation);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("translation", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.setTranslation(valid.getProperties().get("filename").toString());
            }
        } else{
            entity.setTranslation(item.getTranslation());
        }
        service.update(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/file/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteFile(@PathVariable("id")  String id) {
        logger.debug("DELETE deleteFile(" + id + ")");
        AdTranslation entity = service.findById(id);
        RestPreconditions.checkNotNull(entity);
        deleteEntity(entity);
        service.deleteTranslationFile(entity);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/encode", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ControllerResult encode(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idLanguage") String idLanguage,
            @RequestParam(value = "idTable") String idTable,
            @RequestParam(value = "idColumn") String idColumn,
            @RequestParam(value = "rowkey") String rowkey
    ) throws Exception {
        logger.debug("GET encode(" + idLanguage + ", " + idTable + ", " + idColumn + ", " + rowkey + ")");
        ControllerResult result = new ControllerResult();
        AdTranslation item = service.findTranslation(idClient, idTable, idColumn, idLanguage, rowkey);
        AdPreference baserDir = CacheManager.getPreference(item.getIdClient(), AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (item != null && baserDir != null) {
            CacheTable table = service.getCacheTable(item.getIdTable());
            File photo = new File(baserDir.getString() + "/files/" + table.getTable().getName() + "/" + item.getPath());
            boolean isPng = photo.getName().toLowerCase().endsWith(".png");
            try {
                RenderedImage image = ImageIO.read(photo);
                java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream();
                ImageIO.write(image, isPng ? "png" : "jpg", os);
                byte[] b64 = Base64.encodeBase64(os.toByteArray());
                String ss = "data:image/" + (isPng ? "png" : "jpeg") + ";base64," + new String(b64);
                result.put("url", ss);
                result.put("load", true);
                //logger.debug(ss);
            } catch (IOException e) {
                logger.error("Error al cargar/codificar la foto: " + item.getPath() + "/" + item.getTranslation());
                logger.error("File: " + photo.getPath() + "/" + photo.getName());
                result.put("load", false);
            }
        } else {
            result.put("load", false);
        }
        result.put("idTable", idTable);
        result.put("idLanguage", idLanguage);
        result.put("idColumn", idColumn);
        result.put("rowkey", rowkey);
        return result;
    }

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    @ResponseBody
    public ControllerResult messages(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idLanguage", required = false) String idLanguage
    ) {
        logger.debug("GET messages(" + idLanguage + ")");
        if (Converter.isEmpty(idLanguage)) {
            AdPreference language = preferenceService.getPreference("DefaultLanguage", idClient);
            idLanguage = language.getString(FWConfig.FWCORE_ID_LANGUAGE_DEFAULT);
        }
        ControllerResult result = new ControllerResult();
        result.put("messages", service.listMessages(idClient, idLanguage, true, null));
        result.put("fields", service.listFieldCaptions(idClient, idLanguage));
        result.put("fieldDescriptions", service.listFieldDescriptions(idClient, idLanguage));
        result.put("processParams", service.listProccessParams(idClient, idLanguage));
        result.put("chartFilterParams", service.listChartFilterParams(idClient, idLanguage));
        result.put("chartColumns", service.listChartColumns(idClient, idLanguage));
        return result;
    }

    @RequestMapping(value = "/web_msg", method = RequestMethod.GET)
    @ResponseBody
    public ControllerResult webMsg(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idLanguage", required = false) String idLanguage
    ) {
        logger.debug("GET webMsg(" + idLanguage + ")");
        if (Converter.isEmpty(idLanguage)) {
            AdPreference language = preferenceService.getPreference("DefaultLanguage", idClient);
            idLanguage = language.getString(FWConfig.FWCORE_ID_LANGUAGE_DEFAULT);
        }
        ControllerResult result = new ControllerResult();
        result.put("messages", service.listMessages(idClient, idLanguage, null, true));
        return result;
    }

}