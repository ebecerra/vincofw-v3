package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.charts.ChartDefinition;
import com.vincomobile.fw.core.persistence.services.ExtendedFilter;
import com.vincomobile.fw.core.process.ProcessDefinition;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.web.context.support.XmlWebApplicationContext;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class AdXmlWebApplicationContext extends XmlWebApplicationContext {

    private Logger logger = LoggerFactory.getLogger(AdXmlWebApplicationContext.class);

    @Override
    protected void initBeanDefinitionReader(XmlBeanDefinitionReader beanDefinitionReader) {
        logger.debug("initBeanDefinitionReader()");
        initBeanDefinition(beanDefinitionReader, "com.vincomobile.fw.core.process");
    }

    protected void initBeanDefinition(XmlBeanDefinitionReader beanDefinitionReader, String[] packageNames) {
        // See: https://code.google.com/p/reflections/
        ConfigurationBuilder builder = new ConfigurationBuilder();
        for (String name : packageNames) {
            Set<URL> forPackage = new HashSet<>(ClasspathHelper.forPackage(name));
            builder.addUrls(forPackage);
        }
        builder.setScanners(new SubTypesScanner(false));
        Reflections reflections = new Reflections(builder);
        BeanDefinitionRegistry registry = beanDefinitionReader.getRegistry();
        // ProcessDefinition
        logger.debug("\tProcess loader");
        Set<Class<? extends ProcessDefinition>> classes = reflections.getSubTypesOf(ProcessDefinition.class);
        for (Class<? extends Object> serviceClass : classes) {
            registerClass(serviceClass, registry);
        }

        // ExtendedFilter
        logger.debug("\tExtendedFilter loader");
        Set<Class<? extends ExtendedFilter>> classesFilters = reflections.getSubTypesOf(ExtendedFilter.class);
        for (Class<? extends Object> serviceClass : classesFilters) {
            registerClass(serviceClass, registry);
            CacheManager.addExtendedFilter(serviceClass.getName());
        }

        // ExtendedFilter
        logger.debug("\tChart loader");
        Set<Class<? extends ChartDefinition>> classesCharts = reflections.getSubTypesOf(ChartDefinition.class);
        for (Class<? extends Object> serviceClass : classesCharts) {
            registerClass(serviceClass, registry);
        }
    }

    protected void initBeanDefinition(XmlBeanDefinitionReader beanDefinitionReader, String packages) {
        logger.debug("\tReading bean definitions for process:");
        packages = packages.replaceAll(",", ";");
        String[] packageNames = packages.split(";");
        initBeanDefinition(beanDefinitionReader, packageNames);
    }

    private void registerClass(Class<? extends Object> serviceClass, BeanDefinitionRegistry registry) {
        GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
        beanDefinition.setBeanClassName(serviceClass.getName());
        beanDefinition.setFactoryMethodName("getService");
        beanDefinition.setLazyInit(true);
        registry.registerBeanDefinition(serviceClass.getName(), beanDefinition);
        logger.debug("\t\t- "+serviceClass.getName());
    }
}
