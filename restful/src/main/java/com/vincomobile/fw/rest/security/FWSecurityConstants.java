package com.vincomobile.fw.rest.security;

public final class FWSecurityConstants {

    public static final class Privileges {

        public static final String CAN_LOGIN                = "ROLE_LOGIN";             // Can login into application
        public static final String CAN_EXEC_REPORT          = "ROLE_EXEC_REPORT";       // Can execute report and process
        public static final String CAN_OFFLINE_SYNC         = "ROLE_OFFLINE_SYNC";      // Can synchronize offline devices
        public static final String CAN_EXEC_CHART           = "ROLE_EXEC_CHART";        // Can execute chart

        public static final String CAN_AD_READ              = "ROLE_AD_READ";           // Can read on application dictionary
        public static final String CAN_AD_WRITE             = "ROLE_AD_WRITE";          // Can write on application dictionary

        public static final String CAN_SUPERADMIN           = "ROLE_SUPERADMIN";        // Super administrator privilege
    }

    public static final class Roles {

        public static final String ROL_SYSTEM       = "ROL_SYSTEM";                     // System administrator role

    }

    private FWSecurityConstants() {
        throw new AssertionError();
    }

}
