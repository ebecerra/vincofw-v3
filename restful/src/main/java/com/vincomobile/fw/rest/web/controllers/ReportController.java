package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.hooks.HookResult;
import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.core.persistence.services.ReportService;
import com.vincomobile.fw.rest.exceptions.FWResourceNotFoundException;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "vinco_core/report/{idClient}")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class ReportController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    ReportService reportService;

    @Override
    public BaseService getService() {
        throw new FWResourceNotFoundException();
    }

    @RequestMapping(value = "/{report}/{username}/{password}/{fileName}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] download(
            @PathVariable("idClient") String idClient,
            @PathVariable(value = "report") String report,
            @PathVariable(value = "username") String username,
            @PathVariable(value = "password") String password,
            @PathVariable(value = "fileName") String fileName,
            @RequestParam(value = "format") String format,
            @RequestParam(value = "q", required = false, defaultValue = "") String constraints,
            HttpServletResponse response
    ) {
        logger.debug("GET download(" + report + ", " + format + ", " + constraints + ")");
        byte[] result;
        if (validUser(userService, username, password, FWSecurityConstants.Privileges.CAN_EXEC_REPORT)) {
            HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_GET_EXTRA_PARAMS, FWConfig.HOOK_GET_EXTRA_PARAMS_MODE_USRPWD, idClient, username, password);
            if (hookResult.values.containsKey(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY)) {
                constraints += "," + hookResult.values.get(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY);
            }
            result = reportService.getReport(report, format, idClient, null, constraints, "es", username);
        } else {
            result = reportService.getReportError(format, "El usuario no es correcto o no tiene permisos para ejecutar el informe", idClient, null, "es", "'"+username+"'", report, constraints);
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "." + format + "\"");
        response.setContentLength(result.length);
        return result;
    }

}