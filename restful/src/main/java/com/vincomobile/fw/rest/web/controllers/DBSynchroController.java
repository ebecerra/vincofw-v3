package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdColumnSync;
import com.vincomobile.fw.core.persistence.model.AdTableSync;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.sync.SyncError;
import com.vincomobile.fw.core.sync.SyncExportDB;
import com.vincomobile.fw.core.sync.SyncSchemaTable;
import com.vincomobile.fw.core.sync.SyncTable;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/dbsync/{idClient}")
public class DBSynchroController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(DBSynchroController.class);

    @Autowired
    AdOfflineDeviceLogService deviceLogService;

    @Autowired
    AdTableSyncService tableSyncService;

    @Autowired
    AdColumnSyncService columnSyncService;

    @Override
    public BaseService getService() {
        return null;
    }

    @RequestMapping(value = "/schema", method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_OFFLINE_SYNC)
    public ControllerResult schema(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idOfflineDevice") String idOfflineDevice,
            @RequestParam(value = "tableName", required = false) String tableName
    ) {
        logger.debug("schema(" + idOfflineDevice + ", " + tableName + ")");
        deviceLogService.log(idClient, AdOfflineDeviceLogService.DEVICE_LOG_INFO, idOfflineDevice, AdOfflineDeviceLogService.DEVICE_LOG_TYPE_GET_SCHEMA, "Carga el esquema "+(tableName == null ? "completo" : "de la tabla: "+tableName));
        ControllerResult result = new ControllerResult();
        Map flt = new HashMap();
        flt.put("idClient", idClient);
        flt.put("synchronize", true);
        flt.put("active", true);
        if (!Converter.isEmpty(tableName)) {
            flt.put("table.name", tableName);
        }
        List<AdTableSync> tableSync = tableSyncService.findAll(tableSyncService.getSort("seqno", "asc"), flt);
        if (!Converter.isEmpty(tableName) && tableSync.size() == 0) {
            flt.remove("table.name");
            flt.put("nameRemote", tableName);
            tableSync = tableSyncService.findAll(tableSyncService.getSort("seqno", "asc"), flt);
        }
        List<SyncSchemaTable> tables = new ArrayList<>();
        for (AdTableSync sync : tableSync) {
            List<AdColumnSync> columns = columnSyncService.getTableColumns(sync.getIdTableSync());
            SyncSchemaTable schema = new SyncSchemaTable();
            schema.setTable(sync);
            schema.setColumns(columns);
            tables.add(schema);
        }
        result.put("tables", tables);
        result.put("db_version", CacheManager.getPreference(idClient, "OfflineSyncDBVersion", "1").getLong());
        HookManager.executeHook(FWConfig.HOOK_SYNCHRO_SCHEMA, idClient, getAuthUser());
        return result;
    }

    @RequestMapping(value = "/table", method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_OFFLINE_SYNC)
    public ControllerResult table(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idOfflineDevice") String idOfflineDevice,
            @RequestParam(value = "tableName") String tableName,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "param1", required = false) String param1,
            @RequestParam(value = "param2", required = false) String param2,
            @RequestParam(value = "param3", required = false) String param3,
            @RequestParam(value = "param4", required = false) String param4,
            @RequestParam(value = "param5", required = false) String param5,
            @RequestParam(value = "index", required = false) Long index
    ) {
        logger.debug("getTable(" + idOfflineDevice + ", " + tableName + ", " + filters + ", " + param1 + ", " + param2 + ", " + param3 + ", " + param4 + ", " + param5 + ", " + index + ")");
        deviceLogService.log(idClient, AdOfflineDeviceLogService.DEVICE_LOG_INFO, idOfflineDevice, AdOfflineDeviceLogService.DEVICE_LOG_TYPE_GET_TABLE, "Carga la tabla: " + tableName);
        ControllerResult result = new ControllerResult();
        AdTableSync table = tableSyncService.getTable(tableName);
        if (table != null) {
            Date currentTime = tableSyncService.getDatabaseDatetime();
            SyncTable synchronization = new SyncTable(table.getTable().getName(), table.getVersion());
            tableSyncService.getTableUpdateSQL(idClient, synchronization, table, filters, param1, param2, param3, param4, param5);
            result.put("update", synchronization);
            result.put("lastUpdate", currentTime);
            result.put("index", index);
            HookManager.executeHook(FWConfig.HOOK_SYNCHRO_TABLE, idClient, getAuthUser());
        } else {
            result.setError("Table not found");
        }
        return result;
    }

    @RequestMapping(value = "/makeupdate", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_OFFLINE_SYNC)
    public ControllerResult makeUpdate(
            @PathVariable("idClient") String idClient,
            @RequestBody SyncExportDB exportDB
    ) {
        logger.info("makeUpdate(" + exportDB.getIdOfflineDevice() + ", " + exportDB.getIdSeller() + ")");
        logger.debug("makeUpdate(" + exportDB + ")");
        ControllerResult result = new ControllerResult();
        deviceLogService.log(idClient, AdOfflineDeviceLogService.DEVICE_LOG_INFO, exportDB.getIdOfflineDevice(), AdOfflineDeviceLogService.DEVICE_LOG_TYPE_MAKE_UPDATE, "Sube una actualización");
        try {
            SyncError errResult = tableSyncService.makeUpdate(idClient, exportDB);
            result.setSuccess(errResult.getResult() == 0);
            logger.debug("makeUpdate -> "+errResult.getResult());
            if (errResult.getResult() != 0) {
                result.setSuccess(false);
                result.put("errorInfo", errResult);
                result.put("finish", "ERROR");
            } else {
                result.put("finish", "OK");
                HookManager.executeHook(FWConfig.HOOK_SYNCHRO_MAKE_UPDATE, idClient, getAuthUser(), exportDB.getIdSeller());
            }
        } catch (Exception e) {
            deviceLogService.log(idClient, AdOfflineDeviceLogService.DEVICE_LOG_ERROR, exportDB.getIdOfflineDevice(), AdOfflineDeviceLogService.DEVICE_LOG_TYPE_MAKE_UPDATE, "Error en la actualización: "+e.getMessage());
            logger.error("Error en la actualización");
            logger.error("Error", e);
            // TODO: Rollback
            result.setError(e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = "/dbversion", method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_OFFLINE_SYNC)
    public ControllerResult getDBVersion(@PathVariable("idClient") String idClient) {
        logger.debug("getDBVersion()");
        ControllerResult result = new ControllerResult();
        result.put("db_version", CacheManager.getPreference(idClient, "OfflineSyncDBVersion", "1"));
        return result;
    }

}