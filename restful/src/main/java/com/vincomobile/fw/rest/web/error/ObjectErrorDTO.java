package com.vincomobile.fw.rest.web.error;

/**
 * This class contains the information of a single field error.
 *
 * @author Petri Kainulainen
 */
final class ObjectErrorDTO {

    private final String code;

    private final String message;

    ObjectErrorDTO(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
