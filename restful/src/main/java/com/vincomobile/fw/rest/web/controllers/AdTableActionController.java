package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.model.AdTableAction;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.persistence.services.AdTableActionService;

import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.core.business.ValidationError;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.core.persistence.services.PageSearch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vincomobile FW on 16/02/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Controller for table ad_table_action
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_table_action/{idClient}")
public class AdTableActionController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdTableActionController.class);

    @Autowired
    AdTableActionService service;

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<AdTableAction> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdTableAction.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AdTableAction get(@PathVariable("id") String id) {
        logger.debug("GET get("+id+")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    public String create(@RequestBody @Valid AdTableAction entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public void update(@RequestBody @Valid AdTableAction entity) {
        logger.debug("PUT update(" + entity + ")");
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdTableAction bean = (AdTableAction) entity;
        List<ValidationError> result = new ArrayList<>();
        if (AdTableActionService.TYPE_PROCESS.equals(bean.getAtype()) && Converter.isEmpty(bean.getIdProcess())) {
            result.add(new ValidationError(ValidationError.TYPE_FIELD, "idProcess", ValidationError.CODE_NOT_NULL, ""));
        }
        if (AdTableActionService.TYPE_WINDOW.equals(bean.getAtype()) && Converter.isEmpty(bean.getIdWindow())) {
            result.add(new ValidationError(ValidationError.TYPE_FIELD, "idWindow", ValidationError.CODE_NOT_NULL, ""));
        }
        if (AdTableActionService.TYPE_CUSTOM.equals(bean.getAtype()) && Converter.isEmpty(bean.getRestCommand())) {
            result.add(new ValidationError(ValidationError.TYPE_FIELD, "restCommand", ValidationError.CODE_NOT_NULL, ""));
        }
        if (AdTableActionService.TYPE_LINK.equals(bean.getAtype())) {
            if (Converter.isEmpty(bean.getUrl())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "url", ValidationError.CODE_NOT_NULL, ""));
            }
            if (Converter.isEmpty(bean.getTarget())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "target", ValidationError.CODE_NOT_NULL, ""));
            }
        }
        if (result.size() == 0) {
            if (!AdTableActionService.TYPE_PROCESS.equals(bean.getAtype()))
                bean.setIdProcess(null);
            if (!AdTableActionService.TYPE_WINDOW.equals(bean.getAtype()))
                bean.setIdWindow(null);
            if (!AdTableActionService.TYPE_CUSTOM.equals(bean.getAtype()))
                bean.setRestCommand(null);
            if (!AdTableActionService.TYPE_LINK.equals(bean.getAtype())) {
                bean.setUrl(null);
                bean.setTarget(null);
            }
        }
        return result;
    }

}