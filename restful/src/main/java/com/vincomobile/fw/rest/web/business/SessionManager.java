package com.vincomobile.fw.rest.web.business;

import com.vincomobile.fw.core.FWConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SessionManager {

    private Logger logger = LoggerFactory.getLogger(SessionManager.class);

    public void doControl() {
        logger.info("Session control. Active: " + FWConfig.session.count());
        FWConfig.session.clearUsers();
        logger.info("Active: " + FWConfig.session.count());
    }
}
