package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdImage;
import com.vincomobile.fw.core.persistence.model.AdPreference;
import com.vincomobile.fw.core.persistence.model.AdTable;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.FileTool;
import com.vincomobile.fw.core.tools.ImageInfo;
import com.vincomobile.fw.core.tools.ImageResizeManager;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.core.business.ValidationError;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Controller for table ad_image
 * <p/>
 * Date: 23/01/2016
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_image/{idClient}")
public class AdImageController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdImageController.class);

    @Autowired
    AdImageService service;

    @Autowired
    AdMessageService messageService;

    @Autowired
    AdTableService tableService;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<AdImage> list(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        if (sort != null && sort.contains("file")) {
            sort = sort.replaceAll("file", "name");
        }
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdImage.class);
        Page<AdImage> result = service.findAll(pageReq);
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (!Converter.isEmpty(apacheServerCacheUrl)) {
            for (AdImage item : result.getContent()) {
                item.setUrl(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/" + item.getTable().getName() + "/" + item.getIdRow() + "/" + item.getName());
            }
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AdImage get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(
            @PathVariable("idClient") String idClient,
            @RequestPart("entity") AdImage entity,
            @RequestParam(value = "file_name") MultipartFile name,
            BindingResult bindingResults
    ) {
        logger.debug("POST create(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        ControllerResult valid = saveImage(idClient, entity, name);
        if (!valid.isSuccess()) {
            throwError(valid, bindingResults);
        }
        String result = (String) createEntity(entity);
        HookManager.executeHook(FWConfig.HOOK_IMAGE_CREATE_OR_UPDATE, entity);
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(
            @PathVariable("idClient") String idClient,
            @RequestPart("entity") AdImage entity,
            @RequestParam(value = "file_name", required = false) MultipartFile name,
            BindingResult bindingResults
    ) {
        logger.debug("PUT update(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdImage item = service.findById(entity.getId());
        RestPreconditions.checkNotNull(item);
        if (!bindingResults.hasErrors() && name != null) {
            entity.setName(item.getName());
            ControllerResult valid = saveImage(idClient, entity, name);
            if (!valid.isSuccess()) {
                throwError(valid, bindingResults);
            }
        }
        updateEntity(entity);
        HookManager.executeHook(FWConfig.HOOK_IMAGE_CREATE_OR_UPDATE, entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id
    ) {
        logger.debug("DELETE delete(" + id + ")");
        AdImage entity = service.findById(id);
        RestPreconditions.checkRequestElementNotNull(entity);
        AdTable table = tableService.findById(entity.getIdTable());
        AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (baserDir != null) {
            ImageResizeManager resizeManager = new ImageResizeManager();
            resizeManager.loadConfig(preferenceService, idClient, table.getCapitalizeStandardName());
            resizeManager.delete(baserDir.getString() + "/files/" + table.getName() + "/" + entity.getIdRow() + "/" + entity.getName());
        }
        HookManager.executeHook(FWConfig.HOOK_IMAGE_DELETE, entity);
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    /**
     * Save image file to file system
     *
     * @param idClient Client id.
     * @param image Image information
     * @param file File
     * @return Success or fail
     */
    private ControllerResult saveImage(String idClient, AdImage image, MultipartFile file) {
        AdTable table = tableService.findById(image.getIdTable());
        ControllerResult result = new ControllerResult();
        if (table != null) {
            image.setIdModule(table.getIdModule());
            AdPreference prefMaxSize = preferenceService.getPreference(table.getCapitalizeStandardName() + "_ImageMaxSize", idClient);
            result = validFileUpload(file, "image/jpeg;image/png;image/gif", prefMaxSize != null ? prefMaxSize.getLong(720L) : 720L);
            if (result.isSuccess()) {
                AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
                if (baserDir != null) {
                    String imgName = FileTool.cleanFileName(file.getOriginalFilename());
                    File fileImg = new File(baserDir.getString() + "/files/" + table.getName() + "/" + image.getIdRow() + "/" + imgName);
                    boolean res = FileTool.saveFile(file, fileImg.getPath()) != null;
                    if (res) {
                        ImageResizeManager resizeManager = new ImageResizeManager();
                        resizeManager.loadConfig(preferenceService, idClient, table.getCapitalizeStandardName());
                        ImageInfo imageInfo = resizeManager.loadInfo(fileImg);
                        if (imageInfo.isError()) {
                            result.setError(imageInfo.getMsg());
                        } else {
                            if (!imageInfo.isProportionOk()) {
                                logger.error("Invalid image proportion");
                                result.setError(ERROR_IMAGE_PROPORTION, "AD_msgErrorPhotoProportion");
                                return result;
                            }
                        }
                        if (resizeManager.isResize()) {
                            try {
                                resizeManager.resize(fileImg.getAbsolutePath());
                            } catch (Exception e) {
                                logger.error(e.getMessage());
                                result.setError(ERROR_IMAGE_RESIZE, "AD_msgErrorResizePhoto");
                                return result;
                            }
                        }
                        if (result.isSuccess()) {
                            if (!Converter.isEmpty(image.getName()) && !image.getName().equals(imgName)) {
                                resizeManager.delete(baserDir.getString() + "/files/" + table.getName() + "/" + image.getIdRow() + "/" + image.getName());
                            }
                            image.setName(imgName);
                            image.setWidth(imageInfo.getWidth());
                            image.setHeight(imageInfo.getHeight());
                            image.setMimetype(file.getContentType());
                        }
                    } else {
                        logger.error("Can not save file: " + fileImg.getAbsolutePath());
                        result.setError(ERROR_IMAGE_UPLOAD, ERROR_UPLOAD_FILE);
                    }
                } else {
                    logger.error("Not found base directory (Preference: " + AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE + ")");
                    result.setError(ERROR_IMAGE_UPLOAD, ERROR_UPLOAD_FILE);
                }
            } else {
                logger.error("Invalid file: " + result.getMessage());
            }
        } else {
            logger.error("Not found table: " + image.getIdTable());
            result.setError("AD_msgErrorPhotoInvalidTable");
        }
        return result;
    }

    @RequestMapping(value = "/encode", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ControllerResult encode(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idRow") String idRow,
            @RequestParam(value = "table") String table,
            @RequestParam(value = "name") String name
    ) {
        logger.debug("GET encode(" + idRow + " - " + name + ")");
        ControllerResult result = encodeImage(idClient, table, idRow, name);
        result.put("idRow", idRow);
        result.put("name", name);
        return result;
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        List<ValidationError> result = new ArrayList<>();
        HookManager.executeHook(FWConfig.HOOK_IMAGE_VALIDATE, entity, result);
        return result;
    }

}