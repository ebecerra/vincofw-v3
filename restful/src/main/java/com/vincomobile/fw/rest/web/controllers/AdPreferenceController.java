package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdPermission;
import com.vincomobile.fw.core.persistence.model.AdPreference;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.core.persistence.services.PageSearch;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.core.business.ValidationError;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Controller for table ad_preference
 *
 * Date: 17/12/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_preference/{idClient}")
public class AdPreferenceController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdPreferenceController.class);

    @Autowired
    AdPreferenceService service;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<AdPreference> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdPreference.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AdPreference get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdPreference entity) {
        logger.debug("POST create(" + entity + ")");
        String result = (String) createEntity(entity);
        CacheManager.removePreference(entity.getProperty());
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdPreference entity) {
        logger.debug("PUT update(" + entity + ")");
        entity.setClient(null);
        updateEntity(entity);
        CacheManager.removePreference(entity.getProperty());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        AdPreference entity = service.findById(id);
        RestPreconditions.checkNotNull(entity);
        CacheManager.removePreference(entity.getProperty());
        deleteEntity(entity);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public ControllerResult permissions(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idRole") String idRole
    ) {
        logger.debug("GET permissions(" + idClient + ", " + idRole + ")");
        ControllerResult result = new ControllerResult();
        if (hasAuthUser()) {
            result.put("permissions", service.listPermission(idClient, idRole, getAuthUser().getIdUser()));
        } else {
            result.put("permissions", new ArrayList<AdPermission>());
        }
        return result;
    }

    @RequestMapping(value = "/preference", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public ControllerResult preference(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idRole") String idRole,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "params", required = false) String params
    ) {
        logger.debug("GET preference(" + idClient + ", " + idRole + ", " + name + ")");
        ControllerResult result = new ControllerResult();
        result.put("preference", service.getPreference(name, idClient, idRole, getAuthUser().getIdUser(), params));
        return result;
    }

    @RequestMapping(value = "/preferences", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public ControllerResult preferences(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idRole") String idRole
    ) {
        logger.debug("GET preferences(" + idClient + ", " + idRole + ")");
        ControllerResult result = new ControllerResult();
        result.put("preferences", service.getPreferences(idClient, idRole, getAuthUser().getIdUser()));
        return result;
    }

    @RequestMapping(value = "/public_preference", method = RequestMethod.GET)
    @ResponseBody
    public ControllerResult public_preference(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idRole") String idRole,
            @RequestParam(value = "name") String name
    ) {
        logger.debug("GET public_preference(" + idClient + ", " + idRole + ", " + name + ")");
        ControllerResult result = new ControllerResult();
        result.put("preference", service.getPublicPreference(name, idClient, idRole));
        return result;
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        List<ValidationError> result = new ArrayList<>();
        HookManager.executeHook(FWConfig.HOOK_PREFERENCE_VALIDATE, result, entity);
        return result;
    }

}