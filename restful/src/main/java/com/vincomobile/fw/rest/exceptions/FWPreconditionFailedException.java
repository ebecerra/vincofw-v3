package com.vincomobile.fw.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
public final class FWPreconditionFailedException extends RuntimeException {

    public FWPreconditionFailedException() {
        super();
    }

    public FWPreconditionFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FWPreconditionFailedException(final String message) {
        super(message);
    }

    public FWPreconditionFailedException(final Throwable cause) {
        super(cause);
    }

}
