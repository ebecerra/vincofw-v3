package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.business.ValidationError;
import com.vincomobile.fw.core.persistence.model.AdRefList;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.persistence.services.AdRefListService;
import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.core.persistence.services.PageSearch;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Controller para la tabla AD_REF_LIST
 *
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_ref_list/{idClient}")
public class AdRefListController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdRefListController.class);

    @Autowired
    AdRefListService service;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<AdRefList> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "idLanguage", required = false) String idLanguage,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdRefList.class);
        Page<AdRefList> result = service.findAll(pageReq);
        if (!Converter.isEmpty(idLanguage)) {
            loadTranslations(result.getContent(), idClient, idLanguage);
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AdRefList get(@PathVariable("id") String id) {
        logger.debug("GET get("+id+")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdRefList entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdRefList entity) {
        logger.debug("PUT update(" + entity + ")");
        entity.setClient(null);
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        AdRefList entity = service.findById(id);
        RestPreconditions.checkRequestElementNotNull(entity);
        RestPreconditions.checkPreconditionRequired(!service.isUsed(entity));
        deleteEntity(entity);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdRefList bean = (AdRefList) entity;
        List<ValidationError> result = new ArrayList<>();
        if (bean.getValue().contains(" ")) {
            result.add(new ValidationError(ValidationError.TYPE_FIELD, "value", ValidationError.CODE_CHECK_VALUE, "AD_ValidationErrorRefListValue"));
        }
        return result;
    }

}