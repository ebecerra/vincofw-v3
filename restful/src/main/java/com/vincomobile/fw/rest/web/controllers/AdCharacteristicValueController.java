package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdCharacteristic;
import com.vincomobile.fw.core.persistence.model.AdCharacteristicValue;
import com.vincomobile.fw.core.persistence.model.AdPreference;
import com.vincomobile.fw.core.persistence.model.AdTable;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.tools.FileTool;
import com.vincomobile.fw.rest.exceptions.FWBadRequestException;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * Created by Devtools.
 * Controller for table ad_characteristic_value
 * <p/>
 * Date: 23/01/2016
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_characteristic_value/{idClient}")
public class AdCharacteristicValueController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdCharacteristicValueController.class);

    @Autowired
    AdCharacteristicValueService service;

    @Autowired
    AdCharacteristicService characteristicService;

    @Autowired
    AdTableService tableService;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdCharacteristicValue> list(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdCharacteristicValue.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdCharacteristicValue get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdCharacteristicValue entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value="create_file", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String createWithFile(
            @PathVariable("idClient") String idClient,
            @RequestPart("entity") AdCharacteristicValue entity,
            @RequestParam(value = "file_value", required = false) MultipartFile value,
            BindingResult bindingResults) {
        logger.debug("POST createWithFile(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        if (!bindingResults.hasErrors() && value != null) {
            ControllerResult valid = saveFile(idClient, entity, value);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("value", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.setValue(valid.getProperties().get("filename").toString());
            }
        }

        return service.save(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdCharacteristicValue entity) {
        logger.debug("PUT update(" + entity + ")");
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}/file_update", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void updateWithFile(
            @PathVariable("idClient") String idClient,
            @RequestPart("entity") AdCharacteristicValue entity,
            @RequestParam(value = "file_value", required = false) MultipartFile value,
            BindingResult bindingResults
    ) {
        logger.debug("PUT updateWithFile(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdCharacteristicValue item = service.findById(entity.getId());
        RestPreconditions.checkNotNull(item);
        if (!bindingResults.hasErrors() && value != null) {
            entity.setValue(item.getValue());
            ControllerResult valid = saveFile(idClient, entity, value);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("value", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.setValue(valid.getProperties().get("filename").toString());
            }
        } else{
            entity.setValue(item.getValue());
        }

        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id
    ) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
        AdCharacteristicValue item = service.findById(id);
        RestPreconditions.checkNotNull(item);
        AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (baserDir != null) {
            AdCharacteristic characteristic = characteristicService.findById(item.getIdCharacteristic());
            AdTable table = tableService.findById(characteristic.getIdTable());
            FileTool.deleteFile(baserDir.getString() + "/files/" + table.getName() + "/" + item.getKeyValue() + "/characteristic/" + item.getIdCharacteristic() + "_" + item.getValue());
        }
        service.delete(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    /**
     * Save file to file system
     *
     * @param idClient Client id.
     * @param characteristicValue Characteristic value information
     * @param file File
     * @return Success or fail
     */
    public ControllerResult saveFile(String idClient, AdCharacteristicValue characteristicValue, MultipartFile file) {
        AdCharacteristic characteristic = characteristicService.findById(characteristicValue.getIdCharacteristic());
        AdTable table = tableService.findById(characteristic.getIdTable());
        ControllerResult result = new ControllerResult();
        if (table != null) {
            String fileName = FileTool.cleanFileName(file.getOriginalFilename());
            result = saveFile(
                    idClient, table.getName(), characteristicValue.getKeyValue() + "/characteristic/", characteristic.getFileType(), characteristic.getFileMaxsize(),
                    characteristicValue.getIdCharacteristic() + "_" + fileName, characteristicValue.getIdCharacteristic() + "_" + characteristicValue.getValue(), file
            );
            result.put("filename", fileName);
        } else {
            logger.error("Not found table: " + characteristic.getIdTable());
            result.setError("Invalid table");
        }
        return result;
    }

}