package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.business.ValidationError;
import com.vincomobile.fw.core.business.VirtualUser;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.hooks.HookResult;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.exception.FWPasswordRecoveryException;
import com.vincomobile.fw.core.persistence.model.AdUser;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import com.vincomobile.fw.core.persistence.services.AdUserService;
import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.core.persistence.services.PageSearch;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.FileTool;
import com.vincomobile.fw.core.tools.Mailer;
import com.vincomobile.fw.rest.exceptions.FWBadRequestException;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Created by Devtools.
 * Controller para la tabla AD_USER
 * <p>
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_user/{idClient}")
public class AdUserController extends BaseUserController {

    private Logger logger = LoggerFactory.getLogger(AdUserController.class);

    @Autowired
    AdUserService service;

    @Autowired
    Mailer mailer;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdUser> list(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdUser.class);
        Page<AdUser> result = service.findAll(pageReq);
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (!Converter.isEmpty(apacheServerCacheUrl)) {
            for (AdUser item : result.getContent()) {
                if (Converter.isEmpty(item.getPhoto())) {
                    item.setPhotoUrl("app/extensions/vinco_core/images/user_default.png");
                } else {
                    item.setPhotoUrl(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/ad_user/" + item.getId() + "/" + item.getPhoto());
                }
            }
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdUser get(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id
    ) {
        logger.debug("GET get(" + id + ")");
        AdUser item = RestPreconditions.checkNotNull(service.findById(id));
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (Converter.isEmpty(item.getPhoto())) {
            item.setPhotoUrl("app/extensions/vinco_core/images/user_default.png");
        } else {
            item.setPhotoUrl(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/ad_user/" + item.getId() + "/" + item.getPhoto());
        }
        return item;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(
            @PathVariable("idClient") String idClient,
            @RequestPart AdUser entity,
            @RequestParam(value = "file_photo", required = false) MultipartFile photo,
            BindingResult bindingResults
    ) {
        logger.debug("POST create(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        if (photo != null)
            entity.setPhoto(FileTool.cleanFileName(photo.getOriginalFilename()));
        String result = (String) createEntity(entity);
        if (!bindingResults.hasErrors() && photo != null) {
            ControllerResult valid = saveFile(idClient, "ad_user", entity.getId(), "image/jpeg;image/png;image/gif", 500L, null, photo);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("photo", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            }
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(
            @PathVariable("idClient") String idClient,
            @RequestPart AdUser entity,
            @RequestParam(value = "file_photo", required = false) MultipartFile photo,
            BindingResult bindingResults
    ) {
        logger.debug("PUT update(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdUser item = RestPreconditions.checkNotNull(service.findById(entity.getId()));
        processPhoto(idClient, entity, photo, bindingResults, item);
        entity.setClient(null);
        entity.setModule(null);
        entity.setDefaultRole(null);
        updateEntityWithPassword(entity, "password");
    }

    @RequestMapping(value = "/{id}/profile", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void updateProfile(
            @PathVariable("idClient") String idClient,
            @RequestPart AdUser entity,
            @RequestParam(value = "file_photo", required = false) MultipartFile photo,
            BindingResult bindingResults
    ) {
        logger.debug("PUT updateProfile(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdUser item = RestPreconditions.checkNotNull(service.findById(entity.getId()));
        processPhoto(idClient, entity, photo, bindingResults, item);
        item.setName(entity.getName());
        item.setEmail(entity.getEmail());
        item.setPhone(entity.getPhone());
        if (!bindingResults.hasErrors() && photo != null) {
            item.setPhoto(entity.getPhoto());
        }
        item.setPhoneMobile(entity.getPhoneMobile());
        updateEntity(item);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id
    ) {
        logger.debug("DELETE delete(" + id + ")");
        AdUser item = RestPreconditions.checkNotNull(service.findById(id));
        String apacheServerCache = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (apacheServerCache != null) {
            FileTool.deleteFile(apacheServerCache + "/files/ad_user/" + item.getId() + "/" + item.getPhoto());
        }
        deleteEntity(item);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        for (String id : ids) {
            delete(idClient, id);
        }
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/delete_image/{id}/{field}/{fileName}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public void deleteImage(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id,
            @PathVariable("field") String field,
            @PathVariable("fileName") String fileName
    ) {
        logger.debug("POST deleteImage(" + id + ", " + field + ", " + fileName + ")");
        AdUser item = RestPreconditions.checkNotNull(service.findById(id));
        String apacheServerCache = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (apacheServerCache != null) {
            FileTool.deleteFile(apacheServerCache + "/files/ad_user/" + item.getId() + "/" + item.getPropertyValue(field));
        }
        item.setPropertyNull(field, String.class);
        service.save(item);
    }

    @RequestMapping(value = "/{idUser}/change_role", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_LOGIN)
    public void changeRole(
            @PathVariable("idClient") String idClient,
            @PathVariable("idUser") String idUser,
            @RequestParam(value = "idRole") String idRole,
            @RequestParam(value = "idLanguage") String idLanguage
    ) {
        logger.debug("POST changeRole(" + idUser + ", " + idRole + ", " + idLanguage + ")");
        AdUser item = RestPreconditions.checkNotNull(service.findById(idUser));
        item.setDefaultIdRole(idRole);
        item.setDefaultIdLanguage(idLanguage);
        service.save(item);
    }

    @RequestMapping(value = "/{idUser}/change_password", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_LOGIN)
    public ControllerResult changePassword(
            @PathVariable("idClient") String idClient,
            @PathVariable("idUser") String idUser,
            @RequestParam(value = "oldPassword") String oldPassword,
            @RequestParam(value = "newPassword") String newPassword,
            @RequestParam(value = "idLanguage", required = false) String idLanguage,
            @RequestParam(value = "size") Long size,
            @RequestParam(value = "security") String security
    ) {
        logger.debug("POST changePassword(" + idUser + ")");
        ControllerResult result = new ControllerResult();
        AdUser user = service.findById(idUser);
        boolean fwUser = user != null;
        VirtualUser virtualUser;
        if (!fwUser) {
            HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_USER_GET_REAL_USER, idClient, idUser);
            virtualUser = (VirtualUser) hookResult.values.get(FWConfig.HOOK_USER_GET_REAL_USER_REAL_USER);
        } else {
            virtualUser = new VirtualUser(user, user.getIdUser(), user.getPassword(), user.getPasswordUpdate());
        }
        if (virtualUser == null) {
            result.setError(messageService.getMessage(idClient, idLanguage, "AD_ErrNotFoundUser"));
            return result;
        }
        if (!virtualUser.getPassword().equals(oldPassword)) {
            result.setError(messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdOldPasswd"));
            return result;
        }
        // Validate security constraints
        String error = service.checkSecurityPassword(idClient, virtualUser.getPasswordUpdate(), idLanguage, size, security, true);
        if (error != null) {
            result.setError(error);
            return result;
        }
        // Validate user password history
        error = service.checkSecurityPasswordHistory(idClient, virtualUser.getIdUser(), idLanguage, newPassword);
        if (error != null) {
            result.setError(error);
            return result;
        }

        // Change password
        if (fwUser) {
            user.setPassword(newPassword);
            user.setPasswordUpdate(new Date());
            service.update(user);
        } else {
            HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_USER_CHANGE_PASSWORD, idClient, virtualUser, newPassword);
            if (!hookResult.success) {
                result.setError((String) hookResult.values.get(FWConfig.HOOK_USER_CHANGE_PASSWORD_ERROR));
            }
        }

        return result;
    }

    @RequestMapping(value = "/recover", method = RequestMethod.GET)
    @ResponseBody
    public ControllerResult recover(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "language", required = false, defaultValue = "es") String language,
            HttpServletRequest request
    ) {
        logger.debug("GET recover(" + email + ")");
        ControllerResult result = new ControllerResult();
        AdUser user = service.findByEmail(email);
        if (user != null) {
            try {
                passwordRecovery(request, request.getContextPath() + "/#/", mailer, language, user.getUserInfo(), "ADUSER");
            } catch (UnsupportedEncodingException e) {
                logger.error("Unable to send mail recovery", e);
                result.setError("AD_ErrValidationUnknow");
            } catch (NoSuchAlgorithmException e) {
                logger.error("Unable to send mail recovery", e);
                result.setError("AD_ErrValidationUnknow");
            } catch (FWPasswordRecoveryException e) {
                result.setError(e.getReason());
            }
        } else {
            result.setError("AD_ErrNotFoundUserByEmail");
        }
        return result;
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public ControllerResult reset(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "userId") String userId,
            @RequestParam(value = "date") String date,
            @RequestParam(value = "hash") String hash,
            @RequestParam(value = "password") String password
    ) {
        logger.debug("GET reset(" + userId + ")");
        ControllerResult result = new ControllerResult();
        Map model = new HashMap();
        AdUser user = service.findById(userId);
        boolean fwUser = user != null;
        if (!fwUser) {
            HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_USER_RESET_PASSWORD, idClient, userId, date, hash, password);
            String error = (String) hookResult.values.get(FWConfig.HOOK_USER_RESET_PASSWORD_ERROR);
            if (error != null) {
                result.setError(error);
            } else {
                model.put("login", hookResult.values.get(FWConfig.HOOK_USER_RESET_PASSWORD_LOGIN));
            }
        } else {
            try {
                resetPassword(user.getUserInfo(), "ADUSER", date, hash, password);
                user.setPassword(password);
                service.update(user);
                model.put("login", user.getUsername());
            } catch (FWPasswordRecoveryException e) {
                result.setError(e.getReason());
            } catch (UnsupportedEncodingException e) {
                logger.error("Unable to reset password", e);
                result.setError("AD_ErrValidationUnknow");
            } catch (NoSuchAlgorithmException e) {
                logger.error("Unable to reset password", e);
                result.setError("AD_ErrValidationUnknow");
            }
        }

        result.setProperties(model);
        return result;
    }

    @RequestMapping(value = "/check_security", method = RequestMethod.GET)
    @ResponseBody
    public ControllerResult checkSecurity(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idUser") String idUser,
            @RequestParam(value = "idLanguage", required = false) String idLanguage,
            @RequestParam(value = "size") Long size,
            @RequestParam(value = "security") String security
    ) {
        logger.debug("GET checkSecurity(" + idUser + ")");
        ControllerResult result = new ControllerResult();
        VirtualUser virtualUser;
        HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_USER_GET_REAL_USER, idClient, idUser);
        if (hookResult.hasKey(FWConfig.HOOK_USER_GET_REAL_USER_REAL_USER)) {
            virtualUser = (VirtualUser) hookResult.get(FWConfig.HOOK_USER_GET_REAL_USER_REAL_USER);
        } else {
            AdUser user = RestPreconditions.checkNotNull(service.findById(idUser));
            virtualUser = new VirtualUser(user, user.getIdUser(), user.getPassword(), user.getPasswordUpdate());
        }
        String error = service.checkSecurityPassword(idClient, virtualUser.getPasswordUpdate(), idLanguage, size, security, true);
        if (error != null) {
            result.setError(error);
        }
        return result;
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdUser bean = (AdUser) entity;
        List<ValidationError> result = new ArrayList<>();
        if (Converter.isEmpty(bean.getPhone())) {
            bean.setPhone(null);
/*
        } else {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            try {
                Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(bean.getPhone(), "ES");
                logger.info(bean.getPhone() + " = " + phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164));
            } catch (NumberParseException e) {
                System.err.println("NumberParseException was thrown: " + e.toString());
            }
*/
        }
        if (Converter.isEmpty(bean.getPhoneMobile())) {
            bean.setPhoneMobile(null);
        }
        return result;
    }

    /**
     * Process user photo
     *
     * @param idClient Client identifier
     * @param entity User entity
     * @param photo User photo
     * @param bindingResults Errors
     * @param item Load user
     */
    private void processPhoto(String idClient, AdUser entity, MultipartFile photo, BindingResult bindingResults, AdUser item) {
        if (!bindingResults.hasErrors() && photo != null) {
            entity.setPhoto(item.getPhoto());
            ControllerResult valid = saveFile(idClient, "ad_user", entity.getId(), "image/jpeg;image/png;image/gif", 500L, item.getPhoto(), photo);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("photo", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.setPhoto(valid.getProperties().get("filename").toString());
            }
        }
    }
}