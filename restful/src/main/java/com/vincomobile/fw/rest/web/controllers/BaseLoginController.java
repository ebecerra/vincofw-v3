package com.vincomobile.fw.rest.web.controllers;

import com.google.common.collect.Sets;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.beans.AdPrivilegeValue;
import com.vincomobile.fw.core.persistence.beans.AdUserRoleValue;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdRole;
import com.vincomobile.fw.core.persistence.model.AdUser;
import com.vincomobile.fw.core.persistence.model.AdUserRoles;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.rest.exceptions.FWResourceNotFoundException;
import com.vincomobile.fw.rest.security.AuthenticationFacade;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class BaseLoginController extends BaseUserController {

    private Logger logger = LoggerFactory.getLogger(BaseLoginController.class);

    final protected AdUserService usersService;

    final protected AdRoleService roleService;

    final protected AdUserRolesService userRolesService;

    final protected AuthenticationFacade authentication;

    public BaseLoginController(AdUserService usersService, AdRoleService roleService, AdUserRolesService userRolesService, AuthenticationFacade authentication) {
        this.usersService = usersService;
        this.roleService = roleService;
        this.userRolesService = userRolesService;
        this.authentication = authentication;
    }

    @Override
    public BaseService getService() {
        throw new FWResourceNotFoundException();
    }

    /**
     * Login
     *
     * @param idClient Client identifier
     * @param login Login name
     * @return ControllerResult
     */
    protected ControllerResult login(String idClient, String login) {
        ControllerResult result = new ControllerResult();
        result.put("menu", null);
        result.put("user", null);
        result.setSuccess(false);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        if (principal instanceof User) {
            AdUser user = userService.findByLogin(idClient, login);
            if (user != null && user.getActive() != null && user.getActive()) {
                setUserPhoto(user);
                result.put("userName", user.getName());
                result.put("idUserLogged", user.getIdUser());
                result.put("user", user);
            }
            setPrivileges(result, user);
            if (result.isSuccess() && !Converter.isEmpty(user.getEmail())) {
                try {
                    UserRecord firebaseUser = createOrUpdateFirebaseUser(user.getIdUser(), user.getName(), user.getEmail(), user.getPassword(), user.getPhoneMobile(), null);
                    if (firebaseUser != null) {
                        // user.setFirebaseToken(firebaseUser.);
                    }
                }
                catch(Throwable e) {
                    logger.error("Error updating o creating Firebase user: " + login);
                    logger.error(e.getMessage());
                }
            }
        }
        return result;
    }

    protected ControllerResult login(String login) {
        return login(null, login);
    }

    /**
     * Set user photo
     *
     * @param user User
     */
    protected void setUserPhoto(AdUser user) {
        String apacheServerCacheUrl = CacheManager.getPreferenceString(user.getIdClient(), AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (Converter.isEmpty(user.getPhoto())) {
            user.setPhotoUrl("app/extensions/vinco_core/images/user_default.png");
        } else {
            user.setPhotoUrl(apacheServerCacheUrl + user.getUpdated().getTime() + "/files/ad_user/" + user.getId() + "/" + user.getPhoto());
        }
    }

    /**
     * Login again
     *
     * @return ControllerResult
     */
    protected ControllerResult doRelogin() {
        return new ControllerResult();
    }

    /**
     * Logout
     *
     * @param httpRequest HTTP Request
     * @return ControllerResult
     */
    protected ControllerResult doLogout(HttpServletRequest httpRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        if (principal instanceof User) {
            FWConfig.session.removeUser(((User) principal).getUsername(), httpRequest.getSession().getId());
        }
        return new ControllerResult();
    }

    /**
     * Change default user role
     *
     * @param roleId Role identifier
     * @return AdUserRoles
     */
    protected AdUserRoles doChangeRole(String roleId) {
        AdRole role = roleService.findById(roleId);
        RestPreconditions.checkRequestElementNotNull(role);
        AdUser user = usersService.findByLogin(authentication.getMyUser().getUsername());
        user.setDefaultIdRole(roleId);
        usersService.save(user);
        return userRolesService.findUserRole(user.getIdUser(), roleId);
    }

    /**
     * Loads the user privileges
     *
     * @param result Controller result
     * @param user   Principal
     */
    protected void setPrivileges(ControllerResult result, AdUser user) {
        if (user != null) {
            List<AdUserRoles> rolesOfUser = user.getUserRoles();
            Set<AdPrivilegeValue> privileges = Sets.newHashSet();
            List<AdUserRoleValue> roles = new ArrayList<>();
            for (AdUserRoles userRole : rolesOfUser) {
                privileges.addAll(userRole.getRole().getPrivilegeValues());
                roles.add(new AdUserRoleValue(userRole.getIdClient(), userRole.getRole()));
            }
            result.put("privileges", privileges);
            result.put("isUser", true);
            result.setSuccess(true);
            result.put("roles", roles);
        } else {
            result.setError("AD_NotFoundUser");
        }
    }

    /**
     * Create or update Firebase user
     *
     * @param uuid Universal Unique identifier
     * @param displayName Name
     * @param mail Email
     * @param password Password
     * @param phone Phone
     * @param photo Photo
     * @return Firebase User
     * @throws FirebaseAuthException Exception
     */
    protected UserRecord createOrUpdateFirebaseUser(String uuid, String displayName, String mail, String password, String phone, String photo) throws FirebaseAuthException {
        if (uuid != null && mail != null && password != null && phone != null) {
            UserRecord userRecord = null;
            try {
                userRecord = FirebaseAuth.getInstance().getUser(uuid);
            } catch (Throwable e) {
                // Silent exception if user not exists.
            }
            if (userRecord == null) {
                return createFirebaseUser(uuid, displayName, mail, password, phone, photo);
            } else {
                return updateFirebaseUser(uuid, displayName, mail, password, phone, photo);
            }
        }
        return null;
    }

    /**
     * Update Firebase user
     *
     * @param uuid Universal Unique identifier
     * @param displayName Name
     * @param mail Email
     * @param password Password
     * @param phone Phone
     * @param photo Photo
     * @return Firebase User
     * @throws FirebaseAuthException Exception
     */
    protected UserRecord updateFirebaseUser(String uuid, String displayName, String mail, String password, String phone, String photo) throws FirebaseAuthException {
        UserRecord.UpdateRequest request = new UserRecord.UpdateRequest(uuid)
                .setEmail(mail)
                .setPhoneNumber(Converter.phoneToE164(phone, "ES"))
                .setEmailVerified(true)
                .setPassword(password)
                .setDisplayName(displayName)
                .setDisabled(false);

        UserRecord userRecord = FirebaseAuth.getInstance().updateUser(request);
        logger.info("Successfully updated Firebase user: " + userRecord.getUid());
        return userRecord;
    }

    /**
     * Update Firebase user
     *
     * @param uuid Universal Unique identifier
     * @param displayName Name
     * @param mail Email
     * @param password Password
     * @param phone Phone
     * @param photo Photo
     * @return Firebase User
     * @throws FirebaseAuthException Exception
     */
    protected UserRecord createFirebaseUser(String uuid, String displayName, String mail, String password, String phone, String photo) throws FirebaseAuthException {
        UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setUid(uuid)
                .setEmail(mail)
                .setEmailVerified(false)
                .setPassword(password)
                .setPhoneNumber(Converter.phoneToE164(phone, "ES"))
                .setDisplayName(displayName)
                .setDisabled(false);

        UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
        logger.info("Successfully created new Firebase user: " + userRecord.getUid());
        return userRecord;
    }


}