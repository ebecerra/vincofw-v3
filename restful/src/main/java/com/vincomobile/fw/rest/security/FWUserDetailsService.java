package com.vincomobile.fw.rest.security;

import com.google.common.base.Preconditions;
import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.model.AdPrincipal;
import com.vincomobile.fw.core.persistence.model.AdRolePriv;
import com.vincomobile.fw.core.persistence.model.AdUser;
import com.vincomobile.fw.core.persistence.services.AdUserLogService;
import com.vincomobile.fw.core.persistence.services.AdUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Database user authentication service.
 */
@Component
public class FWUserDetailsService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(FWUserDetailsService.class);

    @Autowired
    protected AdUserService userService;

    public FWUserDetailsService() {
        super();
    }

    // API - public

    /**
     * Loads the user from the datastore, by it's user name <br>
     */
    @Override
    public UserDetails loadUserByUsername(final String username) {
        Preconditions.checkNotNull(username);

        try {
            final AdPrincipal principal = getPrincipal(username);
            if (principal == null) {
                throw new UsernameNotFoundException("Username was not found: " + username);
            }
            FWConfig.session.updateUser(principal);
            final List<AdRolePriv> privilegesOfUser = userService.getPrivileges(principal.getRoleId());
            final List<GrantedAuthority> auths = AuthorityUtils.createAuthorityList();
            for (AdRolePriv rolePriv : privilegesOfUser) {
                auths.add(new SimpleGrantedAuthority(rolePriv.getPrivilege().getName()));
            }
            return new User(principal.getName(), principal.getPassword(), auths);
        } catch(Throwable e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    /**
     * Get principal for user
     *
     * @param username User name (Login)
     * @return Principal
     */
    protected AdPrincipal getPrincipal(final String username) {
        FWUserInfo userInfo = getUserInfo(username);
        String login = userInfo.getUsername();
        AdUser user = userService.findByLogin(login);
        AdPrincipal principal = null;
        if (user != null) {
            principal = new AdPrincipal();
            principal.setRoleId(userInfo.getIdRole() == null ? user.getDefaultIdRole() : userInfo.getIdRole());
            principal.setPassword(user.getPassword());
            principal.setName(username);
            principal.setUserId(user.getIdUser());
            principal.setUserName(user.getName());
            principal.setUserType(AdUserLogService.USER_TYPE_AD_USER);
        }
        return principal;
    }

    /**
     * Get user information
     *
     * @param value String in following formats
     *              1. username
     *              2. idRole$username
     * @return User information
     */
    protected FWUserInfo getUserInfo(String value) {
        if (value != null && value.length() > 32 && value.indexOf("$") == 32) {
            return new FWUserInfo(value.substring(33), value.substring(0, 32));
        } else {
            return new FWUserInfo(value);
        }
    }
}
