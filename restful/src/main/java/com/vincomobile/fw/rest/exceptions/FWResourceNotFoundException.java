package com.vincomobile.fw.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( value = HttpStatus.NOT_FOUND )
public final class FWResourceNotFoundException extends RuntimeException {
    private String id;

    public FWResourceNotFoundException() {
        super("404 Resource not found");
    }

    public FWResourceNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FWResourceNotFoundException(final String message) {
        super(message);
    }

    public FWResourceNotFoundException(final Throwable cause) {
        super(cause);
    }

    public FWResourceNotFoundException(String message, String id) {
        this(message);

        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
