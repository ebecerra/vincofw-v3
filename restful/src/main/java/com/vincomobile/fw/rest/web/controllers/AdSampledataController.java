package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.model.AdSampledata;
import com.vincomobile.fw.core.persistence.services.AdSampledataService;

import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.core.persistence.services.PageSearch;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.rest.web.tools.ControllerResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.Assert;
import javax.validation.Valid;
import javax.inject.Inject;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Controller for table ad_sampledata
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_sampledata/{idClient}")
class AdSampledataController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdSampledataController.class);

    private final AdSampledataService service;

    @Inject
    public AdSampledataController(AdSampledataService service) {
        Assert.notNull(service, "AdSampledataService must not be null");
        this.service = service;
    }

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdSampledata> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdSampledata.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdSampledata get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdSampledata entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdSampledata entity) {
        logger.debug("PUT update(" + entity + ")");
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

}