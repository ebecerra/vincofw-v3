package com.vincomobile.fw.rest.web.error;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Petri Kainulainen
 */
public class ValidationFieldError {

    private List<FieldErrorMessage> fieldErrors = new ArrayList<FieldErrorMessage>();

    public ValidationFieldError() {

    }

    public void addFieldError(String path, String message) {
        FieldErrorMessage fieldError = new FieldErrorMessage(path, message);
        fieldErrors.add(fieldError);
    }

    public List<FieldErrorMessage> getFieldErrors() {
        return fieldErrors;
    }
}
