package com.vincomobile.fw.rest.web.tools;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ControllerResult implements Serializable {

    protected boolean success = true;
    protected String message = null;
    protected Long errCode = 0L;

    public ControllerResult(Long errCode, String message) {
        this.message = message;
        this.errCode = errCode;
    }

    public ControllerResult(String message) {
        this.message = message;
    }

    public ControllerResult() {
    }

    protected Map<String, Object> properties = new HashMap<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
        this.message = null;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public void put(String name, Object value) {
        properties.put(name, value);
    }

    public Object get(String name) {
        return properties.get(name);
    }

    public void remove(String name) {
        properties.remove(name);
    }

    public void setError(String error) {
        this.success = false;
        this.message = error;
    }

    public void setError(Long errCode, String error) {
        this.success = false;
        this.message = error;
        this.errCode = errCode;
    }

    public Long getErrCode() {
        return errCode;
    }

    public void setErrCode(Long errCode) {
        this.success = false;
        this.errCode = errCode;
    }
}
