package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.hooks.HookResult;
import com.vincomobile.fw.core.persistence.beans.AdChartResult;
import com.vincomobile.fw.core.persistence.model.AdChart;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.core.business.ValidationError;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vincomobile FW on 28/09/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Controller for table ad_chart
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_chart/{idClient}")
public class AdChartController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdChartController.class);

    @Autowired
    AdChartService service;

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdChart> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list("+constraints+")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdChart.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdChart get(@PathVariable("id") String id) {
        logger.debug("GET get("+id+")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdChart entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdChart entity) {
        logger.debug("PUT update(" + entity + ")");
        entity.setReference(null);
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/evaluate/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_EXEC_CHART)
    public AdChartResult evaluate(
            @PathVariable("id") String id,
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idUserLogged", required = false) String idUserLogged,
            @RequestParam(value = "idLanguage", required = false, defaultValue = FWConfig.DEFUALT_LANGUAGE_ID) String idLanguage,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET evaluate(" + constraints + ")");
        HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_GET_EXTRA_PARAMS, FWConfig.HOOK_GET_EXTRA_PARAMS_MODE_LOGGEDID, idUserLogged);
        if (hookResult.values.containsKey(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY)) {
            constraints += "," + hookResult.values.get(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY);
        }
        PageSearch pageReq = new PageSearch();
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdChart.class);
        return service.evaluate(idClient, id, idLanguage, pageReq.getSearchs());
    }

    @RequestMapping(value = "/evaluate_list/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_EXEC_CHART)
    public AdChartResult evaluateList(
            @PathVariable("id") String id,
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idUserLogged", required = false) String idUserLogged,
            @RequestParam(value = "idLanguage", required = false, defaultValue = FWConfig.DEFUALT_LANGUAGE_ID) String idLanguage,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET evaluate(" + constraints + ")");
        HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_GET_EXTRA_PARAMS, FWConfig.HOOK_GET_EXTRA_PARAMS_MODE_LOGGEDID, idUserLogged);
        if (hookResult.values.containsKey(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY)) {
            constraints += "," + hookResult.values.get(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY);
        }
        PageSearch pageReq = new PageSearch();
        pageReq.parseConstraints(constraints+getClientConstraint(idClient), AdChart.class);
        return service.evaluate(idClient, id, idLanguage, pageReq.getSearchs(), page, sort);
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdChart bean = (AdChart) entity;
        List<ValidationError> result = new ArrayList<ValidationError>();
        if (AdChartService.TYPE_PIE.equals(bean.getCtype()) || AdChartService.TYPE_EASY_PIE.equals(bean.getCtype()) || AdChartService.TYPE_LIST.equals(bean.getCtype())) {
            if (AdChartService.TYPE_PIE.equals(bean.getCtype()) && !AdChartService.MODE_CUSTOM.equals(bean.getMode()) && Converter.isEmpty(bean.getValue1())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "value1", ValidationError.CODE_NOT_NULL, ""));
            }
            if (AdChartService.TYPE_LIST.equals(bean.getCtype())) {
                bean.setValue1(null);
                if (!AdChartService.MODE_NORMAL.equals(bean.getMode()) && !AdChartService.MODE_CUSTOM.equals(bean.getMode())) {
                    result.add(new ValidationError(ValidationError.TYPE_GLOBAL, "", ValidationError.CODE_MESSAGE, "AD_ChartErrValidationTypeList"));
                }
                if (Converter.isEmpty(bean.getRowLimit())) {
                    result.add(new ValidationError(ValidationError.TYPE_FIELD, "rowLimit", ValidationError.CODE_NOT_NULL, ""));
                }
            }
            if (!AdChartService.TYPE_PIE.equals(bean.getCtype())) {
                bean.setTitleX(null);
            }
            bean.setTitleY(null);
            bean.setShowTitleX(false);
            bean.setShowTitleY(false);
        } else {
            if (Converter.isEmpty(bean.getTitleX())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "titleX", ValidationError.CODE_NOT_NULL, ""));
            }
            if (Converter.isEmpty(bean.getTitleY())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "titleY", ValidationError.CODE_NOT_NULL, ""));
            }
            if (Converter.isEmpty(bean.getValue1()) && !AdChartService.MODE_CUSTOM.equals(bean.getMode())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "value1", ValidationError.CODE_NOT_NULL, ""));
            }
        }
        if (AdChartService.MODE_CUSTOM.equals(bean.getMode())) {
            if (Converter.isEmpty(bean.getClassname())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "classname", ValidationError.CODE_NOT_NULL, ""));
            }
            if (Converter.isEmpty(bean.getClassmethod())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "classmethod", ValidationError.CODE_NOT_NULL, ""));
            }
        } else {
            bean.setClassname(null);
            bean.setClassmethod(null);
            if (Converter.isEmpty(bean.getXfrom())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "xfrom", ValidationError.CODE_NOT_NULL, ""));
            }
            if (Converter.isEmpty(bean.getXwhere())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "xwhere", ValidationError.CODE_NOT_NULL, ""));
            }
        }
        return result;
    }

}