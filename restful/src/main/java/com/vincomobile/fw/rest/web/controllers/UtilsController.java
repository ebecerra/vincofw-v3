package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.services.AdUserService;
import com.vincomobile.fw.core.persistence.services.PageSearch;
import com.vincomobile.fw.core.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "vinco_core/utils")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class UtilsController {

    private Logger logger = LoggerFactory.getLogger(UtilsController.class);

    @Autowired
    AdUserService usuariosService;

    /**
     * Devolver el máximo valor de un campo de una tabla
     *
     * @param table       Nombre de tabla
     * @param column      Columna
     * @param constraints Condiciones de filtrado
     * @return Valor máximo
     */
    @RequestMapping(value = "/max_value", method = RequestMethod.GET)
    @ResponseBody
    public Object max(
            @RequestParam(value = "table") String table,
            @RequestParam(value = "column") String column,
            @RequestParam(value = "q", required = false, defaultValue = "") String constraints
    ) {
        logger.debug("GET max(" + table + "," + column + ")");
        PageSearch pageReq = new PageSearch();
        pageReq.parseConstraints(constraints, Object.class);
        String where = usuariosService.buildWhere(pageReq.getSearchs());
        where = Converter.isEmpty(where) ? "" : " where " + where;
        List result = usuariosService.nativeQuery("SELECT MAX(" + column + ") FROM " + table + where, pageReq.getSearchs(), 0, 1);
        if (result.size() > 0 && result.get(0)!=null)
            return result.get(0);
        return (double) 0;
    }
}
