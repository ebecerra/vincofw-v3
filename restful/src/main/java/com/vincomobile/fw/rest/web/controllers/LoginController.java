package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.services.AdRoleService;
import com.vincomobile.fw.core.persistence.services.AdUserRolesService;
import com.vincomobile.fw.core.persistence.services.AdUserService;
import com.vincomobile.fw.rest.security.AuthenticationFacade;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "vinco_core/login")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class LoginController extends BaseLoginController {

    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    public LoginController(AdUserService usersService, AdRoleService roleService, AdUserRolesService userRolesService, AuthenticationFacade authentication) {
        super(usersService, roleService, userRolesService, authentication);
    }

    @RequestMapping(value = "/privileges", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_LOGIN)
    public ControllerResult privileges(@RequestParam(value = "login") String login) {
        logger.debug("GET privileges(" + login + ")");
        return login(login);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_LOGIN)
    public ControllerResult logout(HttpServletRequest httpRequest) {
        logger.debug("GET logout()");
        return doLogout(httpRequest);
    }

}