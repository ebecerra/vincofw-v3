package com.vincomobile.fw.rest.security;

public class FWUserInfo {

    String username;
    String idRole;

    public FWUserInfo(String username) {
        this.username = username;
        this.idRole = null;
    }

    public FWUserInfo(String username, String idRole) {
        this.username = username;
        this.idRole = idRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }
}
