package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.model.AdFieldGrid;
import com.vincomobile.fw.core.persistence.services.AdFieldGridService;

import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.core.persistence.services.PageSearch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

/**
 * Created by Vincomobile FW on 02/06/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Controller for table ad_field_grid
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_field_grid/{idClient}")
public class AdFieldGridController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdFieldGridController.class);

    @Autowired
    AdFieldGridService service;

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdFieldGrid> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdFieldGrid.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdFieldGrid get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdFieldGrid entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdFieldGrid entity) {
        logger.debug("PUT update(" + entity + ")");
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/columns", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdFieldGrid> columns(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idTab") String idTab,
            @RequestParam(value = "idUser") String idUser,
            @RequestParam(value = "idLanguage") String idLanguage
    ) {
        logger.debug("GET columns(" + idTab +", " + idUser + ")");
        return service.getColumns(idClient, idTab, idUser, idLanguage);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult save(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idTab") String idTab,
            @RequestParam(value = "idUser") String idUser,
            @RequestParam(value = "fields") String fields
    ) {
        logger.debug("GET save(" + idTab +", " + idUser + ", " + fields + ")");
        service.saveColumns(idClient, idTab, idUser, fields.split(";"));
        return new ControllerResult();
    }
}