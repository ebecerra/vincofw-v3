package com.vincomobile.fw.rest.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.hooks.HookResult;
import com.vincomobile.fw.core.persistence.exception.FWProcessPlanningException;
import com.vincomobile.fw.core.persistence.model.AdApproval;
import com.vincomobile.fw.core.persistence.model.AdProcess;
import com.vincomobile.fw.core.persistence.model.AdProcessParam;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.process.ProcessExecError;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.rest.security.FWSecurityConstants;
import com.vincomobile.fw.rest.web.business.SortParam;
import com.vincomobile.fw.core.business.ValidationError;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

/**
 * Created by Devtools.
 * Controller para la tabla ad_process
 * <p/>
 * Date: 06/11/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_process/{idClient}")
public class AdProcessController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdProcessController.class);

    @Autowired
    AdProcessService service;

    @Autowired
    AdProcessParamService processParamService;

    @Autowired
    AdUserService adUserService;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdProcess> list(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "idLanguage", required = false) String idLanguage,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdProcess.class);
        Page<AdProcess> result =  service.findAll(pageReq);
        if (!Converter.isEmpty(idLanguage)) {
            loadTranslations(result.getContent(), idClient, idLanguage);
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdProcess get(@PathVariable("idClient") String idClient,
                         @PathVariable("id") String id
    ) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdProcess entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(
            @PathVariable("idClient") String idClient,
            @RequestBody @Valid AdProcess entity
    ) {
        logger.debug("PUT update(" + entity + ")");
        AdProcess item = service.findById(entity.getId());
        boolean oldActive = item.getActive();
        entity.setClient(null);
        updateEntity(entity);
        if (AdProcessService.TYPE_PROCESS.equals(item.getPtype()) && oldActive != entity.getActive()) {
            service.onChangeActive(item, getAuthUser(), idClient, entity.getActive());
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/exec/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_EXEC_REPORT)
    public ControllerResult exec(
            @PathVariable("id") String id,
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idUserLogged", required = false) String idUserLogged,
            @RequestParam(value = "idLanguage", required = false) String idLanguage,
            @RequestParam(value = "params") String params,
            @RequestBody(required = false) List<AdApproval> approvals,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        logger.debug("GET exec(" + id + ", " + params + ")");
        AdProcess process = service.findById(id);
        RestPreconditions.checkNotNull(process);
        if (approvals != null) {
            approvalService.saveApprovals(getAuthUser(), approvals);
        }
        HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_GET_EXTRA_PARAMS, FWConfig.HOOK_GET_EXTRA_PARAMS_MODE_LOGGEDID, idUserLogged);
        if (hookResult.values.containsKey(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY)) {
            params += "," + hookResult.values.get(FWConfig.HOOK_GET_EXTRA_PARAMS_KEY);
        }
        List<AdProcessParam> paramList = service.buildParamsList(process, params);

        ProcessExecError exec = service.exec(process, getAuthUser(), idClient, idLanguage, paramList, false, request, response);
        ControllerResult result = new ControllerResult();
        if (!exec.isSuccess())
            result.setError(exec.getError());
        return result;
    }

    /**
     * Executes a process that has some of its parameters of type MultipartFile
     *
     * @param id          Process ID
     * @param idClient    Client ID
     * @param idLanguage  Language ID
     * @param params      Process parameters (key=value)
     * @param fileMapping Process file mapping. A mapping that will match a filename with a process' parameter.
     *                    It will be a set of key-values in the form (param_name=file_name)
     * @param files       Parameter files
     * @param request     Request
     * @param response    Response
     * @return ControllerResult
     */
    @RequestMapping(value = "/exec_file/{id}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_EXEC_REPORT)
    public ControllerResult exec_file(
            @PathVariable("id") String id,
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idLanguage", required = false) String idLanguage,
            @RequestParam(value = "params", required = false, defaultValue = "") String params,
            @RequestParam(value = "approvals", required = false) String approvals,
            @RequestParam(value = "fileMapping") String fileMapping,
            @RequestParam(value = "file") MultipartFile[] files,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        logger.debug("GET exec_file(" + id + ")");
        AdProcess process = service.findById(id);
        RestPreconditions.checkNotNull(process);
        if (approvals != null) {
            try {
                AdApproval[] approvalArray = new ObjectMapper().readValue(approvals, AdApproval[].class);
                List<AdApproval> approvalList = new ArrayList<>();
                Collections.addAll(approvalList, approvalArray);
                approvalService.saveApprovals(getAuthUser(), approvalList);
            } catch (IOException e) {
                logger.error("Could not deserialize approval information: " + approvals);
            }
        }
        List<AdProcessParam> paramList = service.buildParamsList(process, params);
        List<AdProcessParam> paramListFiles = buildParamsFileList(process, fileMapping, files);
        paramList.addAll(paramListFiles);
        ProcessExecError exec = service.exec(process, getAuthUser(), idClient, idLanguage, paramList, false, request, response);
        ControllerResult result = new ControllerResult();
        if (!exec.isSuccess())
            result.setError(exec.getError());
        return result;
    }

    @RequestMapping(value = "/schedule/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_EXEC_REPORT)
    public ControllerResult schedule(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id,
            @RequestParam(value = "idLanguage", required = false) String idLanguage,
            @RequestParam(value = "idUser", required = false) String idUser,
            @RequestParam(value = "params", required = false, defaultValue = "") String params,
            @RequestParam(value = "cron") String cron
    ) {
        logger.debug("POST schedule(" + id + ", " + cron + ")");
        ControllerResult result = new ControllerResult();
        if (FWConfig.runtimeExcludeQuartz) {
            result.setError("AD_ErrProcessPlanningNotAllow");
        } else {
            AdProcess process = service.findById(id);
            RestPreconditions.checkNotNull(process);
            try {
                service.schedule(process, cron, idUser != null ? adUserService.findById((idUser)) : getAuthUser(), idClient, idLanguage, params);
            } catch (FWProcessPlanningException e) {
                result.setErrCode((long) HttpStatus.BAD_REQUEST.value());
                result.setSuccess(false);
                switch (e.getReasonCode()) {
                    case PROCESS_WILL_NEVER_FIRE:
                        result.setError("AD_ErrProcessPlanningWontFire");
                        break;
                    default:
                        result.setError("AD_ErrValidationUnknow");
                        break;
                }
            }
        }
        return result;
    }

    @RequestMapping(value = "/clear_schedule/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void clearSchedule(
            @PathVariable("id") String id
    ) {
        logger.debug("POST clearSchedule(" + id + ")");
        if (!FWConfig.runtimeExcludeQuartz) {
            AdProcess process = service.findById(id);
            RestPreconditions.checkNotNull(process);
            service.unschedule(process);
        }
    }

    /**
     * Build a param list from arguments
     *
     * @param process     Process
     * @param fileMapping File mapping when parameters are files
     * @return Param list
     */
    private List<AdProcessParam> buildParamsFileList(AdProcess process, String fileMapping, MultipartFile[] files) {
        List<AdProcessParam> result = new ArrayList<>();
        // Add input params
        PageSearch pageReq = new PageSearch();
        pageReq.parseConstraints(fileMapping, AdProcess.class);
        for (String key : pageReq.getSearchs().keySet()) {
            Map<String, Object> filter = new HashMap<>();
            MultipartFile matchingFile = null;
            for (MultipartFile current : files) {
                if (((CommonsMultipartFile) current).getFileItem().getName().equals(pageReq.getSearchs().get(key))) {
                    matchingFile = current;
                }
            }
            if (matchingFile == null) {
                continue;
            }
            filter.put("idProcess", process.getIdProcess());
            filter.put("name", key);
            filter.put("ptype", "IN");
            AdProcessParam param = processParamService.findFirst(filter);
            if (param != null) {
                param.setFile(matchingFile);
                result.add(param);
            } else {
                AdProcessParam newParam = new AdProcessParam();
                newParam.setIdProcess(process.getIdProcess());
                newParam.setPtype("IN");
                newParam.setName(key);
                param.setFile(matchingFile);
                result.add(newParam);
            }
        }
        // Add output params
        Map<String, Object> filter = new HashMap<>();
        filter.put("idProcess", process.getIdProcess());
        filter.put("ptype", "OUT");
        List<AdProcessParam> outParams = processParamService.findAll(filter);
        result.addAll(outParams);
        return result;
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdProcess bean = (AdProcess) entity;
        List<ValidationError> result = new ArrayList<>();
        if (AdProcessService.TYPE_PROCESS.equals(bean.getPtype())) {
            if (Converter.isEmpty(bean.getClassname())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "classname", ValidationError.CODE_NOT_NULL, ""));
            }
            if (Converter.isEmpty(bean.getClassmethod())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "classmethod", ValidationError.CODE_NOT_NULL, ""));
            }
            bean.setEvalSql(null);
            bean.setJrxml(null);
            bean.setJrxmlExcel(null);
            bean.setJrxmlWord(null);
            bean.setJrxml(null);
            bean.setMultiple(null);
            bean.setPdf(null);
            bean.setExcel(null);
            bean.setWord(null);
        } else {
            if (bean.getMultiple()) {
                if (Converter.isEmpty(bean.getEvalSql())) {
                    result.add(new ValidationError(ValidationError.TYPE_FIELD, "evalSql", ValidationError.CODE_NOT_NULL, ""));
                }
            } else {
                bean.setEvalSql(null);
            }
            if ((bean.getPdf() || bean.getHtml()) && Converter.isEmpty(bean.getJrxml())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "jrxml", ValidationError.CODE_NOT_NULL, ""));
            }
            if (bean.getExcel() && Converter.isEmpty(bean.getJrxml()) && Converter.isEmpty(bean.getJrxmlExcel())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "jrxmlExcel", ValidationError.CODE_NOT_NULL, ""));
            }
            if (bean.getWord() && Converter.isEmpty(bean.getJrxml()) && Converter.isEmpty(bean.getJrxmlWord())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "jrxmlWord", ValidationError.CODE_NOT_NULL, ""));
            }
            bean.setClassname(null);
            bean.setClassmethod(null);
        }
        if (bean.getShowConfirm() && Converter.isEmpty(bean.getConfirmMsg())) {
            result.add(new ValidationError(ValidationError.TYPE_FIELD, "confirmMsg", ValidationError.CODE_NOT_NULL, ""));
        }
        return result;
    }

}