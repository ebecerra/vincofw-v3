package com.vincomobile.fw.rest.web.controllers;

import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdTable;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import com.vincomobile.fw.core.persistence.services.AdTableService;
import com.vincomobile.fw.core.persistence.services.BaseService;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.FileTool;
import com.vincomobile.fw.core.tools.FileTypeDetector;
import com.vincomobile.fw.rest.web.tools.ControllerResult;
import com.vincomobile.fw.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

@Controller
@RequestMapping(value = "vinco_core/ckeditor/{idClient}")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class CKEditorController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(CKEditorController.class);

    @Autowired
    AdTableService adTableService;

    @Override
    public BaseService getService() {
        return null;
    }

    @RequestMapping(value = "/upload/{idTable}/{id}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @ResponseBody
    @Transactional
    public String upload(
            @PathVariable("idClient") String idClient,
            @PathVariable("idTable") String idTable,
            @PathVariable(value = "id") String id,
            @RequestParam(value = "upload", required = false) MultipartFile file,
            @RequestParam(value = "contentType", required = false) String contentType,
            @RequestParam(value = "maxSize", required = false, defaultValue = "0") Long maxSize,
            @RequestParam(value = "baseUrl", required = false, defaultValue = "") String baseUrl,
            @RequestParam(value = "CKEditorFuncNum", required = false, defaultValue = "1") Long ckEditorFuncNum,
            HttpServletResponse response
    ) {
        logger.debug("POST upload(" + file.getOriginalFilename() + ", " + id + ")");
        String returnPath = baseUrl;
        String message = "";
        AdTable table = adTableService.findById(idTable);
        RestPreconditions.checkNotNull(table);
        if (!Converter.isEmpty(id)) {
            ControllerResult result = validFileUpload(file, contentType, maxSize);
            if (result.isSuccess()) {
                String fileName = FileTool.cleanFileName(file.getOriginalFilename());
                returnPath += fileName;
                ControllerResult res = saveFile(idClient, table.getName(), id, contentType, null, fileName, null, file);
                if (!res.isSuccess()) {
                    message = res.getMessage();
                } else {
                    result.put("text", "/files/email/" + fileName);
                }
            }
        } else {
            message = "Debe salvar antes de poder insertar imagenes";
        }
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        return "<script>window.parent.CKEDITOR.tools.callFunction(" + ckEditorFuncNum + ", '" + returnPath + "', '" + message + "');</script>";
    }

    @RequestMapping(value = "/encode/{idTable}/{id}/{fileName}.{ext}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<byte[]> encode(
            @PathVariable("idClient") String idClient,
            @PathVariable(value = "id") String id,
            @PathVariable("idTable") String idTable,
            @PathVariable("fileName") String fileName,
            @PathVariable(value = "ext") String ext
    ) {
        logger.debug("GET encode(" + id + ", " + ", " + idTable + ", " + fileName + "." + ext + ")");

        String baserDir = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        final HttpHeaders headers = new HttpHeaders();
        AdTable table = adTableService.findById(idTable);
        if (!Converter.isEmpty(baserDir)) {
            try {
                String path = baserDir + "/files/" + table.getName() + "/" + (id == null ? "" : id + "/") + fileName + "." + ext;
                File imageFile = new File(path);
                String mime = new FileTypeDetector().probeContentType(Paths.get(path));
                org.springframework.http.MediaType mimeType = org.springframework.http.MediaType.valueOf(mime);
                RenderedImage image = ImageIO.read(imageFile);
                java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream();
                ImageIO.write(image, mimeType.getSubtype(), os);
                byte[] resultBA = os.toByteArray();
                headers.setContentType(mimeType);
                return new ResponseEntity<>(resultBA, headers, HttpStatus.CREATED);
            } catch (IOException e) {
                logger.error("Error al cargar/codificar la imagen: " + fileName + "." + ext);
            }
        }
        return new ResponseEntity<>(new byte[0], headers, HttpStatus.NOT_FOUND);
    }
}
