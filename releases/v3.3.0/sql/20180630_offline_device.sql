ALTER TABLE `ad_offline_device`
CHANGE COLUMN `device` `device_name` VARCHAR(50) NOT NULL COMMENT 'Device name' ,
ADD COLUMN `device_model` VARCHAR(100) NULL COMMENT 'Device model' AFTER `device_name`;

UPDATE ad_offline_device SET device_model = device_name;
UPDATE ad_offline_device SET device_name = '-';

ALTER TABLE `ad_offline_device`
CHANGE COLUMN `device_model` `device_model` VARCHAR(100) NOT NULL COMMENT 'Device model' ;
