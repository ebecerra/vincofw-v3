ALTER TABLE `ad_preference`
  ADD COLUMN `visibleat_custom` VARCHAR(150) NULL COMMENT 'Visible for custom validations' AFTER `visibleat_environment`;
