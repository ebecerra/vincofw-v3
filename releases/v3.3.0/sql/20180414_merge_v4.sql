ALTER TABLE `ad_file`
  CHANGE COLUMN `name` `name` VARCHAR(250) NOT NULL COMMENT 'Name' ,
  CHANGE COLUMN `caption` `caption` VARCHAR(500) NULL DEFAULT NULL COMMENT 'Caption for file link' ,
  CHANGE COLUMN `description` `description` VARCHAR(4000) NULL DEFAULT NULL COMMENT 'Description' ;

UPDATE `ad_column` SET `link_parent`=1 WHERE `id_column`='ff80808152e8a7420152e8a936380013';
