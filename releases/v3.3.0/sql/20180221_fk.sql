ALTER TABLE `ad_approval` 
DROP FOREIGN KEY `fk_ad_ad_approval_ad_process`,
DROP FOREIGN KEY `fk_ad_ad_approval_ad_table`,
DROP FOREIGN KEY `fk_ad_ad_approval_ad_table_action`,
DROP FOREIGN KEY `fk_ad_ad_approval_ad_user`,
DROP FOREIGN KEY `fk_ad_ad_approval_ad_user_createdby`,
DROP FOREIGN KEY `fk_ad_ad_approval_ad_user_updatedby`;
ALTER TABLE `ad_approval` 
ADD CONSTRAINT `fk_ad_approval_ad_process`
  FOREIGN KEY (`id_process`)
  REFERENCES `ad_process` (`id_process`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
ADD CONSTRAINT `fk_ad_approval_ad_table`
  FOREIGN KEY (`id_table`)
  REFERENCES `ad_table` (`id_table`),
ADD CONSTRAINT `fk_ad_approval_ad_table_action`
  FOREIGN KEY (`id_table_action`)
  REFERENCES `ad_table_action` (`id_table_action`),
ADD CONSTRAINT `fk_ad_approval_ad_user`
  FOREIGN KEY (`id_user`)
  REFERENCES `ad_user` (`id_user`)
  ON UPDATE RESTRICT,
ADD CONSTRAINT `fk_ad_approval_ad_user_createdby`
  FOREIGN KEY (`created_by`)
  REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ad_approval_ad_user_updatedby`
  FOREIGN KEY (`updated_by`)
  REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_audit`
  DROP FOREIGN KEY `fk_ad_ad_audit_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_audit_ad_user_updatedby`;
ALTER TABLE `ad_audit`
  ADD CONSTRAINT `fk_ad_audit_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_audit_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_aux_input`
  DROP FOREIGN KEY `fk_ad_ad_aux_input_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_aux_input_ad_user_updatedby`;
ALTER TABLE `ad_aux_input`
  ADD CONSTRAINT `fk_ad_aux_input_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_aux_input_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_characteristic`
  DROP FOREIGN KEY `fk_ad_ad_characteristic_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_characteristic_ad_user_updatedby`;
ALTER TABLE `ad_characteristic`
  ADD CONSTRAINT `fk_ad_characteristic_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_characteristic_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_characteristic_def`
  DROP FOREIGN KEY `fk_ad_ad_characteristic_def_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_characteristic_def_ad_user_updatedby`;
ALTER TABLE `ad_characteristic_def`
  ADD CONSTRAINT `fk_ad_characteristic_def_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_characteristic_def_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_characteristic_value`
  DROP FOREIGN KEY `fk_ad_ad_characteristic_value_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_characteristic_value_ad_user_updatedby`;
ALTER TABLE `ad_characteristic_value`
  ADD CONSTRAINT `fk_ad_characteristic_value_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_characteristic_value_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_chart`
  DROP FOREIGN KEY `fk_ad_ad_chart_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_chart_ad_user_updatedby`;
ALTER TABLE `ad_chart`
  ADD CONSTRAINT `fk_ad_chart_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_chart_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_chart_column`
  DROP FOREIGN KEY `fk_ad_ad_chart_column_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_chart_column_ad_user_updatedby`;
ALTER TABLE `ad_chart_column`
  ADD CONSTRAINT `fk_ad_chart_column_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_chart_column_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_chart_filter`
  DROP FOREIGN KEY `fk_ad_ad_chart_filter_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_chart_filter_ad_user_updatedby`;
ALTER TABLE `ad_chart_filter`
  ADD CONSTRAINT `fk_ad_chart_filter_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_chart_filter_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_chart_linked`
  DROP FOREIGN KEY `fk_ad_ad_chart_linked_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_chart_linked_ad_user_updatedby`;
ALTER TABLE `ad_chart_linked`
  ADD CONSTRAINT `fk_ad_chart_linked_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_chart_linked_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_client`
  DROP FOREIGN KEY `fk_ad_ad_client_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_client_ad_user_updatedby`;
ALTER TABLE `ad_client`
  ADD CONSTRAINT `fk_ad_client_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_client_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_client_language`
  DROP FOREIGN KEY `fk_ad_ad_client_language_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_client_language_ad_user_updatedby`;
ALTER TABLE `ad_client_language`
  ADD CONSTRAINT `fk_ad_client_language_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_client_language_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_client_module`
  DROP FOREIGN KEY `fk_ad_ad_client_module_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_client_module_ad_user_updatedby`;
ALTER TABLE `ad_client_module`
  ADD CONSTRAINT `fk_ad_client_module_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_client_module_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_column`
  DROP FOREIGN KEY `fk_ad_ad_column_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_column_ad_user_updatedby`;
ALTER TABLE `ad_column`
  ADD CONSTRAINT `fk_ad_column_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_column_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_column_sync`
  DROP FOREIGN KEY `fk_ad_ad_column_sync_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_column_sync_ad_user_updatedby`;
ALTER TABLE `ad_column_sync`
  ADD CONSTRAINT `fk_ad_column_sync_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_column_sync_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_dbscript`
  DROP FOREIGN KEY `fk_ad_ad_dbscript_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_dbscript_ad_user_updatedby`;
ALTER TABLE `ad_dbscript`
  ADD CONSTRAINT `fk_ad_dbscript_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_dbscript_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_dbscript_sql`
  DROP FOREIGN KEY `fk_ad_ad_dbscript_sql_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_dbscript_sql_ad_user_updatedby`;
ALTER TABLE `ad_dbscript_sql`
  ADD CONSTRAINT `fk_ad_dbscript_sql_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_dbscript_sql_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_field`
  DROP FOREIGN KEY `fk_ad_ad_field_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_field_ad_user_updatedby`;
ALTER TABLE `ad_field`
  ADD CONSTRAINT `fk_ad_field_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_field_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_field_grid`
  DROP FOREIGN KEY `fk_ad_ad_field_grid_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_field_grid_ad_user_updatedby`;
ALTER TABLE `ad_field_grid`
  ADD CONSTRAINT `fk_ad_field_grid_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_field_grid_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_field_group`
  DROP FOREIGN KEY `fk_ad_ad_field_group_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_field_group_ad_user_updatedby`;
ALTER TABLE `ad_field_group`
  ADD CONSTRAINT `fk_ad_field_group_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_field_group_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_file`
  DROP FOREIGN KEY `fk_ad_ad_file_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_file_ad_user_updatedby`;
ALTER TABLE `ad_file`
  ADD CONSTRAINT `fk_ad_file_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_file_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_html_template`
  DROP FOREIGN KEY `fk_ad_ad_html_template_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_html_template_ad_user_updatedby`;
ALTER TABLE `ad_html_template`
  ADD CONSTRAINT `fk_ad_html_template_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_html_template_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_image`
  DROP FOREIGN KEY `fk_ad_ad_image_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_image_ad_user_updatedby`;
ALTER TABLE `ad_image`
  ADD CONSTRAINT `fk_ad_image_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_image_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_language`
  DROP FOREIGN KEY `fk_ad_ad_language_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_language_ad_user_updatedby`;
ALTER TABLE `ad_language`
  ADD CONSTRAINT `fk_ad_language_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_language_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_menu`
  DROP FOREIGN KEY `fk_ad_ad_menu_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_menu_ad_user_updatedby`;
ALTER TABLE `ad_menu`
  ADD CONSTRAINT `fk_ad_menu_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_menu_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_menu_roles`
  DROP FOREIGN KEY `fk_ad_ad_menu_roles_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_menu_roles_ad_user_updatedby`;
ALTER TABLE `ad_menu_roles`
  ADD CONSTRAINT `fk_ad_menu_roles_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_menu_roles_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_message`
  DROP FOREIGN KEY `fk_ad_ad_message_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_message_ad_user_updatedby`;
ALTER TABLE `ad_message`
  ADD CONSTRAINT `fk_ad_message_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_message_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_module`
  DROP FOREIGN KEY `fk_ad_ad_module_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_module_ad_user_updatedby`;
ALTER TABLE `ad_module`
  ADD CONSTRAINT `fk_ad_module_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_module_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_offline_device`
  DROP FOREIGN KEY `fk_ad_ad_offline_device_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_offline_device_ad_user_updatedby`;
ALTER TABLE `ad_offline_device`
  ADD CONSTRAINT `fk_ad_offline_device_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_offline_device_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_offline_device_log`
  DROP FOREIGN KEY `fk_ad_ad_offline_device_log_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_offline_device_log_ad_user_updatedby`;
ALTER TABLE `ad_offline_device_log`
  ADD CONSTRAINT `fk_ad_offline_device_log_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_offline_device_log_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_preference`
  DROP FOREIGN KEY `fk_ad_ad_preference_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_preference_ad_user_updatedby`;
ALTER TABLE `ad_preference`
  ADD CONSTRAINT `fk_ad_preference_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_preference_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_privilege`
  DROP FOREIGN KEY `fk_ad_ad_privilege_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_privilege_ad_user_updatedby`;
ALTER TABLE `ad_privilege`
  ADD CONSTRAINT `fk_ad_privilege_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_privilege_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_process`
  DROP FOREIGN KEY `fk_ad_ad_process_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_process_ad_user_updatedby`;
ALTER TABLE `ad_process`
  ADD CONSTRAINT `fk_ad_process_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_process_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_process_exec`
  DROP FOREIGN KEY `fk_ad_ad_process_exec_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_process_exec_ad_user_updatedby`;
ALTER TABLE `ad_process_exec`
  ADD CONSTRAINT `fk_ad_process_exec_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_process_exec_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_process_log`
  DROP FOREIGN KEY `fk_ad_ad_process_log_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_process_log_ad_user_updatedby`;
ALTER TABLE `ad_process_log`
  ADD CONSTRAINT `fk_ad_process_log_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_process_log_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_process_output`
  DROP FOREIGN KEY `fk_ad_ad_process_output_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_process_output_ad_user_updatedby`;
ALTER TABLE `ad_process_output`
  ADD CONSTRAINT `fk_ad_process_output_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_process_output_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_process_param`
  DROP FOREIGN KEY `fk_ad_ad_process_param_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_process_param_ad_user_updatedby`;
ALTER TABLE `ad_process_param`
  ADD CONSTRAINT `fk_ad_process_param_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_process_param_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_ref_button`
  DROP FOREIGN KEY `fk_ad_ad_ref_button_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_ref_button_ad_user_updatedby`;
ALTER TABLE `ad_ref_button`
  ADD CONSTRAINT `fk_ad_ref_button_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_ref_button_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_reference`
  DROP FOREIGN KEY `fk_ad_ad_reference_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_reference_ad_user_updatedby`;
ALTER TABLE `ad_reference`
  ADD CONSTRAINT `fk_ad_reference_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_reference_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_ref_list`
  DROP FOREIGN KEY `fk_ad_ad_ref_list_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_ref_list_ad_user_updatedby`;
ALTER TABLE `ad_ref_list`
  ADD CONSTRAINT `fk_ad_ref_list_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_ref_list_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_ref_search_column`
  DROP FOREIGN KEY `fk_ad_ad_ref_search_column_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_ref_search_column_ad_user_updatedby`;
ALTER TABLE `ad_ref_search_column`
  ADD CONSTRAINT `fk_ad_ref_search_column_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_ref_search_column_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_ref_sequence`
  DROP FOREIGN KEY `fk_ad_ad_ref_sequence_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_ref_sequence_ad_user_updatedby`;
ALTER TABLE `ad_ref_sequence`
  ADD CONSTRAINT `fk_ad_ref_sequence_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_ref_sequence_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_ref_table`
  DROP FOREIGN KEY `fk_ad_ad_ref_table_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_ref_table_ad_user_updatedby`;
ALTER TABLE `ad_ref_table`
  ADD CONSTRAINT `fk_ad_ref_table_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_ref_table_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_role`
  DROP FOREIGN KEY `fk_ad_ad_role_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_role_ad_user_updatedby`;
ALTER TABLE `ad_role`
  ADD CONSTRAINT `fk_ad_role_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_role_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_role_priv`
  DROP FOREIGN KEY `fk_ad_ad_role_priv_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_role_priv_ad_user_updatedby`;
ALTER TABLE `ad_role_priv`
  ADD CONSTRAINT `fk_ad_role_priv_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_role_priv_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_tab`
  DROP FOREIGN KEY `fk_ad_ad_tab_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_tab_ad_user_updatedby`;
ALTER TABLE `ad_tab`
  ADD CONSTRAINT `fk_ad_tab_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_tab_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_table`
  DROP FOREIGN KEY `fk_ad_ad_table_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_table_ad_user_updatedby`;
ALTER TABLE `ad_table`
  ADD CONSTRAINT `fk_ad_table_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_table_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_table_action`
  DROP FOREIGN KEY `fk_ad_ad_table_action_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_table_action_ad_user_updatedby`;
ALTER TABLE `ad_table_action`
  ADD CONSTRAINT `fk_ad_table_action_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_table_action_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_table_sync`
  DROP FOREIGN KEY `fk_ad_ad_table_sync_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_table_sync_ad_user_updatedby`;
ALTER TABLE `ad_table_sync`
  ADD CONSTRAINT `fk_ad_table_sync_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_table_sync_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_translation`
  DROP FOREIGN KEY `fk_ad_ad_translation_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_translation_ad_user_updatedby`;
ALTER TABLE `ad_translation`
  ADD CONSTRAINT `fk_ad_translation_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_translation_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_user`
  DROP FOREIGN KEY `fk_ad_ad_user_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_user_ad_user_updatedby`;
ALTER TABLE `ad_user`
  ADD CONSTRAINT `fk_ad_user_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_user_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_user_roles`
  DROP FOREIGN KEY `fk_ad_ad_user_roles_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_user_roles_ad_user_updatedby`;
ALTER TABLE `ad_user_roles`
  ADD CONSTRAINT `fk_ad_user_roles_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_user_roles_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_wiki_entry`
  DROP FOREIGN KEY `fk_ad_ad_wiki_entry_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_wiki_entry_ad_user_updatedby`;
ALTER TABLE `ad_wiki_entry`
  ADD CONSTRAINT `fk_ad_wiki_entry_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_wiki_entry_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_wiki_guide`
  DROP FOREIGN KEY `fk_ad_ad_wiki_guide_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_wiki_guide_ad_user_updatedby`;
ALTER TABLE `ad_wiki_guide`
  ADD CONSTRAINT `fk_ad_wiki_guide_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_wiki_guide_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_wiki_guide_role`
  DROP FOREIGN KEY `fk_ad_ad_wiki_guide_role_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_wiki_guide_role_ad_user_updatedby`;
ALTER TABLE `ad_wiki_guide_role`
  ADD CONSTRAINT `fk_ad_wiki_guide_role_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_wiki_guide_role_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_wiki_topic`
  DROP FOREIGN KEY `fk_ad_ad_wiki_topic_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_wiki_topic_ad_user_updatedby`;
ALTER TABLE `ad_wiki_topic`
  ADD CONSTRAINT `fk_ad_wiki_topic_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_wiki_topic_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;

ALTER TABLE `ad_window`
  DROP FOREIGN KEY `fk_ad_ad_window_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_window_ad_user_updatedby`;
ALTER TABLE `ad_window`
  ADD CONSTRAINT `fk_ad_window_ad_user_createdby`
FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_window_ad_user_updatedby`
FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`)
  ON UPDATE CASCADE;
