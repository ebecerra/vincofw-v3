ALTER TABLE `ad_ref_sequence`
  ADD COLUMN `stype` VARCHAR(50) NULL COMMENT 'Sequence type (List)' AFTER `suffix`;

UPDATE `ad_ref_sequence` set stype = 'STANDARD';

ALTER TABLE `ad_ref_sequence`
  CHANGE COLUMN `stype` `stype` VARCHAR(50) NOT NULL COMMENT 'Sequence type (List)' ;

ALTER TABLE `ad_ref_sequence`
  ADD COLUMN `sorder` VARCHAR(50) NULL COMMENT 'Field order (List)' AFTER `stype`,
  ADD COLUMN `field_separator` VARCHAR(5) NULL COMMENT 'Field separator' AFTER `sorder`,
  ADD COLUMN `left_fill` INT NULL COMMENT 'Number of digit to zero left fill' AFTER `field_separator`,
  ADD COLUMN `syear` INT NULL COMMENT 'Sequence year' AFTER `left_fill`;

UPDATE `ad_ref_sequence` set sorder = 'PREFIX_YEAR_SEQ_SUFIX', left_fill = 0;

ALTER TABLE `ad_ref_sequence`
  CHANGE COLUMN `sorder` `sorder` VARCHAR(50) NOT NULL COMMENT 'Field order (List)' ,
  CHANGE COLUMN `left_fill` `left_fill` INT(11) NOT NULL COMMENT 'Number of digit to zero left fill' ;

ALTER TABLE `ad_ref_sequence`
  CHANGE COLUMN `id_ref_secuence` `id_ref_sequence` VARCHAR(32) NOT NULL COMMENT 'Sequence identifier' ;

ALTER TABLE `ad_ref_sequence`
  ADD COLUMN `smonth` INT(11) NULL COMMENT 'Sequence month' AFTER `syear`,
  ADD COLUMN `sday` INT(11) NULL COMMENT 'Sequence day' AFTER `smonth`;

ALTER TABLE `ad_ref_sequence`
  ADD COLUMN `year_twodigit` BIT(1) NULL COMMENT 'Indicate if year is shown with 2 digit' AFTER `sday`;

ALTER TABLE `ad_ref_sequence`
  ADD COLUMN `year_separator` VARCHAR(5) NULL COMMENT 'Year/Month/Day separator' AFTER `year_twodigit`;

ALTER TABLE `ad_ref_sequence`
  ADD COLUMN `year_order` VARCHAR(50) NULL COMMENT 'Year/Month/Day order (List)' AFTER `year_separator`;


