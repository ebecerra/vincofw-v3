1. Descargar los fonts del repo: git@bitbucket.org:ebecerra/jasper-fonts-v3.git

2. Instalar los .jar en el repo local de Maven

   mvn install:install-file -Dfile=arial/1.0/arial-1.0.jar -DgroupId=com.vincomobile.fw -DartifactId=jasper-font-arial -Dversion=1.0 -Dpackaging=jar
   mvn install:install-file -Dfile=couriernew/1.0/couriernew-1.0.jar -DgroupId=com.vincomobile.fw -DartifactId=jasper-font-couriernew -Dversion=1.0 -Dpackaging=jar
   mvn install:install-file -Dfile=timesnew/1.0/timesnew-1.0.jar -DgroupId=com.vincomobile.fw -DartifactId=jasper-font-timesnew -Dversion=1.0 -Dpackaging=jar
   mvn install:install-file -Dfile=verdana/1.0/verdana-1.0.jar -DgroupId=com.vincomobile.fw -DartifactId=jasper-font-verdana -Dversion=1.0 -Dpackaging=jar
