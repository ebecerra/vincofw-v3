ALTER TABLE `ad_field`
  ADD COLUMN `placeholder` VARCHAR(200) NULL COMMENT 'Placeholder for text fields' AFTER `multi_select`;
