ALTER TABLE `ad_user`
  ADD COLUMN `firebase_token` VARCHAR(32) NULL COMMENT 'Firebase registration token' AFTER `photo`;
