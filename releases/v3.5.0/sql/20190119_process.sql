ALTER TABLE `ad_process`
  ADD COLUMN `show_confirm` BIT(1) NULL COMMENT 'Show a confirmation message before execute process' AFTER `privilege_desc`,
  ADD COLUMN `confirm_msg` VARCHAR(1000) NULL COMMENT 'Confirmation message' AFTER `show_confirm`;

UPDATE `ad_process` SET show_confirm = 0;

ALTER TABLE `ad_process`
  CHANGE COLUMN `show_confirm` `show_confirm` BIT(1) NOT NULL COMMENT 'Show a confirmation message before execute process' ;
