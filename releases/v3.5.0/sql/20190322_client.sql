ALTER TABLE `ad_client`
  ADD COLUMN `nick` VARCHAR(20) NULL COMMENT 'Nick name (Identifier)' AFTER `description`;
