ALTER TABLE `ad_translation`
  ADD UNIQUE INDEX `idx_ad_translation_translation` (`id_client` ASC, `id_language` ASC, `id_table` ASC, `id_column` ASC, `rowkey` ASC);
