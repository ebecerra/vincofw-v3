CREATE TABLE `ad_user_hist_pwd` (
  `id_user_hist_pwd` varchar(32) NOT NULL COMMENT 'User log identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `created_by` varchar(32) NOT NULL DEFAULT '100' COMMENT 'Created by user',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `updated_by` varchar(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_user` varchar(32) NOT NULL COMMENT 'User identifier',
  `passwd` varchar(50) NOT NULL COMMENT 'Password used',
  PRIMARY KEY (`id_user_hist_pwd`),
  UNIQUE KEY `idx_ad_user_hist_pwd_passwd` (`id_client`,`id_user`,`passwd`),
  KEY `idx_ad_user_hist_pwd_ad_client` (`id_client`),
  KEY `idx_ad_user_hist_pwd_ad_user_createdby` (`created_by`),
  KEY `idx_ad_user_hist_pwd_ad_user_updatedby` (`updated_by`),
  CONSTRAINT `fk_ad_user_hist_pwd_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_user_hist_pwd_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_user_hist_pwd_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
