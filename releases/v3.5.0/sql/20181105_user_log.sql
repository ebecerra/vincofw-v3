CREATE TABLE `ad_user_log` (
  `id_user_log` varchar(32) NOT NULL COMMENT 'User log identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `created_by` varchar(32) NOT NULL DEFAULT '100' COMMENT 'Created by user',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `updated_by` varchar(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_user` varchar(32) NOT NULL COMMENT 'User identifier',
  `user_type` varchar(50) NOT NULL COMMENT 'User type (List)',
  `user_agent` varchar(250) NOT NULL COMMENT 'User agent',
  `remote_host` varchar(150) DEFAULT NULL COMMENT 'Remote host',
  `remote_ip` varchar(16) NOT NULL COMMENT 'Remote IP',
  `logout_time` datetime DEFAULT NULL COMMENT 'Logout time',
  `session_id` varchar(32) NOT NULL COMMENT 'HTTP Session identifier',
  PRIMARY KEY (`id_user_log`),
  UNIQUE KEY `idx_ad_user_log_session` (`id_client`,`id_user`,`session_id`),
  KEY `idx_ad_user_log_ad_client` (`id_client`),
  KEY `idx_ad_user_log_ad_user_createdby` (`created_by`),
  KEY `idx_ad_user_log_ad_user_updatedby` (`updated_by`),
  CONSTRAINT `fk_ad_user_log_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_user_log_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_user_log_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
