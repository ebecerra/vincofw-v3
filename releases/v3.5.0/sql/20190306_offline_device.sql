ALTER TABLE `ad_offline_device_log` DROP FOREIGN KEY `fk_ad_offline_device_log_ad_offline_device`;
ALTER TABLE `ad_offline_device_log` ADD CONSTRAINT `fk_ad_offline_device_log_ad_offline_device`
FOREIGN KEY (`id_offline_device`) REFERENCES `ad_offline_device` (`id_offline_device`) ON DELETE CASCADE;
