ALTER TABLE `ad_user`
  ADD COLUMN `password_update` DATE NULL COMMENT 'Last password update' AFTER `password`;

UPDATE `ad_user` SET `password_update` = created;

ALTER TABLE `ad_user`
  CHANGE COLUMN `password_update` `password_update` DATE NOT NULL COMMENT 'Last password update' ;
