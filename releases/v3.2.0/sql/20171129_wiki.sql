CREATE TABLE `ad_wiki_guide` (
  `id_wiki_guide` varchar(32) NOT NULL COMMENT 'Wiki User Guide identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `name` varchar(150) NOT NULL COMMENT 'User Guide developer name',
  `title` varchar(1000) NOT NULL COMMENT 'User Guide title',
  `content` text NOT NULL COMMENT 'User Guide content',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  PRIMARY KEY (`id_wiki_guide`),
  UNIQUE KEY `idx_ad_wiki_guide_name` (`id_client`,`name`),
  KEY `idx_ad_wiki_guide_ad_client` (`id_client`),
  KEY `idx_ad_wiki_guide_ad_module` (`id_module`),
  CONSTRAINT `fk_ad_wiki_guide_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_wiki_guide_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_wiki_topic` (
  `id_wiki_topic` varchar(32) NOT NULL COMMENT 'Wiki Topic identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_wiki_guide` varchar(32) NOT NULL COMMENT 'Wiki User Guide identifier',
  `name` varchar(150) NOT NULL COMMENT 'Topic developer name',
  `title` varchar(1000) NOT NULL COMMENT 'Topic title',
  `parent_topic` varchar(150) DEFAULT NULL COMMENT 'Parent topic (Topic tree)',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  PRIMARY KEY (`id_wiki_topic`),
  UNIQUE KEY `idx_ad_wiki_topic_name` (`id_client`,`name`),
  KEY `idx_ad_wiki_topic_ad_client` (`id_client`),
  KEY `idx_ad_wiki_topic_ad_module` (`id_module`),
  KEY `idx_ad_wiki_topic_ad_wiki_guide` (`id_wiki_guide`),
  CONSTRAINT `fk_ad_wiki_topic_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_wiki_topic_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `fk_ad_wiki_topic_ad_wiki_guide` FOREIGN KEY (`id_wiki_guide`) REFERENCES `ad_wiki_guide` (`id_wiki_guide`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_wiki_entry` (
  `id_wiki_entry` varchar(32) NOT NULL COMMENT 'Wiki Entry identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_wiki_topic` varchar(32) NOT NULL COMMENT 'Wiki Topic identifier',
  `name` varchar(150) NOT NULL COMMENT 'Entry developer name',
  `title` varchar(1000) NOT NULL COMMENT 'Entry title',
  `content` text NOT NULL COMMENT 'Entry content',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  PRIMARY KEY (`id_wiki_entry`),
  UNIQUE KEY `idx_ad_wiki_entry_name` (`id_client`,`name`),
  KEY `idx_ad_wiki_entry_ad_client` (`id_client`),
  KEY `idx_ad_wiki_entry_ad_module` (`id_module`),
  KEY `idx_ad_wiki_entry_ad_wiki_topic` (`id_wiki_topic`),
  CONSTRAINT `fk_ad_wiki_entry_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_wiki_entry_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `fk_ad_wiki_entry_ad_wiki_topic` FOREIGN KEY (`id_wiki_topic`) REFERENCES `ad_wiki_topic` (`id_wiki_topic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

