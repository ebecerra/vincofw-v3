ALTER TABLE `ad_table`
  ADD COLUMN `privilege_desc` TEXT NULL COMMENT 'Privilege description (Optional)' AFTER `privilege`;

ALTER TABLE `ad_process`
  ADD COLUMN `privilege_desc` TEXT NULL COMMENT 'Privilege description (Optional)' AFTER `privilege`;

ALTER TABLE `ad_table_action`
  ADD COLUMN `privilege_desc` TEXT NULL COMMENT 'Privilege description (Optional)' AFTER `privilege`;

