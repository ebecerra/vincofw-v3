ALTER TABLE `ad_table`
ADD COLUMN `privilege` VARCHAR(100) NULL COMMENT 'Privilege need to modify data (Optional)' AFTER `audit`;

ALTER TABLE `ad_process`
  ADD COLUMN `privilege` VARCHAR(100) NULL COMMENT 'Privilege need to execute process (Optional)' AFTER `eval_sql`;

ALTER TABLE `ad_table_action`
  ADD COLUMN `privilege` VARCHAR(100) NULL COMMENT 'Privilege need to execute action (Optional)' AFTER `target`;

