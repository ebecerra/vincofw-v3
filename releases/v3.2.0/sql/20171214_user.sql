ALTER TABLE `ad_user` DROP FOREIGN KEY `fk_ad_ad_user_ad_user_createdby`,
  DROP FOREIGN KEY `fk_ad_ad_user_ad_user_updatedby`;

ALTER TABLE `ad_user`  ADD CONSTRAINT `fk_ad_user_ad_user_createdby` FOREIGN KEY (`created_by`)
REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ad_user_ad_user_updatedby` FOREIGN KEY (`updated_by`)
REFERENCES `ad_user` (`id_user`) ON UPDATE CASCADE;
