ALTER TABLE `ad_table`
  ADD COLUMN `privilege` VARCHAR(100) NULL COMMENT 'Privilege need to modify data (Optional)' AFTER `audit`;

ALTER TABLE `ad_process`
  ADD COLUMN `privilege` VARCHAR(100) NULL COMMENT 'Privilege need to execute process (Optional)' AFTER `eval_sql`;

ALTER TABLE `ad_table_action`
  ADD COLUMN `privilege` VARCHAR(100) NULL COMMENT 'Privilege need to execute action (Optional)' AFTER `target`;

CREATE TABLE `ad_approval` (
  `id_approval` varchar(32) NOT NULL COMMENT 'Approval identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `action` varchar(50) NOT NULL COMMENT 'User action identifier (List)',
  `privilege` varchar(100) NOT NULL COMMENT 'Approval privilege',
  `rowkey` varchar(32) NOT NULL COMMENT 'Row key for approved item',
  `id_user` varchar(32) NOT NULL COMMENT 'User identifier',
  `id_table` varchar(32) DEFAULT NULL COMMENT 'Table identifier',
  `id_table_action` varchar(32) DEFAULT NULL COMMENT 'Table action identifier',
  `id_process` varchar(32) DEFAULT NULL COMMENT 'Process identifier',
  PRIMARY KEY (`id_approval`),
  KEY `idx_ad_approval_ad_client` (`id_client`),
  KEY `idx_ad_approval_ad_user` (`id_user`),
  KEY `idx_ad_approval_ad_table` (`id_table`),
  KEY `idx_ad_approval_ad_table_action` (`id_table_action`),
  KEY `idx_ad_approval_ad_process` (`id_process`),
  CONSTRAINT `fk_ad_ad_approval_ad_process` FOREIGN KEY (`id_process`) REFERENCES `ad_process` (`id_process`),
  CONSTRAINT `fk_ad_ad_approval_ad_table` FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`),
  CONSTRAINT `fk_ad_ad_approval_ad_table_action` FOREIGN KEY (`id_table_action`) REFERENCES `ad_table_action` (`id_table_action`),
  CONSTRAINT `fk_ad_ad_approval_ad_user` FOREIGN KEY (`id_user`) REFERENCES `ad_user` (`id_user`),
  CONSTRAINT `fk_ad_approval_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ad_process`
  CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL COMMENT 'Description' ;

ALTER TABLE `ad_table`
  ADD COLUMN `privilege_desc` TEXT NULL COMMENT 'Privilege description (Optional)' AFTER `privilege`;

ALTER TABLE `ad_process`
  ADD COLUMN `privilege_desc` TEXT NULL COMMENT 'Privilege description (Optional)' AFTER `privilege`;

ALTER TABLE `ad_table_action`
  ADD COLUMN `privilege_desc` TEXT NULL COMMENT 'Privilege description (Optional)' AFTER `privilege`;

CREATE TABLE `ad_wiki_guide` (
  `id_wiki_guide` varchar(32) NOT NULL COMMENT 'Wiki User Guide identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `name` varchar(150) NOT NULL COMMENT 'User Guide developer name',
  `title` varchar(1000) NOT NULL COMMENT 'User Guide title',
  `content` text NOT NULL COMMENT 'User Guide content',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  PRIMARY KEY (`id_wiki_guide`),
  UNIQUE KEY `idx_ad_wiki_guide_name` (`id_client`,`name`),
  KEY `idx_ad_wiki_guide_ad_client` (`id_client`),
  KEY `idx_ad_wiki_guide_ad_module` (`id_module`),
  CONSTRAINT `fk_ad_wiki_guide_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_wiki_guide_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_wiki_topic` (
  `id_wiki_topic` varchar(32) NOT NULL COMMENT 'Wiki Topic identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_wiki_guide` varchar(32) NOT NULL COMMENT 'Wiki User Guide identifier',
  `name` varchar(150) NOT NULL COMMENT 'Topic developer name',
  `title` varchar(1000) NOT NULL COMMENT 'Topic title',
  `parent_topic` varchar(150) DEFAULT NULL COMMENT 'Parent topic (Topic tree)',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  PRIMARY KEY (`id_wiki_topic`),
  UNIQUE KEY `idx_ad_wiki_topic_name` (`id_client`,`name`),
  KEY `idx_ad_wiki_topic_ad_client` (`id_client`),
  KEY `idx_ad_wiki_topic_ad_module` (`id_module`),
  KEY `idx_ad_wiki_topic_ad_wiki_guide` (`id_wiki_guide`),
  CONSTRAINT `fk_ad_wiki_topic_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_wiki_topic_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `fk_ad_wiki_topic_ad_wiki_guide` FOREIGN KEY (`id_wiki_guide`) REFERENCES `ad_wiki_guide` (`id_wiki_guide`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_wiki_entry` (
  `id_wiki_entry` varchar(32) NOT NULL COMMENT 'Wiki Entry identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_wiki_topic` varchar(32) NOT NULL COMMENT 'Wiki Topic identifier',
  `name` varchar(150) NOT NULL COMMENT 'Entry developer name',
  `title` varchar(1000) NOT NULL COMMENT 'Entry title',
  `content` text NOT NULL COMMENT 'Entry content',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  PRIMARY KEY (`id_wiki_entry`),
  UNIQUE KEY `idx_ad_wiki_entry_name` (`id_client`,`name`),
  KEY `idx_ad_wiki_entry_ad_client` (`id_client`),
  KEY `idx_ad_wiki_entry_ad_module` (`id_module`),
  KEY `idx_ad_wiki_entry_ad_wiki_topic` (`id_wiki_topic`),
  CONSTRAINT `fk_ad_wiki_entry_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_wiki_entry_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `fk_ad_wiki_entry_ad_wiki_topic` FOREIGN KEY (`id_wiki_topic`) REFERENCES `ad_wiki_topic` (`id_wiki_topic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_wiki_guide_role` (
  `id_wiki_guide_role` varchar(32) NOT NULL COMMENT 'Wiki User Guide - Role identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_wiki_guide` varchar(32) NOT NULL COMMENT 'Wiki User Guide identifier',
  `id_role` varchar(32) NOT NULL COMMENT 'Role identifier',
  PRIMARY KEY (`id_wiki_guide_role`),
  UNIQUE KEY `idx_ad_wiki_guide_role_role` (`id_wiki_guide`,`id_role`),
  KEY `idx_ad_wiki_guide_role_ad_client` (`id_client`),
  KEY `idx_ad_wiki_guide_role_ad_module` (`id_module`),
  KEY `idx_ad_wiki_guide_role_ad_wiki_guide` (`id_wiki_guide`),
  KEY `idx_ad_wiki_guide_role_ad_role` (`id_role`),
  CONSTRAINT `fk_ad_wiki_guide_role_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_wiki_guide_role_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `fk_ad_wiki_guide_role_ad_role` FOREIGN KEY (`id_role`) REFERENCES `ad_role` (`id_role`),
  CONSTRAINT `fk_ad_wiki_guide_role_ad_wiki_guide` FOREIGN KEY (`id_wiki_guide`) REFERENCES `ad_wiki_guide` (`id_wiki_guide`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UPDATE `ad_user` SET `id_user`='100' WHERE `id_user`='377B40CEEB6AEA449AF22549E10D84A5';

-- ad_approval
ALTER TABLE `ad_approval`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_approval` ADD INDEX `idx_ad_approval_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_approval` ADD INDEX `idx_ad_approval_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_approval` ADD CONSTRAINT `fk_ad_approval_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_approval` ADD CONSTRAINT `fk_ad_approval_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_audit
ALTER TABLE `ad_audit`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_audit` ADD INDEX `idx_ad_audit_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_audit` ADD INDEX `idx_ad_audit_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_audit` ADD CONSTRAINT `fk_ad_audit_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_audit` ADD CONSTRAINT `fk_ad_audit_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_aux_input
ALTER TABLE `ad_aux_input`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_aux_input` ADD INDEX `idx_ad_aux_input_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_aux_input` ADD INDEX `idx_ad_aux_input_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_aux_input` ADD CONSTRAINT `fk_ad_aux_input_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_aux_input` ADD CONSTRAINT `fk_ad_aux_input_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_characteristic
ALTER TABLE `ad_characteristic`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_characteristic` ADD INDEX `idx_ad_characteristic_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_characteristic` ADD INDEX `idx_ad_characteristic_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_characteristic` ADD CONSTRAINT `fk_ad_characteristic_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_characteristic` ADD CONSTRAINT `fk_ad_characteristic_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_characteristic_def
ALTER TABLE `ad_characteristic_def`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_characteristic_def` ADD INDEX `idx_ad_characteristic_def_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_characteristic_def` ADD INDEX `idx_ad_characteristic_def_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_characteristic_def` ADD CONSTRAINT `fk_ad_characteristic_def_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_characteristic_def` ADD CONSTRAINT `fk_ad_characteristic_def_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_characteristic_value
ALTER TABLE `ad_characteristic_value`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_characteristic_value` ADD INDEX `idx_ad_characteristic_value_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_characteristic_value` ADD INDEX `idx_ad_characteristic_value_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_characteristic_value` ADD CONSTRAINT `fk_ad_characteristic_value_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_characteristic_value` ADD CONSTRAINT `fk_ad_characteristic_value_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_chart
ALTER TABLE `ad_chart`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_chart` ADD INDEX `idx_ad_chart_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_chart` ADD INDEX `idx_ad_chart_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_chart` ADD CONSTRAINT `fk_ad_chart_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_chart` ADD CONSTRAINT `fk_ad_chart_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_chart_column
ALTER TABLE `ad_chart_column`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_chart_column` ADD INDEX `idx_ad_chart_column_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_chart_column` ADD INDEX `idx_ad_chart_column_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_chart_column` ADD CONSTRAINT `fk_ad_chart_column_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_chart_column` ADD CONSTRAINT `fk_ad_chart_column_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_chart_filter
ALTER TABLE `ad_chart_filter`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_chart_filter` ADD INDEX `idx_ad_chart_filter_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_chart_filter` ADD INDEX `idx_ad_chart_filter_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_chart_filter` ADD CONSTRAINT `fk_ad_chart_filter_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_chart_filter` ADD CONSTRAINT `fk_ad_chart_filter_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_chart_linked
ALTER TABLE `ad_chart_linked`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_chart_linked` ADD INDEX `idx_ad_chart_linked_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_chart_linked` ADD INDEX `idx_ad_chart_linked_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_chart_linked` ADD CONSTRAINT `fk_ad_chart_linked_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_chart_linked` ADD CONSTRAINT `fk_ad_chart_linked_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_client
ALTER TABLE `ad_client`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_client` ADD INDEX `idx_ad_client_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_client` ADD INDEX `idx_ad_client_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_client` ADD CONSTRAINT `fk_ad_client_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_client` ADD CONSTRAINT `fk_ad_client_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_client_language
ALTER TABLE `ad_client_language`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_client_language` ADD INDEX `idx_ad_client_language_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_client_language` ADD INDEX `idx_ad_client_language_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_client_language` ADD CONSTRAINT `fk_ad_client_language_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_client_language` ADD CONSTRAINT `fk_ad_client_language_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_client_module
ALTER TABLE `ad_client_module`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_client_module` ADD INDEX `idx_ad_client_module_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_client_module` ADD INDEX `idx_ad_client_module_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_client_module` ADD CONSTRAINT `fk_ad_client_module_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_client_module` ADD CONSTRAINT `fk_ad_client_module_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_column
ALTER TABLE `ad_column`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_column` ADD INDEX `idx_ad_column_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_column` ADD INDEX `idx_ad_column_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_column` ADD CONSTRAINT `fk_ad_column_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_column` ADD CONSTRAINT `fk_ad_column_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_column_sync
ALTER TABLE `ad_column_sync`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_column_sync` ADD INDEX `idx_ad_column_sync_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_column_sync` ADD INDEX `idx_ad_column_sync_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_column_sync` ADD CONSTRAINT `fk_ad_column_sync_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_column_sync` ADD CONSTRAINT `fk_ad_column_sync_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_dbscript
ALTER TABLE `ad_dbscript`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_dbscript` ADD INDEX `idx_ad_dbscript_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_dbscript` ADD INDEX `idx_ad_dbscript_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_dbscript` ADD CONSTRAINT `fk_ad_dbscript_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_dbscript` ADD CONSTRAINT `fk_ad_dbscript_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_dbscript_sql
ALTER TABLE `ad_dbscript_sql`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_dbscript_sql` ADD INDEX `idx_ad_dbscript_sql_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_dbscript_sql` ADD INDEX `idx_ad_dbscript_sql_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_dbscript_sql` ADD CONSTRAINT `fk_ad_dbscript_sql_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_dbscript_sql` ADD CONSTRAINT `fk_ad_dbscript_sql_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_field
ALTER TABLE `ad_field`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_field` ADD INDEX `idx_ad_field_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_field` ADD INDEX `idx_ad_field_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_field` ADD CONSTRAINT `fk_ad_field_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_field` ADD CONSTRAINT `fk_ad_field_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_field_grid
ALTER TABLE `ad_field_grid`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_field_grid` ADD INDEX `idx_ad_field_grid_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_field_grid` ADD INDEX `idx_ad_field_grid_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_field_grid` ADD CONSTRAINT `fk_ad_field_grid_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_field_grid` ADD CONSTRAINT `fk_ad_field_grid_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_field_group
ALTER TABLE `ad_field_group`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_field_group` ADD INDEX `idx_ad_field_group_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_field_group` ADD INDEX `idx_ad_field_group_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_field_group` ADD CONSTRAINT `fk_ad_field_group_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_field_group` ADD CONSTRAINT `fk_ad_field_group_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_file
ALTER TABLE `ad_file`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_file` ADD INDEX `idx_ad_file_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_file` ADD INDEX `idx_ad_file_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_file` ADD CONSTRAINT `fk_ad_file_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_file` ADD CONSTRAINT `fk_ad_file_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_html_template
ALTER TABLE `ad_html_template`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_html_template` ADD INDEX `idx_ad_html_template_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_html_template` ADD INDEX `idx_ad_html_template_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_html_template` ADD CONSTRAINT `fk_ad_html_template_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_html_template` ADD CONSTRAINT `fk_ad_html_template_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_image
ALTER TABLE `ad_image`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_image` ADD INDEX `idx_ad_image_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_image` ADD INDEX `idx_ad_image_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_image` ADD CONSTRAINT `fk_ad_image_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_image` ADD CONSTRAINT `fk_ad_image_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_language
ALTER TABLE `ad_language`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_language` ADD INDEX `idx_ad_language_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_language` ADD INDEX `idx_ad_language_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_language` ADD CONSTRAINT `fk_ad_language_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_language` ADD CONSTRAINT `fk_ad_language_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_menu
ALTER TABLE `ad_menu`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_menu` ADD INDEX `idx_ad_menu_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_menu` ADD INDEX `idx_ad_menu_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_menu` ADD CONSTRAINT `fk_ad_menu_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_menu` ADD CONSTRAINT `fk_ad_menu_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_menu_roles
ALTER TABLE `ad_menu_roles`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_menu_roles` ADD INDEX `idx_ad_menu_roles_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_menu_roles` ADD INDEX `idx_ad_menu_roles_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_menu_roles` ADD CONSTRAINT `fk_ad_menu_roles_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_menu_roles` ADD CONSTRAINT `fk_ad_menu_roles_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_message
ALTER TABLE `ad_message`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_message` ADD INDEX `idx_ad_message_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_message` ADD INDEX `idx_ad_message_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_message` ADD CONSTRAINT `fk_ad_message_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_message` ADD CONSTRAINT `fk_ad_message_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_module
ALTER TABLE `ad_module`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_module` ADD INDEX `idx_ad_module_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_module` ADD INDEX `idx_ad_module_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_module` ADD CONSTRAINT `fk_ad_module_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_module` ADD CONSTRAINT `fk_ad_module_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_offline_device
ALTER TABLE `ad_offline_device`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_offline_device` ADD INDEX `idx_ad_offline_device_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_offline_device` ADD INDEX `idx_ad_offline_device_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_offline_device` ADD CONSTRAINT `fk_ad_offline_device_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_offline_device` ADD CONSTRAINT `fk_ad_offline_device_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_offline_device_log
ALTER TABLE `ad_offline_device_log`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_offline_device_log` ADD INDEX `idx_ad_offline_device_log_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_offline_device_log` ADD INDEX `idx_ad_offline_device_log_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_offline_device_log` ADD CONSTRAINT `fk_ad_offline_device_log_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_offline_device_log` ADD CONSTRAINT `fk_ad_offline_device_log_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_preference
ALTER TABLE `ad_preference`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_preference` ADD INDEX `idx_ad_preference_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_preference` ADD INDEX `idx_ad_preference_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_preference` ADD CONSTRAINT `fk_ad_preference_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_preference` ADD CONSTRAINT `fk_ad_preference_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_privilege
ALTER TABLE `ad_privilege`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_privilege` ADD INDEX `idx_ad_privilege_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_privilege` ADD INDEX `idx_ad_privilege_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_privilege` ADD CONSTRAINT `fk_ad_privilege_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_privilege` ADD CONSTRAINT `fk_ad_privilege_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_process
ALTER TABLE `ad_process`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_process` ADD INDEX `idx_ad_process_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_process` ADD INDEX `idx_ad_process_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_process` ADD CONSTRAINT `fk_ad_process_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_process` ADD CONSTRAINT `fk_ad_process_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_process_exec
ALTER TABLE `ad_process_exec`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_process_exec` ADD INDEX `idx_ad_process_exec_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_process_exec` ADD INDEX `idx_ad_process_exec_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_process_exec` ADD CONSTRAINT `fk_ad_process_exec_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_process_exec` ADD CONSTRAINT `fk_ad_process_exec_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_process_log
ALTER TABLE `ad_process_log`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_process_log` ADD INDEX `idx_ad_process_log_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_process_log` ADD INDEX `idx_ad_process_log_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_process_log` ADD CONSTRAINT `fk_ad_process_log_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_process_log` ADD CONSTRAINT `fk_ad_process_log_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_process_output
ALTER TABLE `ad_process_output`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_process_output` ADD INDEX `idx_ad_process_output_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_process_output` ADD INDEX `idx_ad_process_output_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_process_output` ADD CONSTRAINT `fk_ad_process_output_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_process_output` ADD CONSTRAINT `fk_ad_process_output_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_process_param
ALTER TABLE `ad_process_param`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_process_param` ADD INDEX `idx_ad_process_param_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_process_param` ADD INDEX `idx_ad_process_param_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_process_param` ADD CONSTRAINT `fk_ad_process_param_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_process_param` ADD CONSTRAINT `fk_ad_process_param_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_ref_button
ALTER TABLE `ad_ref_button`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_ref_button` ADD INDEX `idx_ad_ref_button_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_ref_button` ADD INDEX `idx_ad_ref_button_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_ref_button` ADD CONSTRAINT `fk_ad_ref_button_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_ref_button` ADD CONSTRAINT `fk_ad_ref_button_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_reference
ALTER TABLE `ad_reference`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_reference` ADD INDEX `idx_ad_reference_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_reference` ADD INDEX `idx_ad_reference_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_reference` ADD CONSTRAINT `fk_ad_reference_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_reference` ADD CONSTRAINT `fk_ad_reference_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_ref_list
ALTER TABLE `ad_ref_list`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_ref_list` ADD INDEX `idx_ad_ref_list_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_ref_list` ADD INDEX `idx_ad_ref_list_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_ref_list` ADD CONSTRAINT `fk_ad_ref_list_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_ref_list` ADD CONSTRAINT `fk_ad_ref_list_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_ref_search_column
ALTER TABLE `ad_ref_search_column`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_ref_search_column` ADD INDEX `idx_ad_ref_search_column_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_ref_search_column` ADD INDEX `idx_ad_ref_search_column_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_ref_search_column` ADD CONSTRAINT `fk_ad_ref_search_column_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_ref_search_column` ADD CONSTRAINT `fk_ad_ref_search_column_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_ref_sequence
ALTER TABLE `ad_ref_sequence`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_ref_sequence` ADD INDEX `idx_ad_ref_sequence_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_ref_sequence` ADD INDEX `idx_ad_ref_sequence_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_ref_sequence` ADD CONSTRAINT `fk_ad_ref_sequence_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_ref_sequence` ADD CONSTRAINT `fk_ad_ref_sequence_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_ref_table
ALTER TABLE `ad_ref_table`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_ref_table` ADD INDEX `idx_ad_ref_table_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_ref_table` ADD INDEX `idx_ad_ref_table_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_ref_table` ADD CONSTRAINT `fk_ad_ref_table_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_ref_table` ADD CONSTRAINT `fk_ad_ref_table_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_role
ALTER TABLE `ad_role`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_role` ADD INDEX `idx_ad_role_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_role` ADD INDEX `idx_ad_role_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_role` ADD CONSTRAINT `fk_ad_role_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_role` ADD CONSTRAINT `fk_ad_role_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_role_priv
ALTER TABLE `ad_role_priv`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_role_priv` ADD INDEX `idx_ad_role_priv_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_role_priv` ADD INDEX `idx_ad_role_priv_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_role_priv` ADD CONSTRAINT `fk_ad_role_priv_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_role_priv` ADD CONSTRAINT `fk_ad_role_priv_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_tab
ALTER TABLE `ad_tab`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_tab` ADD INDEX `idx_ad_tab_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_tab` ADD INDEX `idx_ad_tab_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_tab` ADD CONSTRAINT `fk_ad_tab_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_tab` ADD CONSTRAINT `fk_ad_tab_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_table
ALTER TABLE `ad_table`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_table` ADD INDEX `idx_ad_table_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_table` ADD INDEX `idx_ad_table_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_table` ADD CONSTRAINT `fk_ad_table_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_table` ADD CONSTRAINT `fk_ad_table_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_table_action
ALTER TABLE `ad_table_action`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_table_action` ADD INDEX `idx_ad_table_action_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_table_action` ADD INDEX `idx_ad_table_action_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_table_action` ADD CONSTRAINT `fk_ad_table_action_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_table_action` ADD CONSTRAINT `fk_ad_table_action_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_table_sync
ALTER TABLE `ad_table_sync`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_table_sync` ADD INDEX `idx_ad_table_sync_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_table_sync` ADD INDEX `idx_ad_table_sync_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_table_sync` ADD CONSTRAINT `fk_ad_table_sync_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_table_sync` ADD CONSTRAINT `fk_ad_table_sync_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_translation
ALTER TABLE `ad_translation`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_translation` ADD INDEX `idx_ad_translation_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_translation` ADD INDEX `idx_ad_translation_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_translation` ADD CONSTRAINT `fk_ad_translation_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_translation` ADD CONSTRAINT `fk_ad_translation_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_user
ALTER TABLE `ad_user`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_user` ADD INDEX `idx_ad_user_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_user` ADD INDEX `idx_ad_user_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_user` ADD CONSTRAINT `fk_ad_user_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_user` ADD CONSTRAINT `fk_ad_user_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_user_roles
ALTER TABLE `ad_user_roles`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_user_roles` ADD INDEX `idx_ad_user_roles_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_user_roles` ADD INDEX `idx_ad_user_roles_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_user_roles` ADD CONSTRAINT `fk_ad_user_roles_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_user_roles` ADD CONSTRAINT `fk_ad_user_roles_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_wiki_entry
ALTER TABLE `ad_wiki_entry`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_wiki_entry` ADD INDEX `idx_ad_wiki_entry_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_wiki_entry` ADD INDEX `idx_ad_wiki_entry_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_wiki_entry` ADD CONSTRAINT `fk_ad_wiki_entry_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_wiki_entry` ADD CONSTRAINT `fk_ad_wiki_entry_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_wiki_guide
ALTER TABLE `ad_wiki_guide`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_wiki_guide` ADD INDEX `idx_ad_wiki_guide_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_wiki_guide` ADD INDEX `idx_ad_wiki_guide_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_wiki_guide` ADD CONSTRAINT `fk_ad_wiki_guide_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_wiki_guide` ADD CONSTRAINT `fk_ad_wiki_guide_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_wiki_guide_role
ALTER TABLE `ad_wiki_guide_role`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_wiki_guide_role` ADD INDEX `idx_ad_wiki_guide_role_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_wiki_guide_role` ADD INDEX `idx_ad_wiki_guide_role_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_wiki_guide_role` ADD CONSTRAINT `fk_ad_wiki_guide_role_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_wiki_guide_role` ADD CONSTRAINT `fk_ad_wiki_guide_role_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_wiki_topic
ALTER TABLE `ad_wiki_topic`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_wiki_topic` ADD INDEX `idx_ad_wiki_topic_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_wiki_topic` ADD INDEX `idx_ad_wiki_topic_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_wiki_topic` ADD CONSTRAINT `fk_ad_wiki_topic_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_wiki_topic` ADD CONSTRAINT `fk_ad_wiki_topic_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- ad_window
ALTER TABLE `ad_window`
  ADD COLUMN `created_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Created by user' AFTER `created`,
  ADD COLUMN `updated_by` VARCHAR(32) NOT NULL DEFAULT '100' COMMENT 'Updated by user' AFTER `updated`;

ALTER TABLE `ad_window` ADD INDEX `idx_ad_window_ad_user_createdby` (`created_by` ASC);
ALTER TABLE `ad_window` ADD INDEX `idx_ad_window_ad_user_updatedby` (`updated_by` ASC);

ALTER TABLE `ad_window` ADD CONSTRAINT `fk_ad_window_ad_user_createdby` FOREIGN KEY (`created_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `ad_window` ADD CONSTRAINT `fk_ad_window_ad_user_updatedby` FOREIGN KEY (`updated_by`) REFERENCES `ad_user` (`id_user`) ON DELETE RESTRICT ON UPDATE CASCADE;
