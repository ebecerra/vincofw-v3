-- Colors
SELECT PC.value, RL.name
FROM cat_product_color PC JOIN ad_ref_list RL ON PC.value = RL.value AND RL.id_reference = '4F4C373EBD0911E4A564BC5FF4671D01';

SELECT RL.value, RL.name
FROM ad_ref_list RL LEFT JOIN cat_product_material PM ON PM.value = RL.value
WHERE PM.value is null AND RL.id_reference = '4F4C373EBD0911E4A564BC5FF4671D01';

-- Materials
SELECT PM.value, RL.name
FROM cat_product_material PM JOIN ad_ref_list RL ON PM.value = RL.value AND RL.id_reference = '4F4C373EBD0911E4A564BC5FF4671D02';

SELECT RL.value, RL.name
FROM ad_ref_list RL LEFT JOIN cat_product_material PM ON PM.value = RL.value
WHERE PM.value is null AND RL.id_reference = '4F4C373EBD0911E4A564BC5FF4671D02';