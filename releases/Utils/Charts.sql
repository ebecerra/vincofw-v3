-- Linked charts
SELECT CW.name as Window, CT.name as Tab, C.name as Chart, L.name AS LinkedTo
FROM ad_chart_linked CL JOIN ad_chart C ON C.id_chart = CL.id_chart
JOIN ad_chart L ON L.id_chart = CL.id_linked
JOIN ad_tab CT ON CT.id_tab = C.id_tab
JOIN ad_window CW ON CT.id_window = CW.id_window
ORDER BY 1, 2, 3;