SELECT CU.name AS createdBy, UU.name AS updatedBy, T.*
FROM <tableName> T JOIN ad_user CU ON CU.id_user = T.created_by
  JOIN ad_user UU ON UU.id_user = T.created_by;