-- ad_characteristic
SELECT ch.id_client, ch.id_module, ch.name, m.name as module, c.name as client
FROM ad_characteristic ch join ad_module m on ch.id_module = m.id_module
  join ad_client c on ch.id_client = c.id_client;

-- ad_characteristic_def
SELECT cd.id_client, cd.id_module, ch.name, m.name as module, c.name as client, if(cd.id_module <> ch.id_module, 'Error', 'Ok') as valid
FROM ad_characteristic_def cd join ad_characteristic ch on ch.id_characteristic = cd.id_characteristic
  join ad_module m on cd.id_module = m.id_module
  join ad_client c on cd.id_client = c.id_client;

-- ad_client_module
SELECT cm.id_client, cm.id_module, m.name as module, c.name as client
FROM ad_client_module cm join ad_module m on cm.id_module = m.id_module
join ad_client c on cm.id_client = c.id_client;

-- ad_chart
SELECT ch.id_chart, ch.id_client, ch.id_module, ch.name, w.name as window, t.name as tab,
  m.name as module, c.name as client, if(ch.id_module <> t.id_module, 'Error', 'Ok') as valid
FROM ad_chart ch join ad_tab t on t.id_tab = ch.id_tab
  join ad_window w on t.id_window = w.id_window
  join ad_module m on ch.id_module = m.id_module
  join ad_client c on ch.id_client = c.id_client
order by w.name, t.name, ch.name;

-- ad_chart_filter
SELECT cf.id_chart_filter, cf.id_client, cf.id_module, cf.name, w.name as window, t.name as tab,
  m.name as module, c.name as client, if(cf.id_module <> t.id_module, 'Error', 'Ok') as valid
FROM ad_chart_filter cf join ad_tab t on t.id_tab = cf.id_tab
  join ad_window w on t.id_window = w.id_window
  join ad_module m on cf.id_module = m.id_module
  join ad_client c on cf.id_client = c.id_client
order by w.name, t.name, cf.name;

-- ad_chart_linked
SELECT cl.id_chart_linked, cl.id_client, cl.id_module, w.name as window, t.name as tab, ch.name,
  m.name as module, c.name as client, if(cl.id_module <> ch.id_module, 'Error', 'Ok') as valid
FROM ad_chart_linked cl join ad_chart ch on ch.id_chart = cl.id_chart
  join ad_tab t on t.id_tab = ch.id_tab
  join ad_window w on t.id_window = w.id_window
  join ad_module m on cl.id_module = m.id_module
  join ad_client c on cl.id_client = c.id_client
order by w.name, t.name, ch.name;

-- ad_chart_column
SELECT cc.id_chart_column, cc.id_client, cc.id_module, cc.name, w.name as window, t.name as tab, ch.name as chart,
  m.name as module, c.name as client, if(cc.id_module <> ch.id_module, 'Error', 'Ok') as valid
FROM ad_chart_column cc join ad_chart ch on cc.id_chart = ch.id_chart
  join ad_tab t on t.id_tab = ch.id_tab
  join ad_window w on t.id_window = w.id_window
  join ad_module m on ch.id_module = m.id_module
  join ad_client c on ch.id_client = c.id_client
order by w.name, t.name, ch.name, cc.name;

-- ad_column
SELECT col.id_client, col.id_module, col.name, t.name as tableName,
  m.name as module, c.name as client, if(col.id_module <> t.id_module, 'Error', 'Ok') as valid
FROM ad_column col join ad_table t on col.id_table = t.id_table
  join ad_module m on col.id_module = m.id_module
  join ad_client c on col.id_client = c.id_client
order by t.name limit 10000;

-- ad_column_sync
SELECT col.id_client, col.id_module, tb.name as tableName, col.name,
  m.name as module, c.name as client, if(col.id_module <> t.id_module, 'Error', 'Ok') as valid
FROM ad_column_sync col join ad_table_sync t on col.id_table_sync = t.id_table_sync
  join ad_module m on col.id_module = m.id_module
  join ad_client c on col.id_client = c.id_client
  join ad_table tb on t.id_table = tb.id_table
order by tb.name limit 10000;

-- ad_field
SELECT f.id_client, f.id_module, w.name as winName, t.name as tabName, f.name,
  m.name as module, c.name as client, if(f.id_module <> t.id_module, 'Error', 'Ok') as valid
FROM ad_field f join ad_tab t on f.id_tab = t.id_tab
  join ad_window w on t.id_window = w.id_window
  join ad_module m on f.id_module = m.id_module
  join ad_client c on f.id_client = c.id_client
order by w.name, t.name, f.name limit 10000;

-- ad_menu
SELECT mnu.id_client, mnu.id_module, mnu.value, mnu.name, mnu.parent_menu, m.name as module, c.name as client
FROM ad_menu mnu
  join ad_module m on mnu.id_module = m.id_module
  join ad_client c on mnu.id_client = c.id_client
order by mnu.parent_menu;

-- ad_menu_roles
SELECT mr.id_menu_roles, mr.id_client, mr.id_module, r.name as role, mnu.value, mnu.name, mnu.parent_menu,
       m.name as module, c.name as client, if(r.id_module <> mr.id_module, 'Error', 'Ok') as valid
FROM ad_menu_roles mr
  join ad_menu mnu on mnu.id_menu = mr.id_menu
  join ad_role r on mr.id_role = r.id_role
  join ad_module m on mr.id_module = m.id_module
  join ad_client c on mr.id_client = c.id_client
order by m.name;

-- ad_preference
SELECT p.id_client, p.id_module, p.property, p.value, m.name as module, c.name as client
FROM ad_preference p join ad_module m on p.id_module = m.id_module
  join ad_client c on p.id_client = c.id_client
order by p.property;

-- ad_ref_table
SELECT rf.id_client, rf.id_module, r.name as reference, t.name, m.name as module, c.name as client,
  if(rf.id_module <> t.id_module, 'Error', 'Ok') as valid
FROM ad_ref_table rf join ad_table t on rf.id_table = t.id_table
  join ad_reference r on rf.id_reference = r.id_reference
  join ad_module m on rf.id_module = m.id_module
  join ad_client c on rf.id_client = c.id_client
order by t.name;

-- ad_table_sync
SELECT ts.id_table_sync, ts.id_client, ts.id_module, t.name, m.name as module, c.name as client,
  if(ts.id_module <> t.id_module, 'Error', 'Ok') as valid
FROM ad_table_sync ts join ad_table t on ts.id_table = t.id_table
  join ad_module m on ts.id_module = m.id_module
  join ad_client c on ts.id_client = c.id_client
order by t.name;

-- ad_table
SELECT t.id_client, t.id_module, t.name, m.name as module, c.name as client
FROM ad_table t join ad_module m on t.id_module = m.id_module
  join ad_client c on t.id_client = c.id_client
order by t.name;

-- ad_user
SELECT u.id_user, u.id_client, u.id_module, u.name, m.name as module, c.name as client
FROM ad_user u join ad_module m on u.id_module = m.id_module
  join ad_client c on u.id_client = c.id_client
order by u.name;

-- ad_translation - ad_message
SELECT t.id_client, t.id_module, l.name as lang, msg.value, t.translation, m.name as module, c.name as client
FROM ad_translation t JOIN ad_module m ON t.id_module = m.id_module
  JOIN ad_client c on t.id_client = c.id_client
  JOIN ad_message msg ON id_message = rowkey
  JOIN ad_language l ON t.id_language = l.id_language
WHERE  id_table = 'ff80808151382f2a0151385e92b60037'
ORDER BY t.id_module, msg.value;

SELECT t.*
FROM ad_translation t
WHERE id_table = 'ff80808151382f2a0151385e92b60037' AND NOT EXISTS (SELECT m.id_message FROM ad_message m WHERE m.id_message = t.rowkey);

-- ad_wiki_topic
SELECT wt.id_wiki_topic, wt.id_client, wt.id_module, wt.name, wt.title, m.name as module, c.name as client,
  if(wt.id_module <> wg.id_module, 'Error', 'Ok') as valid
FROM ad_wiki_topic wt join ad_wiki_guide wg on wg.id_wiki_guide = wt.id_wiki_guide
  join ad_module m on wt.id_module = m.id_module
  join ad_client c on wt.id_client = c.id_client
order by wt.name;

-- ad_wiki_entry
SELECT we.id_wiki_entry, we.id_client, we.id_module, we.name, we.title, m.name as module, c.name as client,
  if(we.id_module <> wt.id_module, 'Error', 'Ok') as valid
FROM ad_wiki_entry we join ad_wiki_topic wt on we.id_wiki_topic = wt.id_wiki_topic
  join ad_module m on we.id_module = m.id_module
  join ad_client c on we.id_client = c.id_client
order by wt.name;