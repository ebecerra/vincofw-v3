-- Used references (Columns)
SELECT R.id_reference, R.name AS refName, R.rtype, C.id_column, C.name AS columnName, T.id_table, T.name AS tableName
FROM ad_reference R JOIN ad_column C ON R.id_reference = C.id_reference_value
  JOIN ad_table T ON C.id_table = T.id_table
ORDER BY refName LIMIT 10000;

SELECT T.id_table, T.name AS tableName, C.id_column, C.name AS columnName, R.rtype, C.id_reference, C.id_reference_value,
if((LOCATE(R.rtype, 'TABLEDIR;TABLE;SEARCH;BUTTON;LIST') > 0 AND C.id_reference_value IS NOT NULL) OR (LOCATE(R.rtype, 'TABLEDIR;TABLE;SEARCH;BUTTON;LIST') = 0 AND C.id_reference_value IS NULL) , 'OK', 'ERROR') as valid
FROM ad_column C JOIN ad_reference R ON R.id_reference = C.id_reference
JOIN ad_table T ON C.id_table = T.id_table
ORDER BY tableName, columnName LIMIT 10000;

-- Used references (Process Params)
SELECT R.id_reference, R.name AS refName, R.rtype, PP.id_process_param, PP.name AS paramName, P.id_process, P.name AS processName
FROM ad_reference R JOIN ad_process_param PP ON R.id_reference = PP.id_reference_value
  JOIN ad_process P ON P.id_process = PP.id_process
ORDER BY refName LIMIT 10000;

SELECT P.id_process, P.name AS processName, PP.id_process_param, PP.name AS paramName, R.rtype, R.id_reference, PP.id_reference_value, R.name AS refName,
if((LOCATE(R.rtype, 'TABLEDIR;TABLE;SEARCH;BUTTON;LIST') > 0 AND PP.id_reference_value IS NOT NULL) OR (LOCATE(R.rtype, 'TABLEDIR;TABLE;SEARCH;BUTTON;LIST') = 0 AND PP.id_reference_value IS NULL) , 'OK', 'ERROR') as valid
FROM ad_process_param PP JOIN ad_reference R ON R.id_reference = PP.id_reference
  JOIN ad_process P ON P.id_process = PP.id_process
ORDER BY processName, paramName LIMIT 10000;

-- Unused references (On Columns)
SELECT R.id_reference, R.name, R.rtype, M.name as module
FROM ad_reference R LEFT JOIN ad_column C ON R.id_reference = C.id_reference_value
JOIN ad_module M ON M.id_module = R.id_module
WHERE C.id_column is null AND R.base = 0
ORDER BY R.name LIMIT 10000;