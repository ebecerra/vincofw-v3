CREATE TABLE `ad_audit` (
  `id_audit` varchar(32) NOT NULL COMMENT 'Audit identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_table` varchar(32) NOT NULL COMMENT 'Table identifier',
  `id_record` varchar(32) NOT NULL COMMENT 'Record identifier',
  `id_user` varchar(32) NOT NULL COMMENT 'User identifier',
  `operation` varchar(1) NOT NULL COMMENT 'Operation (I - Insert, U - Update, D - Delete)',
  `old_value` text COMMENT 'Old value (JSON)',
  `new_value` text COMMENT 'New value (JSON)',
  PRIMARY KEY (`id_audit`),
  KEY `idx_ad_audit_ad_client` (`id_client`),
  KEY `idx_ad_audit_ad_table` (`id_table`),
  KEY `idx_ad_audit_ad_user` (`id_user`),
  CONSTRAINT `fk_ad_audit_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_audit_ad_table` FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`),
  CONSTRAINT `fk_ad_audit_ad_user` FOREIGN KEY (`id_user`) REFERENCES `ad_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ad_table`
  ADD COLUMN `audit` BIT(1) NULL COMMENT 'Audit table changes' AFTER `id_sort_column`;

UPDATE `ad_table` SET audit = 0;

ALTER TABLE `ad_table`
  CHANGE COLUMN `audit` `audit` BIT(1) NOT NULL COMMENT 'Audit table changes' ;

