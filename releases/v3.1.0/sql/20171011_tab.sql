ALTER TABLE `ad_tab`
  ADD COLUMN `group_mode` VARCHAR(50) NULL COMMENT 'Group visualization mode (List)' AFTER `view_default`;

UPDATE ad_tab SET group_mode = 'COLLAPSIBLE';

ALTER TABLE `ad_tab`
  CHANGE COLUMN `group_mode` `group_mode` VARCHAR(50) NOT NULL COMMENT 'Group visualization mode (List)' ;

