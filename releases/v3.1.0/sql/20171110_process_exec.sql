ALTER TABLE `ad_process_exec` DROP FOREIGN KEY `fk_ad_process_exec_ad_user`;
ALTER TABLE `ad_process_exec` ADD CONSTRAINT `fk_ad_process_exec_ad_user`
FOREIGN KEY (`id_user`) REFERENCES `ad_user` (`id_user`) ON DELETE CASCADE;
