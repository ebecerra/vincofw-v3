CREATE TABLE `ad_dbscript` (
  `id_dbscript` varchar(32) NOT NULL COMMENT 'Database script identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `description` varchar(1000) DEFAULT NULL COMMENT 'Script description',
  `seqno` int(11) NOT NULL COMMENT 'Sequence number',
  PRIMARY KEY (`id_dbscript`),
  KEY `idx_ad_dbscript_ad_client` (`id_client`),
  KEY `idx_ad_dbscript_ad_module` (`id_module`),
  CONSTRAINT `fk_ad_dbscript_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_dbscript_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=287;

CREATE TABLE `ad_dbscript_sql` (
  `id_dbscript_sql` varchar(32) NOT NULL COMMENT 'Database script SQL identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_dbscript` varchar(32) NOT NULL COMMENT 'Database script identifier',
  `sql_update` varchar(2000) NOT NULL COMMENT 'SQL sentence',
  `seqno` int(11) NOT NULL COMMENT 'Sequence number',
  PRIMARY KEY (`id_dbscript_sql`),
  KEY `idx_ad_dbscript_sql_ad_client` (`id_client`),
  KEY `idx_ad_dbscript_sql_ad_module` (`id_module`),
  KEY `idx_ad_dbscript_sql_ad_dbscript` (`id_dbscript`),
  CONSTRAINT `fk_ad_dbscript_sql_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_dbscript_sql_ad_dbscript` FOREIGN KEY (`id_dbscript`) REFERENCES `ad_dbscript` (`id_dbscript`),
  CONSTRAINT `fk_ad_dbscript_sql_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=287;
