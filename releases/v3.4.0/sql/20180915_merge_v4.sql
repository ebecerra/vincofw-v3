ALTER TABLE `ad_field_group`
ADD UNIQUE INDEX `idx_ad_field_group_name` (`id_client` ASC, `id_module` ASC, `name` ASC);

ALTER TABLE `ad_field_group` DROP INDEX `idx_ad_field_group_1`;

ALTER TABLE `ad_file`
  ADD COLUMN `seqno` INT NULL COMMENT 'Sequence number' AFTER `mimetype`;
