ALTER TABLE `ad_ref_button` DROP FOREIGN KEY `fk_ad_ref_button_process`;

ALTER TABLE `ad_ref_button` CHANGE COLUMN `id_process` `id_process` VARCHAR(32) NULL COMMENT 'Process identifier' ;
ALTER TABLE `ad_ref_button` ADD CONSTRAINT `fk_ad_ref_button_process`
  FOREIGN KEY (`id_process`) REFERENCES `ad_process` (`id_process`) ON UPDATE CASCADE;
