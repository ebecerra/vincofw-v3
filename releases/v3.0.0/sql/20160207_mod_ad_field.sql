ALTER TABLE `ad_field`
ADD COLUMN `used_in_child` BIT(1) NULL COMMENT 'Used in child tabs' AFTER `showinstatusbar`;

UPDATE ad_field SET used_in_child = 0;

ALTER TABLE `ad_field`
CHANGE COLUMN `used_in_child` `used_in_child` BIT(1) NOT NULL COMMENT 'Used in child tabs' ;
