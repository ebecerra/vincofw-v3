ALTER TABLE `ad_field`
  ADD COLUMN `filter_type` VARCHAR(150) NULL COMMENT 'Filter modifier (List)' AFTER `filter_range`;

UPDATE ad_field SET filter_type = 'STANDARD';

ALTER TABLE `ad_field`
  CHANGE COLUMN `filter_type` `filter_type` VARCHAR(50) NOT NULL COMMENT 'Filter modifier (List)' ;

