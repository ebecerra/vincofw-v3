ALTER TABLE `ad_process_exec` DROP FOREIGN KEY `FK_ad_process_exec_ad_process_id_process`;
ALTER TABLE `ad_process_exec` ADD CONSTRAINT `FK_ad_process_exec_ad_process_id_process`
FOREIGN KEY (`id_process`) REFERENCES `ad_process` (`id_process`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_process_log` DROP FOREIGN KEY `FK_ad_process_log_ad_process_exec_id_process_exec`;
ALTER TABLE `ad_process_log` ADD CONSTRAINT `FK_ad_process_log_ad_process_exec_id_process_exec`
FOREIGN KEY (`id_process_exec`) REFERENCES `ad_process_exec` (`id_process_exec`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_process_output` DROP FOREIGN KEY `FK_ad_process_output_ad_process_id_process`;
ALTER TABLE `ad_process_output` ADD CONSTRAINT `FK_ad_process_output_ad_process_id_process`
FOREIGN KEY (`id_process`) REFERENCES `ad_process` (`id_process`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_process_param` DROP FOREIGN KEY `FK_ad_process_param_ad_process_id_process`;
ALTER TABLE `ad_process_param` ADD CONSTRAINT `FK_ad_process_param_ad_process_id_process`
FOREIGN KEY (`id_process`) REFERENCES `ad_process` (`id_process`) ON DELETE CASCADE ON UPDATE CASCADE;
