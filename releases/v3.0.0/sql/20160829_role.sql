ALTER TABLE `ad_role`
  ADD COLUMN `rtype` VARCHAR(50) NULL COMMENT 'Role type' AFTER `description`;

UPDATE ad_role set rtype = 'STANDARD';

ALTER TABLE `ad_role`
  CHANGE COLUMN `rtype` `rtype` VARCHAR(50) NOT NULL COMMENT 'Role type' ;
