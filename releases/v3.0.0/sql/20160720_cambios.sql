ALTER TABLE `ad_field_group`
ADD COLUMN `columns` INT NULL COMMENT 'Form columns' AFTER `collapsed`;

ALTER TABLE `ad_field_group`
ADD COLUMN `displaylogic` VARCHAR(2000) NULL COMMENT 'Display logic' AFTER `columns`;

update ad_field_group set columns = 3;

ALTER TABLE `ad_field_group`
CHANGE COLUMN `columns` `columns` INT(11) NOT NULL COMMENT 'Form columns' ;

ALTER TABLE `ad_ref_button`
ADD COLUMN `show_mode` VARCHAR(50) NULL COMMENT 'Show mode (List)' AFTER `icon`;

UPDATE ad_ref_button SET show_mode = 'IN_FORM';

ALTER TABLE `ad_ref_button`
CHANGE COLUMN `show_mode` `show_mode` VARCHAR(50) NOT NULL COMMENT 'Show mode (List)' ;

ALTER TABLE `ad_ref_search_column`
ADD COLUMN `seqno` INT NULL COMMENT 'Sequence' AFTER `active`;

ALTER TABLE `ad_process_output`
CHANGE COLUMN `email_to` `email_to` VARCHAR(2500) NULL DEFAULT NULL COMMENT 'Email to send' ;
