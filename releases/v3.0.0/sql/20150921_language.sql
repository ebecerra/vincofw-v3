ALTER TABLE `ad_language`
  ADD COLUMN `iso_3` VARCHAR(3) NULL COMMENT 'ISO code (3 character)' AFTER `iso_2`;
