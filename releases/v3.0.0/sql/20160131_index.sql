ALTER TABLE `ad_menu` ADD UNIQUE INDEX `IDX_ad_menu_value` (`value` ASC);

ALTER TABLE `ad_user_roles` ADD UNIQUE INDEX `IDX_ad_user_roles_1` (`id_role` ASC, `id_user` ASC);

ALTER TABLE `ad_translation` ADD UNIQUE INDEX `IDX_ad_translation_1` (`id_client` ASC, `id_language` ASC, `id_table` ASC, `id_column` ASC);

ALTER TABLE `ad_user` ADD UNIQUE INDEX `IDX_ad_user_username` (`username` ASC);

ALTER TABLE `ad_user` ADD UNIQUE INDEX `IDX_ad_user_email` (`email` ASC);

ALTER TABLE `ad_table` ADD UNIQUE INDEX `IDX_ad_table_name` (`name` ASC);

ALTER TABLE `ad_sequence` ADD UNIQUE INDEX `IDX_ad_sequence_name` (`name` ASC);

ALTER TABLE `ad_role_priv` ADD UNIQUE INDEX `IDX_ad_role_priv_1` (`id_role` ASC, `id_privilege` ASC, `id_client` ASC);

ALTER TABLE `ad_role` ADD UNIQUE INDEX `IDX_ad_role_name` (`name` ASC);

ALTER TABLE `ad_characteristic` ADD UNIQUE INDEX `IDX_ad_characteristic_1` (`id_client` ASC, `id_table` ASC, `name` ASC);

ALTER TABLE `ad_characteristic_def` ADD UNIQUE INDEX `IDX_ad_characteristic_def_1` (`id_client` ASC, `id_characteristic` ASC, `dtype` ASC);

ALTER TABLE `ad_characteristic_value` ADD UNIQUE INDEX `IDX_ad_characteristic_value_1` (`id_client` ASC, `id_characteristic` ASC, `key_value` ASC);

ALTER TABLE `ad_client` ADD UNIQUE INDEX `IDX_ad_client_name` (`name` ASC);

ALTER TABLE `ad_client` ADD UNIQUE INDEX `IDX_ad_client_email` (`email` ASC);

ALTER TABLE `ad_client_module` ADD UNIQUE INDEX `IDX_ad_client_module_1` (`id_client` ASC, `id_module` ASC);

ALTER TABLE `ad_column` ADD UNIQUE INDEX `IDX_ad_column_1` (`id_table` ASC, `name` ASC);

ALTER TABLE `ad_field` ADD UNIQUE INDEX `IDX_ad_field_1` (`id_tab` ASC, `id_column` ASC);

ALTER TABLE `ad_field_group` ADD UNIQUE INDEX `IDX_ad_field_group_1` (`id_client` ASC, `name` ASC);

ALTER TABLE `ad_language` ADD UNIQUE INDEX `IDX_ad_language_name` (`name` ASC);

ALTER TABLE `ad_menu_roles` ADD UNIQUE INDEX `IDX_ad_menu_roles_1` (`id_menu` ASC, `id_role` ASC, `id_client` ASC);

ALTER TABLE `ad_message` ADD UNIQUE INDEX `IDX_ad_message_value` (`id_client` ASC, `value` ASC);

ALTER TABLE `ad_module` ADD UNIQUE INDEX `IDX_ad_module_name` (`name` ASC);

ALTER TABLE `ad_module` ADD UNIQUE INDEX `IDX_ad_module_java_package` (`java_package` ASC);

ALTER TABLE `ad_module` ADD UNIQUE INDEX `IDX_ad_module_dbprefix` (`dbprefix` ASC);

ALTER TABLE `ad_module` ADD UNIQUE INDEX `IDX_ad_module_rest_path` (`rest_path` ASC);

ALTER TABLE `ad_privilege` ADD UNIQUE INDEX `IDX_ad_privilege_name` (`name` ASC);

ALTER TABLE `ad_process` ADD UNIQUE INDEX `IDX_ad_process_1` (`classname` ASC, `classmethod` ASC);

ALTER TABLE `ad_process_output` ADD UNIQUE INDEX `IDX_ad_process_output_1` (`id_process` ASC, `otype` ASC);

ALTER TABLE `ad_process_param` ADD UNIQUE INDEX `IDX_ad_process_param_1` (`id_process` ASC, `name` ASC);

ALTER TABLE `ad_ref_list` DROP FOREIGN KEY `FK_ad_ref_list_ad_reference_id_reference`;
ALTER TABLE `ad_ref_list` ADD CONSTRAINT `FK_ad_ref_list_ad_reference_id_reference`
FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`)  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_ref_search_column` DROP FOREIGN KEY `FK_ad_ref_search_column_ad_ref_table_id_ref_table`;
ALTER TABLE `ad_ref_search_column` ADD CONSTRAINT `FK_ad_ref_search_column_ad_ref_table_id_ref_table`
FOREIGN KEY (`id_ref_table`) REFERENCES `ad_ref_table` (`id_ref_table`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_ref_button` DROP FOREIGN KEY `FK_ad_ref_button_ad_reference_id_reference`;
ALTER TABLE `ad_ref_button` ADD CONSTRAINT `FK_ad_ref_button_ad_reference_id_reference`
FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_ref_table` DROP FOREIGN KEY `FK_ad_ref_table_ad_reference_id_reference`;
ALTER TABLE `ad_ref_table` ADD CONSTRAINT `FK_ad_ref_table_ad_reference_id_reference`
FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_ref_list` ADD UNIQUE INDEX `IDX_ad_ref_list_1` (`id_reference` ASC, `value` ASC);

ALTER TABLE `ad_ref_search_column` ADD UNIQUE INDEX `IDX_ad_ref_search_column_1` (`id_ref_table` ASC, `id_column` ASC);




