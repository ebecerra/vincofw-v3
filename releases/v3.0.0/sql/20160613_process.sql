ALTER TABLE `ad_process`
ADD COLUMN `multiple` BIT(1) NULL COMMENT 'Multiple process/report' AFTER `showinparams`,
ADD COLUMN `eval_sql` VARCHAR(2000) NULL COMMENT 'Evaluation SQL for multiple process/report' AFTER `multiple`;

