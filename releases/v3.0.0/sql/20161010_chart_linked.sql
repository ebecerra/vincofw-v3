ALTER TABLE `ad_chart_linked`
  ADD COLUMN `id_linked` VARCHAR(32) NOT NULL COMMENT 'Linked chart identifier' AFTER `id_chart`;

ALTER TABLE `ad_chart_linked` DROP FOREIGN KEY `fk_ad_chart_linked_ad_chart`;
ALTER TABLE `ad_chart_linked` ADD CONSTRAINT `fk_ad_chart_linked_ad_chart`
FOREIGN KEY (`id_chart`) REFERENCES `ad_chart` (`id_chart`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_chart_linked`  ADD INDEX `idx_ad_chart_linked_ad_chart_linked` (`id_linked` ASC);
ALTER TABLE `ad_chart_linked`  ADD CONSTRAINT `fk_ad_chart_linked_ad_chart_linked`
FOREIGN KEY (`id_linked`) REFERENCES `ad_chart` (`id_chart`)  ON DELETE CASCADE  ON UPDATE CASCADE;

ALTER TABLE `ad_chart_linked`
  ADD UNIQUE INDEX `idx_ad_chart_id_chart_id_linked` (`id_chart` ASC, `id_linked` ASC, `id_client` ASC);

ALTER TABLE `ad_chart`
  CHANGE COLUMN `key_field` `key_field` VARCHAR(50) NOT NULL COMMENT 'SQL key field' ,
  CHANGE COLUMN `name_field` `name_field` VARCHAR(250) NOT NULL COMMENT 'SQL name field' ,
  ADD COLUMN `key_reference` VARCHAR(32) NULL AFTER `key_field`;

UPDATE ad_chart SET key_reference = '56F03512BD0811E4A564BC5FF4671DB4';

ALTER TABLE `ad_chart`
  CHANGE COLUMN `key_reference` `key_reference` VARCHAR(32) NOT NULL ;

