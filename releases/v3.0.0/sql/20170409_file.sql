ALTER TABLE `ad_file`
  ADD COLUMN `caption` VARCHAR(150) NULL COMMENT 'Caption for file link' AFTER `name`;
