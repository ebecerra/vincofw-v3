ALTER TABLE `ad_process_output`
ADD COLUMN `mail_to` VARCHAR(150) NULL COMMENT 'Email to send' AFTER `port`,
ADD COLUMN `mail_name` VARCHAR(100) NULL COMMENT 'Name of destination' AFTER `mail_to`;

ALTER TABLE `ad_process_output`
CHANGE COLUMN `mail_to` `email_to` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Email to send' ,
CHANGE COLUMN `mail_name` `email_name` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Name of destination' ;
