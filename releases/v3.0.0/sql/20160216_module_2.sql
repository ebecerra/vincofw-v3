ALTER TABLE `ad_ref_search_column` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_ref_search_column_ad_module_idx` (`id_module` ASC);

UPDATE `ad_ref_search_column` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_ref_search_column` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_ref_search_column` ADD CONSTRAINT `fk_ad_ref_search_column_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

