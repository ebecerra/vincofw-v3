CREATE TABLE `ad_table_sync` (
  `id_table_sync` varchar(32) NOT NULL COMMENT 'Table synchonization identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `id_table` varchar(32) NOT NULL COMMENT 'Table identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  `version` int(10) NOT NULL COMMENT 'Table version',
  `sincro` bit(1) NOT NULL COMMENT 'It indicates whether synchronizes with offline devices',
  `exportable` bit(1) NOT NULL COMMENT 'It indicates whether the table is exportable',
  `importable` bit(1) NOT NULL COMMENT 'It indicates whether the table is importable',
  `xselect` varchar(1000) DEFAULT NULL COMMENT 'SELECT modifiers',
  `xfrom` varchar(1000) DEFAULT NULL COMMENT 'FROM part',
  `xwhere` varchar(1000) DEFAULT NULL COMMENT 'WHERE part',
  `xgroup` varchar(1000) DEFAULT NULL COMMENT 'GRUOP BY part',
  `xprefix` varchar(45) DEFAULT NULL COMMENT 'Table prefix',
  `delete_remote` varchar(1000) DEFAULT NULL COMMENT 'Where conditions for remote DELETE',
  `triggers` varchar(150) DEFAULT NULL COMMENT 'Class name for triggers',
  PRIMARY KEY (`id_table_sync`),
  KEY `FK_ad_table_syncad_client` (`id_client`),
  KEY `FK_ad_table_sync_ad_module` (`id_module`),
  KEY `FK_ad_table_sync_ad_table` (`id_table`),
  CONSTRAINT `FK_ad_table_sync_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ad_table_sync_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `FK_ad_table_sync_ad_table` FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_column_sync` (
  `id_column_sync` varchar(32) NOT NULL COMMENT 'Column synchonization identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_table_sync` varchar(32) NOT NULL COMMENT 'Table synchonization identifier',
  `name` varchar(100) NOT NULL COMMENT 'Column name',
  `ctype` varchar(50) NOT NULL COMMENT 'Column type',
  `primary_key` bit(1) NOT NULL COMMENT 'Is primary key',
  `seqno` int(10) NOT NULL COMMENT 'Sequence number (Order)',
  `cformat` varchar(50) DEFAULT NULL COMMENT 'Column format',
  `csource` varchar(10) NOT NULL COMMENT 'Data source',
  `csource_value` text COMMENT 'Data source value',
  `exportable` bit(1) NOT NULL COMMENT 'It indicates whether the table is exportable',
  `importable` bit(1) NOT NULL COMMENT 'It indicates whether the table is importable',
  PRIMARY KEY (`id_column_sync`),
  UNIQUE KEY `IDX_ad_column_sync_name` (`id_column_sync`,`name`),
  KEY `FK_ad_column_sync_ad_client` (`id_client`),
  KEY `FK_ad_column_sync_ad_module` (`id_module`),
  KEY `FK_ad_column_sync_ad_table_sync` (`id_table_sync`),
  CONSTRAINT `FK_ad_column_sync_ad_table_sync` FOREIGN KEY (`id_table_sync`) REFERENCES `ad_table_sync` (`id_table_sync`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ad_column_sync_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `FK_ad_column_sync_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

