ALTER TABLE `ad_offline_device` DROP FOREIGN KEY `FK_ad_offline_device_ad_user`;

ALTER TABLE `ad_offline_device`
ADD COLUMN `user_name` VARCHAR(100) NULL COMMENT 'User name' AFTER `id_user`;

UPDATE `ad_offline_device` SET `user_name`='Eduardo' WHERE `id_offline_device`='100';

ALTER TABLE `ad_offline_device`
CHANGE COLUMN `user_name` `user_name` VARCHAR(100) NOT NULL COMMENT 'User name' ;
