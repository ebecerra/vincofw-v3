CREATE TABLE `ad_aux_input` (
  `id_aux_input` varchar(32) NOT NULL COMMENT 'Auxiliary input identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `value` varchar(50) NOT NULL COMMENT 'Value',
  `name` varchar(200) NOT NULL COMMENT 'Description',
  `query` varchar(1000) NOT NULL COMMENT 'SQL Query',
  PRIMARY KEY (`id_aux_input`),
  UNIQUE KEY `idx_ad_aux_input_value` (`id_client`,`value`),
  KEY `idx_ad_aux_input_ad_client` (`id_client`),
  KEY `idx_ad_aux_input_ad_module` (`id_module`),
  CONSTRAINT `fk_ad_aux_input_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_aux_input_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=496;
