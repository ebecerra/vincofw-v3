ALTER TABLE `ad_tab`
  ADD COLUMN `view_mode` VARCHAR(50) NULL COMMENT 'View mode (List)' AFTER `sort_columns`,
  ADD COLUMN `view_default` BIT(1) NULL COMMENT 'Is default view' AFTER `view_mode`;

UPDATE ad_tab SET view_mode = 'LIST', view_default = 1;

ALTER TABLE `ad_tab`
  CHANGE COLUMN `view_mode` `view_mode` VARCHAR(50) NOT NULL COMMENT 'View mode (List)' ,
  CHANGE COLUMN `view_default` `view_default` BIT(1) NOT NULL COMMENT 'Is default view' ;
