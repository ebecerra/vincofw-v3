ALTER TABLE `ad_process_param` DROP FOREIGN KEY `FK_ad_process_param_ad_reference_id_reference`;
ALTER TABLE `ad_process_param` ADD COLUMN `id_reference_value` VARCHAR(32) NULL COMMENT 'Reference value identifier' AFTER `id_reference`,
ADD INDEX `fk_ad_process_param_1_idx` (`id_reference_value` ASC);

ALTER TABLE `ad_process_param` ADD CONSTRAINT `FK_ad_process_param_ad_reference_id_reference`
FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ad_process_param_ad_reference_value` FOREIGN KEY (`id_reference_value`)
REFERENCES `ad_preference` (`id_preference`) ON DELETE RESTRICT ON UPDATE CASCADE;

