CREATE TABLE `ad_chart_column` (
  `id_chart_column` varchar(32) NOT NULL COMMENT 'Chart column identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_chart` varchar(32) NOT NULL COMMENT 'Chart identifier',
  `id_reference` varchar(32) NOT NULL COMMENT 'Reference identifier',
  `id_reference_value` varchar(32) DEFAULT NULL COMMENT 'Reference value identifier',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `sql_expression` varchar(250) NOT NULL COMMENT 'SQL sentence',
  `seqno` int(10) DEFAULT NULL COMMENT 'Sequence number (Order)',
  `displayed` bit(1) NOT NULL,
  `caption` varchar(100) NOT NULL,
  PRIMARY KEY (`id_chart_column`),
  KEY `idx_ad_chart_column_ad_client` (`id_client`),
  KEY `idx_ad_chart_column_ad_module` (`id_module`),
  KEY `idx_ad_chart_column_ad_chart` (`id_chart`),
  KEY `idx_ad_chart_column_ad_reference` (`id_reference`),
  KEY `idx_ad_chart_column_ad_reference_value` (`id_reference_value`),
  CONSTRAINT `fk_ad_chart_column_ad_chart` FOREIGN KEY (`id_chart`) REFERENCES `ad_chart` (`id_chart`),
  CONSTRAINT `fk_ad_chart_column_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_chart_column_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `fk_ad_chart_column_ad_reference` FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`) ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_chart_column_ad_reference_value` FOREIGN KEY (`id_reference_value`) REFERENCES `ad_reference` (`id_reference`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ad_chart`
  CHANGE COLUMN `value_1` `value_1` VARCHAR(250) NULL COMMENT 'Value 1' ,
  CHANGE COLUMN `title_x` `title_x` VARCHAR(150) NULL COMMENT 'X axis title' ,
  CHANGE COLUMN `title_y` `title_y` VARCHAR(150) NULL COMMENT 'Y axis title' ;
