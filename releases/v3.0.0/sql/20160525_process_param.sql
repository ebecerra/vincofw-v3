ALTER TABLE `ad_process_param`
ADD COLUMN `displayed` BIT(1) NOT NULL COMMENT 'Indicate if param is visible or not' AFTER `ranged`;

update ad_process_param set displayed = 1;
