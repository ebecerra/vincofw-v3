ALTER TABLE `ad_table_sync`
ADD COLUMN `translatable` BIT(1) NOT NULL DEFAULT 0 COMMENT 'It indicates whether the table have translation' AFTER `importable`;
