ALTER TABLE `ad_chart_filter`
  ADD COLUMN `displaylogic` VARCHAR(250) NULL COMMENT 'Display logic' AFTER `caption`;

ALTER TABLE `ad_process_param`
  ADD COLUMN `displaylogic` VARCHAR(250) NULL COMMENT 'Display logic' AFTER `caption`;
