ALTER TABLE `ad_preference` CHANGE COLUMN `property` `property` VARCHAR(32) NOT NULL COMMENT 'Preference property' ;

ALTER TABLE `ad_preference` ADD INDEX `FK_ad_preference_ad_ref_list_idx` (`property` ASC);
ALTER TABLE `ad_preference` ADD CONSTRAINT `FK_ad_preference_ad_ref_list`
FOREIGN KEY (`property`) REFERENCES `ad_ref_list` (`id_ref_list`) ON DELETE RESTRICT ON UPDATE CASCADE;

