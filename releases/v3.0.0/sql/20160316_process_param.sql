ALTER TABLE `ad_process_param` DROP FOREIGN KEY `fk_ad_process_param_ad_reference_value`;
ALTER TABLE `ad_process_param` ADD CONSTRAINT `FK_ad_process_param_ad_reference_value`
FOREIGN KEY (`id_reference_value`) REFERENCES `ad_reference` (`id_reference`) ON UPDATE CASCADE;
