ALTER TABLE `ad_table_action`
ADD COLUMN `uipattern` VARCHAR(50) NULL COMMENT 'Action user interface pattern (List)' AFTER `icon`;

UPDATE `ad_table_action` set uipattern = 'DIALOG';

ALTER TABLE `ad_table_action`
  CHANGE COLUMN `uipattern` `uipattern` VARCHAR(50) NOT NULL COMMENT 'Action user interface pattern (List)' ;

