ALTER TABLE `ad_table_sync`
ADD COLUMN `id_table_real` VARCHAR(32) NULL COMMENT 'Real table identifier' AFTER `id_table`;

UPDATE ad_table_sync SET id_table_real = id_table;

ALTER TABLE `ad_table_sync`
CHANGE COLUMN `id_table_real` `id_table_real` VARCHAR(32) NOT NULL COMMENT 'Real table identifier' ;

ALTER TABLE `ad_table_sync` ADD INDEX `fk_ad_table_sync_ad_table_real_idx` (`id_table_real` ASC);
ALTER TABLE `ad_table_sync` ADD CONSTRAINT `fk_ad_table_sync_ad_table_real`
FOREIGN KEY (`id_table_real`) REFERENCES `ad_table` (`id_table`) ON DELETE CASCADE ON UPDATE CASCADE;

