ALTER TABLE `ad_chart` ADD COLUMN `color_1` VARCHAR(20) NULL COMMENT 'Color for value 5' AFTER `legend_5`;
ALTER TABLE `ad_chart` ADD COLUMN `color_2` VARCHAR(20) NULL COMMENT 'Color for value 5' AFTER `color_1`;
ALTER TABLE `ad_chart` ADD COLUMN `color_3` VARCHAR(20) NULL COMMENT 'Color for value 5' AFTER `color_2`;
ALTER TABLE `ad_chart` ADD COLUMN `color_4` VARCHAR(20) NULL COMMENT 'Color for value 5' AFTER `color_3`;
ALTER TABLE `ad_chart` ADD COLUMN `color_5` VARCHAR(20) NULL COMMENT 'Color for value 5' AFTER `color_4`;
