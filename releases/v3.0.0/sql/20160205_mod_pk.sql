ALTER TABLE `ad_menu_roles` DROP FOREIGN KEY `FK_ad_menu_roles_ad_menu_id_menu`;
ALTER TABLE `ad_menu_roles` ADD CONSTRAINT `FK_ad_menu_roles_ad_menu_id_menu`
  FOREIGN KEY (`id_menu`) REFERENCES `ad_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_role_priv` DROP FOREIGN KEY `FK_ad_role_priv_ad_privilege_id_privilege`,
DROP FOREIGN KEY `FK_ad_role_priv_ad_role_id_role`;

ALTER TABLE `ad_role_priv` ADD CONSTRAINT `FK_ad_role_priv_ad_privilege_id_privilege`
FOREIGN KEY (`id_privilege`) REFERENCES `ad_privilege` (`id_privilege`) ON DELETE CASCADE  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_ad_role_priv_ad_role_id_role` FOREIGN KEY (`id_role`)
REFERENCES `ad_role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_user_roles` DROP FOREIGN KEY `FK_ad_user_roles_ad_role_id_role`,
DROP FOREIGN KEY `FK_ad_user_roles_ad_user_id_user`;
ALTER TABLE `ad_user_roles` ADD CONSTRAINT `FK_ad_user_roles_ad_role_id_role`
FOREIGN KEY (`id_role`) REFERENCES `ad_role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_ad_user_roles_ad_user_id_user` FOREIGN KEY (`id_user`) REFERENCES `ad_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_menu_roles` DROP FOREIGN KEY `FK_ad_menu_roles_ad_role_id_role`;
ALTER TABLE `ad_menu_roles` ADD CONSTRAINT `FK_ad_menu_roles_ad_role_id_role`
FOREIGN KEY (`id_role`) REFERENCES `ad_role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_column` DROP FOREIGN KEY `FK_ad_column_ad_table_id_table`;
ALTER TABLE `ad_column` ADD CONSTRAINT `FK_ad_column_ad_table_id_table`
FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_tab` DROP FOREIGN KEY `FK_ad_tab_ad_window_id_window`;
ALTER TABLE `ad_tab` ADD CONSTRAINT `FK_ad_tab_ad_window_id_window`
FOREIGN KEY (`id_window`) REFERENCES `ad_window` (`id_window`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_field` DROP FOREIGN KEY `FK_ad_field_ad_tab_id_tab`;
ALTER TABLE `ad_field` ADD CONSTRAINT `FK_ad_field_ad_tab_id_tab`
FOREIGN KEY (`id_tab`) REFERENCES `ad_tab` (`id_tab`) ON DELETE CASCADE ON UPDATE CASCADE;
