ALTER TABLE `ad_translation`
  DROP FOREIGN KEY `fk_ad_translation_ad_column`,
  DROP FOREIGN KEY `fk_ad_translation_ad_module`,
  DROP FOREIGN KEY `fk_ad_translation_ad_table`;

ALTER TABLE `ad_translation` ADD CONSTRAINT `fk_ad_translation_ad_column`
FOREIGN KEY (`id_column`) REFERENCES `ad_column` (`id_column`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_ad_translation_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_ad_translation_ad_table`
FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`) ON DELETE CASCADE;
