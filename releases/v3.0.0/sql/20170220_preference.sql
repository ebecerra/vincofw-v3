ALTER TABLE `ad_preference`
  ADD COLUMN `ispublic` BIT(1) NULL COMMENT 'Is public preference' AFTER `visibleat_id_user`;

UPDATE `ad_preference` SET `ispublic` = 0;

ALTER TABLE `ad_preference`
  CHANGE COLUMN `ispublic` `ispublic` BIT(1) NOT NULL COMMENT 'Is public preference' ;
