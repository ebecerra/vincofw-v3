ALTER TABLE `ad_chart`
  ADD COLUMN `span` INT(10) NULL COMMENT 'Render a chart in \"span\" columns' AFTER `row_limit`,
  ADD COLUMN `startnewrow` BIT(1) NULL COMMENT 'Render chart in new row' AFTER `span`;

UPDATE ad_chart SET span = 1, startnewrow = 0;

ALTER TABLE `ad_chart`
  CHANGE COLUMN `span` `span` INT(10) NOT NULL COMMENT 'Render a chart in \"span\" columns' ,
  CHANGE COLUMN `startnewrow` `startnewrow` BIT(1) NOT NULL COMMENT 'Render chart in new row' ;
