ALTER TABLE `ad_table`
  ADD COLUMN `sortable` BIT(1) NOT NULL DEFAULT 0 COMMENT 'Table is sortable' AFTER `export_data`,
  ADD COLUMN `id_sort_column` VARCHAR(32) NULL COMMENT 'Column identifier for sort' AFTER `sortable`;

ALTER TABLE `ad_table` ADD INDEX `fk_ad_table_ad_column_idx` (`id_sort_column` ASC);
ALTER TABLE `ad_table` ADD CONSTRAINT `fk_ad_table_ad_column`
FOREIGN KEY (`id_sort_column`) REFERENCES `ad_column` (`id_column`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_tab`
  ADD COLUMN `sort_columns` INT NULL COMMENT 'Columns on sort panel' AFTER `id_parent_table`;
