ALTER TABLE `ad_sequence` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_sequence_ad_module_idx` (`id_module` ASC);

UPDATE `ad_sequence` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_sequence` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_sequence` ADD CONSTRAINT `fk_ad_sequence_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_role` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_role_ad_module_idx` (`id_module` ASC);

UPDATE `ad_role` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_role` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_role` ADD CONSTRAINT `fk_ad_role_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_language` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_language_ad_module_idx` (`id_module` ASC);

UPDATE `ad_language` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_language` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_language` ADD CONSTRAINT `fk_ad_language_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_image` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_image_ad_module_idx` (`id_module` ASC);

UPDATE `ad_image` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_image` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_image` ADD CONSTRAINT `fk_ad_image_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_file` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_file_ad_module_idx` (`id_module` ASC);

UPDATE `ad_file` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_file` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_file` ADD CONSTRAINT `fk_ad_file_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_characteristic_value` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_characteristic_value_ad_module_idx` (`id_module` ASC);

UPDATE `ad_characteristic_value` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_characteristic_value` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_characteristic_value` ADD CONSTRAINT `fk_ad_characteristic_value_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_characteristic_def` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_characteristic_def_ad_module_idx` (`id_module` ASC);

UPDATE `ad_characteristic_def` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_characteristic_def` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_characteristic_def` ADD CONSTRAINT `fk_ad_characteristic_def_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `ad_characteristic` ADD COLUMN `id_module` VARCHAR(32) NULL AFTER `id_client`,
ADD INDEX `fk_ad_characteristic_ad_module_idx` (`id_module` ASC);

UPDATE `ad_characteristic` SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_characteristic` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL ;
ALTER TABLE `ad_characteristic` ADD CONSTRAINT `fk_ad_characteristic_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;
