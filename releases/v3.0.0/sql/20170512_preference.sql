ALTER TABLE `ad_preference`
  ADD COLUMN `visibleat_environment` VARCHAR(50) NULL COMMENT 'Visible for environment' AFTER `visibleat_id_user`;
