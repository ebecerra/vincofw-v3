DELETE FROM ad_translation;

ALTER TABLE `ad_translation` DROP INDEX `IDX_ad_translation_1` ;

ALTER TABLE `ad_translation`
ADD UNIQUE INDEX `IDX_ad_translation_unique_1` (`id_client` ASC, `id_language` ASC, `id_table` ASC, `id_column` ASC, `rowkey` ASC);

