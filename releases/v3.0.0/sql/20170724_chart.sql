ALTER TABLE `ad_chart` ADD COLUMN `value_6` VARCHAR(250) NULL COMMENT 'Value 6' AFTER `value_5`;
ALTER TABLE `ad_chart` ADD COLUMN `value_7` VARCHAR(250) NULL COMMENT 'Value 7' AFTER `value_6`;
ALTER TABLE `ad_chart` ADD COLUMN `value_8` VARCHAR(250) NULL COMMENT 'Value 8' AFTER `value_7`;
ALTER TABLE `ad_chart` ADD COLUMN `value_9` VARCHAR(250) NULL COMMENT 'Value 9' AFTER `value_8`;
ALTER TABLE `ad_chart` ADD COLUMN `value_10` VARCHAR(250) NULL COMMENT 'Value 10' AFTER `value_9`;

ALTER TABLE `ad_chart` ADD COLUMN `legend_6` VARCHAR(100) NULL COMMENT 'Legend for value 6' AFTER `legend_5`;
ALTER TABLE `ad_chart` ADD COLUMN `legend_7` VARCHAR(100) NULL COMMENT 'Legend for value 7' AFTER `legend_6`;
ALTER TABLE `ad_chart` ADD COLUMN `legend_8` VARCHAR(100) NULL COMMENT 'Legend for value 8' AFTER `legend_7`;
ALTER TABLE `ad_chart` ADD COLUMN `legend_9` VARCHAR(100) NULL COMMENT 'Legend for value 9' AFTER `legend_8`;
ALTER TABLE `ad_chart` ADD COLUMN `legend_10` VARCHAR(100) NULL COMMENT 'Legend for value 10' AFTER `legend_9`;

ALTER TABLE `ad_chart` ADD COLUMN `color_6` VARCHAR(20) NULL COMMENT 'Color for value 6' AFTER `color_5`;
ALTER TABLE `ad_chart` ADD COLUMN `color_7` VARCHAR(20) NULL COMMENT 'Color for value 7' AFTER `color_6`;
ALTER TABLE `ad_chart` ADD COLUMN `color_8` VARCHAR(20) NULL COMMENT 'Color for value 8' AFTER `color_7`;
ALTER TABLE `ad_chart` ADD COLUMN `color_9` VARCHAR(20) NULL COMMENT 'Color for value 9' AFTER `color_8`;
ALTER TABLE `ad_chart` ADD COLUMN `color_10` VARCHAR(20) NULL COMMENT 'Color for value 10' AFTER `color_9`;
