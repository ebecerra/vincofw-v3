CREATE TABLE `ad_client_language` (
  `id_client_language` varchar(32) NOT NULL COMMENT 'Client - Language identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_language` varchar(32) NOT NULL COMMENT 'Language identifier',
  `seqno` int(11) NOT NULL COMMENT 'Sequence number',
  PRIMARY KEY (`id_client_language`),
  UNIQUE KEY `idx_ad_client_language_id_language` (`id_client`,`id_language`),
  KEY `idx_ad_client_language_ad_client` (`id_client`),
  KEY `idx_ad_client_language_ad_module` (`id_module`),
  KEY `idx_ad_client_language_ad_language` (`id_language`),
  CONSTRAINT `fk_ad_client_language_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_client_language_ad_language` FOREIGN KEY (`id_language`) REFERENCES `ad_language` (`id_language`),
  CONSTRAINT `fk_ad_client_language_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
