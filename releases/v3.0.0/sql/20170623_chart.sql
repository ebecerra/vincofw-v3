ALTER TABLE `ad_chart`
  ADD COLUMN `classname` VARCHAR(150) NULL COMMENT 'Full qualify name of chart class' AFTER `startnewrow`,
  ADD COLUMN `classmethod` VARCHAR(60) NULL COMMENT 'Chart implementation method ' AFTER `classname`;

ALTER TABLE `ad_chart`
  CHANGE COLUMN `xfrom` `xfrom` TEXT NULL COMMENT 'SQL for get data' ,
  CHANGE COLUMN `xwhere` `xwhere` TEXT NULL COMMENT 'SQL for get data' ;
