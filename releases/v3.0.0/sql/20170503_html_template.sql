CREATE TABLE `ad_html_template` (
  `id_html_template` varchar(32) NOT NULL COMMENT 'HTML template identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `title` varchar(50) NOT NULL COMMENT 'Template title',
  `description` varchar(500) DEFAULT NULL COMMENT 'Description',
  `image` varchar(150) NOT NULL COMMENT 'Image for template',
  `html` text NOT NULL COMMENT 'HTML template',
  PRIMARY KEY (`id_html_template`),
  UNIQUE KEY `idx_ad_html_template_title` (`id_client`,`title`),
  KEY `idx_ad_html_template_ad_client` (`id_client`),
  KEY `idx_ad_html_template_ad_module` (`id_module`),
  CONSTRAINT `fk_ad_html_template_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_html_template_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
