ALTER TABLE `ad_chart`
  ADD COLUMN `fullname_field` VARCHAR(250) NULL COMMENT 'SQL full name field (Optional)' AFTER `name_field`;
