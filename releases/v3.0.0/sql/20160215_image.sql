ALTER TABLE `ad_image` DROP FOREIGN KEY `FK_ad_image_ad_table_id_table`;
ALTER TABLE `ad_image` CHANGE COLUMN `id_table` `id_table` VARCHAR(32) NOT NULL COMMENT 'Table identifier' ,
ADD COLUMN `id_row` VARCHAR(32) NOT NULL COMMENT 'Row identifier' AFTER `id_table`;
ALTER TABLE `ad_image` ADD CONSTRAINT `FK_ad_image_ad_table_id_table` FOREIGN KEY (`id_table`)
REFERENCES `ad_table` (`id_table`);

ALTER TABLE `ad_image` DROP FOREIGN KEY `FK_ad_image_ad_table_id_table`;
ALTER TABLE `ad_image`ADD CONSTRAINT `FK_ad_image_ad_table_id_table`
FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ad_file` DROP FOREIGN KEY `FK_ad_file_ad_table_id_table`;
ALTER TABLE `ad_file` CHANGE COLUMN `id_table` `id_table` VARCHAR(32) NOT NULL COMMENT 'Table identifier',
ADD COLUMN `id_row` VARCHAR(32) NOT NULL COMMENT 'Row identifier' AFTER `id_table`;
ALTER TABLE `ad_file` ADD CONSTRAINT `FK_ad_file_ad_table_id_table` FOREIGN KEY (`id_table`)
REFERENCES `ad_table` (`id_table`) ON DELETE CASCADE ON UPDATE CASCADE;

