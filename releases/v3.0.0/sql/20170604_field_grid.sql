DROP TABLE `ad_field_grid`;

CREATE TABLE `ad_field_grid` (
  `id_field_grid` varchar(32) NOT NULL COMMENT 'Field grid identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_user` varchar(32) NOT NULL COMMENT 'User identifier',
  `id_tab` varchar(32) NOT NULL COMMENT 'Tab identifier',
  `id_field` varchar(32) NOT NULL COMMENT 'Field identifier',
  `seqno` int(11) NOT NULL COMMENT 'Sequence',
  PRIMARY KEY (`id_field_grid`),
  UNIQUE KEY `idx_ad_field_grid` (`id_client`,`id_user`,`id_tab`,`id_field`),
  KEY `idx_ad_field_grid_ad_client` (`id_client`),
  KEY `idx_ad_field_grid_ad_module` (`id_module`),
  KEY `idx_ad_field_grid_ad_user` (`id_user`),
  KEY `idx_ad_field_grid_ad_tab` (`id_tab`),
  KEY `idx_ad_field_grid_ad_field` (`id_field`),
  CONSTRAINT `fk_ad_field_grid_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `fk_ad_field_grid_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_field_grid_ad_field` FOREIGN KEY (`id_field`) REFERENCES `ad_field` (`id_field`),
  CONSTRAINT `fk_ad_field_grid_ad_tab` FOREIGN KEY (`id_tab`) REFERENCES `ad_tab` (`id_tab`),
  CONSTRAINT `fk_ad_field_grid_ad_user` FOREIGN KEY (`id_user`) REFERENCES `ad_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

