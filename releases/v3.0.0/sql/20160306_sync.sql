ALTER TABLE `ad_column_sync` DROP INDEX `IDX_ad_column_sync_name` ,
ADD UNIQUE INDEX `IDX_ad_column_sync_name` (`id_table_sync` ASC, `name` ASC);

ALTER TABLE `ad_table_sync`
ADD UNIQUE INDEX `IDX_adt_table_sync_name` (`id_client` ASC, `id_table` ASC);

ALTER TABLE `ad_table_sync`
CHANGE COLUMN `sincro` `synchronize` BIT(1) NOT NULL COMMENT 'It indicates whether synchronizes with offline devices' ;

CREATE TABLE `ad_offline_device` (
  `id_offline_device` varchar(100) NOT NULL COMMENT 'Offline device identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `id_user` varchar(32) NOT NULL COMMENT 'User identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `dstatus` varchar(50) NOT NULL COMMENT 'Device status (List)',
  `version` varchar(10) NOT NULL COMMENT 'Software version',
  `device` varchar(100) NOT NULL COMMENT 'Device name, model, ...',
  `device_os` varchar(50) NOT NULL COMMENT 'Device operation system',
  `device_version` varchar(20) NOT NULL COMMENT 'Device OS version',
  PRIMARY KEY (`id_offline_device`),
  KEY `FK_ad_offline_device_ad_client` (`id_client`),
  KEY `FK_ad_offline_device_ad_module` (`id_module`),
  KEY `FK_ad_offline_device_ad_user` (`id_user`),
  CONSTRAINT `FK_ad_offline_device_ad_user` FOREIGN KEY (`id_user`) REFERENCES `ad_user` (`id_user`),
  CONSTRAINT `FK_ad_offline_device_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `Fk_ad_offline_device_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_offline_device_log` (
  `id_offline_device_log` varchar(100) NOT NULL COMMENT 'Offline device log identifier',
  `id_offline_device` varchar(100) NOT NULL COMMENT 'Offline device identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `ltype` varchar(50) NOT NULL COMMENT 'Log type (List)',
  `log` varchar(2000) NOT NULL COMMENT 'Log information',
  PRIMARY KEY (`id_offline_device_log`),
  KEY `FK_ad_offline_device_log_ad_client` (`id_client`),
  KEY `FK_ad_offline_device_log_ad_module` (`id_module`),
  KEY `FK_ad_offline_device_log_ad_offline_device` (`id_offline_device`),
  CONSTRAINT `FK_ad_offline_device_log_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `Fk_ad_offline_device_log_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ad_offline_device_log_ad_offline_device` FOREIGN KEY (`id_offline_device`) REFERENCES `ad_offline_device` (`id_offline_device`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
