ALTER TABLE `ad_client_module`
  ADD COLUMN `module_id` VARCHAR(32) NULL COMMENT 'Assigned Module identifier' AFTER `active`;

UPDATE `ad_client_module` SET module_id = id_module;

ALTER TABLE `ad_client_module`
  CHANGE COLUMN `module_id` `module_id` VARCHAR(32) NOT NULL COMMENT 'Assigned Module identifier' ;

ALTER TABLE `ad_client_module` DROP INDEX `idx_ad_client_module_1` ;

ALTER TABLE `ad_client_module`
  ADD UNIQUE INDEX `idx_ad_client_module_1` (`id_client` ASC, `module_id` ASC);

ALTER TABLE `ad_client_module`
  ADD INDEX `idx_ad_client_module_ad_module_id` (`module_id` ASC);

ALTER TABLE `ad_client_module` ADD CONSTRAINT `fk_ad_client_module_ad_module_id`
FOREIGN KEY (`module_id`) REFERENCES `ad_module` (`id_module`) ON DELETE NO ACTION ON UPDATE NO ACTION;

