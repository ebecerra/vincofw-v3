ALTER TABLE `ad_column`
ADD COLUMN `id_ref_sequence` VARCHAR(32) NULL COMMENT 'Sequence identifier' AFTER `id_val_rule`,
ADD INDEX `idx_ad_column_ad_ref_sequence` (`id_ref_sequence` ASC);

ALTER TABLE `ad_column` ADD INDEX `fk_ad_column_ad_ref_sequence_idx` (`id_ref_sequence` ASC);
ALTER TABLE `ad_column` ADD CONSTRAINT `fk_ad_column_ad_ref_sequence`
FOREIGN KEY (`id_ref_sequence`) REFERENCES `ad_ref_sequence` (`id_ref_secuence`) ON DELETE RESTRICT ON UPDATE CASCADE;

