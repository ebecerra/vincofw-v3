ALTER TABLE `ad_column`
CHANGE COLUMN `csource` `csource` VARCHAR(50) NOT NULL COMMENT 'Data source' ,
CHANGE COLUMN `csource_value` `csource_value` VARCHAR(250) NULL DEFAULT NULL COMMENT 'Data source value' ,
CHANGE COLUMN `readonlylogic` `readonlylogic` VARCHAR(2000) NULL DEFAULT NULL COMMENT 'Read only logic' ;

ALTER TABLE `ad_column_sync`
CHANGE COLUMN `csource` `csource` VARCHAR(50) NOT NULL COMMENT 'Data source',
CHANGE COLUMN `csource_value` `csource_value` VARCHAR(250) NULL DEFAULT NULL COMMENT 'Data source value' ;

ALTER TABLE `ad_field`
CHANGE COLUMN `sortable` `sortable` BIT(1) NOT NULL COMMENT 'Sortable field' ;

ALTER TABLE `ad_image`
CHANGE COLUMN `itype` `itype` VARCHAR(50) NOT NULL COMMENT 'Image type (List)' ;

ALTER TABLE `ad_language`
CHANGE COLUMN `iso_2` `iso_2` VARCHAR(2) NOT NULL COMMENT 'ISO code (2 character)' ;

ALTER TABLE `ad_menu`
CHANGE COLUMN `parent_menu` `parent_menu` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Parent menu search key' ;

ALTER TABLE `ad_module`
CHANGE COLUMN `rest_path` `rest_path` VARCHAR(30) NOT NULL COMMENT 'Module Restful prefix' ,
CHANGE COLUMN `dbprefix` `dbprefix` VARCHAR(20) NOT NULL COMMENT 'Module database prefix' ;

ALTER TABLE `ad_preference`
CHANGE COLUMN `property` `property` VARCHAR(50) NOT NULL COMMENT 'Preference property' ;

ALTER TABLE `ad_process_param`
CHANGE COLUMN `valuedefault` `valuedefault` VARCHAR(60) NULL DEFAULT NULL COMMENT 'Default value' ;

ALTER TABLE `ad_tab`
CHANGE COLUMN `uipattern` `uipattern` VARCHAR(50) NOT NULL COMMENT 'Tab user interface pattern' ;

ALTER TABLE `ad_table`
CHANGE COLUMN `sql_query` `sql_query` VARCHAR(2000) NULL DEFAULT NULL COMMENT 'SQL definition' ;

ALTER TABLE `ad_window`
CHANGE COLUMN `wtype` `wtype` VARCHAR(50) NOT NULL COMMENT 'window type' ;

ALTER TABLE `ad_field`
CHANGE COLUMN `displaylogic` `displaylogic` VARCHAR(2000) NULL DEFAULT NULL COMMENT 'Display logic' ;

ALTER TABLE `ad_column`
ADD COLUMN `length_min` INT NULL COMMENT 'Column minimum length' AFTER `translatable`,
ADD COLUMN `length_max` INT NULL COMMENT 'Column maximum length' AFTER `length_min`;
