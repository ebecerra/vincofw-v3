ALTER TABLE `ad_translation` DROP FOREIGN KEY `fk_ad_translation_ad_language`;
ALTER TABLE `ad_translation` ADD CONSTRAINT `fk_ad_translation_ad_language`
FOREIGN KEY (`id_language`) REFERENCES `ad_language` (`id_language`) ON UPDATE CASCADE;

ALTER TABLE `ad_user` DROP FOREIGN KEY `fk_ad_user_ad_language`;
ALTER TABLE `ad_user` ADD CONSTRAINT `fk_ad_user_ad_language`
FOREIGN KEY (`default_id_language`) REFERENCES `ad_language` (`id_language`) ON UPDATE CASCADE;

