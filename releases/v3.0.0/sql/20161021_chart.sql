ALTER TABLE `ad_chart` ADD INDEX `fk_ad_chart_ad_reference_idx` (`key_reference` ASC);
ALTER TABLE `ad_chart` ADD CONSTRAINT `fk_ad_chart_ad_reference`
FOREIGN KEY (`key_reference`) REFERENCES `ad_reference` (`id_reference`) ON DELETE RESTRICT ON UPDATE CASCADE;


