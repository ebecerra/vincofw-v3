CREATE TABLE `ad_import_delete` (
  `process_id` int(11) NOT NULL COMMENT 'Process identifier',
  `table_name` varchar(100) NOT NULL,
  `table_key` varchar(32) NOT NULL COMMENT 'Imported row identifier',
  PRIMARY KEY (`process_id`,`table_name`,`table_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
