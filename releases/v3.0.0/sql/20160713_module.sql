ALTER TABLE `ad_characteristic` DROP FOREIGN KEY `fk_ad_characteristic_ad_module`;
ALTER TABLE `ad_characteristic` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL COMMENT 'Module identifier' ;
ALTER TABLE `ad_characteristic` ADD CONSTRAINT `fk_ad_characteristic_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE;

ALTER TABLE `ad_client`
ADD COLUMN `id_module` VARCHAR(32) NULL COMMENT 'Module identifier' AFTER `id_client`;

ALTER TABLE `ad_client` ADD INDEX `fk_ad_client_ad_module_idx` (`id_module` ASC);
ALTER TABLE `ad_client` ADD CONSTRAINT `fk_ad_client_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE ad_client SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_client` DROP FOREIGN KEY `fk_ad_client_ad_module`;
ALTER TABLE `ad_client` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL COMMENT 'Module identifier' ;
ALTER TABLE `ad_client` ADD CONSTRAINT `fk_ad_client_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE;

ALTER TABLE `ad_user`
ADD COLUMN `id_module` VARCHAR(32) NULL COMMENT 'Module identifier' AFTER `id_client`;

ALTER TABLE `ad_user` ADD INDEX `fk_ad_user_ad_module_idx` (`id_module` ASC);
ALTER TABLE `ad_user` ADD CONSTRAINT `fk_ad_user_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE ad_user SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_user` DROP FOREIGN KEY `fk_ad_user_ad_module`;
ALTER TABLE `ad_user` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL COMMENT 'Module identifier' ;
ALTER TABLE `ad_user` ADD CONSTRAINT `fk_ad_user_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE;

ALTER TABLE `ad_user_roles`
ADD COLUMN `id_module` VARCHAR(32) NULL COMMENT 'Module identifier' AFTER `id_client`;

ALTER TABLE `ad_user_roles` ADD INDEX `fk_ad_user_roles_ad_module_idx` (`id_module` ASC);
ALTER TABLE `ad_user_roles` ADD CONSTRAINT `fk_ad_user_roles_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE ad_user_roles SET id_module = 'D351EB083EC7E8C4891CD7B25B1E274E';

ALTER TABLE `ad_user_roles` DROP FOREIGN KEY `fk_ad_user_roles_ad_module`;
ALTER TABLE `ad_user_roles` CHANGE COLUMN `id_module` `id_module` VARCHAR(32) NOT NULL COMMENT 'Module identifier' ;
ALTER TABLE `ad_user_roles` ADD CONSTRAINT `fk_ad_user_roles_ad_module`
FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE;