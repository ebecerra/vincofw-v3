CREATE TABLE `ad_ref_sequence` (
  `id_ref_secuence` varchar(32) NOT NULL COMMENT 'Sequence identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `id_reference` varchar(32) NOT NULL COMMENT 'Reference identifier',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `description` varchar(250) DEFAULT NULL COMMENT 'Description',
  `incrementno` int(10) NOT NULL COMMENT 'Increment by',
  `startno` int(10) NOT NULL COMMENT 'Start sequence number',
  `currentnext` int(10) NOT NULL COMMENT 'Current sequence value',
  `prefix` varchar(10) DEFAULT NULL COMMENT 'Sequence prefix',
  `suffix` varchar(10) DEFAULT NULL COMMENT 'Sequence suffix',
  PRIMARY KEY (`id_ref_secuence`),
  KEY `FK_ad_ref_sequencead_client` (`id_client`),
  KEY `FK_ad_ref_sequence_ad_module` (`id_module`),
  KEY `FK_ad_ref_sequence_ad_reference` (`id_reference`),
  CONSTRAINT `FK_ad_ref_sequence_ad_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `FK_ad_ref_sequence_ad_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ad_ref_sequence_ad_reference` FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `ad_sequence`;
