ALTER TABLE `ad_characteristic` DROP FOREIGN KEY `FK_ad_characteristic_ad_table_id_table`;
ALTER TABLE `ad_characteristic` CHANGE COLUMN `id_table` `id_table` VARCHAR(32) NOT NULL COMMENT 'Table identifier' ;
ALTER TABLE `ad_characteristic` ADD CONSTRAINT `FK_ad_characteristic_ad_table_id_table`
FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`);

ALTER TABLE `ad_characteristic` DROP FOREIGN KEY `FK_ad_characteristic_ad_reference_id_reference`;
ALTER TABLE `ad_characteristic` CHANGE COLUMN `id_reference` `id_reference` VARCHAR(32) NOT NULL COMMENT 'Reference identifier' ;
ALTER TABLE `ad_characteristic` ADD CONSTRAINT `FK_ad_characteristic_ad_reference_id_reference`
FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`);
