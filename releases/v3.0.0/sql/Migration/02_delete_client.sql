SET @id_client = 'C00F0280DEE20D3D6B81F347F4EDBFFA';

DELETE FROM ad_characteristic_value WHERE id_client =  @id_client;
DELETE FROM ad_characteristic_def WHERE id_client =  @id_client;
DELETE FROM ad_characteristic WHERE id_client =  @id_client;
DELETE FROM ad_file WHERE id_client =  @id_client;
DELETE FROM ad_image WHERE id_client =  @id_client;
DELETE FROM ad_preference WHERE id_client =  @id_client;

DELETE FROM ad_process_log WHERE id_client =  @id_client;
DELETE FROM ad_process_exec WHERE id_client =  @id_client;
DELETE FROM ad_process_output WHERE id_client =  @id_client;
DELETE FROM ad_process_param WHERE id_client =  @id_client;
DELETE FROM ad_process WHERE id_client =  @id_client;

DELETE FROM ad_user_roles WHERE id_client =  @id_client;
DELETE FROM ad_user WHERE id_client =  @id_client;
DELETE FROM ad_role_priv WHERE id_client =  @id_client;
DELETE FROM ad_privilege WHERE id_client =  @id_client;
DELETE FROM ad_menu_roles WHERE id_client =  @id_client;
DELETE FROM ad_menu WHERE id_client =  @id_client;
DELETE FROM ad_role WHERE id_client =  @id_client;

DELETE FROM ad_translation WHERE id_client =  @id_client;
DELETE FROM ad_message WHERE id_client =  @id_client;
DELETE FROM ad_language WHERE id_client =  @id_client;

DELETE FROM ad_val_rule WHERE id_client =  @id_client;
DELETE FROM ad_field WHERE id_client =  @id_client;
DELETE FROM ad_field_group WHERE id_client =  @id_client;
DELETE FROM ad_tab WHERE id_client =  @id_client;
DELETE FROM ad_window WHERE id_client =  @id_client;
DELETE FROM ad_column WHERE id_client =  @id_client;
DELETE FROM ad_table WHERE id_client =  @id_client;

DELETE FROM ad_ref_button WHERE id_client =  @id_client;
DELETE FROM ad_ref_list WHERE id_client =  @id_client;
DELETE FROM ad_ref_search_column WHERE id_client =  @id_client;
DELETE FROM ad_ref_table WHERE id_client =  @id_client;
DELETE FROM ad_reference WHERE id_client =  @id_client;
DELETE FROM ad_reference WHERE exists (SELECT 1 FROM ad_module WHERE ad_reference.id_module = ad_module.id_module AND ad_module.id_client =  @id_client);
DELETE FROM ad_ref_sequence WHERE id_client =  @id_client;

DELETE FROM ad_client_module WHERE id_client =  @id_client;
DELETE FROM ad_module WHERE id_client =  @id_client;
DELETE FROM ad_client WHERE id_client =  @id_client;
