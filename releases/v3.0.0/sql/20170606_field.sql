ALTER TABLE `ad_field`
  ADD COLUMN `multi_select` BIT(1) NULL DEFAULT NULL  COMMENT 'Field is used for multiselection' AFTER `css_class`;
