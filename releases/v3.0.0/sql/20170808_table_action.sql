ALTER TABLE `ad_table_action`
  ADD COLUMN `url` VARCHAR(500) NULL COMMENT 'Custom URL' AFTER `uipattern`,
  ADD COLUMN `target` VARCHAR(50) NULL COMMENT 'Target window to open URL (List)' AFTER `url`;
