ALTER TABLE `ad_tab`
ADD COLUMN `id_parent_table` VARCHAR(45) NULL COMMENT 'Parent table for core tables (AdImage, AdFile, ...)' AFTER `command`;

ALTER TABLE `ad_tab` ADD INDEX `fk_ad_tab_ad_table_parent_idx` (`id_parent_table` ASC);
ALTER TABLE `ad_tab` ADD CONSTRAINT `fk_ad_tab_ad_table_parent`
FOREIGN KEY (`id_parent_table`) REFERENCES `ad_table` (`id_table`) ON DELETE SET NULL ON UPDATE CASCADE;

