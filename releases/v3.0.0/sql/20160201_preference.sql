DROP TABLE `ad_params`;
DROP TABLE `ad_preference`;
CREATE TABLE `ad_preference` (
  `id_preference` varchar(32) NOT NULL COMMENT 'Reference identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_module` varchar(32) NOT NULL COMMENT 'Module identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `value` text NOT NULL COMMENT 'Preference value',
  `property` varchar(60) NOT NULL COMMENT 'Preference property',
  `privilege` bit(1) NOT NULL COMMENT 'Indicate if is privilege',
  `visibleat_id_client` varchar(32) DEFAULT NULL COMMENT 'Visible for client',
  `visibleat_id_role` varchar(32) DEFAULT NULL COMMENT 'Visible for role',
  `visibleat_id_user` varchar(32) DEFAULT NULL COMMENT 'Visible for user',
  PRIMARY KEY (`id_preference`),
  KEY `FK_ad_preference_ad_client_at_id_client` (`visibleat_id_client`),
  KEY `FK_ad_preference_ad_client_id_client` (`id_client`),
  KEY `FK_ad_preference_ad_module_id_module` (`id_module`),
  KEY `FK_ad_preference_ad_role_id_role` (`visibleat_id_role`),
  KEY `FK_ad_preference_ad_user_id_user` (`visibleat_id_user`),
  CONSTRAINT `FK_ad_preference_ad_client_at_id_client` FOREIGN KEY (`visibleat_id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `FK_ad_preference_ad_client_id_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `FK_ad_preference_ad_module_id_module` FOREIGN KEY (`id_module`) REFERENCES `ad_module` (`id_module`),
  CONSTRAINT `FK_ad_preference_ad_role_id_role` FOREIGN KEY (`visibleat_id_role`) REFERENCES `ad_role` (`id_role`),
  CONSTRAINT `FK_ad_preference_ad_user_id_user` FOREIGN KEY (`visibleat_id_user`) REFERENCES `ad_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

