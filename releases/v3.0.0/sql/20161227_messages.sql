ALTER TABLE `ad_message`
ADD COLUMN `web` BIT(1) NOT NULL DEFAULT 0 COMMENT 'Used in Web Applications' AFTER `msgtext`,
ADD COLUMN `admin` BIT(1) NOT NULL DEFAULT 1 COMMENT 'Used in Control Panel' AFTER `web`;
