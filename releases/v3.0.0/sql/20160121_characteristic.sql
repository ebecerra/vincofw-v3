CREATE TABLE `ad_characteristic` (
  `id_characteristic` varchar(32) NOT NULL COMMENT 'Characteristic identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_table` varchar(32) DEFAULT NULL COMMENT 'Table identifier',
  `id_reference` varchar(32) DEFAULT NULL COMMENT 'Reference identifier',
  `id_reference_value` varchar(32) DEFAULT NULL COMMENT 'Reference value identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `description` varchar(250) DEFAULT NULL COMMENT 'Description',
  `ctype` varchar(50) NOT NULL COMMENT 'Characteristic type (List)',
  `mode` varchar(50) NOT NULL COMMENT 'Mode (List)',
  `translatable` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Translatable characteristic',
  `file_type` varchar(500) DEFAULT NULL COMMENT 'File MIME type',
  `file_maxsize` int(10) DEFAULT NULL COMMENT 'File max size',
  `reference` int(11) DEFAULT NULL,
  `migration_ref` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_characteristic`),
  KEY `FK_ad_characteristic_ad_client_id_client` (`id_client`),
  KEY `FK_ad_characteristic_ad_table_id_table` (`id_table`),
  KEY `FK_ad_characteristic_ad_reference_id_reference` (`id_reference`),
  KEY `FK_ad_characteristic_ad_reference_id_reference_value` (`id_reference_value`),
  CONSTRAINT `FK_ad_characteristic_ad_client_id_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `FK_ad_characteristic_ad_reference_id_reference` FOREIGN KEY (`id_reference`) REFERENCES `ad_reference` (`id_reference`),
  CONSTRAINT `FK_ad_characteristic_ad_reference_id_reference_value` FOREIGN KEY (`id_reference_value`) REFERENCES `ad_reference` (`id_reference`) ON DELETE SET NULL,
  CONSTRAINT `FK_ad_characteristic_ad_table_id_table` FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `ad_characteristic_def` (
  `id_characteristic_def` varchar(32) NOT NULL COMMENT 'Definition Characteristic identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_characteristic` varchar(32) NOT NULL COMMENT 'Characteristic identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `dtype` varchar(50) NOT NULL COMMENT 'Definition type (List)',
  `def_value` varchar(250) NOT NULL COMMENT 'Table identifier',
  `mandatory` bit(1) NOT NULL COMMENT 'Mandatory',
  `seqno` int(10) DEFAULT NULL COMMENT 'Sequence number (Order)',
  PRIMARY KEY (`id_characteristic_def`),
  KEY `FK_ad_characteristic_def_ad_client_id_client` (`id_client`),
  KEY `FK_ad_characteristic_def_ad_characteristic_id_characteristic` (`id_characteristic`),
  CONSTRAINT `FK_ad_characteristic_def_ad_characteristic_id_characteristic` FOREIGN KEY (`id_characteristic`) REFERENCES `ad_characteristic` (`id_characteristic`),
  CONSTRAINT `FK_ad_characteristic_def_ad_client_id_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_characteristic_value` (
  `id_characteristic_value` varchar(32) NOT NULL COMMENT 'Characteristic value identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_characteristic` varchar(32) NOT NULL COMMENT 'Characteristic identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `key_value` varchar(32) NOT NULL COMMENT 'Row table identifier',
  `value` text COMMENT 'Characteristic value',
  PRIMARY KEY (`id_characteristic_value`),
  KEY `FK_ad_characteristic_value_ad_client_id_client` (`id_client`),
  KEY `FK_ad_characteristic_value_ad_characteristic_id_characteristic` (`id_characteristic`),
  CONSTRAINT `FK_ad_characteristic_value_ad_characteristic_id_characteristic` FOREIGN KEY (`id_characteristic`) REFERENCES `ad_characteristic` (`id_characteristic`),
  CONSTRAINT `FK_ad_characteristic_value_ad_client_id_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
