ALTER TABLE `ad_characteristic_value`
ADD COLUMN `image` VARCHAR(100) NULL COMMENT 'Image ' AFTER `value`,
ADD COLUMN `image_update` DATETIME NULL COMMENT 'Last image update' AFTER `image`,
ADD COLUMN `vtype` VARCHAR(50) NULL COMMENT 'Value type (List)' AFTER `image_update`,
ADD COLUMN `name` VARCHAR(150) NULL COMMENT 'Name for value' AFTER `vtype`,
ADD COLUMN `is_default` BIT(1) NULL COMMENT 'Is default value' AFTER `name`,
ADD COLUMN `seqno` INT NULL COMMENT 'Sequence number' AFTER `is_default`,
ADD COLUMN `reference` VARCHAR(10) NULL COMMENT 'Reference value' AFTER `seqno`;

ALTER TABLE `ad_characteristic_value`
DROP INDEX `IDX_ad_characteristic_value_1` ,
ADD UNIQUE INDEX `IDX_ad_characteristic_ref_1` (`id_client` ASC, `id_characteristic` ASC, `key_value` ASC, `reference` ASC);
