ALTER TABLE `ad_column` DROP FOREIGN KEY `fk_ad_column_ad_val_rule`;
ALTER TABLE `ad_column` DROP COLUMN `id_val_rule`, DROP INDEX `idx_ad_column_ad_val_rule`;

ALTER TABLE `ad_table` DROP FOREIGN KEY `fk_ad_table_ad_val_rule`;
ALTER TABLE `ad_table` DROP COLUMN `id_val_rule`, DROP INDEX `idx_ad_table_ad_val_rule`;

DROP TABLE `ad_val_rule`;

DELETE FROM `ad_field` WHERE `id_field`='ff8080815152c6ea01515332baa10023';
DELETE FROM `ad_field` WHERE `id_field`='ff80808151579af0015157a13ba90000';
DELETE FROM `ad_reference` WHERE `id_reference`='A05ACA9B2E1A435CA2829CD738279516';
DELETE FROM `ad_window` WHERE `id_window`='ff8080815159720e0151597a0bfc0001';

DELETE FROM `ad_table` WHERE `id_table`='ff808081515720760151577ea3630000';
