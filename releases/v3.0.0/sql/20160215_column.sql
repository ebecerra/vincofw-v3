ALTER TABLE `ad_column`
ADD COLUMN `link_column` VARCHAR(100) NULL COMMENT 'Name of link column in parent table' AFTER `link_parent`;
