CREATE TABLE `ad_image` (
  `id_image` varchar(32) NOT NULL COMMENT 'Image identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_table` varchar(32) DEFAULT NULL COMMENT 'Table identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `description` varchar(250) DEFAULT NULL COMMENT 'Description',
  `itype` varchar(50) NOT NULL COMMENT 'Image type (List)',
  `width` int(11) NOT NULL COMMENT 'Image width (pixels)',
  `height` int(11) NOT NULL COMMENT 'Image height (pixels)',
  `mimetype` varchar(255) DEFAULT NULL COMMENT 'Image MIME type',
  PRIMARY KEY (`id_image`),
  KEY `FK_ad_image_ad_client_id_client` (`id_client`),
  KEY `FK_ad_image_ad_table_id_table` (`id_table`),
  CONSTRAINT `FK_ad_image_ad_client_id_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `FK_ad_image_ad_table_id_table` FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ad_file` (
  `id_file` varchar(32) NOT NULL COMMENT 'File identifier',
  `id_client` varchar(32) NOT NULL COMMENT 'Client identifier',
  `id_table` varchar(32) DEFAULT NULL COMMENT 'Table identifier',
  `created` datetime NOT NULL COMMENT 'Creation date',
  `updated` datetime NOT NULL COMMENT 'Last update date',
  `active` bit(1) NOT NULL COMMENT 'Active row',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `description` varchar(250) DEFAULT NULL COMMENT 'Description',
  `category` varchar(50) NOT NULL COMMENT 'File category (List)',
  `filesize` int(11) NOT NULL COMMENT 'File size',
  `mimetype` varchar(255) DEFAULT NULL COMMENT 'Image MIME type',
  PRIMARY KEY (`id_file`),
  KEY `FK_ad_file_ad_client_id_client` (`id_client`),
  KEY `FK_ad_file_ad_table_id_table` (`id_table`),
  CONSTRAINT `FK_ad_file_ad_client_id_client` FOREIGN KEY (`id_client`) REFERENCES `ad_client` (`id_client`),
  CONSTRAINT `FK_ad_file_ad_table_id_table` FOREIGN KEY (`id_table`) REFERENCES `ad_table` (`id_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

