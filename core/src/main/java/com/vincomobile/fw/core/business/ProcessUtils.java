package com.vincomobile.fw.core.business;

import com.vincomobile.fw.core.persistence.model.AdProcessParam;

public class ProcessUtils {

    /**
     * Get input parameter
     *
     * @param idProcess Process identifier
     * @param name Parameter name
     * @param value Parameter value
     * @return Parameter
     */
    public static AdProcessParam getInParam(String idProcess, String name, Object value) {
        AdProcessParam newParam = new AdProcessParam();
        newParam.setIdProcess(idProcess);
        newParam.setPtype("IN");
        newParam.setName(name);
        newParam.setValue(value);
        return newParam;
    }

}
