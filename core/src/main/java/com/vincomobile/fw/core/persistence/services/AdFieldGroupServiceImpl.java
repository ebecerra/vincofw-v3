package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdFieldGroup;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service of AD_FIELD_GROUP
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdFieldGroupServiceImpl extends BaseServiceImpl<AdFieldGroup, String> implements AdFieldGroupService {

    /**
     * Constructor.
     *
     */
    public AdFieldGroupServiceImpl() {
        super(AdFieldGroup.class);
    }

}
