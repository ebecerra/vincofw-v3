package com.vincomobile.fw.core.persistence.dao;

import javax.sql.DataSource;
import java.util.Map;

public interface AdTranslationsDAO {

    /**
     * This is the method to be used to initialize database resources ie. connection.
     *
     * @param ds DataSource
     */
    void setDataSource(DataSource ds);

    /**
     * List all translation of messages
     *
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @return List of translation { key: value, value: translation }
     */
    Map<String, String> listMessages(String idClient, String idLanguage);

    /**
     * List all translation of process params
     *
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @return List of translation { key: value, value: translation }
     */
    Map<String, String> listProccessParams(String idClient, String idLanguage);
}
