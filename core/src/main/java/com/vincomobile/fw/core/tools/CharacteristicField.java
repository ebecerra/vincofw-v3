package com.vincomobile.fw.core.tools;

import com.vincomobile.fw.core.persistence.model.AdCharacteristic;

public class CharacteristicField {
    private String name;
    private String ctype;
    private String idReference;
    private String idReferenceValue;
    private Boolean mandatory;
    private String defValue;
    private int seqno;
    private AdCharacteristic characteristic;
    private String mode;
    private String idCharacteristic;
    private String idModule;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    public String getIdReferenceValue() {
        return idReferenceValue;
    }

    public void setIdReferenceValue(String idReferenceValue) {
        this.idReferenceValue = idReferenceValue;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getDefValue() {
        return defValue;
    }

    public void setDefValue(String defValue) {
        this.defValue = defValue;
    }

    public AdCharacteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(AdCharacteristic characteristic) {
        this.characteristic = characteristic;
    }

    public void setSeqno(int seqno) {
        this.seqno = seqno;
    }

    public int getSeqno() {
        return seqno;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setIdCharacteristic(String idCharacteristic) {
        this.idCharacteristic = idCharacteristic;
    }

    public String getIdCharacteristic() {
        return idCharacteristic;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    public String getIdModule() {
        return idModule;
    }
}
