package com.vincomobile.fw.core.tools;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class UnZipManager {

    private static Log logger = LogFactory.getLog(UnZipManager.class);

    private ZipFile zipFile;
    byte[] buffer;
    int rBytes;

    /**
     * Constructor UnZipManager
     *
     * @param fileName ZIP file name
     */
    public UnZipManager(String fileName) {
        try {
            zipFile = new ZipFile(fileName);
            buffer = new byte[Constants.MAX_LENGTH_READ];
            rBytes = 0;
        } catch (Exception e) {
            logger.error("Error creating the ZIP:" + e.getMessage());
        }
    }

    /**
     * Extract .ZIP content to directory
     *
     * @param fileDirName Directory name to extract .ZIP
     * @throws Exception Error in .ZIP processing
     */
    public File extractFilesFromZipFile(String fileDirName) throws Exception {
        File fileDir = new File(fileDirName);
        return extractFilesFromZipFile(fileDir);
    }

    /**
     * Extract .ZIP content to directory
     *
     * @param fileDir Directory to extract .ZIP
     * @throws Exception Error in .ZIP processing
     */
    public File extractFilesFromZipFile(File fileDir) {
        File result = null;
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        Enumeration zipEntries = zipFile.entries();
        String lastEntryFile = "";
        while (zipEntries.hasMoreElements()) {
            try {
                ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
                lastEntryFile = zipEntry.getName();
                if (zipEntry.isDirectory()) {
                    File dir = new File(fileDir + File.separator + zipEntry.getName());
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    if (result == null) {
                        result = dir;
                    }
                } else {
                    FileOutputStream fileOS = new FileOutputStream(fileDir + File.separator + zipEntry.getName());
                    InputStream fileIS = zipFile.getInputStream(zipEntry);
                    // Unzip the file
                    while ((rBytes = fileIS.read(buffer)) != -1)
                        fileOS.write(buffer, 0, rBytes);
                    fileOS.close();
                    fileIS.close();
                }
            } catch (Exception e) {
                logger.error("Fail unzip: " + e.getMessage());
                logger.error("Last entry: " + lastEntryFile);
                return null;
            }
        }
        return result == null ? fileDir : result;
    }

    public InputStream getStreamFromEntry(ZipEntry zipEntry) throws Exception {
        return zipFile.getInputStream(zipEntry);
    }

    public Enumeration getZipEntries() {
        return zipFile.entries();
    }

    /**
     * Close .ZIP file
     *
     * @throws Exception Error in .ZIP processing
     */
    public final void closeZip() throws Exception {
        zipFile.close();
    }

}
