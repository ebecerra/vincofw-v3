package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 04/10/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Model for table ad_chart_linked
 */
@Entity
@Table(name = "ad_chart_linked")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdChartLinked extends AdEntityBean {

    private String idChartLinked;
    private String idChart;
    private String idLinked;

    private AdChart linked;
    private AdChart chart;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idChartLinked;
    }

    @Override
    public void setId(String id) {
            this.idChartLinked = id;
    }

    @Id
    @Column(name = "id_chart_linked")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdChartLinked() {
        return idChartLinked;
    }

    public void setIdChartLinked(String idChartLinked) {
        this.idChartLinked = idChartLinked;
    }

    @Column(name = "id_chart", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdChart() {
        return idChart;
    }

    public void setIdChart(String idChart) {
        this.idChart = idChart;
    }

    @Column(name = "id_linked", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdLinked() {
        return idLinked;
    }

    public void setIdLinked(String idLinked) {
        this.idLinked = idLinked;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_linked", referencedColumnName = "id_chart", insertable = false, updatable = false)
    public AdChart getLinked() {
        return linked;
    }

    public void setLinked(AdChart linked) {
        this.linked = linked;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_chart", referencedColumnName = "id_chart", insertable = false, updatable = false)
    public AdChart getChart() {
        return chart;
    }

    public void setChart(AdChart chart) {
        this.chart = chart;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdChartLinked)) return false;

        final AdChartLinked that = (AdChartLinked) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idChartLinked == null) && (that.idChartLinked == null)) || (idChartLinked != null && idChartLinked.equals(that.idChartLinked)));
        result = result && (((idChart == null) && (that.idChart == null)) || (idChart != null && idChart.equals(that.idChart)));
        result = result && (((idLinked == null) && (that.idLinked == null)) || (idLinked != null && idLinked.equals(that.idLinked)));
        return result;
    }

}

