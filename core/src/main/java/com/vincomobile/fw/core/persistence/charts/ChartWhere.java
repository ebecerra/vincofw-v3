package com.vincomobile.fw.core.persistence.charts;

import com.vincomobile.fw.core.persistence.beans.AdChartTools;
import com.vincomobile.fw.core.persistence.model.AdChartFilter;
import com.vincomobile.fw.core.persistence.model.AdChartLinked;
import com.vincomobile.fw.core.persistence.services.AdChartService;
import com.vincomobile.fw.core.persistence.services.AdReferenceService;

import java.util.*;

public class ChartWhere {

    private String mode;
    private String where;
    private String compareWhere;

    public ChartWhere(String mode, String where) {
        this.mode = mode;
        this.where = where;
        this.compareWhere = mode.startsWith(AdChartService.MODE_DATE_COMPARE) ? where : null;
    }

    /**
     * Replace where parameters
     *
     * @param constraints Constraints
     * @param filters Filters list
     * @param chartLinkeds Chart linkeds
     */
    public void replaceParameters(Map<String, Object> constraints, List<AdChartFilter> filters, List<AdChartLinked> chartLinkeds) {
        where = ChartUtils.replaceRangedParameters(where, constraints, filters);
        where = ChartUtils.replaceDateFilterParameters(where, constraints, chartLinkeds);
        where = ChartUtils.replaceSingleParameters(where, constraints, chartLinkeds);

        if (mode.startsWith(AdChartService.MODE_DATE_COMPARE)) {
            Map<String, Object> compareConstraints = new HashMap();
            for (String key : constraints.keySet()) {
                String filterName = key.split("\\.")[0];
                boolean replaced = false;
                for (AdChartFilter filter : filters) {
                    if (filter.getName().equals(filterName)) {
                        if (AdReferenceService.BASE_ID_DATE.equals(filter.getIdReference())) {
                            compareConstraints.put(key, AdChartTools.compareValue(mode, (String) constraints.get(key), -1));
                            replaced = true;
                        }
                        break;
                    }
                }
                if (!replaced) {
                    compareConstraints.put(key, constraints.get(key));
                }
            }
            compareWhere = ChartUtils.replaceRangedParameters(compareWhere, compareConstraints, filters);
            compareWhere = ChartUtils.replaceDateFilterParameters(compareWhere, constraints, chartLinkeds);
            compareWhere = ChartUtils.replaceSingleParameters(compareWhere, compareConstraints, chartLinkeds);
        }
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getCompareWhere() {
        return compareWhere;
    }

    public void setCompareWhere(String compareWhere) {
        this.compareWhere = compareWhere;
    }
}
