package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdScheduler;

/**
 * Created by Vincomobile FW on 02/08/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_scheduler
 */
public interface AdSchedulerService extends BaseService<AdScheduler, String> {

}


