package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.beans.AdGuide;
import com.vincomobile.fw.core.persistence.model.AdWikiGuide;
import com.vincomobile.fw.core.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vincomobile FW on 29/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_wiki_guide
 */
@Repository
@Transactional(readOnly = true)
public class AdWikiGuideServiceImpl extends BaseServiceImpl<AdWikiGuide, String> implements AdWikiGuideService {

    @Autowired
    AdTranslationService translationService;

    @Autowired
    AdWikiTopicService wikiTopicService;

    /**
     * Constructor.
     */
    public AdWikiGuideServiceImpl() {
        super(AdWikiGuide.class);
    }

    /**
     * List available guides and it topics
     *
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idLanguage Language identifier
     * @return Guides
     */
    @Override
    public List<AdGuide> getGuides(String idClient, String idRole, String idLanguage) {
        String select = "SELECT WG FROM AdWikiGuide WG, AdWikiGuideRole WGR \n" +
                "WHERE WG.active = 1 AND WGR.idWikiGuide = WG.idWikiGuide AND WGR.idRole = :idRole \n" +
                "AND WG.idModule IN (SELECT moduleId FROM AdClientModule WHERE idClient = :idClient) AND WG.idClient IN :clients \n" +
                "ORDER BY WG.seqno";
        Query query = entityManager.createQuery(select, AdWikiGuide.class);
        query.setParameter("idClient", idClient);
        query.setParameter("idRole", idRole);
        query.setParameter("clients", getClientFilter(idClient));

        List<AdGuide> result = new ArrayList<>();
        List<AdWikiGuide> guides = query.getResultList();
        for (AdWikiGuide guide : guides) {
            String title, content;
            if (!Converter.isEmpty(idLanguage)) {
                title  = translationService.getTranslation(idClient, FWConfig.TABLE_ID_WIKI_GUIDE, FWConfig.COLUMN_ID_WIKI_GUIDE_TITLE, idLanguage, guide.getIdWikiGuide(), guide.getTitle());
                content = translationService.getTranslation(idClient, FWConfig.TABLE_ID_WIKI_GUIDE, FWConfig.COLUMN_ID_WIKI_GUIDE_CONTENT, idLanguage, guide.getIdWikiGuide(), guide.getContent());
            } else {
                title = guide.getTitle();
                content = guide.getContent();
            }
            AdGuide resGuide = new AdGuide(guide.getIdWikiGuide(), guide.getName(), title, content, guide.getSeqno());
            resGuide.setTopics(wikiTopicService.loadTopicTree(ROOT_TOPIC, idClient, idLanguage, guide.getIdWikiGuide()));
            result.add(resGuide);
        }
        return result;
    }

}
