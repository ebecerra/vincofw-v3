package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdTableAction;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 16/02/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_table_action
 */
@Repository
@Transactional(readOnly = true)
public class AdTableActionServiceImpl extends BaseServiceImpl<AdTableAction, String> implements AdTableActionService {

    /**
     * Constructor.
     */
    public AdTableActionServiceImpl() {
        super(AdTableAction.class);
    }

}
