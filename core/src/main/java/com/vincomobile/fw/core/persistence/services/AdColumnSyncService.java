package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdColumnSync;

import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_column_sync
 *
 * Date: 05/03/2016
 */
public interface AdColumnSyncService extends BaseService<AdColumnSync, String> {

    /**
     * Find a column in table by name
     *
     * @param idTableSync Table id.
     * @param name Column name
     * @return AdColumnSync
     */
    AdColumnSync findByName(String idTableSync, String name);

    /**
     * Get all columns for a table
     *
     * @param idTableSync Table id.
     * @return Columns
     */
    List<AdColumnSync> getTableColumns(String idTableSync);

    /**
     * Get SQL SELECT to get a record
     *
     * @param columns Column list
     * @param tablename Table name
     * @return SELECT
     */
    String getFindSelect(List<AdColumnSync> columns, String tablename);

    /**
     * Get INSERT statement
     *
     * @param columns Column list
     * @param tablename Table name
     * @param idClient Client identifier
     * @return INSERT
     */
    String getInsertSt(List<AdColumnSync> columns, String tablename, String idClient);

    /**
     * Get UPDATE statement
     *
     * @param columns Column list
     * @param tablename Table name
     * @return UPDATE
     */
    String getUpdateSt(List<AdColumnSync> columns, String tablename);

}


