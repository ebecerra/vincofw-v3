package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 07/11/2018.
 * Copyright © 2018 Vincomobile. All rights reserved.
 *
 * Model for table ad_user_hist_pwd
 */
@Entity
@Table(name = "ad_user_hist_pwd")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdUserHistPwd extends EntityBean<String> {

    private String idUserHistPwd;
    private String idClient;
    private String idUser;
    private String passwd;


    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idUserHistPwd;
    }

    @Override
    public void setId(String id) {
            this.idUserHistPwd = id;
    }

    @Id
    @Column(name = "id_user_hist_pwd")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdUserHistPwd() {
        return idUserHistPwd;
    }

    public void setIdUserHistPwd(String idUserHistPwd) {
        this.idUserHistPwd = idUserHistPwd;
    }

    @Column(name = "id_client", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "id_user", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "passwd", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdUserHistPwd)) return false;

        final AdUserHistPwd that = (AdUserHistPwd) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idUserHistPwd == null) && (that.idUserHistPwd == null)) || (idUserHistPwd != null && idUserHistPwd.equals(that.idUserHistPwd)));
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((passwd == null) && (that.passwd == null)) || (passwd != null && passwd.equals(that.passwd)));
        return result;
    }


}

