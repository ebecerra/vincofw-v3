package com.vincomobile.fw.core.session;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vincomobile.fw.core.tools.Converter;

import java.util.*;

public class SessionInfo {
    String idUser;
    String userType;
    String user;
    String userRole;
    String userName;
    Date connect;
    Date lastUpdate;
    Map<String, HttpInfo> sessions;

    public SessionInfo(String idUser, String user, String userName, String userType) {
        this.idUser = idUser;
        this.user = user;
        this.userRole = user;
        this.userName = userName;
        this.userType = userType;
        this.connect = new Date();
        this.lastUpdate = new Date();
        this.sessions = new HashMap<>();
    }

    public SessionInfo(String user) {
        this.idUser = null;
        this.user = user;
        this.userName = user;
        this.connect = new Date();
        this.lastUpdate = new Date();
        this.sessions = new HashMap<>();
    }

    public void updateSession(String idSession, String userAgent, String ip, String host) {
        HttpInfo session = sessions.get(idSession);
        if (session != null) {
            session.update(userAgent);
        } else {
            sessions.put(idSession, new HttpInfo(idSession, userAgent, ip, host));
        }
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM/dd/yyyy HH:mm:ss", timezone="CET")
    public Date getConnect() {
        return connect;
    }

    public void setConnect(Date connect) {
        this.connect = connect;
    }

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM/dd/yyyy HH:mm:ss", timezone="CET")
    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getSessions() {
        String result = "";
        int indx = 1;
        for (Map.Entry<String, HttpInfo> entry : sessions.entrySet()) {
            if (indx > 1)
                result += "<br/>";
            HttpInfo info = entry.getValue();
            result += indx + ". <b>" + entry.getKey() + "</b>: <i>" + Converter.formatDate(info.getLastUpdate(), "dd/MM/yyyy HH:mm:ss") + "</i> - " + info.getUserAgent();
            indx++;
        }
        return result;
    }

    public List<HttpInfo> getSessionsInfo() {
        List<HttpInfo> result = new ArrayList<>();
        for (Map.Entry<String, HttpInfo> entry : sessions.entrySet()) {
            result.add(entry.getValue());
        }
        return result;
    }

}
