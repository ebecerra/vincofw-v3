package com.vincomobile.fw.core;

/**
 * Created by yokiro on 09/11/2015.
 */
public class JSONColumnStructure{

    private String name;
    private String cType;
    private boolean primaryKey;

    public JSONColumnStructure(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getcType() {
        return cType;
    }

    public void setcType(String cType) {
        this.cType = cType;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }
}
