package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdWikiEntry;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 29/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_wiki_entry
 */
@Repository
@Transactional(readOnly = true)
public class AdWikiEntryServiceImpl extends BaseServiceImpl<AdWikiEntry, String> implements AdWikiEntryService {

    /**
     * Constructor.
     */
    public AdWikiEntryServiceImpl() {
        super(AdWikiEntry.class);
    }

}
