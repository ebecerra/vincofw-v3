package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdColumnSync;
import com.vincomobile.fw.core.persistence.model.AdTableSync;
import com.vincomobile.fw.core.sync.*;
import com.vincomobile.fw.core.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.*;

/**
 * Created by Devtools.
 * Service layer implementation for ad_table_sync
 *
 * Date: 05/03/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdTableSyncServiceImpl extends BaseServiceImpl<AdTableSync, String> implements AdTableSyncService {

    private Logger logger = LoggerFactory.getLogger(AdTableSyncServiceImpl.class);

    /**
     * Constructor.
     */
    public AdTableSyncServiceImpl() {
        super(AdTableSync.class);
    }

    @Autowired
    AdColumnSyncService columnSyncService;

    @Autowired
    AdRefSequenceService sequenceService;

    @Autowired
    AdAuxInputService auxInputService;

    /**
     * Get database date
     *
     * @return Date
     */
    @Override
    public Date getDatabaseDatetime() {
        Query query = entityManager.createQuery("SELECT CURRENT_TIMESTAMP FROM AdTableSync");
        Object result = query.getSingleResult();
        return (Date) result;
    }

    /**
     * Get table information
     *
     * @param name Table name
     * @return AdTable
     */
    @Override
    public AdTableSync getTable(String name) {
        Map filter = new HashMap();
        filter.put("table.name", name);
        AdTableSync result = findFirst(filter);
        if (result == null) {
            filter.clear();
            filter.put("nameRemote", name);
            result = findFirst(filter);
        }
        return result;
    }

    /**
     * Gets the name of a file replacing parameters @[name]
     *
     * @param table Table information
     * @param name  File name
     * @return Real name
     */
    @Override
    public String getFileName(AdTableSync table, String name) {
        name = name.replace("@CapitalizeStandardName", table.getTable().getCapitalizeStandardName());
        return name;
    }

    /**
     * Obtains information update an entire table
     *
     * @param idClient Client identifier
     * @param sincro Informacion de sincronizacion (a completar)
     * @param tableSync Synchronization information (to be completed)
     * @param filters Filter to apply
     * @param param_1 Parameter extra 1
     * @param param_2 Parameter extra 2
     * @param param_3 Parameter extra 3
     * @param param_4 Parameter extra 4
     * @param param_5 Parameter extra 5
     */
    @Override
    public void getTableUpdateSQL(String idClient, SyncTable sincro, AdTableSync tableSync, String filters, String param_1, String param_2, String param_3, String param_4, String param_5) {
        String deleteWhere = tableSync.getDeleteRemote();
        sincro.setDeleteSt("DELETE FROM " + tableSync.getTableRemote() + (Converter.isEmpty(deleteWhere) ? "" : " WHERE " + deleteWhere));
        String tSql;
        List<AdColumnSync> columns = columnSyncService.getTableColumns(tableSync.getIdTableSync());
        if (!Converter.isEmpty(tableSync.getXselect()) || !Converter.isEmpty(tableSync.getXfrom()) || !Converter.isEmpty(tableSync.getXwhere())) {
            // Build SELECT using field from XSELECT, XFROM, XWHERE y XGROUP
            tSql = "SELECT ";
            if (Converter.isEmpty(tableSync.getXselect()))
                tSql += getFieldSelect(columns);
            else
                tSql += tableSync.getXselect();
            if (Converter.isEmpty(tableSync.getXfrom())) {
                String xPrefix = tableSync.getXprefix();
                tSql += " FROM " + tableSync.getTable().getName() + (Converter.isEmpty(xPrefix) ? "" : " " + xPrefix.substring(0, xPrefix.indexOf('.')));
            } else {
                tSql += " FROM " + replaceParameters(idClient, tableSync.getXfrom(), param_1, param_2, param_3, param_4, param_5);
            }
            if (!Converter.isEmpty(tableSync.getXwhere())) {
                tSql += " WHERE " + replaceParams(idClient, tableSync.getXwhere(), param_1, param_2, param_3, param_4, param_5);
                if (!Converter.isEmpty(filters))
                    tSql += " AND " + replaceParameters(idClient, filters, param_1, param_2, param_3, param_4, param_5);
                if (!Converter.isEmpty(tableSync.getXgroup()))
                    tSql += " GROUP BY "+tableSync.getXgroup();
            } else if (!Converter.isEmpty(filters))
                tSql += " WHERE " + replaceParameters(idClient, filters, param_1, param_2, param_3, param_4, param_5);
        } else {
            // Standard select for table
            tSql = "SELECT "+getFieldSelect(columns)+" FROM "+tableSync.getTable().getName();
            if (!Converter.isEmpty(filters))
                tSql += " WHERE " + replaceParameters(idClient, filters, param_1, param_2, param_3, param_4, param_5);
        }
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(tableSync.getTableRemote()).append(" (").append(getFieldNames(columns)).append(") ");
        sql.append("VALUES (").append(getAllNamesParams(columns)).append(")");
        sincro.setInsertSt(sql.toString());
        Query query = entityManager.createNativeQuery(tSql);
        List result = query.getResultList();
        initFields(columns);
        if (!Converter.isEmpty(tableSync.getXselect())) {
            for (Object row : result) {
                sincro.add(getListValue(tableSync.getXselect(), (Object[]) row, columns));
            }
        } else {
            for (Object row : result) {
                sincro.add(getListValue((Object[]) row, columns));
            }
        }
    }

    /**
     * Replace the parameters in the SQL
     *
     * @param idClient Client identifier
     * @param value Value parameters to replace
     * @param param_1 Parameter extra 1
     * @param param_2 Parameter extra 2
     * @param param_3 Parameter extra 3
     * @param param_4 Parameter extra 4
     * @param param_5 Parameter extra 5
     * @return Values replaced
     */
    private String replaceParams(String idClient, String value, String param_1, String param_2, String param_3, String param_4, String param_5) {
        value = replaceParameters(idClient, value, param_1, param_2, param_3, param_4, param_5);
        return auxInputService.replaceAuxInputForIn(idClient, value, param_1, param_2, param_3, param_4, param_5);
    }

    /**
     * Import data from device
     *
     * @param exportDB Export information from device
     * @param idClient Client identifier
     * @return Import errors
     * @throws Exception Processing exception
     */
    @Override
    public SyncError makeUpdate(String idClient, SyncExportDB exportDB) throws Exception {
        SyncError result = new SyncError();
        long time = 0;
        AdTableSync currentTable = null;
        List<AdColumnSync> fieldList = null;
        for (int i = 0; i < exportDB.getTables().size(); i++) {
            SyncOfflineTable table = exportDB.getTables().get(i);
            if (table.getItems().size() > 0) {
                time = System.currentTimeMillis();
                logger.info("Updating table: "+table.getName());
                currentTable = getTableInfo(result, table.getName());
                if (currentTable == null) {
                    if (exportDB.isIgnoreUndefined())
                        logger.info("\tNot found the table information, it will be ignored");
                    else
                        logger.error("\tNot found the table information");
                } else {
                    fieldList = loadFieldList(result, currentTable, table.getColumns());
                    if (fieldList == null) {
                        return result;
                    }
                    result.setRow(0);
                    for (SyncOfflineItem row: table.getItems()) {
                        if (!processRow(result, currentTable, fieldList, row, idClient)) {
                            return result;
                        }
                    }
                }
                time = System.currentTimeMillis() - time;
                logger.info("\tProcessing time: "+time/1000+"."+time%1000);
            }
        }
        return result;
    }

    /**
     * Get select from column list
     *
     * @param columns Column list
     * @return SELECT
     */
    private String getFieldSelect(List<AdColumnSync> columns) {
        String select = "";
        for (AdColumnSync columnSync : columns) {
            if (SOURCE_DATABASE.equals(columnSync.getCsource()) && columnSync.getImportable()) {
                if (select.length() > 0)
                    select += ", ";
                select += columnSync.getName();
            }
        }
        return select;
    }

    /**
     * Get names from column list
     *
     * @param columns Column list
     * @return Names
     */
    private String getFieldNames(List<AdColumnSync> columns) {
        String names = "";
        for (AdColumnSync columnSync : columns) {
            if (columnSync.getImportable()) {
                if (names.length() > 0)
                    names += ", ";
                names += columnSync.getName();
            }
        }
        return names;
    }

    /**
     * List of all parameters (?) For the names separated by commas
     *
     * @param columns Column list
     * @return Parameter list (?)
     */
    private String getAllNamesParams(List<AdColumnSync> columns) {
        String allParams = "";
        for (AdColumnSync columnSync : columns) {
            if (columnSync.getImportable()) {
                if (allParams.length() > 0)
                    allParams += ", ";
                allParams += "?";
            }
        }
        return allParams;
    }

    /**
     * Initialize the DB fields that are constants, parameters or maximum
     *
     * @param columns Column list
     */
    private void initFields(List<AdColumnSync> columns)  {
        for (AdColumnSync columnSync : columns) {
            if ("VALUE".equals(columnSync.getCsource())) {
                columnSync.setValue(columnSync.getCsourceValue());
            }
        }
    }

    /**
     * Gets the values to the INSERT / UPDATE
     *
     * @param xSelect Select fields
     * @param item Row data
     * @param columns Columns information
     * @return Values for the INSERT/UPDATE
     */
    private List getListValue(String xSelect, Object[] item, List<AdColumnSync> columns) {
        int indx = 0;
        List values = new ArrayList();
        List<String> fields = new ArrayList<>();
        int openBracket = 0, tokenIndex = 0;
        for (int i = 0; i < xSelect.length(); i++) {
            if (xSelect.charAt(i) == '(')
                openBracket++;
            if (xSelect.charAt(i) == ')')
                openBracket--;
            if (xSelect.charAt(i) == ',' && openBracket == 0) {
                String fieldName = xSelect.substring(tokenIndex, i);
                tokenIndex = i + 1;
                fields.add(fieldName.trim());
            }
        }
        String fieldName = xSelect.substring(tokenIndex);
        fields.add(fieldName.trim());
        for (String field : fields) {
            int as = field.toLowerCase().indexOf(" as ");
            if (as > 0) {
                field = field.substring(as + 4);
            }
            int point = field.indexOf('.');
            if (point >= 0)
                field = field.substring(point + 1);
            field = field.trim();
            for (AdColumnSync columnSync : columns) {
                if (columnSync.getName().equalsIgnoreCase(field.trim())) {
                    if (columnSync.getImportable()) {
                        if (SOURCE_DATABASE.equals(columnSync.getCsource())) {
                            columnSync.setValue(item[indx++]);
                        }
                        values.add(columnSync.getSqliteValue());
                    }
                    break;
                }
            }
        }
        // Add not real field
        for (AdColumnSync columnSync : columns) {
            if ("VALUE".equals(columnSync.getCsource())) {
                values.add(columnSync.getSqliteValue());
            }
        }
        return values;
    }

    /**
     * Gets the values to the INSERT / UPDATE
     *
     * @param item Row data
     * @param columns Columns information
     * @return Valores para el INSERT/UPDATE
     */
    private List getListValue(Object[] item, List<AdColumnSync> columns) {
        int indx = 0;
        List values = new ArrayList();
        for (AdColumnSync columnSync : columns) {
            if (columnSync.getImportable()) {
                if (SOURCE_DATABASE.equals(columnSync.getCsource())) {
                    columnSync.setValue(item[indx++]);
                }
                values.add(columnSync.getSqliteValue());
            }
        }
        return values;
    }

    /**
     * Get table information
     *
     * @param result Error information
     * @param tablename Table name
     * @return Table information
     */
    private AdTableSync getTableInfo(SyncError result, String tablename) {
        logger.info("\tTable: "+tablename);
        Map filter = new HashMap();
        filter.put("table.name", tablename);
        AdTableSync table = findFirst(filter);
        if (table != null)
            table.loadTriggerClass(entityManager, sequenceService);
        result.setTable(tablename);
        result.setResult(table == null ? 1 : 0);
        return table;
    }

    /**
     * Get columns list for table
     *
     * @param result Error information
     * @param currentTable Current table
     * @param columns Columns names
     * @return Columns information
     */
    private List<AdColumnSync> loadFieldList(SyncError result, AdTableSync currentTable, List<String> columns) {
        List<AdColumnSync> fields = new ArrayList<AdColumnSync>();
        for (String name : columns) {
            boolean find = false;
            for (int i = 0; i < currentTable.getColumns().size(); i++) {
                AdColumnSync columnSync = currentTable.getColumns().get(i);
                if (name.equalsIgnoreCase(columnSync.getName())) {
                    fields.add(columnSync);
                    find = true;
                    break;
                }
            }
            if (!find) {
                logger.error("Not found the column definition: " + name);
                result.setResult(2);
                result.setField(name);
                return null;
            }
        }
        return fields;
    }

    /**
     * Proccess a row
     *
     * @param result Error information
     * @param currentTable Current proccesing table
     * @param columns Columns list
     * @param row Row values
     * @param idClient Client identifier
     * @return Proccess successful
     */
    private boolean processRow(SyncError result, AdTableSync currentTable, List<AdColumnSync> columns, SyncOfflineItem row, String idClient) {
        result.incrementRow();
        result.setIndex(0);
        row.fillColums(columns);
        int sqlResult = 0;
        // Triggers
        if (currentTable.hasTriggers()) {
            String select = columnSyncService.getFindSelect(columns, currentTable.getName());
            int updateCount = 0;
            if (select != null) {
                Query query = entityManager.createNativeQuery(select);
                List qResult = query.getResultList();
                updateCount = qResult.size();
                if (updateCount > 0) {
                    Object[] item = (Object[]) qResult.get(0);
                    int indx = 0;
                    for (AdColumnSync column : columns) {
                        if (AdTableSyncService.SOURCE_DATABASE.equals(column.getCsource())) {
                            column.setOldValue(item[indx++]);
                        }
                    }
                    // Update
                    if (currentTable.beforeUpdate(columns)) {
                        String sql = columnSyncService.getUpdateSt(columns, currentTable.getName());
                        Query update = entityManager.createNativeQuery(sql);
                        sqlResult = sql == null ? 0 : update.executeUpdate();
                        if (sqlResult != 0)
                            currentTable.afterUpdate(columns);
                        else {
                            logger.error("\t\tUpdating error");
                            logger.error(sql);
                        }
                    } else
                        sqlResult = 1;
                }
            }
            if (updateCount == 0) {
                // Insert
                if (currentTable.beforeInsert(columns)) {
                    String sql = columnSyncService.getInsertSt(columns, currentTable.getName(), idClient);
                    Query insert = entityManager.createNativeQuery(sql);
                    sqlResult = insert.executeUpdate();
                    if (sqlResult != 0)
                        currentTable.afterInsert(columns);
                    else {
                        logger.error("\t\tInserting error");
                        logger.error(sql);
                    }
                } else
                    sqlResult = 1;
            }
            if (sqlResult == 0) {
                result.setResult(3);
            }
        } else {
            // Update without triggers
            String sql = columnSyncService.getUpdateSt(columns, currentTable.getName());
            sqlResult = sql == null ? 0 : entityManager.createNativeQuery(sql).executeUpdate();
            if (sqlResult == 0) {
                sql = columnSyncService.getInsertSt(columns, currentTable.getName(), idClient);
                Query update = entityManager.createNativeQuery(sql);
                sqlResult = update.executeUpdate();
            }
            if (sqlResult == 0) {
                result.setResult(3);
            }
        }
        return sqlResult == 1;
    }

}
