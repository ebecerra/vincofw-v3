package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcessOutput;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service layer implementation for ad_process_output
 *
 * Date: 29/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdProcessOutputServiceImpl extends BaseServiceImpl<AdProcessOutput, String> implements AdProcessOutputService {

    /**
     * Constructor.
     */
    public AdProcessOutputServiceImpl() {
        super(AdProcessOutput.class);
    }

}
