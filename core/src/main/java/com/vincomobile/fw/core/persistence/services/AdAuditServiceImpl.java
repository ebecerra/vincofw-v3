package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdAudit;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 02/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_audit
 */
@Repository
@Transactional(readOnly = true)
public class AdAuditServiceImpl extends BaseServiceImpl<AdAudit, String> implements AdAuditService {

    /**
     * Constructor.
     */
    public AdAuditServiceImpl() {
        super(AdAudit.class);
    }

}
