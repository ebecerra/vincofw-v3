package com.vincomobile.fw.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FWRuntimeEnvironmentConfig implements IRuntimeEnvironmentConfig {

    private String enviroment;
    private String excludeProcess;
    private Boolean excludeQuartz;


    @Override
    public String getEnvironment() {
        return enviroment;
    }

    @Override
    @Value("${runtime.environment}")
    public void setEnvironment(String enviroment) {
        this.enviroment = enviroment;
    }

    @Override
    public String getExcludeProcess() {
        return excludeProcess;
    }

    @Override
    @Value("${runtime.exclude.process}")
    public void setExcludeProcess(String excludeProcess) {
        this.excludeProcess = excludeProcess;
    }

    @Override
    public Boolean getExcludeQuartz() {
        return excludeQuartz;
    }

    @Override
    @Value("${runtime.exclude.quartz}")
    public void setExcludeQuartz(Boolean excludeQuartz) {
        this.excludeQuartz = excludeQuartz;
    }
}
