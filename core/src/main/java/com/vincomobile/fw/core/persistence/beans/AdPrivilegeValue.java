package com.vincomobile.fw.core.persistence.beans;

import com.vincomobile.fw.core.persistence.model.AdPrivilege;

public class AdPrivilegeValue {

    private String idPrivilege;
    private String name;

    public AdPrivilegeValue(AdPrivilege item) {
        this.idPrivilege = item.getIdPrivilege();
        this.name = item.getName();
    }

    public String getIdPrivilege() {
        return idPrivilege;
    }

    public String getName() {
        return name;
    }

}
