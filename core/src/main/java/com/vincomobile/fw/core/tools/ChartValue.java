package com.vincomobile.fw.core.tools;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChartValue {
    String name, hint;
    String str1, str2, str3, str4, str5;
    double value1, value2, value3, value4, value5, value6, value7, value8, value9, value10;
    int int1, int2, int3, int4, int5;
    Date date1, date2;

    public ChartValue() {
    }

    public ChartValue(String name) {
        this.name = name;
    }

    public ChartValue(String name, double value1) {
        this.name = name;
        this.value1 = value1;
    }

    public ChartValue(String name, double value1, double value2) {
        this.name = name;
        this.value1 = value1;
        this.value2 = value2;
    }

    public ChartValue(String name, double value1, double value2, double value3) {
        this.name = name;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }

    public ChartValue(String name, double value1, double value2, double value3, double value4) {
        this.name = name;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    public ChartValue(String name, double value1, double value2, double value3, double value4, double value5) {
        this.name = name;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
        this.value5 = value5;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public double getValue1() {
        return value1;
    }

    public void setValue1(double value1) {
        this.value1 = value1;
    }

    public double getValue2() {
        return value2;
    }

    public void setValue2(double value2) {
        this.value2 = value2;
    }

    public double getValue3() {
        return value3;
    }

    public void setValue3(double value3) {
        this.value3 = value3;
    }

    public double getValue4() {
        return value4;
    }

    public void setValue4(double value4) {
        this.value4 = value4;
    }

    public double getValue5() {
        return value5;
    }

    public void setValue5(double value5) {
        this.value5 = value5;
    }

    public double getValue6() {
        return value6;
    }

    public void setValue6(double value6) {
        this.value6 = value6;
    }

    public double getValue7() {
        return value7;
    }

    public void setValue7(double value7) {
        this.value7 = value7;
    }

    public double getValue8() {
        return value8;
    }

    public void setValue8(double value8) {
        this.value8 = value8;
    }

    public double getValue9() {
        return value9;
    }

    public void setValue9(double value9) {
        this.value9 = value9;
    }

    public double getValue10() {
        return value10;
    }

    public void setValue10(double value10) {
        this.value10 = value10;
    }

    public int getInt1() {
        return int1;
    }

    public void setInt1(int int1) {
        this.int1 = int1;
    }

    public int getInt2() {
        return int2;
    }

    public void setInt2(int int2) {
        this.int2 = int2;
    }

    public int getInt3() {
        return int3;
    }

    public void setInt3(int int3) {
        this.int3 = int3;
    }

    public int getInt4() {
        return int4;
    }

    public void setInt4(int int4) {
        this.int4 = int4;
    }

    public int getInt5() {
        return int5;
    }

    public void setInt5(int int5) {
        this.int5 = int5;
    }

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    public String getStr3() {
        return str3;
    }

    public void setStr3(String str3) {
        this.str3 = str3;
    }

    public String getStr4() {
        return str4;
    }

    public void setStr4(String str4) {
        this.str4 = str4;
    }

    public String getStr5() {
        return str5;
    }

    public void setStr5(String str5) {
        this.str5 = str5;
    }

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM/dd/yyyy HH:mm:ss", timezone="CET")
    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM/dd/yyyy HH:mm:ss", timezone="CET")
    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }
}
