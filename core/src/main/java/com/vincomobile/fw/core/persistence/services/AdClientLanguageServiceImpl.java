package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdClientLanguage;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 07/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_client_language
 */
@Repository
@Transactional(readOnly = true)
public class AdClientLanguageServiceImpl extends BaseServiceImpl<AdClientLanguage, String> implements AdClientLanguageService {

    /**
     * Constructor.
     */
    public AdClientLanguageServiceImpl() {
        super(AdClientLanguage.class);
    }

}
