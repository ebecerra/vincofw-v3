package com.vincomobile.fw.core.tools;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Date: 22-ene-2006
 * Time: 11:48:42
 */
public class Datetool {

    Date timer;

    /**
     * Initialize timer counter
     */
    public void initTimeCounter() {
        timer = new Date();
    }

    /**
     * Return time elapsed in miliseconds
     *
     * @return Time elapsed from timer started (initTimeCounter())
     */
    public long getTimer() {
        Date ct = new Date();
        return ct.getTime() - timer.getTime();
    }

    /**
     * Return time elapsed  hh:mm:ss.sss
     *
     * @return Return elapsed time in string
     */
    public String getStringTimer() {
        long time = getTimer();
        long seg  = time / 1000;
        long min  = seg / 60;
        long hour = min / 60;
        return Converter.leftFill(hour, '0', 2)+":"+Converter.leftFill(min % 60, '0', 2)+":"+Converter.leftFill(seg % 60, '0', 2)+"."+(time-seg*1000); 
    }

    /**
     * Get current year
     *
     * @return Current year
     */
    public static int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return cal.get(Calendar.YEAR);
    }

    /**
     * Get current month
     * @return Current month (1 - January, 2 - February, ...)
     */
    public static int getCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return cal.get(Calendar.MONTH)+1;
    }

    /**
     * Get current day
     *
     * @return Current day
     */
    public static int getCurrentDay() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Get month for date
     *
     * @param date Date
     * @return Month (1 - January, 2 - February, ...)
     */
    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH)+1;
    }

    /**
     * Get a day for date
     *
     * @param date Date
     * @return Day (1 - 31)
     */
    public static int getDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Get a day of week for date
     *
     * @param date Date
     * @return Day (MONDAY - SATURDAY)
     */
    public static int getWeekDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * Get a calendar
     *
     * @param day Day
     * @param month Month
     * @param year Year
     * @return Calendar
     */
    public static Calendar getCalendar(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.YEAR, year);
        return cal;
    }

    /**
     * Return a date
     *
     * @param day Day
     * @param month Month
     * @param year year
     * @return Date
     */
    public static Date getDate(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.YEAR, year);
        return cal.getTime();
    }

    /**
     * Return a date for a week (Sunday)
     *
     * @param year year
     * @param week year
     * @return Date
     */
    public static Date getDate(int year, int week) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return cal.getTime();
    }

    /**
     * Return a time
     *
     * @param hour Hour
     * @param minute Minutes
     * @param second Seconds
     * @return Time
     */
    public static Date getTime(int hour, int minute, int second) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        return cal.getTime();
    }

    /**
     * Remove time information
     *
     * @param cal Calendar
     */
    public static void clearTime(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
    }

    /**
     * Remove time information
     *
     * @param value Date
     * @return Date with clear time (00:00.00.000)
     */
    public static Date clearTime(Date value) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(value);
        clearTime(cal);
        return cal.getTime();
    }

    /**
     * Get current time
     *
     * @return Current time
     */
    public static Date getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.YEAR, 1970);
        return cal.getTime();
    }

    /**
     * Get current Week
     *
     * @return Current week
     */
    public static int getCurrentWeek() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return cal.get(Calendar.WEEK_OF_YEAR) + 1;
    }

    /**
     * Get year from a Date
     *
     * @param date Date
     * @return Year
     */
    public static int getYear(Date date) {
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal.get(Calendar.YEAR);
        }
        return 0;
    }

    /**
     * Get first day to previous month to "01/month/year"
     * @param year Year
     * @param month Month (1 - January, 2 - February, ...)
     * @param count Number of months
     * @return First day of month
     */
    public static Date getFirstDayOfPreviousMonth(int year, int month, int count) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month-1, 1);
        if (count != 0)
            cal.add(Calendar.MONTH, -count);
        return cal.getTime();
    }

    /**
     * Get first day of next month
     *
     * @param year year
     * @param month Month (1 - January, 2 - February, ...)
     * @return First day of next month
     */
    public static Date getFirstDayOfNextMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, 1);
        return cal.getTime();
    }

    /**
     * Get quantity of months between two month
     *
     * @param cal1 First month
     * @param cal2 Second month
     * @return Quantity of month between two month
     */
    public static int getMonthsBetween(Calendar cal1, Calendar cal2) {
        int years = Math.abs(cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR));
        int months;
        if (years == 0) {
            months = cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
        } else {
            if (cal1.before(cal2))
                months = (years - 1) * 12 + 12 - cal1.get(Calendar.MONTH) + cal2.get(Calendar.MONTH);
            else
                months = (years - 1) * 12 + 12 - cal2.get(Calendar.MONTH) + cal1.get(Calendar.MONTH);
        }
        return Math.abs(months)+1;
    }

    public static int getMonthsBetween(Date date1, Date date2) {
        return getTimeBetween(date1, date2, Calendar.MONTH);
    }

    /**
     * Get last day of month
     *
     * @param year Year
     * @param month Month (1 - January, 2 - February, ...)
     * @return Last day of month
     */
    public static int getLastDayOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month-1, 1);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * Get last day of year
     *
     * @param year Year
     * @return Last day of year
     */
    public static Date getLastDayOfYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, 11, 31);
        return cal.getTime();
    }

    /**
     * Get first day of year
     *
     * @param year Year
     * @return First day of year
     */
    public static Date getFirstDayOfYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, 0, 1);
        return cal.getTime();
    }

    /**
     * Get current day with cleared time (00:00:00)
     *
     * @return Current day
     */
    public static Date getToday() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Check if two date are equals without time
     *
     * @param d1 Date 1
     * @param d2 Date 2
     * @return True if are the same day
     */
    public static boolean equalDate(Date d1, Date d2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        return cal1.compareTo(cal2) == 0;
    }

    /**
     * Get date from XML element (WebServices)
     * 
     * @param xml XML Date
     * @return Date
     */
    public static Date getDate(XMLGregorianCalendar xml) {
        if (xml == null || !xml.isValid())
            return null;
        GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
        cal.set(xml.getYear(), xml.getMonth()-1, xml.getDay());
        return cal.getTime();
    }

    /**
     * Get week from date
     *
     * @param value Date
     * @return Week
     */
    public static int getWeek(Date value) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(value);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * Get next day
     *
     * @param value Date
     * @return Next day
     */
    public static Date getNextDay(Date value) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(value);
        cal.add(Calendar.DAY_OF_YEAR, 1);
        return cal.getTime();
    }

    /**
     * Get next day
     *
     * @param value Date
     * @return Next day
     */
    public static Date getPreviousDay(Date value) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(value);
        cal.add(Calendar.DAY_OF_YEAR, -1);
        return cal.getTime();
    }

    /**
     * Get next month
     *
     * @param value Date
     * @return Next month
     */
    public static Date getNextMonth(Date value) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(value);
        cal.add(Calendar.MONTH, 1);
        return cal.getTime();
    }

    /**
     * Get number of day between dates
     *
     * @param date1 Date 1
     * @param date2 Date 2
     * @return Days
     */
    public static int getDaysBetween(Date date1, Date date2) {
        return getTimeBetween(date1, date2, Calendar.DAY_OF_YEAR);
    }

    /**
     * Get number of hours between dates
     *
     * @param date1 Date 1
     * @param date2 Date 2
     * @return Hours
     */
    public static int getHoursBetween(Date date1, Date date2) {
        return getTimeBetween(date1, date2, Calendar.HOUR);
    }

    /**
     * Get number of minutes between dates
     *
     * @param date1 Date 1
     * @param date2 Date 2
     * @return Minutes
     */
    public static int getMinutesBetween(Date date1, Date date2) {
        return getTimeBetween(date1, date2, Calendar.MINUTE);
    }

    /**
     * Get time units between dates
     *
     * @param date1 Date 1
     * @param date2 Date 2
     * @param time Time unit (DAY_OF_YEAR, MONTH, HOUR, MINUTE)
     * @return Units
     */
    public static int getTimeBetween(Date date1, Date date2, int time) {
        Calendar d1 = Calendar.getInstance();
        Calendar d2 = Calendar.getInstance();
        d1.setTime(date1);
        d2.setTime(date2);
        if (Calendar.DAY_OF_YEAR == time) {
            double days = ((double) (d2.getTimeInMillis() - d1.getTimeInMillis())) / (1000 * 60 * 60 * 24);
            int result = (int) days;
            return days - result > 0.5 ? result + 1 : result;
        }
        if (Calendar.MONTH == time) {
            return getMonthsBetween(d1, d2);
        }
        if (Calendar.HOUR == time) {
            double hours = ((double) (d2.getTimeInMillis() - d1.getTimeInMillis())) / (1000 * 60 * 60);
            int result = (int) hours;
            return Math.abs(hours - result) > 0.5 ? result + 1 : result;
        }
        if (Calendar.MINUTE == time) {
            double minutes = ((double) (d2.getTimeInMillis() - d1.getTimeInMillis())) / (1000 * 60);
            int result = (int) minutes;
            return Math.abs(minutes - result) > 0.5 ? result + 1 : result;
        }
        return 0;
    }

}

