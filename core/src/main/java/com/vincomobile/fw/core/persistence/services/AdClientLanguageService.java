package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdClientLanguage;

/**
 * Created by Vincomobile FW on 07/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_client_language
 */
public interface AdClientLanguageService extends BaseService<AdClientLanguage, String> {

}


