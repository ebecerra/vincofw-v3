package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.MenuRoleParam;
import com.vincomobile.fw.core.persistence.model.AdMenuRoles;
import com.vincomobile.fw.core.persistence.model.AdUser;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_MENU_ROLES
 *
 * Date: 19/02/2015
 */
public interface AdMenuRolesService extends BaseService<AdMenuRoles, String> {

    /**
     * List menu roles by module
     *
     * @param idModule Module identifier
     * @return Menu/Roles for module
     */
    List<AdMenuRoles> listByModule(String idModule);

    /**
     * Get Menu/Role
     *
     * @param idClient Client identifier
     * @param idMenu Menu identifier
     * @param idRole Role identifier
     * @return Menu/Role
     */
    AdMenuRoles getItem(String idClient, String idMenu, String idRole);

    /**
     * Insert/Update role / menu
     *
     * @param user User
     * @param menuRoles Menu list
     * @return Inserted rows
     */
    int saveRoleMenu(AdUser user, MenuRoleParam menuRoles);

    /**
     * Remove role / menu
     *
     * @param menuRoles Menu list
     * @return Removed rows
     */
    int removeRoleMenu(MenuRoleParam menuRoles);

}


