package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.core.persistence.beans.AdPrivilegeValue;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de AD_ROLE
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_role")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler", "privilegeValues"}, ignoreUnknown = true)
public class AdRole extends AdEntityBean {

    private String idRole;
    private String name;
    private String description;
    private String rtype;

    private List<AdPrivilege> privileges;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idRole;
    }

    @Override
    public void setId(String id) {
            this.idRole = id;
    }

    @Id
    @Column(name = "id_role")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    @Column(name = "name", length = 100, unique = true)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getDescription() {
        return description;
    }

    @Column(name = "rtype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getRtype() {
        return rtype;
    }

    public void setRtype(String rtype) {
        this.rtype = rtype;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany
    @JoinTable (
            name = "ad_role_priv",
            joinColumns = {@JoinColumn(name = "id_role", referencedColumnName = "id_role", updatable = false, insertable = false)},
            inverseJoinColumns = {@JoinColumn(name = "id_privilege", referencedColumnName = "id_privilege", updatable = false, insertable = false)}
    )
    @JsonIgnore
    public List<AdPrivilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<AdPrivilege> privileges) {
        this.privileges = privileges;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdRole)) return false;

        final AdRole that = (AdRole) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idRole == null) && (that.idRole == null)) || (idRole != null && idRole.equals(that.idRole)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((rtype == null) && (that.rtype == null)) || (rtype != null && rtype.equals(that.rtype)));
        return result;
    }

    @Transient
    public List<AdPrivilegeValue> getPrivilegeValues() {
        List<AdPrivilegeValue> result = new ArrayList<>();
        for (AdPrivilege privilege : privileges) {
            result.add(new AdPrivilegeValue(privilege));
        }
        return result;
    }
}

