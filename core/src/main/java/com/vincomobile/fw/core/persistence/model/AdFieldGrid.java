package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 02/06/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_field_grid
 */
@Entity
@Table(name = "ad_field_grid")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdFieldGrid extends AdEntityBean {

    private String idFieldGrid;
    private String idUser;
    private String idTab;
    private String idField;
    private Long seqno;

    private AdField field;
    private boolean checked;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idFieldGrid;
    }

    @Override
    public void setId(String id) {
            this.idFieldGrid = id;
    }

    @Id
    @Column(name = "id_field_grid")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdFieldGrid() {
        return idFieldGrid;
    }

    public void setIdFieldGrid(String idFieldGrid) {
        this.idFieldGrid = idFieldGrid;
    }

    @Column(name = "id_user", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "id_tab", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdTab() {
        return idTab;
    }

    public void setIdTab(String idTab) {
        this.idTab = idTab;
    }

    @Column(name = "id_field", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdField() {
        return idField;
    }

    public void setIdField(String idField) {
        this.idField = idField;
    }

    @Column(name = "seqno")
    @NotNull
    
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_field", referencedColumnName = "id_field", insertable = false, updatable = false)
    public AdField getField() {
        return field;
    }

    public void setField(AdField field) {
        this.field = field;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdFieldGrid)) return false;

        final AdFieldGrid that = (AdFieldGrid) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idFieldGrid == null) && (that.idFieldGrid == null)) || (idFieldGrid != null && idFieldGrid.equals(that.idFieldGrid)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((idTab == null) && (that.idTab == null)) || (idTab != null && idTab.equals(that.idTab)));
        result = result && (((idField == null) && (that.idField == null)) || (idField != null && idField.equals(that.idField)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

    @Transient
    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}

