package com.vincomobile.fw.core.persistence.beans;

import com.vincomobile.fw.core.persistence.model.AdChartColumn;
import com.vincomobile.fw.core.persistence.services.AdChartService;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.Datetool;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdChartTools {

    /**
     * Get a chart value
     *
     * @param values Chart value list
     * @param id Value identifier
     * @return Chart value (if not found return new element)
     */
    public static AdChartValue getChartValue(List<AdChartValue> values, String id) {
        for (AdChartValue value : values) {
            if (value.getId().equals(id))
                return value;
        }
        return null;
    }

    /**
     * Find a chart value
     *
     * @param values Chart value list
     * @param id Value identifier
     * @return Chart value (if not found return new element)
     */
    public static AdChartValue findChartValue(List<AdChartValue> values, String id) {
        AdChartValue result = getChartValue(values, id);
        return result == null ? new AdChartValue(id) : result;
    }

    /**
     * Set chart value from database result set
     *
     * @param value Chart value
     * @param item Result set
     * @param size Number of values to set
     * @param valueOffset Offset of first value (in object)
     * @param offset Offset of first value (in item)
     */
    public static void setChartValue(AdChartValue value, Object[] item, int size, int valueOffset, int offset) {
        for (int i = 0; i < size; i++) {
            value.setValue(valueOffset + i + 1, AdBaseTools.getDouble(item, offset + i));
        }
    }

    /**
     * Set chart value from database result set
     *
     * @param value Chart value
     * @param item Result set
     * @param columns Columns list
     */
    public static void setChartValue(AdChartValue value, Object[] item, List<AdChartColumn> columns) {
        for (int i = 0; i < columns.size(); i++) {
            Object currentVal = (i < item.length && item[i] != null) ? item[i] : null;
            AdChartValue val = new AdChartValue(columns.get(i).getId());
            val.setValue(currentVal);
            value.getValueList().add(val);
        }
    }

    /**
     * Get compare value using "chart mode"
     *
     * @param mode Chart mode
     * @param value Main value
     * @param mult 1 or -1
     * @return Comparable value
     */
    public static String compareValue(String mode, String value, int mult) {
        Calendar result = Calendar.getInstance();
        result.setTime(Converter.getDate(value));
        if (AdChartService.MODE_DATE_COMPARE_WEEK.equals(mode)) {
            result.add(Calendar.WEEK_OF_YEAR, mult);
        } else if (AdChartService.MODE_DATE_COMPARE_MONTH.equals(mode)) {
            result.add(Calendar.MONTH, mult);
        } else if (AdChartService.MODE_DATE_COMPARE_QUARTER.equals(mode)) {
            result.add(Calendar.MONTH, mult * 3);
        } else if (AdChartService.MODE_DATE_COMPARE_SEMESTER.equals(mode)) {
            result.add(Calendar.MONTH, mult * 6);
        } else if (AdChartService.MODE_DATE_COMPARE_YEAR.equals(mode)) {
            result.add(Calendar.YEAR, mult);
        }
        return Converter.formatDate(result.getTime(), "dd/MM/yyyy");
    }

    public static Date compareValue(String mode, Date value) {
        return Converter.getDate(compareValue(mode, Converter.formatDate(value, "dd/MM/yyyy"), -1));
    }

    public static Calendar originalValue(String mode, Calendar value) {
        Date result = Converter.getDate(compareValue(mode, Converter.formatDate(value.getTime(), "dd/MM/yyyy"), 1));
        Calendar cal = Calendar.getInstance();
        cal.setTime(result);
        return cal;
    }

    public static String getItemId(String mode, String dateMode, Object[] item, boolean compareDate) {
        if (AdChartService.DATE_MODE_DAY.equals(dateMode)) {
            if (compareDate) {
                Calendar cal = Datetool.getCalendar(AdBaseTools.getInteger(item, 2), AdBaseTools.getInteger(item, 1), AdBaseTools.getInteger(item, 0));
                cal = originalValue(mode, cal);
                return "" + cal.get(Calendar.YEAR) + Converter.leftFill(cal.get(Calendar.MONTH) + 1, '0', 2) + Converter.leftFill(cal.get(Calendar.DAY_OF_MONTH), '0', 2);
            }
            return "" + AdBaseTools.getLong(item, 0) + Converter.leftFill(AdBaseTools.getLong(item, 1), '0', 2) + Converter.leftFill(AdBaseTools.getLong(item, 2), '0', 2);
        } else if (AdChartService.DATE_MODE_WEEK.equals(dateMode)) {
            return "" + (AdBaseTools.getLong(item, 0) + (compareDate ? 1 : 0));
        } else if (AdChartService.DATE_MODE_MONTH.equals(dateMode)) {
            if (compareDate) {
                Calendar cal = Datetool.getCalendar(1, AdBaseTools.getInteger(item, 1), AdBaseTools.getInteger(item, 0));
                cal = originalValue(mode, cal);
                return "" + cal.get(Calendar.YEAR) + Converter.leftFill(cal.get(Calendar.MONTH) + 1, '0', 2);
            }
            return "" + AdBaseTools.getLong(item, 0) + Converter.leftFill(AdBaseTools.getLong(item, 1), '0', 2);
        }
        return "not-id";
    }
}
