package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdOfflineDevice;

/**
 * Created by Devtools.
 * Service layer interface for ad_offline_device
 *
 * Date: 06/03/2016
 */
public interface AdOfflineDeviceService extends BaseService<AdOfflineDevice, String> {

    String STATUS_ACTIVE    = "ACTIVE";
    String STATUS_LOCKED    = "LOCKED";
    String STATUS_RESET     = "RESET";

    /**
     * Update device information
     *
     * @param idClient Client identifier
     * @param idOfflineDevice Device identifier
     * @param version Version
     * @param idUser Login user identifier
     * @param userName User name
     * @param deviceName Device name
     * @param deviceModel Device model
     * @param deviceOS Device operation system
     * @param deviceVersion Device OS version
     * @return Offline device
     */
    AdOfflineDevice update(String idClient, String idOfflineDevice, String version, String idUser, String userName, String deviceName, String deviceModel, String deviceOS, String deviceVersion);

}


