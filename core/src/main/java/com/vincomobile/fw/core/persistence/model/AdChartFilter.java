package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 28/09/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Model for table ad_chart_filter
 */
@Entity
@Table(name = "ad_chart_filter")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdChartFilter extends AdEntityBean {

    private String idChartFilter;
    private String caption;
    private String displaylogic;
    private String description;
    private Boolean displayed;
    private String idReference;
    private String idReferenceValue;
    private String idTab;
    private Boolean mandatory;
    private String name;
    private Boolean ranged;
    private String saveType;
    private Long seqno;
    private String valuedefault;
    private String valuedefault2;
    private String valuemax;
    private String valuemin;

    private AdReference reference;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idChartFilter;
    }

    @Override
    public void setId(String id) {
            this.idChartFilter = id;
    }

    @Id
    @Column(name = "id_chart_filter")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdChartFilter() {
        return idChartFilter;
    }

    public void setIdChartFilter(String idChartFilter) {
        this.idChartFilter = idChartFilter;
    }

    @Column(name = "caption", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Column(name = "displaylogic", length = 250)
    @Size(min = 1, max = 250)
    public String getDisplaylogic() {
        return displaylogic;
    }

    public void setDisplaylogic(String displaylogic) {
        this.displaylogic = displaylogic;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "displayed")
    @NotNull
    public Boolean getDisplayed() {
        return displayed;
    }

    public void setDisplayed(Boolean displayed) {
        this.displayed = displayed;
    }

    @Column(name = "id_reference", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "id_reference_value", length = 32)
    @Size(min = 1, max = 32)
    public String getIdReferenceValue() {
        return idReferenceValue;
    }

    public void setIdReferenceValue(String idReferenceValue) {
        this.idReferenceValue = idReferenceValue;
    }

    @Column(name = "id_tab", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdTab() {
        return idTab;
    }

    public void setIdTab(String idTab) {
        this.idTab = idTab;
    }

    @Column(name = "mandatory")
    @NotNull
    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "ranged")
    @NotNull
    public Boolean getRanged() {
        return ranged;
    }

    public void setRanged(Boolean ranged) {
        this.ranged = ranged;
    }

    @Column(name = "save_type", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getSaveType() {
        return saveType;
    }

    public void setSaveType(String saveType) {
        this.saveType = saveType;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "valuedefault", length = 60)
    @Size(min = 1, max = 60)
    public String getValuedefault() {
        return valuedefault;
    }

    public void setValuedefault(String valuedefault) {
        this.valuedefault = valuedefault;
    }

    @Column(name = "valuedefault_2", length = 60)
    @Size(min = 1, max = 60)
    public String getValuedefault2() {
        return valuedefault2;
    }

    public void setValuedefault2(String valuedefault2) {
        this.valuedefault2 = valuedefault2;
    }

    @Column(name = "valuemax", length = 60)
    @Size(min = 1, max = 60)
    public String getValuemax() {
        return valuemax;
    }

    public void setValuemax(String valuemax) {
        this.valuemax = valuemax;
    }

    @Column(name = "valuemin", length = 60)
    @Size(min = 1, max = 60)
    public String getValuemin() {
        return valuemin;
    }

    public void setValuemin(String valuemin) {
        this.valuemin = valuemin;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_reference", referencedColumnName = "id_reference", insertable = false, updatable = false)
    public AdReference getReference() {
        return reference;
    }

    public void setReference(AdReference reference) {
        this.reference = reference;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdChartFilter)) return false;

        final AdChartFilter that = (AdChartFilter) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idChartFilter == null) && (that.idChartFilter == null)) || (idChartFilter != null && idChartFilter.equals(that.idChartFilter)));
        result = result && (((caption == null) && (that.caption == null)) || (caption != null && caption.equals(that.caption)));
        result = result && (((displaylogic == null) && (that.displaylogic == null)) || (displaylogic != null && displaylogic.equals(that.displaylogic)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((displayed == null) && (that.displayed == null)) || (displayed != null && displayed.equals(that.displayed)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((idReferenceValue == null) && (that.idReferenceValue == null)) || (idReferenceValue != null && idReferenceValue.equals(that.idReferenceValue)));
        result = result && (((idTab == null) && (that.idTab == null)) || (idTab != null && idTab.equals(that.idTab)));
        result = result && (((mandatory == null) && (that.mandatory == null)) || (mandatory != null && mandatory.equals(that.mandatory)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((ranged == null) && (that.ranged == null)) || (ranged != null && ranged.equals(that.ranged)));
        result = result && (((saveType == null) && (that.saveType == null)) || (saveType != null && saveType.equals(that.saveType)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((valuedefault == null) && (that.valuedefault == null)) || (valuedefault != null && valuedefault.equals(that.valuedefault)));
        result = result && (((valuedefault2 == null) && (that.valuedefault2 == null)) || (valuedefault2 != null && valuedefault2.equals(that.valuedefault2)));
        result = result && (((valuemax == null) && (that.valuemax == null)) || (valuemax != null && valuemax.equals(that.valuemax)));
        result = result && (((valuemin == null) && (that.valuemin == null)) || (valuemin != null && valuemin.equals(that.valuemin)));
        return result;
    }

}

