package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdWindow;

/**
 * Created by Devtools.
 * Interface of service of AD_WINDOW
 *
 * Date: 19/02/2015
 */
public interface AdWindowService extends BaseService<AdWindow, String> {

}


