package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdReference;

/**
 * Created by Devtools.
 * Interface of service of AD_REFERENCE
 *
 * Date: 19/02/2015
 */
public interface AdReferenceService extends BaseService<AdReference, String> {

    String RTYPE_BUTTON         = "BUTTON";
    String RTYPE_DATE           = "DATE";
    String RTYPE_EMAIL          = "EMAIL";
    String RTYPE_FILE           = "FILE";
    String RTYPE_FILEURL        = "FILEURL";
    String RTYPE_HTML           = "HTML";
    String RTYPE_ID             = "ID";
    String RTYPE_IMAGE          = "IMAGE";
    String RTYPE_IMAGEVIEW      = "IMAGEVIEW";
    String RTYPE_INTEGER        = "INTEGER";
    String RTYPE_LIST           = "LIST";
    String RTYPE_LISTMULTIPLE   = "LISTMULTIPLE";
    String RTYPE_NUMBER         = "NUMBER";
    String RTYPE_PASSWORD       = "PASSWORD";
    String RTYPE_SEARCH         = "SEARCH";
    String RTYPE_SEQUENCE       = "SEQUENCE";
    String RTYPE_STRING         = "STRING";
    String RTYPE_TABLE          = "TABLE";
    String RTYPE_TABLEDIR       = "TABLEDIR";
    String RTYPE_TEXT           = "TEXT";
    String RTYPE_TIMESTAMP      = "TIMESTAMP";
    String RTYPE_YESNO          = "YESNO";

    String BASE_ID_BUTTON       = "6F4C373EBD0911E4A564BC5FF4671DB4";
    String BASE_ID_DATE         = "51C849B7BD0911E4A564BC5FF4671DB4";
    String BASE_ID_EMAIL        = "9B17B5448B9A4E15BBCE96EE17B0CF85";
    String BASE_ID_FILE         = "82616836BD0911E4A564BC5FF4671DB4";
    String BASE_ID_FILEURL      = "ff8081815c7839ef015c784cf3220002";
    String BASE_ID_HTML         = "8765F463BD0911E4A564BC5FF4671DB4";
    String BASE_ID_ID           = "ff8081815ddf626d015de1ae25320005";
    String BASE_ID_IMAGE        = "7BFD703ABD0911E4A564BC5FF4671DB4";
    String BASE_ID_IMAGEVIEW    = "ff8080815342a017015342ab5572001a";
    String BASE_ID_INTEGER      = "3E8D4539BD0811E4A564BC5FF4671DB4";
    String BASE_ID_LIST         = "036E0E7D12294B0D9EB574261739EA5C";
    String BASE_ID_LISTMULTIPLE = "ff8081815c2ecf40015c2ed3d8fa0000";
    String BASE_ID_NUMBER       = "4F6DD8A5BD0811E4A564BC5FF4671DB4";
    String BASE_ID_PASSWORD     = "8DB8794BBD0911E4A564BC5FF4671DB4";
    String BASE_ID_SEARCH       = "D44DF6BE72EC49A2855C7C08323B0285";
    String BASE_ID_SEQUENCE     = "A07D1ADD1C6C4772B2C3823C8A2FA674";
    String BASE_ID_STRING       = "56F03512BD0811E4A564BC5FF4671DB4";
    String BASE_ID_TABLE        = "A05ACA9B2E1A435CA2829CD738279502";
    String BASE_ID_TABLEDIR     = "A05ACA9B2E1A435CA2829CD738279503";
    String BASE_ID_TEXT         = "5D05AD9DBD0811E4A564BC5FF4671DB4";
    String BASE_ID_TIMESTAMP    = "5C407AC9BD0911E4A564BC5FF4671DB4";
    String BASE_ID_YESNO        = "62F56A31BD0911E4A564BC5FF4671DB4";

}


