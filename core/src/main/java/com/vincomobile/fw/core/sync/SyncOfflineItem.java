package com.vincomobile.fw.core.sync;

import com.vincomobile.fw.core.persistence.model.AdColumnSync;

import java.util.List;

public class SyncOfflineItem {

    List<String> values;

    /**
     * Fill columns with values
     *
     * @param columns Columns list
     */
    public void fillColums(List<AdColumnSync> columns) {
        for (int i = 0; i < values.size(); i++) {
            AdColumnSync columnSync = columns.get(i);
            columnSync.setValue(values.get(i));
        }
    }

    /**
     * Get/Set methods
     */
    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (String value : values) {
            out.append(value).append(", ");
        }
        return out.toString();
    }

}
