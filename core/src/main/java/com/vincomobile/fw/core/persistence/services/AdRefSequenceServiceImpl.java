package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRefSequence;

import com.vincomobile.fw.core.tools.Datetool;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service layer implementation for ad_sequence
 *
 * Date: 17/12/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdRefSequenceServiceImpl extends BaseServiceImpl<AdRefSequence, String> implements AdRefSequenceService {

    /**
     * Constructor.
     */
    public AdRefSequenceServiceImpl() {
        super(AdRefSequence.class);
    }

    /**
     * Get next sequence value and increment current
     *
     * @param idRefSequence Sequence identifier
     * @return Next value
     */
    @Override
    public String getNextValue(String idRefSequence) {
        AdRefSequence sequence = findById(idRefSequence);
        if (sequence != null) {
            if (TYPE_YEAR_AUTO.equals(sequence.getStype())) {
                if (sequence.getSyear() == null || !sequence.getSyear().equals(Datetool.getCurrentYear())) {
                    sequence.setSyear((long) Datetool.getCurrentYear());
                    sequence.setCurrentnext(0L);
                }
            }
            if (TYPE_YEAR_MONTH.equals(sequence.getStype()) || TYPE_YEAR_MONTH_DAY.equals(sequence.getStype())) {
                if (sequence.getSmonth() == null || !sequence.getSmonth().equals(Datetool.getCurrentMonth())) {
                    sequence.setSmonth((long) Datetool.getCurrentMonth());
                    sequence.setCurrentnext(0L);
                }
            }
            if (TYPE_YEAR_MONTH_DAY.equals(sequence.getStype())) {
                if (sequence.getSday() == null || !sequence.getSday().equals(Datetool.getCurrentDay())) {
                    sequence.setSday((long) Datetool.getCurrentDay());
                    sequence.setCurrentnext(0L);
                }
            }
            sequence.setCurrentnext(sequence.getCurrentnext() + sequence.getIncrementno());
            update(sequence);
            flush();
            return sequence.getCurrentValue();
        } else {
            return null;
        }
    }

}
