package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdColumnSync;

import com.vincomobile.fw.core.tools.Converter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service layer implementation for ad_column_sync
 *
 * Date: 05/03/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdColumnSyncServiceImpl extends BaseServiceImpl<AdColumnSync, String> implements AdColumnSyncService {

    /**
     * Constructor.
     */
    public AdColumnSyncServiceImpl() {
        super(AdColumnSync.class);
    }

    /**
     * Find a column in table by name
     *
     * @param idTableSync Table id.
     * @param name Column name
     * @return AdColumnSync
     */
    @Override
    public AdColumnSync findByName(String idTableSync, String name) {
        Map filter = new HashMap();
        filter.put("idTableSync", idTableSync);
        filter.put("name", name);
        return findFirst(filter);
    }

    /**
     * Get all columns for a table
     *
     * @param idTableSync Table id.
     * @return Columns
     */
    @Override
    public List<AdColumnSync> getTableColumns(String idTableSync) {
        Map filter = new HashMap();
        filter.put("idTableSync", idTableSync);
        return findAll(getSort("seqno", "asc"), filter);
    }

    /**
     * Get SQL SELECT to get a record
     *
     * @param columns Column list
     * @param tablename Table name
     * @return SELECT
     */
    @Override
    public String getFindSelect(List<AdColumnSync> columns, String tablename) {
        StringBuilder selectKeys = new StringBuilder();
        StringBuilder selectFields = new StringBuilder();

        int pk = 0, normal = 0;
        for (AdColumnSync column : columns) {
            if (AdTableSyncService.SOURCE_DATABASE.equals(column.getCsource())) {
                selectFields.append(normal == 0 ? "" : ", ").append(column.getName());
                normal++;
            }
            if (column.getPrimaryKey()) {
                selectKeys.append(pk == 0 ? "" : " AND ").append(column.getName()).append(" = ").append(column.getSQLValue());
                pk++;
            }
        }
        if (selectFields.length() > 0 && selectKeys.length() > 0)
            return  "SELECT " + selectFields + " FROM " + tablename + " WHERE " + selectKeys.toString();
        else
            return null;
    }

    /**
     * Get INSERT statement
     *
     * @param columns Column list
     * @param tablename Table name
     * @param idClient Client identifier
     * @return INSERT
     */
    @Override
    public String getInsertSt(List<AdColumnSync> columns, String tablename, String idClient) {
        StringBuilder insertFields = new StringBuilder("id_client");
        StringBuilder insertValues = new StringBuilder(Converter.getSQLString(idClient, true));
        boolean hasCreated = false, hasUpdated = false, hasActive = false;
        for (AdColumnSync column : columns) {
            if (AdTableSyncService.SOURCE_DATABASE.equals(column.getCsource()) && !AdColumnService.TYPE_AUTOINCREMENT.equals(column.getCtype())) {
                insertFields.append(", ").append(column.getName());
                insertValues.append(", ").append(column.getSQLValue());
            }
            if (!hasCreated && "created".equals(column.getName())) {
                hasCreated = true;
            }
            if (!hasUpdated && "updated".equals(column.getName())) {
                hasUpdated = true;
            }
            if (!hasActive && "active".equals(column.getName())) {
                hasActive = true;
            }
        }
        if (!hasCreated) {
            insertFields.append(", created");
            insertValues.append(", ").append(Converter.getSQLDate(new Date(), true));
        }
        if (!hasUpdated) {
            insertFields.append(", updated");
            insertValues.append(", ").append(Converter.getSQLDate(new Date(), true));
        }
        if (!hasActive) {
            insertFields.append(", active");
            insertValues.append(", 1");
        }
        return "INSERT INTO " + tablename + " (" + insertFields.toString() + ") VALUES (" + insertValues.toString() + ")";
    }

    /**
     * Get UPDATE statement
     *
     * @param columns Column list
     * @param tablename Table name
     * @return UPDATE
     */
    @Override
    public String getUpdateSt(List<AdColumnSync> columns, String tablename) {
        StringBuilder updateKeys = new StringBuilder();
        StringBuilder updateValues = new StringBuilder();

        int pk = 0, normal = 0;
        boolean hasUpdated = false, hasActive = false;
        for (AdColumnSync column : columns) {
            if (column.getPrimaryKey()) {
                updateKeys.append(pk == 0 ? "" : " AND ").append(column.getName()).append(" = ").append(column.getSQLValue());
                pk++;
            } else {
                if (AdTableSyncService.SOURCE_DATABASE.equals(column.getCsource())) {
                    updateValues.append(normal == 0 ? "" : ", ").append(column.getName()).append(" = ").append(column.getSQLValue());
                    normal++;
                }
            }
            if (!hasUpdated && "updated".equals(column.getName())) {
                hasUpdated = true;
            }
            if (!hasActive && "active".equals(column.getName())) {
                hasActive = true;
            }
        }
        if (updateKeys.length() > 0 && updateValues.length() > 0) {
            String update = "UPDATE " + tablename + " SET " + updateValues.toString();
            if (!hasUpdated)
                update += ", updated = " + Converter.getSQLDate(new Date(), true);
            if (!hasActive)
                update += ", active = 1";
            return update + " WHERE " + updateKeys.toString();
        } else {
            return null;
        }
    }

}
