package com.vincomobile.fw.core.tools;

import com.vincomobile.fw.core.persistence.model.AdPreference;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageResizeManager {

    private Logger logger = LoggerFactory.getLogger(ImageResizeManager.class);

    List<ImageResizeItem> resizeItems = new ArrayList<ImageResizeItem>();
    boolean resize = false;
    boolean checkProportion = false;
    Double proportion = 1.0;

    /**
     * Load image resize configuration
     *
     * @param preferenceService Preference service
     * @param idClient Client id
     * @param tableName Table name (Ex: CatProduct)
     */
    public void loadConfig(AdPreferenceService preferenceService, String idClient, String tableName) {
        AdPreference imageResize = preferenceService.getPreference(tableName + "_ImageResize", idClient);
        AdPreference imageResizeCount = preferenceService.getPreference(tableName + "_ImageResize_Count", idClient);
        if (imageResize != null && imageResizeCount != null) {
            resize = imageResize.getBoolean();
            if (resize) {
                for (int i = 1; i <= imageResizeCount.getLong(); i++) {
                    AdPreference imageResizeWidth = preferenceService.getPreference(tableName + "_ImageResize_" + i + "_Width", idClient);
                    AdPreference imageResizePrefix = preferenceService.getPreference(tableName + "_ImageResize_" + i + "_Prefix", idClient);
                    if (imageResizeWidth != null && imageResizePrefix != null) {
                        if (imageResizeWidth.getInteger() > 0 && !Converter.isEmpty(imageResizePrefix.getString()))
                            resizeItems.add(new ImageResizeItem(imageResizeWidth.getInteger(), imageResizePrefix.getString()));
                    }
                }
                resize = resizeItems.size() > 0;
            }
        }
        AdPreference imageSizeProportion = preferenceService.getPreference(tableName + "_ImageResize_Proportion", idClient);
        if (imageSizeProportion != null) {
            if (!Converter.isEmpty(imageSizeProportion.getString())) {
                String[] values = imageSizeProportion.getString().split(":");
                if (values.length == 2) {
                    int w = Converter.getInt(values[0]);
                    int h = Converter.getInt(values[1]);
                    if (w * h != 0) {
                        proportion = (double) w / h;
                        checkProportion = true;
                    }
                }
            }

        }
    }

    /**
     * Resize image
     *
     * @param photoName Image namen (Full path)
     * @throws Exception
     */
    public void resize(String photoName) throws Exception {
        if (resize) {
            File photo = new File(photoName);
            if (photo.exists()) {
                String ext = photoName.substring(photoName.lastIndexOf(".")+1);
                if (Converter.isEmpty(ext))
                    ext = "jpg";
                String destPath = photo.getParent();
                BufferedImage originalImage = ImageIO.read(photo);
                int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
                int sourceWidth = originalImage.getWidth();
                int sourceHeight = originalImage.getHeight();
                double aspectRatio = (double) sourceHeight / (double) sourceWidth;
                for (ImageResizeItem resizeItem: resizeItems) {
                    logger.debug("Resizing image, Width: "+resizeItem.getWidth()+", prefix: "+resizeItem.getPrefix());
                    BufferedImage resizeImageJpg = resizeImage(originalImage, type, resizeItem.getWidth(), (int) (resizeItem.getWidth()*aspectRatio));
                    ImageIO.write(resizeImageJpg, ext.toLowerCase(), new File(destPath + File.separator + resizeItem.getPrefix() + photo.getName()));
                }
            }
        }
    }

    /**
     * Delete a image and other size too
     *
     * @param photoName Image name
     */
    public void delete(String photoName) {
        File photo = new File(photoName);
        String destPath = photo.getParent();
        for (ImageResizeItem resizeItem: resizeItems) {
            File item = new File(destPath + File.separator + resizeItem.getPrefix() + photo.getName());
            if (item.exists()) {
                logger.debug("Delete image: "+item.getName());
                item.delete();
            }
        }
        if (photo.exists()) {
            logger.debug("Delete image: "+photo.getName());
            photo.delete();
        }
    }

    /**
     * Resize image
     *
     * @param originalImage Original image
     * @param type Image type
     * @param width New width
     * @param height New height
     * @return Resized image
     */
    private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height){
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();

        return resizedImage;
    }

    /**
     * Load image information
     *
     * @param photo Image file
     * @return Image information
     */
    public ImageInfo loadInfo(File photo) {
        ImageInfo result = new ImageInfo();
        try {
            BufferedImage orgImg = ImageIO.read(photo);
            result.setWidth(new Long(orgImg.getWidth()));
            result.setHeight(new Long(orgImg.getHeight()));
        } catch (Exception e) {
            logger.error(e.getMessage());
            result.setError(true);
            result.setMsg(e.getMessage());
        }
        // Check proportions
        if (checkProportion) {
            result.checkProportion(proportion);
            if (!result.isProportionOk())
                FileTool.deleteFile(photo);
        }
        return result;
    }

    public boolean isResize() {
        return resize;
    }

    public boolean isCheckProportion() {
        return checkProportion;
    }

    public List<ImageResizeItem> getResizeItems() {
        return resizeItems;
    }
}
