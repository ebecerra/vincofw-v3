package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdSampledata;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_sampledata
 */
@Repository
@Transactional(readOnly = true)
public class AdSampledataServiceImpl extends BaseServiceImpl<AdSampledata, String> implements AdSampledataService {

    /**
     * Constructor.
     */
    public AdSampledataServiceImpl() {
        super(AdSampledata.class);
    }

}
