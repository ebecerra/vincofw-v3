package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.AdProcessParam;
import com.vincomobile.fw.core.persistence.services.AdReferenceService;
import com.vincomobile.fw.core.tools.plugins.CodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;

@Component
public class AdTableBase extends ProcessDefinitionImpl {

    public static final String ERROR = "Error";

    @Autowired
    protected AdReferenceService adReferenceService;

    @Autowired
    protected ServletContext servletContext;

    @Autowired
    CodeGenerator codeGenerator;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Read idTable parameter
     *
     * @param idProcessExec Process Execution Identifier
     * @param paramName Parameter name
     * @return AdProcessParam
     */
    protected AdProcessParam getTableParam(String idProcessExec, String paramName) {
        AdProcessParam tableParam = null;
        for (AdProcessParam current : this.params) {
            if (paramName.equals(current.getName())) {
                tableParam = current;
                break;
            }
        }
        if (tableParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", paramName));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
        return tableParam;
    }


}
