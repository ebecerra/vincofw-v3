package com.vincomobile.fw.core.sync;

import com.vincomobile.fw.core.persistence.model.AdColumnSync;
import com.vincomobile.fw.core.persistence.services.AdRefSequenceService;

import javax.persistence.EntityManager;
import java.util.List;

public interface ActionTrigger {

    /**
     * Inject DB manager
     *
     * @param entityManager Manager
     */
    void setEntityManager(EntityManager entityManager);

    /**
     * Inject Sequence service  manager
     *
     * @param sequenceService Manager
     */
    void setSequenceService(AdRefSequenceService sequenceService);

    /**
     * Triggers called for UPDATE
     * 
     * @param columns Table columns (value = new, old_value = value stored in DB)
     * @return If action can be done
     */
    boolean beforeUpdate(List<AdColumnSync> columns);
    void afterUpdate(List<AdColumnSync> columns);
    
    /**
     * Triggers called for INSERT
     *
     * @param columns Table columns (value = new, old_value = value stored in DB)
     * @return If action can be done
     */
    boolean beforeInsert(List<AdColumnSync> columns);
    void afterInsert(List<AdColumnSync> columns);
    
    /**
     * Triggers called for DELETE
     *
     * @param columns Table columns (value = new, old_value = value stored in DB)
     * @return If action can be done
     */
    boolean beforeDelete(List<AdColumnSync> columns);
    void afterDelete(List<AdColumnSync> columns);

}
