package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_offline_device
 *
 * Date: 06/03/2016
 */
@Entity
@Table(name = "ad_offline_device")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdOfflineDevice extends AdEntityBean {

    private String idOfflineDevice;
    private String idUser;
    private String userName;
    private String deviceName;
    private String deviceModel;
    private String deviceOs;
    private String deviceVersion;
    private String dstatus;
    private String version;


    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idOfflineDevice;
    }

    @Override
    public void setId(String id) {
            this.idOfflineDevice = id;
    }

    @Id
    @Column(name = "id_offline_device")
    public String getIdOfflineDevice() {
        return idOfflineDevice;
    }

    public void setIdOfflineDevice(String idOfflineDevice) {
        this.idOfflineDevice = idOfflineDevice;
    }

    @Column(name = "id_user")
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "user_name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "device_name", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Column(name = "device_model", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    @Column(name = "device_os", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    @Column(name = "device_version", length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    @Column(name = "dstatus", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getDstatus() {
        return dstatus;
    }

    public void setDstatus(String dstatus) {
        this.dstatus = dstatus;
    }

    @Column(name = "version", length = 10)
    @NotNull
    @Size(min = 1, max = 10)
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdOfflineDevice)) return false;

        final AdOfflineDevice that = (AdOfflineDevice) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idOfflineDevice == null) && (that.idOfflineDevice == null)) || (idOfflineDevice != null && idOfflineDevice.equals(that.idOfflineDevice)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((userName == null) && (that.userName == null)) || (userName != null && userName.equals(that.userName)));
        result = result && (((deviceName == null) && (that.deviceName == null)) || (deviceName != null && deviceName.equals(that.deviceName)));
        result = result && (((deviceModel == null) && (that.deviceModel == null)) || (deviceModel != null && deviceModel.equals(that.deviceModel)));
        result = result && (((deviceOs == null) && (that.deviceOs == null)) || (deviceOs != null && deviceOs.equals(that.deviceOs)));
        result = result && (((deviceVersion == null) && (that.deviceVersion == null)) || (deviceVersion != null && deviceVersion.equals(that.deviceVersion)));
        result = result && (((dstatus == null) && (that.dstatus == null)) || (dstatus != null && dstatus.equals(that.dstatus)));
        result = result && (((version == null) && (that.version == null)) || (version != null && version.equals(that.version)));
        return result;
    }

}

