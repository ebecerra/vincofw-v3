package com.vincomobile.fw.core;

import org.springframework.beans.factory.annotation.Value;

public interface IRuntimeEnvironmentConfig {

    String getEnvironment();

    @Value("${runtime.environment}")
    void setEnvironment(String enviroment);

    String getExcludeProcess();

    @Value("${runtime.exclude.process}")
    void setExcludeProcess(String excludeProcess);

    Boolean getExcludeQuartz();

    @Value("${runtime.exclude.quartz}")
    void setExcludeQuartz(Boolean excludeQuartz);

}
