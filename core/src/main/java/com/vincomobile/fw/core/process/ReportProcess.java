package com.vincomobile.fw.core.process;

public interface ReportProcess extends ProcessDefinition {

    /**
     * Generates report files
     *
     * @param idProcessExec Process Execution Identifier
     */
    void executeReport(String idProcessExec);
}
