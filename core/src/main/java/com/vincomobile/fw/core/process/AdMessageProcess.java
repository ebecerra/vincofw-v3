package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.business.DatabaseUtils;
import com.vincomobile.fw.core.persistence.beans.AdMessageTranslation;
import com.vincomobile.fw.core.persistence.model.AdLanguage;
import com.vincomobile.fw.core.persistence.model.AdMessage;
import com.vincomobile.fw.core.persistence.model.AdModule;
import com.vincomobile.fw.core.persistence.services.AdLanguageService;
import com.vincomobile.fw.core.persistence.services.AdMessageService;
import com.vincomobile.fw.core.persistence.services.AdModuleService;
import com.vincomobile.fw.core.persistence.services.AdTranslationService;
import com.vincomobile.fw.core.tools.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;

@Component
@Scope(value = "prototype")
@Transactional(readOnly = true)
public class AdMessageProcess extends ProcessDefinitionImpl {

    private static final String SHEET_NAME = "MsgTranslation";
    private static final String[] COLUMNS = new String[] { "Client", "Module", "Code", "Translation" };

    @Autowired
    AdTranslationService translationService;

    @Autowired
    AdMessageService messageService;

    @Autowired
    AdLanguageService languageService;

    @Autowired
    AdModuleService moduleService;

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdMessageProcess service = new AdMessageProcess();

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Export message translations
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void exportTranslations(String idProcessExec) {
        info(idProcessExec, getMessage("AD_ProcessDBExportMsg"));
        String idClientData = getParamString(idProcessExec, "idClientData");
        String idModule = getOptionalParamString("idModule");
        String idLanguage = getParamString(idProcessExec, "idLanguage");
        String msgSelector = getParamString(idProcessExec, "msgSelector");
        if (idClientData == null || idModule == null || idLanguage == null || msgSelector == null) {
            return;
        }

        AdLanguage language = getLanguage(idProcessExec, idLanguage);
        AdModule module = getModule(idProcessExec, idModule);
        if (language == null || module == null) {
            return;
        }

        File dir = DatabaseUtils.getWorkDirectory(idClient, "/export/translations/" + idClientData, true);
        List<AdMessageTranslation> messages = translationService.listMessages(
                idClientData, idLanguage, idModule,
                "MSG_ALL".equals(msgSelector) ? null : "MSG_ADMIN".equals(msgSelector),
                "MSG_ALL".equals(msgSelector) ? null : "MSG_WEB".equals(msgSelector)
        );
        info(idProcessExec, getMessage("AD_ProcessDBExportMsgRows", messages.size()));
        ExcelUtil excel = new ExcelUtil();
        excel.initBook(SHEET_NAME, ExcelUtil.FORMAT_XLSX);
        excel.createRowCaptions(0, COLUMNS);
        int rowIndex = 1;
        for (AdMessageTranslation msg : messages) {
            Row row = excel.createRow(excel.getSheet(), rowIndex++, 0, msg.getIdClient(), excel.getCsNormal());
            excel.createCell(row, 1, msg.getIdModule(), excel.getCsNormal());
            excel.createCell(row, 2, msg.getValue(), excel.getCsNormal());
            excel.createCell(row, 3, msg.getText(), excel.getCsNormal());
        }
        excel.setWidths(new int[] { 40, 40, 50, 200 });
        try {
            excel.closeExcel(new File(dir.getAbsolutePath() + "/" + module.getRestPath() + "_" + language.getIso2() + ".xlsx"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
        } catch (Exception e) {
            error(idProcessExec, e.getMessage());
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
    }

    /**
     * Import message translations
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void importTranslations(String idProcessExec) {
        info(idProcessExec, getMessage("AD_ProcessDBImportMsg"));
        String idClientData = getParamString(idProcessExec, "idClientData");
        String idModule = getOptionalParamString("idModule");
        String idLanguage = getParamString(idProcessExec, "idLanguage");
        if (idClientData == null || idModule == null || idLanguage == null) {
            return;
        }

        AdLanguage language = getLanguage(idProcessExec, idLanguage);
        AdModule module = getModule(idProcessExec, idModule);
        if (language == null || module == null) {
            return;
        }

        File dir = DatabaseUtils.getWorkDirectory(idClient, "/import/translations/" + idClientData, false);
        if (dir.exists()) {
            File xls = new File(dir.getAbsolutePath() + "/" + module.getRestPath() + "_" + language.getIso2() + ".xlsx");
            if (xls.exists()) {
                ExcelUtil excel = new ExcelUtil();
                try {
                    excel.loadFile(xls);
                    excel.setSheet(SHEET_NAME);
                    String error = excel.checkRowTitles(0, COLUMNS);
                    if (error == null) {
                        int add = 0, update = 0;
                        for (int i = 1; i <= excel.getSheet().getLastRowNum(); i++) {
                            Row row = excel.getSheet().getRow(i);
                            if (!ExcelUtil.isEmptyRow(row, 3)) {
                                String clientId = ExcelUtil.getStringValue(row.getCell(0));
                                String moduleId = ExcelUtil.getStringValue(row.getCell(1));
                                String value = ExcelUtil.getStringValue(row.getCell(2));
                                AdMessage message = messageService.findMessage(clientId, value);
                                String translation = ExcelUtil.getStringValue(row.getCell(3));
                                if (message != null) {
                                    boolean added = translationService.addTranslation(
                                            clientId, user.getIdUser(), moduleId, idLanguage,
                                            FWConfig.TABLE_ID_MESSAGE, FWConfig.COLUMN_ID_MESSAGE_MSGTEXT, message.getIdMessage(), translation
                                    );
                                    if (added) {
                                        add++;
                                    } else {
                                        update++;
                                    }
                                } else {
                                    warn(idProcessExec, getMessage("AD_ProcessDBImportMsgNotFound", value));
                                }
                            } else {
                                break;
                            }
                        }
                        info(idProcessExec, getMessage("AD_ProcessDBImportTranslations", add, update));
                        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
                        return;
                    } else {
                        error(idProcessExec, getMessage("AD_ProcessInvalidFileFormat", xls.getAbsolutePath()));
                    }
                } catch (Exception e) {
                    error(idProcessExec, e.getMessage());
                }
            } else {
                error(idProcessExec, getMessage("AD_ProcessDBImportMsgNotFindFile", xls.getAbsolutePath()));
            }
        } else {
            error(idProcessExec, getMessage("AD_ProcessDBImportMsgNotExistDir", dir.getAbsolutePath()));
        }
        finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
    }

    /**
     * Get language
     *
     * @param idProcessExec Process Execution Identifier
     * @param idLanguage Language identifier
     * @return Language
     */
    private AdLanguage getLanguage(String idProcessExec, String idLanguage) {
        AdLanguage language = languageService.getLanguage(idLanguage);
        if (language == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idLanguage"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
        return language;
    }

    /**
     * Get module
     *
     * @param idProcessExec Process Execution Identifier
     * @param idModule Module identifier
     * @return Module
     */
    private AdModule getModule(String idProcessExec, String idModule) {
        AdModule module = moduleService.findById(idModule);
        if (module == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idModule"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
        return module;
    }
}