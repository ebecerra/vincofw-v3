package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcessOutput;

/**
 * Created by Devtools.
 * Service layer interface for ad_process_output
 *
 * Date: 29/11/2015
 */
public interface AdProcessOutputService extends BaseService<AdProcessOutput, String> {

}


