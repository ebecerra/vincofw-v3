package com.vincomobile.fw.core.process.importing;

import java.util.ArrayList;
import java.util.List;

public class FileErrors {

    String fileName;
    List<BaseError> errors;
    boolean success;
    boolean execute;

    public FileErrors(String fileName) {
        this.fileName = fileName;
        this.errors = new ArrayList<>();
        this.execute = false;
        this.success = true;
    }

    public void addError(BaseError err) {
        errors.add(err);
        success = false;
    }

    public String getFileName() {
        return fileName;
    }

    public List<BaseError> getErrors() {
        return errors;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isExecute() {
        return execute;
    }

    public void setExecute(boolean execute) {
        this.execute = execute;
    }
}
