package com.vincomobile.fw.core.persistence.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Allows to performs paginated requests
 */
public class PageSearch extends PageRequest {
    public static final String AND_SEPARATOR = ",";
    public static final String OR_SEPARATOR = ";";
    public static final String EXTENDED_SQLCOND = "$extended$";

    public static final int DEFAULT_PAGE_SIZE = 1000;
    public static final String EQUAL_COND = "=";
    public static final String NOT_EQUAL_COND = "!=";
    public static final String NOT_NULL_COND = "!null$";
    public static final String NULL_COND = "null$";
    public static final String LESS_COND = "less$";
    public static final String LESS_EQUAL_COND = "lessEqual$";
    public static final String MORE_COND = "more$";
    public static final String MORE_EQUAL_COND = "moreEqual$";
    public static final String DIFF_COND = "diff$";

    public static final String[] SQL_COND = new String[] {
            LESS_COND, LESS_EQUAL_COND, MORE_COND, MORE_EQUAL_COND, DIFF_COND
    };
    public static final String[] SQL_SYMB_COND = new String[] {
            " < ", " <= ", " > ", " >= ", " <> "
    };

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected Map<String, Object> searchs = new HashMap<String, Object>();

    public PageSearch() {
        this(1, DEFAULT_PAGE_SIZE);
    }

    public PageSearch(int page, int size) {
        super(page - 1, size);
    }

    public PageSearch(int page, int size, Sort.Direction direction, String... properties) {
        super(page - 1, size, direction, properties);
    }

    public PageSearch(int page, int size, Sort sort) {
        super(size > 0 ? page - 1 : 0, size > 0 ? size : DEFAULT_PAGE_SIZE, sort);
    }

    public PageSearch(Sort sort) {
        super(0, DEFAULT_PAGE_SIZE, sort);
    }

    public PageSearch(int page, int size, Map<String, Object> searchs) {
        this(size > 0 ? page - 1 : 0, size > 0 ? size : DEFAULT_PAGE_SIZE, null, searchs);
    }

    public PageSearch(Sort sort, Map<String, Object> searchs) {
        super(0, DEFAULT_PAGE_SIZE, sort);

        this.searchs = searchs;
    }

    public PageSearch(Map<String, Object> searchs) {
        super(0, DEFAULT_PAGE_SIZE);

        this.searchs = searchs;
    }

    public PageSearch(int page, int size, Sort sort, Map<String, Object> searchs) {
        super(size > 0 ? page - 1 : 0, size > 0 ? size : DEFAULT_PAGE_SIZE, sort);

        this.searchs = searchs;
    }

    public PageSearch(int page, int size, Sort sort, String constraints, Class clazz) {
        super(size > 0 ? page - 1 : 0, size > 0 ? size : DEFAULT_PAGE_SIZE, sort);

        if (constraints != null) {
            parseConstraints(constraints, clazz);
        }
    }

    public void replaceKey(String oldKey, String newKey) {
        if (searchs.containsKey(oldKey)) {
            Object obj = searchs.remove(oldKey);
            searchs.put(newKey, obj);
        }
    }

    public void parseConstraints(String constraints, Class clazz) {
        if (searchs == null) {
            searchs = new HashMap<>();
        }

        if (constraints == null) {
            return;
        }

        if (constraints.contains(EXTENDED_SQLCOND)) {
            // $extended$={ ... }
            int indxExtended = constraints.indexOf(EXTENDED_SQLCOND);
            String part1 = constraints.substring(0, indxExtended);
            String part2 = constraints.substring(indxExtended);
            int indxOpen = part2.indexOf("{");
            int indxClose = part2.indexOf("}");
            if (indxOpen >= 0 && indxClose > 0) {
                searchs.put(EXTENDED_SQLCOND, part2.substring(indxOpen + 1, indxClose));
            }
            constraints = part1 + part2.substring(indxClose + 1);
        }

        String[] terms = constraints.split(AND_SEPARATOR);
        for (String term : terms) {
            term = term.trim();
            boolean negative = term.indexOf(NOT_EQUAL_COND) >= 0;
            String[] pairs = negative ? term.split(NOT_EQUAL_COND) : term.split(EQUAL_COND);
            String property = (negative ? NOT_EQUAL_COND : "") + pairs[0];
            String value = null;
            if (pairs.length > 1) {
                value = pairs[1];
                for (int i = 2; i < pairs.length; i++) {
                    value += "=" + pairs[i];
                }
            }
            property = property.trim();
            if (property != null && property.trim().length() > 0 && value != null && value.trim().length() > 0) {
                value = value.trim();
                if (value.startsWith("[") && value.endsWith("]")) {
                    value = value.substring(1, value.length() - 1);
                    String[] valueStrs = value.contains(";") ? value.split(";") : new String[]{value};
                    List<String> values = Arrays.asList(valueStrs);

                    searchs.put(property, values);
                } else {
                    searchs.put(property, value);
                }
            }
        }
    }

    public Map<String, Object> getSearchs() {
        return searchs;
    }

    public void setSearchs(Map<String, Object> searchs) {
        this.searchs = searchs;
    }
}
