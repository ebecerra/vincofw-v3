package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRolePriv;

/**
 * Created by Devtools.
 * Interface of service of AD_ROLE_PRIV
 *
 * Date: 19/02/2015
 */
public interface AdRolePrivService extends BaseService<AdRolePriv, String> {

    /**
     * Delete all privilege of role
     *
     * @param idRole Role identifier
     * @return Delete row count
     */
    int deletePrivilege(String idRole);
}


