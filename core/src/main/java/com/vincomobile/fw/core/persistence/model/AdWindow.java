package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_WINDOW
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_window")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdWindow extends AdEntityBean {

    private String idWindow;
    private String name;
    private String wtype;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idWindow;
    }

    @Override
    public void setId(String id) {
        this.idWindow = id;
    }

    @Id
    @Column(name = "id_window")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdWindow() {
        // System.out.println("getIdWindow: AdWindow.idWindow = "+idWindow);
        return idWindow;
    }

    public void setIdWindow(String idWindow) {
        this.idWindow = idWindow;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "wtype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getWtype() {
        return wtype;
    }

    public void setWtype(String wtype) {
        this.wtype = wtype;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdWindow)) return false;

        final AdWindow that = (AdWindow) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idWindow == null) && (that.idWindow == null)) || (idWindow != null && idWindow.equals(that.idWindow)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((wtype == null) && (that.wtype == null)) || (wtype != null && wtype.equals(that.wtype)));
        return result;
    }


    @Transient
    public String getCaption() {
        return "[" + module.getRestPath() + "] " + name;
    }
}

