package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRefSearchColumn;

/**
 * Created by Devtools.
 * Interface del servicio de ad_ref_search_column
 *
 * Date: 07/11/2015
 */
public interface AdRefSearchColumnService extends BaseService<AdRefSearchColumn, String> {

}


