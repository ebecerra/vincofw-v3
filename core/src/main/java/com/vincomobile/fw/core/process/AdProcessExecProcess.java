package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.*;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.Datetool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AdProcessExecProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdProcessExecProcess service = new AdProcessExecProcess();

    @Autowired
    AdProcessExecService processExecService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return true;
    }

    /**
     * Clear tables for process execution and execution logs
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void clearProcessExecution(String idProcessExec) {
        AdProcessParam daysParam = getParam("days");
        if (daysParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "days"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        int rows = processExecService.clearProcessExec(Converter.getInt((String) daysParam.getValue()));
        info(idProcessExec, getMessage("AD_ProcessProcessExecClear", rows));
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

}