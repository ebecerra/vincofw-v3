package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcessParam;

import java.util.List;

/**
 * Created by Devtools.
 * Interface del servicio de ad_process_param
 *
 * Date: 06/11/2015
 */
public interface AdProcessParamService extends BaseService<AdProcessParam, String> {

    String TYPE_IN      = "IN";
    String TYPE_OUT     = "OUT";

    /**
     * Load input params for a process
     *
     * @param idProcess Process identifier
     * @return Input params
     */
    List<AdProcessParam> getInputParams(String idProcess);
}


