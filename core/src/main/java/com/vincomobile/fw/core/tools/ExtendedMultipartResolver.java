package com.vincomobile.fw.core.tools;

import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;

public class ExtendedMultipartResolver  extends CommonsMultipartResolver {
    /**
     * Checks if multipart is supported for the request.
     * Only supports multipart for POST and PUT methods
     *
     * @param request Request being analyzed
     * @return True if multipart is supported
     */
    @Override
    public boolean isMultipart(HttpServletRequest request) {
        if(request!=null) {
            String httpMethod = request.getMethod().toLowerCase();
            boolean validMethod = "put".equals(httpMethod) || "post".equals(httpMethod);
            String contentType = request.getContentType();
            return (contentType != null && contentType.toLowerCase().startsWith("multipart") && validMethod);
        }else {
            return false;
        }
    }
}