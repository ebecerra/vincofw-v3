package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.AdProcessParam;
import com.vincomobile.fw.core.persistence.services.AdImportDeleteService;
import com.vincomobile.fw.core.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Random;

import static java.lang.Math.abs;

public class TestingBaseProcess extends ProcessDefinitionImpl {

    static Random random = new Random();

    @Autowired
    AdImportDeleteService importDeleteService;

    /**
     * Test process
     *
     * @param idProcessExec Process Execution Identifier
     * @param writeDB       Indicate if write to DB
     */
    protected void testProcess(String idProcessExec, boolean writeDB) {
        AdProcessParam stepsParam = getParam("steps");
        if (stepsParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "steps"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        int steps = Converter.getInt((String) stepsParam.getValue(), 1);
        doProcess(idProcessExec, steps, writeDB);
    }

    protected void doProcess(String idProcessExec, int steps, boolean writeDB) {
        try {
            info(idProcessExec, "Testing process started");
            Long processId = abs(random.nextLong());
            if (writeDB) {
                importDeleteService.deleteProcessEntries(1L);
            }
            for (int i = 1; i <= steps; i++) {
                info(idProcessExec, "[" + processId + "] + Wait 5 seconds ... (" + i + ")");
                if (writeDB) {
                    importDeleteService.addRow(1L, "testProcess", "Step_" + i);
                }
                Thread.sleep(5 * 1000);
            }
            finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
    }
}