package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRefButton;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Devtools.
 * Servicio de ad_ref_button
 *
 * Date: 27/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdRefButtonServiceImpl extends BaseServiceImpl<AdRefButton, String> implements AdRefButtonService {

    /**
     * Constructor.
     *
     */
    public AdRefButtonServiceImpl() {
        super(AdRefButton.class);
    }

    /**
     * Get reference button by reference
     *
     * @param idReference Reference identifier
     * @return Reference Table
     */
    @Override
    public AdRefButton getRefButtonByReference(String idReference) {
        Map filter = new HashMap();
        filter.put("idReference", idReference);
        return findFirst(filter);
    }

}
