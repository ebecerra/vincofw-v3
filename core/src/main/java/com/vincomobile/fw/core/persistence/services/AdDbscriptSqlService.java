package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdDbscriptSql;

/**
 * Created by Vincomobile FW on 03/10/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_dbscript_sql
 */
public interface AdDbscriptSqlService extends BaseService<AdDbscriptSql, String> {

}


