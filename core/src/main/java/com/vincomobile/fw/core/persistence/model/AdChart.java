package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Vincomobile FW on 28/09/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Model for table ad_chart
 */
@Entity
@Table(name = "ad_chart")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdChart extends AdEntityBean {

    private String idChart;
    private String ctype;
    private String displaylogic;
    private String idTab;
    private String keyField;
    private String keyReference;
    private String nameField;
    private String fullnameField;
    private String mode;
    private String name;
    private String prefix;
    private Long rowLimit;
    private Long seqno;
    private String titleX;
    private String titleY;
    private Boolean showTitleX;
    private Boolean showTitleY;
    private Long span;
    private Boolean startnewrow;
    private String value1;
    private String value2;
    private String value3;
    private String value4;
    private String value5;
    private String value6;
    private String value7;
    private String value8;
    private String value9;
    private String value10;
    private String legend1;
    private String legend2;
    private String legend3;
    private String legend4;
    private String legend5;
    private String legend6;
    private String legend7;
    private String legend8;
    private String legend9;
    private String legend10;
    private String color1;
    private String color2;
    private String color3;
    private String color4;
    private String color5;
    private String color6;
    private String color7;
    private String color8;
    private String color9;
    private String color10;
    private String compColor1;
    private String compColor2;
    private String compColor3;
    private String compColor4;
    private String compColor5;
    private String compColor6;
    private String compColor7;
    private String compColor8;
    private String compColor9;
    private String compColor10;
    private String xfrom;
    private String xwhere;
    private String xgroupby;
    private String classname;
    private String classmethod;

    private AdReference reference;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idChart;
    }

    @Override
    public void setId(String id) {
        this.idChart = id;
    }

    @Id
    @Column(name = "id_chart")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdChart() {
        return idChart;
    }

    public void setIdChart(String idChart) {
        this.idChart = idChart;
    }

    @Column(name = "ctype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    @Column(name = "displaylogic", length = 250)
    @Size(min = 1, max = 250)
    public String getDisplaylogic() {
        return displaylogic;
    }

    public void setDisplaylogic(String displaylogic) {
        this.displaylogic = displaylogic;
    }

    @Column(name = "id_tab", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdTab() {
        return idTab;
    }

    public void setIdTab(String idTab) {
        this.idTab = idTab;
    }

    @Column(name = "key_field", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getKeyField() {
        return keyField;
    }

    public void setKeyField(String keyField) {
        this.keyField = keyField;
    }

    @Column(name = "key_reference", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getKeyReference() {
        return keyReference;
    }

    public void setKeyReference(String keyReference) {
        this.keyReference = keyReference;
    }

    @Column(name = "name_field", length = 250)
    @NotNull
    @Size(min = 1, max = 250)
    public String getNameField() {
        return nameField;
    }

    public void setNameField(String nameField) {
        this.nameField = nameField;
    }

    @Column(name = "fullname_field", length = 250)
    @Size(min = 1, max = 250)
    public String getFullnameField() {
        return fullnameField;
    }

    public void setFullnameField(String fullnameField) {
        this.fullnameField = fullnameField;
    }

    @Column(name = "mode", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "prefix", length = 10)
    @Size(min = 1, max = 10)
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Column(name = "row_limit")
    public Long getRowLimit() {
        return rowLimit;
    }

    public void setRowLimit(Long rowLimit) {
        this.rowLimit = rowLimit;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "title_x", length = 150)
    @Size(min = 1, max = 150)
    public String getTitleX() {
        return titleX;
    }

    public void setTitleX(String titleX) {
        this.titleX = titleX;
    }

    @Column(name = "title_y", length = 150)
    @Size(min = 1, max = 150)
    public String getTitleY() {
        return titleY;
    }

    public void setTitleY(String titleY) {
        this.titleY = titleY;
    }

    @Column(name = "show_title_x")
    @NotNull
    public Boolean getShowTitleX() {
        return showTitleX;
    }

    public void setShowTitleX(Boolean showTitleX) {
        this.showTitleX = showTitleX;
    }

    @Column(name = "show_title_y")
    @NotNull
    public Boolean getShowTitleY() {
        return showTitleY;
    }

    public void setShowTitleY(Boolean showTitleY) {
        this.showTitleY = showTitleY;
    }

    @Column(name = "span")
    @NotNull
    public Long getSpan() {
        return span;
    }

    public void setSpan(Long span) {
        this.span = span;
    }

    @Column(name = "startnewrow")
    @NotNull
    public Boolean getStartnewrow() {
        return startnewrow;
    }

    public void setStartnewrow(Boolean startnewrow) {
        this.startnewrow = startnewrow;
    }

    @Column(name = "value_1", length = 250)
    @Size(min = 1, max = 250)
    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    @Column(name = "value_2", length = 250)
    @Size(min = 1, max = 250)
    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    @Column(name = "value_3", length = 250)
    @Size(min = 1, max = 250)
    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    @Column(name = "value_4", length = 250)
    @Size(min = 1, max = 250)
    public String getValue4() {
        return value4;
    }

    public void setValue4(String value4) {
        this.value4 = value4;
    }

    @Column(name = "value_5", length = 250)
    @Size(min = 1, max = 250)
    public String getValue5() {
        return value5;
    }

    public void setValue5(String value5) {
        this.value5 = value5;
    }

    @Column(name = "value_6", length = 250)
    @Size(min = 1, max = 250)
    public String getValue6() {
        return value6;
    }

    public void setValue6(String value6) {
        this.value6 = value6;
    }

    @Column(name = "value_7", length = 250)
    @Size(min = 1, max = 250)
    public String getValue7() {
        return value7;
    }

    public void setValue7(String value7) {
        this.value7 = value7;
    }

    @Column(name = "value_8", length = 250)
    @Size(min = 1, max = 250)
    public String getValue8() {
        return value8;
    }

    public void setValue8(String value8) {
        this.value8 = value8;
    }

    @Column(name = "value_9", length = 250)
    @Size(min = 1, max = 250)
    public String getValue9() {
        return value9;
    }

    public void setValue9(String value9) {
        this.value9 = value9;
    }

    @Column(name = "value_10", length = 250)
    @Size(min = 1, max = 250)
    public String getValue10() {
        return value10;
    }

    public void setValue10(String value10) {
        this.value10 = value10;
    }

    @Column(name = "legend_1", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend1() {
        return legend1;
    }

    public void setLegend1(String legend1) {
        this.legend1 = legend1;
    }

    @Column(name = "legend_2", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend2() {
        return legend2;
    }

    public void setLegend2(String legend2) {
        this.legend2 = legend2;
    }

    @Column(name = "legend_3", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend3() {
        return legend3;
    }

    public void setLegend3(String legend3) {
        this.legend3 = legend3;
    }

    @Column(name = "legend_4", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend4() {
        return legend4;
    }

    public void setLegend4(String legend4) {
        this.legend4 = legend4;
    }

    @Column(name = "legend_5", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend5() {
        return legend5;
    }

    public void setLegend5(String legend5) {
        this.legend5 = legend5;
    }

    @Column(name = "legend_6", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend6() {
        return legend6;
    }

    public void setLegend6(String legend6) {
        this.legend6 = legend6;
    }

    @Column(name = "legend_7", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend7() {
        return legend7;
    }

    public void setLegend7(String legend7) {
        this.legend7 = legend7;
    }

    @Column(name = "legend_8", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend8() {
        return legend8;
    }

    public void setLegend8(String legend8) {
        this.legend8 = legend8;
    }

    @Column(name = "legend_9", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend9() {
        return legend9;
    }

    public void setLegend9(String legend9) {
        this.legend9 = legend9;
    }

    @Column(name = "legend_10", length = 100)
    @Size(min = 1, max = 100)
    public String getLegend10() {
        return legend10;
    }

    public void setLegend10(String legend10) {
        this.legend10 = legend10;
    }

    @Column(name = "color_1", length = 20)
    @Size(min = 1, max = 20)
    public String getColor1() {
        return color1;
    }

    public void setColor1(String color1) {
        this.color1 = color1;
    }

    @Column(name = "color_2", length = 20)
    @Size(min = 1, max = 20)
    public String getColor2() {
        return color2;
    }

    public void setColor2(String color2) {
        this.color2 = color2;
    }

    @Column(name = "color_3", length = 20)
    @Size(min = 1, max = 20)
    public String getColor3() {
        return color3;
    }

    public void setColor3(String color3) {
        this.color3 = color3;
    }

    @Column(name = "color_4", length = 20)
    @Size(min = 1, max = 20)
    public String getColor4() {
        return color4;
    }

    public void setColor4(String color4) {
        this.color4 = color4;
    }

    @Column(name = "color_5", length = 20)
    @Size(min = 1, max = 20)
    public String getColor5() {
        return color5;
    }

    public void setColor5(String color5) {
        this.color5 = color5;
    }

    @Column(name = "color_6", length = 20)
    @Size(min = 1, max = 20)
    public String getColor6() {
        return color6;
    }

    public void setColor6(String color6) {
        this.color6 = color6;
    }

    @Column(name = "color_7", length = 20)
    @Size(min = 1, max = 20)
    public String getColor7() {
        return color7;
    }

    public void setColor7(String color7) {
        this.color7 = color7;
    }

    @Column(name = "color_8", length = 20)
    @Size(min = 1, max = 20)
    public String getColor8() {
        return color8;
    }

    public void setColor8(String color8) {
        this.color8 = color8;
    }

    @Column(name = "color_9", length = 20)
    @Size(min = 1, max = 20)
    public String getColor9() {
        return color9;
    }

    public void setColor9(String color9) {
        this.color9 = color9;
    }

    @Column(name = "color_10", length = 20)
    @Size(min = 1, max = 20)
    public String getColor10() {
        return color10;
    }

    public void setColor10(String color10) {
        this.color10 = color10;
    }

    @Column(name = "comp_color_1", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor1() {
        return compColor1;
    }

    public void setCompColor1(String compColor1) {
        this.compColor1 = compColor1;
    }

    @Column(name = "comp_color_2", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor2() {
        return compColor2;
    }

    public void setCompColor2(String compColor2) {
        this.compColor2 = compColor2;
    }

    @Column(name = "comp_color_3", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor3() {
        return compColor3;
    }

    public void setCompColor3(String compColor3) {
        this.compColor3 = compColor3;
    }

    @Column(name = "comp_color_4", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor4() {
        return compColor4;
    }

    public void setCompColor4(String compColor4) {
        this.compColor4 = compColor4;
    }

    @Column(name = "comp_color_5", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor5() {
        return compColor5;
    }

    public void setCompColor5(String compColor5) {
        this.compColor5 = compColor5;
    }

    @Column(name = "comp_color_6", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor6() {
        return compColor6;
    }

    public void setCompColor6(String compColor6) {
        this.compColor6 = compColor6;
    }

    @Column(name = "comp_color_7", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor7() {
        return compColor7;
    }

    public void setCompColor7(String compColor7) {
        this.compColor7 = compColor7;
    }

    @Column(name = "comp_color_8", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor8() {
        return compColor8;
    }

    public void setCompColor8(String compColor8) {
        this.compColor8 = compColor8;
    }

    @Column(name = "comp_color_9", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor9() {
        return compColor9;
    }

    public void setCompColor9(String compColor9) {
        this.compColor9 = compColor9;
    }

    @Column(name = "comp_color_10", length = 20)
    @Size(min = 1, max = 20)
    public String getCompColor10() {
        return compColor10;
    }

    public void setCompColor10(String compColor10) {
        this.compColor10 = compColor10;
    }

    @Column(name = "xfrom")
    public String getXfrom() {
        return xfrom;
    }

    public void setXfrom(String xfrom) {
        this.xfrom = xfrom;
    }

    @Column(name = "xwhere")
    public String getXwhere() {
        return xwhere;
    }

    public void setXwhere(String xwhere) {
        this.xwhere = xwhere;
    }

    @Column(name = "xgroupby", length = 500)
    @Size(min = 1, max = 500)
    public String getXgroupby() {
        return xgroupby;
    }

    public void setXgroupby(String xgroupby) {
        this.xgroupby = xgroupby;
    }

    @Column(name = "classname", length = 150)
    @Size(min = 1, max = 150)
    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Column(name = "classmethod", length = 60)
    @Size(min = 1, max = 60)
    public String getClassmethod() {
        return classmethod;
    }

    public void setClassmethod(String classmethod) {
        this.classmethod = classmethod;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "key_reference", referencedColumnName = "id_reference", insertable = false, updatable = false)
    public AdReference getReference() {
        return reference;
    }

    public void setReference(AdReference reference) {
        this.reference = reference;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdChart)) return false;

        final AdChart that = (AdChart) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idChart == null) && (that.idChart == null)) || (idChart != null && idChart.equals(that.idChart)));
        result = result && (((ctype == null) && (that.ctype == null)) || (ctype != null && ctype.equals(that.ctype)));
        result = result && (((displaylogic == null) && (that.displaylogic == null)) || (displaylogic != null && displaylogic.equals(that.displaylogic)));
        result = result && (((idTab == null) && (that.idTab == null)) || (idTab != null && idTab.equals(that.idTab)));
        result = result && (((keyField == null) && (that.keyField == null)) || (keyField != null && keyField.equals(that.keyField)));
        result = result && (((keyReference == null) && (that.keyReference == null)) || (keyReference != null && keyReference.equals(that.keyReference)));
        result = result && (((nameField == null) && (that.nameField == null)) || (nameField != null && nameField.equals(that.nameField)));
        result = result && (((fullnameField == null) && (that.fullnameField == null)) || (fullnameField != null && fullnameField.equals(that.fullnameField)));
        result = result && (((mode == null) && (that.mode == null)) || (mode != null && mode.equals(that.mode)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((prefix == null) && (that.prefix == null)) || (prefix != null && prefix.equals(that.prefix)));
        result = result && (((rowLimit == null) && (that.rowLimit == null)) || (rowLimit != null && rowLimit.equals(that.rowLimit)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((titleX == null) && (that.titleX == null)) || (titleX != null && titleX.equals(that.titleX)));
        result = result && (((titleY == null) && (that.titleY == null)) || (titleY != null && titleY.equals(that.titleY)));
        result = result && (((showTitleX == null) && (that.showTitleX == null)) || (showTitleX != null && titleY.equals(that.showTitleX)));
        result = result && (((showTitleY == null) && (that.showTitleY == null)) || (showTitleY != null && showTitleY.equals(that.showTitleY)));
        result = result && (((span == null) && (that.span == null)) || (span != null && span.equals(that.span)));
        result = result && (((startnewrow == null) && (that.startnewrow == null)) || (startnewrow != null && startnewrow.equals(that.startnewrow)));
        result = result && (((value1 == null) && (that.value1 == null)) || (value1 != null && value1.equals(that.value1)));
        result = result && (((value2 == null) && (that.value2 == null)) || (value2 != null && value2.equals(that.value2)));
        result = result && (((value3 == null) && (that.value3 == null)) || (value3 != null && value3.equals(that.value3)));
        result = result && (((value4 == null) && (that.value4 == null)) || (value4 != null && value4.equals(that.value4)));
        result = result && (((value5 == null) && (that.value5 == null)) || (value5 != null && value5.equals(that.value5)));
        result = result && (((value6 == null) && (that.value6 == null)) || (value6 != null && value6.equals(that.value6)));
        result = result && (((value7 == null) && (that.value7 == null)) || (value7 != null && value7.equals(that.value7)));
        result = result && (((value8 == null) && (that.value8 == null)) || (value8 != null && value8.equals(that.value8)));
        result = result && (((value9 == null) && (that.value9 == null)) || (value9 != null && value9.equals(that.value9)));
        result = result && (((value10 == null) && (that.value10 == null)) || (value10 != null && value10.equals(that.value10)));
        result = result && (((legend1 == null) && (that.legend1 == null)) || (legend1 != null && legend1.equals(that.legend1)));
        result = result && (((legend2 == null) && (that.legend2 == null)) || (legend2 != null && legend2.equals(that.legend2)));
        result = result && (((legend3 == null) && (that.legend3 == null)) || (legend3 != null && legend3.equals(that.legend3)));
        result = result && (((legend4 == null) && (that.legend4 == null)) || (legend4 != null && legend4.equals(that.legend4)));
        result = result && (((legend5 == null) && (that.legend5 == null)) || (legend5 != null && legend5.equals(that.legend5)));
        result = result && (((legend6 == null) && (that.legend6 == null)) || (legend6 != null && legend6.equals(that.legend6)));
        result = result && (((legend7 == null) && (that.legend7 == null)) || (legend7 != null && legend7.equals(that.legend7)));
        result = result && (((legend8 == null) && (that.legend8 == null)) || (legend8 != null && legend8.equals(that.legend8)));
        result = result && (((legend9 == null) && (that.legend9 == null)) || (legend9 != null && legend9.equals(that.legend9)));
        result = result && (((legend10 == null) && (that.legend10 == null)) || (legend10 != null && legend10.equals(that.legend10)));
        result = result && (((color1 == null) && (that.color1 == null)) || (color1 != null && color1.equals(that.color1)));
        result = result && (((color2 == null) && (that.color2 == null)) || (color2 != null && color2.equals(that.color2)));
        result = result && (((color3 == null) && (that.color3 == null)) || (color3 != null && color3.equals(that.color3)));
        result = result && (((color4 == null) && (that.color4 == null)) || (color4 != null && color4.equals(that.color4)));
        result = result && (((color5 == null) && (that.color5 == null)) || (color5 != null && color5.equals(that.color5)));
        result = result && (((color6 == null) && (that.color6 == null)) || (color6 != null && color6.equals(that.color6)));
        result = result && (((color7 == null) && (that.color7 == null)) || (color7 != null && color7.equals(that.color7)));
        result = result && (((color8 == null) && (that.color8 == null)) || (color8 != null && color8.equals(that.color8)));
        result = result && (((color9 == null) && (that.color9 == null)) || (color9 != null && color9.equals(that.color9)));
        result = result && (((color10 == null) && (that.color10 == null)) || (color10 != null && color10.equals(that.color10)));
        result = result && (((compColor1 == null) && (that.compColor1 == null)) || (compColor1 != null && compColor1.equals(that.compColor1)));
        result = result && (((compColor2 == null) && (that.compColor2 == null)) || (compColor2 != null && compColor2.equals(that.compColor2)));
        result = result && (((compColor3 == null) && (that.compColor3 == null)) || (compColor3 != null && compColor3.equals(that.compColor3)));
        result = result && (((compColor4 == null) && (that.compColor4 == null)) || (compColor4 != null && compColor4.equals(that.compColor4)));
        result = result && (((compColor5 == null) && (that.compColor5 == null)) || (compColor5 != null && compColor5.equals(that.compColor5)));
        result = result && (((compColor6 == null) && (that.compColor6 == null)) || (compColor6 != null && compColor6.equals(that.compColor6)));
        result = result && (((compColor7 == null) && (that.compColor7 == null)) || (compColor7 != null && compColor7.equals(that.compColor7)));
        result = result && (((compColor8 == null) && (that.compColor8 == null)) || (compColor8 != null && compColor8.equals(that.compColor8)));
        result = result && (((compColor9 == null) && (that.compColor9 == null)) || (compColor9 != null && compColor9.equals(that.compColor9)));
        result = result && (((compColor10 == null) && (that.compColor10 == null)) || (compColor10 != null && compColor10.equals(that.compColor10)));
        result = result && (((xfrom == null) && (that.xfrom == null)) || (xfrom != null && xfrom.equals(that.xfrom)));
        result = result && (((xwhere == null) && (that.xwhere == null)) || (xwhere != null && xwhere.equals(that.xwhere)));
        result = result && (((xgroupby == null) && (that.xgroupby == null)) || (xgroupby != null && xgroupby.equals(that.xgroupby)));
        result = result && (((classname == null) && (that.classname == null)) || (classname != null && classname.equals(that.classname)));
        result = result && (((classmethod == null) && (that.classmethod == null)) || (classmethod != null && classmethod.equals(that.classmethod)));
        return result;
    }

    public String getCompareColor(int indx) {
        switch (indx) {
            case 1:
                return compColor1;
            case 2:
                return compColor2;
            case 3:
                return compColor3;
            case 4:
                return compColor4;
            case 5:
                return compColor5;
            case 6:
                return compColor6;
            case 7:
                return compColor7;
            case 8:
                return compColor8;
            case 9:
                return compColor9;
            case 10:
                return compColor10;
        }
        return null;
    }
}

