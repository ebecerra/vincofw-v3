package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdCharacteristicDef;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service layer implementation for ad_characteristic_def
 *
 * Date: 23/01/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdCharacteristicDefServiceImpl extends BaseServiceImpl<AdCharacteristicDef, String> implements AdCharacteristicDefService {

    /**
     * Constructor.
     */
    public AdCharacteristicDefServiceImpl() {
        super(AdCharacteristicDef.class);
    }

}
