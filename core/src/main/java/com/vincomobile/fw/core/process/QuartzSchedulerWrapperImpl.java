package com.vincomobile.fw.core.process;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

public class QuartzSchedulerWrapperImpl implements QuartzSchedulerWrapper {

    private Logger logger = LoggerFactory.getLogger(QuartzSchedulerWrapperImpl.class);

    private org.quartz.Scheduler scheduler;

    public void setScheduler(org.quartz.Scheduler scheduler) {
        this.scheduler = scheduler;
        try {
            this.scheduler.start();
        } catch (SchedulerException e) {
            logger.error("Error starting scheduler", e);
        }
    }

    @Override
    public org.quartz.Scheduler getScheduler(){
        return scheduler;
    }

    @Override
    public void scheduleAJob(Trigger trigger, JobDetail job) throws SchedulerException {
        scheduler.scheduleJob(job, trigger);
    }
}
