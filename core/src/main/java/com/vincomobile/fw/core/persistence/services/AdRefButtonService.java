package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRefButton;

/**
 * Created by Devtools.
 * Interface del servicio de ad_ref_button
 *
 * Date: 27/11/2015
 */
public interface AdRefButtonService extends BaseService<AdRefButton, String> {

    String BTYPE_PROCESS        = "PROCESS";
    String BTYPE_JAVASCRIPT     = "JAVASCRIPT";

    /**
     * Get reference button by reference
     *
     * @param idReference Reference identifier
     * @return Reference Table
     */
    AdRefButton getRefButtonByReference(String idReference);

}


