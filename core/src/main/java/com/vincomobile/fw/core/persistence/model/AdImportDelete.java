package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Vincomobile FW on 23/06/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Model for table ad_import_delete
 */
@Entity
@Table(name = "ad_import_delete")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdImportDelete extends EntityBean<AdImportDeletePK> {

    private AdImportDeletePK id;

    /*
     * Set/Get Methods
     */

    @Id
    public AdImportDeletePK getId() {
        return id;
    }

    public void setId(AdImportDeletePK id) {
        this.id = id;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdImportDelete)) return false;

        final AdImportDelete that = (AdImportDelete) aThat;
        boolean result = true;
        result = result && id.equals(that.getId());
        return result;
    }

}

