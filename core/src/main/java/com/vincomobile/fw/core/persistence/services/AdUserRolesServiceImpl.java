package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdUser;
import com.vincomobile.fw.core.persistence.model.AdUserRoles;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_USER_ROLES
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdUserRolesServiceImpl extends BaseServiceImpl<AdUserRoles, String> implements AdUserRolesService {

    /**
     * Constructor.
     *
     */
    public AdUserRolesServiceImpl() {
        super(AdUserRoles.class);
    }

    /**
     * List user by role
     *
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @return User list
     */
    @Override
    public List<AdUser> listUserByRole(String idClient, String idRole) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idRole", getClientFilter(idRole));
        List<AdUserRoles> userRoles = findAll(filter);
        List<AdUser> result = new ArrayList<>();
        for (AdUserRoles ur : userRoles) {
            result.add(ur.getUser());
        }
        return result;
    }

    /**
     * Find user role
     *
     * @param idUser User identifier
     * @param idRole Role identifier
     * @return AdUserRoles
     */
    @Override
    public AdUserRoles findUserRole(String idUser, String idRole) {
        Map filter = new HashMap();
        filter.put("idUser", idUser);
        filter.put("idRole", idRole);
        return findFirst(filter);
    }

}
