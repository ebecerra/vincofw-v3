package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRole;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service of AD_ROLE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdRoleServiceImpl extends BaseServiceImpl<AdRole, String> implements AdRoleService {

    /**
     * Constructor.
     *
     */
    public AdRoleServiceImpl() {
        super(AdRole.class);
    }

}
