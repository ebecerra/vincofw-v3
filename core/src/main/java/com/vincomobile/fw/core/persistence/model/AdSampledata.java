package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Model for table ad_sampledata
 */
@Entity
@Table(name = "ad_sampledata")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdSampledata extends AdEntityBean {

    private String idSampledata;
    private String name;
    private String description;


    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idSampledata;
    }

    @Override
    public void setId(String id) {
            this.idSampledata = id;
    }

    @Id
    @Column(name = "id_sampledata")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdSampledata() {
        return idSampledata;
    }

    public void setIdSampledata(String idSampledata) {
        this.idSampledata = idSampledata;
    }

    @Column(name = "name", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 2000)
    @Size(min = 1, max = 2000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdSampledata)) return false;

        final AdSampledata that = (AdSampledata) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idSampledata == null) && (that.idSampledata == null)) || (idSampledata != null && idSampledata.equals(that.idSampledata)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        return result;
    }


}

