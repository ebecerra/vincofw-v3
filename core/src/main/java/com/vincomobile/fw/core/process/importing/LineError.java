package com.vincomobile.fw.core.process.importing;

public class LineError extends BaseError {

    long line;

    public LineError(String level, String error, long row) {
        super(level, error);
        this.line = row;
    }

    public long getLine() {
        return line;
    }

    public void setLine(long line) {
        this.line = line;
    }

    @Override
    public String getError() {
        return "[" +  level + "] Line: " + line + " > " + error;
    }

}
