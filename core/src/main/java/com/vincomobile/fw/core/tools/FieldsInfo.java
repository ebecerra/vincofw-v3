package com.vincomobile.fw.core.tools;

public class FieldsInfo {
    String name;
    int type, size, decimals;
    boolean pk, nullable;

    public FieldsInfo(String name, boolean nullable, int type, int size, int decimals) {
        this.nullable = nullable;
        this.name = name;
        this.type = type;
        this.size = size;
        this.decimals = decimals;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getType() {
        switch (type) {
            case -1:
                return "TEXT";
            case -9:
            case 1:
            case 92:
            case 12:
                return "STRING";
            case -5:
            case -6:
            case 5:
            case 6:
            case 4:
                return "INTEGER";
            case 3:
            case 7:
            case 8:
                return "NUMBER";
            case -2:
            case -7:
            case 16:
                return "BOOLEAN";
            case 91:
            case 93:
                return "DATE";
            case -4:
                return "BLOB";
            default:
                return ""+type;
        }
    }

    public boolean isPk() {
        return pk;
    }

    public void setPk(boolean pk) {
        this.pk = pk;
    }

    public void setSize(int size) {
        this.size = size;
    }
    public int getSize() {
        return size;
    }

    public void setDecimals(int decimals) {
        this.decimals = decimals;
    }

    public int getDecimals() {
        return decimals;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }
}

