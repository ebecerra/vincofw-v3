package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdField;
import com.vincomobile.fw.core.persistence.model.AdFieldGrid;
import com.vincomobile.fw.core.persistence.model.AdTab;
import com.vincomobile.fw.core.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_TAB
 * <p/>
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdTabServiceImpl extends BaseServiceImpl<AdTab, String> implements AdTabService {

    /**
     * Constructor.
     */
    public AdTabServiceImpl() {
        super(AdTab.class);
    }

    /**
     * List all fields for Tab
     *
     * @param idTab Tab identifier
     * @return Field list
     */
    @Override
    public List<AdField> listFields(String idTab) {
        Query query = entityManager.createQuery("FROM AdField WHERE idTab = :idTab ORDER BY gridSeqno", AdField.class);
        query.setParameter("idTab", idTab);
        return query.getResultList();
    }

    /**
     * List all fields for Tab (GridView)
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @return Field list
     */
    @Override
    public List<AdField> listGridFields(String idClient, String idTab, String idUser) {
        Query query = entityManager.createQuery(
                "FROM AdFieldGrid WHERE idClient IN :idClient AND idTab = :idTab AND idUser = :idUser ORDER BY seqno",
                AdFieldGrid.class
        );
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idTab", idTab);
        query.setParameter("idUser", idUser);
        List<AdFieldGrid> fieldGrids = query.getResultList();
        if (fieldGrids.size() == 0) {
            query = entityManager.createQuery("FROM AdField WHERE showingrid = true AND idTab = :idTab ORDER BY gridSeqno", AdField.class);
            query.setParameter("idTab", idTab);
            return query.getResultList();
        }
        List<AdField> result = new ArrayList<>();
        for (AdFieldGrid fieldGrid : fieldGrids) {
            result.add(fieldGrid.getField());
        }
        return result;
    }

    /**
     * Clear view default to all tabs of level
     *
     * @param idWindow Window identifier
     * @param idTable Table identifier
     * @param tabLevel Tab level
     */
    @Override
    public void clearViewDefault(String idWindow, String idTable, long tabLevel) {
        if (!Converter.isEmpty(idWindow)) {
            Map params = new HashMap();
            params.put("idWindow", idWindow);
            params.put("idTable", idTable);
            params.put("tabLevel", tabLevel);
            applyUpdate("UPDATE AdTab SET viewDefault = 0 WHERE idWindow = :idWindow AND idTable = :idTable  AND tablevel = :tabLevel", params);
        }
    }

    /**
     * Check and fix the view default to all tabs of level
     *
     * @param idWindow Window identifier
     * @param idTable Table identifier
     * @param tabLevel Tab level
     */
    @Override
    public void checkViewDefault(String idWindow, String idTable, long tabLevel) {
        if (!Converter.isEmpty(idWindow)) {
            Map filter = new HashMap();
            filter.put("idWindow", idWindow);
            filter.put("idTable", idTable);
            filter.put("tabLevel", tabLevel);
            List<AdTab> tabs = findAll(filter);
            boolean viewDefault = false;
            for (AdTab tab : tabs) {
                if (tab.getViewDefault()) {
                    viewDefault = true;
                    break;
                }
            }
            if (!viewDefault) {
                for (AdTab tab : tabs) {
                    if (!AdTabService.UIPATTERN_SORTABLE.equals(tab.getUipattern()) && !AdTabService.UIPATTERN_MULTISELECT.equals(tab.getUipattern())) {
                        tab.setViewDefault(true);
                        update(tab);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Get the first editable tab for user and role
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idRole Role identifier
     * @param idTable Table identifier
     * @return Tab
     */
    @Override
    public AdTab getUserEditableTab(String idClient, String idUser, String idRole, String idTable) {
        Query query = entityManager.createQuery(
                "SELECT T\n" +
                "FROM AdTab T, AdWindow W, AdMenu M, AdMenuRoles MR, AdUserRoles UR\n" +
                "WHERE T.idWindow = W.idWindow AND W.idWindow = M.idWindow AND T.idClient IN :idClient\n" +
                "AND M.active = 1 AND T.active = 1 AND W.active = 1 AND MR.active = 1 AND UR.active = 1 \n" +
                "AND M.action = 'WINDOW' AND T.uipattern IN ('STANDARD', 'EDIT', 'EDIT_DELETE') \n" +
                "AND M.idMenu = MR.idMenu AND MR.idRole = UR.idRole \n" +
                "AND T.idTable = :idTable AND MR.idRole = :idRole AND UR.idUser = :idUser",
                AdTab.class
        );
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idUser", idUser);
        query.setParameter("idRole", idRole);
        query.setParameter("idTable", idTable);
        query.setFirstResult(0);
        query.setMaxResults(1);
        List<AdTab> result = query.getResultList();
        return result.size() > 0 ? result.get(0) : null;
    }

}
