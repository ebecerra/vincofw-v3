package com.vincomobile.fw.core.sync;

import java.util.List;

public class SyncOfflineTable {

    String idTable;
    String name;
    Long version;
    List<String> columns;
    List<SyncOfflineItem> items;

    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<SyncOfflineItem> getItems() {
        return items;
    }

    public void setItems(List<SyncOfflineItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("idTable: ").append(idTable).append(", name: ").append(name).append(", version: ").append(version).append("\n");
        for (SyncOfflineItem item : items) {
            out.append(item).append("\n");
        }
        return out.toString();
    }

}
