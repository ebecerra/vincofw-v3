package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.core.persistence.beans.CodeGenColumn;
import com.vincomobile.fw.core.persistence.services.AdColumnService;
import com.vincomobile.fw.core.persistence.services.AdReferenceService;
import com.vincomobile.fw.core.tools.Converter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de AD_TABLE
 * <p/>
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_table")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdTable extends AdEntityBean {

    private String idTable;
    private String idSortColumn;
    private String name;
    private String dataOrigin;
    private Boolean isview;
    private Long seqno;
    private Long seqnoUpdate;
    private Long version;
    private String sqlQuery;
    private int totalColumns;
    private Boolean exportData;
    private Boolean sortable;
    private Boolean audit;
    private String privilege;
    private String privilegeDesc;

    private List<AdColumn> columns;
    private List<AdTableAction> actions;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idTable;
    }

    @Override
    public void setId(String id) {
        this.idTable = id;
    }

    @Id
    @Column(name = "id_table")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_sort_column", length = 32)
    @Size(min = 1, max = 32)
    public String getIdSortColumn() {
        return idSortColumn;
    }

    public void setIdSortColumn(String idSortColumn) {
        this.idSortColumn = idSortColumn;
    }

    @Column(name = "name", length = 100, unique = true)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "data_origin", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getDataOrigin() {
        return dataOrigin;
    }

    public void setDataOrigin(String dataOrigin) {
        this.dataOrigin = dataOrigin;
    }

    @Column(name = "isview")
    @NotNull
    public Boolean getIsview() {
        return isview;
    }

    public void setIsview(Boolean isview) {
        this.isview = isview;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "seqno_update")
    public Long getSeqnoUpdate() {
        return seqnoUpdate;
    }

    public void setSeqnoUpdate(Long seqnoUpdate) {
        this.seqnoUpdate = seqnoUpdate;
    }

    @Column(name = "version")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Column(name = "sql_query", length = 2000)
    @Size(min = 1, max = 2000)
    public String getSqlQuery() {
        return sqlQuery;
    }

    public void setSqlQuery(String sql) {
        this.sqlQuery = sql;
    }

    @Column(name = "export_data")
    @NotNull
    public Boolean getExportData() {
        return exportData;
    }

    public void setExportData(Boolean exportData) {
        this.exportData = exportData;
    }

    @Column(name = "sortable")
    @NotNull
    public Boolean getSortable() {
        return sortable;
    }

    public void setSortable(Boolean sortable) {
        this.sortable = sortable;
    }

    @Column(name = "audit")
    @NotNull
    public Boolean getAudit() {
        return audit;
    }

    public void setAudit(Boolean audit) {
        this.audit = audit;
    }

    @Column(name = "privilege", length = 50)
    @Size(min = 1, max = 50)
    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @Column(name = "privilege_desc")
    public String getPrivilegeDesc() {
        return privilegeDesc;
    }

    public void setPrivilegeDesc(String privilegeDesc) {
        this.privilegeDesc = privilegeDesc;
    }

    @OneToMany
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    @OrderBy("seqno")
    public List<AdColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<AdColumn> columns) {
        this.columns = columns;
    }

    @OneToMany
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    @OrderBy("seqno")
    public List<AdTableAction> getActions() {
        return actions;
    }

    public void setActions(List<AdTableAction> actions) {
        this.actions = actions;
    }

    /**
     * Equals implementation
     *
     * @param aThat Object to compare with
     * @return true/false
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdTable)) return false;

        final AdTable that = (AdTable) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idSortColumn == null) && (that.idSortColumn == null)) || (idSortColumn != null && idSortColumn.equals(that.idSortColumn)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((dataOrigin == null) && (that.dataOrigin == null)) || (dataOrigin != null && dataOrigin.equals(that.dataOrigin)));
        result = result && (((isview == null) && (that.isview == null)) || (isview != null && isview.equals(that.isview)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((seqnoUpdate == null) && (that.seqnoUpdate == null)) || (seqnoUpdate != null && seqnoUpdate.equals(that.seqnoUpdate)));
        result = result && (((version == null) && (that.version == null)) || (version != null && version.equals(that.version)));
        result = result && (((sqlQuery == null) && (that.sqlQuery == null)) || (sqlQuery != null && sqlQuery.equals(that.sqlQuery)));
        result = result && (((exportData == null) && (that.exportData == null)) || (exportData != null && exportData.equals(that.exportData)));
        result = result && (((sortable == null) && (that.sortable == null)) || (sortable != null && sortable.equals(that.sortable)));
        result = result && (((audit == null) && (that.audit == null)) || (audit != null && audit.equals(that.audit)));
        result = result && (((privilege == null) && (that.privilege == null)) || (privilege != null && privilege.equals(that.privilege)));
        result = result && (((privilegeDesc == null) && (that.privilegeDesc == null)) || (privilegeDesc != null && privilegeDesc.equals(that.privilegeDesc)));
        return result;
    }

    @Transient
    public int getTotalColumns() {
        return totalColumns;
    }

    public void setTotalColumns(int totalColumns) {
        this.totalColumns = totalColumns;
    }

    /**
     * Get columns names for HQL Query
     *
     * @return HQL Columns names
     */
    @Transient
    @JsonIgnore
    public String getHQLFields() {
        String result = "";
        for (AdColumn column : getColumns()) {
            if (!result.equals("")) {
                result += ", ";
            }
            if (AdColumnService.SOURCE_DATABASE.equals(column.getCsource())) {
                result += column.getStandardName();
            }
        }
        return result;
    }

    /**
     * List Key Columns
     *
     * @return Columns list
     */
    @Transient
    public List<AdColumn> getKeyFields() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (column.getPrimaryKey()) {
                results.add(column);
            }
        }
        return results;
    }

    /**
     * List No Key Columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getNoKeyFields() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (!column.getPrimaryKey()) {
                results.add(column);
            }
        }
        return results;
    }

    /**
     * List all columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getAllFields() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            results.add(column);
        }
        return results;
    }

    /**
     * List all database columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getAllDBFields() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (AdColumnService.SOURCE_DATABASE.equals(column.getCsource())) {
                results.add(column);
            }
        }
        return results;
    }

    /**
     * Get field
     *
     * @param name Colunm name
     * @return Column
     */
    @Transient
    @JsonIgnore
    public AdColumn getField(String name) {
        for (AdColumn column : getColumns()) {
            if (column.getName().equals(name)) ;
            return column;
        }
        return null;
    }

    /**
     * List No Key Columns (Exclude core)
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getNoKeyFieldsExclCore() {
        List<AdColumn> results = new ArrayList<AdColumn>();
        for (AdColumn column : getColumns()) {
            if (AdColumnService.SOURCE_DATABASE.equals(column.getCsource())) {
                String name = column.getName();
                if (!column.getPrimaryKey() && !AdColumnService.CORE_FIELDS.contains(name.toLowerCase())) {
                    results.add(column);
                }
            }
        }
        return results;
    }

    /**
     * Test if is simple key (only one field) or not
     *
     * @return True if is simple key
     */
    @Transient
    @JsonIgnore
    public boolean isSimpleKey() {
        return getKeyFields().size() == 1;
    }

    /**
     * Test if is multiple key (more than one filed) or not
     *
     * @return True if is multiple key
     */
    @Transient
    @JsonIgnore
    public boolean getMultipleKey() {
        return this.getKeyFields().size() > 1;
    }

    /*
     * Transform table name: capitalize, standarize, lowercase, uppercase
     */
    @Transient
    @JsonIgnore
    public String getCapitalizeStandardName() {
        return Converter.removeUnderscores(Converter.capitalize(this.name.toLowerCase()));
    }

    @Transient
    @JsonIgnore
    public String getLowerName() {
        return this.name.toLowerCase();
    }

    @Transient
    @JsonIgnore
    public String getUpperCaseName() {
        return this.name.toUpperCase();
    }

    @Transient
    @JsonIgnore
    public String getCapitalizeName() {
        return Converter.capitalize(this.name.toLowerCase());
    }

    @Transient
    @JsonIgnore
    public String getStandardName() {
        return Converter.removeUnderscores(this.name.toLowerCase());
    }

    /**
     * Get type of primary key
     *
     * @return Primery key type
     */
    @Transient
    @JsonIgnore
    public String getKeyType() {
        if (!getMultipleKey()) {
            List<AdColumn> keyFields = this.getKeyFields();
            if (keyFields.size() > 0) {
                AdColumn column = keyFields.get(0);
                if (column.getCtype().equals("DATE")) {
                    return "Date";
                } else if (column.getCtype().equals("STRING")) {
                    return "String";
                } else if (column.getCtype().equals("BOOLEAN")) {
                    return "Boolean";
                }
            }
        }
        return "String";
    }

    /**
     * Return 'true' if has some Date fields
     *
     * @return True/False
     */
    @Transient
    @JsonIgnore
    public boolean getDates() {
        for (AdColumn column : getColumns()) {
            if ("DATE".equals(column.getCtype())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return 'true' if has some Date fields
     *
     * @return True/False
     */
    @Transient
    @JsonIgnore
    public boolean getDatesExclCore() {
        for (AdColumn column : getColumns()) {
            String name = column.getName();
            if ("DATE".equals(column.getCtype()) && !(name.equalsIgnoreCase("created") || name.equalsIgnoreCase("updated") || name.equalsIgnoreCase("active"))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return 'true' if has some Date fields
     *
     * @return True/False
     */
    @Transient
    @JsonIgnore
    public boolean getNoKeyDates() {
        for (AdColumn column : getNoKeyFields()) {
            if ("DATE".equals(column.getCtype())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return 'true' if has some Date fields (excluding core fields)
     *
     * @return True/False
     */
    @Transient
    @JsonIgnore
    public boolean getNoKeyDatesExclCore() {
        for (AdColumn column : getNoKeyFieldsExclCore()) {
            if ("DATE".equals(column.getCtype())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return 'true' if has some Date fields in primary key
     *
     * @return True/False
     */
    @Transient
    @JsonIgnore
    public boolean getKeyDates() {
        for (AdColumn column : getKeyFields()) {
            if ("DATE".equals(column.getCtype())) {
                return true;
            }
        }
        return false;
    }

    @Transient
    @JsonIgnore
    public boolean getNotNullConstraints() {
        for (AdColumn column : getKeyFields()) {
            if (column.getMandatory()) {
                return true;
            }
        }
        return false;
    }

    @Transient
    @JsonIgnore
    public boolean getSizeConstraints() {
        for (AdColumn column : getKeyFields()) {
            if (column.isCheckSize()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get transient columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getTransients() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (AdColumnService.SOURCE_TRANSIENT.equals(column.getCsource())) {
                results.add(column);
            }
        }
        return results;
    }

    /**
     * Get table reference columns (AdRefTable)
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<CodeGenColumn> getTableReferences() {
        List<CodeGenColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (!name.equalsIgnoreCase("id_client") && AdReferenceService.BASE_ID_TABLE.equals(column.getIdReference()) && column.getCodeGen() != null) {
                results.add(column.getCodeGen());
            }
        }
        return results;
    }

    @Transient
    @JsonIgnore
    public AdColumn getColumn(String name) {
        for (AdColumn column : getColumns()) {
            if (column.getName().equals(name) || column.getStandardName().equals(name)) {
                return column;
            }
        }
        return null;
    }

    /**
     * Get ImageView columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getImageViewColumns() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (AdReferenceService.BASE_ID_IMAGEVIEW.equals(column.getIdReference()) && column.getName().endsWith("_url")) {
                results.add(column);
            }
        }
        return results;
    }

    /**
     * Get Image columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getImageColumns() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (AdReferenceService.BASE_ID_IMAGE.equals(column.getIdReference())) {
                results.add(column);
            }
        }
        return results;
    }

    @Transient
    @JsonIgnore
    public boolean getImageViews() {
        for (AdColumn column : getColumns()) {
            if (AdReferenceService.BASE_ID_IMAGEVIEW.equals(column.getIdReference()) && column.getName().endsWith("_url")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get File columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getFileColumns() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (AdReferenceService.BASE_ID_FILE.equals(column.getIdReference())) {
                results.add(column);
            }
        }
        return results;
    }

    @Transient
    @JsonIgnore
    public boolean getFileUrls() {
        for (AdColumn column : getColumns()) {
            if (AdReferenceService.BASE_ID_FILEURL.equals(column.getIdReference()) && column.getName().endsWith("_url")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get File/Image columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumn> getFileImageColumns() {
        List<AdColumn> results = new ArrayList<>();
        for (AdColumn column : getColumns()) {
            if (AdReferenceService.BASE_ID_FILE.equals(column.getIdReference()) || AdReferenceService.BASE_ID_IMAGE.equals(column.getIdReference())) {
                results.add(column);
            }
        }
        return results;
    }
}