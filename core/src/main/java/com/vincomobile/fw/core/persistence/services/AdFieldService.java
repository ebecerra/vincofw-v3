package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdMultiSelectValue;
import com.vincomobile.fw.core.persistence.model.AdField;
import org.springframework.data.domain.Page;

/**
 * Created by Devtools.
 * Interface of service of AD_FIELD
 *
 * Date: 19/02/2015
 */
public interface AdFieldService extends BaseService<AdField, String> {

    String FILTER_TYPE_STANDARD         = "STANDARD";
    String FILTER_TYPE_MULTISELECT      = "MULTISELECT";

    /**
     * Reset MultiSelect column for all field in Tab
     *
     * @param idTab Tab identifier
     */
    void resetMultiSelect(String idTab);

    /**
     * List values for multiselect field
     *
     * @param idClient Client identifier
     * @param field Field information
     * @param parentFieldId Parent field identifier
     * @param rowKey Parent rowkey
     * @return Multi select values
     */
    Page<AdMultiSelectValue> listMultiSelectValues(String idClient, AdField field, String parentFieldId, String rowKey);
}


