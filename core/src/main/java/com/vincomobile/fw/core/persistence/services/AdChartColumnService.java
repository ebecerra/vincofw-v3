package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdChartColumn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 09/11/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_chart_column
 */
public interface AdChartColumnService extends BaseService<AdChartColumn, String> {

    /**
     * Retrieves all the columns of the given chart
     *
     * @param idChart Chart Id
     * @return Chart columns
     */
    List<AdChartColumn> findChartColumns(String idChart);
}


