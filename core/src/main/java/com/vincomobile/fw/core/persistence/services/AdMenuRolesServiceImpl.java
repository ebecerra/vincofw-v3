package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.beans.MenuRoleItem;
import com.vincomobile.fw.core.persistence.beans.MenuRoleParam;
import com.vincomobile.fw.core.persistence.model.AdMenuRoles;

import com.vincomobile.fw.core.persistence.model.AdUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_MENU_ROLES
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdMenuRolesServiceImpl extends BaseServiceImpl<AdMenuRoles, String> implements AdMenuRolesService {

    /**
     * Constructor.
     *
     */
    public AdMenuRolesServiceImpl() {
        super(AdMenuRoles.class);
    }

    /**
     * List menu roles by module
     *
     * @param idModule Module identifier
     * @return Menu/Roles for module
     */
    @Override
    public List<AdMenuRoles> listByModule(String idModule) {
        Map filter = new HashMap();
        filter.put("idModule", idModule);
        return findAll(filter);
    }

    /**
     * Get Menu/Role
     *
     * @param idClient Client identifier
     * @param idMenu Menu identifier
     * @param idRole Role identifier
     * @return Menu/Role
     */
    @Override
    public AdMenuRoles getItem(String idClient, String idMenu, String idRole) {
        Map filter = new HashMap();
        filter.put("idClient", idClient);
        filter.put("idMenu", idMenu);
        filter.put("idRole", idRole);
        return findFirst(filter);
    }

    /**
     * Insert/Update role / menu
     *
     * @param user User
     * @param menuRoles Menu list
     * @return Inserted rows
     */
    @Override
    public int saveRoleMenu(AdUser user, MenuRoleParam menuRoles) {
        int count = 0;
        for (MenuRoleItem item : menuRoles.getMenu()) {
            if (item.isSelected()) {
                AdMenuRoles mrItem = getItem(FWConfig.ALL_CLIENT_ID, item.getIdMenu(), menuRoles.getIdRole());
                if (mrItem == null) {
                    count++;
                    mrItem = new AdMenuRoles();
                    mrItem.setIdClient(FWConfig.ALL_CLIENT_ID);
                    mrItem.setIdModule(item.getIdModule());
                    mrItem.setIdRole(menuRoles.getIdRole());
                    mrItem.setIdMenu(item.getIdMenu());
                    mrItem.setActive(true);
                    mrItem.setCreatedBy(user.getIdUser());
                    mrItem.setUpdatedBy(user.getIdUser());
                    save(mrItem);
                } else {
                    mrItem.setIdModule(item.getIdModule());
                    mrItem.setActive(true);
                    mrItem.setUpdatedBy(user.getIdUser());
                    update(mrItem);
                }
            }
        }
        return count;
    }

    /**
     * Remove role / menu
     *
     * @param menuRoles Menu list
     * @return Removed rows
     */
    @Override
    public int removeRoleMenu(MenuRoleParam menuRoles) {
        List<String> idMenus = new ArrayList<>();
        for (MenuRoleItem item : menuRoles.getMenu()) {
            if (!item.isSelected()) {
                idMenus.add(item.getIdMenu());
            }
        }
        Map params = new HashMap();
        params.put("idRole", menuRoles.getIdRole());
        params.put("idMenus", idMenus);
        return applyUpdate("DELETE FROM AdMenuRoles WHERE idRole = :idRole AND idMenu IN :idMenus", params);
    }

}
