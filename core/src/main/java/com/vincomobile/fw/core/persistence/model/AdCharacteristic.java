package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_characteristic
 *
 * Date: 23/01/2016
 */
@Entity
@Table(name = "ad_characteristic")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdCharacteristic extends AdEntityBean {

    private String idCharacteristic;
    private String idTable;
    private String idReference;
    private String idReferenceValue;
    private String name;
    private String description;
    private String ctype;
    private String mode;
    private Boolean translatable;
    private String fileType;
    private Long fileMaxsize;

    private AdTable table;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idCharacteristic;
    }

    @Override
    public void setId(String id) {
            this.idCharacteristic = id;
    }

    @Id
    @Column(name = "id_characteristic")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdCharacteristic() {
        return idCharacteristic;
    }

    public void setIdCharacteristic(String idCharacteristic) {
        this.idCharacteristic = idCharacteristic;
    }

    @Column(name = "id_table")
    @NotNull
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_reference")
    @NotNull
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "id_reference_value")
    public String getIdReferenceValue() {
        return idReferenceValue;
    }

    public void setIdReferenceValue(String idReferenceValue) {
        this.idReferenceValue = idReferenceValue;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "ctype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    @Column(name = "mode", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    @Column(name = "translatable")
    @NotNull
    public Boolean getTranslatable() {
        return translatable;
    }

    public void setTranslatable(Boolean translatable) {
        this.translatable = translatable;
    }

    @Column(name = "file_type", length = 500)
    @Size(min = 1, max = 500)
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    @Column(name = "file_maxsize")
    public Long getFileMaxsize() {
        return fileMaxsize;
    }

    public void setFileMaxsize(Long fileMaxsize) {
        this.fileMaxsize = fileMaxsize;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdCharacteristic)) return false;

        final AdCharacteristic that = (AdCharacteristic) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idCharacteristic == null) && (that.idCharacteristic == null)) || (idCharacteristic != null && idCharacteristic.equals(that.idCharacteristic)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((idReferenceValue == null) && (that.idReferenceValue == null)) || (idReferenceValue != null && idReferenceValue.equals(that.idReferenceValue)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((ctype == null) && (that.ctype == null)) || (ctype != null && ctype.equals(that.ctype)));
        result = result && (((mode == null) && (that.mode == null)) || (mode != null && mode.equals(that.mode)));
        result = result && (((translatable == null) && (that.translatable == null)) || (translatable != null && translatable.equals(that.translatable)));
        result = result && (((fileType == null) && (that.fileType == null)) || (fileType != null && fileType.equals(that.fileType)));
        result = result && (((fileMaxsize == null) && (that.fileMaxsize == null)) || (fileMaxsize != null && fileMaxsize.equals(that.fileMaxsize)));
        return result;
    }

}

