package com.vincomobile.fw.core.persistence.charts;

import com.vincomobile.fw.core.persistence.beans.AdBaseTools;
import com.vincomobile.fw.core.persistence.beans.AdChartInfo;
import com.vincomobile.fw.core.persistence.model.AdChart;
import com.vincomobile.fw.core.persistence.model.AdChartFilter;
import com.vincomobile.fw.core.persistence.model.AdChartLinked;
import com.vincomobile.fw.core.persistence.services.AdChartService;
import com.vincomobile.fw.core.persistence.services.AdReferenceService;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.Datetool;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChartUtils {

    /**
     * Append/Remove conditional expression
     * <p>
     * Example:
     * value = "active = 1 &type[AND status = 'CLOSED']"
     * <p>
     * if "constraints" contains "type"
     *     return "active = 1 AND status = 'CLOSED'"
     * else
     *     return "active = 1"
     *
     * @param value       Expression to evaluate (Expression: &param[value]
     * @param constraints Real constrains
     * @return If "param" is not present in "constraints" "&param[value]" is removed else is replaced by "value"
     */
    public static String replaceConditional(String value, Map<String, Object> constraints) {
        Pattern pattern = Pattern.compile("\\&(.*?)\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(value);
        if (matcher.find()) {
            int groupCount = matcher.groupCount();
            if (groupCount == 2) {
                int start = matcher.start(0), end = matcher.end(0);
                String paramName = value.substring(matcher.start(1), matcher.end(1)).trim();
                String replaced = " ";
                if (constraints.containsKey(paramName)) {
                    int end2 = matcher.end(2);
                    replaced = (end2 >= value.length() ? value.substring(matcher.start(2)) : value.substring(matcher.start(2), end2)).trim();
                }
                value = value.substring(0, start) + replaced + value.substring(end);
            }
        }
        return value;
    }

    /**
     * Append/Remove all conditional expression
     *
     * @param value       Expression to evaluate
     * @param constraints Real constrains
     * @return Replaced Expression
     */
    public static String appendConditionalExpression(String value, Map<String, Object> constraints) {
        String result = "";
        while (!result.equals(value)) {
            result = value;
            value = replaceConditional(value, constraints);
        }
        return value;
    }

    /**
     * Replace ranged parameters
     *
     * @param where       WHERE condition
     * @param constraints Constraints
     * @param filters     Filters list
     * @return Replaced WHERE
     */
    public static String replaceRangedParameters(String where, Map<String, Object> constraints, List<AdChartFilter> filters) {
        // Expand ranged filters
        for (AdChartFilter filter : filters) {
            if (filter.getRanged()) {
                String range = (String) constraints.get(filter.getName());
                if (!Converter.isEmpty(range)) {
                    AdBaseTools.buildWhereCriteriaBetween(constraints, AdChart.class, filter.getName(), range);
                }
            }
        }
        // Replace range
        Set<String> keys = constraints.keySet();
        for (String key : keys) {
            if (key.endsWith(".begin")) {
                String field = key.substring(0, key.indexOf(".begin"));
                AdChartFilter filter = getFilter(filters, field);
                if (filter != null) {
                    where = where.replaceAll("\\$" + field + "_from", getFilterValue(filter, (String) constraints.get(field + ".begin")));
                    where = where.replaceAll("\\$" + field + "_to", getFilterValue(filter, (String) constraints.get(field + ".end")));
                }
            }
        }
        return where;
    }

    /**
     * Replace single parameters
     *
     * @param where        WHERE condition
     * @param constraints  Constraints
     * @param chartLinkeds Chart linkeds
     * @return Replaced WHERE
     */
    public static String replaceSingleParameters(String where, Map<String, Object> constraints, List<AdChartLinked> chartLinkeds) {
        Set<String> keys = constraints.keySet();
        for (String key : keys) {
            // Replace linked parameters
            boolean replacedLinked = false;
            for (AdChartLinked linked : chartLinkeds) {
                if (key.equals(linked.getLinked().getKeyField())) {
                    where = where.replaceAll("\\$" + key, getFilterValue(linked.getLinked(), (String) constraints.get(key)));
                    replacedLinked = true;
                }
            }
            if (!replacedLinked && constraints.get(key) instanceof String) {
                where = where.replaceAll("\\$" + key, Converter.getSQLString((String) constraints.get(key), true));
            }
        }
        return where;
    }

    /**
     * Replace single parameters
     *
     * @param where        WHERE condition
     * @param constraints  Constraints
     * @param chartLinkeds Chart linkeds
     * @return Replaced WHERE
     */
    public static String replaceDateFilterParameters(String where, Map<String, Object> constraints, List<AdChartLinked> chartLinkeds) {
        Set<String> keys = constraints.keySet();
        for (String key : keys) {
            if (where.contains("@" + key + "Filter")) {
                for (AdChartLinked linked : chartLinkeds) {
                    AdChart chart = linked.getLinked();
                    if (key.equals(chart.getKeyField())) {
                        Date begin = Converter.getDate((String) constraints.get(chart.getNameField() + ".begin"));
                        Date end = Converter.getDate((String) constraints.get(chart.getNameField() + ".end"));
                        if (begin != null && end != null) {
                            int days = Datetool.getDaysBetween(begin, end);
                            if (days > AdChartService.WEEK_MAX_DAYS && days < AdChartService.MONTH_MAX_DAYS) {
                                where = where.replaceAll("@" + key + "Filter", "week");
                                where = where.replaceAll("\\$" + key, Converter.getSQLString((String) constraints.get(key)));
                            } else {
                                if (days <= AdChartService.MONTH_MAX_DAYS) {
                                    where = where.replaceAll("@" + key + "Filter", "date");
                                    where = where.replaceAll("\\$" + key, getFilterValue(chart, (String) constraints.get(key)));
                                } else {
                                    where = where.replaceAll("@" + key + "Filter", "month");
                                    String month = ((String) constraints.get(key)).substring(4);
                                    where = where.replaceAll("\\$" + key, "" + Converter.getInt(month));
                                }
                            }
                        }
                    }
                }
            }
        }
        return where;
    }

    /**
     * Get SQL filter value
     *
     * @param filter Filter information
     * @param value  Value
     * @return SQL value
     */
    public static String getFilterValue(AdChartFilter filter, String value) {
        switch (filter.getReference().getRtype()) {
            case AdReferenceService.RTYPE_DATE:
                return Converter.getSQLDate(Converter.getDate(value, "dd/MM/yyyy"));
            case AdReferenceService.RTYPE_INTEGER:
                return Converter.getSQLLong(Converter.getLong(value));
            case AdReferenceService.RTYPE_NUMBER:
                return Converter.getSQLFloat(Converter.getFloat(value));
            case AdReferenceService.RTYPE_STRING:
                return Converter.getSQLString(value);
            default:
                return "";
        }
    }

    /**
     * Get SQL filter value
     *
     * @param chart Filter information
     * @param value Value
     * @return SQL value
     */
    public static String getFilterValue(AdChart chart, String value) {
        switch (chart.getReference().getRtype()) {
            case AdReferenceService.RTYPE_DATE:
                return Converter.getSQLDate(Converter.getDate(value, "yyyyMMdd"));
            case AdReferenceService.RTYPE_INTEGER:
                return Converter.getSQLLong(Converter.getLong(value));
            case AdReferenceService.RTYPE_NUMBER:
                return Converter.getSQLFloat(Converter.getFloat(value));
            case AdReferenceService.RTYPE_STRING:
                return Converter.getSQLString(value);
            default:
                return "";
        }
    }

    /**
     * Replace parameters for value
     *
     * @param idClient  Client identifier
     * @param value     Value
     * @param chartInfo Runtime chart information
     * @return Replaced value
     */
    public static String replaceValueParameters(String idClient, String value, AdChartInfo chartInfo) {
        if (value == null)
            return null;
        value = value.replaceAll("@days", "" + chartInfo.getDays()).replaceAll("@moths", "" + chartInfo.getMonths());
        return AdBaseTools.replacePreference(idClient, value);
    }

    /**
     * Filter from list
     *
     * @param filters Filters list
     * @param name    Filter name
     * @return Filter
     */
    public static AdChartFilter getFilter(List<AdChartFilter> filters, String name) {
        for (AdChartFilter filter : filters) {
            if (filter.getName().equals(name)) {
                return filter;
            }
        }
        return null;
    }

}
