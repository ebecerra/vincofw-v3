package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdApproval;
import com.vincomobile.fw.core.persistence.model.AdUser;

import java.util.List;

/**
 * Created by Vincomobile FW on 23/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_approval
 */
public interface AdApprovalService extends BaseService<AdApproval, String> {

    String ACTION_PROCESS           = "PROCESS";
    String ACTION_TABLE             = "TABLE";
    String ACTION_TABLE_ACTION      = "TABLE_ACTION";
    String ACTION_CUSTOM            = "CUSTOM";

    /**
     * Save approvals
     *
     * @param user Login user
     * @param approvals Approvals list
     */
    void saveApprovals(AdUser user, List<AdApproval> approvals);
}


