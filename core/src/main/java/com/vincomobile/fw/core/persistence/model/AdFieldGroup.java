package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Created by Devtools.
 * Modelo de AD_FIELD_GROUP
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_field_group")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdFieldGroup extends AdEntityBean {

    private String idFieldGroup;
    private String name;
    private Boolean collapsed;
    private Long columns;
    private String displaylogic;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idFieldGroup;
    }

    @Override
    public void setId(String id) {
            this.idFieldGroup = id;
    }

    @Id
    @Column(name = "id_field_group")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdFieldGroup() {
        return idFieldGroup;
    }

    public void setIdFieldGroup(String idFieldGroup) {
        this.idFieldGroup = idFieldGroup;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "collapsed")
    @NotNull
    public Boolean getCollapsed() {
        return collapsed;
    }

    public void setCollapsed(Boolean collapsed) {
        this.collapsed = collapsed;
    }

    @Column(name = "columns")
    @NotNull
    public Long getColumns() {
        return columns;
    }

    public void setColumns(Long columns) {
        this.columns = columns;
    }

    @Column(name = "displaylogic", length = 2000)
    @Size(min = 1, max = 2000)
    public String getDisplaylogic() {
        return displaylogic;
    }

    public void setDisplaylogic(String displaylogic) {
        this.displaylogic = displaylogic;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdFieldGroup)) return false;

        final AdFieldGroup that = (AdFieldGroup) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idFieldGroup == null) && (that.idFieldGroup == null)) || (idFieldGroup != null && idFieldGroup.equals(that.idFieldGroup)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((collapsed == null) && (that.collapsed == null)) || (collapsed != null && collapsed.equals(that.collapsed)));
        result = result && (((columns == null) && (that.columns == null)) || (columns != null && columns.equals(that.columns)));
        result = result && (((displaylogic == null) && (that.displaylogic == null)) || (displaylogic != null && displaylogic.equals(that.displaylogic)));
        return result;
    }

    @Transient
    public String getCaption() {
        return "[" + module.getRestPath() + "] " + name;
    }

}

