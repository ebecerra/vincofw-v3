package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRole;

/**
 * Created by Devtools.
 * Interface of service of AD_ROLE
 *
 * Date: 19/02/2015
 */
public interface AdRoleService extends BaseService<AdRole, String> {

}


