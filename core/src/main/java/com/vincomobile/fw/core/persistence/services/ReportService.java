package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcess;
import net.sf.jasperreports.engine.JRDataSource;

import java.io.File;
import java.util.HashMap;

public interface ReportService {

    // Format types
    String REPORT_FORMAT_HTML   = "html";           // Formato HTML
    String REPORT_FORMAT_PDF    = "pdf";            // Formato PDF
    String REPORT_FORMAT_WORD   = "doc";            // Formato WORD
    String REPORT_FORMAT_EXCEL  = "xls";            // Formato Excel

    String PREF_HEADER_COMPANY  = "ReportHeaderCompany";
    String PREF_HEADER_EMAIL    = "ReportHeaderEmail";
    String PREF_HEADER_FAX      = "ReportHeaderFax";
    String PREF_HEADER_PHONE    = "ReportHeaderPhone";
    String PREF_HEADER_LOGO     = "ReportHeaderLogo";

    /**
     * Returns a report
     *
     * @param report      Process with the report data
     * @param format      Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @return Report serialized to a byte array
     */
    byte[] getReport(AdProcess report, String format, String idClient, String idLanguage, String constraints, String lang, String username);

    /**
     * Returns a report
     *
     * @param reportName  Name of the report to generate
     * @param format       Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @param dataSource  Datasource
     * @param extraParams Extra parameters to pass to the report
     * @return Report serialized to a byte array
     */
    byte[] getReport(String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username, JRDataSource dataSource, HashMap extraParams);

    /**
     * Returns a report
     *
     * @param reportName  Name of the report to generate
     * @param format      Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @return Report serialized to a byte array
     */
    byte[] getReport(String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username);

    /**
     * Generates an error report with information about the error
     *
     * @param format      Report format
     * @param error       Error message
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param lang        Report language
     * @param username    Username
     * @param report      Report
     * @param constraints Extra parameters to pass to the report
     * @return Report serialized to a byte array
     */
    byte[] getReportError(String format, String error, String idClient, String idLanguage, String lang, String username, String report, String constraints);

    /**
     * Generates a report to a file
     *
     * @param fileName    File name to generate the report to
     * @param reportName  Name of the report to generate
     * @param format      Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @return Returns true if the report was generated OK. False otherwise
     */
    boolean getFileReport(String fileName, String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username);

    boolean getFileReport(File file, String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username);

}

