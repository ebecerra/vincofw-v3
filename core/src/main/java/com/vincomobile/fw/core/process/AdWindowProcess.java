package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.*;
import com.vincomobile.fw.core.persistence.services.AdColumnService;
import com.vincomobile.fw.core.persistence.services.AdFieldService;
import com.vincomobile.fw.core.persistence.services.AdTabService;
import com.vincomobile.fw.core.persistence.services.AdWindowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AdWindowProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdWindowProcess service = new AdWindowProcess();

    @Autowired
    AdTabService adTabService;

    @Autowired
    AdColumnService adColumnService;

    @Autowired
    AdFieldService adFieldService;

    @Autowired
    AdWindowService adWindowService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Generates the fields of the associated table that are not created yet
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void genFields(String idProcessExec) {
        AdProcessParam tabParam = getParam("idTab");
        if (tabParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idTab"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdTab tab = adTabService.findById(tabParam.getValue().toString());
        Map<String, Object> filter = new HashMap<>();
        filter.put("idTable", tab.getTable().getIdTable());
        List<AdColumn> columns = adColumnService.findAll(filter);
        filter = new HashMap<>();
        filter.put("idTab", tab.getId());
        List<AdField> fields = adFieldService.findAll(filter);
        filter = new HashMap<>();
        filter.put("idModule", tab.getIdModule());
        long seqNo = 0;
        for (AdField current : fields) {
            if (current.getSeqno() < 1000L && seqNo < current.getSeqno())
                seqNo = current.getSeqno();
        }
        for (AdColumn column : columns) {
            boolean present = false;
            for (AdField current : fields) {
                if (current.getIdColumn().equals(column.getId())) {
                    present = true;
                    break;
                }
            }
            if (!present) {
                AdField newField = new AdField();
                newField.setIdClient(tab.getIdClient());
                newField.setIdModule(tab.getIdModule());
                newField.setActive(true);
                newField.setIdTab(tab.getId());
                newField.setIdColumn(column.getId());
                newField.setName(column.getName());
                newField.setCaption(column.getName());
                newField.setReadonly(false);
                if (column.getPrimaryKey()) {
                    newField.setSeqno(1000L);
                } else if ("created".equals(column.getName())) {
                    newField.setReadonly(true);
                    newField.setSeqno(1010L);
                    newField.setCaption("Creado");
                } else if ("updated".equals(column.getName())) {
                    newField.setReadonly(true);
                    newField.setSeqno(1020L);
                    newField.setCaption("Actualizado");
                } else if ("created_by".equals(column.getName())) {
                    newField.setReadonly(true);
                    newField.setSeqno(1030L);
                    newField.setCaption("Creado por");
                } else if ("updated_by".equals(column.getName())) {
                    newField.setReadonly(true);
                    newField.setSeqno(1040L);
                    newField.setCaption("Actualizado por");
                } else {
                    seqNo += 10;
                    newField.setSeqno(seqNo);
                }
                if ("id_client".equals(column.getName())) {
                    newField.setCaption("Cliente");
                }
                if ("name".equals(column.getName())) {
                    newField.setCaption("Nombre");
                }
                if ("active".equals(column.getName())) {
                    newField.setCaption("Activo");
                }
                if ("description".equals(column.getName())) {
                    newField.setCaption("Descripción");
                }
                if ("seqno".equals(column.getName())) {
                    newField.setCaption("Orden");
                }
                newField.setGridSeqno((long) 0);
                newField.setDisplayed(!("created".equals(column.getName()) || "updated".equals(column.getName()) ||"created_by".equals(column.getName()) || "updated_by".equals(column.getName()) || column.getPrimaryKey()));
                newField.setShowingrid("active".equals(column.getName()) || "name".equals(column.getName()) || "description".equals(column.getName()));
                newField.setStartnewrow(false);
                newField.setShowinstatusbar(false);
                newField.setUsedInChild(false);
                newField.setSpan((long) 1);
                newField.setFilterRange(false);
                newField.setFilterType(AdFieldService.FILTER_TYPE_STANDARD);
                newField.setSortable(AdColumnService.SOURCE_DATABASE.equals(column.getCsource()));
                adFieldService.save(newField);
                info(idProcessExec, getMessage("AD_ProcessWindowSaveField", column.getName()));
                seqNo += 10;
            } else {
                warn(idProcessExec, getMessage("AD_ProcessWindowSaveExists", column.getName()));
            }
        }
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Generates the fields of the associated table that are not created yet
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void copyFrom(String idProcessExec) {
        AdProcessParam windowTargetParam = getParam("idWindowTarget");
        if (windowTargetParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idWindowTarget"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdProcessParam windowParam = getParam("idWindow");
        if (windowParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idWindow"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdProcessParam tabParam = getParam("idTab");
        if (tabParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idTab"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        AdTab sourceTab = adTabService.findById(tabParam.getValue().toString());
        AdWindow targetWindow = adWindowService.findById(windowTargetParam.getValue().toString());

        AdTab duplicatedTab = (AdTab) sourceTab.clone();
        duplicatedTab.setIdWindow(targetWindow.getIdWindow());
        adTabService.save(duplicatedTab);
        info(idProcessExec, getMessage("AD_ProcessDuplicateTabTabInfo", duplicatedTab.getName()));
        for (AdField field: sourceTab.getFields()){
            AdField cloned = (AdField) field.clone();
            cloned.setIdTab(duplicatedTab.getIdTab());
            adFieldService.save(cloned);
            info(idProcessExec, getMessage("AD_ProcessDuplicateTabFieldInfo", field.getCaption()));
        }

        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

}