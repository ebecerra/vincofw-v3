package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de AD_TAB
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_tab")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdTab extends AdEntityBean implements Cloneable {

    private String idTab;
    private String idWindow;
    private String idTable;
    private String name;
    private Long tablevel;
    private Long columns;
    private Long sortColumns;
    private Long seqno;
    private String uipattern;
    private String ttype;
    private String sqlwhere;
    private String sqlorderby;
    private String displaylogic;
    private String command;
    private String idParentTable;
    private String viewMode;
    private Boolean viewDefault;
    private String groupMode;

    private AdTable table;
    private List<AdField> fields;
    private List<AdChart> charts;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idTab;
    }

    @Override
    public void setId(String id) {
            this.idTab = id;
    }

    @Id
    @Column(name = "id_tab")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdTab() {
        return idTab;
    }

    public void setIdTab(String idTab) {
        this.idTab = idTab;
    }

    @Column(name = "id_window")
    @NotNull
    public String getIdWindow() {
        return idWindow;
    }

    public void setIdWindow(String idWindow) {
        this.idWindow = idWindow;
    }

    @Column(name = "id_table")
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "tablevel")
    @NotNull
    public Long getTablevel() {
        return tablevel;
    }

    public void setTablevel(Long tablevel) {
        this.tablevel = tablevel;
    }

    @Column(name = "columns")
    @NotNull
    public Long getColumns() {
        return columns;
    }

    public void setColumns(Long columns) {
        this.columns = columns;
    }

    @Column(name = "sort_columns")
    public Long getSortColumns() {
        return sortColumns;
    }

    public void setSortColumns(Long sortColumns) {
        this.sortColumns = sortColumns;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "uipattern", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getUipattern() {
        return uipattern;
    }

    public void setUipattern(String uipattern) {
        this.uipattern = uipattern;
    }

    @Column(name = "group_mode", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGroupMode() {
        return groupMode;
    }

    public void setGroupMode(String groupMode) {
        this.groupMode = groupMode;
    }

    @Column(name = "ttype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getTtype() {
        return ttype;
    }

    public void setTtype(String ttype) {
        this.ttype = ttype;
    }

    @Column(name = "sqlwhere", columnDefinition = "TEXT")
    public String getSqlwhere() {
        return sqlwhere;
    }

    public void setSqlwhere(String sqlwhere) {
        this.sqlwhere = sqlwhere;
    }

    @Column(name = "sqlorderby", columnDefinition = "TEXT")
    public String getSqlorderby() {
        return sqlorderby;
    }

    public void setSqlorderby(String sqlorderby) {
        this.sqlorderby = sqlorderby;
    }

    @Column(name = "displaylogic", columnDefinition = "TEXT")
    public String getDisplaylogic() {
        return displaylogic;
    }

    public void setDisplaylogic(String displaylogic) {
        this.displaylogic = displaylogic;
    }

    @Column(name = "command", length = 100)
    @Size(min = 1, max = 100)
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Column(name = "id_parent_table", length = 32)
    @Size(min = 1, max = 32)
    public String getIdParentTable() {
        return idParentTable;
    }

    public void setIdParentTable(String idParentTable) {
        this.idParentTable = idParentTable;
    }

    @Column(name = "view_mode", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getViewMode() {
        return viewMode;
    }

    public void setViewMode(String viewMode) {
        this.viewMode = viewMode;
    }

    @Column(name = "view_default")
    @NotNull
    public Boolean getViewDefault() {
        return viewDefault;
    }

    public void setViewDefault(Boolean viewDefault) {
        this.viewDefault = viewDefault;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    @OneToMany
    @JoinColumn(name = "id_tab", referencedColumnName = "id_tab", insertable = false, updatable = false)
    public List<AdField> getFields() {
        return fields;
    }

    public void setFields(List<AdField> fields) {
        this.fields = fields;
    }

    @OneToMany
    @JoinColumn(name = "id_tab", referencedColumnName = "id_tab", insertable = false, updatable = false)
    public List<AdChart> getCharts() {
        return charts;
    }

    public void setCharts(List<AdChart> charts) {
        this.charts = charts;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdTab)) return false;

        final AdTab that = (AdTab) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idTab == null) && (that.idTab == null)) || (idTab != null && idTab.equals(that.idTab)));
        result = result && (((idWindow == null) && (that.idWindow == null)) || (idWindow != null && idWindow.equals(that.idWindow)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idParentTable == null) && (that.idParentTable == null)) || (idParentTable != null && idParentTable.equals(that.idParentTable)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((tablevel == null) && (that.tablevel == null)) || (tablevel != null && tablevel.equals(that.tablevel)));
        result = result && (((columns == null) && (that.columns == null)) || (columns != null && columns.equals(that.columns)));
        result = result && (((sortColumns == null) && (that.sortColumns == null)) || (sortColumns != null && sortColumns.equals(that.sortColumns)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((uipattern == null) && (that.uipattern == null)) || (uipattern != null && uipattern.equals(that.uipattern)));
        result = result && (((ttype == null) && (that.ttype == null)) || (ttype != null && ttype.equals(that.ttype)));
        result = result && (((sqlwhere == null) && (that.sqlwhere == null)) || (sqlwhere != null && sqlwhere.equals(that.sqlwhere)));
        result = result && (((sqlorderby == null) && (that.sqlorderby == null)) || (sqlorderby != null && sqlorderby.equals(that.sqlorderby)));
        result = result && (((displaylogic == null) && (that.displaylogic == null)) || (displaylogic != null && displaylogic.equals(that.displaylogic)));
        result = result && (((command == null) && (that.command == null)) || (command != null && command.equals(that.command)));
        result = result && (((viewMode == null) && (that.viewMode == null)) || (viewMode != null && viewMode.equals(that.viewMode)));
        result = result && (((viewDefault == null) && (that.viewDefault == null)) || (viewDefault != null && viewDefault.equals(that.viewDefault)));
        result = result && (((groupMode == null) && (that.groupMode == null)) || (groupMode != null && groupMode.equals(that.groupMode)));
        return result;
    }

    @Override
    public Object clone(){
        AdTab newTab = new AdTab();
        // Reference keys
        newTab.setIdClient(this.getIdClient());
        newTab.setIdModule(this.getIdModule());
        newTab.setIdWindow(this.getIdWindow());
        newTab.setIdTable(this.getIdTable());
        newTab.setIdParentTable(this.getIdParentTable());

        newTab.setActive(this.getActive());
        newTab.setName(this.getName());
        newTab.setTablevel(this.getTablevel());
        newTab.setColumns(this.getColumns());
        newTab.setSortColumns(this.getSortColumns());
        newTab.setSeqno(this.getSeqno());
        newTab.setUipattern(this.getUipattern());
        newTab.setTtype(this.getTtype());
        newTab.setSqlwhere(this.getSqlwhere());
        newTab.setSqlorderby(this.getSqlorderby());
        newTab.setDisplaylogic(this.getDisplaylogic());
        newTab.setCommand(this.getCommand());
        newTab.setViewMode(this.getViewMode());
        newTab.setViewDefault(this.getViewDefault());
        newTab.setGroupMode(this.getGroupMode());
        return newTab;
    }

}

