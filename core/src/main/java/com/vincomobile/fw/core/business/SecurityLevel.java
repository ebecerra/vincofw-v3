package com.vincomobile.fw.core.business;

public class SecurityLevel {

    long sizeMin;
    long sizeMax;
    long storeCount;
    long validDays;

    public SecurityLevel(long sizeMin, long sizeMax, long storeCount, long validDays) {
        this.sizeMin = sizeMin;
        this.sizeMax = sizeMax;
        this.storeCount = storeCount;
        this.validDays = validDays;
    }

    public long getSizeMin() {
        return sizeMin;
    }

    public long getSizeMax() {
        return sizeMax;
    }

    public long getStoreCount() {
        return storeCount;
    }

    public long getValidDays() {
        return validDays;
    }
}
