package com.vincomobile.fw.core.persistence.beans;

import java.lang.reflect.Method;

public class ProcessMethod {
    Method method;
    boolean proxy;
    Object target;

    public ProcessMethod(Method method, Object target) {
        this.method = method;
        this.target = target;
        this.proxy = true;
    }

    public ProcessMethod(Method method) {
        this.method = method;
        this.target = null;
        this.proxy = false;
    }

    public Method getMethod() {
        return method;
    }

    public boolean isProxy() {
        return proxy;
    }

    public Object getTarget() {
        return target;
    }
}
