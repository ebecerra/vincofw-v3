package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdBaseTools;
import com.vincomobile.fw.core.persistence.beans.AdExportCache;
import com.vincomobile.fw.core.persistence.cache.CacheTable;
import com.vincomobile.fw.core.persistence.model.AdEntityBean;
import com.vincomobile.fw.core.persistence.model.AdField;
import com.vincomobile.fw.core.persistence.model.AdRefTable;
import com.vincomobile.fw.core.persistence.model.EntityBean;
import com.vincomobile.fw.core.tools.ExcelUtil;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true)
public class AdExportServiceImpl extends BaseServiceImpl<AdEntityBean, String> implements AdExportService {
    private Logger logger = LoggerFactory.getLogger(AdExportServiceImpl.class);

    @Autowired
    AdTranslationService translationService;

    @Autowired
    AdTabService tabService;

    @Autowired
    AdRefListService refListService;

    @Autowired
    AdRefTableService refTableService;

    /**
     * Constructor.
     */
    public AdExportServiceImpl() {
        super(null);
    }

    /**
     * Export associated table data to file
     *
     * @param constraints Filter request
     * @param sort Sort criteria
     * @param idTab Tab being exported
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idLanguage Language identifier
     * @param service Service manager
     * @return Associated table data (currently CSV formatted file)
     */
    @Override
    public byte[] getExport(String constraints, Sort sort, String idTab, String idClient, String idUser, String idLanguage, BaseService service) throws Exception {
        String tableName = service.getTypeTableName();
        CacheTable table = getCacheTable(tableName);
        logger.debug("Export table: " + table.getTable().getStandardName());
        Map<String, AdExportCache> tableDirCache = new HashMap<>();
        Map<String, String> fieldCaptions = translationService.listFieldCaptions(idClient, idLanguage, table.getTable().getIdTable());
        List<AdField> fields = tabService.listGridFields(idClient, idTab, idUser);
        // Set columns captions and widths
        String[] captions = new String[fields.size()];
        int[] widths = new int[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            AdField field = fields.get(i);
            captions[i] = fieldCaptions.get(tableName + "$" + field.getColumn().getName());
            String rtype = field.getColumn().getReference().getRtype();
            switch (rtype) {
                case AdReferenceService.RTYPE_LIST:
                    widths[i] = 35;
                    break;
                case AdReferenceService.RTYPE_DATE:
                case AdReferenceService.RTYPE_TIMESTAMP:
                    widths[i] = 20;
                    break;
                case AdReferenceService.RTYPE_NUMBER:
                case AdReferenceService.RTYPE_INTEGER:
                case AdReferenceService.RTYPE_YESNO:
                    widths[i] = 15;
                    break;
                case AdReferenceService.RTYPE_TABLE:
                case AdReferenceService.RTYPE_TABLEDIR:
                case AdReferenceService.RTYPE_SEARCH:
                    widths[i] = 40;
                    break;
                default:
                    widths[i] = 50;
                    break;
            }
        }

        // Create Excel
        ExcelUtil excel = new ExcelUtil();
        excel.initBook("Data");
        excel.createRowCaptions(0, captions);
        excel.setWidths(widths);

        // Load and export table rows
        int rowIndx = 1, page = 1;
        Page items;
        Map<String, String> refTableNames = new HashMap<>();
        do {
            PageSearch pageReq = new PageSearch(page++, BaseService.PAGE_SIZE_1000, sort);
            pageReq.parseConstraints(constraints, service.getAssociatedType());
            items = service.findAll(pageReq);
            for (Object item : items.getContent()) {
                Row row = excel.getSheet().createRow(rowIndx++);
                for (int i = 0; i < fields.size(); i++) {
                    AdField field = fields.get(i);
                    String rtype = field.getColumn().getReference().getRtype();
                    Object value = ((EntityBean) item).getPropertyValue(field.getColumn().getStandardName());
                    CellStyle style = excel.getCsNormal();
                    switch (rtype) {
                        case AdReferenceService.RTYPE_LIST:
                            value = refListService.getName(idClient, field.getColumn().getIdReferenceValue(), (String) value);
                            excel.createCell(row, i, value, excel.getCsNormal());
                            break;
                        case AdReferenceService.RTYPE_DATE:
                        case AdReferenceService.RTYPE_TIMESTAMP:
                            style = excel.getCsNormalDate();
                            break;
                        case AdReferenceService.RTYPE_NUMBER:
                            style = excel.getCsNormalAmount();
                            break;
                        case AdReferenceService.RTYPE_TABLE:
                        case AdReferenceService.RTYPE_TABLEDIR:
                        case AdReferenceService.RTYPE_SEARCH:
                            if (value != null) {
                                String columnName = field.getColumn().getName();
                                boolean converted = false;
                                if (columnName.startsWith("id_")) {
                                    // Try to read referenced table from object
                                    String fieldName = field.getColumn().getName().substring(3);
                                    String displayName = refTableNames.get(fieldName);
                                    if (displayName == null) {
                                        AdRefTable refTable = refTableService.getRefTableByReference(field.getColumn().getIdReferenceValue());
                                        displayName = refTable.getDisplay().getStandardName();
                                        refTableNames.put(fieldName, displayName);
                                    }
                                    if (((EntityBean) item).hasProperty(fieldName)) {
                                        EntityBean bean = (EntityBean) ((EntityBean) item).getPropertyValue(fieldName);
                                        if (bean != null) {
                                            value = bean.getPropertyValue(displayName);
                                            converted = true;
                                        }
                                    }
                                }
                                if (!converted && rtype.equals(AdReferenceService.RTYPE_TABLEDIR)) {
                                    // Load table values from database
                                    AdExportCache cache = tableDirCache.get(columnName);
                                    if (cache == null) {
                                        cache = loadTableDir(idClient, field.getColumn().getIdReferenceValue());
                                        tableDirCache.put(columnName, cache);
                                    }
                                    value = cache.getValue((String) value);
                                }
                            }
                            break;
                    }
                    if (value != null) {
                        excel.createCell(row, i, value, style);
                    }
                }
            }
        } while (items.getContent().size() == BaseService.PAGE_SIZE_1000);

        // excel.closeExcel(File.createTempFile("Export_" + table.getTable().getStandardName(), ".xls"));
        return excel.getBytes();
    }

    /**
     * Load table content to memory cache
     *
     * @param idClient TableDir reference identifier
     * @param idReference TableDir reference identifier
     * @return Table Cache
     */
    private AdExportCache loadTableDir(String idClient, String idReference) {
        Map<String, String> values = new HashMap<>();
        AdRefTable refTable = refTableService.getRefTableByReference(idReference);
        Query query = entityManager.createNativeQuery(
                "SELECT " + refTable.getKey().getName() + ", " + refTable.getDisplay().getName() + " " +
                "FROM " + refTable.getTable().getName() + " " +
                "WHERE id_client IN :idClient"
        );
        query.setParameter("idClient", getClientFilter(idClient));
        List<Object[]> items = query.getResultList();
        for (Object[] item : items) {
            values.put(AdBaseTools.getString(item, 0), AdBaseTools.getString(item, 1));
        }
        return new AdExportCache(values);
    }

}
