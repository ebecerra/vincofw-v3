package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdFile;

import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_file
 *
 * Date: 23/01/2016
 */
public interface AdFileService extends BaseService<AdFile, String> {

    /**
     * Get files for table row
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     * @return File list
     */
    List<AdFile> getFiles(String idTable, String idRow);
}


