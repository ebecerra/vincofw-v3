package com.vincomobile.fw.core.business;

public class ValidationError {

    public static final String TYPE_FIELD   = "FIELD";
    public static final String TYPE_GLOBAL  = "GLOBAL";

    public static final String CODE_MESSAGE             = "Message";
    public static final String CODE_TEXT                = "Text";
    public static final String CODE_NOT_NULL            = "NotNull";
    public static final String CODE_EMAIL               = "Email";
    public static final String CODE_SIZE                = "Size";
    public static final String CODE_UNIQUE              = "Unique";
    public static final String CODE_FK_NOT_FOUND        = "FkNotFound";
    public static final String CODE_FK_NOT_DELETE       = "FkNotDelete";
    public static final String CODE_DATA_TRUNCATION     = "DataTruncation";
    public static final String CODE_UPLOAD_FILE         = "UploadFile";
    public static final String CODE_UPLOAD_FILE_SIZE    = "UploadFileSize";
    public static final String CODE_CHECK_VALUE         = "CheckValue";
    public static final String CODE_INVALID_VALUE       = "InvalidValue";
    public static final String CODE_UNKNOW              = "Unknow";

    String type;
    String field;
    String code;
    String message;
    String extendedCode;

    public ValidationError(String type, String field, String code, String message) {
        this.type = type;
        this.field = field;
        this.code = code;
        this.message = message;
        this.extendedCode = null;
    }

    public ValidationError(String type, String field, String code, String message, String extendedCode) {
        this.type = type;
        this.field = field;
        this.code = code;
        this.message = message;
        this.extendedCode = extendedCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExtendedCode() {
        return extendedCode;
    }

    public void setExtendedCode(String extendedCode) {
        this.extendedCode = extendedCode;
    }
}
