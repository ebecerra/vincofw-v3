package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.core.persistence.beans.UserInfo;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de AD_USER
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_user")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdUser extends AdEntityBean {

    private String idUser;
    private String name;
    private String username;
    private String password;
    private Date passwordUpdate;
    private String phone;
    private String phoneMobile;
    private String email;
    private String fax;
    private String defaultIdLanguage;
    private String defaultIdRole;
    private String photo;
    private String firebaseToken;

    private String photoUrl;
    private AdRole defaultRole;
    private List<AdUserRoles> userRoles;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idUser;
    }

    @Override
    public void setId(String id) {
            this.idUser = id;
    }

    @Id
    @Column(name = "id_user")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "username", length = 20, unique = true)
    @NotNull
    @Size(min = 1, max = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", length = 45)
    @Size(min = 1, max = 45)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "password_update", length = 45)
    @NotNull
    public Date getPasswordUpdate() {
        return passwordUpdate;
    }

    public void setPasswordUpdate(Date passwordUpdate) {
        this.passwordUpdate = passwordUpdate;
    }

    @Column(name = "phone", length = 45)
    @Size(min = 1, max = 45)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "phone_mobile", length = 45)
    @Size(min = 1, max = 45)
    public String getPhoneMobile() {
        return phoneMobile;
    }

    public void setPhoneMobile(String phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    @Column(name = "email", length = 150, unique = true)
    @Size(min = 1, max = 150)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "fax", length = 45)
    @Size(min = 1, max = 45)
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name = "default_id_language")
    @NotNull
    public String getDefaultIdLanguage() {
        return defaultIdLanguage;
    }

    public void setDefaultIdLanguage(String defaultIdLanguage) {
        this.defaultIdLanguage = defaultIdLanguage;
    }

    @Column(name = "default_id_role")
    @NotNull
    public String getDefaultIdRole() {
        return defaultIdRole;
    }

    public void setDefaultIdRole(String defaultIdRole) {
        this.defaultIdRole = defaultIdRole;
    }

    @Column(name = "photo", length = 45)
    @Size(min = 1, max = 45)
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Column(name = "firebase_token", length = 32)
    @Size(min = 1, max = 32)
    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "default_id_role", referencedColumnName = "id_role", insertable = false, updatable = false)
    public AdRole getDefaultRole() {
        return defaultRole;
    }

    public void setDefaultRole(AdRole defaultRole) {
        this.defaultRole = defaultRole;
    }

    @OneToMany
    @JoinColumn(name = "id_user", referencedColumnName = "id_user", insertable = false, updatable = false)
    @JsonIgnore
    public List<AdUserRoles> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<AdUserRoles> userRoles) {
        this.userRoles = userRoles;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdUser)) return false;

        final AdUser that = (AdUser) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((username == null) && (that.username == null)) || (username != null && username.equals(that.username)));
        result = result && (((password == null) && (that.password == null)) || (password != null && password.equals(that.password)));
        result = result && (((passwordUpdate == null) && (that.passwordUpdate == null)) || (passwordUpdate != null && passwordUpdate.equals(that.passwordUpdate)));
        result = result && (((phone == null) && (that.phone == null)) || (phone != null && phone.equals(that.phone)));
        result = result && (((phoneMobile == null) && (that.phoneMobile == null)) || (phoneMobile != null && phoneMobile.equals(that.phoneMobile)));
        result = result && (((email == null) && (that.email == null)) || (email != null && email.equals(that.email)));
        result = result && (((fax == null) && (that.fax == null)) || (fax != null && fax.equals(that.fax)));
        result = result && (((defaultIdLanguage == null) && (that.defaultIdLanguage == null)) || (defaultIdLanguage != null && defaultIdLanguage.equals(that.defaultIdLanguage)));
        result = result && (((defaultIdRole == null) && (that.defaultIdRole == null)) || (defaultIdRole != null && defaultIdRole.equals(that.defaultIdRole)));
        result = result && (((photo == null) && (that.photo == null)) || (photo != null && photo.equals(that.photo)));
        result = result && (((firebaseToken == null) && (that.firebaseToken == null)) || (firebaseToken != null && firebaseToken.equals(that.firebaseToken)));
        return result;
    }

    /**
     * Get user information
     *
     * @return UserInfo
     */
    @Transient
    @JsonIgnore
    public UserInfo getUserInfo() {
        UserInfo result = new UserInfo();
        result.setId(idUser);
        result.setIdClient(idClient);
        result.setLogin(username);
        result.setPassword(password);
        result.setName(name);
        result.setEmail(email);
        result.setActive(active);
        return result;
    }

    @Transient
    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}

