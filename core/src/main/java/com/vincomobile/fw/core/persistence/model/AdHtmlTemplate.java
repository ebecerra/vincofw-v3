package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 03/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_html_template
 */
@Entity
@Table(name = "ad_html_template")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdHtmlTemplate extends AdEntityBean {

    private String idHtmlTemplate;
    private String title;
    private String description;
    private String image;
    private String html;

    private String imageUrl;


    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idHtmlTemplate;
    }

    @Override
    public void setId(String id) {
            this.idHtmlTemplate = id;
    }

    @Id
    @Column(name = "id_html_template")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdHtmlTemplate() {
        return idHtmlTemplate;
    }

    public void setIdHtmlTemplate(String idHtmlTemplate) {
        this.idHtmlTemplate = idHtmlTemplate;
    }

    @Column(name = "title", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "description", length = 500)
    @Size(min = 1, max = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "image", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Column(name = "html")
    @NotNull
    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }


    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdHtmlTemplate)) return false;

        final AdHtmlTemplate that = (AdHtmlTemplate) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idHtmlTemplate == null) && (that.idHtmlTemplate == null)) || (idHtmlTemplate != null && idHtmlTemplate.equals(that.idHtmlTemplate)));
        result = result && (((title == null) && (that.title == null)) || (title != null && title.equals(that.title)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((image == null) && (that.image == null)) || (image != null && image.equals(that.image)));
        result = result && (((html == null) && (that.html == null)) || (html != null && html.equals(that.html)));
        return result;
    }

    @Transient
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

