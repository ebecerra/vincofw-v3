package com.vincomobile.fw.core.hooks;

import java.util.ArrayList;
import java.util.List;

public class HookList {

    List<Hook> hooks = new ArrayList<>();

    public void add(Hook hook) {
        hooks.add(hook);
    }

}
