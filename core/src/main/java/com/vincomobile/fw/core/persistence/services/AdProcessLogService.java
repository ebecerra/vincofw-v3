package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcessLog;

import java.util.List;

/**
 * Created by Devtools.
 * Interface del servicio de ad_process_log
 *
 * Date: 28/11/2015
 */
public interface AdProcessLogService extends BaseService<AdProcessLog, String> {

    /**
     * Get execution log
     *
     * @param idProcessExec Execution log identifier
     * @return Log
     */
    List<AdProcessLog> getLog(String idProcessExec);
}


