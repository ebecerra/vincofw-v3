package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdSampledata;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_sampledata
 */
public interface AdSampledataService extends BaseService<AdSampledata, String> {

}


