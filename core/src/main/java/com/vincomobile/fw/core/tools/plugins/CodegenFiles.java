package com.vincomobile.fw.core.tools.plugins;

import java.util.ArrayList;
import java.util.List;

public class CodegenFiles {
    private String path;
    private String template;
    private String property;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public static List<CodegenFiles> listFiles(String mode) {
        ArrayList<CodegenFiles> items = new ArrayList<CodegenFiles>();
        if ("JAVA".equals(mode)) {
            items.add(new CodegenFiles(){{
                setPath("src/main/java/@package/rest/web/controllers/@CapitalizeStandardNameController.java");
                setTemplate("spring/controller.java");
                setProperty("controller");
            }});
            items.add(new CodegenFiles(){{
                setPath("src/main/java/@package/persistence/model/@CapitalizeStandardName.java");
                setTemplate("spring/model.java");
                setProperty("model");
            }});
            items.add(new CodegenFiles(){{
                setPath("src/main/java/@package/persistence/model/@CapitalizeStandardNamePK.java");
                setTemplate("spring/modelPK.java");
                setProperty("modelPK");
            }});
            items.add(new CodegenFiles(){{
                setPath("src/main/java/@package/persistence/services/@CapitalizeStandardNameServiceImpl.java");
                setTemplate("spring/service_impl.java");
                setProperty("serv_impl");
            }});
            items.add(new CodegenFiles(){{
                setPath("src/main/java/@package/persistence/services/@CapitalizeStandardNameService.java");
                setTemplate("spring/service_inter.java");
                setProperty("serv_inter");
            }});
        } else if ("SWIFT".equals(mode)) {
            items.add(new CodegenFiles(){{
                setPath("Model/Bean/DB/@CapitalizeStandardName.swift");
                setTemplate("swift/bean.swift");
                setProperty("bean");
            }});
            items.add(new CodegenFiles(){{
                setPath("Model/Bean/Rest/@CapitalizeStandardName.swift");
                setTemplate("swift/rest.swift");
                setProperty("bean");
            }});
        }
        return items;
    }
}
