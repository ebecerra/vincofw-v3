package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdProcessExecuted;
import com.vincomobile.fw.core.persistence.exception.FWProcessPlanningException;
import com.vincomobile.fw.core.persistence.model.AdProcess;
import com.vincomobile.fw.core.persistence.model.AdProcessParam;
import com.vincomobile.fw.core.persistence.model.AdUser;
import com.vincomobile.fw.core.process.ProcessDefinition;
import com.vincomobile.fw.core.process.ProcessExecError;
import org.springframework.data.domain.Sort;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Devtools.
 * Interface del servicio de ad_process
 * <p>
 * Date: 06/11/2015
 */
public interface AdProcessService extends BaseService<AdProcess, String> {

    String TYPE_REPORT = "REPORT";
    String TYPE_PROCESS = "PROCESS";

    /**
     * Execute a process
     *
     * @param process    Process to execute
     * @param user       User
     * @param idClient   Client id.
     * @param idLanguage Language id.
     * @param params     Parameter list
     * @param async      Asynchrone execution
     * @param request    Http request (Optional)
     * @param response   Http response (Optional)
     * @return Process execution result
     */
    ProcessExecError exec(AdProcess process, AdUser user, String idClient, String idLanguage, List<AdProcessParam> params, boolean async, HttpServletRequest request, HttpServletResponse response);

    /**
     * Schedules a process
     *
     * @param process    Process
     * @param cron       Cron expression
     * @param authUser   Authenticated User
     * @param idClient   Client ID
     * @param idLanguage Language ID
     * @param paramList  Parameters
     */
    void schedule(AdProcess process, String cron, AdUser authUser, String idClient, String idLanguage, String paramList) throws FWProcessPlanningException;
    void schedule(AdProcess process, AdUser authUser, String idClient, String idLanguage, String paramList) throws FWProcessPlanningException;


    /**
     * Unschedules a process
     *
     * @param process Process to unschedule
     */
    void unschedule(AdProcess process);

    /**
     * Build a param list from arguments
     *
     * @param process Process
     * @param params  Params (idParam1=value1,idParam2=value2,idParam3=value3, ....)
     * @return Param list
     */
    List<AdProcessParam> buildParamsList(AdProcess process, String params);

    /**
     * Called when process change active flag
     *
     * @param process Process
     * @param user User
     * @param idClient Client identifier
     * @param active Active / Inactive
     */
    void onChangeActive(AdProcess process, AdUser user, String idClient, boolean active);

    /**
     * Get executed process (runtime only)
     *
     * @param sort Sort field
     * @return Process list
     */
    List<AdProcessExecuted> getExecutedProcess(Sort sort);

}
