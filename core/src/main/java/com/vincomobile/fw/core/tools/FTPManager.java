package com.vincomobile.fw.core.tools;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class FTPManager {

    private Logger logger = LoggerFactory.getLogger(FTPManager.class);

    String server;
    Integer port;
    String user;
    String password;

    public FTPManager(String server, Integer port, String user, String password) {
        logger.info("FTPManager: "+user+"@"+server+":"+port);
        this.server = server;
        this.port = port;
        this.user = user;
        this.password = password;
    }

    /**
     * Descarga un fichero desde un servidor SFTP remoto
     * See: http://www.codejava.net/java-se/networking/ftp/java-ftp-file-download-tutorial-and-example
     *
     * @param remoteDirectory Directorio remoto (terminado en /)
     * @param localDirectory Directorio local
     * @param fileToDownload Fichero a descargar
     * @return Verdadero si se descargo el fichero
     */
    public boolean download(String remoteDirectory, String localDirectory, String fileToDownload){
        logger.info("Download: "+remoteDirectory + fileToDownload);
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, password);
            File downloadFile = new File(localDirectory + fileToDownload);
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
            InputStream inputStream = ftpClient.retrieveFileStream(remoteDirectory + fileToDownload);
            byte[] bytesArray = new byte[50 * 1024];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(bytesArray)) != -1) {
                outputStream.write(bytesArray, 0, bytesRead);
            }

            boolean success = ftpClient.completePendingCommand();
            if (success) {
                logger.debug("File: " + fileToDownload + " has been downloaded successfully.");
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException ex) {
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        } finally {
            disconnectFtp(ftpClient);
        }
        return true;
    }

    /**
     * Sube un fichero a un servidor de FTP
     * See: http://www.codejava.net/java-se/networking/ftp/java-ftp-file-upload-tutorial-and-example
     *
     * @param remoteDirectory Directorio remoto (terminado en /)
     * @param localDirectory Directorio local
     * @param fileFromFTP Fichero a subir
     * @param fileToFTP Fichero a almacenar
     * @return Verdadero si se subio el fichero
     */
    public boolean upload(String remoteDirectory, String localDirectory, String fileFromFTP, String fileToFTP){
        logger.info("Upload: "+remoteDirectory + fileToFTP);
        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, password);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // APPROACH #1: uploads first file using an InputStream
            File firstLocalFile = new File(localDirectory + "/" + fileFromFTP);
            InputStream inputStream = new FileInputStream(firstLocalFile);

            logger.debug("Start uploading file");
            boolean done = ftpClient.storeFile(fileToFTP, inputStream);
            inputStream.close();
            if (done) {
                logger.debug("The file is uploaded successfully.");
                return true;
            }
        } catch (IOException ex) {
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        } finally {
            disconnectFtp(ftpClient);
        }
        return false;
    }

    public boolean upload(String remoteDirectory, String localDirectory, String fileFromFTP) {
        return upload(remoteDirectory, localDirectory, fileFromFTP, fileFromFTP);
    }

    /**
     * Borra un fichero en un servidor de SFTP
     *
     * @param remoteDirectory Directorio remoto (terminado en /)
     * @param fileToDelete Fichero a borrar
     * @return Verdadero si se borro
     */
    public boolean delete(String remoteDirectory, String fileToDelete){
        logger.info("delete: "+remoteDirectory + fileToDelete);
        return true;
    }

    private void disconnectFtp(FTPClient ftpClient) {
        try {
            if (ftpClient.isConnected()) {
                ftpClient.logout();
                ftpClient.disconnect();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
