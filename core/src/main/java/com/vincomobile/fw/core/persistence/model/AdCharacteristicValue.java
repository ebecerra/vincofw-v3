package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Devtools.
 * Modelo de ad_characteristic_value
 *
 * Date: 23/01/2016
 */
@Entity
@Table(name = "ad_characteristic_value")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdCharacteristicValue extends AdEntityBean {

    private String idCharacteristicValue;
    private String idCharacteristic;
    private String keyValue;
    private String value;
    private String image;
    private Date imageUpdate;
    private String vtype;
    private String name;
    private Boolean isDefault;
    private Long seqno;
    private String reference;

    private AdCharacteristic characteristic;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idCharacteristicValue;
    }

    @Override
    public void setId(String id) {
            this.idCharacteristicValue = id;
    }

    @Id
    @Column(name = "id_characteristic_value")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdCharacteristicValue() {
        return idCharacteristicValue;
    }

    public void setIdCharacteristicValue(String idCharacteristicValue) {
        this.idCharacteristicValue = idCharacteristicValue;
    }

    @Column(name = "id_characteristic")
    @NotNull
    public String getIdCharacteristic() {
        return idCharacteristic;
    }

    public void setIdCharacteristic(String idCharacteristic) {
        this.idCharacteristic = idCharacteristic;
    }

    @Column(name = "key_value", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    @Column(name = "value", columnDefinition = "TEXT")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "image", length = 100)
    @Size(min = 1, max = 100)
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Column(name = "image_update")
    public Date getImageUpdate() {
        return imageUpdate;
    }

    public void setImageUpdate(Date imageUpdate) {
        this.imageUpdate = imageUpdate;
    }

    @Column(name = "vtype", length = 50)
    @Size(min = 1, max = 50)
    public String getVtype() {
        return vtype;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    @Column(name = "name", length = 150)
    @Size(min = 1, max = 150)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "is_default")
    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "reference", length = 10)
    @Size(min = 1, max = 10)
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_characteristic", referencedColumnName = "id_characteristic", insertable = false, updatable = false)
    public AdCharacteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(AdCharacteristic characteristic) {
        this.characteristic = characteristic;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdCharacteristicValue)) return false;

        final AdCharacteristicValue that = (AdCharacteristicValue) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idCharacteristicValue == null) && (that.idCharacteristicValue == null)) || (idCharacteristicValue != null && idCharacteristicValue.equals(that.idCharacteristicValue)));
        result = result && (((idCharacteristic == null) && (that.idCharacteristic == null)) || (idCharacteristic != null && idCharacteristic.equals(that.idCharacteristic)));
        result = result && (((keyValue == null) && (that.keyValue == null)) || (keyValue != null && keyValue.equals(that.keyValue)));
        result = result && (((value == null) && (that.value == null)) || (value != null && value.equals(that.value)));
        result = result && (((image == null) && (that.image == null)) || (image != null && image.equals(that.image)));
        result = result && (((imageUpdate == null) && (that.imageUpdate == null)) || (imageUpdate != null && imageUpdate.equals(that.imageUpdate)));
        result = result && (((vtype == null) && (that.vtype == null)) || (vtype != null && vtype.equals(that.vtype)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((isDefault == null) && (that.isDefault == null)) || (isDefault != null && isDefault.equals(that.isDefault)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((reference == null) && (that.reference == null)) || (reference != null && reference.equals(that.reference)));
        return result;
    }

}

