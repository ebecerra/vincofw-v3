package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdCharacteristicDef;

/**
 * Created by Devtools.
 * Service layer interface for ad_characteristic_def
 *
 * Date: 23/01/2016
 */
public interface AdCharacteristicDefService extends BaseService<AdCharacteristicDef, String> {

    String DEFINITION_TYPE_STANDARD = "STANDARD";

    String MODE_STANDARD            = "STANDARD";
    String MODE_ADVANCED            = "ADVANCED";
}


