package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 17/10/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Model for table ad_aux_input
 */
@Entity
@Table(name = "ad_aux_input")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdAuxInput extends AdEntityBean {

    private String idAuxInput;
    private String name;
    private String query;
    private String value;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idAuxInput;
    }

    @Override
    public void setId(String id) {
            this.idAuxInput = id;
    }

    @Id
    @Column(name = "id_aux_input")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdAuxInput() {
        return idAuxInput;
    }

    public void setIdAuxInput(String idAuxInput) {
        this.idAuxInput = idAuxInput;
    }

    @Column(name = "name", length = 200)
    @NotNull
    @Size(min = 1, max = 200)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "query", length = 1000)
    @NotNull
    @Size(min = 1, max = 1000)
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Column(name = "value", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdAuxInput)) return false;

        final AdAuxInput that = (AdAuxInput) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idAuxInput == null) && (that.idAuxInput == null)) || (idAuxInput != null && idAuxInput.equals(that.idAuxInput)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((query == null) && (that.query == null)) || (query != null && query.equals(that.query)));
        result = result && (((value == null) && (that.value == null)) || (value != null && value.equals(that.value)));
        return result;
    }

}

