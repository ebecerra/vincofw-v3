package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.hooks.HookResult;
import com.vincomobile.fw.core.tools.Converter;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Devtools.
 * Modelo de ad_preference
 *
 * Date: 17/12/2015
 */
@Entity
@Table(name = "ad_preference")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdPreference extends AdEntityBean {

    private String idPreference;
    private String value;
    private String property;
    private Boolean privilege;
    private String visibleatIdClient;
    private String visibleatIdRole;
    private String visibleatIdUser;
    private String visibleatEnvironment;
    private String visibleatCustom;
    private Boolean ispublic;
    private Boolean isimportable;

    private AdClient visibleatClient;
    private AdRole visibleatRole;
    private AdUser visibleatUser;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idPreference;
    }

    @Override
    public void setId(String id) {
            this.idPreference = id;
    }

    @Id
    @Column(name = "id_preference")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdPreference() {
        return idPreference;
    }

    public void setIdPreference(String idPreference) {
        this.idPreference = idPreference;
    }

    @Column(name = "value", columnDefinition = "TEXT")
    @NotNull
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "property", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Column(name = "privilege")
    public Boolean getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Boolean privilege) {
        this.privilege = privilege;
    }

    @Column(name = "visibleat_id_client")
    public String getVisibleatIdClient() {
        return visibleatIdClient;
    }

    public void setVisibleatIdClient(String visibleatIdClient) {
        this.visibleatIdClient = visibleatIdClient;
    }

    @Column(name = "visibleat_id_role")
    public String getVisibleatIdRole() {
        return visibleatIdRole;
    }

    public void setVisibleatIdRole(String visibleatIdRole) {
        this.visibleatIdRole = visibleatIdRole;
    }

    @Column(name = "visibleat_id_user")
    public String getVisibleatIdUser() {
        return visibleatIdUser;
    }

    public void setVisibleatIdUser(String visibleatIdUser) {
        this.visibleatIdUser = visibleatIdUser;
    }

    @Column(name = "visibleat_environment", length = 50)
    @Size(min = 1, max = 50)
    public String getVisibleatEnvironment() {
        return visibleatEnvironment;
    }

    public void setVisibleatEnvironment(String visibleatEnvironment) {
        this.visibleatEnvironment = visibleatEnvironment;
    }

    @Column(name = "visibleat_custom", length = 150)
    @Size(min = 1, max = 150)
    public String getVisibleatCustom() {
        return visibleatCustom;
    }

    public void setVisibleatCustom(String visibleatCustom) {
        this.visibleatCustom = visibleatCustom;
    }

    @Column(name = "ispublic")
    @NotNull
    public Boolean getIspublic() {
        return ispublic;
    }

    public void setIspublic(Boolean ispublic) {
        this.ispublic = ispublic;
    }

    @Column(name = "isimportable")
    @NotNull
    public Boolean getIsimportable() {
        return isimportable;
    }

    public void setIsimportable(Boolean isimportable) {
        this.isimportable = isimportable;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "visibleat_id_client", referencedColumnName = "id_client", insertable = false, updatable = false)
    @JsonIgnore
    public AdClient getVisibleatClient() {
        return visibleatClient;
    }

    public void setVisibleatClient(AdClient visibleatClient) {
        this.visibleatClient = visibleatClient;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "visibleat_id_role", referencedColumnName = "id_role", insertable = false, updatable = false)
    @JsonIgnore
    public AdRole getVisibleatRole() {
        return visibleatRole;
    }

    public void setVisibleatRole(AdRole visibleatRole) {
        this.visibleatRole = visibleatRole;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "visibleat_id_user", referencedColumnName = "id_user", insertable = false, updatable = false)
    @JsonIgnore
    public AdUser getVisibleatUser() {
        return visibleatUser;
    }

    public void setVisibleatUser(AdUser visibleatUser) {
        this.visibleatUser = visibleatUser;
    }

    /**
     * Check if preference is valid for
     *
     * @param idClient Client id
     * @param idRole Role id
     * @param idUser User id
     * @param params Custom parameters
     * @return Is valid or not
     */
    @Transient
    public boolean isValidFor(String idClient, String idRole, String idUser, Object... params) {
        boolean visibleCustom = true;
        if (!Converter.isEmpty(visibleatCustom)) {
            HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_PREFERENCE_CUSTOM_VALID, this, idClient, idRole, idUser, params);
            visibleCustom = hookResult.hasKey(FWConfig.HOOK_PREFERENCE_CUSTOM_VALID_VISIBLE) && (Boolean) hookResult.get(FWConfig.HOOK_PREFERENCE_CUSTOM_VALID_VISIBLE);
        }
        return visibleCustom &&
                (visibleatIdUser == null || visibleatIdUser.equals(idUser)) &&
                (visibleatIdRole == null || visibleatIdRole.equals(idRole)) &&
                (visibleatIdClient == null || visibleatIdClient.equals(idClient) || visibleatIdClient.equals("0")) &&
                (visibleatEnvironment == null || visibleatEnvironment.equals(FWConfig.runtimeEnvironment));
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdPreference)) return false;

        final AdPreference that = (AdPreference) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idPreference == null) && (that.idPreference == null)) || (idPreference != null && idPreference.equals(that.idPreference)));
        result = result && (((value == null) && (that.value == null)) || (value != null && value.equals(that.value)));
        result = result && (((property == null) && (that.property == null)) || (property != null && property.equals(that.property)));
        result = result && (((privilege == null) && (that.privilege == null)) || (privilege != null && privilege.equals(that.privilege)));
        result = result && (((visibleatIdClient == null) && (that.visibleatIdClient == null)) || (visibleatIdClient != null && visibleatIdClient.equals(that.visibleatIdClient)));
        result = result && (((visibleatIdRole == null) && (that.visibleatIdRole == null)) || (visibleatIdRole != null && visibleatIdRole.equals(that.visibleatIdRole)));
        result = result && (((visibleatIdUser == null) && (that.visibleatIdUser == null)) || (visibleatIdUser != null && visibleatIdUser.equals(that.visibleatIdUser)));
        result = result && (((visibleatEnvironment == null) && (that.visibleatEnvironment == null)) || (visibleatEnvironment != null && visibleatEnvironment.equals(that.visibleatEnvironment)));
        result = result && (((visibleatCustom == null) && (that.visibleatCustom == null)) || (visibleatCustom != null && visibleatCustom.equals(that.visibleatCustom)));
        result = result && (((ispublic == null) && (that.ispublic == null)) || (ispublic != null && ispublic.equals(that.ispublic)));
        result = result && (((isimportable == null) && (that.isimportable == null)) || (isimportable != null && isimportable.equals(that.isimportable)));
        return result;
    }

    @Transient
    public String getAtClient() {
        return visibleatClient != null ? visibleatClient.getName() : "";
    }

    @Transient
    public String getAtRole() {
        return visibleatRole != null ? visibleatRole.getDescription() : "";
    }

    @Transient
    public String getAtUser() {
        return visibleatUser != null ? visibleatUser.getName() : "";
    }

    /**
     * Get preference value
     *
     * @param defValue Default value
     * @return Preference value
     */
    @Transient
    @JsonIgnore
    public String getString(String defValue) {
        return value != null ? value : defValue;
    }

    @Transient
    @JsonIgnore
    public String getString() {
        return getString(null);
    }

    @Transient
    @JsonIgnore
    public Long getLong(Long defValue) {
        return value != null ? Converter.getLong(value, defValue) : defValue;
    }

    @Transient
    @JsonIgnore
    public Long getLong(){
        return getLong((Long) null);
    }

    @Transient
    @JsonIgnore
    public Integer getInteger(Integer defValue) {
        return value != null ? Converter.getInt(value, defValue) : defValue;
    }

    @Transient
    @JsonIgnore
    public Integer getInteger(){
        return getInteger(null);
    }

    @Transient
    @JsonIgnore
    public Date getDate(Date defValue) {
        return value != null ? Converter.getDate(value) : defValue;
    }

    @Transient
    @JsonIgnore
    public Date getDate() {
        return getDate((Date) null);
    }

    @Transient
    @JsonIgnore
    public Boolean getBoolean(boolean defValue) {
        return value != null ? Converter.getBoolean(value) : defValue;
    }

    @Transient
    @JsonIgnore
    public Boolean getBoolean() {
        return getBoolean(false);
    }

    @Transient
    @JsonIgnore
    public Double getDouble(Double defValue) {
        return value != null ? Converter.getFloat(value, defValue) : defValue;
    }

    @Transient
    @JsonIgnore
    public Double getDouble() {
        return getDouble((Double) null);
    }

}

