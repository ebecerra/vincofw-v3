package com.vincomobile.fw.core.persistence.dao;

import com.vincomobile.fw.core.tools.Converter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedCaseInsensitiveMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.*;

@Repository
public class AdTranslationsJDBCTemplate implements AdTranslationsDAO {

    @PersistenceContext
    protected EntityManager entityManager;

    private DataSource dataSource;
    private SimpleJdbcCall adTranslateFieldCaptions;
    private SimpleJdbcCall adTranslateMessage;
    private SimpleJdbcCall adTranslateProcessParam;

    /**
     * This is the method to be used to initialize database resources ie. connection.
     *
     * @param ds DataSource
     */
    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.adTranslateFieldCaptions =  new SimpleJdbcCall(dataSource).withProcedureName("adTranslateFieldCaptions");
        this.adTranslateMessage =  new SimpleJdbcCall(dataSource).withProcedureName("adTranslateMessage");
        this.adTranslateProcessParam =  new SimpleJdbcCall(dataSource).withProcedureName("adTranslateProcessParam");
    }

    /**
     * List all translation of messages
     *
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @return List of translation { key: value, value: translation }
     */
    @Override
    public Map<String, String> listMessages(String idClient, String idLanguage) {
        return listTranslations(adTranslateMessage, idClient, idLanguage);
    }

    /**
     * List all translation of process params
     *
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @return List of translation { key: value, value: translation }
     */
    @Override
    public Map<String, String> listProccessParams(String idClient, String idLanguage) {
        return listTranslations(adTranslateProcessParam, idClient, idLanguage);
    }

    /**
     * Generic list of translations
     *
     * @param jdbcCall JDBC store procedure
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @return List of translation { key: value, value: translation }
     */
    private Map<String, String> listTranslations(SimpleJdbcCall jdbcCall, String idClient, String idLanguage) {
        Map<String, Object> out = jdbcCall.execute(idLanguage, idClient);

        Map<String, String> result = new HashMap<String, String>();
        Collection<Object> values = out.values();
        if (!values.isEmpty()) {
            for (Object n : values) {
                if (n instanceof ArrayList) {
                    ArrayList next = (ArrayList) n;
                    for (Object aNext : next) {
                        LinkedCaseInsensitiveMap item = (LinkedCaseInsensitiveMap) aNext;
                        String key = (String) item.get("value");
                        if (!result.containsKey(key)) {
                            String translation = (String) item.get("translation");
                            result.put(key, translation != null ? translation : (String) item.get("name"));
                        }
                    }
                }
            }
        }
        return result;
    }

}
