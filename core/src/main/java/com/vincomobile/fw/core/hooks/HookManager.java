package com.vincomobile.fw.core.hooks;

import com.vincomobile.fw.core.tools.Converter;

import java.util.HashMap;
import java.util.Map;

public class HookManager {

    private static Map<String, HookList> hooks = new HashMap<>();

    /**
     * Register a Hook
     *
     * @param hook Hook
     */
    static public void registerHook(Hook hook) {
        if (!Converter.isEmpty(hook.name)) {
            HookList list = hooks.get(hook.name);
            if (list == null) {
                list = new HookList();
                hooks.put(hook.name, list);
            }
            list.add(hook);
        }
    }

    /**
     * Execute all registered hooks
     *
     * @param name Hook name
     * @param arguments Hook arguments
     * @return Execution result
     */
    static public HookResult executeHook(String name, Object... arguments) {
        HookList list = hooks.get(name);
        HookResult result = new HookResult();
        if (list != null) {
            for (Hook hook: list.hooks) {
                hook.execute(result, arguments);
                if (!result.success)
                    break;
            }
        }
        return result;
    }

}
