package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcessLog;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Servicio de ad_process_log
 *
 * Date: 28/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdProcessLogServiceImpl extends BaseServiceImpl<AdProcessLog, String> implements AdProcessLogService {

    /**
     * Constructor.
     */
    public AdProcessLogServiceImpl() {
        super(AdProcessLog.class);
    }

    /**
     * Get execution log
     *
     * @param idProcessExec Execution log identifier
     * @return Log
     */
    @Override
    public List<AdProcessLog> getLog(String idProcessExec) {
        Map filter = new HashMap();
        filter.put("idProcessExec", idProcessExec);
        return findAll(0, 1000, new SqlSort("created", "asc"), filter);
    }

}
