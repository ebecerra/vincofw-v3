/**
 * Created by Vincomobile FW on 23/06/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_import_delete
 */
package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdImportDelete;
import com.vincomobile.fw.core.persistence.model.AdImportDeletePK;

public interface AdImportDeleteService extends BaseService<AdImportDelete, AdImportDeletePK> {

    /**
     * Remove all entries for process
     *
     * @param processId Process identifier
     * @return Number of delete rows
     */
    int deleteProcessEntries(Long processId);

    /**
     * Remove all entries for process/table
     *
     * @param processId Process identifier
     * @param tableName Table name
     * @return Number of delete rows
     */
    int deleteProcessTableEntries(Long processId, String tableName);

    /**
     * Add a row
     *
     * @param processId Process identifier
     * @param tableName Table name
     * @param tableKey Table key
     */
    void addRow(Long processId, String tableName, String tableKey);
}


