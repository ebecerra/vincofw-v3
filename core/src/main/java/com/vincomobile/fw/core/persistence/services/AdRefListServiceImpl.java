package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.beans.AdRefListValue;
import com.vincomobile.fw.core.persistence.model.AdColumn;
import com.vincomobile.fw.core.persistence.model.AdRefList;

import com.vincomobile.fw.core.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_REF_LIST
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdRefListServiceImpl extends BaseServiceImpl<AdRefList, String> implements AdRefListService {

    @Autowired
    AdTranslationService translationService;

    @Autowired
    AdColumnService columnService;

    /**
     * Constructor.
     *
     */
    public AdRefListServiceImpl() {
        super(AdRefList.class);
    }

    /**
     * Get reference list for value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @return RefList
     */
    @Override
    public AdRefList getRefList(String idClient, String idReference, String value) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idReference", idReference);
        filter.put("value", value);
        return findFirst(filter);
    }

    /**
     * Get name for reference list value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @param idLanguage Language identifier
     * @return Name
     */
    @Override
    public String getName(String idClient, String idReference, String value, String idLanguage) {
        AdRefList refList = getRefList(idClient, idReference, value);
        if (refList != null) {
            if (Converter.isEmpty(idLanguage)) {
                refList.getName();
            } else {
                return translationService.getTranslation(idClient, FWConfig.TABLE_ID_REF_LIST, FWConfig.COLUMN_ID_REF_LIST_NAME, idLanguage, refList.getIdRefList(), refList.getName());
            }
        }
        return value;
    }

    @Override
    public String getName(String idClient, String idReference, String value) {
        return getName(idClient, idReference, value, null);
    }

    /**
     * Get identifier for reference list value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @return Identifier
     */
    @Override
    public String getIdRefList(String idClient, String idReference, String value) {
        AdRefList refList = getRefList(idClient, idReference, value);
        return refList == null ? null : refList.getIdRefList();
    }

    /**
     * Get list value of reference
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param idLanguage Language identifier
     * @return List values
     */
    @Override
    public List<AdRefListValue> listValues(String idClient, String idReference, String idLanguage) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idReference", idReference);
        List<AdRefList> refList = findAll(filter);
        List<AdRefListValue> result = new ArrayList<>();
        for (int i = 0; i < refList.size(); i++) {
            AdRefList item = refList.get(i);
            String name = translationService.getTranslation(idClient, FWConfig.TABLE_ID_REF_LIST, FWConfig.COLUMN_ID_REF_LIST_NAME, idLanguage, item.getIdRefList(), item.getName());
            result.add(new AdRefListValue(item.getValue(), name, item.getExtraInfo()));
        }
        return result;
    }

    /**
     * Find reference by name
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param name Name
     * @return Reference value
     */
    @Override
    public AdRefList findByName(String idClient, String idReference, String name) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idReference", idReference);
        filter.put("name", name);
        return findFirst(filter);
    }

    /**
     * Get reference value by name
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param name Name
     * @return Reference value
     */
    @Override
    public String getRefValueByName(String idClient, String idReference, String name) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idReference", idReference);
        filter.put("name", name);
        List refList = query("SELECT value FROM AdRefList WHERE idClient IN :idClient AND idReference = :idReference AND name = :name", filter);
        return refList.size() > 0 ? (String) refList.get(0) : null;
    }

    /**
     * Get reference name by value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @return Reference value
     */
    @Override
    public String getRefNameByValue(String idClient, String idReference, String value) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idReference", idReference);
        filter.put("value", value);
        List refList = query("SELECT value " +
                "FROM AdRefList " +
                "WHERE idClient IN :idClient AND idReference = :idReference " +
                "AND value = :value", filter);
        return refList.size() > 0 ? (String) refList.get(0) : null;
    }

    /**
     * Get reference value by ExtraInfo
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param extraInfo ExtraInfo
     * @return Reference value
     */
    @Override
    public String getRefValueByExtraInfo(String idClient, String idReference, String extraInfo) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idReference", idReference);
        filter.put("extraInfo", extraInfo);
        List refList = query("SELECT value FROM AdRefList WHERE idClient IN :idClient AND idReference = :idReference AND extraInfo = :extraInfo", filter);
        return refList.size() > 0 ? (String) refList.get(0) : null;
    }

    /**
     * Check if RefList is used
     *
     * @param refList RefList Value
     * @return Is used or not
     */
    @Override
    public boolean isUsed(AdRefList refList) {
        Map filter = new HashMap();
        filter.put("idReferenceValue", refList.getIdReference());
        List<AdColumn> columns = columnService.findAll(filter);
        boolean result = false;
        for (AdColumn column : columns) {
            // Columns
            Query query = entityManager.createQuery(
                    "SELECT count(*) FROM " + column.getTable().getCapitalizeStandardName() + " " +
                    "WHERE " + column.getStandardName() + " LIKE :value"
            );
            query.setParameter("value", "%" + refList.getValue() + "%");
            Number count = (Number) query.getSingleResult();
            if (count.intValue() > 0) {
                return true;
            }
            // Chart columns
            query = entityManager.createQuery("SELECT count(*) FROM AdChartColumn WHERE idReferenceValue = :value");
            query.setParameter("value", refList.getValue());
            count = (Number) query.getSingleResult();
            if (count.intValue() > 0) {
                return true;
            }
            // Chart filters
            query = entityManager.createQuery("SELECT count(*) FROM AdChartFilter WHERE idReferenceValue = :value");
            query.setParameter("value", refList.getValue());
            count = (Number) query.getSingleResult();
            if (count.intValue() > 0) {
                return true;
            }
            // Process parameter
            query = entityManager.createQuery("SELECT count(*) FROM AdProcessParam WHERE idReferenceValue = :value");
            query.setParameter("value", refList.getValue());
            count = (Number) query.getSingleResult();
            if (count.intValue() > 0) {
                return true;
            }
        }
        return result;
    }

}
