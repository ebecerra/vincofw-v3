package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdDbscript;

import java.util.List;

/**
 * Created by Vincomobile FW on 03/10/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_dbscript
 */
public interface AdDbscriptService extends BaseService<AdDbscript, String> {

    /**
     * List all active DB Scripts
     * @return DB Scripts
     */
    List<AdDbscript> listActives();
}


