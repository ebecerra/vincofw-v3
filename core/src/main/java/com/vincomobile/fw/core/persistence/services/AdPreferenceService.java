package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdPreferenceValue;
import com.vincomobile.fw.core.persistence.model.AdPermission;
import com.vincomobile.fw.core.persistence.model.AdPreference;

import java.util.Date;
import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_preference
 *
 * Date: 17/12/2015
 */
public interface AdPreferenceService extends BaseService<AdPreference, String> {

    String GLOBAL_WEB_SERVER_URL                    = "WebServerUrl";
    String GLOBAL_APACHE_SERVER_CACHE               = "ApacheServerCache";
    String GLOBAL_APACHE_SERVER_CACHE_URL           = "ApacheServerCacheUrl";
    String GLOBAL_FROM_EMAIL_NOTIFICATIONS          = "FromEmailNotifications";
    String GLOBAL_PUBLIC_WEB_PAGE_INDEX             = "PublicWebPageIndex";
    String GLOBAL_RESET_PASSWORD_VALID_DAYS         = "ResetPasswordValidDaysForToken";
    String GLOBAL_QUARTZ_SCHEDULER_EXECUTE_PROCESS  = "QuartzSchedulerExecuteProcess";

    String GLOBAL_PASSWORD_CHECK_MODE               = "PasswordCheckMode";
    String GLOBAL_PASSWORD_SIZE_MIN                 = "PasswordSizeMin";
    String GLOBAL_PASSWORD_SIZE_MAX                 = "PasswordSizeMax";
    String GLOBAL_PASSWORD_STORE_COUNT              = "PasswordStoreCount";
    String GLOBAL_PASSWORD_VALID_DAYS               = "PasswordValidDays";

    String PASSWORD_CHECK_MODE_BASIC                = "BASIC";
    String PASSWORD_CHECK_MODE_STANDARD             = "STANDARD";
    String PASSWORD_CHECK_MODE_HIGH                 = "HIGH";
    String PASSWORD_CHECK_MODE_SECURE               = "SECURE";

    /**
     * Get preference
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param params Custom parameters
     * @return Preference value
     */
    AdPreference getPreference(String name, String idClient, String idRole, String idUser, Object... params);
    AdPreference getPreference(String name, String idClient, String idRole, String idUser);
    AdPreference getPreference(String name, String idClient, String idRole);
    AdPreference getPreference(String name, String idClient);

    /**
     * Get all preferences (Not privileges)
     *
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @return Preference list
     */
    List<AdPreferenceValue> getPreferences(String idClient, String idRole, String idUser);

    /**
     * Get public preference
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param params Custom parameters
     * @return Preference value
     */
    AdPreference getPublicPreference(String name, String idClient, String idRole, String idUser, Object... params);
    AdPreference getPublicPreference(String name, String idClient, String idRole, String idUser);
    AdPreference getPublicPreference(String name, String idClient, String idRole);
    AdPreference getPublicPreference(String name, String idClient);

    /**
     * Get string preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return String preference value
     */
    String getPreferenceString(String name, String idClient, String idRole, String idUser, String defValue, Object... params);
    String getPreferenceString(String name, String idClient, String idRole, String idUser, Object... params);
    String getPreferenceString(String name, String idClient, String defValue);
    String getPreferenceString(String name, String idClient);

    /**
     * Get boolean preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Boolean preference value
     */
    Boolean getPreferenceBool(String name, String idClient, String idRole, String idUser, Boolean defValue, Object... params);
    Boolean getPreferenceBool(String name, String idClient, String idRole, String idUser, Object... params);
    Boolean getPreferenceBool(String name, String idClient, Boolean defValue);
    Boolean getPreferenceBool(String name, String idClient);

    /**
     * Get long preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Long preference value
     */
    Long getPreferenceLong(String name, String idClient, String idRole, String idUser, Long defValue, Object... params);
    Long getPreferenceLong(String name, String idClient, String idRole, String idUser, Object... params);
    Long getPreferenceLong(String name, String idClient, Long defValue);
    Long getPreferenceLong(String name, String idClient);

    /**
     * Get double preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Double preference value
     */
    Double getPreferenceDouble(String name, String idClient, String idRole, String idUser, Double defValue, Object... params);
    Double getPreferenceDouble(String name, String idClient, String idRole, String idUser, Object... params);
    Double getPreferenceDouble(String name, String idClient, Double defValue);
    Double getPreferenceDouble(String name, String idClient);

    /**
     * Get date preference value
     *
     * @param name Preference name
     * @param format Date format
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Date preference value
     */
    Date getPreferenceDate(String name, String format, String idClient, String idRole, String idUser, Date defValue, Object... params);
    Date getPreferenceDate(String name, String format, String idClient, String idRole, String idUser, Object... params);
    Date getPreferenceDate(String name, String format, String idClient);
    Date getPreferenceDate(String name, String idClient, String idRole, String idUser, Date defValue, Object... params);
    Date getPreferenceDate(String name, String idClient, String idRole, String idUser, Object... params);
    Date getPreferenceDate(String name, String idClient);

    /**
     * List permissions
     *
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @return Permission
     */
    List<AdPermission> listPermission(String idClient, String idRole, String idUser);

}


