package com.vincomobile.fw.core.session;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.model.AdPrincipal;
import com.vincomobile.fw.core.persistence.model.AdUserLog;

import java.util.*;

public class Session {

    private List<SessionInfo> users = new ArrayList<>();

    /**
     * Adds/updates an user
     *
     * @param principal User information
     */
    public void updateUser(AdPrincipal principal) {
        if (principal.getUserId() == null)
            return;
        for (SessionInfo info : users) {
            if (info.getIdUser() == null) {
                if (info.getUser().equals(principal.getName())) {
                    info.setUserName(principal.getUserName());
                    info.setIdUser(principal.getUserId());
                    info.setUserRole(principal.getRoleId());
                    info.setLastUpdate(new Date());
                    info.setUserType(principal.getUserType());
                    // Store user log information
                    List<HttpInfo> sessions = info.getSessionsInfo();
                    for (HttpInfo s : sessions) {
                        if (!s.loadAdded) {
                            AdUserLog userLog = new AdUserLog();
                            userLog.setActive(true);
                            userLog.setIdClient(FWConfig.ALL_CLIENT_ID);
                            userLog.setIdUser(info.getIdUser());
                            userLog.setSessionId(s.getIdSession());
                            userLog.setUserType(info.getUserType());
                            userLog.setUserAgent(s.getUserAgent());
                            userLog.setRemoteIp(s.getIp());
                            userLog.setRemoteHost(s.getHost());
                            FWConfig.sessionLog.addUserLog(userLog);
                            s.setLoadAdded(true);
                        }
                    }
                    return;
                }
            } else if (info.getIdUser().equals(principal.getUserId())) {
                info.setLastUpdate(new Date());
                return;
            }
        }
    }

    /**
     * Adds/updates an user
     *
     * @param user User login
     * @param idSession Session identifier
     * @param userAgent User Agent
     * @param ip Remote IP
     * @param host Remote host
     */
    public void updateUser(String user, String idSession, String userAgent, String ip, String host) {
        for (SessionInfo info : users) {
            if (info.getUser().equals(user)) {
                info.setLastUpdate(new Date());
                return;
            }
        }
        SessionInfo info = new SessionInfo(user);
        users.add(info);
        info.updateSession(idSession, userAgent, ip, host);
    }

    /**
     * Remove a user (Logout)
     *
     * @param user User login
     */
    public void removeUser(String user, String idSession) {
        for (SessionInfo info : users) {
            if (info != null && info.getUser().equals(user)) {
                for (HttpInfo httpInfo : info.getSessionsInfo()) {
                    if (idSession.equals(httpInfo.getIdSession())) {
                        httpInfo.setExpired(true);
                        removeSessionInfo(info);
                        return;
                    }
                }
            }
        }
    }

    /**
     * Deletes inactive sessions
     */
    public void clearUsers() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, -FWConfig.SESSION_IDLE_TIME);
        Date expire = cal.getTime();
        List<SessionInfo> toRemove = new ArrayList<>();
        for (SessionInfo info : users) {
            for (HttpInfo httpInfo : info.getSessionsInfo()) {
                if (expire.after(httpInfo.getLastUpdate())) {
                    httpInfo.setExpired(true);
                    if (!toRemove.contains(info))
                        toRemove.add(info);
                }
            }
        }
        for (SessionInfo info : toRemove) {
            removeSessionInfo(info);
        }
    }

    /**
     * Gets the list of users
     *
     * @return List of users
     */
    public List<SessionInfo> getUsers() {
        return users;
    }

    /**
     * Gets the total of connected users
     *
     * @return Total of connected users
     */
    public int count() {
        return users.size();
    }

    /**
     * Get User information
     *
     * @param idUser User identifier
     * @return User
     */
    public SessionInfo getUser(String idUser) {
        for (SessionInfo info : users) {
            if (info.getIdUser().equals(idUser)) {
                return info;
            }
        }
        return null;
    }

    /**
     * Update User Log information to remove user
     *
     * @param info Seesion information
     */
    private void removeSessionInfo(SessionInfo info) {
        for (HttpInfo httpInfo : info.getSessionsInfo()) {
            if (httpInfo.isExpired()) {
                FWConfig.sessionLog.addUserDisconnect(new UserLog(info.getIdUser(), httpInfo.getIdSession(), httpInfo.getLastUpdate()));
                info.sessions.remove(httpInfo.getIdSession());
            }
        }
        if (info.getSessionsInfo().size() == 0) {
            users.remove(info);
        }
    }
}
