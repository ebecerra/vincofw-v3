package com.vincomobile.fw.core.session;

import java.util.Date;

public class UserLog {

    private String idUser;
    private String sessionId;
    private boolean processed;
    private Date lastUpdate;

    public UserLog(String idUser, String sessionId, Date lastUpdate) {
        this.idUser = idUser;
        this.sessionId = sessionId;
        this.lastUpdate = lastUpdate;
        this.processed = false;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }
}
