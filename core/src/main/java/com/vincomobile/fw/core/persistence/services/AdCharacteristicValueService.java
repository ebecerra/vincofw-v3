package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdCharacteristicValue;

import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_characteristic_value
 *
 * Date: 23/01/2016
 */
public interface AdCharacteristicValueService extends BaseService<AdCharacteristicValue, String> {

    String TYPE_IMAGE   = "IMAGE";
    String TYPE_TEXT    = "TEXT";

    /**
     * Load characteristics for a item
     *
     * @param idClient Client identifier
     * @param keyValue Key value for item
     * @param characteristicType Characteristic Type for item
     * @param active Active/Inactive rows
     * @return Characteristics
     */
    List<AdCharacteristicValue> loadCharacteristicValues(String idClient, String keyValue, String characteristicType, Boolean active);

    /**
     * Load characteristics for a item
     *
     * @param idClient Client identifier
     * @param idCharacteristic Characteristic identifier
     * @return Characteristics
     */
    List<AdCharacteristicValue> loadCharacteristicAllValues(String idClient, String idCharacteristic);

    /**
     * Get characteristic value
     *
     * @param idClient Client identifier
     * @param idCharacteristic Characteristic identifier
     * @param keyValue Key value for item
     * @return Characteristic value
     */
    AdCharacteristicValue getValueFor(String idClient, String idCharacteristic, String keyValue);

    /**
     * Get characteristic value
     *
     * @param idClient Client identifier
     * @param idCharacteristic Characteristic identifier
     * @param keyValue Key value for item
     * @return Characteristic value
     */
    String getStringValueFor(String idClient, String idCharacteristic, String keyValue);

    /**
     * Remove characteristic values
     *
     * @param idClient Client identifier
     * @param keyValue Key value for item
     * @param characteristicType Characteristic Type for item
     */
    void deleteAdvancedCharacteristic(String idClient, String keyValue, String characteristicType);

    /**
     * Update a characteristic
     *
     * @param idUser User identifier
     * @param idClient Client identifier
     * @param idModule Module identifier
     * @param keyValue Key value for item
     * @param idCharacteristic Characteristic identifier
     * @param value Value
     */
    void updateCharacteristic(String idUser, String idClient, String idModule, String keyValue, String idCharacteristic, String value);
}


