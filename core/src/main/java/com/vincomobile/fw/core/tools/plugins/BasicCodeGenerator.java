package com.vincomobile.fw.core.tools.plugins;

import com.vincomobile.fw.core.tools.FreeMaker;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class BasicCodeGenerator implements CodeGenerator {

    @Autowired
    ServletContext servletContext;

    /**
     * Code generation
     *
     * @param mode Generation mode
     * @param options Opciones de generacion
     * @return Lista con el código generado
     */
    @Override
    public List<CodeItem> generate(String mode, Map options) throws IOException, TemplateException {
        List<CodeItem> codeItems = new ArrayList<CodeItem>();
        List<CodegenFiles> tipos_files = CodegenFiles.listFiles(mode);
        for (CodegenFiles files : tipos_files) {
            codeItems.add(new CodeItem(files.getProperty(), generateContent(files.getTemplate(), options), files));
        }
        return codeItems;
    }


    /**
     * Adiciona opciones especificas del generador
     *
     * @param id_tipo Tipo de generacion
     * @param options Opciones de generacion
     */
    @Override
    public void addOptions(String id_tipo, Map options) {
    }

    /**
     * Genera el codigo para un template
     *
     * @param options Opciones de generacion
     * @return Codigo generado
     * @throws Exception
     */
    protected String generateContent(String template, Map options) throws IOException, TemplateException {
        return genFreeMaker(template, options);
    }

    /**
     * Genera un Template de una tabla
     *
     * @param template Nombre del template
     * @param options  Opciones de generacion
     * @return Salida del template
     */
    private String genFreeMaker(String template, Map options) throws IOException, TemplateException {
        String path = servletContext.getRealPath("WEB-INF/template/vinco_core");
        return FreeMaker.genFreeMaker(path + "/" + template, options);
    }

}
