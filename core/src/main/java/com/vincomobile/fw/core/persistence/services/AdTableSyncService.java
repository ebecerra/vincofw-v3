package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdTableSync;
import com.vincomobile.fw.core.sync.SyncError;
import com.vincomobile.fw.core.sync.SyncExportDB;
import com.vincomobile.fw.core.sync.SyncTable;

import java.util.Date;

/**
 * Created by Devtools.
 * Service layer interface for ad_table_sync
 *
 * Date: 05/03/2016
 */
public interface AdTableSyncService extends BaseService<AdTableSync, String> {

    String SOURCE_DATABASE  = "DATABASE";
    String SOURCE_VALUE     = "VALUE";

    /**
     * Get database date
     *
     * @return Date
     */
    Date getDatabaseDatetime();

    /**
     * Get table information
     *
     * @param name Table name
     * @return AdTable
     */
    AdTableSync getTable(String name);

    /**
     * Obtains information update an entire table
     *
     * @param idClient Client identifier
     * @param sincro Informacion de sincronizacion (a completar)
     * @param tableSync Synchronization information (to be completed)
     * @param filters Filter to apply
     * @param param_1 Parameter extra 1
     * @param param_2 Parameter extra 2
     * @param param_3 Parameter extra 3
     * @param param_4 Parameter extra 4
     * @param param_5 Parameter extra 5
     */
    void getTableUpdateSQL(String idClient, SyncTable sincro, AdTableSync tableSync, String filters, String param_1, String param_2, String param_3, String param_4, String param_5);

    /**
     * Gets the name of a file replacing parameters @[name]
     *
     * @param table Table information
     * @param name  File name
     * @return Real name
     */
    String getFileName(AdTableSync table, String name);

    /**
     * Import data from device
     *
     * @param idClient Client identifier
     * @param exportDB Export information from device
     * @return Import errors
     * @throws Exception Processing exception
     */
    SyncError makeUpdate(String idClient, SyncExportDB exportDB) throws Exception;


}


