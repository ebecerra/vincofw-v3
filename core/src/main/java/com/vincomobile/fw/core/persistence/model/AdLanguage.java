package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_LANGUAGE
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_language")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdLanguage extends AdEntityBean {

    private String idLanguage;
    private String name;
    private String iso2;
    private String iso3;
    private String logo;
    private String countrycode;

    protected AdClient client;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idLanguage;
    }

    @Override
    public void setId(String id) {
            this.idLanguage = id;
    }

    @Id
    @Column(name = "id_language")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "iso_2", length = 2)
    @NotNull
    @Size(min = 1, max = 2)
    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    @Column(name = "iso_3", length = 3)
    @NotNull
    @Size(min = 1, max = 3)
    public String getIso3() {
        return iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    @Column(name = "logo", length = 45)
    @Size(min = 1, max = 45)
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Column(name = "countrycode", length = 2)
    @NotNull
    @Size(min = 1, max = 2)
    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdLanguage)) return false;

        final AdLanguage that = (AdLanguage) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idLanguage == null) && (that.idLanguage == null)) || (idLanguage != null && idLanguage.equals(that.idLanguage)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((iso2 == null) && (that.iso2 == null)) || (iso2 != null && iso2.equals(that.iso2)));
        result = result && (((iso3 == null) && (that.iso3 == null)) || (iso3 != null && iso3.equals(that.iso3)));
        result = result && (((logo == null) && (that.logo == null)) || (logo != null && logo.equals(that.logo)));
        result = result && (((countrycode == null) && (that.countrycode == null)) || (countrycode != null && countrycode.equals(that.countrycode)));
        return result;
    }

}

