package com.vincomobile.fw.core.session;

import java.util.Date;

public class HttpInfo {

    String idSession;
    String userAgent;
    String ip;
    String host;
    Date connect;
    Date lastUpdate;
    boolean loadAdded;
    boolean expired;

    public HttpInfo(String idSession, String userAgent, String ip, String host) {
        this.idSession = idSession;
        this.userAgent = userAgent;
        this.ip = ip;
        this.host = host;
        this.connect = new Date();
        this.lastUpdate = new Date();
        this.loadAdded = false;
        this.expired = false;
    }

    public void update(String userAgent) {
        this.userAgent = userAgent;
        this.lastUpdate = new Date();
    }

    public String getIdSession() {
        return idSession;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getIp() {
        return ip;
    }

    public String getHost() {
        return host;
    }

    public Date getConnect() {
        return connect;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public boolean isLoadAdded() {
        return loadAdded;
    }

    public void setLoadAdded(boolean loadAdded) {
        this.loadAdded = loadAdded;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }
}
