package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRolePriv;
import com.vincomobile.fw.core.persistence.model.AdUser;

import java.util.Date;
import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_USER
 * <p>
 * Date: 19/02/2015
 */
public interface AdUserService extends BaseService<AdUser, String> {

    /**
     * Gets the user by login
     *
     * @param idClient Client identifier
     * @param login Login to search
     * @return User data
     */
    AdUser findByLogin(String idClient, String login);
    AdUser findByLogin(String login);

    /**
     * Gets the user by email
     *
     * @param email Email to search
     * @return User data
     */
    AdUser findByEmail(String email);

    /**
     * Gets the privileges of a role
     *
     * @param idRole Role ID
     * @return Privilege list
     */
    List<AdRolePriv> getPrivileges(String idRole);

    /**
     * Validate security restrictions for a user password
     *
     * @param idClient Client identifier
     * @param lastUpdate Last password update
     * @param idLanguage Language identifier
     * @param size Password size
     * @param security Security level satisfy
     * @param checkValidDays Check valid days
     * @return Error or null
     */
    String checkSecurityPassword(String idClient, Date lastUpdate, String idLanguage, Long size, String security, boolean checkValidDays);

    /**
     * Validate security restrictions for a user password
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idLanguage Language identifier
     * @param newPassword New password
     * @return Error or null
     */
    String checkSecurityPasswordHistory(String idClient, String idUser, String idLanguage, String newPassword);
}


