package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.beans.AdEntityExtraValue;
import com.vincomobile.fw.core.tools.Converter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MappedSuperclass
public abstract class EntityBean<PK extends Serializable> {

    protected static final String LIGHT_JSON_TYPES = "String;Boolean;Long;Integer;Date";

    protected Date created;
    protected Date updated;
    protected String createdBy;
    protected String updatedBy;
    protected Boolean active;
    protected Map<String, Object> extra;
    protected List<AdApproval> approvals;

    @Column(name = "created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Column(name = "updated")
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "active", columnDefinition = "BIT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Transient
    public abstract PK getId();

    public abstract void setId(PK id);

    @Transient
    public String getIdStr() {
        return getId() == null ? null : getId().toString();
    }


    @Transient
    public List<AdApproval> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<AdApproval> approvals) {
        this.approvals = approvals;
    }

    @Transient
    public Map<String, Object> getExtra() {
        return extra;
    }

    public void setExtra(List<AdEntityExtraValue> extra) {
        if (extra != null) {
            this.extra = new HashMap();
            for (AdEntityExtraValue item : extra) {
                this.extra.put(item.getKey(), item.getValue());
            }
        }
    }

    public void put(String key, Object value) {
        if (this.extra == null) {
            this.extra = new HashMap<>();
        }
        this.extra.put(key, value);
    }

    public Object get(String key) {
        return this.extra == null ? null : this.extra.get(key);
    }

    public Boolean getBoolean(String key) {
        return (Boolean) get(key);
    }

    public String getString(String key) {
        return (String) get(key);
    }

    public Long getLong(String key) {
        return (Long) get(key);
    }

    public Double getDouble(String key) {
        return (Double) get(key);
    }

    public Date getDate(String key) {
        return (Date) get(key);
    }

    @PrePersist
    public void onPrePersist() {
        if (this.created == null)
            this.created = new Date();
        this.updated = this.created;
        if (createdBy == null)
            this.createdBy = FWConfig.ID_USER_SYSTEM;
        if (updatedBy == null)
            this.updatedBy = FWConfig.ID_USER_SYSTEM;
    }

    @PreUpdate
    public void onPreUpdate() {
        this.updated = new Date();
    }

    /**
     * Equals implementation
     *
     * @param aThat Object to compare with
     * @return true/false
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object aThat) {
        final EntityBean that = (EntityBean) aThat;
        boolean result = super.equals(aThat);
        result = result && (((created == null) && (that.created == null)) || (created != null && created.equals(that.created)));
        result = result && (((createdBy == null) && (that.createdBy == null)) || (createdBy != null && createdBy.equals(that.createdBy)));
        result = result && (((created == null) && (that.updated == null)) || (updated != null && updated.equals(that.updated)));
        result = result && (((updatedBy == null) && (that.updatedBy == null)) || (updatedBy != null && updatedBy.equals(that.updatedBy)));
        result = result && (((active == null) && (that.active == null)) || (active != null && active.equals(that.active)));
        return result;
    }

    /**
     * Implementation of toString
     *
     * @return String representation of this class.
     * @see Object#toString()
     */
    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            return "toString(): " + e.getMessage();
        }
    }

    /**
     * Set a value for property
     *
     * @param name  Property name (CamelCase)
     * @param value Value
     */
    public void setPropertyValue(String name, Object value) {
        name = Converter.capitalize2(name);
        try {
            Method method = this.getClass().getMethod("set" + name, value != null ? value.getClass() : Object.class);
            method.setAccessible(true);
            method.invoke(this, value);
        } catch (Exception e) {
            System.out.println("Error calling: " + this.getClass().getName() + ".set" + name);
            e.printStackTrace();
        }
    }

    /**
     * Set a null value for property
     *
     * @param name  Property name (CamelCase)
     * @param clazz Value clazz
     */
    public void setPropertyNull(String name, Class clazz) {
        name = Converter.capitalize2(name);
        try {
            Method method = this.getClass().getMethod("set" + name, clazz);
            method.setAccessible(true);
            method.invoke(this, (Object) null);
        } catch (Exception e) {
            System.out.println("Error calling: " + this.getClass().getName() + ".set" + name);
            e.printStackTrace();
        }
    }

    /**
     * Get value for property
     *
     * @param name Property name (CamelCase)
     * @return Property value
     */
    public Object getPropertyValue(String name) {
        name = Converter.capitalize2(name);
        try {
            Method method = this.getClass().getMethod("get" + name);
            method.setAccessible(true);
            return method.invoke(this);
        } catch (Exception e) {
            System.out.println("Error calling: " + this.getClass().getName() + ".get" + name);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Check has a public property (get[Name])
     *
     * @param name Property name
     * @return If has property
     */
    public boolean hasProperty(String name) {
        name = Converter.capitalize2(name);
        try {
            this.getClass().getMethod("get" + name);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Initialize bean
     *
     * @param idUser User identifier
     */
    public void init(String idUser) {
        this.createdBy = idUser;
        this.updatedBy = idUser;
        this.active = true;
    }

    /**
     * Get first level JSON
     */
    @Transient
    @JsonIgnore
    public String getLightJson() {
        String result = "{";
        int count = 0;
        for(Field field : this.getClass().getDeclaredFields()) {
            String typeName = field.getType().getSimpleName();
            if (LIGHT_JSON_TYPES.contains(typeName)) {
                result += (count++ > 0 ? "," : "") + "\"" + field.getName() + "\":";
                Object value = getPropertyValue(field.getName());
                if (value == null) {
                    result += "null";
                } else if ("String".equals(typeName)) {
                    result += "\"" + value + "\"";
                } else if ("Long".equals(typeName) || "Integer".equals(typeName) || "Boolean".equals(typeName)) {
                   result += value;
                } else if ("Date".equals(typeName)) {
                    result += ((Date) value).getTime();
                }
            }
        }
        return result + "}";
    }
}