package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdUser;
import com.vincomobile.fw.core.persistence.model.AdUserRoles;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_USER_ROLES
 *
 * Date: 19/02/2015
 */
public interface AdUserRolesService extends BaseService<AdUserRoles, String> {

    /**
     * List user by role
     *
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @return User list
     */
    List<AdUser> listUserByRole(String idClient, String idRole);

    /**
     * Find user role
     *
     * @param idUser User identifier
     * @param idRole Role identifier
     * @return AdUserRoles
     */
    AdUserRoles findUserRole(String idUser, String idRole);
}


