package com.vincomobile.fw.core.business;

import com.vincomobile.fw.core.persistence.model.EntityBean;

import java.util.Date;

public class VirtualUser {

    String idUser;
    EntityBean realUser;
    String password;
    Date passwordUpdate;

    public VirtualUser(EntityBean realUser, String idUser, String password, Date passwordUpdate) {
        this.realUser = realUser;
        this.idUser = idUser;
        this.password = password;
        this.passwordUpdate = passwordUpdate;
    }

    public VirtualUser(EntityBean realUser, String idUser, String password) {
        this.realUser = realUser;
        this.idUser = idUser;
        this.password = password;
        this.passwordUpdate = new Date();
    }

    public String getIdUser() {
        return idUser;
    }

    public EntityBean getRealUser() {
        return realUser;
    }

    public String getPassword() {
        return password;
    }

    public Date getPasswordUpdate() {
        return passwordUpdate;
    }
}
