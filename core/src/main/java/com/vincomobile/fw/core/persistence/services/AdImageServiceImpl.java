package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdPhoto;
import com.vincomobile.fw.core.persistence.model.AdImage;
import com.vincomobile.fw.core.tools.Converter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service layer implementation for ad_image
 *
 * Date: 23/01/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdImageServiceImpl extends BaseServiceImpl<AdImage, String> implements AdImageService {

    /**
     * Constructor.
     */
    public AdImageServiceImpl() {
        super(AdImage.class);
    }

    /**
     * Get first image
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     * @param itype Image type
     * @return Image
     */
    @Override
    public AdImage getImage(String idTable, String idRow, String itype) {
        Map filter = new HashMap();
        filter.put("idTable", idTable);
        filter.put("idRow", idRow);
        if (!Converter.isEmpty(itype)) {
            filter.put("itype", itype);
        }
        return findFirst(filter, "seqno", "asc");
    }

    @Override
    public AdImage getImage(String idTable, String idRow) {
        return getImage(idTable, idRow, null);
    }

    /**
     * Get first image
     *
     * @param idTable Table identifier
     * @param idRow   Row identifier
     * @param itype   Image type
     * @return Image
     */
    @Override
    public AdPhoto getPhoto(String idTable, String idRow, String itype) {
        AdImage image = getImage(idTable, idRow, itype);
        return image == null ? null : new AdPhoto(image);
    }

    @Override
    public AdPhoto getPhoto(String idTable, String idRow) {
        return getPhoto(idTable, idRow, null);
    }

    /**
     * Load all images
     *
     * @param idTable Table identifier
     * @param idRow   Row identifier
     * @param itype   Image type
     * @return Image list
     */
    @Override
    public List<AdPhoto> getAllPhotos(String idTable, String idRow, String itype) {
        Map filter = new HashMap();
        filter.put("idTable", idTable);
        filter.put("idRow", idRow);
        if (!Converter.isEmpty(itype)) {
            filter.put("itype", itype);
        }
        SqlSort sort = new SqlSort();
        sort.addSorter("seqno", "asc");
        List<AdImage> images = findAll(sort, filter);
        List<AdPhoto> result = new ArrayList<>();
        for (AdImage image : images) {
            result.add(new AdPhoto(image));
        }
        return result;
    }

    public List<AdPhoto> getAllPhotos(String idTable, String idRow) {
        return getAllPhotos(idTable, idRow, null);
    }

}