package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.AdProcessParam;
import com.vincomobile.fw.core.persistence.model.AdRefSequence;
import com.vincomobile.fw.core.persistence.services.AdRefSequenceService;
import com.vincomobile.fw.core.tools.Datetool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdRefSequenceProcess extends TestingBaseProcess {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdRefSequenceProcess service = new AdRefSequenceProcess();

    @Autowired
    AdRefSequenceService sequenceService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Execute test
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void doResetYear(String idProcessExec) {
        AdProcessParam refSecuenceParam = getParam("idRefSequence");
        if (refSecuenceParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idRefSequence"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdRefSequence sequence = sequenceService.findById(refSecuenceParam.getString());
        if (sequence == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idRefSequence"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        if (AdRefSequenceService.TYPE_YEAR.equals(sequence.getStype())) {
            if (sequence.getSyear() == null || sequence.getSyear() < Datetool.getCurrentYear()) {
                sequence.setSyear((long) Datetool.getCurrentYear());
                sequence.setCurrentnext(0L);
                sequenceService.update(sequence);
                info(idProcessExec, getMessage("AD_ProcessSequenceReset", sequence.getCurrentValue()));
            } else {
                warn(idProcessExec, getMessage("AD_ProcessSequenceResetNotYearPassed", sequence.getSyear()));
            }
        } else {
            warn(idProcessExec, getMessage("AD_ProcessSequenceResetNotYearSeq"));
        }
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

}