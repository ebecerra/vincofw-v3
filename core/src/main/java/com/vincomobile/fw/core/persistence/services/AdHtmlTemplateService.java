package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdHtmlTemplate;

/**
 * Created by Vincomobile FW on 03/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_html_template
 */
public interface AdHtmlTemplateService extends BaseService<AdHtmlTemplate, String> {

}


