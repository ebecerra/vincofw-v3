package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcessExec;

/**
 * Created by Devtools.
 * Interface del servicio de ad_process_exec
 *
 * Date: 28/11/2015
 */
public interface AdProcessExecService extends BaseService<AdProcessExec, String> {

    /**
     * Clear process execution table
     *
     * @param days Number of days to preserve
     * @return Number of remove rows
     */
    int clearProcessExec(int days);

}


