package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 09/11/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Model for table ad_chart_column
 */
@Entity
@Table(name = "ad_chart_column")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdChartColumn extends AdEntityBean {

    private String idChartColumn;
    private String idChart;
    private String idReference;
    private String idReferenceValue;
    private String name;
    private String caption;
    private Long seqno;
    private String sqlExpression;
    private Boolean displayed;

    private AdReference reference;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idChartColumn;
    }

    @Override
    public void setId(String id) {
            this.idChartColumn = id;
    }

    @Id
    @Column(name = "id_chart_column")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdChartColumn() {
        return idChartColumn;
    }

    public void setIdChartColumn(String idChartColumn) {
        this.idChartColumn = idChartColumn;
    }

    @Column(name = "id_chart", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdChart() {
        return idChart;
    }

    public void setIdChart(String idChart) {
        this.idChart = idChart;
    }

    @Column(name = "id_reference", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "id_reference_value", length = 32)
    @Size(min = 1, max = 32)
    public String getIdReferenceValue() {
        return idReferenceValue;
    }

    public void setIdReferenceValue(String idReferenceValue) {
        this.idReferenceValue = idReferenceValue;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "sql_expression", length = 250)
    @NotNull
    @Size(min = 1, max = 250)
    public String getSqlExpression() {
        return sqlExpression;
    }

    public void setSqlExpression(String sqlExpression) {
        this.sqlExpression = sqlExpression;
    }

    @Column(name = "caption", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Column(name = "displayed")
    @NotNull
    public Boolean getDisplayed() {
        return displayed;
    }

    public void setDisplayed(Boolean displayed) {
        this.displayed = displayed;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_reference", referencedColumnName = "id_reference", insertable = false, updatable = false)
    public AdReference getReference() {
        return reference;
    }

    public void setReference(AdReference reference) {
        this.reference = reference;
    }

    /**
     * Equals implementation
     *
     * @see java.lang.Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdChartColumn)) return false;

        final AdChartColumn that = (AdChartColumn) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idChartColumn == null) && (that.idChartColumn == null)) || (idChartColumn != null && idChartColumn.equals(that.idChartColumn)));
        result = result && (((idChart == null) && (that.idChart == null)) || (idChart != null && idChart.equals(that.idChart)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((idReferenceValue == null) && (that.idReferenceValue == null)) || (idReferenceValue != null && idReferenceValue.equals(that.idReferenceValue)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((caption == null) && (that.caption == null)) || (caption != null && caption.equals(that.caption)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((sqlExpression == null) && (that.sqlExpression == null)) || (sqlExpression != null && sqlExpression.equals(that.sqlExpression)));
        result = result && (((displayed == null) && (that.displayed == null)) || (displayed != null && displayed.equals(that.displayed)));
        return result;
    }

}

