package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by Devtools.
 * Modelo de AD_MENU_ROLES
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_menu_roles")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdMenuRoles extends AdEntityBean {

    private String idMenuRoles;
    private String idMenu;
    private String idRole;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idMenuRoles;
    }

    @Override
    public void setId(String id) {
            this.idMenuRoles = id;
    }

    @Id
    @Column(name = "id_menu_roles")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdMenuRoles() {
        return idMenuRoles;
    }

    public void setIdMenuRoles(String idMenuRoles) {
        this.idMenuRoles = idMenuRoles;
    }


    @Column(name = "id_menu")
    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    @Column(name = "id_role")
    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdMenuRoles)) return false;

        final AdMenuRoles that = (AdMenuRoles) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idMenuRoles == null) && (that.idMenuRoles == null)) || (idMenuRoles != null && idMenuRoles.equals(that.idMenuRoles)));
        result = result && (((idMenu == null) && (that.idMenu == null)) || (idMenu != null && idMenu.equals(that.idMenu)));
        result = result && (((idRole == null) && (that.idRole == null)) || (idRole != null && idRole.equals(that.idRole)));
        return result;
    }

}

