package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Devtools.
 * Modelo de AD_REF_TABLE
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_ref_table")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdRefTable extends AdEntityBean {

    private String idRefTable;
    private String idReference;
    private String idTable;
    private String idKey;
    private String idDisplay;
    private String sqlwhere;
    private String sqlorderby;

    private AdReference reference;
    private AdTable table;
    private AdColumn key;
    private AdColumn display;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idRefTable;
    }

    @Override
    public void setId(String id) {
            this.idRefTable = id;
    }

    @Id
    @Column(name = "id_ref_table")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdRefTable() {
        return idRefTable;
    }

    public void setIdRefTable(String idRefTable) {
        this.idRefTable = idRefTable;
    }

    @Column(name = "id_reference")
    @NotNull
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "id_table")
    @NotNull
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_key")
    @NotNull
    public String getIdKey() {
        return idKey;
    }

    public void setIdKey(String idKey) {
        this.idKey = idKey;
    }

    @Column(name = "id_display")
    @NotNull
    public String getIdDisplay() {
        return idDisplay;
    }

    public void setIdDisplay(String idDisplay) {
        this.idDisplay = idDisplay;
    }

    @Column(name = "sqlwhere", columnDefinition = "TEXT")
    public String getSqlwhere() {
        return sqlwhere;
    }

    public void setSqlwhere(String sqlwhere) {
        this.sqlwhere = sqlwhere;
    }

    @Column(name = "sqlorderby", columnDefinition = "TEXT")
    public String getSqlorderby() {
        return sqlorderby;
    }

    public void setSqlorderby(String sqlorderby) {
        this.sqlorderby = sqlorderby;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_reference", referencedColumnName = "id_reference", insertable = false, updatable = false)
    public AdReference getReference() {
        return reference;
    }

    public void setReference(AdReference reference) {
        this.reference = reference;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_key", referencedColumnName = "id_column", insertable = false, updatable = false)
    public AdColumn getKey() {
        return key;
    }

    public void setKey(AdColumn key) {
        this.key = key;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_display", referencedColumnName = "id_column", insertable = false, updatable = false)
    public AdColumn getDisplay() {
        return display;
    }

    public void setDisplay(AdColumn display) {
        this.display = display;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdRefTable)) return false;

        final AdRefTable that = (AdRefTable) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idRefTable == null) && (that.idRefTable == null)) || (idRefTable != null && idRefTable.equals(that.idRefTable)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idKey == null) && (that.idKey == null)) || (idKey != null && idKey.equals(that.idKey)));
        result = result && (((idDisplay == null) && (that.idDisplay == null)) || (idDisplay != null && idDisplay.equals(that.idDisplay)));
        result = result && (((sqlwhere == null) && (that.sqlwhere == null)) || (sqlwhere != null && sqlwhere.equals(that.sqlwhere)));
        result = result && (((sqlorderby == null) && (that.sqlorderby == null)) || (sqlorderby != null && sqlorderby.equals(that.sqlorderby)));
        return result;
    }

}

