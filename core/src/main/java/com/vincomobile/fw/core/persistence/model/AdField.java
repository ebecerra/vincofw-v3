package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_FIELD
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_field")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdField extends AdEntityBean implements Cloneable {

    private String idField;
    private String idTab;
    private String idColumn;
    private String idFieldGroup;
    private String name;
    private String caption;
    private Long seqno;
    private Long gridSeqno;
    private Boolean readonly;
    private Boolean displayed;
    private Boolean showingrid;
    private Boolean startnewrow;
    private Boolean showinstatusbar;
    private Boolean usedInChild;
    private String onchangefunction;
    private String displaylogic;
    private Long span;
    private Boolean filterRange;
    private String filterType;
    private Boolean sortable;
    private String cssClass;
    private Boolean multiSelect;
    private String placeholder;

    private AdColumn column;
    private AdFieldGroup fieldGroup;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idField;
    }

    @Override
    public void setId(String id) {
            this.idField = id;
    }

    @Id
    @Column(name = "id_field")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdField() {
        return idField;
    }

    public void setIdField(String idField) {
        this.idField = idField;
    }

    @Column(name = "id_tab")
    @NotNull
    public String getIdTab() {
        return idTab;
    }

    public void setIdTab(String idTab) {
        this.idTab = idTab;
    }

    @Column(name = "id_column")
    @NotNull
    public String getIdColumn() {
        return idColumn;
    }

    public void setIdColumn(String idColumn) {
        this.idColumn = idColumn;
    }

    @Column(name = "id_field_group")
    public String getIdFieldGroup() {
        return idFieldGroup;
    }

    public void setIdFieldGroup(String idFieldGroup) {
        this.idFieldGroup = idFieldGroup;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "caption", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "grid_seqno")
    public Long getGridSeqno() {
        return gridSeqno;
    }

    public void setGridSeqno(Long gridSeqno) {
        this.gridSeqno = gridSeqno;
    }

    @Column(name = "readonly")
    @NotNull
    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    @Column(name = "displayed")
    @NotNull
    public Boolean getDisplayed() {
        return displayed;
    }

    public void setDisplayed(Boolean displayed) {
        this.displayed = displayed;
    }

    @Column(name = "showingrid")
    @NotNull
    public Boolean getShowingrid() {
        return showingrid;
    }

    public void setShowingrid(Boolean showingrid) {
        this.showingrid = showingrid;
    }

    @Column(name = "startnewrow")
    @NotNull
    public Boolean getStartnewrow() {
        return startnewrow;
    }

    public void setStartnewrow(Boolean startnewrow) {
        this.startnewrow = startnewrow;
    }

    @Column(name = "showinstatusbar")
    @NotNull
    public Boolean getShowinstatusbar() {
        return showinstatusbar;
    }

    public void setShowinstatusbar(Boolean showinstatusbar) {
        this.showinstatusbar = showinstatusbar;
    }

    @Column(name = "used_in_child")
    @NotNull
    public Boolean getUsedInChild() {
        return usedInChild;
    }

    public void setUsedInChild(Boolean usedInChild) {
        this.usedInChild = usedInChild;
    }

    @Column(name = "onchangefunction", length = 250)
    @Size(min = 1, max = 250)
    public String getOnchangefunction() {
        return onchangefunction;
    }

    public void setOnchangefunction(String onchangefunction) {
        this.onchangefunction = onchangefunction;
    }

    @Column(name = "displaylogic", length = 2000)
    @Size(min = 1, max = 2000)
    public String getDisplaylogic() {
        return displaylogic;
    }

    public void setDisplaylogic(String displaylogic) {
        this.displaylogic = displaylogic;
    }

    @Column(name = "span")
    @NotNull
    public Long getSpan() {
        return span;
    }

    public void setSpan(Long span) {
        this.span = span;
    }

    @Column(name = "filter_range")
    @NotNull
    public Boolean getFilterRange() {
        return filterRange;
    }

    public void setFilterRange(Boolean filterRange) {
        this.filterRange = filterRange;
    }

    @Column(name = "filter_type", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    @Column(name = "sortable")
    @NotNull
    public Boolean getSortable() {
        return sortable;
    }

    public void setSortable(Boolean sortable) {
        this.sortable = sortable;
    }

    @Column(name = "css_class", length = 100)
    @Size(min = 1, max = 100)
    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    @Column(name = "multi_select")
    public Boolean getMultiSelect() {
        return multiSelect;
    }

    public void setMultiSelect(Boolean multiSelect) {
        this.multiSelect = multiSelect;
    }

    @Column(name = "placeholder", length = 200)
    @Size(min = 1, max = 200)
    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_column", referencedColumnName = "id_column", insertable = false, updatable = false)
    public AdColumn getColumn() {
        return column;
    }

    public void setColumn(AdColumn column) {
        this.column = column;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_field_group", referencedColumnName = "id_field_group", insertable = false, updatable = false)
    public AdFieldGroup getFieldGroup() {
        return fieldGroup;
    }

    public void setFieldGroup(AdFieldGroup fieldGroup) {
        this.fieldGroup = fieldGroup;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdField)) return false;

        final AdField that = (AdField) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idField == null) && (that.idField == null)) || (idField != null && idField.equals(that.idField)));
        result = result && (((idTab == null) && (that.idTab == null)) || (idTab != null && idTab.equals(that.idTab)));
        result = result && (((idColumn == null) && (that.idColumn == null)) || (idColumn != null && idColumn.equals(that.idColumn)));
        result = result && (((idFieldGroup == null) && (that.idFieldGroup == null)) || (idFieldGroup != null && idFieldGroup.equals(that.idFieldGroup)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((caption == null) && (that.caption == null)) || (caption != null && caption.equals(that.caption)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((gridSeqno == null) && (that.gridSeqno == null)) || (gridSeqno != null && gridSeqno.equals(that.gridSeqno)));
        result = result && (((readonly == null) && (that.readonly == null)) || (readonly != null && readonly.equals(that.readonly)));
        result = result && (((displayed == null) && (that.displayed == null)) || (displayed != null && displayed.equals(that.displayed)));
        result = result && (((showingrid == null) && (that.showingrid == null)) || (showingrid != null && showingrid.equals(that.showingrid)));
        result = result && (((startnewrow == null) && (that.startnewrow == null)) || (startnewrow != null && startnewrow.equals(that.startnewrow)));
        result = result && (((showinstatusbar == null) && (that.showinstatusbar == null)) || (showinstatusbar != null && showinstatusbar.equals(that.showinstatusbar)));
        result = result && (((usedInChild == null) && (that.usedInChild == null)) || (usedInChild != null && usedInChild.equals(that.usedInChild)));
        result = result && (((onchangefunction == null) && (that.onchangefunction == null)) || (onchangefunction != null && onchangefunction.equals(that.onchangefunction)));
        result = result && (((displaylogic == null) && (that.displaylogic == null)) || (displaylogic != null && displaylogic.equals(that.displaylogic)));
        result = result && (((span == null) && (that.span == null)) || (span != null && span.equals(that.span)));
        result = result && (((filterRange == null) && (that.filterRange == null)) || (filterRange != null && filterRange.equals(that.filterRange)));
        result = result && (((filterType == null) && (that.filterType == null)) || (filterType != null && filterType.equals(that.filterType)));
        result = result && (((sortable == null) && (that.sortable == null)) || (sortable != null && sortable.equals(that.sortable)));
        result = result && (((cssClass == null) && (that.cssClass == null)) || (cssClass != null && cssClass.equals(that.cssClass)));
        result = result && (((multiSelect == null) && (that.multiSelect == null)) || (multiSelect != null && multiSelect.equals(that.multiSelect)));
        result = result && (((placeholder == null) && (that.placeholder == null)) || (placeholder != null && placeholder.equals(that.placeholder)));
        return result;
    }

    @Override
    public Object clone(){
        AdField newField = new AdField();
        // Reference keys
        newField.setIdClient(this.getIdClient());
        newField.setIdModule(this.getIdModule());
        newField.setIdTab(this.getIdTab());
        newField.setIdColumn(this.getIdColumn());
        newField.setIdFieldGroup(this.getIdFieldGroup());

        newField.setActive(this.getActive());
        newField.setName(this.getName());
        newField.setSeqno(this.getSeqno());
        newField.setGridSeqno(this.getGridSeqno());
        newField.setReadonly(this.getReadonly());
        newField.setDisplayed(this.getDisplayed());
        newField.setShowingrid(this.getShowingrid());
        newField.setStartnewrow(this.getStartnewrow());
        newField.setShowinstatusbar(this.getShowinstatusbar());
        newField.setUsedInChild(this.getUsedInChild());
        newField.setOnchangefunction(this.getOnchangefunction());
        newField.setDisplaylogic(this.getDisplaylogic());
        newField.setSpan(this.getSpan());
        newField.setFilterRange(this.getFilterRange());
        newField.setFilterType(this.getFilterType());
        newField.setSortable(this.getSortable());
        newField.setCaption(this.getCaption());
        return newField;
    }

}

