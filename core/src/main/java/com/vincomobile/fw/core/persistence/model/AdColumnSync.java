package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.services.AdColumnService;
import com.vincomobile.fw.core.tools.Constants;
import com.vincomobile.fw.core.tools.Converter;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Devtools.
 * Modelo de ad_column_sync
 *
 * Date: 05/03/2016
 */
@Entity
@Table(name = "ad_column_sync")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdColumnSync extends AdEntityBean {

    private String idColumnSync;
    private String cformat;
    private String csource;
    private String csourceValue;
    private String ctype;
    private Boolean exportable;
    private String idTableSync;
    private Boolean importable;
    private String name;
    private Boolean primaryKey;
    private Long seqno;

    private Object orgValue;
    private String value, oldValue;

    private AdTableSync tableSync;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idColumnSync;
    }

    @Override
    public void setId(String id) {
            this.idColumnSync = id;
    }

    @Id
    @Column(name = "id_column_sync")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdColumnSync() {
        return idColumnSync;
    }

    public void setIdColumnSync(String idColumnSync) {
        this.idColumnSync = idColumnSync;
    }

    @Column(name = "cformat", length = 50)
    @Size(min = 1, max = 50)
    public String getCformat() {
        return cformat;
    }

    public void setCformat(String cformat) {
        this.cformat = cformat;
    }

    @Column(name = "csource", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getCsource() {
        return csource;
    }

    public void setCsource(String csource) {
        this.csource = csource;
    }

    @Column(name = "csource_value", length = 250)
    @Size(min = 1, max = 250)
    public String getCsourceValue() {
        return csourceValue;
    }

    public void setCsourceValue(String csourceValue) {
        this.csourceValue = csourceValue;
    }

    @Column(name = "ctype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    @Column(name = "exportable")
    @NotNull
    public Boolean getExportable() {
        return exportable;
    }

    public void setExportable(Boolean exportable) {
        this.exportable = exportable;
    }

    @Column(name = "id_table_sync")
    @NotNull
    public String getIdTableSync() {
        return idTableSync;
    }

    public void setIdTableSync(String idTableSync) {
        this.idTableSync = idTableSync;
    }

    @Column(name = "importable")
    @NotNull
    public Boolean getImportable() {
        return importable;
    }

    public void setImportable(Boolean importable) {
        this.importable = importable;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "primary_key")
    @NotNull
    public Boolean getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Column(name = "seqno")
    @NotNull
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table_sync", referencedColumnName = "id_table_sync", insertable = false, updatable = false)
    @JsonIgnore
    public AdTableSync getTableSync() {
        return tableSync;
    }

    public void setTableSync(AdTableSync tableSync) {
        this.tableSync = tableSync;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdColumnSync)) return false;

        final AdColumnSync that = (AdColumnSync) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idColumnSync == null) && (that.idColumnSync == null)) || (idColumnSync != null && idColumnSync.equals(that.idColumnSync)));
        result = result && (((cformat == null) && (that.cformat == null)) || (cformat != null && cformat.equals(that.cformat)));
        result = result && (((csource == null) && (that.csource == null)) || (csource != null && csource.equals(that.csource)));
        result = result && (((csourceValue == null) && (that.csourceValue == null)) || (csourceValue != null && csourceValue.equals(that.csourceValue)));
        result = result && (((ctype == null) && (that.ctype == null)) || (ctype != null && ctype.equals(that.ctype)));
        result = result && (((exportable == null) && (that.exportable == null)) || (exportable != null && exportable.equals(that.exportable)));
        result = result && (((idTableSync == null) && (that.idTableSync == null)) || (idTableSync != null && idTableSync.equals(that.idTableSync)));
        result = result && (((importable == null) && (that.importable == null)) || (importable != null && importable.equals(that.importable)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((primaryKey == null) && (that.primaryKey == null)) || (primaryKey != null && primaryKey.equals(that.primaryKey)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

    /**
     * Get string value representation
     *
     * @param value Object
     * @return String value
     */
    private String getValueFromObject(Object value) {
        if (value == null) {
            return null;
        }
        if (AdColumnService.TYPE_DATE.equals(ctype) || AdColumnService.TYPE_DATETIME.equals(ctype)) {
            String format = Converter.isEmpty(cformat) ? (AdColumnService.TYPE_DATE.equals(ctype) ? FWConfig.SQL_DATE_INPUT_FORMAT : FWConfig.SQL_DATETIME_INPUT_FORMAT) : cformat;
            if (value instanceof String)
                value = Converter.getDate((String) value, format);
            return Converter.getSQLDateTime((Date) value, format, false);
        } else if (AdColumnService.TYPE_BOOLEAN.equals(ctype)) {
            if (value instanceof BigDecimal)
                return ((BigDecimal) value).intValue() == 1 ? "1" : "0";
            else {
                if (value instanceof String)
                    value = Converter.getBoolean((String) value);
                if (value instanceof Number)
                    value = ((Number) value).longValue() > 0;
                return (Boolean) value ? "1" : "0";
            }
        } else {
            return value.toString();
        }
    }

    @Transient
    public String getValue() {
        return value;
    }

    /**
     * Updates the value of the field
     *
     * @param value Value
     */
    public void setValue(Object value) {
        orgValue = value;
        this.value = getValueFromObject(value);
    }

    public void setOldValue(Object value) {
        this.oldValue = getValueFromObject(value);
    }

    /**
     * Get the value for SQLite
     *
     * @return Value for SQLite
     */
    @Transient
    @JsonIgnore
    public Object getSqliteValue() {
        if (value == null)
            return null;
        if (AdColumnService.TYPE_BOOLEAN.equals(ctype)) {
            return Converter.getBoolean(value) ? 1 : 0;
        } else if (AdColumnService.TYPE_STRING.equals(ctype)) {
            return value;
        } else if (AdColumnService.TYPE_NUMBER.equals(ctype)) {
            return Converter.getFloat(value);
        } else if (AdColumnService.TYPE_INTEGER.equals(ctype) || AdColumnService.TYPE_AUTOINCREMENT.equals(ctype)) {
            return Converter.getLong("true".equals(value) ? "1" : value);
        } else if (AdColumnService.TYPE_DATE.equals(ctype) || AdColumnService.TYPE_DATETIME.equals(ctype)) {
            if (Converter.isEmpty(value))
                return null;
            String format = Converter.isEmpty(cformat) ? (AdColumnService.TYPE_DATE.equals(ctype) ? FWConfig.SQL_DATE_INPUT_FORMAT : FWConfig.SQL_DATETIME_INPUT_FORMAT) : cformat;
            return Converter.formatDate(Converter.getDate(value, format), format);
        }
        return null;
    }

    /**
     * Get the value for SQL
     *
     * @return SQL value
     */
    @Transient
    @JsonIgnore
    public String getSQLValue() {
        if (AdColumnService.TYPE_BOOLEAN.equals(ctype)) {
            return Converter.isEmpty(value) ? "null" : Converter.getSQLBoolean(Converter.getBoolean(value));
        } else if (AdColumnService.TYPE_STRING.equals(ctype)) {
            return Converter.getSQLString(value);
        } else if (AdColumnService.TYPE_NUMBER.equals(ctype)) {
            return value == null ? "null" : Converter.getSQLFloat(Converter.getFloat(value));
        } else if (AdColumnService.TYPE_INTEGER.equals(ctype) || AdColumnService.TYPE_AUTOINCREMENT.equals(ctype)) {
            return value == null ? "null" : ("true".equals(value) ? "1" : Converter.getSQLLong(value));
        } else if (AdColumnService.TYPE_DATE.equals(ctype)) {
            if (Converter.isEmpty(value) || "null".equals(value))
                return "null";
            String format = Converter.isEmpty(cformat) ? Constants.SQL_DATE_INPUT_FORMAT : cformat;
            if (Constants.DB_TYPE == Constants.DB_TYPE_ORACLE) {
                return "to_date('"+value+"', '"+format+"')";
            } else {
                return Converter.getSQLDate(Converter.getDate(value, format), Constants.SQL_DATE_OUTPUT_FORMAT, true);
            }
        } else if (AdColumnService.TYPE_DATETIME.equals(ctype)) {
            if (Converter.isEmpty(value) || "null".equals(value))
                return "null";
            String format = Converter.isEmpty(cformat) ? Constants.SQL_DATETIME_INPUT_FORMAT : cformat;
            if (Constants.DB_TYPE == Constants.DB_TYPE_ORACLE) {
                return "to_date('"+value+"', '"+format+"')";
            } else {
                return Converter.getSQLDate(Converter.getDate(value, format), Constants.SQL_DATETIME_OUTPUT_FORMAT, true);
            }
        }
        return "null";
    }

    /**
     * Get type for Swift
     *
     * @return Type name
     */
    @Transient
    @JsonIgnore
    public String getSwiftType() {
        String pk = this.primaryKey ? "" : "?";
        if (this.ctype.equals(AdColumnService.TYPE_STRING)) {
            return "String" + pk;
        } else if (this.ctype.equals(AdColumnService.TYPE_INTEGER)) {
            return "Int" + pk;
        } else if (this.ctype.equals(AdColumnService.TYPE_NUMBER)) {
            return "Double" + pk;
        } else if (this.ctype.equals(AdColumnService.TYPE_DATE) || this.ctype.equals(AdColumnService.TYPE_DATETIME)) {
            return "NSDate" + pk;
        } else if (this.ctype.equals(AdColumnService.TYPE_BOOLEAN)) {
            return "Bool" + pk;
        } else if (this.ctype.equals("IMAGE") || this.ctype.equals("FILE")) {
            return "String" + pk;
        }
        return "ERROR";
    }

    @Transient
    @JsonIgnore
    public String getStandardName() {
        return Converter.removeUnderscores(this.name.toLowerCase());
    }

}

