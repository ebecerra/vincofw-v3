package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_image
 *
 * Date: 23/01/2016
 */
@Entity
@Table(name = "ad_image")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdImage extends AdEntityBean {

    private String idImage;
    private String idTable;
    private String idRow;
    private String name;
    private String description;
    private String itype;
    private Long width;
    private Long height;
    private String mimetype;
    private Long seqno;

    private AdTable table;
    private String url;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idImage;
    }

    @Override
    public void setId(String id) {
            this.idImage = id;
    }

    @Id
    @Column(name = "id_image")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    @Column(name = "id_table")
    @NotNull
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_row")
    @NotNull
    public String getIdRow() {
        return idRow;
    }

    public void setIdRow(String idRow) {
        this.idRow = idRow;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "itype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getItype() {
        return itype;
    }

    public void setItype(String itype) {
        this.itype = itype;
    }

    @Column(name = "width")
    @NotNull
    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    @Column(name = "height")
    @NotNull
    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    @Column(name = "mimetype", length = 255)
    @Size(min = 1, max = 255)
    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    @JsonIgnore
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    @Transient
    public String getFile() {
        return name;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdImage)) return false;

        final AdImage that = (AdImage) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idImage == null) && (that.idImage == null)) || (idImage != null && idImage.equals(that.idImage)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idRow == null) && (that.idRow == null)) || (idRow != null && idRow.equals(that.idRow)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((itype == null) && (that.itype == null)) || (itype != null && itype.equals(that.itype)));
        result = result && (((width == null) && (that.width == null)) || (width != null && width.equals(that.width)));
        result = result && (((height == null) && (that.height == null)) || (height != null && height.equals(that.height)));
        result = result && (((mimetype == null) && (that.mimetype == null)) || (mimetype != null && mimetype.equals(that.mimetype)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

    @Transient
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

