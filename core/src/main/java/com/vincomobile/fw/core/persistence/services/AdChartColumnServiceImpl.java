package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdChartColumn;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 09/11/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_chart_column
 */
@Repository
@Transactional(readOnly = true)
public class AdChartColumnServiceImpl extends BaseServiceImpl<AdChartColumn, String> implements AdChartColumnService {

    /**
     * Constructor.
     */
    public AdChartColumnServiceImpl() {
        super(AdChartColumn.class);
    }

    /**
     * Retrieves all the columns of the given chart
     *
     * @param idChart Chart Id
     * @return Chart columns
     */
    @Override
    public List<AdChartColumn> findChartColumns(String idChart) {
        Map<String,String> filter = new HashMap<>();
        filter.put("idChart", idChart);
        return findAll(new SqlSort("seqno", "asc"), filter);
    }

}
