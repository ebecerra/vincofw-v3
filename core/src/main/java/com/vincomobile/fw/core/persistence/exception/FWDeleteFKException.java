package com.vincomobile.fw.core.persistence.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( value = HttpStatus.CONFLICT )
public final class FWDeleteFKException extends RuntimeException {

    public FWDeleteFKException() {
        super();
    }

    public FWDeleteFKException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FWDeleteFKException(final String message) {
        super(message);
    }

    public FWDeleteFKException(final Throwable cause) {
        super(cause);
    }

}
