package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.IConfig;
import com.vincomobile.fw.core.persistence.beans.AdPreferenceValue;
import com.vincomobile.fw.core.persistence.model.AdPermission;
import com.vincomobile.fw.core.persistence.model.AdPreference;

import com.vincomobile.fw.core.tools.Converter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Devtools.
 * Service layer implementation for ad_preference
 * <p/>
 * Date: 17/12/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdPreferenceServiceImpl extends BaseServiceImpl<AdPreference, String> implements AdPreferenceService {

    private static List<IConfig> configList = new ArrayList<IConfig>();

    /**
     * Constructor.
     */
    public AdPreferenceServiceImpl() {
        super(AdPreference.class);
    }

    /**
     * Get preference
     *
     * @param name     Preference name
     * @param idClient Client identifier
     * @param idRole   Role identifier
     * @param idUser   User identifier
     * @param params Custom parameters
     * @return Preference value
     */
    @Override
    public AdPreference getPreference(String name, String idClient, String idRole, String idUser, Object... params) {
        return getPref(name, idClient, idRole, idUser, null, params);
    }

    @Override
    public AdPreference getPreference(String name, String idClient, String idRole, String idUser) {
        return getPref(name, idClient, idRole, idUser, null);
    }

    @Override
    public AdPreference getPreference(String name, String idClient, String idRole) {
        return getPreference(name, idClient, idRole, null);
    }

    @Override
    public AdPreference getPreference(String name, String idClient) {
        return getPreference(name, idClient, null, null);
    }

    /**
     * Get all preferences (Not privileges)
     *
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @return Preference list
     */
    @Override
    public List<AdPreferenceValue> getPreferences(String idClient, String idRole, String idUser) {
        List<String> prefererences = query("SELECT distinct property FROM AdPreference WHERE active = 1 AND privilege = 0");
        List<AdPreferenceValue> result = new ArrayList<>();
        for (String prop : prefererences) {
            AdPreference preference = getPref(prop, idClient, idRole, idUser, null);
            if (preference.getIdPreference() != null) {
                result.add(new AdPreferenceValue(preference.getProperty(), preference.getValue()));
            }
        }
        return result;
    }

    /**
     * Get public preference
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param params Custom parameters
     * @return Preference value
     */
    @Override
    public AdPreference getPublicPreference(String name, String idClient, String idRole, String idUser, Object... params) {
        return getPref(name, idClient, idRole, idUser, true, params);
    }

    @Override
    public AdPreference getPublicPreference(String name, String idClient, String idRole, String idUser) {
        return getPref(name, idClient, idRole, idUser, true);
    }

    @Override
    public AdPreference getPublicPreference(String name, String idClient, String idRole) {
        return getPublicPreference(name, idClient, idRole, null);
    }

    @Override
    public AdPreference getPublicPreference(String name, String idClient) {
        return getPublicPreference(name, idClient, null, null);
    }

    /**
     * Get string preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return String preference value
     */
    @Override
    public String getPreferenceString(String name, String idClient, String idRole, String idUser, String defValue, Object... params) {
        AdPreference preference = this.getPreference(name, idClient, idRole, idUser, params);
        if (preference != null) {
            String value = preference.getValue();
            return Converter.isEmpty(value) ? defValue : value;
        }
        return defValue;
    }

    @Override
    public String getPreferenceString(String name, String idClient, String idRole, String idUser, Object... params) {
        return getPreferenceString(name, idClient, idRole, idUser, null, params);
    }

    @Override
    public String getPreferenceString(String name, String idClient, String defValue) {
        return getPreferenceString(name, idClient, null, null, defValue);
    }

    @Override
    public String getPreferenceString(String name, String idClient) {
        return getPreferenceString(name, idClient, null, null, (String) null);
    }

    /**
     * Get boolean preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Boolean preference value
     */
    @Override
    public Boolean getPreferenceBool(String name, String idClient, String idRole, String idUser, Boolean defValue, Object... params) {
        String value = getPreferenceString(name, idClient, idRole, idUser, params);
        return Converter.getBoolean(value, defValue);
    }

    @Override
    public Boolean getPreferenceBool(String name, String idClient, String idRole, String idUser, Object... params) {
        return getPreferenceBool(name, idClient, idRole, idUser, false, params);
    }

    @Override
    public Boolean getPreferenceBool(String name, String idClient, Boolean defValue) {
        return getPreferenceBool(name, idClient, null, null, defValue);
    }

    @Override
    public Boolean getPreferenceBool(String name, String idClient) {
        return getPreferenceBool(name, idClient, null, null, false);
    }

    /**
     * Get long preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Long preference value
     */
    @Override
    public Long getPreferenceLong(String name, String idClient, String idRole, String idUser, Long defValue, Object... params) {
        String value = getPreferenceString(name, idClient, idRole, idUser, params);
        return Converter.getLong(value, defValue);
    }

    @Override
    public Long getPreferenceLong(String name, String idClient, String idRole, String idUser, Object... params) {
        return getPreferenceLong(name, idClient, idRole, idUser, null, params);
    }

    @Override
    public Long getPreferenceLong(String name, String idClient, Long defValue) {
        return getPreferenceLong(name, idClient, null, null, defValue);
    }

    @Override
    public Long getPreferenceLong(String name, String idClient) {
        return getPreferenceLong(name, idClient, null, null, (Long) null);
    }

    /**
     * Get double preference value
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Double preference value
     */
    @Override
    public Double getPreferenceDouble(String name, String idClient, String idRole, String idUser, Double defValue, Object... params) {
        String value = getPreferenceString(name, idClient, idRole, idUser, params);
        return Converter.getFloat(value, defValue);
    }

    @Override
    public Double getPreferenceDouble(String name, String idClient, String idRole, String idUser, Object... params) {
        return getPreferenceDouble(name, idClient, idRole, idUser, null, params);
    }

    @Override
    public Double getPreferenceDouble(String name, String idClient, Double defValue) {
        return getPreferenceDouble(name, idClient, null, null, defValue);
    }

    @Override
    public Double getPreferenceDouble(String name, String idClient) {
        return getPreferenceDouble(name, idClient, null, null, (Double) null);
    }

    /**
     * Get date preference value
     *
     * @param name Preference name
     * @param format Date format
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param defValue Default value
     * @param params Custom parameters
     * @return Date preference value
     */
    @Override
    public Date getPreferenceDate(String name, String format, String idClient, String idRole, String idUser, Date defValue, Object... params) {
        String value = getPreferenceString(name, idClient, idRole, idUser, params);
        return Converter.getDate(value, format);
    }

    @Override
    public Date getPreferenceDate(String name, String format, String idClient, String idRole, String idUser, Object... params) {
        return getPreferenceDate(name, format, idClient, idRole, idUser, null, params);
    }

    @Override
    public Date getPreferenceDate(String name, String format, String idClient) {
        return getPreferenceDate(name, format, idClient, null, (String) null, (Date) null);
    }

    @Override
    public Date getPreferenceDate(String name, String idClient, String idRole, String idUser, Date defValue, Object... params) {
        return getPreferenceDate(name, "dd/MM/yyyy", idClient, idRole, idUser, defValue, params);
    }

    @Override
    public Date getPreferenceDate(String name, String idClient, String idRole, String idUser, Object... params) {
        return getPreferenceDate(name, "dd/MM/yyyy", idClient, idRole, idUser, null, params);
    }

    @Override
    public Date getPreferenceDate(String name, String idClient) {
        return getPreferenceDate(name, "dd/MM/yyyy", idClient, null, (String) null, (Date) null);
    }


    /**
     * List permissions
     *
     * @param idClient Client identifier
     * @param idRole   Role identifier
     * @param idUser   User identifier
     * @return Permission
     */
    @Override
    public List<AdPermission> listPermission(String idClient, String idRole, String idUser) {
        List<AdPermission> permissions = new ArrayList<>();
        Map filter = new HashMap();
        filter.put("privilege", true);
        SqlSort sort = new SqlSort();
        sort.addSorter("property", "ASC");
        sort.addSorter("visibleatIdUser", "DESC");
        sort.addSorter("visibleatIdRole", "DESC");
        sort.addSorter("visibleatIdClient", "DESC");
        List<AdPreference> result = findAll(sort, filter);
        String lastProperty = "";
        boolean skipProperty = false;
        AdPreference lastPreference = null;
        for (AdPreference preference : result) {
            if (!lastProperty.equals(preference.getProperty())) {
                if (lastPreference != null) {
                    permissions.add(new AdPermission(lastPreference.getProperty(), Converter.getBoolean(lastPreference.getValue())));
                    lastPreference = null;
                }
                lastProperty = preference.getProperty();
                skipProperty = false;
            }
            if (!skipProperty && preference.isValidFor(idClient, idRole, idUser)) {
                lastPreference = preference;
                skipProperty = true;
            }
        }
        if (lastPreference != null) {
            permissions.add(new AdPermission(lastPreference.getProperty(), Converter.getBoolean(lastPreference.getValue())));
        }
        return permissions;
    }

    /**
     * Get preference
     *
     * @param name Preference name
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idUser User identifier
     * @param ispublic Filter by "ispublic"
     * @param params Custom parameters
     * @return Preference value
     */
    private AdPreference getPref(String name, String idClient, String idRole, String idUser, Boolean ispublic, Object... params) {
        if (Converter.isEmpty(name))
            return null;
        Map filter = new HashMap();
        filter.put("property", name);
        filter.put("idClient", Converter.isEmpty(idClient) || FWConfig.ALL_CLIENT_ID.equals(idClient) ? FWConfig.ALL_CLIENT_ID : getClientFilter(idClient));
        filter.put("active", true);
        if (ispublic != null) {
            filter.put("ispublic", ispublic);
        }
        SqlSort sort = new SqlSort();
        sort.addSorter("visibleatCustom", "DESC");
        sort.addSorter("visibleatIdUser", "DESC");
        sort.addSorter("visibleatIdRole", "DESC");
        sort.addSorter("visibleatIdClient", "DESC");
        List<AdPreference> result = findAll(sort, filter);
        for (AdPreference preference : result) {
            if (preference.isValidFor(idClient, idRole, idUser, params)) {
                return preference;
            }
        }
        return new AdPreference();
    }

}
