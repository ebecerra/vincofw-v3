package com.vincomobile.fw.core.process;

import org.springframework.stereotype.Component;

@Component
public class TestingProcess2 extends TestingBaseProcess {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static TestingProcess2 service = new TestingProcess2();

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Execute test
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void test(String idProcessExec) {
        testProcess(idProcessExec, true);
    }

}