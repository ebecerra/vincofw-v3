package com.vincomobile.fw.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by yokiro on 04/11/2015.
 */
public class JSONTable {

    private String tableName;
    List<HashMap<String,Object>> rows;

    public JSONTable(){
        this.rows = new ArrayList<>();
    }

    public JSONTable(String tableName){
        this.tableName = tableName;
        this.rows = new ArrayList<>();
    }

    public List<HashMap<String,Object>> getRows(){
        return this.rows;
    }

    public void addRows(HashMap<String,Object> row){
        this.rows.add(row);
    }

    public void setTableName(String tableName){
        this.tableName = tableName;
    }

    public  String getTableName(){
        return this.tableName;
    }
}
