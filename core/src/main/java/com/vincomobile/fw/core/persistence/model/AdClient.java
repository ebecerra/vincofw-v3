package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.core.tools.Converter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_CLIENT
 * <p/>
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_client")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdClient extends EntityBean<String> {

    private String idClient;
    private String idModule;
    private String name;
    private String description;
    private String nick;
    private String email;
    private String smtpHost;
    private Long smtpPort;
    private Boolean smtpSsl;
    private String smtpUser;
    private String smtpPassword;

    private AdModule module;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idClient;
    }

    @Override
    public void setId(String id) {
        this.idClient = id;
    }

    @Id
    @Column(name = "id_client")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "id_module")
    @NotNull
    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    @Column(name = "name", length = 100, unique = true)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "nick", length = 20)
    @Size(min = 1, max = 20)
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Column(name = "email", length = 150, unique = true)
    @Email
    @Size(min = 1, max = 150)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = Converter.isEmpty(email) ? null : email;
    }

    @Column(name = "smtp_host", length = 100)
    @Size(min = 1, max = 100)
    public String getSmtpHost() {
        return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    @Column(name = "smtp_port")
    public Long getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(Long smtpPort) {
        this.smtpPort = smtpPort;
    }

    @Column(name = "smtp_ssl")
    public Boolean getSmtpSsl() {
        return smtpSsl;
    }

    public void setSmtpSsl(Boolean smtpSsl) {
        this.smtpSsl = smtpSsl;
    }

    @Column(name = "smtp_user", length = 45)
    @Size(min = 1, max = 45)
    public String getSmtpUser() {
        return smtpUser;
    }

    public void setSmtpUser(String smtpUser) {
        this.smtpUser = smtpUser;
    }

    @Column(name = "smtp_password", length = 20)
    @Size(min = 1, max = 20)
    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_module", referencedColumnName = "id_module", insertable = false, updatable = false)
    public AdModule getModule() {
        return module;
    }

    public void setModule(AdModule module) {
        this.module = module;
    }

    /**
     * Implementa el equals
     *
     * @param aThat Object to compare with
     * @return true/false
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdClient)) return false;

        final AdClient that = (AdClient) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((idModule == null) && (that.idModule == null)) || (idModule != null && idModule.equals(that.idModule)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((nick == null) && (that.nick == null)) || (nick != null && nick.equals(that.nick)));
        result = result && (((email == null) && (that.email == null)) || (email != null && email.equals(that.email)));
        result = result && (((smtpHost == null) && (that.smtpHost == null)) || (smtpHost != null && smtpHost.equals(that.smtpHost)));
        result = result && (((smtpPort == null) && (that.smtpPort == null)) || (smtpPort != null && smtpPort.equals(that.smtpPort)));
        result = result && (((smtpSsl == null) && (that.smtpSsl == null)) || (smtpSsl != null && smtpSsl.equals(that.smtpSsl)));
        result = result && (((smtpUser == null) && (that.smtpUser == null)) || (smtpUser != null && smtpUser.equals(that.smtpUser)));
        result = result && (((smtpPassword == null) && (that.smtpPassword == null)) || (smtpPassword != null && smtpPassword.equals(that.smtpPassword)));
        return result;
    }

    @Transient
    public boolean isSmtpConfigured() {
        return !Converter.isEmpty(smtpHost) && !Converter.isEmpty(smtpUser) &&!Converter.isEmpty(smtpPassword) && !Converter.isEmpty(smtpPort);
    }
}

