package com.vincomobile.fw.core.process.importing;

import com.vincomobile.fw.core.process.ProcessDefinition;

import java.util.ArrayList;
import java.util.List;

public class ImportErrors {

    ProcessDefinition process;
    String idProcessExec;
    List<FileErrors> errors;

    public ImportErrors(ProcessDefinition process, String idProcessExec) {
        this.process = process;
        this.idProcessExec = idProcessExec;
        this.errors = new ArrayList<>();
    }

    public void addError(String fileName, BaseError err) {
        String level = err.getLevel();
        if (BaseError.LEVEL_WARNING.equals(level)) {
            process.warn(idProcessExec, err.getError());
        } else if (BaseError.LEVEL_ERROR.equals(level)) {
            process.error(idProcessExec, err.getError());
        }
        for (FileErrors fileError : errors) {
            if (fileError.fileName.equals(fileName)) {
                fileError.addError(err);
                return;
            }
        }
        FileErrors fileError = new FileErrors(fileName);
        fileError.addError(err);
        errors.add(fileError);
    }

    public boolean isSuccess() {
        for (FileErrors fileError : errors) {
            if (!fileError.isSuccess()) {
                return false;
            }
        }

        return true;
    }

}
