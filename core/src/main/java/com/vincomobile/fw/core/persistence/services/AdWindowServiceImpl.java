package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdWindow;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service of AD_WINDOW
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdWindowServiceImpl extends BaseServiceImpl<AdWindow, String> implements AdWindowService {

    /**
     * Constructor.
     *
     */
    public AdWindowServiceImpl() {
        super(AdWindow.class);
    }

}
