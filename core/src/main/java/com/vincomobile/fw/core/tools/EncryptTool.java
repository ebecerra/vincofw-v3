package com.vincomobile.fw.core.tools;

public class EncryptTool {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("Numero de parámetros incorrecto");
            System.out.println("\tEjemplo:");
            System.out.println("\t\tcom.vincomobile.fw.tools \"Frase secreta\" \"Texto a encriptar\"");
            return;
        }
        System.out.println("Texto encriptado:");
        String text = Converter.encrypt(args[0], args[1]);
        System.out.println(text);
        System.out.println("Texto plano:");
        System.out.println(Converter.decrypt(args[0], text));
    }
}
