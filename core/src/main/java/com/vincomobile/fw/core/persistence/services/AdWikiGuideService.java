package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdGuide;
import com.vincomobile.fw.core.persistence.model.AdWikiGuide;

import java.util.List;

/**
 * Created by Vincomobile FW on 29/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_wiki_guide
 */
public interface AdWikiGuideService extends BaseService<AdWikiGuide, String> {

    String ROOT_TOPIC = "ROOT";

    /**
     * List available guides and it topics
     *
     * @param idClient Client identifier
     * @param idRole Role identifier
     * @param idLanguage Language identifier
     * @return Guides
     */
    List<AdGuide> getGuides(String idClient, String idRole, String idLanguage);
}


