package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Created by Devtools.
 * Modelo de ad_ref_search_column
 *
 * Date: 07/11/2015
 */
@Entity
@Table(name = "ad_ref_search_column")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdRefSearchColumn extends AdEntityBean {

    private String idRefSearchColumn;
    private String idRefTable;
    private String idColumn;
    private Long seqno;

    private AdColumn column;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idRefSearchColumn;
    }

    @Override
    public void setId(String id) {
            this.idRefSearchColumn = id;
    }

    @Id
    @Column(name = "id_ref_search_column")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdRefSearchColumn() {
        return idRefSearchColumn;
    }

    public void setIdRefSearchColumn(String idRefSearchColumn) {
        this.idRefSearchColumn = idRefSearchColumn;
    }

    @Column(name = "id_ref_table")
    @NotNull
    public String getIdRefTable() {
        return idRefTable;
    }

    public void setIdRefTable(String idRefTable) {
        this.idRefTable = idRefTable;
    }

    @Column(name = "id_column")
    @NotNull
    public String getIdColumn() {
        return idColumn;
    }

    public void setIdColumn(String idColumn) {
        this.idColumn = idColumn;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_column", referencedColumnName = "id_column", insertable = false, updatable = false)
    public AdColumn getColumn() {
        return column;
    }

    public void setColumn(AdColumn column) {
        this.column = column;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdRefSearchColumn)) return false;

        final AdRefSearchColumn that = (AdRefSearchColumn) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idRefSearchColumn == null) && (that.idRefSearchColumn == null)) || (idRefSearchColumn != null && idRefSearchColumn.equals(that.idRefSearchColumn)));
        result = result && (((idRefTable == null) && (that.idRefTable == null)) || (idRefTable != null && idRefTable.equals(that.idRefTable)));
        result = result && (((idColumn == null) && (that.idColumn == null)) || (idColumn != null && idColumn.equals(that.idColumn)));
        return result;
    }

}

