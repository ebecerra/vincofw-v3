package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdGuideTopic;
import com.vincomobile.fw.core.persistence.model.AdWikiTopic;

import java.util.List;

/**
 * Created by Vincomobile FW on 29/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_wiki_topic
 */
public interface AdWikiTopicService extends BaseService<AdWikiTopic, String> {

    /**
     * Load a topic tree
     *
     * @param parent Parent topic
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param idWikiGuide Guide identifier
     * @return Topic tree
     */
    List<AdGuideTopic> loadTopicTree(String parent, String idClient, String idLanguage, String idWikiGuide);
}


