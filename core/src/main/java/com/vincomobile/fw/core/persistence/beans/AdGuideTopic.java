package com.vincomobile.fw.core.persistence.beans;

import java.util.List;

public class AdGuideTopic {

    private String idWikiTopic;
    private String name;
    private String title;
    private String parentTopic;
    private Long seqno;

    List<AdGuideTopic> topics;

    public AdGuideTopic(String idWikiTopic, String name, String title, String parentTopic, Long seqno) {
        this.idWikiTopic = idWikiTopic;
        this.name = name;
        this.title = title;
        this.parentTopic = parentTopic;
        this.seqno = seqno;
    }

    public String getIdWikiTopic() {
        return idWikiTopic;
    }

    public void setIdWikiTopic(String idWikiTopic) {
        this.idWikiTopic = idWikiTopic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentTopic() {
        return parentTopic;
    }

    public void setParentTopic(String parentTopic) {
        this.parentTopic = parentTopic;
    }

    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    public List<AdGuideTopic> getTopics() {
        return topics;
    }

    public void setTopics(List<AdGuideTopic> topics) {
        this.topics = topics;
    }
}
