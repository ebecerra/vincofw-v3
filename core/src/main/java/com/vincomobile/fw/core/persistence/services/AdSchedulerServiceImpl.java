package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdScheduler;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 02/08/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_scheduler
 */
@Repository
@Transactional(readOnly = true)
public class AdSchedulerServiceImpl extends BaseServiceImpl<AdScheduler, String> implements AdSchedulerService {

    /**
     * Constructor.
     */
    public AdSchedulerServiceImpl() {
        super(AdScheduler.class);
    }

}
