package com.vincomobile.fw.core.persistence.charts;

import com.vincomobile.fw.core.persistence.beans.AdChartInfo;
import com.vincomobile.fw.core.persistence.beans.AdChartResult;
import com.vincomobile.fw.core.persistence.model.AdChart;
import com.vincomobile.fw.core.persistence.model.AdChartFilter;
import com.vincomobile.fw.core.persistence.model.AdChartLinked;

import java.util.List;
import java.util.Map;

public class ChartDefinitionImpl implements ChartDefinition {

    protected String idClient;
    protected String idLanguage;
    protected AdChartResult result;
    protected AdChart chart;
    protected List<AdChartFilter> filters;
    protected List<AdChartLinked> chartLinkeds;
    protected Map<String, Object> constraints;
    protected AdChartInfo chartInfo;

    /**
     * Set client id
     *
     * @param idClient Client id
     */
    @Override
    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    /**
     * Set language id
     *
     * @param idLanguage Language id
     */
    @Override
    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    /**
     * Set chart result
     *
     * @param result Chart result
     */
    @Override
    public void setChartResult(AdChartResult result) {
        this.result = result;
    }

    /**
     * Set chart
     *
     * @param chart Chart
     */
    @Override
    public void setChart(AdChart chart) {
        this.chart = chart;
    }

    /**
     * Set chart filters
     *
     * @param filters Filters
     */
    @Override
    public void setFilters(List<AdChartFilter> filters) {
        this.filters = filters;
    }

    /**
     * Set linked charts
     *
     * @param chartLinkeds linked chart list
     */
    @Override
    public void setChartLinkeds(List<AdChartLinked> chartLinkeds) {
        this.chartLinkeds = chartLinkeds;
    }

    /**
     * Chart constraints
     *
     * @param constraints Constraints
     */
    @Override
    public void setConstraints(Map<String, Object> constraints) {
        this.constraints = constraints;
    }

    /**
     * Set chart information
     *
     * @param chartInfo Chart information
     */
    @Override
    public void setChartInfo(AdChartInfo chartInfo) {
        this.chartInfo = chartInfo;
    }

    /**
     * Replace where parameters, constraints, filters and linked charts
     *
     * @param where Original where
     * @return Replace where
     */
    protected String replaceWhere(String where) {
        where = ChartUtils.appendConditionalExpression(where, constraints);
        where = where.replaceAll("@idClient", idClient);
        where = ChartUtils.replaceRangedParameters(where, constraints, filters);
        where = ChartUtils.replaceSingleParameters(where, constraints, chartLinkeds);
        return where;
    }
}
