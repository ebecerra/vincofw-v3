package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdProcessExec;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.Datetool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Calendar;

/**
 * Created by Devtools.
 * Servicio de ad_process_exec
 *
 * Date: 28/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdProcessExecServiceImpl extends BaseServiceImpl<AdProcessExec, String> implements AdProcessExecService {

    private Logger logger = LoggerFactory.getLogger(AdProcessExecServiceImpl.class);

    /**
     * Constructor.
     */
    public AdProcessExecServiceImpl() {
        super(AdProcessExec.class);
    }

    /**
     * Clear process execution table
     *
     * @param days Number of days to preserve
     * @return Number of remove rows
     */
    @Override
    public int clearProcessExec(int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -days);
        logger.debug("clearProcessExec: Clear logs since: " + Converter.formatDate(cal.getTime()));
        Query query = entityManager.createQuery("DELETE FROM AdProcessExec WHERE created < :created");
        query.setParameter("created", cal.getTime());
        return query.executeUpdate();
    }
}
