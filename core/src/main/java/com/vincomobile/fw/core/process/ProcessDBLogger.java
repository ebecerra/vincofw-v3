package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.model.AdProcess;
import com.vincomobile.fw.core.persistence.model.AdProcessExec;
import com.vincomobile.fw.core.persistence.model.AdProcessLog;
import com.vincomobile.fw.core.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.UUID;

public class ProcessDBLogger {

    private Logger logger = LoggerFactory.getLogger(ProcessDBLogger.class);

    /**
     * Initialize process execution
     *
     * @param process Process
     * @param idUser User identifier
     * @return Process Execution Identifier
     */
    public String initExec(final AdProcess process, final String idUser) {
        Date created = new Date();
        AdProcessExec processExec = new AdProcessExec();
        processExec.setIdProcessExec(UUID.randomUUID().toString().replaceAll("-", ""));
        processExec.setIdClient(process.getIdClient());
        processExec.setIdModule(process.getIdModule());
        processExec.setIdProcess(process.getIdProcess());
        processExec.setIdUser(idUser);
        processExec.setCreated(created);
        processExec.setUpdated(created);
        processExec.setActive(true);
        processExec.setStarted(created);
        processExec.setStatus(ProcessDefinition.STATUS_RUNNING);
        FWConfig.proccessLogger.addProcessExec(processExec);
        return processExec.getIdProcessExec();
    }

    /**
     * Finish process execution
     *
     * @param idProcessExec Process Execution Identifier
     * @param status Finish status
     */
    public void finishExec(String idProcessExec, String status) {
        AdProcessExec processExec = FWConfig.proccessLogger.getProcessExec(idProcessExec);
        if (processExec != null) {
            processExec.setFinished(new Date());
            processExec.setStatus(status);
        } else {
            logger.error("Not found AdProcessExec: " + idProcessExec);
        }
    }

    /**
     * Save log to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param type Log type
     * @param log  Log info
     */
    public AdProcessLog logDB(String idProcessExec, String type, String log) {
        AdProcessExec processExec = FWConfig.proccessLogger.getProcessExec(idProcessExec);
        if (processExec != null) {
            Date created = new Date();
            AdProcessLog processLog = new AdProcessLog();
            processLog.setIdProcessLog(UUID.randomUUID().toString().replaceAll("-", ""));
            processLog.setIdClient(processExec.getIdClient());
            processLog.setIdModule(processExec.getIdModule());
            processLog.setCreated(created);
            processLog.setUpdated(created);
            processLog.setActive(true);
            processLog.setIdProcessExec(idProcessExec);
            processLog.setLtype(type);
            processLog.setLog(Converter.truncText(log, 65500));
            FWConfig.proccessLogger.addProcessLog(processExec, processLog);
            return processLog;
        } else {
            logger.error("Not found AdProcessExec: " + idProcessExec);
            return null;
        }
    }

    /**
     * Get start time
     *
     * @param idProcess Process Execution Identifier
     * @return Start time
     */
    public Date getStarted(String idProcess) {
        AdProcessExec processExec = FWConfig.proccessLogger.getProcess(idProcess);
        return processExec != null ? processExec.getStarted() : null;
    }

    /**
     * Get finish time
     *
     * @param idProcess Process Execution Identifier
     * @return Finish time
     */
    public Date getFinished(String idProcess) {
        AdProcessExec processExec = FWConfig.proccessLogger.getProcess(idProcess);
        return processExec != null ? processExec.getFinished() : null;
    }

}
