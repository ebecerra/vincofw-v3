package com.vincomobile.fw.core.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.core.JSONTable;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.*;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import com.vincomobile.fw.core.persistence.services.AdProcessService;
import com.vincomobile.fw.core.persistence.services.AdTableService;
import com.vincomobile.fw.core.process.ProcessExecError;
import com.vincomobile.fw.core.tools.Converter;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseUtils {

    public static final String DB_PROCESS_EXPORT_SAMPLEDATA = "ff80818169b97d490169b9e674cf0003";
    public static final String DB_PROCESS_IMPORT_SAMPLEDATA = "ff80818169ba172e0169ba9f41290002";

    /**
     * Get directory for Export o Import
     *
     * @param idClient Client identifier
     * @param path Relative path
     * @param create Create directories if not exists
     * @return Directory
     */
    public static File getWorkDirectory(String idClient, String path, boolean create) {
        String apacheServerCache = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        File dir = new File(apacheServerCache + path);
        if (create && !dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    /**
     * Export data to JSON object
     *
     * @param items Items to export
     * @param columns Table columns
     * @param jsonTable JSON table
     * @return Exported rows count
     */
    public static int exportDataToJSON(List<Object[]> items, List<AdColumn> columns, JSONTable jsonTable) {
        int exported = 0;
        for (Object[] item : items) {
            HashMap<String, Object> data = new HashMap<>();
            for (int i = 0; i < columns.size(); i++) {
                AdColumn aColumn = columns.get(i);
                data.put(aColumn.getName(), item[i]);
            }
            jsonTable.addRows(data);
            exported++;
        }
        return exported;
    }

    /**
     * Write JSON to a file
     *
     * @param directory Directory
     * @param tableName Table name
     * @param jsonTable JSON data
     * @throws IOException
     */
    public static void writeToJSON(File directory, String tableName, JSONTable jsonTable) throws IOException {
        String fullPath = directory.getPath() + "/" + tableName + ".json";
        ObjectMapper mapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        mapper.setDateFormat(df);
        // Object to JSON in file
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(fullPath), jsonTable);
    }

    /**
     * Get select for get row by key
     *
     * @param tableName Table name
     * @param primaryKey Primary key field name
     * @param valueKey Primary key field value
     * @return SQL
     */
    public static String getTableSelectByPk(String tableName, String primaryKey, String valueKey) {
        // Creating query to obtain row by value key
        String sqlQuery = "SELECT * FROM " + tableName;
        sqlQuery += " WHERE " + primaryKey + " = " + Converter.getSQLString(valueKey);
        return sqlQuery;
    }

    /**
     * Get SQL value (for UPDATE or INSERT)
     *
     * @param columName Column name
     * @param value Value
     * @param idUser User identifier
     * @return SQL value
     */
    public static String getSQLValue(String columName, Object value, String idUser) {
        if ("updated_by".equals(columName) || ("created_by".equals(columName) && value == null)) {
            value = idUser;
        }
        if (value != null) {
            if ("true".equals(value.toString()) || "false".equals(value.toString()))
                return value.toString();
            else
                return Converter.getSQLString(value.toString(), true);
        } else {
            return "null";
        }
    }

    /**
     * Build INSERT
     *
     * @param sql Field part
     * @param valuesIn Values part
     * @return INSERT
     */
    public static String getSQLInsert(String sql, String valuesIn) {
        sql = sql.substring(0, sql.length() - 2);
        sql += ")";
        valuesIn = valuesIn.substring(0, valuesIn.length() - 2);
        valuesIn += ")";
        return sql + valuesIn;
    }

    /**
     * Get WHERE condition for deleting not imported rows
     *
     * @param threadId Thread identifier
     * @param table Table information
     * @param idClientData Client selector
     * @return WHERE condition
     */
    public static String getWHEREDelete(long threadId, AdTable table, String idClientData) {
        String where =  " WHERE not exists (" +
                "SELECT table_key FROM ad_import_delete " +
                "WHERE process_id = " + threadId + " AND table_name = " + Converter.getSQLString(table.getName()) +
                " AND table_key = " + table.getKeyFields().get(0).getName() + ")";
        if (idClientData != null) {
            where += " AND id_client = " + Converter.getSQLString(idClientData, true);
        }
        return where;
    }

    /**
     * Log error
     *
     * @param logger Logger
     * @param sql SQL
     * @param e Exception
     * @param valueCol Value
     */
    public static void logError(Logger logger, String sql, Throwable e, Object valueCol) {
        logger.error("SQL: " + sql);
        logger.error("Error", e);
        if (valueCol != null) {
            logger.error(valueCol.toString());
        }
    }

    /**
     * Log error
     *
     * @param logger Logger
     * @param tableService Table Service
     * @param table Table
     * @param where Where condition
     */
    public static void logError(Logger logger, AdTableService tableService, AdTable table, String where) {
        try {
            List<Map<String, Object>> tableKeys = tableService.executeNativeSelectSql("SELECT " + table.getKeyFields().get(0).getName() + " FROM " + table.getName() + where, 10);
            for (Map<String, Object> item: tableKeys) {
                logger.error(item.toString());
            }
        } catch (SQLException e) {
            logger.error("Error", e);
        }
    }

    /**
     * Execute Import Sample data process
     *
     * @param logger Logger
     * @param processService Process Service
     * @param user User
     * @param idClient Client identifier
     * @param idClientData Client identifier data
     * @param idSampledata Sample data identifier
     */
    public static void execImportSampledata(Logger logger, AdProcessService processService, AdUser user, String idClient, String idClientData, String idSampledata) {
        AdProcess process = processService.findById(DB_PROCESS_IMPORT_SAMPLEDATA);
        if (process == null) {
            logger.error("Can not find: Import sample data process");
            return;
        }
        // Create parameter list
        List<AdProcessParam> paramList = new ArrayList<>();
        paramList.add(ProcessUtils.getInParam(process.getIdProcess(), "idClientData", idClientData));
        paramList.add(ProcessUtils.getInParam(process.getIdProcess(), "idSampledata", idSampledata));
        ProcessExecError exec = processService.exec(process, user, idClient, null, paramList, true, null, null);
        if (!exec.isSuccess()) {
            logger.error("Can not execute: Import sample data process");
            logger.error(exec.getError());
        }
    }
}
