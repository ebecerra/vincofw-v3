package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdClient;
import com.vincomobile.fw.core.persistence.model.AdClientModule;

import java.util.List;

/**
 * Created by Devtools.
 * Interface del servicio de ad_client_module
 *
 * Date: 28/11/2015
 */
public interface AdClientModuleService extends BaseService<AdClientModule, String> {

    /**
     * List all clients for module
     *
     * @param idModule Module identifier
     * @return Client list
     */
    List<AdClient> listClients(String idModule);
}


