package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdChartLinked;

import java.util.List;

/**
 * Created by Vincomobile FW on 04/10/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_chart_linked
 */
public interface AdChartLinkedService extends BaseService<AdChartLinked, String> {

    /**
     * Get linked chart
     *
     * @param idClient Client identifier
     * @param idChart Chart identifier
     * @return List of linked charts
     */
    List<AdChartLinked> getChartLinkeds(String idClient, String idChart);
}


