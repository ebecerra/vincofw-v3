package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdAllMenu;
import com.vincomobile.fw.core.persistence.model.AdMenu;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_MENU
 *
 * Date: 19/02/2015
 */
public interface AdMenuService extends BaseService<AdMenu, String> {

    String ACTION_MANUAL    = "MANUAL";
    String ACTION_WINDOW    = "WINDOW";
    String ACTION_PROCESS   = "PROCESS";

    /**
     * Loads the submenus recursively
     *
     * @param menu     Parent menu
     * @param clients  Clients ids
     * @param idRole   Role id
     * @return Menu list
     */
    List<AdMenu> loadSubmenu(AdMenu menu, List<String> clients, String idRole);

    /**
     * Gets submenu list
     *
     * @param parentMenu Parent menu
     * @param clients    Clients ids
     * @param idRole     Role id
     * @return Submenu list
     */
    List<AdMenu> findSubmenu(AdMenu parentMenu, List<String> clients, String idRole);

    /**
     * Expands a menu
     *
     * @param result   Result list
     * @param mainMenu Main menu (menu to expand)
     */
    void expandMenu(List<AdMenu> result, List<AdMenu> mainMenu);

    /**
     * Expands a menu, includes the menus with no children
     *
     * @param result   Result list
     * @param mainMenu Main menu (menu to expand)
     */
    void expandMenuFull(List<AdMenu> result, List<AdMenu> mainMenu);

    /**
     * Build full menu tree for a role
     *
     * @param idRole Role identifier
     * @return Full menu tree
     */
    List<AdAllMenu> buildRoleMenu(String idRole);
}


