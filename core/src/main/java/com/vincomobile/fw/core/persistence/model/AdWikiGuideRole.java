package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 30/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_wiki_guide_role
 */
@Entity
@Table(name = "ad_wiki_guide_role")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdWikiGuideRole extends AdEntityBean {

    private String idWikiGuideRole;
    private String idWikiGuide;
    private String idRole;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idWikiGuideRole;
    }

    @Override
    public void setId(String id) {
            this.idWikiGuideRole = id;
    }

    @Id
    @Column(name = "id_wiki_guide_role")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdWikiGuideRole() {
        return idWikiGuideRole;
    }

    public void setIdWikiGuideRole(String idWikiGuideRole) {
        this.idWikiGuideRole = idWikiGuideRole;
    }

    @Column(name = "id_wiki_guide", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdWikiGuide() {
        return idWikiGuide;
    }

    public void setIdWikiGuide(String idWikiGuide) {
        this.idWikiGuide = idWikiGuide;
    }

    @Column(name = "id_role", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdWikiGuideRole)) return false;

        final AdWikiGuideRole that = (AdWikiGuideRole) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idWikiGuideRole == null) && (that.idWikiGuideRole == null)) || (idWikiGuideRole != null && idWikiGuideRole.equals(that.idWikiGuideRole)));
        result = result && (((idWikiGuide == null) && (that.idWikiGuide == null)) || (idWikiGuide != null && idWikiGuide.equals(that.idWikiGuide)));
        result = result && (((idRole == null) && (that.idRole == null)) || (idRole != null && idRole.equals(that.idRole)));
        return result;
    }


}

