package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdFieldGroup;

/**
 * Created by Devtools.
 * Interface of service of AD_FIELD_GROUP
 *
 * Date: 19/02/2015
 */
public interface AdFieldGroupService extends BaseService<AdFieldGroup, String> {

}


