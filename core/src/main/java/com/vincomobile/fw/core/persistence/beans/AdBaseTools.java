package com.vincomobile.fw.core.persistence.beans;

import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.services.BaseService;
import org.apache.commons.beanutils.ConvertUtils;
import org.springframework.beans.BeanUtils;

import javax.persistence.Query;
import java.beans.PropertyDescriptor;
import java.text.SimpleDateFormat;
import java.util.*;

public class AdBaseTools {

    /**
     * Auxiliary function to obtain the values of a row
     *
     * @param cols  List of columns
     * @param index Index
     * @return Value
     */
    public static Long getLong(Object[] cols, int index) {
        return cols[index] != null ? ((Number) cols[index]).longValue() : null;
    }

    public static Long getLong(Object[] cols, int index, long defaultValue) {
        return cols[index] != null ? ((Number) cols[index]).longValue() : defaultValue;
    }

    public static Integer getInteger(Object[] cols, int index) {
        return cols[index] != null ? ((Number) cols[index]).intValue() : null;
    }

    public static Integer getInteger(Object[] cols, int index, int defaultValue) {
        return cols[index] != null ? ((Number) cols[index]).intValue() : defaultValue;
    }

    public static Date getDate(Object[] cols, int index) {
        return cols[index] != null ? ((Date) cols[index]) : null;
    }

    public static Date getDate(Object[] cols, int index, Date defaultValue) {
        return cols[index] != null ? ((Date) cols[index]) : defaultValue;
    }

    public static String getString(Object[] cols, int index) {
        return cols[index] != null ? ((String) cols[index]) : null;
    }

    public static String getString(Object[] cols, int index, String defaultValue) {
        return cols[index] != null ? ((String) cols[index]) : defaultValue;
    }

    public static Double getDouble(Object[] cols, int index) {
        return cols[index] != null ? ((Number) cols[index]).doubleValue() : null;
    }

    public static Double getDouble(Object[] cols, int index, double defaultValue) {
        return cols[index] != null ? ((Number) cols[index]).doubleValue() : defaultValue;
    }

    public static Boolean getBoolean(Object[] cols, int index) {
        Object item = cols[index];
        return item != null ? ((item instanceof Boolean) ? (Boolean) item : ((Number) item).longValue() != 0) : null;
    }

    public static Boolean getBoolean(Object[] cols, int index, boolean defaultValue) {
        Object item = cols[index];
        return item != null ? ((item instanceof Boolean) ? (Boolean) item : ((Number) item).longValue() != 0) : defaultValue;
    }

    public static Date formatDate(String valueStr) {
        Date value = null;
        try {
            // Short date
            value = (new SimpleDateFormat("dd/MM/yyyy")).parse(valueStr);
        } catch (Throwable e21) {
            try {
                // Long date
                value = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).parse(valueStr);
            } catch (Throwable e3) {
                try {
                    // Long date
                    value = (new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss")).parse(valueStr);
                } catch (Throwable e4) {
                }
            }
        }

        return value;
    }

    /**
     * Replace with preference value
     *
     * @param idClient Client identifier
     * @param value Value parameters to replace
     * @return Preference replaced
     */
    public static String replacePreference(String idClient, String value) {
        int paramIndx = value.indexOf("@PREFERENCE$");
        if (paramIndx >= 0) {
            String param = value.substring(value.indexOf("$") + 1, value.lastIndexOf("$"));
            String paramValue = CacheManager.getPreference(idClient, param).getString();
            value = value.replaceAll("\\@PREFERENCE\\$" + param + "\\$", paramValue);
        }
        return value;
    }

    /**
     * Converts the value of a property of the given class
     *
     * @param clazz Class
     * @param value Value
     * @return Converted object
     */
    public static Object convert(Class clazz, Object value) {
        if (value != null) {
            if (Collection.class.isAssignableFrom(value.getClass())) {
                return convertCollect(clazz, ((Collection) value));
            } else {
                Object obj = null;
                // String are formatted elsewhere
                if (value instanceof String && Date.class.isAssignableFrom(clazz)) {
                    obj = formatDate((String) value);

                    if (obj != null) {
                        return obj;
                    }
                }

                obj = value;
                try {
                    obj = ConvertUtils.convert(value, clazz);
                } catch (Throwable e) {
                    // TOOD
                }
                return obj;
            }
        } else {
            return null;
        }
    }

    public static List convertCollect(Class clazz, Collection items) {
        List result = new ArrayList();
        for (Object item : items) {
            result.add(convert(clazz, item));
        }

        return result;
    }

    /**
     * Gets the property description of a property of a class
     *
     * @param clazz    Class
     * @param property Property
     * @return Description
     */
    public static PropertyDescriptor getPropertyDescription(Class clazz, String property) {
        String[] properties = property.split("\\.");

        PropertyDescriptor desc = null;
        for (String prop : properties) {
            desc = BeanUtils.getPropertyDescriptor(clazz, prop);
            if (desc == null) {
                return null;
            }
            clazz = desc.getPropertyType();
        }
        return desc;
    }


    public static void setQueryParams(Query query, Map filters) {
        if (filters != null && !filters.isEmpty()) {
            Iterator itKeys = filters.entrySet().iterator();

            while (itKeys.hasNext()) {
                Map.Entry pairs = (Map.Entry) itKeys.next();
                Object value = pairs.getValue();
                try {
                    if (((List) value).size() == 1) {
                        if (((List) value).get(0).equals("-"))
                            continue;
                        if (((List) value).get(0).equals("!-"))
                            continue;
                    }
                } catch (Exception ex) {
                }
                if (value != null && !value.equals("null")) {
                    if (pairs.getKey().equals("|") || pairs.getKey().equals("&"))
                        setQueryParams(query, (Map) pairs.getValue());
                    else
                        query.setParameter(getKeyParam((String) pairs.getKey()), value);
                }
            }
        }
    }

    public static Object getParameter(String name, Map<String, Object> searchs) {
        if (searchs.containsKey(name))
            return searchs.get(name);
        return null;
    }

    public static String getKeyParam(String key) {
        return key.replace(".", "_");
    }

    public static String buildOrCriteria(String property, List list) {
        String criteria = "( (1=0) ";
        for (Object current : list) {
            String element = (String) current;

            if (element.equals("-"))
                criteria += " or " + property + " is null";
            if (element.equals("!-"))
                criteria += " or " + property + " is not null";
        }
        criteria += ") ";
        return criteria;
    }

    public static String buildWhereCriteriaBetween(Map filters, Class clazz, String key, String value) {
        String beginStr = value.substring(0, value.indexOf(BaseService.QUERY_BETWEEN_OP));
        String endStr = value.substring(value.indexOf(BaseService.QUERY_BETWEEN_OP) + 1);

        beginStr = beginStr.trim();
        if (beginStr.length() == 0) {
            beginStr = null;
        }
        endStr = endStr.trim();
        if (endStr.length() == 0) {
            endStr = null;
        }

        return buildWhereCriteriaBetween(filters, clazz, key, beginStr, endStr);
    }

    public static String buildWhereCriteriaBetween(Map filters, Class clazz, String key, String beginStr, String endStr) {
        //Date beginDate = formatDate(beginStr);
        //Date endDate = formatDate(endStr);
        Object begin = AdBaseTools.convert(clazz, beginStr);
        Object end = AdBaseTools.convert(clazz, endStr);

        return buildWhereCriteriaBetweenValue(filters, clazz, key, begin, end);
    }

    public static String buildWhereCriteriaBetweenValue(Map filters, Class clazz, String key, Object beginDate, Object endDate) {
        String keyBegin = key + ".begin";
        String paramBegin = getKeyParam(keyBegin);
        String keyEnd = key + ".end";
        String paramEnd = getKeyParam(keyEnd);

        filters.remove(key);
        if (beginDate != null) {
            filters.put(keyBegin, beginDate);
        }
        if (endDate != null && Date.class.isAssignableFrom(clazz)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime((Date) endDate);
            cal.add(Calendar.DAY_OF_MONTH, 1);
            cal.add(Calendar.MILLISECOND, -1);
            Date endOfDay = cal.getTime();
            filters.put(keyEnd, endOfDay);
        } else
            filters.put(keyEnd, endDate);

        if (Date.class.isAssignableFrom(clazz) && beginDate != null && endDate != null) {
            return key + " between :" + paramBegin + " and :" + paramEnd;
        } else if (!Date.class.isAssignableFrom(clazz) && beginDate != null && endDate != null) {
            return key + " >= :" + paramBegin + " and " + key + " <= :" + paramEnd;
        } else if (beginDate != null) {
            return key + " >= :" + paramBegin;
        } else if (endDate != null) {
            return key + " <= :" + paramEnd;
        } else {
            return null;
        }
    }


}
