package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdDbscriptSql;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 03/10/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_dbscript_sql
 */
@Repository
@Transactional(readOnly = true)
public class AdDbscriptSqlServiceImpl extends BaseServiceImpl<AdDbscriptSql, String> implements AdDbscriptSqlService {

    /**
     * Constructor.
     */
    public AdDbscriptSqlServiceImpl() {
        super(AdDbscriptSql.class);
    }

}
