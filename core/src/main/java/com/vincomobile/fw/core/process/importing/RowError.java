package com.vincomobile.fw.core.process.importing;

import org.apache.poi.ss.usermodel.Row;

public class RowError {

    Row row;

    public Row getRow() {
        return row;
    }

    public void setRow(Row row) {
        this.row = row;
    }
}
