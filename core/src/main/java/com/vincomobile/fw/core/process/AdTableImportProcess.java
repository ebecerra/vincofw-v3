package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.*;
import com.vincomobile.fw.core.persistence.services.AdColumnService;
import com.vincomobile.fw.core.persistence.services.AdRefTableService;
import com.vincomobile.fw.core.persistence.services.AdReferenceService;
import com.vincomobile.fw.core.persistence.services.AdTableService;
import com.vincomobile.fw.core.process.importing.RowError;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.ExcelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class AdTableImportProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdTableImportProcess service = new AdTableImportProcess();

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    AdTableService tableService;

    @Autowired
    AdColumnService columnService;

    @Autowired
    AdReferenceService referenceService;

    @Autowired
    AdRefTableService refTableService;

    /**
     * Read external file and insert row into table
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void doImport(String idProcessExec) {
        AdProcessParam tableParam = getParam("idTable");
        if (tableParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idTable"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdProcessParam columnParam = getParam("idColumn");
        if (columnParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idColumn"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdProcessParam clientParam = getParam("idClient");
        if (clientParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idClient"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdProcessParam fileParam = getParam("file");
        if (fileParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "file"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        AdTable table = tableService.findById(tableParam.getString());
        if (table == null) {
            error(idProcessExec, getMessage("AD_ProcessTableErrId", tableParam.getId()));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdColumn column = columnService.findById(columnParam.getString());
        if (column == null) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadColumn", tableParam.getId()));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        if (table.getColumn(column.getName()) == null) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadColumnKey", table.getName(), column.getName()));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        String ext = fileParam.getFile().getOriginalFilename().toLowerCase().endsWith(".xls") ? ".xls" : ".xlsx";
        String contentType = "application/octet-stream;" + (fileParam.getFile().getOriginalFilename().toLowerCase().endsWith(".xls") ? "application/vnd.ms-excel" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        File uploadFile = uploadFile(idProcessExec, fileParam.getFile(), contentType, ext, 1000L, System.getProperty("java.io.tmpdir"));
        if (uploadFile != null) {
            ExcelUtil excel = new ExcelUtil();
            try {
                excel.loadFile(uploadFile);
            } catch (Exception e) {
                error(idProcessExec, getMessage("AD_ErrValidationUploadFile", tableParam.getId()));
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                return;
            }
            RowError rowError = new RowError();
            try {
                importExcel(idProcessExec, clientParam.getString(), table, column, excel, rowError);
            } catch (Exception e) {
                error(idProcessExec, getMessage("AD_ErrValidationUploadRowUpdate", rowError.getRow().getRowNum() + 1));
                error(idProcessExec, e.getMessage());
                if (e.getCause() != null) {
                    error(idProcessExec, e.getCause().getMessage());
                    if (e.getCause().getCause() != null) {
                        error(idProcessExec, e.getCause().getCause().getMessage());
                    }
                }
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            }
        }
    }

    /**
     * Import external Excel file
     *
     * @param idProcessExec Process Execution Identifier
     * @param impIdClient Client identifier to imported data
     * @param table Table information
     * @param column Column search key
     * @param excel Excel file
     * @param rowError Last processed row
     */
    private void importExcel(String idProcessExec, String impIdClient, AdTable table, AdColumn column, ExcelUtil excel, RowError rowError) {
        // Verify table PK
        List<AdColumn> keyColumns = table.getKeyFields();
        if (keyColumns.size() != 1) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadTableKey", column.getName()));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdColumn keyColumn = keyColumns.get(0);
        // Header/Column validation
        int keyColumnIndex = -1;
        boolean hasIdClient = false, hasCreated = false, hasUpdated = false, hasCreatedBy = false, hasUpdatedBy = false, hasActive = false, hasPk = false;
        Row row = excel.getSheet().getRow(0);
        rowError.setRow(row);
        List<AdColumn> columns = new ArrayList<>();
        for (int i = 0; i < row.getLastCellNum(); i++) {
            Cell cell = row.getCell(i);
            String refColumn = null;
            String colName = cell.getStringCellValue().trim();
            Pattern pattern = Pattern.compile("\\[(.*?)\\]");
            Matcher matcher = pattern.matcher(colName);
            if (matcher.find()) {
                refColumn = colName.substring(matcher.start() + 1, matcher.end() - 1);
                colName = colName.substring(0, matcher.start());
            }
            AdColumn headerColumn = table.getColumn(colName);
            if (headerColumn == null) {
                error(idProcessExec, getMessage("AD_ErrValidationUploadNotFoundColumn", getStringCellValue(cell), ExcelUtil.columns[cell.getColumnIndex()]));
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                return;
            }
            columns.add(headerColumn);
            if (refColumn != null) {
                if (headerColumn.getIdReferenceValue() == null) {
                    error(idProcessExec, getMessage("AD_ErrValidationUploadInvalidRefColumn", getStringCellValue(cell)));
                    finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                    return;
                }
                AdReference ref = referenceService.findById(headerColumn.getIdReferenceValue());
                if (!(AdReferenceService.RTYPE_TABLE.equals(ref.getRtype()) || AdReferenceService.RTYPE_TABLEDIR.equals(ref.getRtype()) || AdReferenceService.RTYPE_SEARCH.equals(ref.getRtype()))) {
                    error(idProcessExec, getMessage("AD_ErrValidationUploadInvalidRefColumnType", getStringCellValue(cell)));
                    finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                    return;
                }
                AdRefTable refTable = refTableService.getRefTableByReference(ref.getIdReference());
                if (refTable == null) {
                    error(idProcessExec, getMessage("AD_ErrValidationUploadNotFoundRefColumn", getStringCellValue(cell)));
                    finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                    return;
                }
                AdColumn columnRef = refTable.getTable().getColumn(refColumn);
                if (columnRef == null) {
                    error(idProcessExec, getMessage("AD_ErrValidationUploadNotFoundRefColumn", getStringCellValue(cell)));
                    finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                    return;
                }

                headerColumn.setImportRefColumn(columnRef);
                headerColumn.setImportRefQuery(entityManager.createNativeQuery(
                        "SELECT " + refTable.getKey().getName() + " FROM " + refTable.getTable().getName() +
                        " WHERE " + columnRef.getName() + " = :" + columnRef.getStandardName()
                ));
            }
            if (headerColumn.getName().equals(column.getName())) {
                keyColumnIndex = i;
            }
            if ("id_client".equals(headerColumn.getName())) {
                hasIdClient = true;
            }
            if ("created".equals(headerColumn.getName())) {
                hasCreated = true;
            }
            if ("updated".equals(headerColumn.getName())) {
                hasUpdated = true;
            }
            if ("created_by".equals(headerColumn.getName())) {
                hasCreatedBy = true;
            }
            if ("updated_by".equals(headerColumn.getName())) {
                hasUpdatedBy = true;
            }
            if ("active".equals(headerColumn.getName())) {
                hasActive = true;
            }
            if (keyColumn.getName().equals(headerColumn.getName())) {
                hasPk = true;
            }
        }
        if (keyColumnIndex == -1) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadNotFoundKeyColumn", column.getName()));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        // Import data
        String insert = "INSERT INTO " + table.getName() + " (id_client, created, updated, created_by, updated_by, active " + (!hasPk ? ", " + keyColumn.getName() : "");
        String insertParams = "";
        String updateFields = (!hasUpdated ? ", updated = :updated" : "") + (!hasUpdatedBy ? ", updated_by = :updatedBy" : "");
        if (!hasIdClient) {
            insertParams += ", :idClient";
        }
        if (!hasCreated) {
            insertParams += ", :created";
        }
        if (!hasUpdated) {
            insertParams += ", :updated";
        }
        if (!hasCreatedBy) {
            insertParams += ", :createdBy";
        }
        if (!hasUpdatedBy) {
            insertParams += ", :updatedBy";
        }
        if (!hasActive) {
            insertParams += ", :active";
        }
        if (!hasPk) {
            insertParams += ", :" + keyColumn.getStandardName();
        }
        for (AdColumn col : columns) {
            insertParams += ", :" + col.getStandardName();
            if ("created".equals(col.getName()) || "updated".equals(col.getName()) || "active".equals(col.getName())) {
                if ("active".equals(col.getName())) {
                    updateFields += ", " + col.getName() + " = :" + col.getStandardName();
                }
            } else {
                insert += ", " + col.getName();
                updateFields += ", " + col.getName() + " = :" + col.getStandardName();
            }
        }
        insert = insert + ") VALUES (" + insertParams.substring(2) + ")";
        String update = "UPDATE " + table.getName() + " SET " + updateFields.substring(2) + " WHERE " + keyColumn.getName() + " = :" + keyColumn.getStandardName();
        Query queryInsert = entityManager.createNativeQuery(insert);
        Query queryUpdate = entityManager.createNativeQuery(update);
        if (!hasIdClient) {
            queryInsert.setParameter("idClient", impIdClient);
        }
        if (!hasCreated) {
            queryInsert.setParameter("created", new Date());
        }
        if (!hasCreatedBy) {
            queryInsert.setParameter("createdBy", user.getIdUser());
        }
        if (!hasUpdated) {
            queryInsert.setParameter("updated", new Date());
            queryUpdate.setParameter("updated", new Date());
        }
        if (!hasUpdatedBy) {
            queryInsert.setParameter("updatedBy", user.getIdUser());
            queryUpdate.setParameter("updatedBy", user.getIdUser());
        }
        if (!hasActive) {
            queryInsert.setParameter("active", true);
        }

        info(idProcessExec, getMessage("AD_ImportExcelRows", excel.getSheet().getLastRowNum()));
        for (int i = 1; i < excel.getSheet().getLastRowNum(); i++) {
            row = excel.getSheet().getRow(i);
            rowError.setRow(row);
            String rowId = existValue(idProcessExec, table, keyColumn, column, row.getCell(keyColumnIndex));
            boolean isNew = rowId == null;
            for (int c = 0; c < row.getLastCellNum(); c++) {
                AdColumn col = columns.get(c);
                Cell cell = row.getCell(c);
                setParameter(idProcessExec, isNew ? queryInsert : queryUpdate, col, cell);
            }
            if (isNew) {
                if (!hasPk) {
                    rowId = UUID.randomUUID().toString().replaceAll("-", "");
                    queryInsert.setParameter(keyColumn.getStandardName(), rowId);
                }
                queryInsert.executeUpdate();
            } else {
                queryUpdate.setParameter(keyColumn.getStandardName(), rowId);
                queryUpdate.executeUpdate();
            }
        }
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Verify if a key value exist in table
     *
     * @param idProcessExec Process Execution Identifier
     * @param table Table information
     * @param keyColumn Table key column
     * @param column Column key data
     * @param cell Cell key
     * @return Row identifier or null
     */
    private String existValue(String idProcessExec, AdTable table, AdColumn keyColumn, AdColumn column, Cell cell) {
        Query query = entityManager.createQuery(
                "SELECT " + keyColumn.getStandardName() + " FROM " + table.getCapitalizeStandardName() +
                " WHERE " + column.getStandardName() + " = :" + column.getStandardName()
        );
        setParameter(idProcessExec, query, column, cell);
        List result = query.getResultList();
        return result.size() == 0 ? null : result.get(0).toString();
    }

    /**
     * Set parameter to query
     *
     * @param idProcessExec Process Execution Identifier
     * @param query Query
     * @param column Column parameter
     * @param cell Cell value
     */
    private void setParameter(String idProcessExec, Query query, AdColumn column, Cell cell) {
        CellType cellType = cell.getCellTypeEnum();
        if (cell == null || cellType == CellType.BLANK) {
            query.setParameter(column.getStandardName(), null);
            return;
        }
        if (column.getImportRefColumn() != null) {
            Query queryValue = column.getImportRefQuery();
            setParameter(idProcessExec, queryValue, column.getImportRefColumn(), cell);
            List result = queryValue.getResultList();
            if (result.size() > 0) {
                query.setParameter(column.getStandardName(), result.get(0));
            } else {
                query.setParameter(column.getStandardName(), null);
                warn(idProcessExec, getMessage("AD_ImportExcelRowNotFoundRefValue", cell.getRow().getRowNum() + 1, ExcelUtil.getColumnName(cell.getColumnIndex()), getStringCellValue(cell)));
            }
            return;
        }
        if (AdColumnService.TYPE_STRING.equals(column.getCtype()) || AdColumnService.TYPE_TEXT.equals(column.getCtype())) {
            query.setParameter(column.getStandardName(), getStringCellValue(cell));
        } else if (AdColumnService.TYPE_DATE.equals(column.getCtype()) || AdColumnService.TYPE_DATETIME.equals(column.getCtype())) {
            query.setParameter(column.getStandardName(), cell.getDateCellValue());
        } else if (AdColumnService.TYPE_BOOLEAN.equals(column.getCtype())) {
            query.setParameter(column.getStandardName(), Converter.getBoolean(getStringCellValue(cell)));
        } else {
            query.setParameter(column.getStandardName(), cell.getNumericCellValue());
        }
    }

    /**
     * Get string value from String cell or Numeric cell
     *
     * @param cell Cell
     * @return String value
     */
    private String getStringCellValue(Cell cell) {
        if (cell.getCellTypeEnum() == CellType.NUMERIC) {
            Double value = cell.getNumericCellValue();
            if (value > value.intValue())
                return "" + value;
            else
                return "" + value.intValue();
        } else {
            return cell.getStringCellValue();
        }
    }
}
