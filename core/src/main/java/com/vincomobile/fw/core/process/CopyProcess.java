package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.AdMenu;
import com.vincomobile.fw.core.persistence.model.AdMenuRoles;
import com.vincomobile.fw.core.persistence.model.AdProcessParam;
import com.vincomobile.fw.core.persistence.services.AdMenuRolesService;
import com.vincomobile.fw.core.persistence.services.AdMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CopyProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static CopyProcess service = new CopyProcess();

    @Autowired
    AdMenuRolesService menuRolesService;

    @Autowired
    AdMenuService menuService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Export data to json files
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void copyMenuRole(String idProcessExec) {
        AdProcessParam idModuleFromParam = getParam("idModuleFrom");
        if (idModuleFromParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idModuleFrom"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdProcessParam idModuleToParam = getParam("idModuleTo");
        if (idModuleToParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idModuleTo"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        AdProcessParam idRoleParam = getParam("idRole");
        if (idRoleParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idRole"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        if (idModuleFromParam.getValue().equals(idModuleToParam.getValue())) {
            error(idProcessExec, getMessage("AD_GlobalErrModuleEquals"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        List<AdMenuRoles> menuRoles = menuRolesService.listByModule((String) idModuleFromParam.getValue());
        String idModule = (String) idModuleToParam.getValue();
        String idRole = (String) idRoleParam.getValue();
        for (AdMenuRoles mr : menuRoles) {
            AdMenuRoles item = menuRolesService.getItem("0", mr.getIdMenu(), idRole);
            AdMenu menu = menuService.findById(mr.getIdMenu());
            if (item == null) {
                info(idProcessExec, getMessage("AD_ProcessCopyMenuAdd", menu.getName()));
                item = new AdMenuRoles();
                item.setIdClient("0");
                item.setIdModule(idModule);
                item.setIdMenu(mr.getIdMenu());
                item.setIdRole(idRole);
                item.setActive(true);
                menuRolesService.save(item);
            } else {
                warn(idProcessExec, getMessage("AD_ProcessCopyMenuExist", menu.getName()));
            }
        }

        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

}