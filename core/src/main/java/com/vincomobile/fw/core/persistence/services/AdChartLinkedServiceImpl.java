package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdChartLinked;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 04/10/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_chart_linked
 */
@Repository
@Transactional(readOnly = true)
public class AdChartLinkedServiceImpl extends BaseServiceImpl<AdChartLinked, String> implements AdChartLinkedService {

    /**
     * Constructor.
     */
    public AdChartLinkedServiceImpl() {
        super(AdChartLinked.class);
    }

    /**
     * Get linked chart
     *
     * @param idClient Client identifier
     * @param idChart Chart identifier
     * @return List of linked charts
     */
    @Override
    public List<AdChartLinked> getChartLinkeds(String idClient, String idChart) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idChart", idChart);
        return findAll(filter);
    }

}
