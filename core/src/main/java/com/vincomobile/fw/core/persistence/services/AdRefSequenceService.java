package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRefSequence;

/**
 * Created by Devtools.
 * Service layer interface for ad_sequence
 *
 * Date: 17/12/2015
 */
public interface AdRefSequenceService extends BaseService<AdRefSequence, String> {

    String TYPE_STANDARD                    = "STANDARD";
    String TYPE_YEAR                        = "YEAR";
    String TYPE_YEAR_AUTO                   = "YEAR_AUTO";
    String TYPE_YEAR_MONTH                  = "YEAR_MONTH";
    String TYPE_YEAR_MONTH_DAY              = "YEAR_MONTH_DAY";

    String ORDER_PREFIX_YEAR_SEQ_SUFIX      = "PREFIX_YEAR_SEQ_SUFIX";
    String ORDER_YEAR_PREFIX_SEQ_SUFIX      = "YEAR_PREFIX_SEQ_SUFIX";

    String YEAR_ORDER_YEAR_MONTH_DAY        = "YEAR_MONTH_DAY";
    String YEAR_ORDER_YEAR_DAY_MONTH        = "YEAR_DAY_MONTH";
    String YEAR_ORDER_MONTH_DAY_YEAR        = "MONTH_DAY_YEAR";
    String YEAR_ORDER_DAY_MONTH_YEAR        = "DAY_MONTH_YEAR";

    /**
     * Get next sequence value and increment current
     *
     * @param idRefSequence Sequence identifier
     * @return Next value
     */
    String getNextValue(String idRefSequence);
}


