package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.model.AdProcessExec;
import com.vincomobile.fw.core.persistence.model.AdProcessLog;
import com.vincomobile.fw.core.persistence.services.AdProcessExecService;
import com.vincomobile.fw.core.persistence.services.AdProcessLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class ProcessLoggerManager {

    private Logger logger = LoggerFactory.getLogger(ProcessLoggerManager.class);

    @Autowired
    AdProcessExecService processExecService;

    @Autowired
    AdProcessLogService processLogService;

    @Transactional(propagation = Propagation.REQUIRED)
    public void doSave() {
        long count = FWConfig.proccessLogger.count();
        logger.info("Save process log: " + count);
        if (count > 0) {
            List<AdProcessExec> logItems = FWConfig.proccessLogger.getItems();
            for (AdProcessExec processExec : logItems) {
                if (!processExec.getSaved()) {
                    processExecService.save(processExec);
                } else {
                    AdProcessExec dbExec = processExecService.findById(processExec.getIdProcessExec());
                    if (dbExec != null) {
                        dbExec.setFinished(processExec.getFinished());
                        dbExec.setStatus(processExec.getStatus());
                        processExecService.update(dbExec);
                    } else {
                        processExec.getLogs().clear();
                    }
                }
                for (AdProcessLog processLog : processExec.getLogs()) {
                    processLogService.save(processLog);
                }
            }
            FWConfig.proccessLogger.clearProcessExec();
        }
    }

}
