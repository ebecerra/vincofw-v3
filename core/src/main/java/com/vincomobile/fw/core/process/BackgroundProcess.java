package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdProcess;
import com.vincomobile.fw.core.persistence.model.AdProcessParam;
import com.vincomobile.fw.core.persistence.model.AdUser;
import com.vincomobile.fw.core.persistence.services.AdPreferenceService;
import com.vincomobile.fw.core.persistence.services.AdProcessParamService;
import com.vincomobile.fw.core.persistence.services.AdProcessService;
import com.vincomobile.fw.core.persistence.services.AdUserService;
import com.vincomobile.fw.core.tools.Converter;
import com.vincomobile.fw.core.tools.Datetool;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@DisallowConcurrentExecution
public class BackgroundProcess implements Job {

    private Logger logger = LoggerFactory.getLogger(BackgroundProcess.class);

    @Autowired
    AdProcessService processService;

    @Autowired
    AdProcessParamService processParamService;

    @Autowired
    AdUserService userService;

    @Autowired
    ApplicationContext context;

    @Autowired
    QuartzSchedulerWrapper scheduler;

    private String idClient;
    private String idLanguage;
    private String idUser;
    private String paramList;
    private Boolean manual;
    private JobDetail jobDetail;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        String idProcess = jobExecutionContext.getTrigger().getKey().getName();

        List<AdProcessParam> params = new ArrayList<>();
        AdProcess process = processService.findById(idProcess);
        if (process == null) {
            logger.error("No process found for background task: " + idProcess);
            return;
        }
        if (!process.getActive()) {
            logger.warn("No execute the process: " + process.getName() + " is inactive.");
            return;
        }

        AdUser user = userService.findById(idUser);
        if (user == null) {
            logger.error("No user assigned for background task: " + idProcess);
            return;
        }
        try {
            TriggerKey triggerKey = new TriggerKey(process.getId(), process.getIdModule());
            Trigger trigger = scheduler.getScheduler().getTrigger(triggerKey);
            if (trigger != null) {
                jobDetail = scheduler.getScheduler().getJobDetail(new JobKey(process.getId(), process.getIdModule()));
                boolean manual = jobDetail.getJobDataMap().getBoolean("manual");
                if (!manual) {
                    boolean execute = CacheManager.getPreferenceBoolean(idClient, AdPreferenceService.GLOBAL_QUARTZ_SCHEDULER_EXECUTE_PROCESS, true);
                    if (!execute) {
                        logger.warn("Background task execution is disabled. Skipping the process: " + process.getName());
                        return;
                    }
                }

                if (jobDetail.getJobDataMap().containsKey("paramList")) {
                    String paramList = jobDetail.getJobDataMap().getString("paramList");
                    List<AdProcessParam> parameters = processService.buildParamsList(process, paramList);
                    for (AdProcessParam current : parameters) {
                        params.add(current);
                    }
                }
            }
        } catch (SchedulerException e) {
            logger.error("Scheduler exception: " + e.getMessage());
            return;
        }
        List<AdProcessParam> inputParams = processParamService.getInputParams(idProcess);
        for (AdProcessParam param : inputParams) {
            if (!Converter.isEmpty(param.getValuedefault())) {
                String defaultValue = param.getValuedefault();
                Pattern pattern = Pattern.compile("\\@(.*?)\\@");
                Matcher m = pattern.matcher(defaultValue);
                if (m.matches()) {
                    m.find();
                    defaultValue = defaultValue.substring(m.start(), m.end());
                    if ("idClient".equals(defaultValue)) {
                        defaultValue = idClient;
                    } else if ("today".equals(defaultValue)) {
                        defaultValue = Converter.formatDate(new Date());
                    } else if ("yesterday".equals(defaultValue)) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(Datetool.getToday());
                        cal.add(Calendar.DAY_OF_YEAR, -1);
                        defaultValue = Converter.formatDate(cal.getTime());
                    } else if ("tomorrow".equals(defaultValue)) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(Datetool.getToday());
                        cal.add(Calendar.DAY_OF_YEAR, 1);
                        defaultValue = Converter.formatDate(cal.getTime());
                    } else if ("year".equals(defaultValue)) {
                        defaultValue = "" + Datetool.getCurrentYear();
                    } else if ("month".equals(defaultValue)) {
                        defaultValue = "" + Datetool.getCurrentMonth();
                    } else if ("week".equals(defaultValue)) {
                        defaultValue = "" + Datetool.getCurrentWeek();
                    } else if ("firstMonthDay".equals(defaultValue)) {
                        defaultValue = Converter.formatDate(Datetool.getDate(1, Datetool.getCurrentMonth(), Datetool.getCurrentYear()));
                    } else if ("lastMonthDay".equals(defaultValue)) {
                        defaultValue = Converter.formatDate(Datetool.getDate(Datetool.getLastDayOfMonth(Datetool.getCurrentYear(), Datetool.getCurrentMonth()), Datetool.getCurrentMonth(), Datetool.getCurrentYear()));
                    } else if ("firstYearDay".equals(defaultValue)) {
                        defaultValue = Converter.formatDate(Datetool.getFirstDayOfYear(Datetool.getCurrentYear()));
                    } else if ("lastYearDay".equals(defaultValue)) {
                        defaultValue = Converter.formatDate(Datetool.getLastDayOfYear(Datetool.getCurrentYear()));
                    }
                }
                param.setValue(defaultValue);
                boolean found = false;
                for (AdProcessParam param1 : params) {
                    if (param1.getIdProcessParam().equals(param.getIdProcessParam())) {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    params.add(param);
            }
        }
        String instanceId = "<invalid>";
        try {
            instanceId = jobExecutionContext.getScheduler().getMetaData().getSchedulerInstanceId();
        } catch (SchedulerException e) {
            logger.error("Can not get Scheduler instances Id");
        }
        logger.info(instanceId + ": Execute background process: " + process.getName() + ", User: " + user.getName() + ", idClient: " + idClient);
        processService.exec(process, user, idClient, idLanguage, params, false, null, null);
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getParamList() {
        return paramList;
    }

    public void setParamList(String paramList) {
        this.paramList = paramList;
    }

    public JobDetail getJobDetail() {
        return jobDetail;
    }

    public void setJobDetail(JobDetail jobDetail) {
        this.jobDetail = jobDetail;
    }

    public Boolean getManual() {
        return manual;
    }

    public void setManual(Boolean manual) {
        this.manual = manual;
    }
}
