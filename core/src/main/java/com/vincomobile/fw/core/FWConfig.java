package com.vincomobile.fw.core;

import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.process.ProcessLogger;
import com.vincomobile.fw.core.process.QuartzSchedulerWrapper;
import com.vincomobile.fw.core.process.ReportUtils;
import com.vincomobile.fw.core.process.SessionLog;
import com.vincomobile.fw.core.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Component
public class FWConfig implements IConfig {

    private Logger logger = LoggerFactory.getLogger(FWConfig.class);

    public static final String ALL_CLIENT_ID                        = "0";
    public static final String ID_USER_SYSTEM                       = "100";

    public static final String APPROVAL_NEED                        = "APPROVAL_NEED";

    public static final String MODULE_ID                            = "D351EB083EC7E8C4891CD7B25B1E274E";

    public static final String TABLE_ID_REF_LIST                    = "ff80808151382f2a0151386023350055";
    public static final String TABLE_ID_CHARACTERISTIC              = "ff8080815267b11b015267b2c0d00000";
    public static final String TABLE_ID_CHARACTERISTIC_VALUE        = "ff808081526d9f5c01526dcb1d27003e";
    public static final String TABLE_ID_CHART                       = "ff808181576c6b2801576c7033e20000";
    public static final String TABLE_ID_CHART_FILTER                = "ff808181576f64f501576f6821b70000";
    public static final String TABLE_ID_CHART_COLUMN                = "ff8081815847fce1015847fe5e070000";
    public static final String TABLE_ID_CHART_LINKED                = "ff8081815790216f015790336a1f0003";
    public static final String TABLE_ID_COLUMN                      = "ff80808150dba1800150dba69dee0018";
    public static final String TABLE_ID_FIELD                       = "ff80808151382f2a0151385c9296000d";
    public static final String TABLE_ID_FILE                        = "ff808081526d9f5c01526dce31a2007e";
    public static final String TABLE_ID_MESSAGE                     = "ff80808151382f2a0151385e92b60037";
    public static final String TABLE_ID_PROCESS                     = "ff80808150dba1800150dbbb9631002f";
    public static final String TABLE_ID_PROCESS_PARAM               = "ff80808150dba1800150dbbff03d0045";
    public static final String TABLE_ID_TABLE                       = "ff80808150dba1800150dba621c8000a";
    public static final String TABLE_ID_TABLE_ACTION                = "ff8081815a45a438015a45e35d1f00b8";
    public static final String TABLE_ID_WIKI_ENTRY                  = "ff8081816007aa5201600891138d001a";
    public static final String TABLE_ID_WIKI_GUIDE                  = "ff8081816007aa520160088a4c950000";
    public static final String TABLE_ID_WIKI_TOPIC                  = "ff8081816007aa520160088c61d6000b";

    public static final String COLUMN_ID_REF_LIST_NAME              = "ff80808151382f2a015138602767005e";
    public static final String COLUMN_ID_CHARACTERISTIC_NAME        = "ff808081526d55b301526d81966a0401";
    public static final String COLUMN_ID_CHART_FILTER_CAPTION       = "ff808181576f64f501576f6838c90029";
    public static final String COLUMN_ID_CHART_COLUMN_CAPTION       = "ff8081815848082401584825063e0024";
    public static final String COLUMN_ID_CHARACTERISTIC_VALUE       = "ff808081526d9f5c01526dcb35ed004f";
    public static final String COLUMN_ID_COLUMN_DESCRIPTION         = "ff8081815fbb72e2015fbb740e9c0000";
    public static final String COLUMN_ID_FILE_CAPTION               = "ff8081815b51f980015b51f9f6950006";
    public static final String COLUMN_ID_FILE_DESCRIPTION           = "ff808081526d9f5c01526dce4e1f008f";
    public static final String COLUMN_ID_FIELD_CAPTION              = "ff80808151387fb201513881d0ca0003";
    public static final String COLUMN_ID_MESSAGE_MSGTEXT            = "ff80808151382f2a0151385e97b7003f";
    public static final String COLUMN_ID_PROCESS_NAME               = "ff80808150dba1800150dbbba4190036";
    public static final String COLUMN_ID_PROCESS_PARAM_CAPTION      = "ff808081518a697801518a8165a50045";
    public static final String COLUMN_ID_PROCESS_PRIVILEGE_DESC     = "ff80808150dba1800150dbbba4190036";
    public static final String COLUMN_ID_TABLE_PRIVILEGE_DESC       = "ff80818160068c3f01600691363f0003";
    public static final String COLUMN_ID_TABLE_ACTION_PRIVILEGE_DESC= "ff80818160068c3f01600691d3680004";
    public static final String COLUMN_ID_WIKI_ENTRY_CONTENT         = "ff8081816007aa5201600891457b0024";
    public static final String COLUMN_ID_WIKI_ENTRY_TITLE           = "ff8081816007aa520160089145750023";
    public static final String COLUMN_ID_WIKI_GUIDE_CONTENT         = "ff8081816007aa520160088a8b680009";
    public static final String COLUMN_ID_WIKI_GUIDE_TITLE           = "ff80818160068c3f01600691d3680004";
    public static final String COLUMN_ID_WIKI_TOPIC_TITLE           = "ff8081816007aa520160088c85be0014";

    public static final String FWCORE_ID_MODULE                     = "D351EB083EC7E8C4891CD7B25B1E274E";
    public static final String FWCORE_ID_LANGUAGE_DEFAULT           = "D804EFC2B38FEEA39FA751540709D0BA";
    public static final String FWCORE_ID_PROCESS_APP_STARTED        = "D804EFC2B38FEEA39FA751540709D0BA";

    public static final String HOOK_SYNCHRO_SCHEMA                  = "AdSynchroSchema";
    public static final String HOOK_SYNCHRO_TABLE                   = "AdSynchroTable";
    public static final String HOOK_SYNCHRO_MAKE_UPDATE             = "AdSynchroMakeUpdate";
    public static final String HOOK_GET_EXTRA_PARAMS                = "AdGetExtraParams";
    public static final String HOOK_USER_GET_REAL_USER              = "AdUserGetRealUser";
    public static final String HOOK_USER_RECOVER_PASSWORD           = "AdUserRecoverPassword";
    public static final String HOOK_USER_CHANGE_PASSWORD            = "AdUserChangePassword";
    public static final String HOOK_USER_RESET_PASSWORD             = "AdUserResetPassword";
    public static final String HOOK_USER_VALIDATE                   = "AdUserValidate";
    public static final String HOOK_APP_STARTED                     = "AdAppStarted";
    public static final String HOOK_IMAGE_VALIDATE                  = "AdImageValidate";
    public static final String HOOK_IMAGE_CREATE_OR_UPDATE          = "AdImageCreateOrUpdate";
    public static final String HOOK_IMAGE_DELETE                    = "AdImageDelete";
    public static final String HOOK_PREFERENCE_CUSTOM_VALID         = "AdPreferenceCustomValid";
    public static final String HOOK_PREFERENCE_VALIDATE             = "AdPreferenceValidate";
    public static final String HOOK_REPORT_HEADER_PARAMS            = "AdReportHeaderParams";
    public static final String HOOK_TABLE_SORT                      = "AdTableSort";
    public static final String HOOK_TABLE_DELETE_BATCH              = "AdTableDeleteBatch";
    public static final String HOOK_PROCESS_FINISH                  = "AdProcessFinish";
    public static final String HOOK_CHART_EVALUATE                  = "AdChartEvaluate";

    public static final String HOOK_DB_TABLE_ROW_CREATE             = "AdDBTableRowCreate";
    public static final String HOOK_DB_TABLE_ROW_UPDATE             = "AdDBTableRowUpdate";
    public static final String HOOK_DB_TABLE_ROW_DELETE             = "AdDBTableRowDelete";

    public static final String HOOK_GET_EXTRA_PARAMS_KEY            = "extraParams";
    public static final String HOOK_GET_EXTRA_PARAMS_MODE_LOGGEDID  = "LoggedId";
    public static final String HOOK_GET_EXTRA_PARAMS_MODE_USRPWD    = "UserAndPassword";

    public static final String HOOK_USER_GET_REAL_USER_REAL_USER    = "realUser";
    public static final String HOOK_USER_CHANGE_PASSWORD_USER       = "user";
    public static final String HOOK_USER_CHANGE_PASSWORD_ERROR      = "error";
    public static final String HOOK_USER_RESET_PASSWORD_LOGIN       = "login";
    public static final String HOOK_USER_RESET_PASSWORD_ERROR       = "error";
    public static final String HOOK_USER_VALIDATE_USER              = "user";

    public static final String HOOK_USER_RECOVER_PWD_TEMPLATE       = "template";
    public static final String HOOK_USER_RECOVER_PWD_SUBJECT        = "subject";
    public static final String HOOK_USER_RECOVER_PWD_MAIL_SENDER    = "mailSender";

    public static final String HOOK_PREFERENCE_CUSTOM_VALID_VISIBLE = "Visible";

    public static final String DATABASE_TYPE_SQLSERVER              = "SQLSERVER";      // Microsoft SQL Server
    public static final String DATABASE_TYPE_MYSQL                  = "MYSQL";          // MySQL Server
    public static final String DATABASE_TYPE_POSTGRES               = "POSTGRES";       // Postgres Server
    public static final String DATABASE_TYPE_ORACLE                 = "ORACLE";         // Oracle Server

    public static final String DEFUALT_LANGUAGE_ID                  = "D804EFC2B38FEEA39FA751540709D0BA";
    public static final String DEFUALT_LANGUAGE_ISO2                = "es";

    public static int       SESSION_IDLE_TIME                       = 15;

    public static long      MOBILE_LOG_LEVEL                        = 4;

    public static String[]  MONTHS_NAMES                            = new String[12];
    public static String[]  WEEK_DAYS_NAMES                         = new String[7];

    public static String    DATABASE_TYPE                           = DATABASE_TYPE_MYSQL;    // DB type
    public static String    SQL_DATE_INPUT_FORMAT                   = "dd/MM/yyyy";
    public static String    SQL_DATETIME_INPUT_FORMAT               = "dd/MM/yyyy HH:mm:ss";
    public static String    SQL_DATE_MOBILE_FORMAT                  = "yyyy-MM-dd";

    public static String runtimeEnvironment = "UNDEFINED";
    public static String runtimeExcludeProcess = "";
    public static Boolean runtimeExcludeQuartz = false;

    public static final Session session = new Session();
    public static final ProcessLogger proccessLogger = new ProcessLogger();
    public static final SessionLog sessionLog = new SessionLog();
    public static Random random = new Random(new Date().getTime());

    public static Date appStartTime = new Date();

    @Autowired
    public AdPreferenceService preferenceService;

    @Autowired
    public AdRefListService refListService;

    @Autowired
    protected AdMessageService messageService;

    @Autowired
    protected AdPrivilegeService privilegeService;

    @Autowired
    AdProcessService processService;

    @Autowired
    protected WebApplicationContext applicationContext;

    @Autowired
    IRuntimeEnvironmentConfig runtimeEnvironmentConfig;

    @Autowired
    QuartzSchedulerWrapper scheduler;

    @PostConstruct
    public void init() {
        FWConfig.runtimeEnvironment = runtimeEnvironmentConfig.getEnvironment();
        logger.debug("Application loading. Environment: " + FWConfig.runtimeEnvironment);
        FWConfig.runtimeExcludeProcess = runtimeEnvironmentConfig.getExcludeProcess();
        FWConfig.runtimeExcludeQuartz = runtimeEnvironmentConfig.getExcludeQuartz();
        CacheManager.preferenceService = preferenceService;
        CacheManager.refListService = refListService;
        CacheManager.privilegeService = privilegeService;
        CacheManager.messageService = messageService;
        CacheManager.applicationContext = applicationContext;
        CacheManager.loadExtendedFilters();
        ReportUtils.messageService = messageService;
        // Month names
        MONTHS_NAMES[0] = messageService.getMessage("AD_MonthName_1");
        MONTHS_NAMES[1] = messageService.getMessage("AD_MonthName_2");
        MONTHS_NAMES[2] = messageService.getMessage("AD_MonthName_3");
        MONTHS_NAMES[3] = messageService.getMessage("AD_MonthName_4");
        MONTHS_NAMES[4] = messageService.getMessage("AD_MonthName_5");
        MONTHS_NAMES[5] = messageService.getMessage("AD_MonthName_6");
        MONTHS_NAMES[6] = messageService.getMessage("AD_MonthName_7");
        MONTHS_NAMES[7] = messageService.getMessage("AD_MonthName_8");
        MONTHS_NAMES[8] = messageService.getMessage("AD_MonthName_9");
        MONTHS_NAMES[9] = messageService.getMessage("AD_MonthName_10");
        MONTHS_NAMES[10] = messageService.getMessage("AD_MonthName_11");
        MONTHS_NAMES[11] = messageService.getMessage("AD_MonthName_12");
        // Week day names
        WEEK_DAYS_NAMES[0]                                      = messageService.getMessage("AD_WeekDaySunday");
        WEEK_DAYS_NAMES[Calendar.MONDAY - Calendar.SUNDAY]      = messageService.getMessage("AD_WeekDayMonday");
        WEEK_DAYS_NAMES[Calendar.TUESDAY - Calendar.SUNDAY]     = messageService.getMessage("AD_WeekDayTuesday");
        WEEK_DAYS_NAMES[Calendar.WEDNESDAY - Calendar.SUNDAY]   = messageService.getMessage("AD_WeekDayWednesday");
        WEEK_DAYS_NAMES[Calendar.THURSDAY - Calendar.SUNDAY]    = messageService.getMessage("AD_WeekDayThursday");
        WEEK_DAYS_NAMES[Calendar.FRIDAY - Calendar.SUNDAY]      = messageService.getMessage("AD_WeekDayFriday");
        WEEK_DAYS_NAMES[Calendar.SATURDAY - Calendar.SUNDAY]    = messageService.getMessage("AD_WeekDaySaturday");
    }

    @PreDestroy
    public void destroy() {
        CacheManager.destroy();
    }

}

