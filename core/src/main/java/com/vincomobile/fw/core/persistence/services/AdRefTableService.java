package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRefTable;

/**
 * Created by Devtools.
 * Interface of service of AD_REF_TABLE
 *
 * Date: 19/02/2015
 */
public interface AdRefTableService extends BaseService<AdRefTable, String> {

    /**
     * Get reference table by reference
     *
     * @param idReference Reference identifier
     * @return Reference Table
     */
    AdRefTable getRefTableByReference(String idReference);
}


