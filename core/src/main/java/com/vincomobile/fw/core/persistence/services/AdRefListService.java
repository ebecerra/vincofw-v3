package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.beans.AdRefListValue;
import com.vincomobile.fw.core.persistence.model.AdRefList;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_REF_LIST
 *
 * Date: 19/02/2015
 */
public interface AdRefListService extends BaseService<AdRefList, String> {

    /**
     * Get reference list for value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @return RefList
     */
    AdRefList getRefList(String idClient, String idReference, String value);

    /**
     * Get name for reference list value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @param idLanguage Language identifier
     * @return Name
     */
    String getName(String idClient, String idReference, String value, String idLanguage);
    String getName(String idClient, String idReference, String value);

    /**
     * Get identifier for reference list value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @return Identifier
     */
    String getIdRefList(String idClient, String idReference, String value);

    /**
     * Get list value of reference
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param idLanguage Language identifier
     * @return List values
     */
    List<AdRefListValue> listValues(String idClient, String idReference, String idLanguage);

    /**
     * Find reference by name
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param name Name
     * @return Reference value
     */
    AdRefList findByName(String idClient, String idReference, String name);

    /**
     * Get reference value by name
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param name Name
     * @return Reference value
     */
    String getRefValueByName(String idClient, String idReference, String name);

    /**
     * Get reference name by value
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param value Value
     * @return Reference value
     */
    String getRefNameByValue(String idClient, String idReference, String value);

    /**
     * Get reference value by ExtraInfo
     *
     * @param idClient Client identifier
     * @param idReference Reference identifier
     * @param extraInfo ExtraInfo
     * @return Reference value
     */
    String getRefValueByExtraInfo(String idClient, String idReference, String extraInfo);

    /**
     * Check if RefList is used
     *
     * @param refList RefList Value
     * @return Is used or not
     */
    boolean isUsed(AdRefList refList);
}


