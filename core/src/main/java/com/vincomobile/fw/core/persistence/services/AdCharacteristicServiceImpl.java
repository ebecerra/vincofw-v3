package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdCharacteristic;

import com.vincomobile.fw.core.tools.CharacteristicField;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Service layer implementation for ad_characteristic
 * <p/>
 * Date: 23/01/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdCharacteristicServiceImpl extends BaseServiceImpl<AdCharacteristic, String> implements AdCharacteristicService {


    /**
     * Constructor.
     */
    public AdCharacteristicServiceImpl() {
        super(AdCharacteristic.class);
    }

    /**
     * Returns the characteristic fields associated with a given table
     *
     * @param idClient Client identifier
     * @param dtype   Table type
     * @param idTable Table ID
     * @param mode Characteristic mode
     * @return List of fields
     */
    @Override
    public List<CharacteristicField> listFields(String idClient, String dtype, String idTable, String mode) {
        String queryStr = "SELECT c.idCharacteristic, c.name, c.ctype, c.idReference, c.idReferenceValue, c.idCharacteristic, \n" +
                "cd.mandatory, cd.defValue, cd.seqno, c.mode, c.idModule FROM AdCharacteristicDef cd, AdCharacteristic c \n" +
                "WHERE c.active = true and cd.active = true and c.mode = :mode and  cd.idCharacteristic = c.idCharacteristic \n " +
                "and c.idTable = :idTable and cd.idClient in :clients and cd.dtype = :dtype";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("idTable", idTable);
        query.setParameter("mode", mode);
        query.setParameter("clients", getClientFilter(idClient));
        query.setParameter("dtype", dtype);
        List<Object[]> results = query.getResultList();
        List<CharacteristicField> list = new ArrayList<>();
        for (Object[] current : results) {
            CharacteristicField field = new CharacteristicField();
            if (current[0] != null)
                field.setIdCharacteristic(current[0].toString());
            if (current[1] != null)
                field.setName(current[1].toString());
            if (current[2] != null)
                field.setCtype(current[2].toString());
            if (current[3] != null)
                field.setIdReference(current[3].toString());
            if (current[4] != null)
                field.setIdReferenceValue(current[4].toString());
            field.setCharacteristic(this.findById(current[5].toString()));
            if (current[6] != null && current[6].getClass().equals(Boolean.class))
                field.setMandatory((Boolean) current[6]);
            if (current[7] != null)
                field.setDefValue(current[7].toString());
            if (current[8] != null)
                field.setSeqno(Integer.parseInt(current[8].toString()));
            if (current[9] != null)
                field.setMode(current[9].toString());
            if (current[10] != null)
                field.setIdModule(current[10].toString());
            list.add(field);

        }
        return list;
    }
}
