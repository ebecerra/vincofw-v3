package com.vincomobile.fw.core.hooks;

import java.util.HashMap;
import java.util.Map;

public class HookResult {

    public Map<String, Object> values = new HashMap<>();
    public boolean success = true;
    public String error = "";

    public void put(String key, Object value) {
        this.values.put(key, value);
    }

    public Object get(String key) {
        return values.get(key);
    }

    public boolean hasKey(String key) {
        return values.containsKey(key);
    }

    public void setError(String error) {
        this.success = false;
        this.error = error;
    }
}
