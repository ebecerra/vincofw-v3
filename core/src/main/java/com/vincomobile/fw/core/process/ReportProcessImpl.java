package com.vincomobile.fw.core.process;

import com.vincomobile.fw.core.persistence.model.*;
import com.vincomobile.fw.core.persistence.services.*;
import com.vincomobile.fw.core.tools.Constants;
import com.vincomobile.fw.core.tools.Converter;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Transactional(readOnly = true)
public class ReportProcessImpl extends ProcessDefinitionImpl implements ReportProcess {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static ReportProcess service = new ReportProcessImpl();

    @Autowired
    ServletContext context;

    @Autowired
    ReportService reportService;

    @Autowired
    AdLanguageService languageService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Generates report files
     *
     * @param idProcessExec Process Execution Identifier
     */
    @Override
    public void executeReport(String idProcessExec) {
        info(idProcessExec, getMessage("AD_ProcessCheckOutputFormat"));
        String standardConstraints = "";
        AdLanguage language = languageService.findById(this.user.getDefaultIdLanguage());
        setServletContext(context);
        // Multiple process
        if (process.getMultiple() && !Converter.isEmpty(process.getEvalSql())) {
            logger.debug("Executing a multiple report");
            standardConstraints = "outputType=pdf";
            AdProcessParam outputParam = new AdProcessParam();
            outputParam.setName("outputType");
            outputParam.setValue("pdf");
            String sql = languageService.replaceParameters(
                    idClient, process.getEvalSql(), params.size() > 0 ? params.get(0).getValue().toString() : null,
                    params.size() > 1 ? params.get(1).getValue().toString() : null, params.size() > 2 ? params.get(2).getValue().toString() : null,
                    params.size() > 3 ? params.get(3).getValue().toString() : null, params.size() > 4 ? params.get(4).getValue().toString() : null);
            List evalValues = languageService.nativeQuery(sql);
            logger.debug("Report count: " + evalValues.size());
            String select = sql.toLowerCase().replaceAll("\\n", " ");
            select = select.replaceAll("\\((.*?)\\)", "subselect");
            Pattern pattern = Pattern.compile("select(.*?)from");
            Matcher matcher = pattern.matcher(select);
            if (matcher.find()) {
                select = select.substring(matcher.start() + 6, matcher.end() - 4);
                List<OutputParam> params = new ArrayList<>();
                String[] fields = select.split(",");
                for (int i = 0; i < fields.length; i++) {
                    fields[i] = Converter.removeUnderscores(fields[i].trim());
                }
                for (Object evalValue : evalValues) {
                    Object[] item = (Object[]) evalValue;
                    String extraConstraints = "";
                    for (int i = 0; i < fields.length; i++) {
                        String key = fields[i];
                        int indx = key.toLowerCase().indexOf("as ");
                        if (indx > 0) {
                            key = key.substring(indx + 3).trim();
                        }
                        indx = key.toLowerCase().indexOf(".");
                        if (indx > 0) {
                            key = key.substring(indx + 1).trim();
                        }
                        extraConstraints += ("," + key + "=" + item[i]);
                        addParam(params, key, (String) item[i]);
                    }
                    info(idProcessExec, "Report multiple selector constraints: " + extraConstraints);
                    File tmpFile = generateReport(idProcessExec, outputParam, standardConstraints + extraConstraints, language);
                    if (tmpFile != null) {
                        sendOutput(idProcessExec, tmpFile, params);
                    } else {
                        finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                    }
                }
            }
            finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
        } else {
            logger.debug("Executing a single report");
            AdProcessParam outputParam = null;
            for (AdProcessParam param : this.params) {
                if (Constants.OUTPUT_REPORT_TYPE.equals(param.getName()))
                    outputParam = param;
                else
                    standardConstraints += ("," + param.getName() + "=" + param.getValue());
            }
            if (outputParam == null || outputParam.getValue() == null) {
                outputParam = new AdProcessParam();
                outputParam.setName("outputType");
                outputParam.setValue("pdf");
            }
            if (ReportService.REPORT_FORMAT_HTML.equals(outputParam.getValue())) {
                info(idProcessExec, getMessage("AD_ProcessGeneratePreview"));
            } else {
                info(idProcessExec, getMessage("AD_ProcessGenerateReport", outputParam.getValue()));
            }
            if (outputs == null)
                outputs = new ArrayList<>();
            AdProcessOutput newOutput = new AdProcessOutput();
            newOutput.setFilePath("");
            newOutput.setOtype("HTTP");
            newOutput.setFileExt("");
            newOutput.setFileTimeFmt("yyyyMMdd_HHmm");
            newOutput.setFilePrefix("");
            outputs.add(newOutput);
            File tmpFile = generateReport(idProcessExec, outputParam, standardConstraints, language);
            if (tmpFile != null) {
                logger.debug("Generate report: " + tmpFile.getAbsolutePath());
            } else {
                logger.error("Report not generated");
            }
            finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS, tmpFile);
        }
    }

    /**
     * Add parameter to list
     *
     * @param params Parameter list
     * @param param Parameter name
     * @param value Value
     */
    private void addParam(List<OutputParam> params, String param, String value) {
        for (OutputParam outputParam : params) {
            if (outputParam.getParam().equals(param)) {
                outputParam.setValue(value);
                return;
            }
        }
        params.add(new OutputParam(param, value));
    }

    /**
     * Generate a report
     *
     * @param idProcessExec Process Execution Identifier
     * @param outputParam Output parameter
     * @param constraints Constraints
     * @param language Language identifier
     * @return Report file
     */
    private File generateReport(String idProcessExec, AdProcessParam outputParam, String constraints, AdLanguage language) {
        byte[] result = reportService.getReport(process, outputParam.getValue().toString(), idClient, idLanguage, constraints, language.getIso2(), this.user.getName());
        if (result.length == 0) {
            logger.error("Error creating report");
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return null;
        }
        File tmpFile;
        try {
            tmpFile = File.createTempFile(Converter.getCleanFileName(process.getName()), "." + outputParam.getValue());
            FileUtils.writeByteArrayToFile(tmpFile, result);
        } catch (IOException ex) {
            logger.error("Error creating temporary file", ex);
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return null;
        }
        // Changing output extension to the extension requested by the client
        for (AdProcessOutput output : outputs) {
            output.setFileExt("." + outputParam.getValue());
        }
        return tmpFile;
    }
}
