package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.hooks.HookManager;
import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdModule;
import com.vincomobile.fw.core.persistence.model.AdProcess;
import com.vincomobile.fw.core.tools.Converter;
import net.sf.jasperreports.crosstabs.JRCrosstab;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRElementsVisitor;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.*;
import org.apache.commons.beanutils.ConvertUtils;
import org.hibernate.engine.spi.SessionImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContext;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.util.*;

@Component
public class ReportServiceImpl implements ReportService {

    private Logger logger = LoggerFactory.getLogger(ReportServiceImpl.class);

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    ServletContext servletContext;

    @Autowired
    AdModuleService moduleService;

    /**
     * Returns a report
     *
     * @param report      Process with the report data
     * @param format      Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @return Report serialized to a byte array
     */
    @Override
    public byte[] getReport(AdProcess report, String format, String idClient, String idLanguage, String constraints, String lang, String username) {
        byte[] result;
        AdModule module = moduleService.findById(report.getIdModule());
        report.setModule(module);
        String fullPath = servletContext.getRealPath("WEB-INF/reports/" + module.getRestPath());
        fullPath = fullPath.replaceAll("\\\\","/");
        constraints += ",SUBREPORT_DIR=" + fullPath + "/";
        String jrxml = report.getJrxml();
        if (REPORT_FORMAT_EXCEL.equals(format.toLowerCase()) && !Converter.isEmpty(report.getJrxmlExcel()))
            jrxml = report.getJrxmlExcel();
        else if (REPORT_FORMAT_WORD.equals(format.toLowerCase()) && !Converter.isEmpty(report.getJrxmlWord()))
            jrxml = report.getJrxmlWord();
        String fileName = fullPath + "/" + jrxml + ".jrxml";
        Connection connection = entityManager.unwrap(SessionImplementor.class).connection();
        result = getExportReport(idClient, idLanguage, constraints, fileName, connection, format, lang, username, report.getName());
        return result;
    }

    /**
     * Returns a report
     *
     * @param reportName  Name of the report to generate
     * @param format      Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @param dataSource  Datasource
     * @param extraParams Extra parameters to pass to the report
     * @return Report serialized to a byte array
     */
    @Override
    public byte[] getReport(String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username, JRDataSource dataSource, HashMap extraParams) {
        byte[] result;
        String fullPath = servletContext.getRealPath("WEB-INF/reports");
        constraints += ",SUBREPORT_DIR=" + fullPath + "/";
        String fileName = reportName;
        result = getExportReport(idClient, idLanguage, constraints, fileName, format, lang, username, reportName, dataSource, extraParams);
        return result;
    }

    /**
     * Returns a reporte
     *
     * @param reportName  Name of the report to generate
     * @param format      Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @return Report serialized to a byte array
     */
    @Override
    public byte[] getReport(String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username) {
        byte[] result;
        String fullPath = servletContext.getRealPath("WEB-INF/reports");
        fullPath = fullPath.replaceAll("\\\\","/");
        constraints += ",SUBREPORT_DIR=" + fullPath + "/";
        String fileName = fullPath + "/" + reportName + ".jrxml";
        Connection connection = entityManager.unwrap(SessionImplementor.class).connection();
        result = getExportReport(idClient, idLanguage, constraints, fileName, connection, format, lang, username, reportName);
        return result;
    }

    /**
     * Generates an error report with information about the error
     *
     * @param format      Report format
     * @param error       Error message
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param lang        Report language
     * @param username    Username
     * @param report      Report
     * @param constraints Extra parameters to pass to the report
     * @return Report serialized to a byte array
     */
    @Override
    public byte[] getReportError(String format, String error, String idClient, String idLanguage, String lang, String username, String report, String constraints) {
        byte[] result;
        String fullPath = servletContext.getRealPath("WEB-INF/reports");
        String fileName = fullPath + "/error.jrxml";
        Connection connection = entityManager.unwrap(SessionImplementor.class).connection();
        result = getExportReport(idClient, idLanguage, "user=" + username + ",report=" + report + ",ERROR_MESSAGE=" + error + "," + constraints, fileName, connection, format, lang, username, "error");
        return result;
    }

    /**
     * Generates a report to a file
     *
     * @param fileName    File name to generate the report to
     * @param reportName  Name of the report to generate
     * @param format      Report format
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param lang        Report language
     * @param username    Username
     * @return Returns true if the report was generated OK. False otherwise
     */
    @Override
    public boolean getFileReport(String fileName, String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username) {
        return getFileReport(new File(fileName), reportName, format, idClient, idLanguage, constraints, lang, username);
    }

    @Override
    public boolean getFileReport(File file, String reportName, String format, String idClient, String idLanguage, String constraints, String lang, String username) {
        byte[] result = getReport(reportName, format, idClient, idLanguage, constraints, lang, username);
        try {
            FileOutputStream output = new FileOutputStream(file);
            output.write(result);
            output.flush();
            output.close();
            return true;
        } catch (Exception e) {
            logger.error("ERROR: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Generates a report in memory
     *
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param fileName    Name of the jrxml report file
     * @param connection  Database connection
     * @param format      Report format
     * @param lang        Report language
     * @param username    Username
     * @param reportName  Name of the report
     * @return Report serialized to a byte array
     */
    private byte[] getExportReport(String idClient, String idLanguage, String constraints, String fileName, Connection connection, String format, String lang, String username, String reportName) {
        byte[] result;
        try {
            JasperReport report = compileReport(fileName);
            Locale locale = new Locale(lang);
            constraints = extendConstrains(idClient, constraints);
            PageSearch pageReq = new PageSearch();
            pageReq.parseConstraints(constraints, ReportServiceImpl.class);
            Map<String, Object> params = convertedParams(idClient, idLanguage, pageReq.getSearchs());
            HookManager.executeHook(FWConfig.HOOK_REPORT_HEADER_PARAMS, idClient, params);
            params.put(JRParameter.REPORT_LOCALE, locale);
            ResourceBundle rb = ResourceBundle.getBundle("locale.messages", locale);
            params.put(JRParameter.REPORT_RESOURCE_BUNDLE, rb);
            JasperPrint print = (connection != null) ? JasperFillManager.fillReport(report, params, connection) : JasperFillManager.fillReport(report, params);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            JRAbstractExporter exporter = getJRExporter(format, print, output);
            exporter.exportReport();
            output.toByteArray();
            output.close();
            result = output.toByteArray();
        } catch (Exception e) {
            logger.error("ERROR. Report: " + fileName + ", Format: " + format + ", Constraints: " + constraints);
            logger.error("Exception: " + e.getMessage());
            if (reportName.equals("error"))
                return new byte[0];
            return getReportError(format, e.getMessage(), idClient, idLanguage, lang, "'" + username + "'", reportName, constraints);
        }
        return result;
    }


    /**
     * Generate a report to memory
     *
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Extra parameters to pass to the report
     * @param fileName    Name of the jrxml report file
     * @param format      Report format
     * @param lang        Report language
     * @param username    Username
     * @param reportName  Name of the report
     * @param dataSource  Datasource
     * @param extraParams Extra parameters to pass to the report
     * @return In-memory generated report
     */
    private byte[] getExportReport(String idClient, String idLanguage, String constraints, String fileName, String format, String lang, String username, String reportName, JRDataSource dataSource, HashMap extraParams) {
        byte[] result;
        try {
            JasperReport report = compileReport(fileName);
            Locale locale = new Locale(lang);
            PageSearch pageReq = new PageSearch();
            pageReq.parseConstraints(constraints, ReportServiceImpl.class);
            Map<String, Object> params = convertedParams(idClient, idLanguage, pageReq.getSearchs());
            params.put(JRParameter.REPORT_LOCALE, locale);
            ResourceBundle rb = ResourceBundle.getBundle("locale.messages", locale);
            params.put(JRParameter.REPORT_RESOURCE_BUNDLE, rb);
            if (extraParams != null) {
                params.putAll(extraParams);
            }
            JasperPrint print = (dataSource != null) ? JasperFillManager.fillReport(report, params, dataSource) : JasperFillManager.fillReport(report, params);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            JRAbstractExporter exporter = getJRExporter(format, print, output);
            exporter.exportReport();
            output.toByteArray();
            output.close();
            result = output.toByteArray();
        } catch (Exception e) {
            logger.error("ERROR. Report: " + fileName + ", Format: " + format + ", Constraints: " + constraints);
            logger.error("Exception: " + e.getMessage());
            if (reportName.equals("error"))
                return new byte[0];
            return getReportError(format, e.getMessage(), idClient, idLanguage, lang, username, reportName, constraints);
        } catch (Throwable e) {
            logger.error("ERROR. Report: " + fileName + ", Format: " + format + ", Constraints: " + constraints);
            logger.error("Exception: " + e.getMessage());
            if (reportName.equals("error"))
                return new byte[0];
            return getReportError(format, e.getMessage(), idClient, idLanguage, lang, username, reportName, constraints);
        }
        return result;
    }

    /**
     * Gets the corresponding exporter
     *
     * @param format Output format
     * @param print JasperPrint param
     * @param output  OutputStream
     * @return The corresponding exporter for the given format
     */
    private JRAbstractExporter getJRExporter(String format, JasperPrint print, ByteArrayOutputStream output) {
        JRAbstractExporter exporter;
        if ("xls".equals(format))
            exporter = getXlsExporter(print, output);
        else if ("pdf".equals(format))
            exporter = getPdfExporter(print, output);
        else
            exporter = getHtmlExporter(print, output);
        return exporter;
    }

    /**
     * Returns the PDF exporter
     *
     * @param print  Report compiled in JasperPrint
     * @param output Stream to generate the report to
     * @return PDF exporter to generate the report
     */
    private JRAbstractExporter getPdfExporter(JasperPrint print, ByteArrayOutputStream output) {
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        exporter.setConfiguration(configuration);
        return exporter;
    }

    /**
     * Returns the XLS exporter
     *
     * @param print  Report compiled in JasperPrint
     * @param output Stream to generate the report to
     * @return XLS exporter to generate the report
     */
    private JRAbstractExporter getXlsExporter(JasperPrint print, ByteArrayOutputStream output) {
        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
        SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
        configuration.setDetectCellType(true);
        configuration.setWhitePageBackground(false);
        configuration.setRemoveEmptySpaceBetweenRows(true);
        configuration.setWrapText(false);
        String[] sheetNames = new String[1];
        sheetNames[0] = "1";
        configuration.setSheetNames(sheetNames);
        exporter.setConfiguration(configuration);
        return exporter;
    }


    /**
     * Returns the HTML exporter
     *
     * @param print  Report compiled in JasperPrint
     * @param output Stream to generate the report to
     * @return HTML exporter to generate the report
     */
    private JRAbstractExporter getHtmlExporter(JasperPrint print, ByteArrayOutputStream output) {
        HtmlExporter exporter = new HtmlExporter();
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setExporterOutput(new SimpleHtmlExporterOutput(output));
        SimpleHtmlExporterConfiguration configuration = new SimpleHtmlExporterConfiguration();
        configuration.setBetweenPagesHtml("");
        exporter.setConfiguration(configuration);
        return exporter;
    }

    /**
     * Get stardard parameters for all reports
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return Stardard parameters
     */
    protected Map<String, Object> getStandardParams(String idClient, String idLanguage) {
        Map<String, Object> result = new HashMap<>();
        result.put("idClient", idClient);
        result.put("idLanguage", idLanguage);
        result.put(PREF_HEADER_LOGO, CacheManager.getPreference(idClient, PREF_HEADER_LOGO).getString());
        result.put(PREF_HEADER_COMPANY, CacheManager.getPreference(idClient, PREF_HEADER_COMPANY).getString());
        result.put(PREF_HEADER_EMAIL, CacheManager.getPreference(idClient, PREF_HEADER_EMAIL).getString());
        result.put(PREF_HEADER_PHONE, CacheManager.getPreference(idClient, PREF_HEADER_PHONE).getString());
        result.put(PREF_HEADER_FAX, CacheManager.getPreference(idClient, PREF_HEADER_FAX).getString());

        return result;
    }

    /**
     * Convert parameters from "constraints" to report
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param hash Controller parameters
     * @return Report parameters
     */
    protected Map<String, Object> convertedParams(String idClient, String idLanguage, Map<String, Object> hash) {
        Map<String, Object> finalMap = getStandardParams(idClient, idLanguage);
        for (Object key : hash.keySet()) {
            Object value = hash.get(key);
            Object converted = convert(Date.class, value);

            if (value.toString().startsWith("!!")) {
                value = value.toString().replaceAll("!!", "");
                finalMap.put(key.toString(), value);
            } else {
                if (converted != null && converted.getClass().equals(Date.class))
                    finalMap.put(key.toString(), converted);
                else {
                    converted = convert(Long.class, value);
                    if (converted != null && converted.getClass().equals(Long.class))
                        finalMap.put(key.toString(), converted);
                    else {
                        converted = convert(Integer.class, value);
                        if (converted != null && converted.getClass().equals(Integer.class))
                            finalMap.put(key.toString(), converted);
                        else {
                            if (value.toString().startsWith("!!"))
                                value = value.toString().replaceAll("!!", "");
                            finalMap.put(key.toString(), value);
                        }
                    }
                }
            }
        }
        return finalMap;
    }

    /**
     * Extend contrainsts to add standard parameters
     *
     * @param idClient Client identifier
     * @param constraints Constraints
     * @return Extended contrainsts
     */
    protected String extendConstrains(String idClient, String constraints) {
        if (!constraints.contains("WEB_URL=")) {
            constraints += ",WEB_URL=" + CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_WEB_SERVER_URL);
        }
        if (!constraints.contains("BASE_URL=")) {
            constraints += ",BASE_URL=" + CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL) + (new Date().getTime()) + "/files";
        }
        return constraints;
    }

    /**
     * Converts the value of a property of the given class
     *
     * @param clazz Class
     * @param value Value
     * @return Converted value
     */
    protected Object convert(Class clazz, Object value) {
        if (value != null) {
            if (Collection.class.isAssignableFrom(value.getClass())) {
                return convertCollect(clazz, ((Collection) value));
            } else {
                Object obj = null;
                // Strings are formatted elsewhere
                if (value instanceof String && Date.class.isAssignableFrom(clazz)) {
                    obj = Converter.getDate((String) value);

                    if (obj != null) {
                        return obj;
                    }
                }
                if (value instanceof String && Long.class.isAssignableFrom(clazz)) {
                    try {
                        obj = Long.parseLong((String) value);

                    } catch (NumberFormatException ex) {
                        return null;

                    }
                    return ConvertUtils.convert(value, clazz);
                }
                if (value instanceof String && Integer.class.isAssignableFrom(clazz)) {
                    try {
                        obj = Integer.parseInt((String) value);

                    } catch (NumberFormatException ex) {
                        return null;

                    }
                    return ConvertUtils.convert(value, clazz);
                }

                obj = value;
                try {
                    obj = ConvertUtils.convert(value, clazz);
                } catch (Throwable e) {
                    // TODO
                }
                return obj;
            }
        } else {
            return null;
        }
    }

    protected List convertCollect(Class clazz, Collection items) {
        List result = new ArrayList();
        for (Object item : items) {
            result.add(convert(clazz, item));
        }

        return result;
    }

    /**
     * Recompile subreports recursively
     *
     * @param reportName Report name
     * @return Compiled report
     * @throws JRException  Jasper exception
     */
    public JasperReport compileReport(String reportName) throws JRException {
        String reportsPath = servletContext.getRealPath("WEB-INF/reports/").replaceAll("\\\\","/");
        reportName = reportName.replaceAll("\\\\","/");
        if (reportName.startsWith(reportsPath)) {
            reportsPath = "";
        }
        if (reportName.endsWith(".jrxml")) {
            reportName = reportName.substring(0, reportName.length() - 6);
        }
        JasperDesign jasperDesign = JRXmlLoader.load(reportsPath + reportName + ".jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JRSaver.saveObject(jasperReport, reportsPath + reportName + ".jasper");
        logger.info("Saving compiled report to: " + reportsPath + reportName + ".jasper");
        //Compile sub reports
        JRElementsVisitor.visitReport(jasperReport, new JRVisitor() {
            @Override
            public void visitBreak(JRBreak breakElement) {
            }

            @Override
            public void visitChart(JRChart chart) {
            }

            @Override
            public void visitCrosstab(JRCrosstab jrCrosstab) {

            }

            @Override
            public void visitElementGroup(JRElementGroup elementGroup) {
            }

            @Override
            public void visitEllipse(JREllipse ellipse) {
            }

            @Override
            public void visitFrame(JRFrame frame) {
            }

            @Override
            public void visitImage(JRImage image) {
            }

            @Override
            public void visitLine(JRLine line) {
            }

            @Override
            public void visitRectangle(JRRectangle rectangle) {
            }

            @Override
            public void visitStaticText(JRStaticText staticText) {
            }

            @Override
            public void visitSubreport(JRSubreport subreport) {
                try {
                    String expression = subreport.getExpression().getText().replace(".jasper", "");
                    StringTokenizer st = new StringTokenizer(expression, "+");
                    String subReportName = null;
                    while (st.hasMoreTokens())
                        subReportName = st.nextToken();
                    compileReport(subReportName.trim().replaceAll("\"", ""));
                } catch (Throwable e) {
                    logger.error("Error compiling report" + subreport.getExpression().getText(), e);
                }
            }

            @Override
            public void visitTextField(JRTextField textField) {
            }

            @Override
            public void visitComponentElement(JRComponentElement componentElement) {
            }

            @Override
            public void visitGenericElement(JRGenericElement element) {
            }
        });
        return jasperReport;
    }

}

