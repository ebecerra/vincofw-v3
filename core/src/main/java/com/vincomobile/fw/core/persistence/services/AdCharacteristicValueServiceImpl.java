package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.cache.CacheManager;
import com.vincomobile.fw.core.persistence.model.AdCharacteristicValue;

import com.vincomobile.fw.core.persistence.model.AdPreference;
import com.vincomobile.fw.core.persistence.model.AdTable;
import com.vincomobile.fw.core.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service layer implementation for ad_characteristic_value
 *
 * Date: 23/01/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdCharacteristicValueServiceImpl extends BaseServiceImpl<AdCharacteristicValue, String> implements AdCharacteristicValueService {

    @Autowired
    AdTableService tableService;

    /**
     * Constructor.
     */
    public AdCharacteristicValueServiceImpl() {
        super(AdCharacteristicValue.class);
    }

    /**
     * Load characteristics for a item
     *
     * @param idClient Client identifier
     * @param keyValue Key value for item
     * @param characteristicType Characteristic Type for item
     * @param active Active/Inactive rows
     * @return Characteristics
     */
    @Override
    public List<AdCharacteristicValue> loadCharacteristicValues(String idClient, String keyValue, String characteristicType, Boolean active) {
        String sql = "SELECT CV FROM AdCharacteristicValue CV, AdCharacteristicDef CD\n" +
                "WHERE CV.idCharacteristic = CD.idCharacteristic AND CV.keyValue = :keyValue \n" +
                "AND CD.dtype = :characteristicType ";
        if (active != null)
            sql += "AND CV.active = :active\n";
        sql += "ORDER BY CD.seqno, CV.seqno";

        Query query = entityManager.createQuery(sql, AdCharacteristicValue.class);
        query.setParameter("keyValue", keyValue);
        query.setParameter("characteristicType", characteristicType);
        if (active != null)
            query.setParameter("active", active);
        return query.getResultList();
    }

    /**
     * Load characteristics for a item
     *
     * @param idClient Client identifier
     * @param idCharacteristic Characteristic identifier
     * @return Characteristics
     */
    @Override
    public List<AdCharacteristicValue> loadCharacteristicAllValues(String idClient, String idCharacteristic) {
        Map filter = new HashMap();
        filter.put("idClient", idClient);
        filter.put("idCharacteristic", idCharacteristic);
        filter.put("active", true);
        return findAll(filter);
    }

    /**
     * Get characteristic value
     *
     * @param idClient Client identifier
     * @param idCharacteristic Characteristic identifier
     * @param keyValue Key value for item
     * @return Characteristic value
     */
    @Override
    public AdCharacteristicValue getValueFor(String idClient, String idCharacteristic, String keyValue) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idCharacteristic", idCharacteristic);
        filter.put("keyValue", keyValue);
        return findFirst(filter);
    }

    /**
     * Get characteristic value
     *
     * @param idClient Client identifier
     * @param idCharacteristic Characteristic identifier
     * @param keyValue Key value for item
     * @return Characteristic value
     */
    @Override
    public String getStringValueFor(String idClient, String idCharacteristic, String keyValue) {
        Query query = entityManager.createQuery(
                "SELECT value FROM AdCharacteristicValue " +
                "WHERE idClient IN :idClient AND idCharacteristic = :idCharacteristic AND keyValue = :keyValue"
        );
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idCharacteristic", idCharacteristic);
        query.setParameter("keyValue", keyValue);
        return (String) query.getSingleResult();
    }

    /**
     * Remove characteristic values
     *
     * @param idClient Client identifier
     * @param keyValue Key value for item
     * @param characteristicType Characteristic Type for item
     */
    @Override
    public void deleteAdvancedCharacteristic(String idClient, String keyValue, String characteristicType) {
        List<AdCharacteristicValue> characteristicValues = loadCharacteristicValues(idClient, keyValue, characteristicType, null);
        for (AdCharacteristicValue characteristicValue : characteristicValues) {
            if (AdCharacteristicDefService.MODE_ADVANCED.equals(characteristicValue.getCharacteristic().getMode())) {
                if (AdCharacteristicValueService.TYPE_IMAGE.equals(characteristicValue.getVtype())) {
                    AdTable table = tableService.findById(characteristicValue.getCharacteristic().getIdTable());
                    if (table != null) {
                        AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
                        if (baserDir != null) {
                            File file = new File(baserDir.getString() + "/files/" + table.getName() + "/" + characteristicValue.getKeyValue() + "/characteristic/" + characteristicValue.getIdCharacteristic() + "/" + characteristicValue.getImage());
                            if (file.exists()) {
                                file.delete();
                            }
                        }
                    }
                }
                delete(characteristicValue);
            }
        }
    }

    /**
     * Update a characteristic
     *
     * @param idUser User identifier
     * @param idClient Client identifier
     * @param idModule Module identifier
     * @param keyValue Key value for item
     * @param idCharacteristic Characteristic identifier
     * @param value Value
     */
    @Override
    public void updateCharacteristic(String idUser, String idClient, String idModule, String keyValue, String idCharacteristic, String value) {
        Map filter = new HashMap();
        filter.put("idClient", idClient);
        filter.put("idCharacteristic", idCharacteristic);
        filter.put("keyValue", keyValue);
        List characteristicValues = query(
                "SELECT idCharacteristicValue FROM AdCharacteristicValue " +
                "WHERE idClient = :idClient AND idCharacteristic = :idCharacteristic AND keyValue = :keyValue", filter
        );
        if (characteristicValues.size() == 0) {
            if (!Converter.isEmpty(value) && !"null".equals(value)) {
                AdCharacteristicValue characteristicValue = new AdCharacteristicValue();
                characteristicValue.setIdClient(idClient);
                characteristicValue.setIdModule(idModule);
                characteristicValue.setIdCharacteristic(idCharacteristic);
                characteristicValue.setKeyValue(keyValue);
                characteristicValue.setValue(value.trim());
                characteristicValue.setCreatedBy(idUser);
                characteristicValue.setUpdatedBy(idUser);
                characteristicValue.setActive(true);
                save(characteristicValue);
            }
        } else {
            String idCharacteristicValue = (String) characteristicValues.get(0);
            if (Converter.isEmpty(value) || "null".equals(value)) {
                delete(idCharacteristicValue);
            } else {
                applyUpdate(
                        "UPDATE AdCharacteristicValue SET updated = now(), active = 1, updatedBy = " + Converter.getSQLString(idUser) + ", value = " + Converter.getSQLString(value) +
                        " WHERE idCharacteristicValue = " + Converter.getSQLString(idCharacteristicValue)
                );
            }
        }
    }

}
