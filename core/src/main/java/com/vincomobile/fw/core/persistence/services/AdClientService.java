package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdClient;
import com.vincomobile.fw.core.tools.Mailer;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_CLIENT
 *
 * Date: 19/02/2015
 */
public interface AdClientService extends BaseService<AdClient, String> {

    /**
     * Change mailer configuration
     *
     * @param idClient Client identifier
     * @param mailer Mailer
     * @param customSender Standard mail sender
     */
    void setMailerConfig(String idClient, Mailer mailer, JavaMailSenderImpl customSender);

    /**
     * Get all active clients (exclude client: *)
     *
     * @return Client list
     */
    List<AdClient> getActiveClients();

}


