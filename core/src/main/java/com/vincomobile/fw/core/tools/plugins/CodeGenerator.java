package com.vincomobile.fw.core.tools.plugins;

import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CodeGenerator {

    /**
     * Code generation
     *
     * @param mode Generation mode
     * @param options Opciones de generacion
     * @return Lista con el código generado
     */
    List<CodeItem> generate(String mode, Map options) throws IOException, TemplateException;

    /**
     * Adiciona opciones especificas del generador
     *
     * @param id_tipo Tipo de generacion
     * @param options Opciones de generacion
     */
    void addOptions(String id_tipo, Map options);

}
