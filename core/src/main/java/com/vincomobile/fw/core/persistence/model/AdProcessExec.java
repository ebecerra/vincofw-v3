package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.core.process.ProcessDefinition;
import com.vincomobile.fw.core.tools.Converter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de ad_process_exec
 *
 * Date: 28/11/2015
 */
@Entity
@Table(name = "ad_process_exec")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdProcessExec extends AdEntityBean {

    private String idProcessExec;
    private String idUser;
    private String idProcess;
    private Date started;
    private Date finished;
    private String status;

    private AdProcess process;

    private List<AdProcessLog> logs;
    private boolean saved = false;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idProcessExec;
    }

    @Override
    public void setId(String id) {
            this.idProcessExec = id;
    }

    @Id
    @Column(name = "id_process_exec")
    public String getIdProcessExec() {
        return idProcessExec;
    }

    public void setIdProcessExec(String idProcessExec) {
        this.idProcessExec = idProcessExec;
    }

    @Column(name = "id_user")
    @NotNull
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "id_process")
    @NotNull
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "started")
    @NotNull
    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    @Column(name = "finished")
    public Date getFinished() {
        return finished;
    }

    public void setFinished(Date finished) {
        this.finished = finished;
    }

    @Column(name = "status", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_process", referencedColumnName = "id_process", insertable = false, updatable = false)
    public AdProcess getProcess() {
        return process;
    }

    public void setProcess(AdProcess process) {
        this.process = process;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdProcessExec)) return false;

        final AdProcessExec that = (AdProcessExec) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idProcessExec == null) && (that.idProcessExec == null)) || (idProcessExec != null && idProcessExec.equals(that.idProcessExec)));
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((started == null) && (that.started == null)) || (started != null && started.equals(that.started)));
        result = result && (((finished == null) && (that.finished == null)) || (finished != null && finished.equals(that.finished)));
        result = result && (((status == null) && (that.status == null)) || (status != null && status.equals(that.status)));
        return result;
    }

    @Transient
    public String getTime() {
        if (started != null && finished != null) {
            long seconds = (finished.getTime() - started.getTime()) / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            return Converter.leftFill(hours, '0', 2) + ":" + Converter.leftFill(minutes % 60, '0', 2) + ":" + Converter.leftFill(seconds % 60, '0', 2);
        } else {
            return null;
        }
    }

    @Transient
    public List<AdProcessLog> getLogs() {
        if (logs == null) {
            logs = new ArrayList<>();
        }
        return logs;
    }

    public void setLogs(List<AdProcessLog> logs) {
        this.logs = logs;
    }

    @Transient
    public boolean getSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public AdProcessExec duplicate() {
        AdProcessExec result = new AdProcessExec();
        result.idProcessExec = idProcessExec;
        result.idClient = idClient;
        result.idModule = idModule;
        result.created = created;
        result.updated = updated;
        result.createdBy = createdBy;
        result.updatedBy = updatedBy;
        result.active = active;
        result.idUser = idUser;
        result.idProcess = idProcess;
        result.started = started;
        result.finished = finished;
        result.status = status;
        result.saved = saved;
        result.logs = new ArrayList<>();
        result.logs.addAll(logs);
        return result;
    }

}

