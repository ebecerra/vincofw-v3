package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdReference;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service of AD_REFERENCE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdReferenceServiceImpl extends BaseServiceImpl<AdReference, String> implements AdReferenceService {

    /**
     * Constructor.
     *
     */
    public AdReferenceServiceImpl() {
        super(AdReference.class);
    }

}
