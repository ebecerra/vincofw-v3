package com.vincomobile.fw.core.persistence.beans;

import java.util.Map;

public class AdExportCache {

    Map<String, String> values;

    public AdExportCache(Map<String, String> values) {
        this.values = values;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public String getValue(String key) {
        String value = values.get(key);
        return value != null ? value : key;
    }
}
