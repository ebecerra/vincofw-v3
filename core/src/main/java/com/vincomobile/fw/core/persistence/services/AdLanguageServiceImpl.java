package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.FWConfig;
import com.vincomobile.fw.core.persistence.beans.AdLanguageInfo;
import com.vincomobile.fw.core.persistence.model.AdLanguage;

import com.vincomobile.fw.core.tools.Converter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_LANGUAGE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdLanguageServiceImpl extends BaseServiceImpl<AdLanguage, String> implements AdLanguageService {

    /**
     * Constructor.
     *
     */
    public AdLanguageServiceImpl() {
        super(AdLanguage.class);
    }

    /**
     * Get language information by identifier, if not found return default langague
     *
     * @param idLanguage Language identifier
     * @return Language information
     */
    @Override
    public AdLanguageInfo getLanguageInfo(String idLanguage) {
        if (!Converter.isEmpty(idLanguage)) {
            AdLanguage language = getLanguage(idLanguage);
            if (language != null) {
                return new AdLanguageInfo(language.getIdLanguage(), language.getIso2());
            }
        }
        return new AdLanguageInfo(FWConfig.DEFUALT_LANGUAGE_ID, FWConfig.DEFUALT_LANGUAGE_ISO2);
    }

    /**
     * Get a language by identifier, if not found return default langague
     *
     * @param idLanguage Language identifier
     * @return Language
     */
    @Override
    public AdLanguage getLanguage(String idLanguage) {
        Map filter = new HashMap();
        filter.put("idLanguage", idLanguage);
        AdLanguage language = findFirst(filter);
        if (language == null) {
            filter.put("idLanguage", FWConfig.DEFUALT_LANGUAGE_ID);
            language = findFirst(filter);
        }
        return language;
    }

    /**
     * Finds a language by its ISO code
     *
     * @param idClient Client identifier
     * @param iso ISO code
     * @return Language. Returns null if no language matches the ISO code
     */
    @Override
    public AdLanguage findByISO(String idClient, String iso) {
        if (iso == null)
            return null;
        Map filter = new HashMap();
        if (!Converter.isEmpty(idClient))
            filter.put("idClient", getClientFilter(idClient));
        filter.put("iso2", iso);
        return findFirst(filter);
    }

    @Override
    public AdLanguage findByISO(String iso) {
        return findByISO(null, iso);
    }

    /**
     * Get language identifier
     *
     * @param language Language ISO2
     * @return Language identifier
     */
    @Override
    public String getIdLanguageByISO2(String language) {
        String idLanguage = null;
        if (!Converter.isEmpty(language)) {
            AdLanguage lang = findByISO(language);
            if (lang != null) {
                idLanguage = lang.getIdLanguage();
            }
        }
        return idLanguage;
    }

    /**
     * Finds a language by its ISO3 code
     *
     * @param idClient Client identifier
     * @param iso3 ISO code
     * @return Language. Returns null if no language matches the ISO3 code
     */
    @Override
    public AdLanguage findByISO3(String idClient, String iso3) {
        if (iso3 == null)
            return null;
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("iso3", iso3);
        return findFirst(filter);
    }

    /**
     * Get all active languages
     *
     * @return Language list
     */
    @Override
    public List<AdLanguage> getActiveLanguages() {
        Map filter = new HashMap();
        filter.put("active", true);
        return findAll(filter);
    }

}
