package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdDbscript;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 03/10/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_dbscript
 */
@Repository
@Transactional(readOnly = true)
public class AdDbscriptServiceImpl extends BaseServiceImpl<AdDbscript, String> implements AdDbscriptService {

    /**
     * Constructor.
     */
    public AdDbscriptServiceImpl() {
        super(AdDbscript.class);
    }

    /**
     * List all active DB Scripts
     * @return DB Scripts
     */
    @Override
    public List<AdDbscript> listActives() {
        Map filter = new HashMap();
        filter.put("active", true);
        return findAll(new SqlSort("seqno", "asc"), filter);
    }

}
