package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdRefSearchColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Servicio de ad_ref_search_column
 *
 * Date: 07/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdRefSearchColumnServiceImpl extends BaseServiceImpl<AdRefSearchColumn, String> implements AdRefSearchColumnService {

    /**
     * Constructor.
     *
     */
    public AdRefSearchColumnServiceImpl() {
        super(AdRefSearchColumn.class);
    }

}
