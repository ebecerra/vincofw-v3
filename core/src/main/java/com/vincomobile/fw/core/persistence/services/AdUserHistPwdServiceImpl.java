package com.vincomobile.fw.core.persistence.services;

import com.vincomobile.fw.core.persistence.model.AdUserHistPwd;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 07/11/2018.
 * Copyright © 2018 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_user_hist_pwd
 */
@Repository
@Transactional(readOnly = true)
public class AdUserHistPwdServiceImpl extends BaseServiceImpl<AdUserHistPwd, String> implements AdUserHistPwdService {

    /**
     * Constructor.
     */
    public AdUserHistPwdServiceImpl() {
        super(AdUserHistPwd.class);
    }

    /**
     * Get user passwords store
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @return List
     */
    @Override
    public List<AdUserHistPwd> listUserPassword(String idClient, String idUser) {
        Map filter = new HashMap();
        filter.put("idClient", idClient);
        filter.put("idUser", idUser);
        return findAll(new SqlSort("updated", "desc"), filter);
    }


}
