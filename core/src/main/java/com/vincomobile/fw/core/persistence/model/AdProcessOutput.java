package com.vincomobile.fw.core.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_process_output
 *
 * Date: 29/11/2015
 */
@Entity
@Table(name = "ad_process_output")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdProcessOutput extends AdEntityBean {

    private String idProcessOutput;
    private String idProcess;
    private String otype;
    private String server;
    private String username;
    private String password;
    private Long port;
    private String emailTo;
    private String emailName;
    private String filePath;
    private String filePrefix;
    private String fileExt;
    private String fileTimeFmt;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idProcessOutput;
    }

    @Override
    public void setId(String id) {
            this.idProcessOutput = id;
    }

    @Id
    @Column(name = "id_process_output")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdProcessOutput() {
        return idProcessOutput;
    }

    public void setIdProcessOutput(String idProcessOutput) {
        this.idProcessOutput = idProcessOutput;
    }

    @Column(name = "id_process")
    @NotNull
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "otype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getOtype() {
        return otype;
    }

    public void setOtype(String otype) {
        this.otype = otype;
    }

    @Column(name = "server", length = 150)
    @Size(min = 1, max = 150)
    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    @Column(name = "username", length = 30)
    @Size(min = 1, max = 30)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", length = 30)
    @Size(min = 1, max = 30)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "port")
    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    @Column(name = "email_to", length = 2500)
    @Size(min = 1, max = 2500)
    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    @Column(name = "email_name", length = 100)
    @Size(min = 1, max = 100)
    public String getEmailName() {
        return emailName;
    }

    public void setEmailName(String emailName) {
        this.emailName = emailName;
    }

    @Column(name = "file_path", length = 150)
    @Size(min = 1, max = 150)
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Column(name = "file_prefix", length = 30)
    @Size(min = 1, max = 30)
    public String getFilePrefix() {
        return filePrefix;
    }

    public void setFilePrefix(String filePrefix) {
        this.filePrefix = filePrefix;
    }

    @Column(name = "file_ext", length = 30)
    @Size(min = 1, max = 30)
    public String getFileExt() {
        return fileExt;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    @Column(name = "file_time_fmt", length = 30)
    @Size(min = 1, max = 30)
    public String getFileTimeFmt() {
        return fileTimeFmt;
    }

    public void setFileTimeFmt(String fileTimeFmt) {
        this.fileTimeFmt = fileTimeFmt;
    }


    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdProcessOutput)) return false;

        final AdProcessOutput that = (AdProcessOutput) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idProcessOutput == null) && (that.idProcessOutput == null)) || (idProcessOutput != null && idProcessOutput.equals(that.idProcessOutput)));
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((otype == null) && (that.otype == null)) || (otype != null && otype.equals(that.otype)));
        result = result && (((server == null) && (that.server == null)) || (server != null && server.equals(that.server)));
        result = result && (((username == null) && (that.username == null)) || (username != null && username.equals(that.username)));
        result = result && (((password == null) && (that.password == null)) || (password != null && password.equals(that.password)));
        result = result && (((port == null) && (that.port == null)) || (port != null && port.equals(that.port)));
        result = result && (((emailTo == null) && (that.emailTo == null)) || (emailTo != null && emailTo.equals(that.emailTo)));
        result = result && (((emailName == null) && (that.emailName == null)) || (emailName != null && emailName.equals(that.emailName)));
        result = result && (((filePath == null) && (that.filePath == null)) || (filePath != null && filePath.equals(that.filePath)));
        result = result && (((filePrefix == null) && (that.filePrefix == null)) || (filePrefix != null && filePrefix.equals(that.filePrefix)));
        result = result && (((fileExt == null) && (that.fileExt == null)) || (fileExt != null && fileExt.equals(that.fileExt)));
        result = result && (((fileTimeFmt == null) && (that.fileTimeFmt == null)) || (fileTimeFmt != null && fileTimeFmt.equals(that.fileTimeFmt)));
        return result;
    }

}

