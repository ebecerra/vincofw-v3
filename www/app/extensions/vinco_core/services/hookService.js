/**
 * This service handles the dynamic cache for application
 */
define(['app', 'lodash'], function (app, _) {
    "use strict";

    return app.registerFactory('hookService', ['$rootScope', '$log', '$q',
        function ($rootScope, $log, $q) {
            var service = {};

            service.hooks = [];

            /**
             * Register a Hook
             *
             * @param hookName Hook name
             * @param hookPromise Hook promise
             */
            service.registerHook = function (hookName, hookPromise) {
                $log.info("Register hook: " + hookName);
                if (hookPromise) {
                    service.hooks.push({ hookName: hookName, hookPromise: hookPromise });
                }
            };

            /**
             * Get a list of hooks
             *
             * @param hookName Hook name
             * @param args Arguments
             * @returns {Array}
             */
            service.getHooks = function (hookName, args) {
                var promises = [];
                _.each(service.hooks, function (hook) {
                    if (hook.hookName == hookName) {
                        promises.push(hook.hookPromise(args));
                    }
                });
                return promises;
            };

            /**
             * Excecute register hooks
             *
             * @param hookName Hook name
             * @param args Arguments
             */
            service.execHooks = function (hookName, args) {
                var execHooks = service.getHooks(hookName, args);
                var deferred = $q.defer();
                var promise = deferred.promise;
                if (execHooks.length > 0) {
                    $q.all(execHooks).then(function (data) {
                        deferred.resolve({ success: true, results: data });
                    }, function(error) {
                        $log(error);
                        deferred.reject();
                    });
                } else {
                    deferred.resolve({ success: true, results: [] });
                }
                return promise;
            };

            return service;
        }
    ]);
});

