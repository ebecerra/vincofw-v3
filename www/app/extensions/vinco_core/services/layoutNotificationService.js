define(['app', 'jquery', 'lodash', 'blockui'], function (app, $, _) {
    "use strict";

    /**
     * @ngdoc service
     * @name app.layout.service:layoutNotificationService
     * @description
     * Contains all the logic to handle the layout of the site, including methods to handle messages to show to the user
     */
    return app.registerFactory('layoutNotificationService', ['$templateRequest', '$rootScope', function ($templateRequest, $rootScope) {
        var service = {};
        service.messageTypes = {
            ERROR: 'error',
            INFO: 'info',
            WARN: 'warn'
        };

        /**
         * @ngdoc method
         * @name blockUI
         * @methodOf app.layout.service:layoutNotificationService
         * @description
         * Blocks the user interface showing a message
         *
         * @param {string} message Message to show in the UI
         */
        service.blockUI = function (message) {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    'font-size': '1.2em',
                    'font-weight': 'bold'
                },
                message: message
            });
        };

        /**
         * @ngdoc method
         * @name unblock
         * @methodOf app.layout.service:layoutNotificationService
         * @description
         * Unblocks the user interface
         */
        service.unblock = function () {
            $.unblockUI();
        };

        /**
         * @ngdoc method
         * @name showMessage
         * @methodOf app.layout.service:layoutNotificationService
         * @description
         * Shows a pop up message to the user
         *
         * @param {Object} options Options to pass to the dialog
         * @param {string} options.title Message title
         * @param {string} options.message Message content
         * @param {string} options.type Message type ('error', 'warn', 'info')
         */
        service.showMessage = function (options) {
            $rootScope.informationMessage = _.defaults(options || {type: service.messageTypes.INFO}, {
                title: options.title,
                type: options.type,
                message: options.message
            });
            $("#modal-message").modal();
        };

        /**
         * @ngdoc method
         * @name showError
         * @methodOf app.layout.service:layoutNotificationService
         * @description
         * Shows a pop up error message to the user
         *
         * @param {Object} options Options to pass to the dialog
         * @param {string} options.message Message content
         */
        service.showError = function (options) {
            options = _.defaults(options || {}, {
            });
            options.title = $rootScope.getMessage('AD_titleError');
            options.type = service.messageTypes.ERROR;
            service.showMessage(options);
        };

        /**
         * @ngdoc method
         * @name showWarning
         * @methodOf app.layout.service:layoutNotificationService
         * @description
         * Shows a pop up warning message to the user
         *
         * @param {Object} options Options to pass to the dialog
         * @param {string} options.message Message content
         */
        service.showWarning = function (options) {
            options = _.defaults(options || {}, {
            });
            options.title = $rootScope.getMessage('AD_titleWarning');
            options.type = service.messageTypes.WARN;
            service.showMessage(options);
        };

        /**
         * @ngdoc method
         * @name showInfo
         * @methodOf app.layout.service:layoutNotificationService
         * @description
         * Shows a pop up information message to the user
         *
         * @param {Object} options Options to pass to the dialog
         * @param {string} options.message Message content
         */
        service.showInfo = function (options) {
            options = _.defaults(options || {}, {
            });
            options.title = $rootScope.getMessage('AD_titleWarning');
            options.type = service.messageTypes.INFO;
            service.showMessage(options);
        };

        /**
         * @ngdoc method
         * @name showConfirmYesNo
         * @methodOf app.layout.service:layoutNotificationService
         * @description
         * Shows a confirmation dialog
         *
         * @param {String} title Dialog title
         * @param {String} msg Dialog message
         * @param {String} mode Visualization mode (success, info, warning, danger)
         * @param {Object} scope Scope
         * @param {Function} callback Callback to execute when button is clicked
         */
        service.showConfirmYesNo = function (title, msg, mode, scope, callback) {
            $("#dlgConfirm .modal-title").html(title);
            $("#dlgConfirm .modal-body").html('<div class="alert alert-' + mode + '">' + msg + '</div>');
            $("#dlgConfirm .modal-footer .btn-danger").html($rootScope.getMessage('AD_btnCancel'));
            $("#dlgConfirm .modal-footer .btn-success").html($rootScope.getMessage('AD_btnOK'));
            $("#dlgConfirm").modal({ backdrop: 'static' });

            // Events
            $("#dlgConfirm .btn").on("click", function (e) {
                console.log("button pressed: " + e.currentTarget.className);
                $("#dlgConfirm .btn").off("click");
                $("#dlgConfirm").modal('hide');     // dismiss the dialog
                callback.call(scope, e.currentTarget.className.indexOf("btn-success") >= 0);
            });
        };

        return service;
    }]);
});
