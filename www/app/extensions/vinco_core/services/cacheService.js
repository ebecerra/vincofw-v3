/**
 * This service handles the dynamic cache for application
 */
define(['app', 'appConfig'], function (app) {
    "use strict";

    return app.registerFactory('cacheService',[
        '$resource', '$rootScope', '$q', '$log', '$window', '$timeout', 'commonService', 'autogenService', 'authenticationService',
        'adWindowService', 'adTabService', 'adRefListService', 'adRefTableService', 'adRefButtonService', 'adRefSearchColumnService',
        'adTableService', 'adRoleService', 'adReferenceService', 'adPreferenceService', 'adClientLanguageService',
        'adTranslationService', 'adChartFilterService', 'adChartLinkedService', 'adChartColumnService',
        function ($resource, $rootScope, $q, $log, $window, $timeout, commonService, autogenService, authenticationService,
                  adWindowService, adTabService, adRefListService, adRefTableService, adRefButtonService, adRefSearchColumnService,
                  adTableService, adRoleService, adReferenceService, adPreferenceService, adClientLanguageService,
                  adTranslationService, adChartFilterService, adChartLinkedService, adChartColumnService) {
        var service = {};

        // Constants

        // Properties
        service.reloadRetry = 0;
        service.rolClientId = null;
        service.idLanguage = null;
        service.windows = [];
        service.tabs = [];
        service.pendTabs = [];
        service.tables = [];
        service.pendTables = [];
        service.fieldGroups = [];
        service.processes = [];
        service.references = [];
        service.tempTabledir = [];
        service.roles = [];
        service.modules = [];
        service.primaryKeys = [];
        service.editingItem = [];
        service.tabFormItem = {};
        service.preferences = {};

        /**
         * Load preference information (from Cache if exist)
         *
         * @param property Preference property name.
         * @param defValue Default value
         * @returns {*}
         */
        service.loadPreference = function (property, defValue) {
            var defered = $q.defer();
            var promise = defered.promise;
            if (service.preferences[property]) {
                defered.resolve(service.preferences[property]);
            } else {
                adPreferenceService.preference({
                    idClient: service.loggedIdClient(),
                    idRole: authenticationService.getCurrentRole(),
                    name: property
                }, function (data) {
                    if (data.properties && data.properties.preference) {
                        var pref = data.properties.preference;
                        service.preferences[property] = {
                            idPreference: pref.idPreference,
                            value: pref.value,
                            property: pref.property
                        };
                        defered.resolve(service.preferences[property]);
                    } else {
                        if (defValue) {
                            service.preferences[property] = {
                                idPreference: '0',
                                value: defValue,
                                property: property
                            };
                            defered.resolve(service.preferences[property]);
                        } else {
                            defered.reject("No reference with property: " + property);
                        }
                    }
                }, function (err) {
                    defered.reject(err);
                });
            }
            return promise;
        };
        /**
         * Load preference information (from Cache if exist)
         *
         * @param property Preference property name.
         * @param defValue Default value
         * @returns {*}
         */
        service.loadPublicPreference = function (property, defValue) {
            var defered = $q.defer();
            var promise = defered.promise;
            if (service.preferences[property]) {
                defered.resolve(service.preferences[property]);
            } else {
                adPreferenceService.publicPreference({
                    idClient: service.loggedIdClient(),
                    idRole: authenticationService.getCurrentRole(),
                    name: property
                }, function (data) {
                    if (data.properties && data.properties.preference && data.properties.preference.idPreference) {
                        var pref = data.properties.preference;
                        service.preferences[property] = {
                            idPreference: pref.idPreference,
                            value: pref.value,
                            property: pref.property
                        };
                        defered.resolve(service.preferences[property]);
                    } else {
                        if (defValue) {
                            service.preferences[property] = {
                                idPreference: 'undefined',
                                value: defValue,
                                property: property
                            };
                            defered.resolve(service.preferences[property]);
                        } else {
                            defered.reject("No reference with property: " + property);
                        }
                    }
                }, function (err) {
                    defered.reject(err);
                });
            }
            return promise;
        };

        /**
         * Load window information (from Cache if exist)
         *
         * @param idWindow Tab id.
         * @returns {*}
         */
        service.loadWindow = function (idWindow) {
            var defered = $q.defer();
            var promise = defered.promise;
            // Check cache
            var window = _.find(this.windows, function (win) {
                return win.idWindow == idWindow;
            });
            var emptyTabs = true;
            if (window && window.info && window.info.tabs) {
                emptyTabs = _.where(window.info.tabs, function (current) {
                    return current.tab === null || current.tab === undefined;
                });
            }
            if (window && !emptyTabs) {
                defered.resolve(window.info);
            } else {
                var that = this;
                adWindowService.query(
                    {
                        idClient: service.loggedIdClient(),
                        idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : null,
                        q: 'idWindow=' + idWindow
                    },
                    function (data) {
                        if (data.content.length > 0) {
                            // Loading window tabs
                            adTabService.query(
                                {
                                    idClient: service.loggedIdClient(),
                                    idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : null,
                                    q: 'idWindow=' + idWindow,
                                    sort: '[{ "property": "tablevel", "direction": "ASC" }]'
                                },
                                function (dataTabs) {
                                    if (dataTabs.content.length > 0) {
                                        var tabs = [];
                                        _.each(dataTabs.content, function (tab) {
                                            tabs.push({idTab: tab.idTab});
                                            that.pendTabs.push(tab);
                                        });
                                        that.processPendTabs().then(function () {
                                            _.each(tabs, function (tab) {
                                                tab.tab = that.getTab(tab.idTab);
                                            });
                                            var winInfo = {
                                                idWindow: idWindow,
                                                info: {
                                                    idWindow: data.content[0].idWindow,
                                                    name: data.content[0].name,
                                                    tabs: tabs
                                                }
                                            };
                                            that.windows.push(winInfo);
                                            defered.resolve(winInfo.info);
                                        }).catch(function (err) {
                                            defered.reject(err);
                                        });
                                    } else {
                                        defered.reject();
                                    }
                                },
                                function (err) {
                                    defered.reject(err);
                                }
                            );
                        } else {
                            defered.reject();
                        }
                    },
                    function (err) {
                        $log.log(err);
                        defered.reject(err);
                    }
                );
            }
            return promise;
        };

        /**
         * Load tab information (from Cache if exist)
         *
         * @param idTab Tab id.
         * @returns {*}
         */
        service.loadTab = function (idTab) {
            var defered = $q.defer();
            var promise = defered.promise;

            // Check cache
            var tab = _.find(this.tabs, function (tab) {
                return tab.idTab == idTab;
            });
            if (tab) {
                defered.resolve(tab.info);
            } else {
                var that = this;
                adTabService.query(
                    {
                        idClient: service.loggedIdClient(),
                        q: 'idTab=' + idTab
                    },
                    function (data) {
                        if (data.content.length > 0) {
                            that.processTab(data.content[0]).then(function (tab) {
                                defered.resolve(tab);
                            }).catch(function () {
                                defered.reject();
                            });
                        } else {
                            defered.reject();
                        }
                    },
                    function (err) {
                        $log.log(err);
                        defered.reject(err);
                    }
                );
            }

            return promise;
        };

        /**
         * Load table by name
         *
         * @param name Table name
         * @returns {*}
         */
        service.loadTable = function (name) {
            var defered = $q.defer();
            var promise = defered.promise;

            var table = _.find(this.tables, function (table) {
                return table.info.name == name;
            });
            // Check cache
            if (table) {
                defered.resolve(table.info);
            } else {
                var that = this;
                adTableService.query(
                    {
                        idClient: service.loggedIdClient(),
                        q: 'name=' + name
                    },
                    function (data) {
                        if (data.content.length > 0) {
                            that.processTable(data.content[0]).then(function (table) {
                                defered.resolve(table);
                            }).catch(function () {
                                defered.reject();
                            });
                        } else {
                            defered.reject();
                        }
                    },
                    function (err) {
                        $log.log(err);
                        defered.reject(err);
                    }
                );
            }

            return promise;
        };

        /**
         * Load table by id
         *
         * @param id Table id
         * @returns {*}
         */
        service.loadTableById = function (id) {
            var defered = $q.defer();
            var promise = defered.promise;

            var table = _.find(this.tables, function (table) {
                return table.info.name == name;
            });
            // Check cache
            if (table) {
                defered.resolve(table.info);
            } else {
                var that = this;
                adTableService.query(
                    {
                        idClient: service.loggedIdClient(),
                        q: 'id=' + id
                    },
                    function (data) {
                        if (data.content.length > 0) {
                            that.processTable(data.content[0]).then(function (table) {
                                defered.resolve(table);
                            }).catch(function () {
                                defered.reject();
                            });
                        } else {
                            defered.reject();
                        }
                    },
                    function (err) {
                        $log.log(err);
                        defered.reject(err);
                    }
                );
            }

            return promise;
        };

        /**
         * Load Reference information (from Cache if exist)
         *
         * @param idReference Reference Id.
         * @returns {*}
         */
        service.loadReferenceData = function (idReference) {
            var defered = $q.defer();
            var promise = defered.promise;

            // Check cache
            var reference = _.find(this.references, function (ref) {
                return ref.idReference == idReference;
            });

            if (reference) {
                defered.resolve(reference.rtype);
            } else {
                var that = this;
                adReferenceService.query(
                    {
                        idClient: service.loggedIdClient(),
                        q: 'idReference=' + idReference
                    },
                    function (data) {
                        if (data.content.length > 0) {
                            that.loadReference(idReference, data.content[0].rtype).then(function () {
                                that.processPendTables().then(function () {
                                    defered.resolve(data.content[0].rtype);
                                }).catch(function (err) {
                                    $log.log(err);
                                    defered.reject(err);
                                });
                            }).catch(function (err) {
                                $log.log(err);
                                defered.reject(err);
                            });
                        } else {
                            defered.reject("No reference loaded" + idReference);
                        }

                    },
                    function (err) {
                        $log.log(err);
                        defered.reject(err);
                    });
            }
            return promise;
        };

        /**
         * Load Reference information (from Cache if exist)
         *
         * @param idReference Reference Id.
         * @param rtype Reference type
         * @returns {*}
         */
        service.loadReference = function (idReference, rtype) {
            var defered = $q.defer();
            var promise = defered.promise;

            // Check cache
            var reference = _.find(this.references, function (ref) {
                return ref.idReference == idReference;
            });
            if (reference) {
                defered.resolve();
            } else {
                var that = this;
                if (rtype == 'LIST' || rtype == "LISTMULTIPLE") {
                    adRefListService.query(
                        {
                            idClient: service.loggedIdClient(),
                            idLanguage: $rootScope.selectedLanguage ? $rootScope.selectedLanguage.id : null,
                            q: 'idReference=' + idReference
                        },
                        function (data) {
                            that.references.push({idReference: idReference, rtype: rtype, values: data.content});
                            defered.resolve();
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                } else if (rtype == "TABLEDIR" || rtype == "TABLE" || rtype == "SEARCH") {
                    adRefTableService.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: 'idReference=' + idReference
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                var ref = data.content[0];
                                var referenceInfo = {
                                    idReference: idReference,
                                    rtype: rtype,
                                    info: {
                                        idTable: ref.idTable,
                                        idRefTable: ref.idRefTable,
                                        idKey: ref.idKey,
                                        idDisplay: ref.idDisplay,
                                        sqlwhere: ref.sqlwhere,
                                        sqlorderby: ref.sqlorderby
                                    }
                                };
                                that.references.push(referenceInfo);
                                that.pendTables.push(ref.table);
                                if (rtype == 'SEARCH') {
                                    // Obtaining column list
                                    adRefSearchColumnService.query(
                                        {
                                            idClient: service.loggedIdClient(),
                                            q: 'idRefTable=' + ref.idRefTable
                                        },
                                        function (datacol) {
                                            referenceInfo.info.searchColumns = [];
                                            // Loading columns
                                            _.each(datacol.content, function (current) {
                                                referenceInfo.info.searchColumns.push({
                                                    idColumn: current.idColumn,
                                                    name: current.column.name,
                                                    standardName: commonService.removeUnderscores(current.column.name),
                                                    idReference: current.column.idReference,
                                                    idReferenceValue: current.idReferenceValue,
                                                    seqno: current.seqno
                                                });
                                            });
                                            defered.resolve();
                                        },
                                        function (err) {
                                            $log.log(err);
                                            defered.reject();
                                        }
                                    );
                                } else {
                                    defered.resolve();
                                }
                            } else {

                                defered.resolve();   // defered.reject("Empty array received from server for AdRefTable for reference ID " + idReference + "");
                            }
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                } else if (rtype == "BUTTON") {
                    adRefButtonService.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: 'idReference=' + idReference
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                var ref = data.content[0];
                                that.references.push({
                                    idReference: idReference,
                                    rtype: rtype,
                                    info: {
                                        idProcess: ref.idProcess,
                                        btype: ref.btype,
                                        jsCode: ref.jsCode,
                                        icon: ref.icon,
                                        showMode: ref.showMode
                                    }
                                });
                                that.processProcess(ref.process);
                                defered.resolve();
                            } else {
                                defered.reject();
                            }
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                } else {
                    that.references.push({
                        idReference: idReference,
                        rtype: rtype
                    });
                    defered.resolve();
                }
            }
            return promise;
        };

        /**
         * Loads the reference information
         *
         * @param idReference Reference ID
         * @returns {*}
         */
        service.loadReferenceInfo = function (idReference) {
            var defered = $q.defer();
            var promise = defered.promise;
            service.loadReferenceData(idReference)
                .then(function (rtype) {
                    service.loadReference(idReference, rtype)
                        .then(function () {
                            var loadedRef = service.getReference(idReference);
                            defered.resolve(loadedRef);
                        })
                        .catch(function (err) {
                            defered.reject(err);
                        });
                })
                .catch(function (err) {
                    defered.reject(err);
                });
            return promise;
        };

        /**
         * Process information of process and store in cache
         *
         * @param processInfo Process information
         * @returns {*}
         */
        service.processProcess = function (processInfo) {
            // Check cache
            var process = _.find(this.processes, function (proc) {
                return proc.idProcess == processInfo.idProcess;
            });
            if (!process) {
                this.processes.push({
                    idProcess: processInfo.idProcess,
                    info: {
                        idProcess: processInfo.idProcess,
                        name: processInfo.name,
                        description: processInfo.description,
                        ptype: processInfo.ptype,
                        uipattern: processInfo.uipattern,
                        pdf: processInfo.pdf,
                        excel: processInfo.excel,
                        html: processInfo.html,
                        word: processInfo.word,
                        showinparams: processInfo.showinparams
                    }
                });
            }
        };

        /**
         * Process validation rule
         *
         * @param ruleInfo Validation Rule information
         */
        service.processValRule = function (ruleInfo) {
            var valRule = {};
            if (ruleInfo) {
                valRule.idValRule = ruleInfo.idValRule;
                valRule.name = ruleInfo.name;
                valRule.vtype = ruleInfo.vtype;
                valRule.code = ruleInfo.code;
                valRule.errormsg = ruleInfo.errormsg;
            }
            return valRule;
        };

        /**
         * Process table information and store in cache
         *
         * @param tableInfo Table information
         */
        service.processTable = function (tableInfo) {
            var defered = $q.defer();
            var promise = defered.promise;

            // Check cache
            var table = _.find(this.tables, function (table) {
                return table.idTable == tableInfo.idTable;
            });
            if (table) {
                defered.resolve(table.info);
            } else {
                var that = this, columns = [];
                if (tableInfo) {
                    _.each(tableInfo.columns, function (column) {
                        columns.push({
                            idColumn: column.idColumn,
                            idTable: column.idTable,
                            idReference: column.idReference,
                            idReferenceValue: column.idReferenceValue,
                            idValRule: column.idValRule,
                            name: column.name,
                            standardName: commonService.removeUnderscores(column.name),
                            ctype: column.ctype,
                            cformat: column.cformat,
                            csource: column.csource,
                            primaryKey: column.primaryKey,
                            mandatory: column.mandatory,
                            valuemin: column.valuemin,
                            valuemax: column.valuemax,
                            valuedefault: column.valuedefault,
                            readonlylogic: column.readonlylogic,
                            showinsearch: column.showinsearch,
                            groupedsearch: column.groupedsearch,
                            linkParent: column.linkParent,
                            linkColumn: column.linkColumn,
                            translatable: column.translatable,
                            completed: false,
                            reference: {
                                name: column.reference.name,
                                rtype: column.reference.rtype,
                                base: column.reference.base
                            },
                            valRule: that.processValRule(column.valRule)
                        });
                    });
                    var cacheTable = {
                        idTable: tableInfo.idTable,
                        info: {
                            idTable: tableInfo.idTable,
                            name: tableInfo.name,
                            dataOrigin: tableInfo.dataOrigin,
                            isview: tableInfo.isview,
                            exportData: tableInfo.exportData,
                            characteristic: tableInfo.exportData,
                            columns: columns,
                            valRule: that.processValRule(tableInfo.valRule),
                            module: {
                                dbprefix: tableInfo.module.dbprefix ? tableInfo.module.dbprefix : "",
                                restPath: tableInfo.module.restPath,
                                idModule: tableInfo.module.idModule
                            }
                        }
                    };
                    this.tables.push(cacheTable);
                    var module = this.getModule(tableInfo.module.idModule);
                    if (!module) {
                        this.modules.push({
                            dbprefix: tableInfo.module.dbprefix ? tableInfo.module.dbprefix : "",
                            restPath: tableInfo.module.restPath,
                            idModule: tableInfo.module.idModule
                        });
                    }
                    _.each(columns, function (column) {
                        if (column.idReferenceValue) {
                            that.loadReference(column.idReferenceValue, column.reference.rtype).then(function () {
                                column.completed = true;
                                if (!_.find(columns, function (col) {
                                        return !col.completed
                                    })) {
                                    defered.resolve(cacheTable.info);
                                }
                            }).catch(function () {
                                defered.reject();
                            });
                        } else {
                            column.completed = true;
                            if (!_.find(columns, function (col) {
                                    return !col.completed
                                })) {
                                defered.resolve(cacheTable.info);
                            }
                        }
                    });
                }
            }
            return promise;
        };

        /**
         * Process tab information and store in cache
         *
         * @param tabInfo Tab information
         */
        service.processTab = function (tabInfo) {
            var defered = $q.defer();
            var promise = defered.promise;

            var fields = [], charts = [], that = this;
            _.each(tabInfo.fields, function (field) {
                fields.push({
                    column: {
                        linkParent: field.column.linkParent,
                        linkColumn: field.column.linkColumn
                    },
                    valuemax: field.column.valuemax,
                    valuemin: field.column.valuemin,
                    idColumn: field.idColumn,
                    idFieldGroup: field.idFieldGroup,
                    idField: field.id,
                    name: field.name,
                    caption: field.caption,
                    standardName: commonService.removeUnderscores(field.name),
                    readonly: field.readonly,
                    displayed: field.displayed,
                    showingrid: field.showingrid,
                    startnewrow: field.startnewrow,
                    showinstatusbar: field.showinstatusbar,
                    onchangefunction: field.onchangefunction,
                    displaylogic: field.displaylogic,
                    span: field.span,
                    usedInChild: field.usedInChild,
                    filterRange: field.filterRange,
                    filterType: field.filterType,
                    sortable: field.sortable,
                    gridSeqno: field.gridSeqno,
                    seqno: field.seqno,
                    ctype: field.column.ctype,
                    runtime: {}
                });
                if (field.idFieldGroup) {
                    that.fieldGroups.push({
                        idFieldGroup: field.idFieldGroup,
                        info: {
                            idFieldGroup: field.idFieldGroup,
                            name: field.fieldGroup.name,
                            columns: field.fieldGroup.columns,
                            collapsed: field.fieldGroup.collapsed,
                            displaylogic: field.fieldGroup.displaylogic
                        }
                    });
                }
            });
            if (tabInfo.charts) {
                _.each(tabInfo.charts, function (chart) {
                    charts.push({
                        ctype: chart.ctype,
                        displaylogic: chart.displaylogic,
                        extra: chart.extra,
                        idChart: chart.id,
                        idTab: chart.idTab,
                        keyField: chart.keyField,
                        legend1: chart.legend1,
                        legend2: chart.legend2,
                        legend3: chart.legend3,
                        legend4: chart.legend4,
                        legend5: chart.legend5,
                        mode: chart.mode,
                        name: chart.name,
                        prefix: chart.prefix,
                        rowLimit: chart.rowLimit,
                        seqno: chart.seqno,
                        titleX: chart.titleX,
                        titleY: chart.titleY,
                        value1: chart.value1,
                        value2: chart.value2,
                        value3: chart.value3,
                        value4: chart.value4,
                        value5: chart.value5,
                        span: chart.span,
                        startnewrow: chart.startnewrow,
                        runtime: {}
                    });
                });
            }
            var tabData = {
                idTab: tabInfo.idTab,
                info: {
                    idTab: tabInfo.idTab,
                    idTable: tabInfo.idTable,
                    idParentTable: tabInfo.idParentTable,
                    idWindow: tabInfo.idWindow,
                    name: tabInfo.name,
                    tablevel: tabInfo.tablevel,
                    columns: tabInfo.columns,
                    uipattern: tabInfo.uipattern,
                    ttype: tabInfo.ttype,
                    displaylogic: tabInfo.displaylogic,
                    seqno: tabInfo.seqno,
                    command: tabInfo.command,
                    sqlwhere: tabInfo.sqlwhere,
                    fields: fields,
                    charts: charts
                }
            };
            this.tabs.push(tabData);
            if (tabInfo.ttype == "CHART") {
                that.processChartFilters(tabData).then(function () {
                    that.processPendTables().then(function () {
                        defered.resolve(that.getTab(tabInfo.idTab));
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                }).catch(function (err) {
                    defered.reject(err);
                });
            }
            else {
                this.processTable(tabInfo.table).then(function () {
                    that.processPendTables().then(function () {
                        defered.resolve(that.getTab(tabInfo.idTab));
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                }).catch(function (err) {
                    defered.reject(err);
                });
            }
            return promise;
        };

        service.processChartFilters = function (tabData) {
            var defered = $q.defer();
            var promise = defered.promise;
            adChartFilterService.query({
                idClient: authenticationService.loggedIdClient(),
                q: 'idTab=' + tabData.idTab
            }, function (data) {
                tabData.info.chartFilters = [];
                _.each(data.content, function (current) {
                    tabData.info.chartFilters.push({
                        caption: current.caption,
                        description: current.description,
                        displayed: current.displayed,
                        extra: current.extra,
                        idChartFilter: current.id,
                        idReference: current.idReference,
                        idReferenceValue: current.idReferenceValue,
                        mandatory: current.mandatory,
                        name: current.name,
                        standardName: commonService.removeUnderscores(current.name),
                        ranged: current.ranged,
                        saveType: current.saveType,
                        seqno: current.seqno,
                        valuedefault: current.valuedefault,
                        valuedefault2: current.valuedefault2,
                        valuemax: current.valuemax,
                        valuemin: current.valuemin,
                        displaylogic: current.displaylogic
                    });
                });
                defered.resolve();
            }, function (err) {
                defered.reject(err);
            });
            return promise;

        };

        /**
         * Process pending tabs
         *
         * @returns {*}
         */
        service.processPendTabs = function () {
            var defered = $q.defer();
            var promise = defered.promise;

            var tabInfo = this.pendTabs.pop();
            if (tabInfo) {
                var that = this;
                this.processTab(tabInfo).then(function () {
                    that.processPendTabs().then(function () {
                        defered.resolve();
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                }).catch(function (err) {
                    defered.reject(err);
                });
            } else {
                defered.resolve();
            }

            return promise;
        };

        /**
         * Process pending tables
         *
         * @returns {*}
         */
        service.processPendTables = function () {
            var defered = $q.defer();
            var promise = defered.promise;

            var tableInfo = this.pendTables.pop();
            if (tableInfo) {
                var that = this;
                this.processTable(tableInfo).then(function () {
                    that.processPendTables().then(function () {
                        defered.resolve();
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                }).catch(function (err) {
                    defered.reject(err);
                });
            } else {
                defered.resolve();
            }

            return promise;
        };

        service.processChartColumns = function (idTab, idChart) {
            var defered = $q.defer();
            var promise = defered.promise;

            var tab = _.find(service.tabs, function (tab) {
                return tab.idTab == idTab && _.find(tab.info.charts, function (chart) {
                        return chart.idChart == idChart;
                    });
            });
            if (tab) {
                var chart = _.find(tab.info.charts, function (chart) {
                    return chart.idChart == idChart;
                });
                if (chart.columns !== undefined)
                    defered.resolve();
                adChartColumnService.query({
                    idClient: authenticationService.loggedIdClient(),
                    q: 'idChart=' + idChart
                }, function (data) {
                    chart.columns = [];
                    _.each(data.content, function (current) {
                        current.standardName = commonService.removeUnderscores(current.name);
                        current.chartName = chart.name;
                        chart.columns.push(current);
                    });
                    defered.resolve(chart.columns);
                }, function (err) {
                    defered.reject(err);
                });
            }
            else {
                defered.reject("Tab not processed");
            }

            return promise;

        };
        /**
         * Refresh the cache for a new added item
         *
         * @param tableName Table to check
         * @param id New element ID
         */
        service.refreshCache = function (tableName, id) {
            if (tableName == 'ad_role') {
                service.loadRole(id);
            }
        };

        /**
         * Returns idClient associated to the selected role
         *
         * @returns {*} idClient
         */
        service.loggedIdClient = function () {
            return appConfig.ID_CLIENT;
        };

        /**
         * Get role data
         *
         * @param idRole Role id
         */
        service.loadRole = function (idRole) {
            var defered = $q.defer();
            var promise = defered.promise;

            // Check cache
            var role = _.find(this.roles, function (role) {
                return role.idRole == idRole;
            });
            if (role) {
                defered.resolve(role);
            } else {
                var that = this;
                if (!authenticationService.isUserLogged()) {
                    defered.reject();
                } else {
                    adRoleService.query(
                        {
                            idClient: authenticationService.loggedIdClient(),
                            q: 'idRole=' + idRole
                        },
                        function (data) {
                            if (data.content.length > 0) {
                                var resolvedRole = data.content[0];
                                that.roles.push(resolvedRole);
                                defered.resolve(resolvedRole);
                            } else {
                                defered.reject();
                            }
                        },
                        function (err) {
                            $log.log(err);
                            defered.reject(err);
                        }
                    );
                }
            }

            return promise;
        };

        /**
         * Get window information from cache
         *
         * @param idWindow Window id.
         * @returns {*}
         */
        service.getWindow = function (idWindow) {
            var window = _.find(this.windows, function (win) {
                return win.idWindow == idWindow;
            });
            return window ? window.info : null;
        };

        /**
         * Get tab information from cache
         *
         * @param idTab Tab id.
         * @returns {*}
         */
        service.getTab = function (idTab) {
            var tab = _.find(this.tabs, function (tab) {
                return tab.idTab == idTab;
            });
            return tab ? tab.info : null;
        };

        /**
         * Get siblings for the given tab
         *
         * @param idTab Tab id.
         * @returns {*} Siblings
         */
        service.getSiblingTabs = function (idTab) {
            var tab = _.find(this.tabs, function (tab) {
                return tab.idTab == idTab;
            });
            return tab ? _.filter(this.tabs, function (current) {
                return (tab.info.idWindow == current.info.idWindow) && ((tab.info.tablevel + 1) == current.info.tablevel);
            }) : new [];
        };

        /**
         * Get table information from cache
         *
         * @param idTable Table id.
         * @returns {*}
         */
        service.getTable = function (idTable) {
            var table = _.find(this.tables, function (table) {
                return table.idTable == idTable;
            });
            return table ? table.info : null;
        };

        /**
         * Get table information from cache by name
         *
         * @param name Table name
         * @returns {*}
         */
        service.getTableByName = function (name) {
            var table = _.find(this.tables, function (table) {
                return table.info.name == name;
            });
            return table ? table.info : null;
        };

        /**
         * Get process information from cache
         *
         * @param idProcess Process id.
         * @returns {*}
         */
        service.getProcess = function (idProcess) {
            var process = _.find(this.processes, function (proc) {
                return proc.idProcess == idProcess;
            });
            if (process && process.info && !process.info.idClient) {
                process.info.idClient = service.loggedIdClient();
            }
            return process ? process.info : null;
        };

        /**
         * Get reference information from cache
         *
         * @param idReference Reference id.
         * @returns {*}
         */
        service.getReference = function (idReference) {
            return _.find(this.references, function (ref) {
                return ref.idReference == idReference;
            });
        };

        /**
         * Get module information from cache
         *
         * @param idModule Module id.
         * @returns {*}
         */
        service.getModule = function (idModule) {
            return _.find(this.modules, function (mod) {
                return mod.idModule == idModule;
            });
        };

        /**
         * Get field group information from cache
         *
         * @param idFieldGroup Field group id.
         * @returns {*}
         */
        service.getFieldGroup = function (idFieldGroup) {
            var fieldGroup = _.find(this.fieldGroups, function (group) {
                return group.idFieldGroup == idFieldGroup;
            });
            return fieldGroup ? fieldGroup.info : null;
        };

        /**
         * Get detail information of AdRefTable
         *
         * @param idReferenceValue Reference id.
         * @returns {*}
         */
        service.getRefTableInfo = function (idReferenceValue) {
            var refTable = this.getReference(idReferenceValue);
            if (refTable) {
                var table = this.getTable(refTable.info.idTable);
                if (table) {
                    var colKey = _.find(table.columns, function (col) {
                        return col.idColumn == refTable.info.idKey;
                    });
                    var colDisplay = _.find(table.columns, function (col) {
                        return col.idColumn == refTable.info.idDisplay;
                    });
                    return {
                        refTable: refTable,
                        table: table,
                        colKey: colKey,
                        colDisplay: colDisplay
                    }
                }
            }
            return null;
        };

        /**
         * Get a name of reference list value
         *
         * @param idRefList Reference id.
         * @param value Value to find
         * @returns {*}
         */
        service.getListValue = function (idRefList, value) {
            var list = _.find(this.references, function (ref) {
                return ref.idReference === idRefList && ref.rtype == 'LIST';
            });
            if (list) {
                var refList = _.find(list.values, function (item) {
                    return item.value === value;
                });
                return refList ? refList.name : value;
            }
            return value;
        };

        /**
         * Get value of TABLE or TABLEDIR reference (ForeignKey)
         *
         * @param column Column information
         * @param item Row element
         * @returns {*}
         */
        service.getTableValue = function (column, item) {
            if (column.refTableField) {
                if (column.refTableField.fieldValid || column.reference.rtype != 'TABLEDIR') {
                    return column.refTableField.fieldValid && item[column.refTableField.fieldName] ? item[column.refTableField.fieldName][column.refTableField.fieldDisplay] : item[column.standardName];
                }
            }
            column.refTableField = {
                fieldValid: false
            };
            var tableInfo = this.getRefTableInfo(column.idReferenceValue);
            if (tableInfo) {
                if (column.reference.rtype == 'TABLEDIR') {
                    var colInfo = this.getTempTableDir(column.idTable, column.idColumn);
                    if (colInfo) {
                        var value = _.find(colInfo.data.data.content, function (value) {
                            return item[column.standardName] == value[column.standardName];
                        });
                        if (value) {
                            return value[colInfo.data.colDisplay.standardName];
                        }
                    }
                }
                var fieldName = column.name;
                if (fieldName.indexOf('id_') == 0) {
                    fieldName = fieldName.substring(3);
                } else if (fieldName.indexOf('id') == 0) {
                    fieldName = fieldName.substring(2);
                }
                fieldName = commonService.removeUnderscores(fieldName);
                if (fieldName in item) {
                    column.refTableField.fieldValid = true;
                    column.refTableField.fieldName = fieldName;
                    column.refTableField.fieldDisplay = commonService.removeUnderscores(tableInfo.colDisplay.name);
                    return this.getTableValue(column, item);
                }
            }

            return item[column.standardName];
        };

        /**
         * Get table information (TABLEDIR) from temporal cache
         *
         * @param idTable Table id.
         * @param idColumn Column id. (optional)
         * @returns {*}
         */
        service.getTempTableDir = function (idTable, idColumn) {
            var table = _.find(this.tempTabledir, function (t) {
                return t.idTable == idTable;
            });
            if (!table || !idColumn) {
                return table;
            }
            return _.find(table.references, function (col) {
                return col.idColumn == idColumn;
            });
        };

        /**
         * Load to cache values of TABLEDIR fields if not have FK field
         *
         * @param table Table
         * @param reset Indicate if clear cache
         * @returns {*}
         */
        service.loadTempTabledir = function (table, reset, restrictColumns) {
            if (restrictColumns && !Array.isArray(restrictColumns)) {
                $log.warn("Column cache restrict information is not a valid array. Ignoring restriction data.");
            }

            var defered = $q.defer();
            var promise = defered.promise;

            var tabledir = this.getTempTableDir(table.idTable, null);
            if (reset) {
                if (tabledir) {
                    tabledir.references = [];
                }
                _.each(table.columns, function (column) {
                    if (column.reference.rtype == 'TABLEDIR') {
                        column.loadTabledir = false;
                    }
                });
            }

            var columnsToCheck = table.columns;
            if (restrictColumns) {
                columnsToCheck = _.where(columnsToCheck, function (currentColumn) {
                        var found = _.find(restrictColumns, function (current) {
                            return current.standardName === currentColumn.standardName;
                        });
                        return found;
                    }
                );
            }
            var pendColumn = _.find(columnsToCheck, function (column) {
                return column.reference.rtype == 'TABLEDIR' && !column.loadTabledir;
            }, this);

            if (pendColumn) {
                var that = this;
                if (!pendColumn.referenceValue) {
                    pendColumn.referenceValue = this.getReference(pendColumn.idReferenceValue);
                }
                if (!tabledir) {
                    tabledir = {idTable: table.idTable, references: []};
                    that.tempTabledir.push(tabledir);
                }
                pendColumn.loadTabledir = true;
                if (this.isCompleteConditional(pendColumn.referenceValue.info.sqlwhere)) {
                    this.loadReferenceTableData(pendColumn.idReferenceValue, pendColumn.referenceValue.info.sqlwhere).then(function (data) {
                        tabledir.references.push({
                            idColumn: pendColumn.idColumn,
                            data: data
                        });
                        that.loadTempTabledir(table, false, restrictColumns).then(function () {
                            defered.resolve();
                        }).catch(function (err) {
                            defered.reject(err);
                        });
                    }).catch(function (err) {
                        defered.reject(err);
                    });
                } else {
                    var data = null, tableInfo = this.getRefTableInfo(pendColumn.idReferenceValue);
                    if (tableInfo) {
                        data = {
                            colKey: tableInfo.colKey,
                            colDisplay: tableInfo.colDisplay,
                            data: {content: [], totalElements: 0},
                            table: tableInfo.table,
                            idRefTable: tableInfo.refTable.info.idRefTable,
                            sqlwhere: tableInfo.refTable.info.sqlwhere,
                            sqlorderby: tableInfo.refTable.info.sqlorderby
                        };
                    }
                    tabledir.references.push({
                        idColumn: pendColumn.idColumn,
                        data: data
                    });
                    defered.resolve();
                }
            } else {
                defered.resolve();
            }

            return promise;
        };

        service.updateLinkedCharts = function (idChart) {
            var defered = $q.defer();
            var promise = defered.promise;
            var tab = _.find(service.tabs, function (tab) {
                return _.find(tab.charts, function (chart) {
                    return chart.idChart == idChart;
                });
            });
            var chart = null;
            if (tab) {
                chart = _.find(tab.charts, function (chart) {
                    return chart.idChart == idChart;
                });
            }
            if (chart && chart.linkedCharts && chart.linkedCharts.length != 0)
                defered.resolve(chart.linkedCharts);
            else {
                adChartLinkedService.query({
                    idClient: service.loggedIdClient(),
                    q: 'idChart=' + idChart
                }, function (data) {
                    var tab = _.find(service.tabs, function (tab) {
                        return _.find(tab.charts, function (chart) {
                            return chart.idChart == idChart;
                        });
                    });
                    if (tab) {
                        chart = _.find(tab.charts, function (chart) {
                            return chart.idChart == idChart;
                        });
                        chart.linkedCharts = _.map(data.content, function (current) {
                            return current.idLinked;
                        });
                    }
                    defered.resolve(_.map(data.content, function (current) {
                        return current.idLinked;
                    }));
                }, function (err) {
                    defered.reject(err);
                });
            }
            return promise;

        };

        /**
         * Load data for referenced table (AdRefTable)
         *
         * @param idReferenceValue Referenced table id.
         * @param sqlwhere Extra condition
         * @returns {*}
         */
        service.loadReferenceTableData = function (idReferenceValue, sqlwhere) {
            var defered = $q.defer();
            var promise = defered.promise;

            var tableInfo = this.getRefTableInfo(idReferenceValue);
            if (tableInfo) {
                var _service = null;
                try {
                    _service = autogenService.getAutogenService(tableInfo.table);
                } catch (e) {
                    $log.log(e);
                }
                if (_service) {
                    var q = "";
                    if (sqlwhere) {
                        if (!this.isCompleteConditional(sqlwhere)) {
                            defered.resolve({
                                colKey: tableInfo.colKey,
                                colDisplay: tableInfo.colDisplay,
                                data: {content: [], totalElements: 0},
                                table: tableInfo.table,
                                idRefTable: tableInfo.refTable.info.idRefTable,
                                sqlwhere: tableInfo.refTable.info.sqlwhere,
                                sqlorderby: tableInfo.refTable.info.sqlorderby
                            });
                            return;
                        }
                        q += "$extended$={" + sqlwhere + "}";
                    }
                    var sort = tableInfo.colDisplay.csource === "DATABASE" ? tableInfo.colDisplay.standardName : null;
                    if (tableInfo.refTable.info.sqlorderby) {
                        sort = tableInfo.refTable.info.sqlorderby;
                    }
                    // Load data from table
                    _service.query(
                        {
                            idClient: service.loggedIdClient(),
                            q: q,
                            sort: '[{ "property":"' + sort + '","direction":"ASC" }]'
                        },
                        function (data) {
                            defered.resolve({
                                colKey: tableInfo.colKey,
                                colDisplay: tableInfo.colDisplay,
                                data: data,
                                table: tableInfo.table,
                                idRefTable: tableInfo.refTable.info.idRefTable,
                                sqlwhere: tableInfo.refTable.info.sqlwhere,
                                sqlorderby: tableInfo.refTable.info.sqlorderby
                            });
                        },
                        function (err) {
                            // TODO: Error handling
                            defered.reject(err);
                        }
                    );
                } else {
                    // TODO: Error handling
                    defered.reject();
                }
            } else {
                // TODO: Error handling
                defered.reject();
            }

            return promise;
        };

        /**
         * Replace field value in conditional string, for example:
         *
         *    SQL condition: base = 1 and rtype = '{idReference}'
         *
         * @param field Field information
         * @param value Current value of field
         * @param conditional Conditional string
         * @returns {*}
         */
        service.replaceSQLConditional = function (field, value, conditional) {
            if (field && field.column) {
                var pattern = new RegExp("\\{" + field.column.standardName + "\\}", "g");
                if (pattern.test(conditional)) {
                    return conditional.replace(pattern, value);
                }
                pattern = new RegExp("\\{" + field.column.standardName + "\\.(.*?)\\}", "");
                if (pattern.test(conditional)) {
                    var values = conditional.match(pattern);
                    conditional = conditional.replace(pattern, "#(" + field.column.idReferenceValue + ";" + value + ")." + values[1] + "#");
                    if (this.isFieldInConditional(field, conditional)) {
                        return this.replaceSQLConditional(field, value, conditional);
                    } else {
                        return conditional;
                    }
                }
            }
            return conditional;
        };

        /**
         * Replace field value in conditional string, for example:
         *
         *    Javascript condition:
         *          '{idReference}' === 'LIST' || '{idReference}' === 'TABLE' || '{idReference}' === 'TABLEDIR'
         *          @idClient@
         *
         * @param field Field information
         * @param value Current value of field
         * @param conditional Conditional string
         * @returns {*}
         */
        service.replaceJSConditional = function (field, value, conditional) {
            if (field && field.column) {
                var pattern = new RegExp("\\{" + field.column.standardName + "\\}", "g");
                if (pattern.test(conditional)) {
                    return conditional.replace(pattern, value);
                }
            }
            // Predefined replace patterns
            pattern = new RegExp("\\@(.*?)\\@", "g");
            while (pattern.test(conditional)) {
                var values = conditional.match(pattern);
                _.each(values, function (val) {
                    if (val.length > 2) {
                        var item = val.substr(1, val.length - 2);
                        var replacePattern = new RegExp("\\@" + item + "\\@");
                        var value;
                        switch (item) {
                            case "idClient":
                                value = this.loggedIdClient();
                                break;
                            case "idUserLogged":
                                var loginData = authenticationService.getLoginData();
                                value = loginData ? (loginData.idUserLogged ? loginData.idUserLogged : loginData.idUser) : " ";
                                break;
                            case "today":
                                value = new Date();
                                break;
                            case "yesterday":
                                value = moment().subtract(1, 'days').toDate();
                                break;
                            case "tomorrow":
                                value = moment().add(1, 'days').toDate();
                                break;
                            case "year":
                                value = moment().year();
                                break;
                            case "month":
                                value = moment().month() + 1;
                                break;
                            case "week":
                                value = moment().week();
                                break;
                            case "firstMonthDay":
                                var today = new Date();
                                value = moment([today.getFullYear(), today.getMonth()]).toDate();
                                break;
                            case "lastMonthDay":
                                value = moment().endOf('month').toDate();
                                break;
                            case "firstYearDay":
                                value = moment([today.getFullYear()]).toDate();
                                break;
                            case "lastYearDay":
                                value = moment().endOf('year').toDate();
                                break;
                            case "true":
                                value = true;
                                break;
                            case "false":
                                value = false;
                                break;
                            default:
                                value = item;
                                if (item.indexOf('_daysago') > 0) {
                                    var itemParts = item.split('_');
                                    if (itemParts.length == 2) {
                                        value = moment().subtract(parseInt(itemParts[0]), 'days').toDate();
                                    }
                                }
                        }
                        conditional = conditional.replace(replacePattern, value);
                    }
                }, this);
            }
            // Permissions
            pattern = new RegExp("\\#(.*?)\\#", "g");
            while (pattern.test(conditional)) {
                var permissions = conditional.match(pattern);
                _.each(permissions, function (val) {
                    if (val.length > 2) {
                        var item = val.substr(1, val.length - 2);
                        var replacePattern = new RegExp("\\#" + item + "\\#");
                        conditional = conditional.replace(replacePattern, $rootScope.hasPermission(item));
                    }
                }, this);
            }
            return conditional;
        };

        /**
         * Verify if conditional string have replace all parameters
         *
         * @param conditional Conditional string
         * @returns {boolean}
         */
        service.isCompleteConditional = function (conditional) {
            return !/\{(.*?)\}/.test(conditional) && !/\@(.*?)\@/.test(conditional);
        };

        /**
         * Verify if conditional have a reference to field
         *
         * @param field Field information
         * @param conditional Conditional string
         * @returns {boolean}
         */
        service.isFieldInConditional = function (field, conditional) {
            if (field && field.column) {
                var pattern = new RegExp("\\{" + field.column.standardName + "\\}|\\{" + field.column.standardName + "\.(.*?)\\}", "");
                return pattern.test(conditional);
            }
            return false;
        };

        service.getMessages = function () {
            // Load messages
            adTranslationService.webMessages({
                idLanguage: service.getIdLanguage() ? service.getIdLanguage() : "",
                idClient: service.loggedIdClient() ? service.loggedIdClient() : "%%"
            }, function (data) {
                if (data.success) {
                    $rootScope.tranlationsField = data.properties.fields;
                    $rootScope.tranlationsMessages = data.properties.messages;
                    $rootScope.translationChartFilterParams = data.properties.chartFilterParams;
                    $rootScope.translationChartColumns = data.properties.chartColumns;
                    $rootScope.tranlationsProcessParams = data.properties.processParams;
                }
            }, function (data) {
            });
        };

        service.setIdLanguage = function (idLanguage) {
            localStorage['idLanguage'] = idLanguage;
            service.idLanguage = idLanguage;
        };

        service.getIdLanguage = function () {
            return service.idLanguage;
        };

        service.getLanguage = function () {
            var lang = _.find($rootScope.languages, function (current) {
                return current.idLanguage == service.idLanguage;
            });
            if (!lang && $rootScope.languages.length > 0) {
                lang = $rootScope.languages[0];
            }
            return lang;
        };

        service.languagesLoaded = function () {
            return $rootScope.languages === undefined
                || $rootScope.languages === null
                || $rootScope.languages.length === 0;
        };

        service.messagesLoaded = function () {
            return !($rootScope.tranlationsMessages === undefined
            || $rootScope.tranlationsMessages === null
            || $rootScope.tranlationsMessages.length === 0);
        };

        service.loadLanguages = function (reload) {
            var defered = $q.defer();
            var promise = defered.promise;
            if (!$rootScope.languages || reload) {
                // Load languages
                adClientLanguageService.list({
                    idClient: service.loggedIdClient()
                }, function (data) {
                    service.reloadRetry = 0;
                    $rootScope.languages = data.content;
                    $rootScope.selectedLanguage = localStorage['idLanguage'];
                    var lang = _.find($rootScope.languages, function (current) {
                        return current.idLanguage === $rootScope.selectedLanguage;
                    });
                    if (!lang && $rootScope.languages.length > 0) {
                        $rootScope.selectedLanguage = $rootScope.languages[0].idLanguage;
                        service.setIdLanguage($rootScope.languages[0].idLanguage);
                    }
                    defered.resolve($rootScope.languages);
                    if (reload)
                        $window.location.reload();
                    service.getMessages();
                }, function (error) {
                    $log.error("Error loading messages. Attempting to reload");
                    service.reloadRetry++;
                    defered.reject(error);
                    if (service.reloadRetry < 20) {
                        $timeout(function () {
                            service.loadLanguages();
                        }, 1000, true);
                    }
                });
            }
            defered.resolve($rootScope.languages);
            return promise;
        };

        return service;
    }]);
});
