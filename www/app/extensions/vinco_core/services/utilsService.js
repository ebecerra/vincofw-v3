define(['app'], function (app) {
    "use strict";

    return app.registerFactory('utilsService', function ($resource) {
        return $resource(
            appConfig.REST_HOST_CORE + 'utils',
            {
                _: (new Date()).getTime()
            }, {
                max: {
                    method: "GET",
                    url: appConfig.REST_HOST_CORE + 'utils/max_value',
                    transformResponse: function (data) {
                        if (isNaN(data))
                            return 0;
                        return {max: (Number(data))};
                    }
                }
            }
        );
    });
});

