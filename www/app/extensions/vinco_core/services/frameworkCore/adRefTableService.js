define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adRefTableService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_ref_table/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});

