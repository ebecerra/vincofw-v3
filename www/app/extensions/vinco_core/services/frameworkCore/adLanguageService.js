define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adLanguageService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_language/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});

