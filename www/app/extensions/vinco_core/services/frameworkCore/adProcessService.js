define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adProcessService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_process/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT',
                    params: {idProcess: '@idProcess'},
                    url: REST_HOST_CORE + 'ad_process/:idClient/:idProcess'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_process/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                },
                exec: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_process/:idClient/exec/:id/?params=:params',
                    responseType: 'arraybuffer',
                    headers: {'Content-Type': undefined},
                    transformResponse: function (data, headers) {
                        var fileName = 'unknown';
                        if (headers('content-disposition')) {
                            fileName = headers('content-disposition').replace(/attachment; filename=/g, '');
                            fileName = fileName.replace(/"/g, '');
                            fileName = fileName.replace(/'/g, '');
                        }
                        return {
                            result: data,
                            fileName: fileName,
                            headers: headers
                        };
                    }
                },
                execFile: {
                    method: 'POST',
                    params: {idClient: '@idClient', id: '@id'}
                    , url: REST_HOST_CORE + 'ad_process/:idClient/exec_file/:id',
                    responseType: 'arraybuffer',
                    headers: {'Content-Type': undefined},
                    transformRequest: function (data) {
                        var formData = new FormData();
                        var keys = _.keys(data);
                        var containsFiles = false;
                        var newData = {};
                        _.each(keys, function (key) {
                            if (!(data[key] instanceof (FileList))) {
                                formData.append(key, data[key]);
                                 // newData[key] = data[key];
                            }
                            else
                                newData[key] = '';
                        });
                        _.each(keys, function (key) {
                            if ((data[key] instanceof (FileList))) {
                                containsFiles = true;
                                _.each(data[key], function(current){
                                    formData.append("file", current);
                                })
                            }
                        });
                        return formData;
                    },
                    transformResponse: function (data, headers) {
                        var fileName = 'unknown';
                        if (headers('content-disposition')) {
                            fileName = headers('content-disposition').replace(/attachment; filename=/g, '');
                            fileName = fileName.replace(/"/g, '');
                            fileName = fileName.replace(/'/g, '');
                        }
                        return {
                            result: data,
                            fileName: fileName,
                            headers: headers
                        };
                    }
                },
                schedule: {
                    method: 'POST',
                    params: {
                        idProcess: '@idProcess',
                        idClient: '@idClient',
                        idUser: '@idUser',
                        cron: '@cron'
                    },
                    url: REST_HOST_CORE + 'ad_process/:idClient/schedule/:idProcess?cron=:cron&idUser=:idUser'
                },
                clearSchedule: {
                    method: 'DELETE',
                    params: {
                        idProcess: '@idProcess',
                        idClient: '@idClient'
                    },
                    url: REST_HOST_CORE + 'ad_process/:idClient/clear_schedule/:idProcess'
                }
            }
        );
    });
});

