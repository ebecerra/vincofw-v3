define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adTabService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_tab/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT'
                    , params: {
                        idClient: '@idClient',
                        idTab: '@idTab'
                    }
                    , url: REST_HOST_CORE + 'ad_tab/:idClient/:idTab'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_tab/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

