/**
 * Created by yokiro on 02/12/2015.
 */
define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adRefSearchColumnService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_ref_search_column/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});