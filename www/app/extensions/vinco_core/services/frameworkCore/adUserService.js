define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adUserService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_user/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idUser: '@idUser'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT',
                    params: {
                        idClient: '@idClient',
                        idUser: '@idUser'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/:idUser'
                },
                changePassword: {
                    method: 'POST',
                    params: {
                        idClient: '@idClient',
                        idUser: '@idUser',
                        oldPassword: '@oldPassword',
                        newPassword: '@newPassword'
                    },
                    url: REST_HOST_CORE + 'ad_user/:idClient/:idUser/change_password'
                }
            }
        );
    });
});

