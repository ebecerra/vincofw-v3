define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adClientLanguageService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_client_language/:idClient/list',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                list: { method: "GET", isArray: false }
            }
        );
    });
});

