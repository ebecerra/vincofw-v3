define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adProcessParamService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_process_param/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT',
                    params: {idProcessParam: '@idProcessParam'},
                    url: REST_HOST_CORE + 'ad_process_param/:idClient/:idProcessParam'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_process_param/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

