define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adWindowService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_window/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT'
                    , params: {
                        idClient: '@idClient',
                        idWindow: '@idWindow'
                    }
                    , url: REST_HOST_CORE + 'ad_window/:idClient/:idWindow'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_window/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

