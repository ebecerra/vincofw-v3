define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adModuleService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_module/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});

