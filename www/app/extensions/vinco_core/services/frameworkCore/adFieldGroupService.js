define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adFieldGroupService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_field_group/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                update: {
                    method: 'PUT'
                    , params: {
                        idClient: '@idClient',
                        idFieldGroup: '@idFieldGroup'
                    }
                    , url: REST_HOST_CORE + 'ad_field_group/:idClient/:idFieldGroup'
                },
                create: {
                    method: 'POST'
                    , url: REST_HOST_CORE + 'ad_field_group/:idClient',
                    transformResponse: function (data) {
                        return {id: data};
                    }
                }
            }
        );
    });
});

