define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adProcessOutputService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_process_output/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false}
            }
        );
    });
});

