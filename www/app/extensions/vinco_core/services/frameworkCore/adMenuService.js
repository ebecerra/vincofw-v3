define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adMenuService', function ($resource, Authentication) {
        return $resource(
            REST_HOST_CORE +'ad_menu/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient',
                'notExpand': true
            },
            {
                query: {
                    method: "GET",
                    params: {'q': 'username='+Authentication.getLoginData().userLogin},
                    isArray: false
                }
            }
        );
    });
});

