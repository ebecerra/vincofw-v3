define(['app'], function (app) {
    "use strict";

return app.registerFactory('adCharacteristicService', function ($resource) {
        return $resource(
            REST_HOST_CORE + 'ad_characteristic/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false},
                fields: {
                    method: "GET", isArray: true,
                    params: {
                        idTable: '@idTable',
                        dtype: '@dtype',
                        mode: '@mode'
                    },
                    url: REST_HOST_CORE + 'ad_characteristic/:idClient/fields/:idTable/:mode/:dtype'
                }
            }
        );
    });
});

