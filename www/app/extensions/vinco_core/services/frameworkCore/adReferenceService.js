define(['app', 'lodash'], function (app) {
    "use strict";

    return app.registerFactory('adReferenceService', function ($resource, $q) {
        var resource = $resource(
            REST_HOST_CORE + 'ad_reference/:idClient/:id',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            }, {
                query: {method: "GET", isArray: false, cache: true}
            }
        );

        /**
         * Performs a filter against the query result.
         *
         * @param params Params to add to the GET request
         * @param filters Filters to apply to the resulting collection
         * @returns {*} List of elements that matches the filters
         */
        resource.filter = function (params, filters) {
            var deferred = $q.defer();
            resource.query(params, function (data) {
                    var filtered = data.content;
                    if (filters)
                        filtered = _.where(filtered, filters);
                    deferred.resolve(filtered);
                },
                function (data) {
                    deferred.reject('Promise Rejected');
                }
            );
            return deferred.promise;
        };

        return resource;
    });
});

