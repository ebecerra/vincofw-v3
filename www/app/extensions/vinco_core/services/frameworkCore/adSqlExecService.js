define(['app'], function (app) {
    "use strict";

    return app.registerFactory('adSqlExecService', function ($resource) {
        return $resource(
            REST_HOST_CORE +'ad_sql_exec/:idClient',
            {
                _: (new Date()).getTime(),
                idClient: '@idClient'
            },
            {
                exec: {
                    method: 'POST'
                    , params: {
                        idClient: '@idClient'
                    }
                    , url: REST_HOST_CORE + 'ad_sql_exec/:idClient/exec'
                }
            }
        );
    });
});

