define(['app'], function (app) {
    "use strict";

    return app.registerFactory('authenticationService', function () {
        var service = {
            currentRole: '54a2010d5b80ecbc015b8c3370091232'
        };

        /**
         * Gets the active language for the registered user
         *
         * @returns {*}
         */
        service.getCurrentLanguage = function () {
            return 'D804EFC2B38FEEA39FA751540709D0BA';
        };
        service.getCurrentRole = function(){
            return service.currentRole;
        };
        service.setCurrentRole = function(role){
            service.currentRole = role;
        };
        service.isUserLogged = function(){
            // TODO
            return false;
        };

        return service;
    });
});

