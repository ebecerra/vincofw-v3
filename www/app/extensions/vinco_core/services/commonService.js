define(['app', 'moment', 'sprintf'], function (app, moment) {
    "use strict";

    return app.registerFactory('commonService', ['$resource', '$rootScope', '$log', function ($resource, $rootScope, $log) {
        var service = {};

        $rootScope.getMessage = function (key, args) {
            if ($rootScope.tranlationsMessages && angular.isDefined($rootScope.tranlationsMessages[key])) {
                var value = $rootScope.tranlationsMessages[key];
                // Codes sent as parameters are replaced
                if (value && args && args.length > 0) {
                    value = vsprintf(value, args);
                }
                return value ? value : key;
            } else if ($rootScope.tranlationsMessages){
                return key;
            } else {
                return "";
            }
        };

        service.getFormattedDate = function (date, format) {
            if (moment.isMoment(date))
                return date.format(format);
            if (moment.isDate(date))
                return moment(date).format(format);
            if (!moment(date, format).isValid())
                return moment(bookingData.dateTo).format(scope.dateFormat);
            if (moment(date, format).isValid() && typeof(date) === "string")
                return date;
            $log.warn("Could not properly convert : " + date + " to format " + format);
            return date;
        };

        /**
         * Gets the first day of the current month
         *
         * @returns {Date} First day
         */
        service.getFirstDayOfMonth = function () {
            var today = new Date();
            return new Date((today.getMonth() + 1) + /01/ + (today.getYear() + 1900));

        };

        /**
         * Gets the last day of the current month
         *
         * @returns {Date} Last day
         */
        service.getLastDayOfMonth = function () {
            var today = new Date();
            return new Date((today.getYear() + 1900), (today.getMonth() + 1), 0);
        };

        //show message
        service.showLoading = function () {
            $('.jarviswidget-loader').show();
        };

        //show message
        service.showSuccesNotification = function (msg) {
            // $('.jarviswidget-loader').hide();
            // $.smallBox({
            //     title: $rootScope.getMessage('AD_titleSuccess'),
            //     content: $rootScope.getMessage(msg),
            //     color: "#739E73",
            //     timeout: 4000,
            //     icon: "fa fa-check"
            // });
        };

        service.showRejectNotification = function (msg) {
            // $('.jarviswidget-loader').hide();
            // $.smallBox({
            //     title: $rootScope.getMessage('AD_titleFail'),
            //     content: $rootScope.getMessage(msg),
            //     color: "#C46A69",
            //     timeout: 4000,
            //     icon: "fa fa-warning"
            // });
        };

        /**
         * Remove underscores y convert to Java conventions
         *
         * @param input Name to convert (id_item_product)
         * @returns {string} Java-convention name (idItemProduct)
         */
        service.removeUnderscores = function (input) {
            var result = "";
            var tmp = input.split("_");
            for (var i = 0; i < tmp.length; i++) {
                var fragment;
                if (i == 0) {
                    fragment = tmp[i];
                }
                else {
                    fragment = tmp[i].substring(0, 1).toUpperCase() + tmp[i].substring(1);
                }
                result += fragment;
            }
            return result;
        };

        service.downloadMemoryFile = function (memoryStream, fileName) {
            var arrayBuffer2String = function (buf, callback) {
                var f = new FileReader();
                f.onload = function (e) {
                    callback(e.target.result)
                };
                var blob = new Blob([memoryStream], {type: "octet/stream"});
                f.readAsText(blob);
            };
            arrayBuffer2String(memoryStream,
                function (string) {
                    var parsed = null;
                    try {
                        parsed = JSON.parse(string);
                    }
                    catch (e) {
                    }
                    if (!parsed || parsed.success !== false) {
                        var blob = new Blob([memoryStream], {type: "octet/stream"});
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = fileName;
                        link.click();
                    }
                }
            );
        };
        return service;
    }]);
});

