/**
 * This service handles the injection of dynamic services
 * We will asume that the injected service will have the table name (without underscores) followed by the string AutogenService
 * The following is an example of the generated service name
 *
 *      Table       Service Name
 *      ad_column   adColumnAutogenService
 *      ad_table    adTableAutogenService
 */
define(['app'], function (app) {
    "use strict";

    return app.registerFactory('autogenService', function ($resource, $rootScope, $injector, commonService, $log) {
        var service = {};

        service.getAutogenService = function (adTable) {
            $log.log("Generating autogen service for table: " + adTable.name);
            var serviceName = commonService.removeUnderscores(adTable.name) + "AutogenService";
            var service = null;
            try {
                service = $injector.get(serviceName);
            } catch (e) {
                $log.log("Not found: " + serviceName + " it be created");
            }
            if (service) {
                $log.log("Returning already registered service for table: " + adTable.name);
                return service;
            } else {
                var primaryKey = _.find(adTable.columns, function (currentCol) {
                    return currentCol.primaryKey;
                });
                if (!primaryKey) {
                    $log.error("Could not register service. Table does not have a primary key. Table: " + adTable.name);
                    return null;
                }
                var params = {};
                params[primaryKey.standardName] = '@' + primaryKey.standardName;
                params['_'] = (new Date()).getTime();
                var putParams = {};
                putParams[primaryKey.standardName] = '@' + primaryKey.standardName;

                var baseUrl = adTable.module.restPath + '/' + adTable.name;
                var url = baseUrl + '/:idClient/:id';
                var exportUrl = baseUrl + "/:idClient/export/:idTab";
                var putUrl = baseUrl + '/:idClient/:' + primaryKey.standardName;
                var postUrl = baseUrl + '/:idClient';
                if (adTable.name == "ad_client") {
                    url = baseUrl + "/:id";
                    exportUrl = baseUrl + "/export/:idTab";
                    putUrl = baseUrl + '/:' + primaryKey.standardName;
                    postUrl = baseUrl;
                }
                var operations = {
                    query: {
                        method: "GET",
                        isArray: false
                    },
                    export: {
                        method: "GET",
                        url: appConfig.REST_HOST_PATH + exportUrl,
                        responseType: 'arraybuffer',
                        transformResponse: function (data, headers) {
                            var fileName = 'unknown';
                            if (headers('content-disposition')) {
                                fileName = headers('content-disposition').replace(/attachment; filename=/g, '');
                                fileName = fileName.replace(/"/g, '');
                                fileName = fileName.replace(/'/g, '');
                            }
                            return {
                                result: data,
                                fileName: fileName,
                                headers: headers
                            };
                        }
                    },
                    // Updates an item
                    update: {
                        method: 'PUT',
                        params: putParams,
                        url: appConfig.REST_HOST_PATH + putUrl
                    },
                    // Updates an item
                    updateFile: {
                        method: 'PUT',
                        params: putParams,
                        url: appConfig.REST_HOST_PATH + putUrl,
                        headers: {'Content-Type': undefined},
                        transformRequest: function (data) {
                            var formData = new FormData();
                            var keys = _.keys(data);
                            var containsFiles = false;
                            var newData = {};
                            _.each(keys, function (key) {
                                if (!(data[key] instanceof (FileList))) {
                                    newData[key] = data[key];
                                }
                            });
                            var entityData = new Blob([JSON.stringify(newData)], {
                                type: "application/json"
                            });
                            formData.append("entity", entityData);
                            var multipleFiles = _.filter(keys, function (key) {
                                return ((data[key] instanceof (FileList)))
                            }).length > 1;
                            _.each(keys, function (key) {
                                if ((data[key] instanceof (FileList))) {
                                    containsFiles = true;
                                    formData.append("file_"+key, data[key][0]);
                                }
                            });
                            return formData;
                        }
                    },
                    // Creates an item
                    create: {
                        method: 'POST',
                        url: appConfig.REST_HOST_PATH + postUrl,
                        transformResponse: function (data) {
                            return {id: data};
                        }
                    },
                    // Creates an item
                    createFile: {
                        method: 'POST',
                        url: appConfig.REST_HOST_PATH + postUrl,
                        headers: {'Content-Type': undefined},
                        transformRequest: function (data) {
                            var formData = new FormData();
                            var keys = _.keys(data);
                            var containsFiles = false;
                            var newData = {};
                            _.each(keys, function (key) {
                                if (!(data[key] instanceof (FileList))) {
                                    newData[key] = data[key];
                                }
                            });

                            var entityData = new Blob([JSON.stringify(newData)], {
                                type: "application/json"
                            });
                            formData.append("entity", entityData);
                            _.each(keys, function (key) {
                                if ((data[key] instanceof (FileList))) {
                                    containsFiles = true;
                                    formData.append("file_"+key, data[key][0]);
                                }
                            });
                            return formData;
                        }, transformResponse: function (data) {
                            return {id: data};
                        }
                    },
                    // Deletes an item
                    delete: {
                        method: 'DELETE',
                        params: {id: '@id'}
                    }
                };
                app.registerFactory(serviceName, function ($resource) {
                    return $resource(
                        appConfig.REST_HOST_PATH + url,
                        {
                            _: (new Date()).getTime(),
                            idClient: '@idClient'
                        },
                        operations
                    );
                });
                return $injector.get(serviceName);
            }
        };
        return service;
    });
});

