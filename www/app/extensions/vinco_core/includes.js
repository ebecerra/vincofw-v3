var APP_EXTENSION_VINCO_CORE = 'app/extensions/vinco_core/';
var EXTENSION_VINCO_CORE = 'extensions/vinco_core/';

MODULES['vinco_core'] = APP_EXTENSION_VINCO_CORE;

define([
    'angular',
    
    // Module
    EXTENSION_VINCO_CORE + 'module',

    // Services
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adClientService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adClientLanguageService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adColumnService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adFieldGroupService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adFieldService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adLanguageService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adMenuService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adModuleService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adRefButtonService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adReferenceService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adRefListService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adRefSearchColumnService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adRefTableService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adRoleService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adTableService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adTabService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adTranslationService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adUserService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adWindowService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/utilsService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adRefButtonService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adProcessService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adProcessParamService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adProcessOutputService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adPreferenceService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adCharacteristicService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adCharacteristicValueService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adSqlExecService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adChartFilterService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adChartService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adChartLinkedService',
    EXTENSION_VINCO_CORE + 'services/frameworkCore/adChartColumnService',
    
    // Filters
    EXTENSION_VINCO_CORE + 'filters/range',

    // Directives
    EXTENSION_VINCO_CORE + 'components/bootstrapDatepicker/bootstrapDatepicker',
    EXTENSION_VINCO_CORE + 'components/touchspin/touchspin',
    EXTENSION_VINCO_CORE + 'components/validFile/validFile',
    EXTENSION_VINCO_CORE + 'components/numbersOnly/numbersOnly',
    EXTENSION_VINCO_CORE + 'components/invertVisibility/invertVisibility',

    // Services
    EXTENSION_VINCO_CORE + 'services/authenticationService',
    EXTENSION_VINCO_CORE + 'services/autogenService',
    EXTENSION_VINCO_CORE + 'services/Base64',
    EXTENSION_VINCO_CORE + 'services/cacheService',
    EXTENSION_VINCO_CORE + 'services/commonService',
    EXTENSION_VINCO_CORE + 'services/hookService',
    EXTENSION_VINCO_CORE + 'services/layoutNotificationService',
    EXTENSION_VINCO_CORE + 'services/utilsService'

], function () {
    'use strict';
});