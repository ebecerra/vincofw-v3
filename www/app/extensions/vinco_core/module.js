/**
 * @ngdoc overview
 * @name web.vinco_core
 * @description
 * # web.vinco_core
 *
 * Module containing the core logic of the application
 */
define(['angular',
    'angular-couch-potato',
    'angular-ui-router'], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('web.vinco_core', ['ui.router']);

    couchPotato.configureApp(module);

    module.config(['$stateProvider', '$couchPotatoProvider', function ($stateProvider, $couchPotatoProvider) {
    }]);

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;

});
