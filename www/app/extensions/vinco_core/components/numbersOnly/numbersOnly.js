define(['app', 'jquery'], function (app, $) {
    "use strict";

    /**
     * @ngdoc directive
     * @name app.directive:numbersOnly
     * @description Limits the input of an input element to support only numbers
     * @restrict A
     * @param {Boolean} allowBlank Set to true to allow empty text. False otherwise. Defaults to true.
     * @param {int} min Minimum value to allow. Non required.
     * @param {int} max Maximum value to allow. Non required.
     */
    app.registerDirective('numbersOnly', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope:{
              allowBlank:'=',
              min:'=',
              max:'='
            },
            link: function (scope, element, attr, ngModel) {
                if (scope.allowBlank===null || scope.allowBlank === undefined)
                    scope.allowBlank = true;
                element.removeAttr("allow-blank");
                element.removeAttr("numbers-only");
                element.on("keydown", function (event) {
                    var value = ngModel.$viewValue + "";
                    switch(event.which) {
                        case 8:// Backspace
                        case 46:// Delete
                        case 37:// Arrow Left
                        case 39:// Arrow Right
                            return;
                    }
                    var attemptedValue = value + event.key;
                    if (isNaN(attemptedValue)) {
                        event.preventDefault();
                    }
                });
                element.on("keyup", function (event) {
                    switch(event.which) {
                        case 37:// Arrow Left
                        case 39:// Arrow Right
                            return;
                    }
                    if ((event.target.value==="" || event.target.value===null) && !scope.allowBlank){
                        var min = 0;
                        if (scope.min)
                            min = scope.min;
                        ngModel.$setViewValue(Number(min));
                        event.target.value = Number(min);
                        return;
                    }
                    var attemptedNumber = Number(ngModel.$viewValue);
                    if (!isNaN(attemptedNumber)) {
                        if (attr.max && !isNaN(Number(attr.max))) {
                            if (attemptedNumber > Number(attr.max)) {
                                ngModel.$setViewValue(Number(attr.max));
                                $(element[0]).val(attr.max);
                                return;
                            }
                        }
                        if (attr.min && !isNaN(Number(attr.min))) {
                            if (attemptedNumber < Number(attr.min)) {
                                ngModel.$setViewValue(Number(attr.min));
                                $(element[0]).val(attr.min);
                                return;
                            }
                        }
                        ngModel.$setViewValue(Number(event.target.value));
                        event.target.value = Number(event.target.value);
                    } else {
                        event.preventDefault();
                    }
                });
            }
        };
    });
});
