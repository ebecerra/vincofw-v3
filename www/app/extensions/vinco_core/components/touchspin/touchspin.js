define(['app', 'jquery', 'bootstrap-touchspin'], function (app, $) {
    "use strict";

    app.registerDirective('touchspin', function () {
        return {
            restrict: "A",
            scope: {
                min:'=',
                max:'='
            },
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attributes, ngModel) {
                        element.removeAttr('touchspin');
                        var options = {
                            verticalbuttons: true
                        };
                        if (scope.min!==null && scope.min!== undefined){
                            options.min = scope.min;
                        }
                        var max = 0;
                        if (scope.max!==null && scope.max!== undefined){
                            options.max = scope.max;
                        }
                        $(element).TouchSpin(options);
                    }
                }

            }
        };
    })
});
