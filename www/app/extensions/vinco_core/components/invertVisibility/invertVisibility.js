define(['app', 'jquery'], function (app, $) {
    "use strict";

    /**
     * @ngdoc directive
     * @name app.directive:invertVisibility
     * @description Inverts the visibility of the applied element, when a click event is triggered
     * @restrict A
     * @param {String} relatedElement Related element ID. Inverts the visibility of the related element
     */
    app.registerDirective('invertVisibility', function() {
        return {
            restrict: 'A',
            scope:{
                relatedElement:'@?'
            },
            link: function (scope, element, attr, ngModel) {
                element.removeAttr("invert-visibility");

                var related = null;
                if (scope.relatedElement)
                    related = $("#"+scope.relatedElement);
                element.removeAttr("related-element");
                element.on("click", function (event) {
                    if ($(element).hasClass('hide')){
                        $(element).removeClass('hide');
                    }
                    else{
                        $(element).addClass('hide');
                    }
                    if (related){
                        if (related.hasClass('hide')){
                            related.removeClass('hide');
                        }
                        else{
                            related.addClass('hide');
                        }
                    }
                });

            }
        };
    });
});
