define(['app', 'jquery', 'moment', 'bootstrap-datetimepicker'], function (app, $, moment) {
    "use strict";

    /**
     * @ngdoc directive
     * @name app.directive:numbersOnly
     * @description Limits the input of an input element to support only numbers
     * @restrict A
     * @param {Boolean} allowBlank Set to true to allow empty text. False otherwise. Defaults to true.
     * @param {int} min Minimum value to allow. Non required.
     * @param {int} max Maximum value to allow. Non required.
     */
    app.registerDirective('bootstrapDatepicker', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope:{
                options:'@'
            },
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attr, ngModel) {
                        if (scope.allowBlank === null || scope.allowBlank === undefined)
                            scope.allowBlank = true;
                        scope.datepickerOptions = scope.options ? JSON.parse(scope.options) : undefined;
                        scope.datepickerOptions = _.defaults(scope.datepickerOptions || {},{
                            widgetPositioning: {
                                horizontal: 'auto',
                                vertical: 'bottom'
                            },
                            format: 'DD/MM/YYYY'
                        });
                        element.removeAttr("bootstrap-datepicker");
                        $(element).off('dp.change');
                        $(element).on('dp.change', function (e) {
                            ngModel.$setViewValue(e.date);
                        });
                        if (ngModel) {
                            ngModel.$render = function () {
                                if (this.$viewValue) {
                                    var format = "DD/MM/YYYY";
                                    if (!Number.isNaN(Number(this.$viewValue))) {
                                        $(element).val(moment(Number(this.$viewValue)).format(format));
                                    } else if (moment(this.$viewValue, format).isValid()){
                                        $(element).val(moment(this.$viewValue, format).format("DD/MM/YYYY"));
                                    } else if (moment.isMoment(this.$viewValue)){
                                        $(element).val(this.$viewValue.format("DD/MM/YYYY"));
                                    } else {
                                        $(element).val(moment(this.$viewValue).format("DD/MM/YYYY"));
                                    }
                                } else
                                    $(element).val('');
                            }
                        }
                        $(element).datetimepicker(scope.datepickerOptions);
                    }
                }
            }
        }
    });
});
